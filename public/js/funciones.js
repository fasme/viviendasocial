var eventos = {
    CAIDA: 1,
    PUERTA: 2,
    NICTURIA: 3
};

var esRutValido = function(ruti, dvi) {
    var rut = ruti + "-" + dvi;
    if (rut.length < 7)
        return(false)
    i1 = rut.indexOf("-");
    dv = rut.substr(i1 + 1);
    dv = dv.toUpperCase();
    nu = rut.substr(0, i1);

    cnt = 0;
    suma = 0;
    for (i = nu.length - 1; i >= 0; i--) {
        dig = nu.substr(i, 1);
        fc = cnt + 2;
        suma += parseInt(dig) * fc;
        cnt = (cnt + 1) % 6;
    }
    dvok = 11 - (suma % 11);
    if (dvok == 11)
        dvokstr = "0";
    if (dvok == 10)
        dvokstr = "K";
    if ((dvok != 11) && (dvok != 10))
        dvokstr = "" + dvok;
    if (dvokstr == dv)
        return(true);
    else
        return(false);
}

var Fn = {
    validaRut : function (rutCompleto) {
        if (!/^[0-9]+-[0-9kK]{1}$/.test( rutCompleto ))
            return false;
        var tmp     = rutCompleto.split('-');
        var digv    = tmp[1]; 
        var rut     = tmp[0];
        if ( digv == 'K' ) digv = 'k' ;
        return (Fn.dv(rut) == digv );
    },
    dv : function(T){
        var M=0,S=1;
        for(;T;T=Math.floor(T/10))
            S=(S+T%10*(9-M++%6))%11;
        return S?S-1:'k';
    }
}

var getCurrentDate = function() {
    return moment().format("DD-MM-YYYY");
}

var validarFormatoFecha = function(fecha) {
    var RegExPattern = /^\d{1,2}\-\d{1,2}\-\d{2,4}$/;
    if(!fecha.match(RegExPattern)) return false;
    return true;
}

var esFechaMayor=function(fecha){
    var current = getCurrentDate();
    if (Date.parse(fecha) > Date.parse(current)) return true;
    return false;
}

var soloNumeros=function(e){
	var keynum = window.event ? window.event.keyCode : e.which;
    if ((keynum == 8) || (keynum == 46))
        return true;
    return /\d/.test(String.fromCharCode(keynum));
}

function darVueltaFechaString(fecha){ 
    var fmind = fecha.replace(/\//g, '-' ).split('-');
    return fmind[2] + '-' + fmind[1] + '-' + fmind[0];
}

function formatoFecha(fecha){ 
    return new Date(fecha.replace(/\-/g, '/' ));
}

function compararDosFechas(idMin, idMax){
    var min = formatoFecha(darVueltaFechaString(idMin));
    var max = formatoFecha(darVueltaFechaString(idMax));
    if(min<=max)
        return "OK";
    
    if(min>max)
        return "La fecha de término no puede ser menor a la inicial";
    
}

var dataDualList=function(msg){
    return {
        filterPlaceHolder: "Buscar",
        filterTextClear: "Quitar todo",
        infoText: "Mostrando {0}",
        moveAllLabel: "Mover todo",
        selectedListLabel: msg+" seleccionadas",
        nonSelectedListLabel: msg+" no seleccionados",
        infoTextEmpty: "Lista vacía",
        infoTextFiltered: "<span class='label label-warning'>Filtrados</span> {0} de {1}"
    }
}


var eliminarUsuario = function(rut, tipoUsuario){
    bootbox.confirm({
    title: 'Confirmación',
    message: '<h4>¿Está seguro que desea eliminar el '+tipoUsuario+'?</h4>',
    buttons: {
      'confirm': {
        label: 'Aceptar',
        className: 'btn-primary'
      },
      'cancel': {
        label: 'Cancelar',
        className: 'btn-danger'
      }
    },
    callback: function(result) {
        if (result) {
            $.ajax({
                url: "eliminarUsuario",
                data: {rut: rut, tipoUsuario: tipoUsuario},
                type: "post",
                dataType: "json",
                success: function(data){
                    console.log(data);                  
                    if(data.exito) bootbox.alert("<h4>"+data.exito+"</h4>", function(){ 
                        location.reload();
                    });
                    if(data.error) bootbox.alert("<h4>"+data.error+"</h4>");
                    
                }
            });
        }
    }
    });
}

// Agrega un paciente a la lista de pacientes
var agregarContactoALista = function(idBotonLista, idRut, idDv, idNombre, idBuscar, idSelect, tipoUsuario){
    $(idBotonLista).prop("disabled", true);
    var rut = $(idRut).val();
    var dv = $( idDv).val();
    var nombre = $(idNombre).val();
    console.log($(idBuscar).val());
    if($(idBuscar).val() == rut+"-"+dv && rut!=""){     
        if($(idSelect+' option[value="'+rut+'"]').length == 0){
             $(idSelect).append('<option value="'+rut+'" selected="selected"><strong>'+rut+"-"+dv+"</strong> - "+nombre+'</option>');
        }
    }else{
        bootbox.alert("<h4>Seleccione un "+tipoUsuario+" de la lista de sugerencias</h4>");
    }
    $(idBuscar).val("");
} 

// Elimina todos los option de la lista  
var limpiarListaContactos = function(idSelect){
    $(idSelect).empty();
}

// Elimina uno o más option seleccionados de la lista  
var eliminarContactoDeLista = function(idSelect){
    $(idSelect).each(function() {
        $(this).find('option:selected').remove();
    });
}

function textoEspacioGuion(e){  
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toLowerCase();
    if(tecla=="'" || tecla=="\"")return false;
    letras = " áéíóúabcdefghijklmnñopqrstuvwxyz-";
    especiales = [8,39];
     //if(tecla.indexOf("'")==-1) return false;
     tecla_especial = false;
     for(var i in especiales){
        if(key == especiales[i]){
            tecla_especial = true;
            break;
        } 
     }
     if(letras.indexOf(tecla)==-1 && !tecla_especial)
        return false;
}

function textoRut(e){  
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toLowerCase();
    if(tecla=="'" || tecla=="\"")return false;
    letras = "1234567890k.-";
    especiales = [8,39];
     //if(tecla.indexOf("'")==-1) return false;
     tecla_especial = false;
     for(var i in especiales){
        if(key == especiales[i]){
            tecla_especial = true;
            break;
        } 
     }
     if(letras.indexOf(tecla)==-1 && !tecla_especial)
        return false;
}

function numerosSinPunto(e){  
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toLowerCase();
    if(tecla=="'" || tecla=="\"")return false;
    letras = "1234567890";
    especiales = [8,39];
     tecla_especial = false;
     for(var i in especiales){
        if(key == especiales[i]){
            tecla_especial = true;
            break;
        } 
     }
     if(letras.indexOf(tecla)==-1 && !tecla_especial)
        return false;
}

function habilitarBoton(idFormulario, idBotonEditar, idBotonGuardar){
    $('#' + idFormulario).find('input[type=text], input[type=number], input[type=date], input[type=time], input[type=checkbox], textarea,  select, input[type=radio],button.btn_agregar').removeAttr('disabled'); 
    $('#' + idBotonEditar).hide();
    $('#' + idBotonGuardar).show();
    /*$('#' + idFormulario).find('input[class=btn-bonito]').bootstrapSwitch('disabled', false);

    $("#"+idFormulario+" select[multiple]").attr('disabled', false);
    $("#"+idFormulario+" select[multiple] option").attr('disabled', false);
    $("#"+idFormulario+" select[multiple]").selectpicker('refresh');*/
    $("#"+idFormulario+" .btn-radio-group").removeClass('disabled');

}

function deshabilitarBoton(idFormulario, idBotonEditar, idBotonGuardar){
    $('#' + idFormulario).find('input[type=text], input[type=number], input[type=date], input[type=time], input[type=checkbox], textarea,  select, input[type=radio],button.btn_agregar').attr('disabled','disabled'); 
    $('#' + idBotonEditar).show();
    $('#' + idBotonGuardar).hide();
    /*$('#' + idFormulario).find('input[class=btn-bonito]').bootstrapSwitch('disabled', true);

    $("#"+idFormulario+" select[multiple]").attr('disabled', false);
    $("#"+idFormulario+" select[multiple] option").attr('disabled', true);
    $("#"+idFormulario+" select[multiple]").selectpicker('refresh');*/
    $("#"+idFormulario+" .btn-radio-group").addClass('disabled').prop('disabled',true);
    
}

function calcularPromTestTimedGetUp(p1,p2,p3,resultado){
    if($("#"+p1).val() !="" && $("#"+p2).val() !="" &&  $("#"+p3).val() !=""){
        valor1 = parseFloat( $("#"+p1).val() ); 
        valor2 = parseFloat( $("#"+p2).val() );
        valor3 = parseFloat( $("#"+p3).val() );
        suma   = parseFloat(valor1 + valor2 + valor3);
        promedio   = (suma/3).toFixed(2);
        $("#"+resultado).val(promedio);
    }else{
        $("#"+resultado).val(0);;
    }
}


function calcularEvaluacionConductaMotrizInicial(){

    var num1 = $("#formConductaMotrizInicial").find("input[type='radio'][name=p-8]:checked").val()
    var num2 = $("#formConductaMotrizInicial").find("input[type='radio'][name=p-9]:checked").val()
    var total = num1 * num2;
    $("#TotalEvaluacionConducaMotrizInicial").val(total);
    console.log(num1);
    console.log(num2);


}

function calcularEvaluacionConductaMotrizFinal(){

    var num1 = $("#formConductaMotrizFinal").find("input[type='radio'][name=p-8]:checked").val()
    var num2 = $("#formConductaMotrizFinal").find("input[type='radio'][name=p-9]:checked").val()
    var total = num1 * num2;
    $("#TotalEvaluacionConducaMotrizFinal").val(total);
    console.log(num1);
    console.log(num2);


}

//se usa
function sumarMmse(name,idResultado,idMensaje,idFormulario){

    console.log("hohasdhasdadalñk");
    resultado=0;
    for (var i = 1; i <= 30; i++) {
  
        if( $('#'+idFormulario).find('input[type="radio"][id='+name+i+']:checked').attr('valor') != null ){
            resultado += parseInt($('#'+idFormulario).find('input[type="radio"][id='+name+i+']:checked').attr('valor'));
        }    
    };

    $("."+idResultado).val(resultado);
    $("#resultadoMmse").text(resultado);

}


//se usa
function sumarIndiceBarthel(name, idResultado, idMensaje, idFormulario){
   
    resultado = 0;
    for (var i = 1; i <= 10; i++) {
        if( $('#'+idFormulario).find('input[type="radio"][name='+name+i+']:checked').val() != null )
            resultado += parseInt($('#'+idFormulario).find('input[type="radio"][name='+name+i+']:checked').val());
    };

    $("."+idResultado).val(resultado);
    $("#resultadoBarthel").text(resultado);

}

//se usa
function sumarIndiceLawtonBrody(name, idResultado, idMensaje, idFormulario){
   
    resultado = 0;
    for (var i = 1; i <= 31; i++) {
        if( $('#'+idFormulario).find('input[type="radio"][id='+name+i+']:checked').attr('valor') != null ){
            resultado += parseInt($('#'+idFormulario).find('input[type="radio"][id='+name+i+']:checked').attr('valor'));
        }    
    };
    
    $("."+idResultado).val(resultado);
    $("#resultadoLawton").text(resultado);

}

//se usa
function sumarIndiceCharlson(name, idResultado, idMensaje, idFormulario){
   
    resultado = 0;

    for (var i = 1; i <= 19; i++) {
        if( $('#'+idFormulario).find('input[type="checkbox"][id='+name+i+']:checked').val() != null ){
            resultado += parseInt($('#'+idFormulario).find('input[type="checkbox"][id='+name+i+']:checked').val());
        }    
    };
    
    $("."+idResultado).val(resultado);
    $("#resultadoCharlson").text(resultado);

}




//se usa
function sumarIndiceWhoqol(name, idResultado, idMensaje, idFormulario){
    //console.log("idFormulario "+idFormulario+ " name "+name+" ");
    resultado = 0;
    for (var i = 1; i <= 27; i++) {
        
        if( $('#'+idFormulario).find('input[type="radio"][name='+name+i+']:checked').val() != null )
            resultado += parseInt($('#'+idFormulario).find('input[type="radio"][name='+name+i+']:checked').val());
    };

    //$("."+idResultado).val(resultado);
    $("#resultadoWhoqol").text(resultado);

}

//se usa
function sumarIndiceWhoqolA3(name, idResultado, idMensaje, idFormulario){
    //console.log("idFormulario "+idFormulario+ " name "+name+" ");
    resultado = 0;
    for (var i = 1; i <= 27; i++) {
        
        if( $('#'+idFormulario).find('input[type="radio"][name='+name+i+']:checked').val() != null )
            resultado += parseInt($('#'+idFormulario).find('input[type="radio"][name='+name+i+']:checked').val());
    };

    //$("."+idResultado).val(resultado);
    $("#resultadoWhoqolA3").text(resultado);

}

function sumarEvaluacionCognitiva(name, idResultado, idMensaje, idFormulario){
   var $numero1 = $('#primer_numero').val();
   var $numero2 = $('#segundo_numero').val();
   var $numero3 = $('#tercer_numero').val();
   var $numero4 = $('#cuarto_numero').val();
   var $numero5 = $('#quinto_numero').val();

   var bien="-bien";


   if($numero1!=9)$numero1=0;else $numero1=1;
   if($numero2!=7)$numero2=0;else $numero2=1;
   if($numero3!=5)$numero3=0;else $numero3=1;
   if($numero4!=3)$numero4=0;else $numero4=1;
   if($numero5!=1)$numero5=0;else $numero5=1;

    resultado = 0;
    for (var i = 1; i <= 14; i++) {
        if( $('#'+idFormulario).find('input[type="radio"][id='+name+i+bien+']:checked').attr('valor') != null ){
            resultado += parseInt($('#'+idFormulario).find('input[type="radio"][id='+name+i+bien+']:checked').attr('valor'));
        }
        
    };
    resultado=resultado+$numero1+$numero2+$numero3+$numero4+$numero5;
    $("."+idResultado).val(resultado);

}

function sumarEvaluacionCognitivaFinal(name, idResultado, idMensaje, idFormulario){
   var $numero1 = $('#primer_numeroF').val();
   var $numero2 = $('#segundo_numeroF').val();
   var $numero3 = $('#tercer_numeroF').val();
   var $numero4 = $('#cuarto_numeroF').val();
   var $numero5 = $('#quinto_numeroF').val();
   var bien="-bien";

   if($numero1!=9)$numero1=0;else $numero1=1;
   if($numero2!=7)$numero2=0;else $numero2=1;
   if($numero3!=5)$numero3=0;else $numero3=1;
   if($numero4!=3)$numero4=0;else $numero4=1;
   if($numero5!=1)$numero5=0;else $numero5=1;

    resultado = 0;
    for (var i = 1; i <= 14; i++) {
        if( $('#'+idFormulario).find('input[type="radio"][id='+name+i+bien+']:checked').attr('valor') != null ){
            resultado += parseInt($('#'+idFormulario).find('input[type="radio"][id='+name+i+bien+']:checked').attr('valor'));
        }
    };
    resultado=resultado+$numero1+$numero2+$numero3+$numero4+$numero5;
    $("."+idResultado).val(resultado);

}

function sumarIndiceTinetiEquilibrio(name, idResultado, idMensaje, idFormulario){
   
    resultado = 0;
    for (var i = 1; i <= 26; i++) {
        if( $('#'+idFormulario).find('input[type="radio"][id='+name+i+']:checked').attr('valor') != null ){
            resultado += parseInt($('#'+idFormulario).find('input[type="radio"][id='+name+i+']:checked').attr('valor'));
        }    
    };
    
    $("."+idResultado).val(resultado);

}

function sumarIndiceTinetiMarcha(name, idResultado, idMensaje, idFormulario){
   
    resultado = 0;
    for (var i = 27; i <= 48; i++) {
        if( $('#'+idFormulario).find('input[type="radio"][id='+name+i+']:checked').attr('valor') != null ){
            resultado += parseInt($('#'+idFormulario).find('input[type="radio"][id='+name+i+']:checked').attr('valor'));
        }    
    };
    
    $("."+idResultado).val(resultado);

}

function sumarIndiceTineti(name, idResultado, idMensaje, idFormulario){
   
    resultado = 0;
    for (var i = 1; i <= 48; i++) {
        if( $('#'+idFormulario).find('input[type="radio"][id='+name+i+']:checked').attr('valor') != null ){
            resultado += parseInt($('#'+idFormulario).find('input[type="radio"][id='+name+i+']:checked').attr('valor'));
        }    
    };
    
    $("."+idResultado).val(resultado);

}

function sumarIndiceICIQ(name, idResultado, idMensaje, idFormulario){
   
    resultado = 0;
    for (var i = 1; i <= 21; i++) {
        if( $('#'+idFormulario).find('input[type="radio"][id='+name+i+']:checked').attr('valor') != null ){
            resultado += parseInt($('#'+idFormulario).find('input[type="radio"][id='+name+i+']:checked').attr('valor'));
        }    
    };
    
    $("."+idResultado).val(resultado);

}

function esSeleccionado(idElem){
    //Funcion que determina si el elemento esta seleccionado o no,
    //esta devuelve una "X" si está seleciconado y " " si no.
    if( $('#'+idElem).is(':checked') )
        return "X";
    else
        return "  ";
}

function esSeleccionadoGeriatrico(idElem){
    si="si";
    no="no";
    nulo="null";
    
    if( $('#'+idElem+si).is(':checked') )
        return "[Si]";
    else
        if( $('#'+idElem+no).is(':checked') )
            return "[No]";
        else
            if( $('#'+idElem+nulo).is(':checked') )
                return "[Ninguno]";
            else
                return "[Ninguno]";

}

function esSeleccionadoCognitiva(idElem){
    bien="bien";
    mal="mal";
    ns="ns";
    nr="nr";
    
    if( $('#'+idElem+bien).is(':checked') ) return "[Bien]";
    if( $('#'+idElem+mal).is(':checked') ) return "[Mal]";
    if( $('#'+idElem+ns).is(':checked') ) return "[N.S]";
    if( $('#'+idElem+nr).is(':checked') ) return "[N.R]";
    return "[  ]";
} 

function esSeleccionadoMotriz1(idElem){
    if( $('#'+idElem).is(':checked') )
        return "[Si]";
    else
        return "[No]";
} 

function esSeleccionadoMotriz2(idElem){
    if( $('#'+idElem).is(':checked') )
        return "[ X ]";
    else
        return "[  ]";
} 


function esSeleccionadoGDS(idElem){

    if( $('#'+idElem).is(':checked') )
        return "X";
    else
        return "  ";
} 

function esSeleccionadoMmse(idElem){
    si="si";
    no="no";
    nulo="null";
    
    if( $('#'+idElem).is(':checked') )
        return "[Bueno]";
    else{
        
            return "[Malo]";
    }
       
}

function esSeleccionadoBarthel(idElem){
    //se chequea si el elemento está seleccionado, de ser asi
    //se retorna su valor "val()"
    if( $('#'+idElem).is(':checked') )
        return "["+$('#'+idElem).val()+"]";
    else
        return "[  ]";
}

function esSeleccionadoLawton(idElem){
    //se chequea si el elemento está seleccionado, de ser asi
    //se retorna su valor "val()"
    if( $('#'+idElem).is(':checked') )
        return "X";
    else
        return "  ";
}


function imprimirPDF(idFormulario){
    //lo primero es evaluar a qué tipo d enecuesta eprtenece cada id del formulario
    //cada idFormulario tiene el nombre de la encuesta.
    nombreArchivo="";
    tipo="";
    if(idFormulario.indexOf('GetUp') != -1){
        //se trabajara con timedGetUp
        nombreArchivo="Timed Get Up and Go Test";
        if(idFormulario.indexOf('Inicial') != -1){
            //Se trabajará con el timedGetUp Inicial
            tipo="Inicial";
        }else{
            //Se trabajará con el timedGetUp final
            tipo="Final";
        }

        var columns = ["Timed Get Up and Go (Medidas de movilidad en las personas que son\n capaces de caminar por su cuenta (dispositivo de asistencia permitida) "+tipo+" ", ""];
        var rows = [
        ["RUT:",  numberWithCommas($("#paciente-rut").val()) + "-" + $("#paciente-dv").val()],
            ["Nombre:",  $("#paciente-nombre").val() + " " + $("#paciente-apellido").val()],
            ["Fecha "+$('#timedGetUp'+tipo+'-fecha-encuesta').val()+"",""],
            ["La persona puede usar su calzado habitual y puede utilizar cualquier dispositivo\nde ayuda que normalmente usa.\n1.El paciente debe sentarse en la silla con la espalda apoyada y los brazos\n descansandosobre los apoyabrazos.\n2.Pídale a la persona que se levante de una silla estándar y camine una distancia\n de 3 metros.\n3.Haga que la persona se dé media vuelta, camine de vuelta a la silla y se siente\n de nuevo.",""],
            ["Repetición","Respuesta"],
            ["1","Segundos\n["+$('#timedGetUp'+tipo+'-p-1').val()+"]"],
            ["2","Segundos\n["+$('#timedGetUp'+tipo+'-p-2').val()+"]"],
            ["3","Segundos\n["+$('#timedGetUp'+tipo+'-p-3').val()+"]"],
            ["Promedio Total (Segundos)","["+$('#timedGetUp'+tipo+'-tiempo-encuesta').val()+"]"],
            ["El cronometraje comienza cuando la persona comienza a levantarse de la silla\ny termina cuando regresa a la silla y se sienta.\nLa persona debe dar un intento de práctica y luego repite 3 intentos. Se promedian\nlos tres ensayos reales.",""],
            ["Resultados predictivos",""],
            ["Valoración en segundos",""],
            ["<10","Movilidad independiente"],
            ["<20","Mayormente independiente"],
            ["20-29","Movilidad variable"],
            [">20","Movilidad reducida"],
            ["Source: Podsiadlo, D., Richardson, S. The timed ‘Up and Go’ Test: a Test of\n Basic Functional Mobility for Frail Elderly Persons.\nJournal of American Geriatric Society. 1991; 39:142-148",""],
        ];
    }
    if(idFormulario.indexOf('Geriatricos') != -1){
        //se trabajara con Geriatricos
        nombreArchivo="Síndromes Geriátricos";
        if(idFormulario.indexOf('Inicial') != -1){
            //Se trabajará con el Geriatricos Inicial
            tipo="Inicial";
        }else{
            //Se trabajará con el Geriatricos final
            tipo="Final";
        }

        var columns = ["Síndromes Geriátricos  "+tipo+" ","",""];
        
        var rows = [
            ["RUT:",  numberWithCommas($("#paciente-rut").val()) + "-" + $("#paciente-dv").val()],
            ["Nombre:",  $("#paciente-nombre").val() + " " + $("#paciente-apellido").val()],
            ["Fecha "+$('#sindromesGeriatricos'+tipo+'-fecha-encuesta').val()+"","\t\t\t\t\t\t\t\t\t\t\t\t\t","Caidas Últimos 6 meses ["+$('#sindromesGeriatricos'+tipo+'-caidas-ultimos-meses').val()+"]"],
            ["","Si/No","Comentarios"],
            ["Caídas y Trastorno Marcha",""+esSeleccionadoGeriatrico('sindromesGeriatricos'+tipo+'-p-1-')+"",""+$('#sindromesGeriatricos'+tipo+'-c-1').val()+""],
            ["Polifarmacia",""+esSeleccionadoGeriatrico('sindromesGeriatricos'+tipo+'-p-2-')+"",""+$('#sindromesGeriatricos'+tipo+'-c-2').val()+""],
            ["Incontinencia Urinaria",""+esSeleccionadoGeriatrico('sindromesGeriatricos'+tipo+'-p-3-')+"",""+$('#sindromesGeriatricos'+tipo+'-c-3').val()+""],
            ["Incontinencia Fecal",""+esSeleccionadoGeriatrico('sindromesGeriatricos'+tipo+'-p-4-')+"",""+$('#sindromesGeriatricos'+tipo+'-c-4').val()+""],
            ["Deterioro Cognitivo",""+esSeleccionadoGeriatrico('sindromesGeriatricos'+tipo+'-p-5-')+"",""+$('#sindromesGeriatricos'+tipo+'-c-5').val()+""],
            ["Delirium",""+esSeleccionadoGeriatrico('sindromesGeriatricos'+tipo+'-p-6-')+"",""+$('#sindromesGeriatricos'+tipo+'-c-6').val()+""],
            ["Trastorno del Ánimo",""+esSeleccionadoGeriatrico('sindromesGeriatricos'+tipo+'-p-7-')+"",""+$('#sindromesGeriatricos'+tipo+'-c-7').val()+""],
            ["Trastorno Sueño",""+esSeleccionadoGeriatrico('sindromesGeriatricos'+tipo+'-p-8-')+"",""+$('#sindromesGeriatricos'+tipo+'-c-8').val()+""],
            ["Malnutrición",""+esSeleccionadoGeriatrico('sindromesGeriatricos'+tipo+'-p-9-')+"",""+$('#sindromesGeriatricos'+tipo+'-c-9').val()+""],
            ["Deficit Sensorial",""+esSeleccionadoGeriatrico('sindromesGeriatricos'+tipo+'-p-10-')+"",""+$('#sindromesGeriatricos'+tipo+'-c-10').val()+""],
            ["Inmovilidad",""+esSeleccionadoGeriatrico('sindromesGeriatricos'+tipo+'-p-11-')+"",""+$('#sindromesGeriatricos'+tipo+'-c-11').val()+""],
            ["Úlceras por Presión",""+esSeleccionadoGeriatrico('sindromesGeriatricos'+tipo+'-p-12-')+"",""+$('#sindromesGeriatricos'+tipo+'-c-12').val()+""],
        ];
    }
    if(idFormulario.indexOf('Barthel') != -1){
        //se trabajara con Barthel
        nombreArchivo="Indice de Barthel";
        if(idFormulario.indexOf('Inicial') != -1){
            //Se trabajará con el Barthel Inicial
            tipo="Inicial";
        }else{
            //Se trabajará con el Barthel final
            tipo="Final";
        }

        var columns = ["Índice de Barthel "+tipo+" ","",""];
        
        var rows = [
        ["RUT:",  numberWithCommas($("#paciente-rut").val()) + "-" + $("#paciente-dv").val()],
            ["Nombre:",  $("#paciente-nombre").val() + " " + $("#paciente-apellido").val()],
            ["Fecha "+$('#indiceBarthel'+tipo+'-fecha-encuesta').val()+"","",""],
            ["Parámetro","Situación del paciente","Puntuación"],
            ["Comer","- Totalmente independiente",""+esSeleccionadoBarthel('indiceBarthel'+tipo+'-p-1-10')+""],
            ["","- Necesita ayuda para cortar carne, el pan, etc.",""+esSeleccionadoBarthel('indiceBarthel'+tipo+'-p-1-5')+""],
            ["","- Dependiente",""+esSeleccionadoBarthel('indiceBarthel'+tipo+'-p-1-0')+""],
            ["Lavarse","- Independiente: entra y sale solo del baño",""+esSeleccionadoBarthel('indiceBarthel'+tipo+'-p-2-10')+""],
            ["","- Dependiente",""+esSeleccionadoBarthel('indiceBarthel'+tipo+'-p-2-5')+""],
            ["Vestirse","- Independiente: capaz de ponerse y de quitarse la ropa,\nabotonarse, atarse los zapatos",""+esSeleccionadoBarthel('indiceBarthel'+tipo+'-p-3-10')+""],
            ["","- Necesita ayuda",""+esSeleccionadoBarthel('indiceBarthel'+tipo+'-p-3-5')+""],
            ["","- Dependiente",""+esSeleccionadoBarthel('indiceBarthel'+tipo+'-p-3-0')+""],
            ["Arreglarse","- Independiente para lavarse la cara, las manos,\npeinarse, afeitarse, maquillarse, etc.",""+esSeleccionadoBarthel('indiceBarthel'+tipo+'-p-4-10')+""],
            ["","- Dependiente",""+esSeleccionadoBarthel('indiceBarthel'+tipo+'-p-4-5')+""],
            ["Deposiciones\n(valórese la semana previa)","- Continencia normal",""+esSeleccionadoBarthel('indiceBarthel'+tipo+'-p-5-10')+""],
            ["","- Ocasionalmente algún episodio de incontinencia, o\nnecesita ayuda para administrarse supositorios o lavativas",""+esSeleccionadoBarthel('indiceBarthel'+tipo+'-p-5-5')+""],
            ["","- Incontinencia",""+esSeleccionadoBarthel('indiceBarthel'+tipo+'-p-5-0')+""],
            ["Micción\n(valórese la semana previa)","- Continencia normal, o es capaz de cuidarse de la\nsonda si tiene una puesta",""+esSeleccionadoBarthel('indiceBarthel'+tipo+'-p-6-10')+""],
            ["","- Un episodio diario como máximo de incontinencia, o\nnecesita ayuda para cuidar de la sonda",""+esSeleccionadoBarthel('indiceBarthel'+tipo+'-p-6-5')+""],
            ["","- Incontinencia",""+esSeleccionadoBarthel('indiceBarthel'+tipo+'-p-6-0')+""],
            ["Usar el retrete","- Independiente para ir al cuarto de aseo, quitarse y\nponerse la ropa…",""+esSeleccionadoBarthel('indiceBarthel'+tipo+'-p-7-10')+""],
            ["","- Necesita ayuda para ir al retrete, pero se limpia solo",""+esSeleccionadoBarthel('indiceBarthel'+tipo+'-p-7-5')+""],
            ["","- Dependiente",""+esSeleccionadoBarthel('indiceBarthel'+tipo+'-p-7-0')+""],
            ["Trasladarse","- Independiente para ir del sillón a la cama",""+esSeleccionadoBarthel('indiceBarthel'+tipo+'-p-8-15')+""],
            ["","- Mínima ayuda física o supervisión para hacerlo",""+esSeleccionadoBarthel('indiceBarthel'+tipo+'-p-8-10')+""],
            ["","- Necesita gran ayuda, pero es capaz de mantenerse\nsentado solo",""+esSeleccionadoBarthel('indiceBarthel'+tipo+'-p-8-5')+""],
            ["","- Dependiente",""+esSeleccionadoBarthel('indiceBarthel'+tipo+'-p-8-0')+""],
            ["Deambular","- Independiente, camina solo 50 metros",""+esSeleccionadoBarthel('indiceBarthel'+tipo+'-p-9-15')+""],
            ["","- Necesita ayuda física o supervisión para caminar 50\nmetros",""+esSeleccionadoBarthel('indiceBarthel'+tipo+'-p-9-10')+""],
            ["","- Independiente en silla de ruedas sin ayuda",""+esSeleccionadoBarthel('indiceBarthel'+tipo+'-p-9-5')+""],
            ["","- Dependiente",""+esSeleccionadoBarthel('indiceBarthel'+tipo+'-p-9-0')+""],
            ["Escalones","- Independiente para bajar y subir escaleras",""+esSeleccionadoBarthel('indiceBarthel'+tipo+'-p-10-10')+""],
            ["","- Necesita ayuda física o supervisión para hacerlo",""+esSeleccionadoBarthel('indiceBarthel'+tipo+'-p-10-5')+""],
            ["","- Dependiente",""+esSeleccionadoBarthel('indiceBarthel'+tipo+'-p-10-0')+""],
            ["Total:","",""+$('.indiceBarthel'+tipo+'-total').val()+""],
            ["Máxima puntuación: 100 puntos\n(90 si va en silla de ruedas)","",""],
            ["Resultado","Grado de dependencia",""],
            ["<20","Total",""],
            ["20-35","Grave",""],
            ["40-55","Moderado",""],
            [">=60","Leve",""],
            ["100","Independiente",""],
        ];
    }
    if(idFormulario.indexOf('Lawton') != -1){
        //se trabajara con Lawton
        nombreArchivo="Indice LawtonBrody";
        if(idFormulario.indexOf('Inicial') != -1){
            //Se trabajará con el Lawton Inicial
            tipo="Inicial";
        }else{
            //Se trabajará con el Lawton final
            tipo="Final";
        }

        var columns = ["Índice de LawtonBrody "+tipo+"\n(Actividades instrumentales de la vida diaria)", ""];
        var rows = [
        ["RUT:",  numberWithCommas($("#paciente-rut").val()) + "-" + $("#paciente-dv").val()],
            ["Nombre:",  $("#paciente-nombre").val() + " " + $("#paciente-apellido").val()],
            ["Fecha "+$('#LawtonBrody'+tipo+'-fecha-encuesta').val()+" ",""],
            ["Teléfono",""],
            ["Utilizar el telefono por iniciativa propia, buscar y marcar los numeros", "["+esSeleccionado('indiceLawtonBrody'+tipo+'-p-1')+"] (1)"],
            ["Sabe marcar números desconocidos", "["+esSeleccionado('indiceLawtonBrody'+tipo+'-p-2')+"] (1)"],
            ["Contesta al teléfono, pero no sabe marcar", "["+esSeleccionado('indiceLawtonBrody'+tipo+'-p-3')+"] (1)"],
            ["No utiliza el teléfono en absoluto", "["+esSeleccionado('indiceLawtonBrody'+tipo+'-p-4')+"] (0)"],
            ["Compras", ""],
            ["Realiza todas las compras necesarias de manera independiente", "["+esSeleccionado('indiceLawtonBrody'+tipo+'-p-5')+"] (1)"],
            ["Sólo sabe hacer pequeñas compras","["+esSeleccionado('indiceLawtonBrody'+tipo+'-p-6')+"] (0)"],
            ["Ha de ir acompañado para cualquier compra","["+esSeleccionado('indiceLawtonBrody'+tipo+'-p-7')+"] (0)"],
            ["Completamente incapaz de hacer la compra","["+esSeleccionado('indiceLawtonBrody'+tipo+'-p-8')+"] (0)"],
            ["Preparación de la comida",""],
            ["Organiza, Prepara y sirve cualquier comida por si solo/a","["+esSeleccionado('indiceLawtonBrody'+tipo+'-p-9')+"] (1)"],
            ["Prepara la comida sólo si le proporcionan los ingredientes","["+esSeleccionado('indiceLawtonBrody'+tipo+'-p-10')+"] (0)"],
            ["Prepara, calienta y sirve la comida, pero no sigue una dieta adecuada","["+esSeleccionado('indiceLawtonBrody'+tipo+'-p-11')+"] (0)"],
            ["Necesita que le preparen y le sirvan la comida","["+esSeleccionado('indiceLawtonBrody'+tipo+'-p-12')+"] (0)"],
            ["Tareas Domésticas",""],
            ["Realiza las tareas de la casa por si sola, sólo ayuda ocacional","["+esSeleccionado('indiceLawtonBrody'+tipo+'-p-13')+"] (1)"],
            ["Realiza tareas ligeras (fregar platos, camas...)","["+esSeleccionado('indiceLawtonBrody'+tipo+'-p-14')+"] (1)"],
            ["Realiza tareas ligeras, pero no mantiene un nivel de limpieza adecuado","["+esSeleccionado('indiceLawtonBrody'+tipo+'-p-15')+"] (1)"],
            ["Necesita ayuda, pero realiza todas las tareas domésticas","["+esSeleccionado('indiceLawtonBrody'+tipo+'-p-16')+"] (1)"],
            ["No participa ni hace ninguna tarea","["+esSeleccionado('indiceLawtonBrody'+tipo+'-p-17')+"] (0)"],
            ["Lavar la ropa",""],
            ["Lava sola toda la ropa","["+esSeleccionado('indiceLawtonBrody'+tipo+'-p-18')+"] (1)"],
            ["Lava sólo prendas pequeñas (calcetines, medias, etc.)","["+esSeleccionado('indiceLawtonBrody'+tipo+'-p-19')+"] (1)"],
            ["La ropa la tiene que lavar otra persona","["+esSeleccionado('indiceLawtonBrody'+tipo+'-p-20')+"] (0)"],
            ["Transporte",""],
            ["Viaja por si solo/a, utiliza transporte publico/conduce coche","["+esSeleccionado('indiceLawtonBrody'+tipo+'-p-21')+"] (1)"],
            ["Puede ir solo en taxi, no utiliza otro transporte publico","["+esSeleccionado('indiceLawtonBrody'+tipo+'-p-22')+"] (1)"],
            ["Solo viaja en transporte público si va acompañado","["+esSeleccionado('indiceLawtonBrody'+tipo+'-p-23')+"] (1)"],
            ["Viajes limitados en taxi o coche con ayuda de otros (adaptado)","["+esSeleccionado('indiceLawtonBrody'+tipo+'-p-24')+"] (0)"],
            ["No viaja en absoluto","["+esSeleccionado('indiceLawtonBrody'+tipo+'-p-25')+"] (0)"],
            ["Responsabilidad respecto a la medicación",""],
            ["Es capaz de tomar la medicación a la hora y en la dosis correcta, solo/a","["+esSeleccionado('indiceLawtonBrody'+tipo+'-p-26')+"] (1)"],
            ["Toma la medicación sólo si se la preparan previamente","["+esSeleccionado('indiceLawtonBrody'+tipo+'-p-27')+"] (0)"],
            ["No es capaz de tomar la medicación solo/a","["+esSeleccionado('indiceLawtonBrody'+tipo+'-p-28')+"] (0)"],
            ["Capacidad de utilizar el dinero",""],
            ["Se responsabiliza de asuntos económicos solo/a","["+esSeleccionado('indiceLawtonBrody'+tipo+'-p-29')+"] (1)"],
            ["Se encarga de compras diarias, pero necesita ayuda para ir al banco","["+esSeleccionado('indiceLawtonBrody'+tipo+'-p-30')+"] (1)"],
            ["Incapaz de utilizar el dinero","["+esSeleccionado('indiceLawtonBrody'+tipo+'-p-31')+"] (0)"],
            ["Total:",""+$('.indiceLawtonBrody'+tipo+'-total').val()+""],
            ["Fuente bibliográfica de la que se ha obtenido esta versión: Lawton MP, Brody EM. Assessment of\nolder people: self-maintaining and instrumental activities of daily living. Gerontologist 1969;\n9:179-86.",""],
            ["Comentarios",""],
            ["Actividades instrumentales propias del medio extrahospitalario y necesarias para vivir solo. Su\nnormalidad suele ser indicativa de integridad de las actividades básicas para el autocuidado y del\nestado mental (es útil en programas de screening de ancianos de riesgo en la comunidad). Hay tres\nactividades que en la cultura occidental son más propias de mujeres (comida, tareas del hogar, lavar\nropa); por ello, los autores de la escala admiten que en los hombres estas actividades puedan\nsuprimirse de la evaluación, de esta manera existirá una puntuación total para hombres y otra para\nmujeres (se considera anormal < 5 en hombre y < 8 en mujer). El deterioro de las actividades\ninstrumentales, medido con el índice de Lawton, es predictivo de deterioro de las actividades básicas,\ndurante un ingreso hospitalario (Sager et al. J Am Geriatr Soc 1996; 44: 251-7); por ello, algunos\nautores han sugerido que este índice puede ser un indicador de fragilidad (Nourhashémi F, et al.\nJ Gerontol Med Sci 2001; 56A: M448-M53).",""],
        ];


    }
    if(idFormulario.indexOf('Tineti') != -1){
        //se trabajara con Tineti
        nombreArchivo="Tinetti";
        if(idFormulario.indexOf('Inicial') != -1){
            //Se trabajará con el Tineti Inicial
            tipo="Inicial";
        }else{
            //Se trabajará con el Tineti final
            tipo="Final";
        }

        var columns = ["Evaluación de la marcha y el equilibrio "+tipo+"", ""];
        var rows = [
        ["RUT:",  numberWithCommas($("#paciente-rut").val()) + "-" + $("#paciente-dv").val()],
            ["Nombre:",  $("#paciente-nombre").val() + " " + $("#paciente-apellido").val()],
            ["Tinetti Primera parte: equilibrio",""],
            ["Fecha "+$('#Tineti'+tipo+'-fecha-encuesta').val()+" ",""],
            ["Instrucciones:",""],
            ["Equilibrio: el paciente está situado en una silla dura sin apoyabrazos. Se realizan las siguientes\nmaniobras:", ""],
            ["1. Equilibrio sentado", ""],
            ["Se inclina o se desliza en la silla", "["+esSeleccionado('indicetinetti'+tipo+'-p-1')+"] (0)"],
            ["Se mantiene seguro", "["+esSeleccionado('indicetinetti'+tipo+'-p-2')+"] (1)"],
            ["2. Levantarse", ""],
            ["Imposible sin ayuda", "["+esSeleccionado('indicetinetti'+tipo+'-p-3')+"] (0)"],
            ["Capaz, pero necesita más de un intento","["+esSeleccionado('indicetinetti'+tipo+'-p-4')+"] (1)"],
            ["Capaz, sin usar los brazos","["+esSeleccionado('indicetinetti'+tipo+'-p-5')+"] (2)"],
            ["3. Intentos para levantarse",""],
            ["Incapaz sin ayuda","["+esSeleccionado('indicetinetti'+tipo+'-p-6')+"] (0)"],
            ["Capaz, pero necesita más de un intento","["+esSeleccionado('indicetinetti'+tipo+'-p-7')+"] (1)"],
            ["Capaz de levantarse con sólo un intento","["+esSeleccionado('indicetinetti'+tipo+'-p-8')+"] (2)"],
            ["4. Equilibrio en bipedestación inmediata (los primeros 5 segundos)",""],
            ["Inestable (se tambalea, mueve los pies), marcado balanceo del tronco","["+esSeleccionado('indicetinetti'+tipo+'-p-9')+"] (0)"],
            ["Estable pero usa el andador, bastón o se agarra a otro objeto para mantenerse","["+esSeleccionado('indicetinetti'+tipo+'-p-10')+"] (1)"],
            ["Estable sin andador, bastón u otros soportes","["+esSeleccionado('indicetinetti'+tipo+'-p-11')+"] (2)"],
            ["5. Equilibrio en bipedestación",""],
            ["Inestable","["+esSeleccionado('indicetinetti'+tipo+'-p-12')+"] (0)"],
            ["Estable, pero con apoyo amplio (talones separados más de 10 cm),o bien usa bastón u otro soporte","["+esSeleccionado('indicetinetti'+tipo+'-p-13')+"] (1)"],
            ["Apoyo estrecho sin soporte","["+esSeleccionado('indicetinetti'+tipo+'-p-14')+"] (2)"],
            ["6. Empujar (bipedestación con el tronco erecto y los pies juntos). El examinador empuja\nsuavemente en el esternón del paciente con la palma de la mano, 3 veces",""],
            ["Empieza a caerse","["+esSeleccionado('indicetinetti-p'+tipo+'-15')+"] (0)"],
            ["Se tambalea, se agarra, pero se mantiene","["+esSeleccionado('indicetinetti'+tipo+'-p-16')+"] (1)"],
            ["Estable","["+esSeleccionado('indicetinetti'+tipo+'-p-17')+"] (2)"],
            ["7. Ojos cerrados (en la posición de 6)",""],
            ["Inestable","["+esSeleccionado('indicetinetti'+tipo+'-p-18')+"] (0)"],
            ["Estable","["+esSeleccionado('indicetinetti'+tipo+'-p-19')+"] (1)"],
            ["8. Vuelta de 360 grados",""],
            ["Pasos discontinuos","["+esSeleccionado('indicetinetti'+tipo+'-p-20')+"] (0)"],
            ["Continuos","["+esSeleccionado('indicetinetti'+tipo+'-p-21')+"] (1)"],
            ["Inestable (se tambalea, se agarra)","["+esSeleccionado('indicetinetti'+tipo+'-p-22')+"] (0)"],
            ["Estable","["+esSeleccionado('indicetinetti'+tipo+'-p-23')+"] (1)"],
            ["9. Sentarse",""],
            ["Inseguro, calcula mal la distancia, cae en la silla","["+esSeleccionado('indicetinetti'+tipo+'-p-24')+"] (0)"],
            ["Usa los brazos o el movimiento es brusco","["+esSeleccionado('indicetinetti'+tipo+'-p-25')+"] (1)"],
            ["Seguro, movimiento suave","["+esSeleccionado('indicetinetti'+tipo+'-p-26')+"] (2)"],
            ["Total Equilibrio:",""+$('.indiceTineti-total-equilibrio').val()+""],
            ["Tinetti Segunda parte: Marcha",""+esSeleccionado('indicetinetti-p-3')+""],
            ["El paciente permanece de pie con el examinador, camina por el pasillo o por la habitación (unos 8\nmetros) a «paso normal», luego regresa a «paso rápido pero seguro».",""],
            ["10. Iniciación de la marcha (inmediatamente después de decir que ande)",""],
            ["Algunas vacilaciones o múltiples intentos para empezar","["+esSeleccionado('indicetinetti'+tipo+'-p-27')+"] (0)"],
            ["No vacila","["+esSeleccionado('indicetinetti'+tipo+'-p-28')+"] (1)"],
            ["11. Longitud y altura de paso",""],
            ["a) Movimiento del pie derecho",""],
            ["No sobrepasa el pie izquierdo con el paso","["+esSeleccionado('indicetinetti'+tipo+'-p-29')+"] (0)"],
            ["Sobrepasa al pie izquierdo","["+esSeleccionado('indicetinetti'+tipo+'-p-30')+"] (1)"],
            ["El pie derecho no se separa completamente del suelo con el paso","["+esSeleccionado('indicetinetti'+tipo+'-p-31')+"] (0)"],
            ["El pie derecho se separa completamente del suelo con el paso","["+esSeleccionado('indicetinetti'+tipo+'-p-32')+"] (1)"],
            ["b) Movimiento del pie izquierdo",""],
            ["No sobrepasa el pie derecho con el paso","["+esSeleccionado('indicetinetti'+tipo+'-p-33')+"] (0)"],
            ["Sobrepasa al pie derecho","["+esSeleccionado('indicetinetti'+tipo+'-p-34')+"] (1)"],
            ["El pie izquierdo no se separa completamente del suelo con el paso","["+esSeleccionado('indicetinetti'+tipo+'-p-35')+"] (0)"],
            ["El pie izquierdo se separa completamente del suelo con el paso","["+esSeleccionado('indicetinetti'+tipo+'-p-36')+"] (1)"],
            ["12. Simetría del paso",""],
            ["La longitud de los pasos con los pies izquierdo y derecho no es igual","["+esSeleccionado('indicetinetti'+tipo+'-p-37')+"] (0)"],
            ["La longitud parece igual","["+esSeleccionado('indicetinetti'+tipo+'-p-38')+"] (1)"],
            ["13. Fluidez del paso",""],
            ["Paradas entre los pasos","["+esSeleccionado('indicetinetti'+tipo+'-p-39')+"] (0)"],
            ["Los pasos parecen continuos","["+esSeleccionado('indicetinetti'+tipo+'-p-40')+"] (1)"],
            ["14. Trayectoria (observar el trazado que realiza uno de los pies durante unos 3 metros)",""],
            ["Desviación grave de la trayectoria","["+esSeleccionado('indicetinetti'+tipo+'-p-41')+"] (0)"],
            ["Leve/moderada desviación o usa ayudas para mantener la trayectoria","["+esSeleccionado('indicetinetti'+tipo+'-p-42')+"] (1)"],
            ["Sin desviación o ayudas","["+esSeleccionado('indicetinetti'+tipo+'-p-43')+"] (2)"],
            ["15. Tronco",""],
            ["Balanceo marcado o usa ayudas","["+esSeleccionado('indicetinetti'+tipo+'-p-44')+"] (0)"],
            ["No balancea pero flexiona las rodillas o la espalda o separa los brazos al caminar","["+esSeleccionado('indicetinetti'+tipo+'-p-45')+"] (1)"],
            ["No se balancea, no reflexiona, ni otras ayudas","["+esSeleccionado('indicetinetti'+tipo+'-p-46')+"] (2)"],
            ["16. Postura al caminar",""],
            ["Talones separados","["+esSeleccionado('indicetinetti'+tipo+'-p-47')+"] (0)"],
            ["Talones casi juntos al caminar","["+esSeleccionado('indicetinetti'+tipo+'-p-48')+"] (1)"],
            ["Total Marcha:",""+$('.indiceTineti'+tipo+'-total-marcha').val()+""],
            ["Total Tinetti",""+$('.indiceTineti'+tipo+'-total').val()+""],
            ["Fuente bibliográfica de la que se ha obtenido esta versión:",""],
            ["Rubenstein LZ. Instrumentos de evaluación. En: Abrams WB, Berkow R. El Manual Merck de\nGeriatría (Ed Esp). Barcelona: Ed Doyma; 1992. p. 1251-63 (en dicho libro se hace constar que esta\nversión es una modificación adaptada de Tinetti et al, en: J Am Geriatr Soc 1986; 34: 119). También es\nla misma versión recomendada por el Grupo de Trabajo de Caídas de la SEGG (Navarro C, Lázaro M,\nCues- ta F, Vilorria A, Roiz H. Métodos clínicos de evaluación de los trastornos del equilibrio y la marcha.\nEn: Grupo de trabajo de caídas de la Sociedad Española de Geriatría y Gerontología. 2.ª ed. Eds.\nFundación Mapfre Medicina; 2001. p. 101-22).",""],
        ];

    }
    if(idFormulario.indexOf('Charlson') != -1){
        //se trabajara con Charlson
        nombreArchivo="Charlson";
        if(idFormulario.indexOf('Inicial') != -1){
            //Se trabajará con el Charlson Inicial
            tipo="Inicial";
        }else{
            //Se trabajará con el Charlson final
            tipo="Final";
        }

        var columns = ["Indice de comorbilidad de Charlson "+tipo+"", ""];
        var rows = [
        ["RUT:",  numberWithCommas($("#paciente-rut").val()) + "-" + $("#paciente-dv").val()],
            ["Nombre:",  $("#paciente-nombre").val() + " " + $("#paciente-apellido").val()],
            ["Fecha "+$('#Charlson'+tipo+'-fecha-encuesta').val()+"",""],
            ["Infarto de miocardio: debe existir evidencia en la historia clínica que el paciente fue hospitalizado por\nello, o bien evidencias de que existieron cambios e enzimas y/o en ecg","["+esSeleccionado('charlson'+tipo+'-p-1')+"] (1)"],
            ["Insuficiencia cardiaca: debe existir historia de disnea de esfuerzos y/o signos de insuficiencia cardiaca\nen la exploración física que respondieron favorablemente al tratamiento con digital, diuréticos o\nvasodilatadores. los pacientes que estén tomando estos tratamientos pero no podamos constatar que\hubo mejoría clínica de los síntomas y/o signos, no se incluirán como tales","["+esSeleccionado('charlson'+tipo+'-p-2')+"] (1)"],
            ["Enfermedad arterial periférica: incluye claudicación intermitente, interveidos de by-pass arterial\n periférico, isquemia arterial aguda y aquellos con aneurisma de la aorta (torácica o abdominal) de > 6 cm\nde diámetro","["+esSeleccionado('charlson'+tipo+'-p-3')+"] (1)"],
            ["Enfermedad cerebrovascular: paciente con AVC con mínimas secuelas o AVC transitorio","["+esSeleccionado('charlson'+tipo+'-p-4')+"] (1)"],
            ["Demencia: pacientes con evidencia en la historia clínica de deterioro cognitivo crónico","["+esSeleccionado('charlson'+tipo+'-p-5')+"] (1)"],
            ["Enfermedad respiratoria crónica: debe existir evidencia en la historia clínica, en la exploración física\ny en exploración complementaria de cualquier enfermedad respiratoria crónica, incluyendo EPOC y asma","["+esSeleccionado('charlson'+tipo+'-p-6')+"] (1)"],
            ["Enfermedad del tejido conectivo: incluye lupus, polimiositis, enfermedad mixta, polimialgia reumática,\narteritis de celulas gigantes y artritis reumatoide","["+esSeleccionado('charlson'+tipo+'-p-7')+"] (1)"],
            ["Úlcera gastroduodenal: incluye a aquellos que han recibido tratamiento por un ulcus y quellos que tuvieron\nsangrado por úlceras","["+esSeleccionado('charlson'+tipo+'-p-8')+"] (1)"],
            ["Hepatopatía crónica leve: sin evidencias de hipertensión portal, incluye pacientes con hepatitis crónica","["+esSeleccionado('charlson'+tipo+'-p-9')+"] (1)"],
            ["Diabetes: incluye los tratados con insulina o hipoglicemiantes, pero sin complicaciones tardías, no se\nincluirán los tratados únicamente con dieta","["+esSeleccionado('charlson'+tipo+'-p-10')+"] (1)"],
            ["Hemiplejia: evidencia de hemiplejia o paraplejia como consecuencia de un AVC u otra condición","["+esSeleccionado('charlson'+tipo+'-p-11')+"] (2)"],
            ["Insuficiencia renal crónica moderada/severa: incluye pacientes en diálisis, o bien con creatininas > 3 mg/dl\nobjetivadas de forma repetida y mantenida","["+esSeleccionado('charlson'+tipo+'-p-12')+"] (2)"],
            ["Diabetes con lesión en órganos diana: evidencia de retinopatía, neuropatía o nefropatía, se incluyen\ntambién antecedentes de cetoacidosis o descompensación hiperosmolar","["+esSeleccionado('charlson'+tipo+'-p-13')+"] (2)"],
            ["Tumor o neoplasia sólida: incluye pacientes con cáncer, pero si metástasis documentadas","["+esSeleccionado('charlson'+tipo+'-p-14')+"] (2)"],
            ["Leucemia: incluye leucemia mieloide crónica, leucemia crónica, policitemia vera, otras leucemias crónicas\ny todas las leucemias agudas","["+esSeleccionado('charlson'+tipo+'-p-15')+"] (2)"],
            ["Linfoma: incluye todos los linfomas, waldestrom y mieloma","["+esSeleccionado('charlson'+tipo+'-p-16')+"] (2)"],
            ["Hepatopatía crónica moderada/severa: con evidencia de hipertensión portal (ascitis, várices esofágicas o\nencefalopatía)","["+esSeleccionado('charlson'+tipo+'-p-17')+"] (3)"],
            ["Tumor o neoplasia sólida con metástasis","["+esSeleccionado('charlson'+tipo+'-p-18')+"] (6)"],
            ["Sida definido: no incluye portadores asintomáticos","["+esSeleccionado('charlson'+tipo+'-p-19')+"] (6)"],
            ["Total:",""+$('.indiceCharlson'+tipo+'-total').val()+""],
            ["Fuente bibliográfica de la que se ha obtenido esta versión:",""],
            ["Charlson M, Pompei P, Ales KL, McKenzie CR. A new method of classyfing prognostic comorbidity in\nlongitudinal studies: development and validation. J Chron Dis 1987; 40: 373-83.",""],
            ["Comentarios:",""],
            ["En general, se considera ausencia de comorbilidad: 0-1 puntos, comorbilidad baja: 2 puntos y alta > 3 puntos.\nPredicción de mortalidad en seguimientos cortos (< 3 años); índice de 0: (12% mortalidad/año);\níndice 1-2: (26%); índice 3-4: (52%); índice > 5: (85%). En seguimientos prolongados (> 5 años), la\npredicción de mortalidad deberá corregirse con el factor edad, tal como se explica en el artículo original\n(Charlson M, J Chron Dis 1987; 40: 373-83). Esta corrección se efectúa añadiendo un punto al índice\npor cada década existente a partir de los 50 años(p. ej.,50 años = 1 punto, 60 años = 2, 70 años = 3,\n80 años = 4, 90 años = 5, etc.). Así, un paciente de 60 años (2 puntos) con una comorbilidad de 1, tendrá\nun índice de comorbilidad corregido de 3 puntos, o bien, un paciente de 80 años (4 puntos) con una\ncomorbilidad de 2, tendrá un índice de comorbilidad corregido de 6 puntos. Tiene la limitación de que la\nmortalidad del sida en la actualidad no es la misma que cuando se publicó el índice.",""],
        ];
    }
    if(idFormulario.indexOf('ICIQ') != -1){
        //se trabajara con ICIQ
         nombreArchivo="Indice ICIQ-SF";
        if(idFormulario.indexOf('Inicial') != -1){
            //Se trabajará con el ICIQ Inicial
            tipo="Inicial";
        }else{
            //Se trabajará con el ICIQ final
            tipo="Final";
        }

        var columns = ["Índice ICIQ-SF "+tipo+" ", ""];
        var rows = [
        ["RUT:",  numberWithCommas($("#paciente-rut").val()) + "-" + $("#paciente-dv").val()],
            ["Nombre:",  $("#paciente-nombre").val() + " " + $("#paciente-apellido").val()],
            ["Fecha "+$('#ICIQ-SF'+tipo+'-fecha-encuesta').val()+" ",""],
            ["¿Con qué frecuencia pierde orina?",""],
            ["Nunca", "["+esSeleccionado('indiceICIQ-SF'+tipo+'-p-1')+"] (0)"],
            ["Una vez a la semana o menos", "["+esSeleccionado('indiceICIQ-SF'+tipo+'-p-2')+"] (1)"],
            ["Dos o tres veces a la semana", "["+esSeleccionado('indiceICIQ-SF'+tipo+'-p-3')+"] (2)"],
            ["Una vez al día", "["+esSeleccionado('indiceICIQ-SF'+tipo+'-p-4')+"] (3)"],
            ["Varias veces al día", "["+esSeleccionado('indiceICIQ-SF'+tipo+'-p-5')+"] (4)"],
            ["Contínuamente", "["+esSeleccionado('indiceICIQ-SF'+tipo+'-p-6')+"] (5)"],
            ["Nos gustaría saber su impresión acerca de la cantidad de orina que usted cree que se le escapa\n Cantidad de orina que pierde habitualmente (Tanto si lleva protección como si no).",""],
            ["No se me escapa nada","["+esSeleccionado('indiceICIQ-SF'+tipo+'-p-7')+"] (0)"],
            ["Muy poca cantidad","["+esSeleccionado('indiceICIQ-SF'+tipo+'-p-8')+"] (2)"],
            ["Una cantidad moderada","["+esSeleccionado('indiceICIQ-SF'+tipo+'-p-9')+"] (4)"],
            ["Mucha Cantidad","["+esSeleccionado('indiceICIQ-SF'+tipo+'-p-10')+"] (6)"],
            ["¿Estos escapes de orina que tiene cuánto afectan su vida diaria?",""],
            ["["+esSeleccionado('indiceICIQ-SF'+tipo+'-p-11')+"] (0)     ["+esSeleccionado('indiceICIQ-SF'+tipo+'-p-12')+"] (1)     ["+esSeleccionado('indiceICIQ-SF'+tipo+'-p-13')+"] (2)     ["+esSeleccionado('indiceICIQ-SF'+tipo+'-p-14')+"] (3)     ["+esSeleccionado('indiceICIQ-SF'+tipo+'-p-15')+"] (4)     ["+esSeleccionado('indiceICIQ-SF'+tipo+'-p-16')+"] (5)     ["+esSeleccionado('indiceICIQ-SF'+tipo+'-p-17')+"] (6)     ["+esSeleccionado('indiceICIQ-SF'+tipo+'-p-18')+"] (7)     ["+esSeleccionado('indiceICIQ-SF'+tipo+'-p-19')+"] (8)     ["+esSeleccionado('indiceICIQ-SF'+tipo+'-p-20')+"] (9)     ["+esSeleccionado('indiceICIQ-SF'+tipo+'-p-21')+"] (10)",""],
            ["TOTAL:",$('.indiceICIQ-SF'+tipo+'-total').val()],
            ["¿Cuándo pierde orina? (Señale todo lo que le pasa a usted)",""],
            ["Nunca pierde orina","["+esSeleccionado('indiceICIQ-SF'+tipo+'-p-22')+"]"],
            ["Pierde orina antes de llegar al WC","["+esSeleccionado('indiceICIQ-SF'+tipo+'-p-23')+"]"],
            ["Pierde orina cuando tose o estornuda","["+esSeleccionado('indiceICIQ-SF'+tipo+'-p-24')+"]"],
            ["Pierde orina cuando duerme","["+esSeleccionado('indiceICIQ-SF'+tipo+'-p-25')+"]"],
            ["Pierde orina cuando hace esfuerzos físicos o ejercicio","["+esSeleccionado('indiceICIQ-SF'+tipo+'-p-26')+"]"],
            ["Pierde orina al acabar de orinar y ya se ha vestido","["+esSeleccionado('indiceICIQ-SF'+tipo+'-p-27')+"]"],
            ["Pierde orina sin un motivo evidente","["+esSeleccionado('indiceICIQ-SF'+tipo+'-p-28')+"]"],
            ["Pierde orina de forma continua","["+esSeleccionado('indiceICIQ-SF'+tipo+'-p-29')+"]"],
        ];

    }
console.log(idFormulario);
if(idFormulario.indexOf('Cognitiva') != -1){
    
        //se trabajara con Barthel
        nombreArchivo="Evaluación Cognitiva";
        if(idFormulario.indexOf('Inicial') != -1)
        {
            tipo="Inicial";
            var fecha=$('#EvaluacionCognitiva-fecha-encuesta').val();
            var numero1 = $('#primer_numero').val();
            var numero2 = $('#segundo_numero').val();
            var numero3 = $('#tercer_numero').val();
            var numero4 = $('#cuarto_numero').val();
            var numero5 = $('#quinto_numero').val();

            var repeticiones = $('#repeticiones').val();
            var total=$('#EvaluacionCognitivaInicial-total').val();
        }
        else
        {
            tipo="Final";
            var fecha=$('#EvaluacionCognitivaFin-fecha-encuesta').val();
            var numero1 = $('#primer_numeroF').val();
            var numero2 = $('#segundo_numeroF').val();
            var numero3 = $('#tercer_numeroF').val();
            var numero4 = $('#cuarto_numeroF').val();
            var numero5 = $('#quinto_numeroF').val();
            var repeticiones = $('#repeticionesF').val();
            var total=$('#EvaluacionCognitivaFinal-total').val();
        }

           if(numero1!=9)numero1="mal";else numero1="correcto";
           if(numero2!=7)numero2="mal";else numero2="correcto";
           if(numero3!=5)numero3="mal";else numero3="correcto";
           if(numero4!=3)numero4="mal";else numero4="correcto";
           if(numero5!=1)numero5="mal";else numero5="correcto";
        var columns = ["Evaluación Cognitiva "+tipo+" ","",""];
        
        var rows = [
        ["RUT:",  numberWithCommas($("#paciente-rut").val()) + "-" + $("#paciente-dv").val()],
            ["Nombre:",  $("#paciente-nombre").val() + " " + $("#paciente-apellido").val()],
            ["Fecha "+fecha+"","",""],
            ["","","Respuesta"],
            ["Sondee el mes, el día del mes, el año y el día de la semana.","",""],
            ["","- Mes",""+esSeleccionadoCognitiva('EvaluacionCognitiva'+tipo+'-p-1-')+""],
            ["","- Día mes",""+esSeleccionadoCognitiva('EvaluacionCognitiva'+tipo+'-p-2-')+""],
            ["","- Año",""+esSeleccionadoCognitiva('EvaluacionCognitiva'+tipo+'-p-3-')+""],
            ["","- Dia semana",""+esSeleccionadoCognitiva('EvaluacionCognitiva'+tipo+'-p-4-')+""],
            ["Ahora le voy a nombrar tres objetos. Después que se los\ndiga,le voy a pedir que repita en voz alta los que recuerde, en\ncualquier orden.","",""],
            ["","- Árbol",""+esSeleccionadoCognitiva('EvaluacionCognitiva'+tipo+'-p-5-')+""],
            ["","- Mesa",""+esSeleccionadoCognitiva('EvaluacionCognitiva'+tipo+'-p-6-')+""],
            ["","- Avión",""+esSeleccionadoCognitiva('EvaluacionCognitiva'+tipo+'-p-7-')+""],
            ["","- Numero de repeticiones",""+repeticiones+""],
            ["Ahora voy a decirle unos números y quiero que me los\nrepita al revés => Números: 9 7 5 3 1.","",""],
            ["","- Numero uno",""+numero1+""],
            ["","- Numero dos",""+numero2+""],
            ["","- Numero tres",""+numero3+""],
            ["","- Numero cuatro",""+numero4+""],
            ["","- Numero cinco",""+numero5+""],
            ["Le voy a dar un papel; tómelo con su mano derecha, dóblelo\npor la mitad con ambas manos y colóqueselo sobre las\npiernas.","",""],
            ["","- Toma papel con la mano derecha",""+esSeleccionadoCognitiva('EvaluacionCognitiva'+tipo+'-p-8-')+""],
            ["","- Dobla por la mitad con ambas manos",""+esSeleccionadoCognitiva('EvaluacionCognitiva'+tipo+'-p-9-')+""],
            ["","- Coloca sobre las piernas",""+esSeleccionadoCognitiva('EvaluacionCognitiva'+tipo+'-p-10-')+""],
            ["Hace un momento le leí una serie de tres palabras y usted \nrepitío las que recordó. Por favor, dígame ahora cuáles\nrecuerda.","",""],
            ["","- Árbol",""+esSeleccionadoCognitiva('EvaluacionCognitiva'+tipo+'-p-11-')+""],
            ["","- Mesa",""+esSeleccionadoCognitiva('EvaluacionCognitiva'+tipo+'-p-12-')+""],
            ["","- Avión",""+esSeleccionadoCognitiva('EvaluacionCognitiva'+tipo+'-p-13-')+""],
            ["Muestre al entrevistado el dibujo con los círculos que se\ncruzan.","",""],
            ["","- Evalue el dibujo realizado",""+esSeleccionadoCognitiva('EvaluacionCognitiva'+tipo+'-p-14-')+""],
            [" ","TOTAL",""+total+""],
        ];
    }
if(idFormulario.indexOf('GDS') != -1){
        nombreArchivo="GDS";
        if(idFormulario.indexOf('Final') != -1){
            tipo="Final";
            var fecha=$("#formularioGDSFinal #fecha_gds").val();
        }else{
            tipo="";
            var fecha=$("#formularioGDS #fecha_gds").val();
        }

        var columns = ["GDS  "+tipo+" ","","",""];
        
        var rows = [
        ["RUT:",  numberWithCommas($("#paciente-rut").val()) + "-" + $("#paciente-dv").val()],
            ["Nombre:",  $("#paciente-nombre").val() + " " + $("#paciente-apellido").val()],
            ["Fecha "+fecha+"","","",""],
            ["Estadio GDS","Estadio FAST y\ndiagnóstico clínico","Características","Selección"],
            ["GDS 1. Ausencia de\nalteración cognitiva","1.Adulto normal","Ausencia de dificultades\nobjetivas o subjetivas",""+esSeleccionadoGDS('GDS'+tipo+'-p-1')+""],
            ["GDS 2. Defecto cognitivo\nmuy leve","2.Adulto normal de edad","Quejas de pérdida de memoria. No se\nobjetiva déficit en el examen clínico.\nHay pleno conocimiento y valoración de la\nsintomatología",""+esSeleccionadoGDS('GDS'+tipo+'-p-2')+""],
            ["GDS 3. Defecto cognitivo\nleve ","3.EA incipiente","Primeros defectos claros\nManifestación en una o más de estas\náreas:\n*Haberse perdido en un lugar no familiar.\n*Evidencia de rendimiento laboral pobre.\n*Dificultad incipiente para evocar\nnombres de persona.\n*Tras la lectura retiene escaso material.\n*Olvida la ubicación, pierde o coloca\nerróneamente objetos de valor.\n*Escasa capacidad para recordar a\npersonas nuevas que ha conocido.\n*Disminución de la capacidad organizativa\n\nSe observa evidencia objetiva de defectos\nde memoria únicamente en una\nentrevista intensiva.",""+esSeleccionadoGDS('GDS'+tipo+'-p-3')+""],
            ["GDS 4. Defecto cognitivo\nmoderado","4.EA leve","Disminución de la capacidad para realizar\ntareas complejas.\nDefectos claramente definidos en una\nentrevista clínica cuidadosa:\n*Conocimiento disminuído de\nacontecimientos actuales y recientes.\n*El paciente puede presentar cierto déficit\n en el recuerdo de su historia personal.\n*Dificultad de concentración evidente en\nla sustracción seriada.\n*Capacidad disminuída para viajar,\ncontrolar su economía,etc.\n\nFrecuentemente no hay defectos en:\n*Orientación en tiempo y persona\n*Reconocimiento de caras y personas\nfamiliares.\n*Capacidad de viajar a lugares conocidos.\n\nLa negación es el mecanismo de defensa\npredominante.",""+esSeleccionadoGDS('GDS'+tipo+'-p-4')+""],
            ["GDS 5. Defecto cognitivo\nmoderadamente grave","5.EA moderada","El paciente no puede sobrevivir mucho\ntiempo sin alguna asistencia. Requiere \nasistencia para escoger su ropa.\nEs incapaz de recordar aspectos\nimportantes de su vida cotidiana\n(dirección, teléfono, nombres de familiares).\n Es frecuente cierta desorientación en\ntiempo o en lugar.\nDificultad para contar al revés\ndesde 40 de 4 en 4 o desde 20 de 2 en 2.\nSabe su nombre y generalmente el de su\nesposa e hijos.",""+esSeleccionadoGDS('GDS'+tipo+'-p-5')+""],
            ["GDS 6. Defecto cognitivo\ngrave","6.EA moderada-grave","Se viste incorrectamente sin asistencia o\nindicaciones.\nOlvida a veces el nombre de su esposa\nde quien depende para vivir.\nRetiene algunos datos del pasado.\nDesorientación temporoespacial.\nDificultad para contar de 10 en 10\nen orden inverso o directo.\nRecuerda su nombre y diferencia los\nfamiliares de los desconocidos.\nRitmo diurno frecuentemente alterado.\nPresenta cambios de la personalidad y la\nafectividad (delirio,síntomas obsesivos,\nansiedad,agitación o agresividad\ny abulia cognoscitiva).",""+esSeleccionadoGDS('GDS'+tipo+'-p-6')+""],
            ["","6a","Se viste incorrectamente sin asistencia\no indicaciones",""],
            ["","6b","Incapaz de bañarse correctamente",""],
            ["","6c","Incapaz de utilizar el váter",""],
            ["","6d","Incontinencia urinaria",""],
            ["","6e","Incontinencia fecal",""],
            ["GDS 7. Defecto cognitivo\nmuy grave","7.EA grave","Pérdida progresiva de todas las\ncapacidades verbales y motoras.\nCon frecuencia se observan signos\nneurológicos.",""+esSeleccionadoGDS('GDS'+tipo+'-p-7')+""],
            ["","7a","Incapaz de decir más de media docena de palabras ",""],
            ["","7b","Incapaz de bañarse correctamente",""],
            ["","7c","Incapacidad de deambular sin ayuda",""],
            ["","7d","Incapacidad para mantenerse sentado\nsin ayuda",""],
            ["","7e","Pérdida de capacidad de sonreír",""],
            ["","7f","Pérdida de capacidad de mantener la\ncabeza erguida",""],
        ];
    }

    if(idFormulario.indexOf('ConductaMotriz') != -1){
        //se trabajara con Geriatricos
        nombreArchivo="Conducta motriz anómala";

        if(idFormulario.indexOf('Final') != -1){
            tipo="Final";
            var fecha=$("#EvaluacionConductaMotrizFinal-fecha-encuesta").val();
        }else{
            tipo="Inicial";
            var fecha=$("#EvaluacionConductaMotrizInicial-fecha-encuesta").val();
        }

        var columns = ["Conducta motriz anómala  "+tipo+" ","",""];
        
        var rows = [
        ["RUT:",  numberWithCommas($("#paciente-rut").val()) + "-" + $("#paciente-dv").val()],
            ["Nombre:",  $("#paciente-nombre").val() + " " + $("#paciente-apellido").val()],
            ["Fecha "+fecha+"","\t\t\t\t\t\t\t\t\t\t\t\t\t",""],
            ["","","Selección"],
            ["¿El paciente hace una y otra vez cosas tales\ncomo abrir los armarios o cajones o coge las cosas repetidas\nveces o enrolla un cordel o hilos?","",""+esSeleccionadoMotriz1('EvaluacionConductaMotriz'+tipo+'-p-0')+""],
            ["","1.¿Va y viene por la casa sin un objeto aparente?",""+esSeleccionadoMotriz1('EvaluacionConductaMotriz'+tipo+'-p-1')+""],
            ["","2.¿Busca desordenadamente, abriendo y vaciando cajones\no armarios?",""+esSeleccionadoMotriz1('EvaluacionConductaMotriz'+tipo+'-p-2')+""],
            ["","3.¿Se pone y se quita la ropa reiteradamente?",""+esSeleccionadoMotriz1('EvaluacionConductaMotriz'+tipo+'-p-3')+""],
            ["","4.¿Realiza actividades repetitivas o tiene costumbres que\nrepite una y otra vez?",""+esSeleccionadoMotriz1('EvaluacionConductaMotriz'+tipo+'-p-4')+""],
            ["","5.¿Se dedica a actividades repetitivas como manipular\nbotones, recoger, enrollar hilo, etc.?",""+esSeleccionadoMotriz1('EvaluacionConductaMotriz'+tipo+'-p-5')+""],
            ["","6.¿Se mueve demasiado, parece incapaz de estar sentado\nsin moverse o mueve los pies o tamborilea mucho con los\ndedos?",""+esSeleccionadoMotriz1('EvaluacionConductaMotriz'+tipo+'-p-6')+""],
            ["","7.¿Realiza el paciente cualesquiera otras actividades\nde una manera repetitiva?",""+esSeleccionadoMotriz1('EvaluacionConductaMotriz'+tipo+'-p-7')+""],
            ["FRECUENCIA","",""],
            ["","0.- No presente",""+esSeleccionadoMotriz2('EvaluacionConductaMotriz'+tipo+'-p-8-0')+""],
            ["","1.- De vez en cuando: menos de una vez a la semana",""+esSeleccionadoMotriz2('EvaluacionConductaMotriz'+tipo+'-p-8-1')+""],
            ["","2.- A menudo: alrededor de una vez a la semana ",""+esSeleccionadoMotriz2('EvaluacionConductaMotriz'+tipo+'-p-8-2')+""],
            ["","3.- Con frecuencia: varias veces a la semana pero no cada día ",""+esSeleccionadoMotriz2('EvaluacionConductaMotriz'+tipo+'-p-8-3')+""],
            ["","4.- Con mucha frecuencia: una o más veces al día ",""+esSeleccionadoMotriz2('EvaluacionConductaMotriz'+tipo+'-p-8-4')+""],

            ["GRAVEDAD","",""],
            ["","0.- No presente",""+esSeleccionadoMotriz2('EvaluacionConductaMotriz'+tipo+'-p-9-0')+""],
            ["","1.- Leve: la actividad motriz anómala es notable pero no\ninterfiere muchoen la rutina diaria.",""+esSeleccionadoMotriz2('EvaluacionConductaMotriz'+tipo+'-p-9-1')+""],
            ["","2.- Moderada: la actividad motriz anómala es muy evidente;\npuede ser superada con ayuda del cuidador.",""+esSeleccionadoMotriz2('EvaluacionConductaMotriz'+tipo+'-p-9-2')+""],
            ["","3.- Severa: la actividad motriz anómala es muy evidente, por lo\ngeneral no responde a niguna intervención por parte del\ncuidador y es una fuente importante de aflicción.",""+esSeleccionadoMotriz2('EvaluacionConductaMotriz'+tipo+'-p-9-3')+""],
            
            ["TOTAL","", ""+ $('#TotalEvaluacionConducaMotriz'+tipo+'').val()+ ""],

            ["ANGUSTIA","",""],
            ["","1.- Nada en absoluto",""+esSeleccionadoMotriz2('EvaluacionConductaMotriz'+tipo+'-p-10-1')+""],
            ["","2.- Mínimamente ",""+esSeleccionadoMotriz2('EvaluacionConductaMotriz'+tipo+'-p-10-2')+""],
            ["","3.- Levemente",""+esSeleccionadoMotriz2('EvaluacionConductaMotriz'+tipo+'-p-10-3')+""],
            ["","4.- Severamente",""+esSeleccionadoMotriz2('EvaluacionConductaMotriz'+tipo+'-p-10-4')+""],
            ["","5.- Muy severamente o extremdamente",""+esSeleccionadoMotriz2('EvaluacionConductaMotriz'+tipo+'-p-10-5')+""],
        ];
    }

    if(idFormulario.indexOf('Mmse') != -1){


        nombreArchivo="MMSE";
        if(idFormulario.indexOf('Inicial') != -1){
            tipo = "Inicial";
        }
        else{
            tipo = "Final";
        }
        var columns = ["Pregunta","Resultado"];
        var rows = [
        ["RUT:",  numberWithCommas($("#paciente-rut").val()) + "-" + $("#paciente-dv").val()],
            ["Nombre:",  $("#paciente-nombre").val() + " " + $("#paciente-apellido").val()],
            ["Fecha "+$('#EvaluacionMmse'+tipo+'-fecha-encuesta').val() + " ", "" ],
            ["1.- Orientación temporal"],
            ["¿En qué día estamos? (fecha)", ""+ esSeleccionadoMmse("EvaluacionMmse"+tipo+"-p-1") + ""]
            ,
            ["¿En qué mes?", ""+ esSeleccionadoMmse("EvaluacionMmse"+tipo+"-p-2") + ""]
            ,
            ["¿En qué día de la semana?", ""+ esSeleccionadoMmse("EvaluacionMmse"+tipo+"-p-3") + ""]
            ,
            ["¿En qué año estamos?", ""+ esSeleccionadoMmse("EvaluacionMmse"+tipo+"-p-4") + ""]
            ,
            ["¿En qué estación?", ""+ esSeleccionadoMmse("EvaluacionMmse"+tipo+"-p-5") + ""]
            ,
            ["1.- Orientación espacial"],
            ["¿En qué hospital (o lugar) estamos?", ""+ esSeleccionadoMmse("EvaluacionMmse"+tipo+"-p-6") + ""]
            ,
            ["¿En qué piso (o planta, sala, servicio)?", ""+ esSeleccionadoMmse("EvaluacionMmse"+tipo+"-p-7") + ""]
            ,
            ["¿En qué pueblo (ciudad)?", ""+ esSeleccionadoMmse("EvaluacionMmse"+tipo+"-p-8") + ""]
            ,
            ["¿En qué provincia estamos?", ""+ esSeleccionadoMmse("EvaluacionMmse"+tipo+"-p-9") + ""]
            ,
            ["¿En qué país (o nación, autonomía)?", ""+ esSeleccionadoMmse("EvaluacionMmse"+tipo+"-p-10") + ""]
            ,
            ["2.- Repeticion inmediata"],
            ["Nombre tres palabras Árbol-Mesa-Perro a razón de 1 por segundo.\nLuego se pide al paciente que las repita. Esta primera repetición otorga la puntuación.\nOtorgue 1 punto por cada palabra correcta, pero continúe diciéndolas hasta que el sujeto\nrepita las 3, hasta un máximo de 6 veces."],
            ["Arbol", ""+ esSeleccionadoMmse("EvaluacionMmse"+tipo+"-p-11") + ""]
            ,
            ["Mesa", ""+ esSeleccionadoMmse("EvaluacionMmse"+tipo+"-p-12") + ""]
            ,
            ["Perro", ""+ esSeleccionadoMmse("EvaluacionMmse"+tipo+"-p-13") + ""]
            ,
            ["3.- Atención y cálculo"],
            ["O", ""+ esSeleccionadoMmse("EvaluacionMmse"+tipo+"-p-14") + ""]
            ,
            ["D", ""+ esSeleccionadoMmse("EvaluacionMmse"+tipo+"-p-15") + ""]
            ,
            ["N", ""+ esSeleccionadoMmse("EvaluacionMmse"+tipo+"-p-16") + ""]
            ,
            ["U", ""+ esSeleccionadoMmse("EvaluacionMmse"+tipo+"-p-17") + ""]
            ,
            ["M", ""+ esSeleccionadoMmse("EvaluacionMmse"+tipo+"-p-18") + ""]
            ,
            ["4.- Memoria"],
            ["Pedir que repita las 3 palabras previas, dar 1 punto por cada respuesta correcta."],
            ["Arbol", ""+ esSeleccionadoMmse("EvaluacionMmse"+tipo+"-p-19") + ""]
            ,
            ["Mesa", ""+ esSeleccionadoMmse("EvaluacionMmse"+tipo+"-p-20") + ""]
            ,
            ["Perro", ""+ esSeleccionadoMmse("EvaluacionMmse"+tipo+"-p-21") + ""]
            ,
            ["5.- Lenguaje"],
            ["DENOMINACIÓN. Mostrarle un lápiz o un boligrafo y pregutar ¿Qué es?", ""+ esSeleccionadoMmse("EvaluacionMmse"+tipo+"-p-22") + ""]
            ,
            ["Mostrarle un reloj de pulsera y preguntar ¿Qué es?", ""+ esSeleccionadoMmse("EvaluacionMmse"+tipo+"-p-23") + ""]
            ,
            ["REPETICIÓN. Pedirle que repita la frase:'Ni sí, ni no, ni pero' (o 'En un trigal había 5 perros')", ""+ esSeleccionadoMmse("EvaluacionMmse"+tipo+"-p-24") + ""]
            ,
            ["ORDENES. Pedirle que siga la orden: coja un papel con la mano derecha", ""+ esSeleccionadoMmse("EvaluacionMmse"+tipo+"-p-25") + ""]
            ,
            ["Dóblelo por la mitad", ""+ esSeleccionadoMmse("EvaluacionMmse"+tipo+"-p-26") + ""]
            ,
            ["Y póngalo en el suelo", ""+ esSeleccionadoMmse("EvaluacionMmse"+tipo+"-p-27") + ""]
            ,
            ["LECTURA. Escriba legiblemente en un papel 'Cierre los ojos'.\nPídale que lo lea y haga lo que dice la frase", ""+ esSeleccionadoMmse("EvaluacionMmse"+tipo+"-p-28") + ""]
            ,
            ["ESCRITURA. Que escriba una frase (con sujeto y predicado)", ""+ esSeleccionadoMmse("EvaluacionMmse"+tipo+"-p-29") + ""]
            ,
            ["COPIA. Dibuje 2 pentágonos intersectados y pida al sujeto que los copie tal cual.\nPara otorgar un punto deben estar presentes los 10 ángulos y la intersección", ""+ esSeleccionadoMmse("EvaluacionMmse"+tipo+"-p-30") + ""]
            ,
            ["Puntaje total", ""+ $("#EvaluacionMmse"+tipo+"-total").val() + "/30"]
        ];
    }
    // Only pt supported (not mm or in)
    var doc = new jsPDF('p', 'pt');
        doc.autoTable(columns, rows, {
        //startY: doc.autoTableEndPosY() + 40, 
        theme: 'grid'
    });
    doc.save(nombreArchivo+" "+tipo+'.pdf');

}


function numberWithCommas(x) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}

function aTiempo(decimal,conHora)
{
	var conHora=conHora||false;
	if(decimal==null||decimal==""||decimal==undefined)
	{
		return conHora?"00:00:00":"00:00";
	}
	var segundos=Math.round(decimal*60);
	
	var hora=0;
	var minuto=0;
	var segundo=segundos;
	
	var contseg=0;
	var contmin=0;
	
	while(segundo>=60)
	{
		segundo-=60;
		contseg++;
	}
	minuto+=contseg;
	if(conHora)
	{
		while(minuto>=60)
		{
			minuto-=60;
			contmin++;
		}
		hora+=contmin;
	}

	
	var minuto=minuto<10?"0"+minuto:minuto;
	var segundo=segundo<10?"0"+segundo:segundo;
	var hora=hora<10?"0"+hora:hora;
	return conHora?(hora+":"+minuto+":"+segundo):(minuto+":"+segundo);
}
function formato_numero(numero, decimales, separador_decimal, separador_miles){ 
    numero=parseFloat(numero);
    if(isNaN(numero)){
        return "";
    }

    if(decimales!==undefined){
        // Redondeamos
        numero=numero.toFixed(decimales);
    }

    // Convertimos el punto en separador_decimal
    numero=numero.toString().replace(".", separador_decimal!==undefined ? separador_decimal : ",");

    if(separador_miles){
        // Añadimos los separadores de miles
        var miles=new RegExp("(-?[0-9]+)([0-9]{3})");
        while(miles.test(numero)) {
            numero=numero.replace(miles, "$1" + separador_miles + "$2");
        }
    }

    return numero;
}
function transformarDato(valor)
{
	if(valor===null||valor=="null")
		return "Sin datos";
	if(valor===true)
		return "Sí";
	if(valor===false)
		return "No";
	return valor;
}
function generarInforme(datos,con_graficos)
{
    if(con_graficos===undefined)
    	con_graficos=true;
	var nombreArchivo="Informe acciones repetidas";

	var columns = ["Encuesta","Resultado"];
	
	var rows=[];
	var doc = new jsPDF('p', 'pt');
	
	var pos_x=50;

	var ancho=250;
	var alto=150;
	
	var ancho_2=400;
	var alto_2=250;
	
	var fecha=new Date();
	var imagen_logo="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASIAAAC8CAYAAADcpaMxAAAACXBIWXMAAA3XAAAN1wFCKJt4AAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAHxVSURBVHja7J13mGRlmbfv96SKXaG7OvfkCANDzoqogKAI5rjmvGteXdc166q7roqoKAKGjfpt0HVVUBEVEEUkiDAwsXOo6u7qVLnqhO+P962enqErDArMSD3XNdfATE2fqlPn3Of3ZOF5Hi1rWcta9nia1joFLWtZy1ogalnLWtYCUesUtKxlLWuBqGUta1kLRK1T0LKWtawFopa1rGUtELVOQcta1rIWiFrWspa1QNQ6BS1rWctaIGpZy1rWAlHrFLSsZS1rgahlLWvZE96M1ik4NuzB/QvsG1rCMh/3Z4cOPAn4GPAU9Wcu8HH163Hvoi6VHU7YFmfzukjrwmmBqGV/prYZ+CfghMOU9WvU7x9pnaKWtUDUskfTtgP/Dpy6yt+tA96r/rsFo5YdkbViRC1r1o4H/q0GhKoWAP4G+GjrdLWsBaKW/altB/At4LQmXusH3tdSRS1rgahlf0o7AfgGcMYR/Bs/8LctGLWsBaKW/akgdD1w5iP4t37g/S03rWUtELXsj7ETgWuBs/6In+FTMPpQ63S2rAWilh2p7QSuAc5p4rV2g7+3FIhaMGpZC0Qta9pOAr4CnNvEa/8XeAWw1OB1pgLRB1unt2WrWauOqGWHQ+hq4LwmXnsb8E5gRKmi64FoAxhVg9d/3zrVLWspopatZicDX2oSQt8D3qEgBPDfwOuBhSYefB8FPtA63S1rKaKWHW6nAFcBT27itf8D/PUKCLECRppy6zrq/Hudgz1pn2qd+pa1FFHLQFZKf6FJCN0KvHsVCFXtP9XfzzVx3X0CmVFrWctaIGopIa4Ezm/itd9VSmi0wev+BXgrMNvEtffJFoxa1nLNWhA6Egi9Axhv8md/GxDq53fVeZ1QMAL4dOsraSmilj3x3LErOThPqJ79t3K3xo/wGP+hFNR0g9cJZKzofeq/W9YCUcueIBD6XJMQ+h7wLmrHhBrZvyFHg6SaeO0/AO9pXZMtELXsieGOfQ64oInX/g+yTmj8jzzmvyAbYJNNvPYzSkXpra/qiWWtGNETC0KfbxJC3wPeDkz+iY79LfX7p4DeJmDkITN5dutraymilj0xIfQ/yMD05J/4PXwLWcjYzM/9JwVCs/XVtRRRy56YSuhtwNSj9F6+iQxIfwLoa/Daz6nfvwyUW19jSxG17IkBoWqKfupRfk/fQDbATjTx2s8ha5J8ra+ypYhadmzayTSXoneA/0Km6Kceo/f2DaWMPgb0N6mMrgZKra+1BaKWHTt2UpMQcpF1Qm8Aso/xe/y6gtFHm4SRi5yRVGx9vS0QtezYgNBVTUDIA/4PmaLPPk7v9Xr1+0eAgQavvVL9/jWg0Pqa/7ysFSP687KdwBebgFAJ+A4y/pJ8nN/z9cpFa6Ze6UrgTci1RS1rKaKWHYV2IjLD1KiL3lbu2F/Ukkmu8+htjdZ1UQtGQimjRm5aVRldC+RbX3sLRC07NiH0f8gliKsCyDAE7e3+R+2Nzi2WcB1vNSBdp37/8BG4aS0YtUDUsqPETkBmlBpBqAJ8H/grVjSieh64rodhaESjJm1hi53b44/am73ngTTFksNipozngRB/FIw89W9aMGqBqGWPhXme/LXCBHID61eBJzX452VkndAbgczKn6nrgkjYJBqxOHFb/FH/HKeeIIc33rsrzdRMHscFXROHw8ijuWzaF9TvX+fxC7i3rAWiJ44F/Dp+n47relUV0a+UUCMIVZDbNh4GIQT094Q4YWvsMf88p+zogF2QzVfI5W1c9xB1VI0ZfZTGFdhfQG4R+Vfleq4G7ZYd5dbKmh0jtnFNG5vWtlEqO9U/upzGK3/KyJjQOx4GIWBt3+MDoZUwevIZPfQkAoB3ODyuUyBqpgL7JcBakG6m36fjs1qXdgtELXtUTNcFPkvHdT0fcHYDRVtEzpB+JStS9MsQ6g9xwtb4UfG5Tt7RQV9X8PB40UoYNWqUPQ3YUVWLJx/fwdq+cOuCaYGoZY+KKlrbxgnbYgBrXc/rbuCO/SfwKlYEcj2kw7O2P3zUQGgljGIRH677MJ/qemRav177SdR1vbBAxqB6u1plRi0QtexPaQIIIRcXRoHg2r4wJ25v3+d5/MT1PLuGO/ZD5DAyd+VfOI5HT2fgcXXH6plXO7BzPTKTtmrxpet6N3ged556YoKezmDrqjkGrRWsPnothGzXeCZymJgGHABuXNsX2i/gh/ftnjvfg6cLQdUPyQI/AN5MjTXQqyiOY8Wq7SAfBroBC6h4njfneXz59BMTB3o6AxEgfNgDtoxcb9QastYCUcseAYQ+CPyt43lUPA8PD0to6EJ8AviPNX2hd9mO+6LfPzj3Fr9PfxNyiNhXkL1YeSCIHJ/hInuzas308QNtK64FF8ipf+P8iT+XHzkGVqj3U26g1oPqczkKstcDDwKvBS7xPG53XN52xkkd0z2JwDbkzOvXH/ZzRpBD1n5Kq2G2BaKWNW1B5EjVtxddlz/kF/lDbomM63BuW5ydwQgBTX8ZEEfwStMQV3keV60I9BrAccgWjvOR2bLvIrdqHF74lwBegxxw37niz68B/h9yoaL7JwJQWEGiT8Hld8BNyOLK0iogPhV4NbBVveYfgXuAXwO/9jxwHJczT+6kOxFYhwxsP7lScbFtDw/QNbAsbZ0Q4vvIavIvtWB0lMYgvFbBxdFkAeT0wrfnHMf8xvQYNy6kqHguhhDM2xUui3fz1t4NxHSzAPzV6GTum/c9lMbQtWpM6XnIvWKHj1n9R9vxPtTbFaicfmJCQ66FfgfwgYrnUfZcPM/DEBo+TUPAIvAMBYw/BkYh5CaQT1TbSDzAONji8R5kq0bm13dPM79Y0jRNfAXZ3LrSsuqz3eyBa9suZ0kI9SpoPjmTqXDbr2d4cM8itu3S3xvk/Cd1MdAfRNeEDfwdsim4NdfoKLNWsProMT9yfMdfF1zH/FJykB/MT6EJ8GsahhB0mRY/mE/xo/kknoTWtrV9IX3n9nZKJQflYl3K6rOeXy6EeJbq8ToO+JEHH1h0Knxvboo3HLiX5+/5HZ+d3M/+QhZPBsd/ouJUj9R0ZKHhJ2zbI50u8bt70vzmtzNksnY1XvVZFdMK67pACNYDT13lZ4UVSDaXSg5n7OykOxEAeDlwej5vc/MtSXY9tIDfpxEOGaTnS/zoxxPsenAB1/UM5DLHt6v4UstaIGrZYWYp1+INtudx3fQIv1yaxZLKZNk8IKBpTJSLZB0b4GLgzBUB6PXAttUOYNtuuK/Lv+aU4zssZEbtjNlKmaumhvhqcoglx8HSNH6VSfPxiT2MlvJVN/F9f8TnOhu4xHU9fvrzKb79X8Pcedcs9943x1ev38uuB6VyQW7ueNNZJ3fq7VHfGY7rtdX4eVurLuSKz7wNCNzz+zkODGYJBA5uIjJ0gW273H7HDA88tIjneaZSnG9rhSVaIGrZw5/01wIvLbsun588wI3zKQyhrbr21BCC6XKZVKUEMojrHMaqWr521PM4G3ijB3+xaFf47/QkP1+cIawby8cyhcZMpcLfj+9FKatzeOR7xt4PWPc/MM/IaA4EaJpA0wTBgM5Pbp7gwd2L2PayMnqdJ+FXyxXsA3oO+zMPwPXAXSXMoGmCSsXl9jumeeDBRTwPH/D3yIFwrf1pR4m1ngqPrwWRQeRnO8BVyUF+uTSLXgNCAAXX5ay2GJv9IYB9wO5GB/E8j6DfqCTi/iuAl42XC3xhapD7cku06Q+/BASgHYx+x5CjZK95BJ/vKYD+hwcXcRz3YZ/J7zf4yc1TCCE4blsUwxBf60r4f5vNVYIV21ut0po6oK39tNUElbJURgAnHB/zC8HH1Uf97CP5mS1rKaI/F2sDPg08zQO+MHWAW5bSaELUhFDZc9keCLNJQghgPzXqharmeh6WqbN5Q8RaNxAOjZYKfG7yAPfnlghotb/+Fe+hANz2CD+jB1AqOjWbUAN+nZ/cPMlDexZxHI9NayNnrekLx3VdHFHj6ob1Yfp6AlVXb1UYlcsOv/7tDA88uAAyxvYR5NKAlrVA9ISNCf078DYPQp+fPMAvF2cPB8AhVvE8YrrFa7rWcEooBnAv8KP6SggMQ2P9mjbW9YfJODY3zE9zX24RXw0IeYBP09jgW4ZdEVm780hMAPT2BmtNZpTKyKfz05sneXD3Aq7rsX1TlLV9YZqE0XeA3Wv6g5x0Yjt+v1GzaFPTBKWSw2/unOEPu+ZBZvQ+ilxz3bIWiJ5wEPpP4FmAkEpoFq8BhCK6wdt6NnBeWwdCFhxeBdwJrOrCuJ6HaWhs3RBh89o2pislPj91gO/OTRLWa3vkjucR1Q1elugH2bP2mz/CdfkUUDr9lDi6Vn80h8+n89Obk+x6cPEgjPpDaNrqMFrxmX+BnMlU2HFclCef24llaXVhVCw63HFnmj88sFCN0X0EWUbQssfJ9I9+9KOts/DY2neA5wDaVVOD/GIxjdsAQm26wTt6N3JuJI6GKABvUYrKHRzLcP+eeSxLB9n68CzX9dYFggYnbI2zpifEUCnHpyf2cX8+g1XHHat4Ht2mxcfXbGeNL4iQvV1P5ZFXWJeA14WCpj4wEGL//gyOUzP2g2EI9h3I0NZm0NUZIBH34Tgei5nKymmO/2kY2kNjUznawgaRsAVwN7AoBE/rTPiMQMBkYjJPxfZWxrpWQEwGsJPTRSxLo7vL7wPOUp/z161LtKWI/pzNh5wkeBmgfWx8Lz9dnMbFqwMhl7Cu8/aejZwbXobQm5EBbqd6U618+ruuh8/S2bI2Qk8iwFSlxDdmx9hbzGIIURdCXYaE0FoJoTngCv64dc93AxcJgd3d6eeKywawTK2uMjJNjZt+keSBhxbwEGzdGGH9QBjtMEUlR34sfx5bqaK/FUKUj98W4UnndmGZGo5TTxnZ3PG7We6XyigKfKDlprVA9OduXwReCvi/mBzi3txCHR0kA9MhzeAdvZs4ty2OJkRRQejb6sZj/8gSv9+VRtcEtuNRqbieZere1k1R1vSGSNolvpIc4reZeXya3lgJrV2GUBpZVX3vH/mZXeBXyzDqCkgYWbVhJASYhsbPfj7FrgfnEQi2bjgII9vxsB0PQxfccc8MY1O55Y+B7LX7G00TleO3RXnyuV2YDWBUKNjccdcsf9i1DKMPtty0x95aLR6PjV2N7OkKfDE5xC8XZyh7Xt3sWFjTeWfvJs5ta0cXooxMoX9b3XAynuN48iZTP6hYcs5xPe9fY2Fr01A5z5VTB9hXyKHXycSVPJde08dH12xnvYTQLLI6+x4atHbcdNsEhqHx1HN6G4YAkMP9f+J5WKlUgf+9YZxyyUHTauToPbAdlwsv6OWE42N4nscf9szRFQ88J9Hh/371NaYhDv8ZPgXszzquZzy0e5Fbbk9hV7yaAXPX9QgGTc48vZ2dO+Ig1eCnkan9lrVAdMxbP7Kh9NVLth29NjXMr7NzDSEU1HT+um8zZ4fjGAch9J0GbtIJyj05Z7iU17+YHGJXbglT0+pAyKHX9PORgW1s8IeqSuhS5VK5tQCRni9y74NzVCouQoBPxqd42rm9jWB0PnCj5+FLpgr84IZxCkWnJiA8DxzX4+lP6ebEHXEcx0PT+IAQ4tPUD6D7kL1qn3ccT9+9Z5Fbbp+mUnHrwigUNDnj9A527oiBPBf/0ILRY2OtYPWjax9AdpxHv5Ic5PamIGTw7r5NnBNuxxCi0iSEEsg+qktSlbJ+/fQId2cXHtYistKKnkOf6efDa7azUUJoVsWv7mqkhHL5CqMTuWUlYtvSXers8OP31XQBPWAMuF0IXhIOmUZ/X5DBoSzliruqMhJCeq+DQznCIYOe7gBCiLPUufhNnbfoAPcBc5omLu5o94lwyGR0PK9gViuA7TAzW8QwBN1dgSByBK1LK4DdAtExbJ9EZrdiV04N8qtMuiGEAkLnPX2bOaetHUMIW0GsEYR2AN8AnjFSyptXTR3g3twiuqatGgAUQNF16LMCfGhgG5skhGaQw/ib6rTPF2wmp/PLwWIh5K/+7iABf91ifQ8YBX4tBC8Oh0yjvzfA4HA9GMk/GxzOEgoadHf5LeTSgEoDQNgKRrOaJp7R0e4TobDRBIxcZmdL6LpGd5c/iBxH4rVg1ALRsWYJ5Iqb1y45legXkoP8OjNHyfNqZgbKnktA0/nr/k1VCDnA65DjLRq5Y18Gzh8vFc2vpIa4L7eEVgdCOcdhwBfggwNb2OwPVyF0BfBbmqwXyhdsJlP5lVkrAAZ6Qo1AtBJGvxGCF4XDptHXG2RwJEulXB9Gw8NZgkGd7q6AhWyobQZGf1gJo3DYZHQsh23XhlG54jGbLqLry8rolBaMWiA61uwGZLFi6KPje7gru4gDDSH0nt5DIFRVQvXm5sRVDOOZ4+WC/pXUMPfmluoqoZzjsMYX4AMDW9hyEELPAe7gCIoWHwmIcnkby9RWwmgY+K0QvCAcNs3e7gBDI1lKpdow8oDhkTxBqYyqm0ycJmE0o2ni0o52H+Gw0QBGUC57zMwWMaUyCrWUUQtEx5JdDTwXsD43eYDfZRdwG0DIr2m8p3fZHXOVEmoEoeORHfuXjpcL1jXJEe7JLaILWC0qJICs47DWH+Dv+rewNRAGSAHPVzeWVyo73PrbFI7j0R7z/clBZFdc9g0v8cC+Bdb1hxBCVGF0pxA8v03BaHi0Poxc12NkLEcgoNPdFfABZyp38vYGMLofmNY0cWl7u4+2sMnIWA6nDowqFQmjFW7aKeqvWzBqgeiotB7gQ8Drso4duGpqkF9n57DrQKjkufhVTOjcgxB6bRMQWofM5Fw6Xi5aX0uNcHduAQ1WryKuQsgX4G/7t7BNQigJvAi4vVC0vVt/m2RiukCh6BAKGnQlAn96ENkuU9MFFpbKJGcKDI5mWNsX9jRNDAG/E4LnR9pMs7s7wMholmLJOXwVNSBrf1zXY3Q8X4WRv0kYVRSMZjRNXNLR7qMtZEgYObVhVKp4zKZLVTctBJysjvWb1mXfAlFTnw25ZnkbsBPZUzT+KBynG9l8+sy864T/fnIfd+UWsWsEpoWCUEBovLdvC+cdGYRiyNnNzx0rFcxrU8PclV+sC6Elx2a9L8jfDmxme6AN5H6wF+cL9m23/S7F1HSBbN6mYrsYuka0zaSrYxlElyLnEZ0IbEKNHHmkIJqdL1EoOhRLDuWKy0y6yOBoxlvTGxrUNHE38NxIm2l1d/kZHc1TrJHa1zSB6zwMRmfQOMP1MBiFwwYj41kcm9owKrnMzpXQDI0e6aadrNy0Foz+RGb8mQLoo8gsUL+S5QI5OP5u5FD54p/wWNch07x8eUrW7tSKCQmg6Ln4haaUUBxduiivQxYrNkrR/xPwovFy0bpueoS7sosIrT6ENvhD/E3fJrb72wB+n8vbL7zzvpn9uibI5StomsDQxeEBovOBKxVkzRXuzTuR0xRvqPM+X4AsA9jOwdLxrytXchkkAEvZMq7n8Zt7ZzzHcX987mnd51mm9t8D/aHNl1zcx49vmiCTsbFWWR+t67Ka/NZfTSOAE3fEY8hBbEK9x5qCTn1n6Lr4wnHbooDgF7cmsW0Pw1gNfJDN2tx9Txrheew8Id6FHMZfnWf0p7Q4cpztRSvO/a+QvYX3tEB0bFg38C3gSUB4sJhjuFSg2/SxI9gGcpSqD3gxD99o8UjsO8ClBdfha6kRfpOdw2sEIRWYVhXTnlJC/9EAQlGlhF4yVir4r52W7pgQoFMbQht9Qd7bt4njAm2Uyy73753fmMmWv5XJVdA1IQzj0HfqOB4V29WUq7ke4OeLswR1neMCYaK62Qdscl1v1HUPdWfKFbfa87YB6OLQ6YcbPY/XViqu47oHK8E1TaAhWMqUcV3Pu+Oeaa0tbPbt3B5nTX+QSy7s58c/mySTqawKIzkK1uOW26fxPNh5QjyOHIPLkcEoAnj84tZUQxjdde8cCMHOHbEqjPgTwui1yJKPbch5VVU7UT1Y34qcI/5nZ39OldXd6qnx9Kzj8OXkIPuKWcqeh4FgZyjC23o2YAgN4L+BF/6Rx/ov4LyS52qfHNvLfYUlnDp1Qssxof5NnBc+BEL/zoq2jVWsX91Uzxss5f3XpUa4L7coVUENJbTo2Gz0h3hv3yaOD7RRKjnct3uO6dkiCB4We/E86Wd0tvs5YWuMYMBgTzHLlyaHWHQr6Ahihsl5be28sKOPYsnh13dPUyw5CCEnQB63Ocaa3hCGoXH/rgX2D2ZwXY/e7gCnndKOz6eTzVW4b/c8i5nyquep2pnf3Rng5OM7MHTB6HiOn/xsikymjM/SV03t2baHaQrOP6+LE2WLxjwyo/iZBt9jAFmBfaXjeDy0d4lf3JLEcVaHEYDrQlubwWknt7PzhDjIzOM/AJ//I6/fGHKsy5b5xTJ3/HaWXN4mENA5/ZR2uuUa7WFk+8qfHYz+XGJEvcC/ABdmHJvPT8oCwkXHpuS6LLk2Q8U8Y+UCJ4Ui+DS9H1nstv8RqsibkEV14lMTe/lddrFxdkzovKdvE0+KHBGETkTWJF2+t5DzXZsa5g/5JbQGENrkD/Ke3s3sCB6E0Ey6uDwv+mEQ8qCzw8eJ29oJBgweKmT4+/F9DJXzlF2XrOswY5cYLxfZ6A8yEAgghGA6XUAIQVvY4sRtcXRd4yc3T/Hg7kVmZkosLpaZmytxYDjL+jVh2tpMOuI+JlN5bNt9WIxJvjdBNlchl6/Q3REgHrPo7PAxPpEnV7AxDW3VmJFtu0xM5PH5DLq7/AHgdBXHaZRNq9YZLceMhhsFsEsu6bkShnFIat/5I2NGPwROHx3Lcdvt04yM5llYLLOwWGZkLE80ahGPWTHgPGDPI7x2WyB6FK1PuWMXL9kVrkwO8pvsHBoCU2hoQqALgQMs2BW2BMIMWIEispXhrkdwvG8jO9O5OjnMrUuzIERDCP1130bOjyTQEB6yTuhfqb8GeY2KCV22v5jTr5se5g/5DHqdwPSCY7PFH+LdvZs4IRihWHS4f4+EkBDiYXOAJIRka8YJW+PLEPr0+D6mKkVCmo4QYvkcFlyX4VKeS2PdBPwGg6MZNE3QkwjSnQiwd/8Sv7s7Tbkse7p0XeC4sJSpMDmVp783RDRikoj5SC+WKZedh8FI/q+EUbZg06Vg1NHhZ2KyQC5nY5qrw6hsu0xOFvD5NLq7As3CqMLBCmwJo5DB6FgWx2kQwJ4toRvL2bQ/BkZx5KA746c/n2JyqoCmyfMnhCCft5mcLBAMGiQ6fHH1ENz75wSjYx1E/SoQ+owlx+aLySFuz8yhI1ZVDCFdZ0egjY3+UAH4MUc25qIHWen8nKLr8NnJA9yaqT/UrOy5+ITOu/s28ZRIAiFvijcC/9wAQsepYPGlB4o5/frpEe5rAkKb/UHe1buJE4MRiiWH+/fOM90QQoFlCO0uZPiHiX1MKAgd7gZ5QMaxeWmiHyFg//ASuq5xyo4ODENj7/4lppKFQ+AihAwsL2UqTEwVGOgNEo/7aI/5yOQqFIrOw95b9f+zuQr5gk1nu5/2uI+OdqsujHRNUK64TE4V8Pv1qjJqpl+s2g4yp2niGYkOH6GwychobRgBsh0kXcI0Nbo6/SF1rAqyQPRI7PnAsw4MZS21aeSQc6LrgkLRYSpZkKqy3RdHFnPuBQ78OYDoWJ5H1KcCjpcsORW+lBzktky6JoRADhqryJiYUEHrIznWfwLPzDg2/zixn1uW0lQ8tyGE3tW7kQsiHSsh9M0GEDobWRj57F2FjPnV1LBs26gDoXm7wmZ/iHcqCBUKNvfvmSc1W6gJIXcZQjIm9GAhwz9M7GesvDqEZEwK1vqCChZi+WetfDe1Zq+ZpsbMTJEbfjpJOl0iEjY5cVuc9qhVYxSs/FlT03nu3zNPueKybm2YC5/aSyxiUVwFYNXjlEoOv/rNNPfLGUPtyGzaext8xwXkPKP36Lpgx/YoTz2/B12juu7oYbAUAnI5mzvvTnO/HMifQM4zevsRXstbAUPTZFC81vnL5Wxu+VWK/QcyAJuRK7QvaSmix1cJXQs8c9Gu8OXkELfWgZCnwLDOF+QliX5CulFQX+JIk8f7kspacM30ML9cSqMLQa3e9qo79q6+jTw1mkA9kd+EbE6t11R6gnLHnravmOXr06P8Ib+EUeNzVSG0JRDiHT0bOSkUpVRypvePZEbGk7lOXRMPc308T4686Er42bElTiho8GA+w2cm9zNayhPWjVUh5AIV1+X1XevY4A+SzVUYmciiaRAOmkTaLPIFm337szVhVFVGk8kCA31BYlGLaNhibrFEsfjw2UTV974kR8X+IRqxoh3tPqM97mMqWSBTSxnpgnLZZTJZwO/TV8aMmlFG9wALQoiLOxM+gkGDkbqNslAqSzdNKaMgsqapqILPzdgE8OZwyDDv/v0cjrP6HHJdFxQKhyijDmQx5/5j3U07FkE0AHwNeNaCXeEryeGGELI9l+ODEf6qdwPr5BP9HmQ6vNLgWN3A9cCLsq6tXzl5gNsyc4gG7phf6Lyz9xAIvVn9nHopyh3INPDTB0t58Y0VEKqnhLb4Q7yjbxMnhaIAw4Wi85bxZO5fcwV7Qtc1nwpsDjquN1gqufsQHOjvCVnHbY5FQ0GDXYUMn53cz0gpT1sNCDnqHL6xex0XRjvRgYlU/sfp+dIwMKjr2lBPZ2BjOGSwZ98ShaJd050xdMHSUoWpVIH+3iDxmEW0zaJUdg4sZCp3livuAV0Tw0KIQWBQCDHpuPyHYYhPJeL+2yxTuyAWs4LtcYvJZIFsdnUYGbqgWHZJJgv4DoWRTeMRImoGtri4M+EnGDAYGc81hNHMzCEwOkuprGZgNAe8UNdFt2VqYmgkW1Nd6rogX3CYSuaJRX20x60qjA4cyzA61kC0VsnnyxacCl9JDXFLHQi5eOAJTgy28Zc9G9jiD6Musrc2oYa6VBD8ufN2Wf/s5AFuXUpLSV5LCbmyTmgFhBzgL5ULSQN37J+Ai/cVc+IbM6Pcm1tE1MmOzdkVtgXCvL1vEycHIyBTu39Zqbg/mp0vTeYL9u26Jn7rON4PCkX7hljE96Od29u/N9ATMtb0Bi8OBozAg4UMn5s8wFApT6QmhDxsz+NNXet4bkcvptDwPD6Wmi3+48JS+cdCcIPP0n/c3xM8zjC0DV2dftKzZbK5OjAyBAuLZVKpIn09AdrjPmIRy+1o91/Z3x368vxi+SfZfOVGXRM3CCFu8Dz+ty1kjnUnArtMUxsEnhqLWaF4zCKZKpBpAKOpZAG/tRzAPqNJGN0FLAkhLu7q9BMM6oyONVBGFZfpmRI+S6er0x9Q32uuSRhlgOd3d/oJ+HSGR3MNYZRMFYhFLOISRmccy8roWALRWhU7uWzervC11DC/WJrFQKuphEquyzp/kLf1bqxuRr1bqZNmsmWfBP7CA76cHOKXS7MENL02hDyXgK7zjt4NPC3auRJC1zY4zg7gc8DTBot5vjE9yj25xbop+nm7wtZAiHf0LkNoSMH1xorq6ZpfLLmVijfdHveld2yJp9f0hdLdicDOtpD5N6ahbdyVz/D5qQMMFnN1IeR4Hm/sWsfz2nur7+djnscn5xZKs/OL5bQQpENBY7a/O3gX8OJImxns6vQzOp5b1d1ajnkYGvOLZVKzRfp7g0QjVrAtZHZGwuZNkbD5UFdHIL2UKaczucqMEMJuC8nWE9PUHlKf94J4FUbTBZYyh3T3HwK9YtFlKnWIm3ZmEzByFUCyQnBRV1eAgN9gdDyL49bOppUrLtPTRXx+na6EP6AyXBnkrKd6thcoCSGe1t3tx2fpjI7lcL3Vj2UagmzWJjldIBqxqsrodGDwWITRsQKidSpO8+y5KoQWZzCEVlMJOR7sDEZ4Y/c6jpM9VncBf9XEBdGtbup3LTgV68qpQW7PzGGK2nH9sitHebytdyNPlxCy1c9oBkKfAS4aKRXEt2ZGubsJCG0JhHl7z0ZOlu7YoAqO3gAyeOq6HusH2ujrDrK2L0RPZ5CA37gYObvopF2FJT43NciBYo5oLQh5Hi4er+9ex/Pb+6ru4ceBT3oelfRCifnFMkJAMGDQ3x2cQfZxPSMcMkKJDj8TU3kKhTowMjXm58tMz5bo6QkQChprgTOCAePBaJs1FgmbdHb4WcxID7q3K1iFzYNK0Z4fj1nheNQiOV0kk6msrowMQbHkMpUs4pepfb9ynSpNwign4KLuLj9+v87YWJ7DK8sPgVHZIXUQRn5kpf9igwdgdZyJLYR4ak93AJ+lMTqew60BPtPUyGRtUgdhlFCZuyHkOvIWiP6Eth65AePytF3mutQIv1iaQRfaqutxPDxcBaG39KyvNnr+ToGhkUQeUAHlV8/bleDnp/bzi8XZunOfK65LUDd4W++GKoQqwNtUHKuenaXiVJccKObEN2dGuSu70Jw71ruhCqEDwDtYsfFVCEE04iPR7icWsapNqM9AlgMc/0B+iSunDrC/mCdWA0K2SmO9tmstL+g4BEJ/j1z6wSogQj2JHwAuikbMUEe7n7n5EtmsXVcZzc2XmZkp0tsTJBg0+pBNpfcHA8Z4LGIRCZu0x3xEwubKn/MgcsDak6swmpoukM3Uc9MckiqA3dXl9yvXqUT9dLur/j4HXNTTFcDv0xmdyOPWmfRYLrukpov4/TqdEkZPBhYawKg6QcAWgqf2dgcwLY2xiQYwythMzxRpazNoj/uOSRgd7SBaj6wsviJtV7guNcrPl2bRqQUhKLguWwMh/qpnQ3VH/API/p1GELKQPV+XFF3H+Mzkfm5bmqNNN+ruHQvqOm/t2bgSQm9vAkLVmNBT9xdzfHN6lLuaiAltDYR5e8+GlUroEAjJp792ePXxM5DtB8fvKmT4wtQB9hVzxHSzJoQE8Jqutbywo6+aGfw48Aml9KgDoiqMHgLOikbM9kTCR3K6SD5vPyyDt3ziTY25uTIz6RJ9vQGCAaMXOEndtMlQ0CAcMle7EXchJyqcF49ZbbGISTJVrB8zKsmYkSp69CGnCzSCUXUgWh64qKdbwmhsov7Y2XLZZSpVJBjQ6Uz4fcAFyPaTuxoc63bARXBBb08A09AYb6iMKkxPFwmHTDral2F0zLhpRzOINqqn+BXpSpmvz4zys6UZDMSqEHI9Dwc4KRTh9V3rqkrobmTH+O1NKKG/A145b1f455kxbliYJlJnNXNFbdv4y54NXCghVFbHuqYJN/NK4Klj5YKEUHZhuXq5lju2LRDmbT0bOEVCaD/wLmRbQD1bhtAD+SWuSg6yp5gjXgNCFc9DB17dtYYXdfRX42GfUCBa3vbaAESoJ/GDwIVtYbOtPe5jMlmo66ZZlkZ6rsTcXInuLj8hqYxOUD+n3viWKozOjcd9bbFYAxhVY0ZJVfTY6W8WRqjrqAhc2NMdwNcEjCoVl2SySEDCyEJuzp1rAka3qd8v6O0JYBga45O5mvEp09TIZm1SM0XCIaMKo1OPFWV0tIJoQxVCs3aZb0yPclMDCNmex0mhKG/uXnbH7lQuUiMIbVZB8FfN2WW+mBzi54szBBssJAxpBm/p3sBFsWUIvQu5zqeexVVM6PL9xZz2z9NjcpSHoGGd0Nt6Nq6E0LuBHzQ41sUH3bEMV00NsruYrQshDXhV1xpe3NG/MmD/EQ6rfWoCRKx4Gp8RiZjt8ZjF7FypvjKyNGbSJWbTZXq6/dWY0SkKNmMNYDQplZGvLRaV2bSlbP0AdjIlYdQlYXQeMt3eDIxKwNN7ugPC8mmMN1JGqto7FDJJdPgs4GnIrSl3NwEjgAv6egMYumBislDzWNWY0fR0kUDQoLPD13msuGlHI4g2qizSc2YqJb45PcpNi7MYojaEKp7LicEob+pez+ZACOQg+Hc1cVFVq1MvnVM1STcvVrNjtSHUphu8qXt9FUIlBYavNDjWBcj1Qi/flc/o35oe5a5c/ZjQvCMD01IJxaqZlb9uEkKfB3ZUldBDhSztRh0lJASv6lzDixOHQOjDrFKA2SSIQNYw7QF2xqJWT3vMR2q6SL5QH0az6RLp+TLdnX5CIaMPOdjuwQYwekDB6Ox43IrEoiapVLEujEoll6mpZRhZKo6TU9dPPfuVevg8tac7ICxLZ3yyGRjlqzAygac3CaNbDsIoiKEJJpKFmiNuTVMjk7OZmSmiGxqJdiuhaeI0FZ/as1LZtkBUH0KfBZ47U5FK6OYGELI9j52hKG/oXlcdg1qF0G+aBN5l83aFb82MccNCikjdmJBHWDd4c/e6KoSKyNaBqxscq11l0C4bKRXEtdPD/D63hKGJ+hDyHwKhfQpCzbpjO+7PL/HF5CAP5WtDqOy5mELjFYkBXnIQQp9Cjr5dtQDT82A6XZTjPOqDqBpQfxB4eiRiRmMRi9RMkXzBWbVQE8BnaUzPlFhcqtDVGai6aSeq2NNoAxglJYx8kWjUIlWnzkhXdUaTUwWCjwxGFeCCnu6AZpo6E3VgpFVhNJknFDJIdPhN4ELkGJG7Gxzrl8j5Tk/p6w1i6ILJqQJ2HWWUzztMJvOEQyadCX9CCJ6klNGuFojq26ZlCNklvjkzxs8aKiGPE1WKfvuRQWirulkvn7PLfC01IgeA1XHHbM+jTdd5Y9f6wyH05QbHSqgYy7OHSnn9W9Nj3JdfRK9XMe3I3rG3H3THjhhCD+QzfCk5xEP5DO2G9TCiCAUhSwj+IjHASxIDKyH0gUZflodgeDyD40LAr7OmN1Tv5SMKIDujUaszEjGZni5RKMrhmavxyLI0ZuekMurq9BEKmf0KRo2U0f0KRme1x61INGKSmi7WhVGp5DKZKhA4CKOnIGt/moGRDVzQ2x3QTFNjYrKA63qrKj5NyAkBE5MF2sLLyuhCYJrG0xd/oWB0fl9vUOi6xuRUviaMdF1QKjqk0yWCAZ1Ewh9ClosMHY0B7KMFRNtV7OS5M5Uy35oZ42cLMw0hdEIowhsOQuhOFSxuxh27Erhszq5w7fQwNyzImFAtJeQod+wN3eu4ONaFyp78TRMQegrwPuANDxUy+jenx/hdg0H3C06FTX7ZO6YgtFcBr5E7dhEyw3j8A/klvpQcYlc+Q8LwrQqhkoLQyzvXVCHkIcsJ/q7RlyWEIByUgfzOdj+Jdj/RNqvRP3tIAfXUeMzqirSZzMxKZbRaYy6AZWhMzxSZW1h2044ERikJI1+kGjOqG8AuKWUU0Onq9JsqqLxE42zrbcp9Pb8Ko8mpghrytjqMKhWXsYk8bW3LMLoYOUv83iZgZADn9fUGNE1Tyqium+YwPVvEsjQ64r5OTROncxS2gxwNIDKQ3dGvnioX+ZfZMW5amEFvAKEdwTbe2L2e4w4qoXc3oYT8yHaLZ8/ZFa6bHuFHcylihlkTQrbnEdENXte1lmcchND7moDQacg0/sV7CzmuSQ1zX34JQ81IahJCe4D3NBkTOgRCD+QzJAzzYQGegxDSeGligJdKCLnqQfD+I/nijgBCK920A8Bp8ZjV2RYxmUuXyKkA9mow8vmkmza/UKbrUBg1ctPuV0rjjHjcikZVzKhear9UdpmYKhAK6HR2+g0Vx1loAka3KpCf39sd0HRD1IeRdhBGkTaTjg6fodRsszAygXP7eoOaEIJkqohtuzVgJMjl7OX6qfZ2X0LXRbUCe18LRAdtPfC2iueu+Z+5Kb47N4VZo1hxGUJKCR0vs2NVCDXaNbVRKYvXztllrpse5QdzSeKGWfMfOJ5HVDd4TddaLpEQyinF8KUmFN7VwBkHSjmuSQ7zUCGLUaNjX05WrLDRF+SdvZtWKqH3NOGOXYQcqtUchFwXn6bx0kQ/L5MQcpA1Te9/jL7vA8o9OC0esxKRNvOQbFp9GFVWumnNBLCXYdQe90WjUZnazzbo2p84qIx05TrN07gi/1b1+5N6ewK6YQimpoo4jlsfRuPLyqgKo8kmYeQDzunvC+qagKlUoQ6MNApFl9R0gXjUoqPDl1Awv03FqFogUl/0i6cqpcgP5pNMVkr4VxnKsuyOBSO8oWftSgg1ExParBTDa9KVMtfPNAehiG7y6odD6IsNjnUecobx0/YUslw3PcKuQqbm2JAqhNb7D4HQviOA0BeB46oxoQfyGTpWCUyvhNBLDkLIVgH79z/G3/k+FTc6PRazOtrCJnPzJXK5BjCaLq5URtUA9q4jhdFUEzAanywQChl0Jvy6Os9zRwoj3dCYSjZQRrbL6HiOaMSio91nIFc4NQOjnyuFf1Zfb1CXyqgOjAypjHJ5h57uAKGg0aXcsztaIJL2LOCK0VJBuyMzx7xdeZgaksWKHjuVEjruYJ1QsxD6DHDFdKXE9dOj/GA+RXsjJWSYvLJzDZfGu1CByw80AaFzkeM+zt5byHJ1SsZpDFF7DXTGcdjgD/Ku3s3Viun9TbpjFypltl1CaHBZCa0GoaIrt8qugFBFwfl9j9P3vhc5h2dbPGb1RCIW8wsVcjlbxaFqwajEwmJlpZt20pEqo1jEJJlsAKOKx/hEnnB4GUbPQKbbm4GRBpzb2xPQdV00hJFtu4yO5YhGTDrafTrwTPV5ft8EjILAmf29QUNAAzdNI50ukUj46O0JCGS27uctEEnbCjxlulIO/jY7T9quYK74wlzPo4LHiUFZrLj1YGC6GQhtqQbBpytlBaEkCaN2TMPBI2aY/EXnAM+Kd6MClh9uAkLbkMPwjx8pFbhqapC9xSym0GouWlx0bDb4gvx13yZ2yi76qhL6v2YhtKvQXEyoqoReLiFUVu7c3zzO3/1u9euEWNTqi0Ys0nMlsg2UUWqmyPziIcroJGTqvnllFDGZShbJ5RxMc5Ws0wrXScFIU2plhsbTG25BZrjO6e0JGLomSE4XV10YsNJNGxnPV5WRrh7QzcDoZgWjM/r7goagtjISQKHosGFdmP6+YBWav2iBSFoZeHLF8wYGi3mGSvnlzIy9XDEt2za2HpodawSh9ahygFSlxDemR/nhfIrOVVLZy9DDI25YvDyxDKFF5LLGqxocq9o7dvb+Yo5rUzImpNeB0JJjs9YX5F29G9l5UAk1U6z4dBV/2r4rn+GLy9mx1SFUVIPaVkCopKDaDIRiyJaUqHJNH41iuBEF4HOjEbOjrc1gYaFCLldZvg5qxYxWKKPlRlnqt4McAqN41GQyma8JoyogRsdztMl6HKHUSpLG6fZbVCLm7N6egCE0QaoZGI3liEaXYXTZEcAodDiMKrZ7yOoo2/GwLI3jtkXpTPhBTmy4vQUiaYvA6RHdOLXX8rPo2Cw5NrYnR2ucHo7x2q51bJUV03ciGz0b+bUbkZ3iL5qqFLkuNcIPFhpDKGZYvKyjvwqhBWSf1ZUNjnUmciPHGQeKOa6aGmRXQbpjtSCUcx3WKiW0oou+md6xpyHbSLY9WMjwRXWs9loxIc/FJzRe3NHPX3QeEYR6gecgt+K+RsVINigwDVJ/3G0AOTvqachesR3qz+sFRYeBNHB8LGp1xmMW8wtlslm7LoxSM0UWDlVGRwSjeNwXjUcsJqcK5AoOplHDdap4EkbhZRhdxsEMV72pm79EZrjO7lMwmp5uHMAeHssRj5l0tPu0I4RRGDitvy9o4kE6XaZYdGThb0VuVjn15HaO2xbDNLV96oE22gJR1RuSN/0JHYbVf2ooSsyw6LX8nN0W5+WdA/RafpQcfnsTEKq6Yy+aqhS5PjXCD+dTdJu+Va8YDzk6JK5bvDTRz2USQvPIFodGS/NORG573TpWKnBlcpB9xVxdCGVdh7U+P+/r38KJ0h07EghdA2x9qJBZBl4jCL0ksQyhorrwGg2RX4ssavwYcFbaLm9wPG+rT9OeCryEg3N1VoPRachVSa9BDpF/ofp1kjr+JLKfazV7QEFuZ6TN7I5GfKTnJIzkoPpaMCodDqNTkLvKmoZRLGYyMSlnJxm1YOR4jI7miLRZJBI+FCAmm4SRDzizrydgCgHTMyXsOjBybJfh0TyxmFWF0bMVrP/QJIxO7e8LmoGAgS7AsnTicYvjt0U59aQOggEddS38C0eJHS0FjaPqJHf6ND2wNRCOnR6OcVygDZ+mzyn4vKdJCP0j8Lxkpcj1KemO9dSBkItHu2Hx4o5+nt3ejXoyf0plk+rZDuTsop3DpTxXJ4d5SGXH6imhPtPPe/s3c0Iggrrx3tkEhC5AtohsebCQ5aqpwfrZMQWhFx1UQssbKpqA0MeBVy05Nr/OzPGj+RQPFaXC67J8aIiLVQbpjlXcuJ8Azy1X3K3VGUOuB4Yh1uq6eL5yH+5Rbt5qtl/dcDsjEbM7HDJZWCyvCGDXhtH8Yrnatd+rgHhfEzBaLnqMxyzGJySMdL0GIByP4dEskYhJot0PgmcrGP2+AYx+sQyj3qAJgunZRm6ax/BoTu50kzC6XJ2b+xsc62b1Xazv6vRHtm6J0Nsb4JST2lm7JoRpajngu8g6uNkWiB5uE0pdDKv7aVJd7N9W6uShBv9+EzJt/rxUpcTXUzIw3WP6Vn10VyHUoVu8KNHH5e09qBvsMzTeZX4Scp/aGaOlAp+bOtDQHcu7Dt2mj/f1bWZnMFp1R97RBITOR2biNu8uZvjC1CC7GkDIEhov7OjnFQch9NUmILRGqaBXV0eh/OPEfsYrRe7PZ7h5cYYu08caK4ghxCUKwksr/v0ngGfPpkvsemiRO+9O85s7Z5lXIOns8GMY2pnIuU+NYDQK7IjFrJ5Y1CI9X4VR/TqjxcUK3Z0BggdhdI+6jurBaAY4K65gNDaep1RqAKORHLGoRXu7DyFhNKIepI1g5AdO6+sNWK4LM+lSXRg5tsfQaI72mEW7hNEVSkHvagJGU1WPIxgwPE0TE+rf/YdyzZMcRXY0dt/vRu6m/zdF7t8i0+eNMlafAl6QqpT4+vQo359P0lsDQtWYUIdh8YKOPq44CKF/Uoqqnp0A/C9wwmipwGem9rO/kKubHSu4Dl2mj/f1b65u2xhBjihpBKEnI/egbd5dyHClglC9BlZLaLyoo49Xdq4BWQV+jQqCN1JCHwNek6yU+E56nH+fHafP8mMKDZ+moQmNOzJznBSK0mv5KyorVN3B3gH8b6Xi8r3/G2P33kVKJRe/T2dxscyBoQymodHV6ccwtLOUOri3Doz2IdP7Z0YiZmckYpKeUzBqos6oW9bJ9CJnODeC0R+Um3Z2PO5ra483B6PBkSzxgzC6Qn2n9zeIn/1CZbhO6+8LWo4Ls+kStu2t+pmqbtrQSI72dot43CfEQRg9WOdYnnov/4lMz+9WD7NPKlfRPtpu+j+HldPVmNDzUpUS16dGFIT8Nb8l5+EQSisV9A8NjrUOuBHYNFUp8qnxfRwo5g8pNzjciq5LwrR4f/9WTpJKaBQ5trYZCH0L2LS3kOXzU4M8mK83ykN20b+4o49XHITQ15qE0EeB18xUSlyTHOZ780m6DnNnBRDQdXpMHycEI9UFldcia2aeCzxvZCzHHx5YwDS15dSxpgksS2f/gQyGpdGVWIaRoVyaWjAaUrHDndGI1d4WNkjPl8nnGymjIvMLJXq6lpXR6ch6mUYwSgLnSBj5GB3PUyo7h2SdVgLCdTwGh7PEYxbt7RZCiCuUyn2gAYx+rlzUU/r7gpbrwcxsfRjZjsvQSJaOdj/xmKUJwXOUcnywwbGq1/Z9yuM4au1YB9EmFcu5YsYu842ZUb4/l2pOCbUvQ2hW/YxGENquXJzTx8tFOXK1lFu1FaVqJdel3TD5wMDWap3QGHKzx48aHOtclYnbuLeQ5XNTB3iokCVuGDUhZKwOoXc34Y59HHjNnF3mO7OTfG9uik5z9eyijmBLMMRJwahQbsZnFIheCZzz0J5FUtOrx6J9Pp19+7NYpqgqo7ObgNH9SmmcHIvJrv10utQEjGRvWk/3MozOVAH2qQZuWhI4Lx63wu3tFqNjecrl1adKaprA8+DAUJb2uEV7fBlGQ03CqA04pb83aDmuR3quAYwqHsPDWTo6LOIxn1Aw2qvUjssxbscyiLYoeDx3plLiWzNjfD+dpMusl6KHDsPkRR391ZhQ+ggg9CPg9MlykY+P72VvoT6Eyq5L/OEQejNq20YdO0e5pRv3FXN8bmo/DxVyNQfdVzwXHcGLE/0r3bFmIDRQdcdSlRLfTk/wb7PjD1NCh9wQAi6OdrHBH7SB76lfmspUPT2TsxkcytQceub3aew9kMUytSOB0R517k6LRa2OcNhidraJ3rTZIvPzZXp7AgQDRg9yWcGdTcLo3HjMF+5o9zE8mqNSqb2xw/Ng/1CGjriP9piFEOI5TbhO1ThOBNjZ3xf0OXZjGFVsj6GRHB1xH7GYKYQQz1Xn55iH0bEKooOziyplrk2N8N25qbo3kQu06wYvOAihWWSN0KebgNANwMZkucTHxvcwWspjanXWC3kuUcPgQwPbqhAaB163IqZSy85WwcSN+4o5Pju5n911IGR73iOFUDUw/dpkpcT10yP8T3qyQYkDbAmEeXliAEvTXAXv6pCtHPCmzoSf/QcydcfBShhlsCxBZ8KP2TyMksD2eMzqCIUMLT1fplhnnpHPqsKoRG9PkEBA71GQv6NBoPZ+BasnxWNWuKPdx9BotuaoDQkjwf6hLB1xH3EJo+c26TrdjCwWPbG/L+i3HZf0XLmhMhoazZKI+4jFLKHAt5ejePrinyuINiJ7pC6ftyt8Y2aU76an6LHq30TtusnzO/p5zqEQ+lQTELoR2JCqlPjI2G5GywWMOjvOKp5HVDf50MC2ap3QFLKu5qYGxzoLmTXcsL+Y4zMT+9hTZ+9YddvGSx6ZEvoE8JpZu8y/zIzx/blkTYh76k46zh/iI2u20yYXCvweWdNVtSng1UBs3ZoQY+N5CsUGMNqfxWdpJA7CSG8QwH5QBVpPb4/71nR2+pmZLak6o9owmpmVw9WUMupWbm8zMJpArioKJdotBodzDWAE+wczJNotYo8MRjsH+oI+227STRvJkujwEY1amoLRnmMZRscaiKoQerYc7zrKd+cau2PtuskLOnp5TntvFUKfb0IJbQN+CqxLVUp8aGw34+VCXXfMVgPUPnIQQkngtern1LMzgf8C1h0o5fjMxH72NYAQwMsOhdB1yMLIetaPrDh/ddou853ZCb4zO1nz/FWLPbf7w3xszTYiulm9SS8Fsoe9/Abgcr9fj21YF2ZkLE+h6FDrdEkYLWEdDGCf04QymkVmuE5pC5udgYDOTLokj1Nn0uPMbJG5ebmqKCBhdB5ybEyqzrl6QCnZC+IxXzDR4WNwKFNzImIVRnsHM3R2+B8JjOLAiQN9QZ9jK2XkeKtmYqtu2uBIjs6En2jE1FR86phVRscSiNYj2xOePVMpcf3MCN9vMMrDxaNdl9mxI4TQdqVg1koIPcRYqVgXQo7n0WYYfHRgKydICE0jWyTqQUggd5b/N7B2sJTjHyf2sa/eGmgFoZceCqFrm4TQJ4DXzNglvjk9xv9rAkLbAmE+umZ7FUIPILvQV1MTcyqOdrnPp8c2rgszMpalUHTrwEhn7/4MPkun8yCMGimjvSogvLU97muPRkxzbq5EsejgedSG0VyZdLpM/0EYPQnZZ9UkjKxgosPPgeGMnEstVoeRQLBv/xKJhJ9YdBlG1aByPUD8DDnb/IT+vqDftpt000aydHT4iElldMzC6JhbOZ2ulLlW1QnV66J3VdvGCzv6VrpjzaTotykIrZmulPjg6G7GysW6KfrqKNmPrdnO8cG26rFepi6uehA6DVkrtXaomOfTE/s5UMzRVncNNLyscxlCBZpL0fcha0heM2eX+deZcf57bqouhPA8tgbCfGzN9up+twc4ONK0ls0hyxKeY/n06MZ1bYyM5SgWHagLo6XDYWQhU+75GsfZpxTgKbGYdfz6tWHm5sqk50poWg1lZMoZ2LPpEgN9Qfx+vQtZLHoL9fvgHkCWXDwtHrMCnQk/+wczNedSCyGj+nv3LdF5EEbPaxJGNylldMKAgtHcXJlKvQB2RRZYdnT4iUVNTQhx+bEIo2MBRP0KQpfP2xW+OTPKDxZSdDQBoRXu2JxSQo0gtEXJZAmhsYcYbwAhF7nZ48NrtrJDzkmaUxC6qQGETkEWRq4ZKuX51OReDhTzhOtAyMHj5Yk1vKJzDUJCqFqsWK/KtlfFwl694FT499lxvp2eoMuoE1NbhtAh7tgzGkBoFRhp0Y3rwgyP5hq4aQpGvmUYVQPY9WBUtZN9Pj3m92laeq5cN5tmGRrpuTIz6SL9fUECRw6jC+Mxy9/Z4edAPRgpSOxeHUaNAPGzlW5apeKSnq+vjCoVj5HRrIRRxNSPRRgd7SBai2zOuyJty+zY/y0kadfrQyi2rISW3bHPNeGOba1CaKZS5gOjuxkv13fHPCCsmXxizfbqxMgF4KVNuGMnAz/wYGC0VOCTE/sYLOQJ6/qqcHA9DxuPv+gcqEKoqNyxRhDqUZ/71QtOhX+bGZcp+qYgtJ2IbnrIzFizEDocRs+1fHp0w4YwIyON3bR9+zMyZtS5rIzMBjDahcxEHheLWWvicZ8ux846B9XJYWYY1fXWRQb6l5XRBciq53QDGA0DF8Vjlr+zU2YI3RruYBUSu/cu0dV5CIx2NwmjDuCEgf6gT7pppSZglCPR7iMaNXUhxLNXBLDdFoj+OCV0DfDsOQWhH8xLJeTVcZHipslLEv3VYsVpZMFeoy76LQchVOKDY7sZK+frZsdcIKzpfGLNcdV9aovI7vSfNIDQziqExkp5/n58L0PFPEFdrwnWsufxys4B/qJzAA1RVOflPQ0usG5ku8qr5+0K/zI7xndmJ+isAyHXkzGhjw1sI2KYngqyXnyEEFoJox8Az/FZenTDhjZGRuorI59PkzAyNbo6AxiGqLppd1G7az+NHCS3PRoxt/f2BER6rsTiUrnmeusqjKZni6zpD1Vh9FR1DdSD0S5kjdAl8Zjl6+z0s+9ABhrAaM++JboOVUZ7lGJp5KZ1KDfNV6l4zDWEkcvww2G091iA0dEKohiyO/i587bNdalhfrSQWrXRc6VqiOkmL070c3m8p6pOPtcAQkIpoZ8Ba2ftMh8Y281YqT6EPMAnND66dlt1bG0GeEETSmgn8CMPBsbKBT4xsZfhcp6ApteEXcmVEHp5YgBdiBKyuvtvGlzEnVUILdk2/zY7zrdnJ+rPY/KqgeltRJuAULni4rje8q/V+rJWwOh5PkuLNKOMLEtj32AGy6dgpIuzke0kd9aBUUkB5Ixg0EhEIqaYmam/3towNOYXyqRmSlUYdSJHrfxMve9a9hByasKl8ZhldXcG2Lt/qaYCO6iMFunqOgRGDzUJo3bgxIH+oK/iQDpdrB/Atl1GxrJ0tPuIRi1DCHGZiqkd1TA6GkHUjRxZ8aJ5p8JXU8P8SM2YrqeEqhC6or3HQ84T+mwDd0wgCyMlhCplPjS2m5FSHrMBhAKaxj+sPb4KoRzwvCZiQicAN3rQP14u8PGxPYyUC/iFXvM4JdfhlZ1rqhAqq/PyPuo3LXYim3dfveBU+NaszI4lGijJZQjph0DoYf1ZFdulVHb48S0TDI5lODCSYXA0y4Y1YVzXW603a/4gjHQFo/rKyLI09h04RBlVYfS7OjAaQraDnBNpM2Pt7T4xPVskl7drbpQ1dI2FhTKpmQJrB4L4fMsw+ql63/VgtA94VixmmV2dfvbsyzSG0Z4lursCRKImmhDPP0IYyTojx2vYKFupeIyM5eho9xONmYYQ4pnIMoKjtgL7aANRD3K8xHMWnQpfSQ5x4/x0Qwi1GyZ/0TXAZVIJTSm35eomIbRutlLmw2O7GW4AIQBLaHx63fbq2NoCsuHzZ01A6Mce9E2UCnx8fA9j5QK+GsfykGNDXtW5lpcl+jGEVlGf52+RQ+9rWYdSga9K2xWunx7mf9PJuufP9ly2B8J8ZGA7MeOQmNAhELIdj2LZ5ce/HGdkIodpyMZWTRNoAvYNLXFgJMOGtRE8j8PdouWYkc/S2zasD4uh0RzFOkWPlqWxf1DCqLsrgH5QGf0WGSNbzapP/rMjbWa8tzso3bTF2m6argsWFiokZ4qsXROqwuhC5WLP13keVSHy7HjMMrq7/Ozdt1Tz81Qh8dCeJbq7/MSiPoTg+SugZjfhpu0c6AtatgMzs0U1kL8OjMZzdLT7iEUtUwieqY5zVMLoaAJRh4LQs5Ycm6uTQ/x4YZpYnTohW0HoFYk1XHpw0P2VTUBos/py18/aJT44vofhUr5uYBpAF4JPrjmO7VIJlZAjGW6qEzAWwPHATzzomywXZItIuYBVB0I51+a1nWt5aWIAU2i2+jzvR873rmVx9dlfuWDb/PPMGN+fTzY4fy7bA20rIfTQ4RByHI9CyeHHvxxndCIrAVRjfo6mCfYPL7FveJENa9vknx987UEY+fTIhvVhMTKao6A2va4KI1PCyDwURn5kZXSpxsc6gBwKf3IoaAx0JvwiPVdmYbFcy31chlFqusj6tWEsS+tUivBG5eLX+qqqfV5XRKOm3t0VYHcTMNq9Z5HurgDRqLUSRo2U0U/Vd3zyQF/Qcm2PmXSpAYzkdpB4u4+4hNGlShk91CDJ8YQFUReyHuaK7DKEZojq9Vf+tBkGL+0Y4DI5WTGjXLFm3DEJoUqZD43tYaQJCBlC8Ol1x1WzYzZyVOjPGkDoOHWsvslykY+M7WGsXKwJIYCsY/ParrW8pGMASxMOsnThA3VUwEoIvWrBrvDNmTG+OzdFrM75q1SV0JrtxA+F0ASA63rk8jY33jLO+FQOw9Dq3mDLH1pIIB0YWWLP4CLrB8JoQlT/bbXosQojRkbzFAp2XRgdGMximoKe7mUYBZCV0bVglFK/zg+FjLZIxBKpGbk3rZ4yWlwqk5ousG5NCMvSEwpGP1KJiLowEkI8Nxo1tZ6uALv3NqOMFunu9hONLCujXU26aTHg5P7+oGXbHrPpIo5DQxi1S2VkCcElKsb14NEEo6MBRB3APwOXZ12HLyYH+cnCTLWIriaEwrrOyxNrqtmxGaUYGm3bOFQJje1mpFRoDCHg79ceX4WQi2xx+FmDL/J49RTrn6wU+Mj4HiYa1CQtORVe17WOl3T0Y2mapz7PB+rERUD2KV0FvGrWLvO1aRlTi9Y5f2XP5Th/Gx9es7263+1B1C4tz4NsvsKPfznBeDKP2SSAVgOSrgkGR7PsHlxk/UAbuiw2TB8Co3UhRsbqKyNzGUYaPV3+KoyCyMroWipxr4LredGIGevu8ouZdImlTGXVGUMgVwgtLlWYSi0ro5UwaqSMHhJCPD8SMUV3l5/de5dqQu9hblrMRAjxAvU9NAujUwf6g6Zje8yki43XW4/naY9bxGOWpT7TEAeblp/wIGpHzt15Zs5x+FJykJsWZqqNlTWf5AnTxxu61leXH6aRe8euaXCsTUpqb5q1y3xwdE9TENKF4JNrj2dH8BAI3dwAQscpUPVPVYp8eFRCqN6xFp0Kr+9ax4s7+vHJzv4r1eeqV9AXUYrplWm7zHWpEX7a4PyVXJfjg218aM3WalFoFUKjS9kKP75lnMlUHsMQPAL+rAIkeYMPjWV46MAC6/rDGIaWVt+FglGY4SZgNDiUxTAEvT0BNG1ZGf2qTtxsP7KReF04bO7o6wkwv1Bmbr62m6ZpgqWlCpPJPBvWLbtpz1BuZS1l5CqAPCCEeGEsatGtlFFTMOr0KzdNvEDBs1GG6yb1ADptoD9oOI7H9Ex9GJXLLuMTeeIxi3jM8qk42CiyWPUJDaI4cu7OpSXXFVenhrlpcZpQnZtINpWavCTRxyWx7mph3z8gG2Hr2UZ14W9dmR1rBCFNCD61djs7ZO8YwCUKMPUukm0KVH3JcokPj+1hsgGEFpwKb+xex4s6+qvrtr8AfISHN5autDCyxOEVGccW35we44aFFOGGEArzwYGtJAxfNfvzLGBkKVvmF7+Zwmfpj97FpmJI6/pDGIY2q76T5/h8emT92jCjY3ny+druk2lqDA1n0XVBX28ATRNnIacd3lYHRjnl1j4tGDDCoaDB5GSBUsmpeRxNEyxlbSan8mxYH8Yyl2H0fxw6p/twGO0D7q/CqKfbz0N7GsNo995Furv8RCPLvWm7mwgqV7v2T+3vDRquC6npUt1VReWyy8RknljUJB73+ZE78kaQxZpPSBDFkFtRLym4jvaFqQP8ZHGGoKbXhVBQ03ll1xqeHe+pFvZ9HDkl0G0AoR8C22btsvhwkxDSEXxy7XHsCEYQ8udfiJys1wyE+qvNshPlInqdY807Zd7YtZ4XJvqq9URfUEqo3pzukApgv2LeqWjXpIa5cT5VF+KFqhLq30bC9FWV0GUSQhV+8esk1qMIoZU3xP7hzOHK6Aq/X4+sXxtidDxHPu/ULUYcGsmia4Le3oDQNHEGctrhLXUyT/uVG3d6NGL1yLnUOUolty6MMlmbyakCG9eHMU0toZRjIxjtB3YJwQsiEQWj3YuNYbRviZ7OQHXg2RUcrMCuN5f650BECE7v7w3qrucxPVMfRqWSy8RUgWjUoj1u+ZHlCqOPN4yE5z3m8aoIcjOHgtAgNy/O4te0Wn2RVDyXmG7xmu41XBjpRBdiCbk366vUT3tuUf795tlKWXxkbA9DpVxdMHjL7th2dgQiCPlHT1cXej0IbVUXRl+yUhIfGt3NZLlQs4YFYM6WSuiFHX1VCH9ewXWxznECyg19+ZJj69emRrhxPkVA12uev4LrsiMY5gP9W+k6CKFnA0NLmbL3izuSWKYWVcd+w6P43d+LHEPyU9vxnIvO68Pv16tB/Z8C/QuLZX5wwzjpufKqO8aW1V3Z5dyzEpx5WgJdFzayxupv6gSwNaXC/8nzeM3sXIlbbksxMprD56tTQe96dHf5efalAwSDhqcAcTH1VxX5kAsqv+O6MDya5f9uGMfQ6z/8XMfj2c/sZ/26MELWjb0KOR6mXszIRDY0v9N1PfPOe+a45970qiunl2Osjkc4bHDBk7vZtKGtGt54m7ovnzCK6N+By1zQr5oa4mdLM3UhZHseIaWEnhHtQhcioyB0TQMIbVQQ2jJrl8XHxvcw2CSEPrXmOI4PtqEhHHXx7msAoS0KQgMzdkl8cGw3U5XaEBJAegWEQgeV0CfqBEVBpq6/Cry85LrGN2ZG+eF8si6Ecq7NCYE2/m5AQkhICF0BDM4vlrzb7kxhGFpCCD4NvFHdROaj9KsfeD6wR2hi3+BIxu3vDWKZ+qyKe1zh9+ttaweCjI3nydVRRromGBnNoWvQ1xvUhBCnKmX0yxo3rqeC/geEYEcwYKyPx0ymZ4pkc3bNALYQgmzWZmKqwMYNbUIpo2cgG5ZrqVZnpTKKRky6OwMN3TSEagfpDBCPWbpym/c3yHC5yHKFoBDi7N7ugIYHyVSxZlNuVRlNpaQyisesILL5d/zxUkaPJYhCyJEXl5U81/jc1AFuXpzBJ+opIQ+f0HhV11qeFe/GEMJRN+uXqF/Yt2mlO/axsT3sL9afMe0CJoJPrNnOjlAEDeGpC65RzcUmZMPkQHVsyGSlQK1PJYBZu8ybutbxwkQfIc1AZb0+Tv1qXh+yxOHlOdcxv5Ya5ofzSfxaHQg5NieGorx/YAs9ph8hP8tzkLU2Xrnisn8kg2lqJyBnPVmPtgJXQMoJuLlc8UpbN0QwDA1k5vNnwLMDASOyZiDE2ESObHb1tT5CyBtqZCyHpkF/b1AXQpym3P6b6zw4ZoExIXhaOGxGIhGTkVHpptUKYAshyOYqTEzm2SRhVI0Zfb8BjPZWs2nRmEl3p5/de2u7afLyFOzbn6E74ScWs0zlDh5okOFyVJwsqGnirN6egIYQDWFULLkkk8swCiE3x4w9Htm0xwpEIeD/AZdUPNf4/OQBfrE0i1UHQmUVE3pd91qeGe/CFFoWWTH9lTryG2SK/v+A42YqZfGJib3sL+ZqbmCV36JHQNMVhNqqEHmGAozX4Fg/RzXLfmhsNxMNIDRjl3lT93pekOgjLCH0BeWu1OtvspDd9i8tuY5VXaNtaTpajeNkHZudwQh/O7CFXtNfDUw/T6k7b36hzK/uTqn0PGuANz2G191xwDcMQywMjWXo7Q5imTrIJuWbJYz0yJr+IOMTBalY6sBodCwHAgb6QroQnKJUbC0YeSom8v+EIB6NmKesWxdmaclmZrZYheKqMMrlbCYmC2zcGBamoXWq5EUjZbRvGUZRS8FoqQGMYO/+DF0JP3EJo0uagJGNzCAGNE2c09MdENBAGQlBqeQwdSiMnoSsJXvgzw1EQeSit2dUPNf43OQgv1yarbmQsKqE/JrgtV1ruTTWhSm0WeTIi39uAKEtyM0Sx8/aZfH343vY1wBCAD2mn/cPbOH4YJurIR5Cjne9qYFvvlld7GurHfsT5WJdCE3bZd7ctY7nJ/poOwihT1K/49tUEHqZ7XnWddMj/N98EkvTVj2WAJYcm52hKH/bv4U+y1+NCT1fBT89kE2r+4eXME0N5DD91z2G152hFNh8qeyydWMU8yAAptUD4LJAwIgM9AcZn8yTzdaH0dh4DiFgoD9kIMestFO76t1VAed7hRD9wYCxo7srQDZXYXq2VDM2dVAZFdi0oU0YB2H0PWpnOB0OFj0+Pxq16OoMsHvPEcGoWvsz3AAQFRWUtzRNnNfTHQABU6lCnUFuCkapApE2i/a4FUaO0p18LGH0aIMoqGJCz5QQOsAvl2bRhah5w1Y8D1MIXte1jkvjXdUq5E+om7ERhP4HOGGmUhafnNjL3mIWvQ7wHKTq+ruBLRwXaEOTl8DLlYvQPIRGdzNRKdY8jgBmKmXe3LOOF3T0Vet8voAcWDbb4Ia9Dnip7Xm+a1LD/HAuiSFWb7MQwKJjc3Iowvv6ttAvIbQLeKEKssog+UKJ39wzraqll7OYL1Lf12Nh31MPp4Kua4xMZOntDGJZyzBKqVjPs4IBIzLQF2R8Ks9SpoKha6veuELA2Lis++zvDxpCwqiD+i04S0BKCJ4a8OuxtrDF2ITM2pnNw6hLQeJ/m4VRLGrS1eVnTwNl5Hke+9UM7Hjc8gEX0TjdXgF+A5iaJs7r7vKjCcFUsjGMUqkikYhJPGa1ITeeJHmM6oweTRAF1MV2acXzzM9NHuCWpXRdCJU9F5/QeX33MoTmkdsivk796uKtyLnPJ1YhtKeQqQuhyvKg++0cF1x2x56tIGQ3iAn9HNWx/8Gx3YxXimh1IDRdUe7YwyE00wBC1wMvdaoQmk/J81cDQguOzamhKO/t28yAFUBdsC9WbtnB2FHe5sBopqqGQBZN+lXA8rGwV6inuycEFEsOG9a0HV7DlERmKi8NBo1of1+AyakCS0uVVd2n6g02PpEHvKoyOknBqN6MqEngZiFYGw4bW9YOhMjlbZLTxZXn52HHyuQqTEwU2LShDcPUulcEsOvBaPdBGFl0JQLs2VcPRgLXVTCSyqjZdHsZ2Y9n6po4t7vTj6YJJqcKeF5tGBVLDqnpIm1tJu1xqw253mrqsVBGj1b63o9MO15ie57x2an93LKYRhPUdCeKnktY03lD93qeFk3gE9q0ign9ZwMltFW95qTpSolPT+xjdwMIuZ5Hp+nn3b2bODHUhpCq5HXITRR2AyV0E7B+xpZKaLzcCEIl3tyznue399Ime7++gOyHm673gFDwfUnJc31fSw1z4/w0QmX1akMownt6NzPgW4bQS1e7iGbnitz2uxR+n77yRyRUDOnJDc73H3tdfBa54nlZcZbKDk87t49I2Fzto52mlO7a2XSRG346ycxMEZ+vxgwn18Pz4KzTE5x9ZqIK2WaWC+xAlk9cPDtX4le3T7N/MIPfX2NMi7ptujv9XP6sfvx+A5obqWsqhfrvrusxNJzlRz+ZqDnetvqZdF3wzIv72LB+eRzx25A78OpZBJlhfm+l4nLPffPcedcsnufVhJ/rekSjFk86p4tNG8KoTNr7mjjWUQeiFRCS7tgtS2k5x7fGmS55LkFN541d67gw1okltEV14fxHg5tiG7L24ZQZu8Q/ju/jgUIGQzSqSTL5h3U7WOcLICR4zlI3R6OapJuAddOVMh8ee4ixcv3A9HSlxJu61/P8jr5q71wzENKUEnrZTKXsu356hF8tzeHh1YTQvF3h1HCM9/RuYo2E0P3IDSJ/WDVt9HAQrczMhXn0xkRoq8XD6oCoasubTmZmS/z4pkmmZwr4fTVG67oeHnDWaUcEI6FiI9/wYMv0dJGf/XKKZLJQE3pVIHV3+7n80oEqtB5QLlSyAYxeDPzrShiBoNbeTsf1MHXBJRf1s1ECIq28hUaAiCL7Fd9brrjc+/s5fnt3GhrAKBa1OO+czmqd0RhyBM2jBqM/tWtWdceeaXue8bmpwboQEkDJc/BrOm/qXs/To534tOXA9HeagNB/AKfOVMp8ZvIAuwpLGEIgasafXNp0kw8ObGVbIFwtVnyeikc0Kla8CVg3Uynx4bHdjJXq1wmlKiXe/EdAqOi6vutnRvj54kxdJTRnVzgtHOXdvZtY6wui4PPKWhACqFRcBseyKwPEK92HArIl4tH4tap7bdse2zdFa2aslPv0G+CiUNCI9fUGmUoVWFiqrOo+CSG/2YmpPJ4HA/1BEzkds7MJN+0PAs4KBY1EZ0IucVxYKNdx01B1Rnnpph2MGX2/jpvmKnd5rxDiebGoRaLDx74D2ZorkTQhsB21PqjdIh73VWt/JhrEcUrI6Zaarovzujr96JpgciqP69USB4Ji0WFmpkRb2CQet6LqYTD9aMWM/tQg+mfguTae/rnJ/dyaqQ+hvOsQ003e3L2ep0kILSKrY79N/bEX25F9aqelKiU+M7GfXYUltDrxp6LrEtJ1PjCwlZNCUYSMz7wCWW/kNQDej4H102qA2mipULMwspodq8aEFISuRI5unW5w/r4OvCznOr5rUsP8fGEGgagDoTKnh2K8u38z6ySE7kNW49632g/P5Cr86q4U0+lizVjB42G6LpiZK3JgZIm+7uCqAWl1w90BXBgKGrG+7gDJVIGFxdowcj2YSsobbk1zMHLV0/8OIVgfDpmburv8LGVtZmZKdWFUbQfZtD5UjRld2CSM9gkhnhuLKRgNZmrDSBPYtlwf1N7uoz2+XPsz1QSM7gL0KowMXTAxuTqMqvVMxaLD9EyJcNigPe6LAqcrJfaHoxlErwZeW3Cd6OcnD3BbJr1M8tVuooLr0m5YvKlnPRccjAlVlVA9CB0H/AtwRrJS4nOTB3igsFQzCC5UYLrP8vORge2cGIogpN/7PGSa2GkAoR8BG6erK6ebgNAbu9athNBVyMbceov81iCHwj1/slz0XZMc4bZMernSe7XjpO0yZ4bjvLNvE+sPKqHXIDelHpoaylb49d3TJGcKZHMVKo5bt/XksbZq1qZcdpmdLzE4mqGnM7iaYhtXT/enhUJGvLc7QHK6yPxiGWsVSGhC4LoSRp7rMdAfagZGnlJG9wnBxnDI3Jzo8LGwVGFurlSnzggyGZvJZIGNG9owDa1HuWj/R+06o0NgFI9ZJNp97B9cwvNE/V1mo1na48swOk+5gvUAUUSO2tV1XZzXmfCjG4LxyTye+7CJmsswKhRsZtJlwmGT9rgVQ27WuZtHtlDhMQHRR4Adty7NWTcupMi7zqoD6CWEHBkT6l7PU6MJLKEVlA/6bw0gdDzwLeDMZKXElZP7+UO+PoSKrkubYfDevs1VCFVU/KRRseJ2dRFtSVWKfHRsD8N1xob8ERASyCLNF+Qc27x+ZoRbliTE60HojHCMd/YuQ+j3yNqne1e+dmGxzB2/lwDKZCtUbBdd144aJXQ4jGTrgUOp7JBeKDE0lqGrI3C4EhlXN9RTQyEj3tMTIDVdYG6+sjL9f8iN67owlSriuUfkpqWUOrosFDKC7XEfyekCizXcwYNuWoWpqQIbJIyaVUYPAoNCiOfE4xYd7T72D2brKqPqLrP2uEV73BcGzqVxur2olJHQdXGeXPUtGJ/IrzbeV5VFSBjNzpUI+HQ6OnxdKgTzv0criK4DQv86Myb2F/OrBoyr7ljcsHhzt1RCltBKyh37VhMQ+iZw5lS5yBemBrmvDoRQwIvoJu/r38xp4Vg1MP0SpXIaDTX7LrAtWSnxsfG9DNWZZ11N0b+x+xAIfaEJCFXdsRdnXdu8ZnqYWxbn6saEJITivKN3ExskhO5F9ojdXX3d3GKJO++bJTVTYClbwT6KAVQPSMWSw/xiieHxLJ3t/pWqZ0x93gvCISPe0+VnZqbE3Hy5Nowcj6lU4UiU0crYVH84ZGzq7vSzsFghnS7VTe0vZSpMJYvVrv0eZNP0Dxooo10rYdQe8zE4lMFxVw8qa5qgXB0FG/NVCxHPUddbI2V09+EwmphQbloNGGUyFUxLY/PGNk2dl/84WkH0KUD899wUqXKpOtzrYRCKaCZvVCl6S2gLwHsVhOoNANuhbtizJislrkoO8vv8IkaDmqQBK8D7B7ZwaiiGkHGT59B4lMcOlaE5LlUp8rGxvQyWcjXHu1aLFd/QvZYXdfQfCYQGVCbnxalKyfe11Ai3LtV2xzRkj9qZ4Thv793IxkMhdBdAer7EXffPkpotsrhUxnZcjGMEQKvd0Loma1sKRYeFTJmRiSztMV+13mgMuAd4Sjhktnd3+pmeLTE3Vx9GsuXhEGXUQe01UC6ygPA+IVjfFja3JDr8zM3L9dZWnXaQTKbCVLJwOIx+2ASMhoQQz2lv9xGPWQyNZHGc1WGkq+bV0TE58Kw97qvW/swcKYx0Q6urjBzHIx7zsXVzpPpeKysffkcTiF4KxLKuo+0r5ih7B+MQAsi5DhHd4M3d63lqLIEltDKyPuGb1C9W3KFu2HMmy0W+mBzi97naEBJA3nFImD4+tGZrdbxrEri8VhD3MCX0n8DxyXKRj4/v5UAx9zCorjzWbKXM69VQM1Ws+EUF5ekGMaGrgeelKiXj+ukRfpWZA682hKbtMmeF47y9ZwMb/aEqhN4E3JWeL3LvA2mSMwUWlsrYttv0jOljAki6BFK+4LCYqTA2mSMW9eGz9FF1Hp4cDksYzcyWmJsr1YSR43gkUwUc12ONVEYnNYBR1U27B+gKh4wdiQ4/C/MlZufLq8WxDiqjpQpTqQIb17dVYfQ0ZK1avXlG9wPDQnBFUzDSBaWyw9j4ITA66whgpFVhZNZx0yoVl/Z2H9u2RFCu5FXU74983EC0FThhrS/gGykVGCxJGDmex6Jt02n6eUvPckzIVkqoUcX0iciO83Mny0W+lBzi3lx9JZRzbdpNi/f0bWJnMIpSWq9EDlunAfC+A5wwWSny9xP72FfMVScmrg4hu8xru9by4sQhEPpkE9mxrwHPLbqudt30CLdm0g0hdHY4ztt6NrBJQuge4C3A72bnS9y7K81itiIV0J8JgOoBKVewmUkXSbQvw+g+4EnhsNnR1TSMDlFGJyN70+rBaFbF4nrCYWNHIuFjYb7C3HztAHZ17Gxyusj6daEqjJ6CzMLWG8h/PzAqBJd3xH1EYxbDw/VhVCw5jD8cRtNHAqNO5aaNjudxHW+55862PUxD47jtUfp7g1U1+tmj1TV7EDjJp2mbt/hDaAjaDYs1viDntMV5SaKfc9raMYW2tAJC9dyxnciZQ+dOlItcnRzintwiBrULI8uey0Z/iPf2bebUUAzlhr2O+nvHqsf6N2DnZFnuot9bqA0hkPU7r+1ay0uOHELXAS9N22Xj2qo71oQSWgGhu4G/Au6cnS9y7640haKDZf55AqgWkEolh5m5Ih3tfvyWPqJuuPMOwqhIeq6MrwaMbNsjNVM4UhjNqfMfDYfMkzsTfhYXpZtWD0YLSxVS0wXWrw1jmlovMuX+0wYwug8YQ3B5ot1HNGIxPJrFrgejoqNGwR6RMiocHsAOhw2S0wXm5suUSi7BoM7ZZ3ay47gopqlVixv3/Um/1z9xZfVxyFlBT190bPKOzIyHdb16s44gqzz/m/rFiiceDqG7c4vodYK4WcdmwBfkwwNb2Sxv2AnkPvNGJ+wkFaM6ebJc5JPje9lTzNZcAw2ykvnVnWt5SefyKI8vIRtzZxq4Y59SENKvS41wq6qYNpqH0F3AW4Hfzs4VufdBCSHT0E5TMacyTwwLAL8uV9xkOGhw+s4E0TYL9X1fA2ydSha4+ZdJJqbyhEMGq13mjuNh6ILTTu3g7DMSVYVwNbK1qJ6tRc6PetX0dJFbbpeTHmu1g0hV4TLQH+KZF/cRDBogyxBejOy5q2UaMht6HcDuvUvc/MsklYq76oQAIaBUcgmHDJ5+QTebNkaq98H7kUsq6llcAeZvHFe6r4W8AwL8Pp2uruVkwb8ga9U4mkEEMu39LGSP0LOR5ey7lCr5EXKAk9dAnVwNPGmsVOCa1DC/yy5iCGrW72Rcm27Tx1/3buLMcLzqjr2cxinGnQpCp0yUC3x6Yj8PFTJ1Z2fP2xVe3bWGlyb6q0PNrgY+Sv0ueg1ZpPki2/P44tQgP1uUzFpdCQlSdomzw3He2rOhCta7kP1Fd8zMlbjvwTT5on2qaWh/ox4AEerXRP05mancgy+UK+5/h0MGp5+QIBpZhtFXgW3LMJrMEw6vDiPbli7IGad2cJaEUQm5lKARjNYoGL16eqbILb+aZngkSyBQ+9qpVFzWDIS49OI+QhJGdwAvQ672qXmPKlV/LSB271nk5luTVMpujeZfKJZc2sIGT39KD5s2toEsefiAgkg9iynw/Z2Kmx1yqpTi/w/kwLejHkTVG69X3SC6CvbtQ25UaKROvgw8aaxc4JrkMHdlF9DF6tXF1f3wGwMh/rJ7AyfJbRt3qyfATU0c6+vAaRPlIp+e2MuD+SwhvfaFtODYvDIxwMs7B6qw+gqyfqoRhK5RX7D+leQwNy6ksL1aSkgwbRc5O9zOX60CoanpPLv2LZDP26Zpap9q4ob5c7ZRAS8vVdxfRUImJ2yP09nuRwWFrwa2T04V+Pkt9ZWRbXsYpuD0U9o5+4xOlLL8UpPK6HPAC2Zmi9z26xkGhzJ1lVG54rJ2IMilF/UTChkgywNegRx8Vs/eqACr7d67yC9uSVEqO/Vh1Gbw9Cf3sGnTMoz+rgllpCvXsU/FfXuQo2jnkNMQCo/GF/lojQHxkGnKQXWCk9Qf7Yry0b8EPLkKod9lFzBrQAig7LpsDYR5Z+8mdsjs2F719LitwbFOUU+Y08fLBf5xYj+78pm6+8AWHZtXdA7wF4dC6MPUH2pW3Tv2hqFSXvvn6TF+ujjdAEIlzgq3r1RCvwPeqS5YkjMFRiay+H36FhWwXvcEBlEU8Bu6+O3CUjnT0xWsumhDyIrls9razERnws9sukR6rlQ7ZuR4pFLFam+ajuytijR4oC0is3b9oaBxfGfCz1Kmwsxs7ZiRrgvm5svMpkusWxPEsvQ1ynu4hfqjgu9W99GliQ6/1tZmMjqep1xxV523bRqCfM5hMlUgGjFpj/si6jPNNogZecpdvB85ZO125Ubup35T+FEJoiO1k5YhVCrwtWkJIaMOhLKOw4AV4K09G6vLDzPqxvxFg2OdisxanTFWKvBPU/u5P79ERDdq+osZp8LLE2t4xUEIfRX4EPXTl7p63WtdPL6aGuFnqoG1HoTODMd4W8/GlRB6RxVCUzMFhsayuJ6HJsRmZJtKL09sywE367qWKhZtQgGDYMCowmgPcEZb2OxMJPyk0w0C2I5HarogYdQX1IEzkQP5b2oQwL4X2BoMGpu7En6WMvXHzuq6YG5OwmjtQAjL0taqh+NtDa6pu1Uy5BmJDp8eDhuMjecpl1eft22acrztVLJ4pDCqmvtoKaCjEUQnI2sSzh8tF7g2NcKdDSBUcF02+IO8qXs9p4djKNX1FmQpfSMldA1w5li5wD9NHeAPuUYQsnlZYg2v7ByoBrC/piRuvaeXoVyD11c8j+unR/jFYrpmxXQNCN2JHFvxm+rrJlN5RsZz+OWNtA65jaPrCQ6iCeCHui5S84tl4lFZlaxsUMHo9Law2Zno8JFOl+srI9sleVAZacjMU5j6mdc5ZJZrSzBobOxM+Flcskmn68MorVTa2oEglqWvUw/kXzdQ2XcjkyIXdSb8RjhkMD5ZD0Ya2ZxNMimnLz4CGPFEANEpyCrkp4yU8lw/Pcqd2fm6EMq7DlsCYd7Ws4HTDkLozchCsXp2moLDWaOlAp+d3M99+SWiNSDkIbdgvKyzn1d2rsUvIXStyiwsNHAXrgLeuK+Y499nx7hhYRq3oTv2MCX0LlbUPlVdMsfxEFKKl9QTe8cTHETfV78KruOxtj9MpO2QZSSDymU/va1Nwmi2IYxk1mgFjM5GLoCoB6Np5Cyi84NBo7Or08/iYoXZOql9w9CYnZWtKQMDQXyWvh6ZQPlNAxjdpf7+wsNhZNSAUU7BaIUyOlPB6L4nOojOVIG+p4yWCnx9epTfZupDaMmxWe8L8fbeDeyUgekpmqsTOlUFwc8ZLRX4/NR+7sktEjPMmhAqOA4vSvTz6s611XqiryPrnxYbQOgLwOtSlRLXpkb4hZrRXStFP6MaWN/as5EtBwPTh0AIYHA8y8RUfuUywByy5+d8ZP3LE9F+i8xYDssgrawyDgXMqnu2Ekb7gdPa2szORLuP9Fy5YcwomSoghKC/L6ghm0r9yFnltSyJHAe7Nhg01ncmfBQKDjOzRXS9NoxmZkssLJYZ6Avi8+nrkRX+dzQBowXgaZ0JvxEK6ExOFSiWV0/tm6ZGNitH4Kotr1UYpXiM5lLXs8dj02vVrgNeP1zM882ZMX6TmWvgjjn0WwHe1L2Oc9vaq1mA9yMLERspoS8C546WC1w5eYC7sgvEa0DIBYquwws7+nhN59pqe8fXFRzqrYE2ldv2mqxj8+XkELct1R+FMmtXOC0U5e29yxD6HfBu5FqYQ+zB/QvsG1pabdzF05FzYp6IdhOyylyeUwHZXIWdx3WwdUNktddfjJwNdfzEZJ6f35pkcqpQt85I1wVnnd7BGaclqn/8aeWaN4p5fgG4YGGxzE0/n2JsPL9qpffy9V102LyxjQsv6KGtzUQ9XN+qXMt69jZkX2Nw10ML3PrraQp5Z9VjCQGFgkMsZnHBk7vZfDC1/7fIJRdPOBBdDnx13q70fWN6hBsXprGEVhNCOddhky/Em7vXc1o4WnXH3oXsaK5npys36dyRUp6rpga5M7tAex0IlVyXF3T08tqutdVG168js1bZOsfZpt7Pm/YVc/xwPslNC7MIvJoQStsVTn04hN6FzFJwBCBq2QorlR1O3N7OprVttV7yDORs6uMnp/Lc/MskU8kCoTow0nTB2acnOOO05dKaTyFnQXsNYPQdYPv0bJFf3jrN2Hi27tjZQsFm86YIT7+gh4iE0U3IcbC7m4DRp4HQrocWuO32afKFZmDUw+aN4Wqc7S+RY2+eUK7Z+4Dzf7Iww//OT+Ehas75ybsOm3xB3tKzoQqh+5AD1H7UBIS+AJwnITTEb7MLdNSBUNl1eX57L6/vXobQN9WFkGtwnH8CXry/mOPr07Jtw9Bqr/yZUxB6R+/D3LHba0ZDF0vMzJUa7k9/optte/T3hoi11Vxae0D9OrWtzezs7PAxky7Vn2fkeExMydHA/X1BkHU2BnLEcC0YpZAlBMFQ0NjRmfBRrrjMzhZrzoo2TY1USs486u8N4vPpm9RD7i7q16rdqR6U53d1+q2AX2cqVaBYqu+mTaUKdHX6iUbMiLoF/o9Hb175UQmiFwI7b1hIcXd2kbYaRYR512GN5edNPes5Qwamh5TrcmMTELoSeNJIKc9VySF+m52jw7BqQqjiuTy3vYc3dK+rzh36pnrS1IOQT7mGT19ybL6ckk259TawztkVTg5FeWfvxpXFinUhVJ3JUy679XentwzUyIpom3X4eqLVYHRKW5vZlUj4mJ0tNhiu5jExlUcXgj4Jo/ObgNGQ+vuzQyFjXX9fkKQCTa3v0bI0kqkCS1mbvp5AFUZbm4DRb9W1el5Xp98XCOhMJQsUi7VhtLAgO4L6e4NYlragQgLpJxKIrgBOuiM7z4OF7KotFTmlhN7Us56zZNvGPSom9KNmITRcyvOlqSHuyM6RqAkhD9vzeE577+EQeiv1m3LXAB8DXjhYzPOd9Di3Z+ZqrjE6CKEI7+zdtBJC76wFobmFEuNTeQbHsqTnSy23rJkLWhcsLJUplGwKRRvD0FfbVlKF0X7gtEib2dmZ8DE9U2R+of6kx/HJPLoh6Os9BEa31IFRXrndx1umtr5DBcoXF8urpturMJpKyqmavT1B/BJGm9U9MNMARnng3K5Ov8/v15msAyNNkzvyenuCRNrMgroOh55IIHoJcMJEucieYhbbOxhLcZUS2uIP8uae9ZwhIfQrZGHfzxv83DMOgVByiN80AaEr4j28sXv9Sgj9FfULuU5D7qt/9Ugpz7XTMjtmCq2mEpq3K5wUivDugxC6uxaE0gslplJ5DoxmGBzN4DgelqXhtTjTlFVrZyaSOYplh1LJQdfFakA61E1TMFpoBKMJBaOeIAjOV1/xbXXcmmlkFmwgHDa3J9r95HI2c/OlmsrIZ2lMTRXIZiv0dgfw+/XNRwCjInBOV6ffH/DrTCWLFIvOw2BULDqsXRti65YIPkvbjewjm30igeg44Jwuy2ek7TK78hJGFc/DweOEQBtv6l7PKXKUxy9VnKbRNLhqTOhJw6U8X04O8evMHJ11IOR4HpfHe3lT9/pqjOqfkYWRxQZK6BvAxTOVMl9NDXFXbgFfHXdswamwU0FoxSiPt3NYij49XyI5U2D/8BIHFID8fv0JMd7jT22aJvBZOtm8zfhUnmLJoWK7CPEwIB3ipnUmfExPF1lYqqw+kF/BaGwij6FLNSEET2kCRmnk6I9t4bCxvbs7wNKSzfxCuTaMfBoTU3lyOZve7uBKGN3bAEZ3rISRX8WMcnlneUNKuexgWTpnnp6grzdQfdh/44kWIxoGtgc1fVuv6SdumAQ0jS7Tx5MjHbwkMcDxsm3jVhU/+X0TEPr8QQgN86vMHF01IOR4cgHfZfEe3tKzDKF/QRZGNoLQ54BnzNplvjkzyu2ZOcw67tiCU+GEYIS/Pgihew6HUHq+yHS6wN6hRQ6MZHA9D7+lVwsXW/bHAsmnkcvbjE7lKBZtWRR6KJCW3bS2NrOzs9NParrIwmJtGHkujI3nMQ2N3h4/QoinqL++vc7NXETOyN4e8Ovbe7oDzMwWWVysByNdwihvr1RGG9U90QhGZeCsrk6/37I0ymWXQtHBcz26Ov2cfFI7WzZFME0tpR7ij1s90eMFokUFo0S7YW07JRQVJ4UinN4W5+nRBB2GBTJ1+Zc03rt9hoLQk4dKea5ODvGrzBzdDSD0rHg3b+lZj84yhN5I/RlJW4DPAC8cLxf515kxblqYwdJqQ2jRsdkRiPDe/mUI3asC4L+uAmhmrsjuA4scGM3gekgAtRTQo6SQNPIFh7HJHLmC7N/0PAjIbvkDyAkRp7e1mZ1dCQmjxRp70yQ4BKPjOSxLo6c7UIWR2wSMbgR2+P36to52P7m8zdxcuebaaZ9PZ3wyTz5v031kMPoNstn87O6ugL+/L0gwYNDXE+C0kzvYuiWCaWpJZPP2vz6e38/jWVk9jizaKgDpkG6UorpRVoG9H6tAcKNirmpg+snDpTxfSQ7xq0yabsO36lXgqEKRZ8W6+cveI4JQRMWOLp8oF7k2NcIvl2bxa3pNCC05NscHwry3fwsbfSHURfNXwG/S8yXS8yUe3LfAgdEMeOCz9KNq19ifO5AKBYeRiSy5gr28jDHg0w+pwO7u9DOVKrJUc6Os/LaHx3L4LI2ergBCiAuQXep3UHs+VEnB6Phw2NjW3xckX3BIz5VX/NxDza9gVMg7dHf7qzDa0CSMloBAwK+H+3uDoYH+EOGwkVPq/EvIQtzH1R7PyurVAsBrkRmIZoZyL9cJDRXzfDU1xK2ZNN2GH3cVLVSF0DPj3fxl9/rqTf9vyBaRepMNdWQB29+UXJdPT+7l9qX5urOsM47NtkCY9/VtYYN/ee/YW4A70vMl7tmVZilTxu/TW+n4x/PiB2zHo1CyScT9nLKjg5gcrnYxshB2+2SywE9vniSdLuH366sWPbqu3Bd//nldnLwzXv1OP6Cum3rWpq7By23b5Qc3TjA6mkPTV1dGQsBSpsKO7VHOf1I3saiFAtp7kcMH61kXcBmyvzOm1PkN6mHvtUD0yKG1HJi+WrVTdJv1IXRpvIu/6tlQHbzfDISq5gH888wY/zYzXhdCWddhiz/M3/Zvru4d+wPwBuBOCaFZiiWn1lrllj1OQCpVHCJhayWMLkK2Bm2fSub5yc+mZG9ajcpo1wXH9bjgvC5OObl9ZUxxvAkY/RfwjNl0kV/fMcPQSA5Nqw2jxUyFHcfFuOC8bqJR80hgdPR+B8cgiLYD1wPnDZfyXJ1SEKqhhGzPQ0NwSbyLv+pZvxJCr6XxsDaBbJi9K+86PPOhO2oOTzsIoRDv799S3cB6v4LQb+cWStz1h1lKFTdh6OIMINhCwFFl0+WKc9thMLoQ2Sy9bSpZ4Cc/myQ9v/o8oyqMwONVL9tINGqVkB0AVzdx7B51TT8rl69w66+m2Xcgq5Yb1obRCcfFOP+8rqoy+rG6pqeOxZNvHGsuPrIq+7xUpcSXk0P8qoES0hA8I9bJW3s2VOM5/94khKpu2XNBDudv02qfrpzrsPlQCD0AvB64M60gVKm4nYYu3queXi07uixtmfrrlrKVG+55IF05dUcHsahVbTy9urcnsPWSC/u48WeTzNfcKCu76fceyHDGqR3V2GIzlkTW1n07FDQvu/jpvdj2FEMj2VVjRp4H0TaTBx5cADwueFIvkYhxCXK34NdoPJL5qLyxjyXrBI4rui43Lcxw8+JMfQgJeEask7f1bkBI9+o/gFc3CSFUsPE7AF2mj2CN2UUF12GTP8gH+rdWIbQLeA2yB4j7HpyjXHbQdfF8ZGFmy44+6wC+ZJnalqVchQf3L8+9+xkyybCvpyfApRf1EYtZlMurJ8U8j5WK6UgCgFkFo1t0XeOCJ3ezbm0Qz4PVvBbPg2jE5L77F9i9dxHH9kAureg5Fk/+sQaiPmBDslLi3twiHYZV2x0TcHGsS0FIuAoor+LI5u56StkUAC6IdICqxq7+KrguG3whPti/lXW+AMhGx1eh1kDPL5bRBNVy/qccg+f8iWbbDF3WCS1myith9BZgf093gGde1Ec8ZlKuuIcErx3Xw2fpJDp81XsreYTHzqnj/KStzeBpT+ll4/pwXRiFQwZjEzky2UpVgUVaIHr0LQ9kA5pG1DCxvdpK6KJoF2/r2YhAOMD/Q25KeCTDv3VkBoXnd/TylGiCXstHj+mjy/RxeijGR9ZsZa1UQnuQW2WXq8Dv2ZUmk6tUa4NOPAbd4SeSOUBZ1wSzCyXu33PINOCbkSu+D/T0BLj04gHiUQtNk5s5HMejPe7jpBPj1T60BRqPqVnNHkLOIr+xLWxw8dN62bihDRCrZuyEgIrt4roeyEzyxlaM6NG3KWB3wrAu3BFo4+dLM4dsxKh4LobQuCjWydt7NiIkeP5LQcj5Iy7O9wMXdJm+k97buzlwZ3Ye25PQe3KkHeXqHQD+YiWEFjNldE2sTNEfQLa3tFTR0Xs/6IAct+JBJluhLWxW//7nyLjfN3q6/Rue/ax+7rt/gXkVwN55Ypx1a0JVCF1N/fqeRg/cFwI/tHzaBU+/oJcbfzrB2HgOTeOQgtdKxaM95quuMNrP49S0+sfa0bLFo1krASFNiGdFDdNasCvMOxVKrnTQIrrJUyIdvKN3IwLxp4DQSl//JvV7tN8KOGt8gdw6XyCHHCr1v8gMySHzf2/7XZJC0UY/mO4fBs5Tsa6WHX22H/gfYEwIQS5vs5itsK4/vPI1w8hs6JOCAaNt4/qwfvz2KNu2RKrZq2Hk9paP/5HvpYKMMW43DbExHrXIZm0WMxWVnYOK7REOGZx0YjvdXQGE4LvImULZY+3EH4vp+17keufXOZ7HDQspfr00hyE0Lox1cnZbDJ/QbXVBvZw//fbTAeT8Yk09PX9Hjc2Xt96ZJJOrHF4x/UYFrRi0GuqPhntA3fQTyNq0by9LYcejs8PPWSev+tw4DdmbuHPFnxWAf6TxvKwjsYC6li+p2K64+ZcpJqfyuK5cl33+k7rYuL4N5CC2yxW8aIHosbGt6qI5mYfv9RpFdhK/ksd5BXMNEIGcY7OB5rN3LXt03bFZ5XYd0vDcAESPpQWA7yLH3IqJqTzz82VOOC5WzctNI7fS/uux+iUcqyCq2rPVU2lgBYQ+iezzedytDohadgzYUQQikAWw30bGGKMKoHlkA/mnkKUpx/TT4Fi2H/DIMhMta9mxZnnkZNMTgEuQccZ7lBu4dMz7x8e4ImpZy1r2Z2CtNHLLWtayFoha1rKWtawFopa1rGUtELWsZS1rWQtELWtZy1ogalnLWtayFoha1rKWtUDUspa1rGUtELWsZS1rgahlLWtZy1ogalnLWtYCUcta1rKWtUDUspa1rAWilrWsZS1rgahlLWvZ427/fwCO1noEQ3wVzgAAAABJRU5ErkJggg==";
	doc.addImage(imagen_logo,20,50,145,94);
	doc.setFontSize(20);
	doc.text(450, 100, (fecha.getDate()<10?("0"+fecha.getDate()):fecha.getDate())+"-"+((fecha.getMonth()+1)<10?("0"+(fecha.getMonth()+1)):(fecha.getMonth()+1))+"-"+fecha.getFullYear());
	
	
	doc.setFontSize(50);
	doc.text(180, 400, "Informe de");
	doc.text(80, 480, "Acciones Repetidas");
	
	doc.addPage();
	
	var canvas=$('#graf')[0];
	//label
	var labels_paciente=[];
	labels_paciente.push(
		{ 
			title: {
				text: 'Cantidad'
			},
			labels: {
				format: '{value} veces'
			}
		}
		);
	var labels_paciente1=[];
	labels_paciente1.push(
		{ 
			title: {
				text: 'Cantidad'
			},
			labels: {
				format: '{value} personas'
			}
		}
		);
	for(var i=0;i<datos.encuestas.length;i++)
	{
		console.log("rut:"+datos.encuestas[i].rut);
		//datos de encuestas
		rows.push(["Rut",datos.encuestas[i].rut+"-"+Fn.dv(datos.encuestas[i].rut)]);
		rows.push(["Nombre",datos.encuestas[i].nombre]);
		rows.push(["Edad",datos.encuestas[i].edad]);
		rows.push(["Género",datos.encuestas[i].genero==null?"Sin datos":(datos.encuestas[i].genero=="M"?"Masculino":"Femenino")]);
		rows.push(["Sensor 1",datos.encuestas[i].ubicacion_sensor1]);
		rows.push(["Sensor 2",datos.encuestas[i].ubicacion_sensor2]);
		rows.push(["Sensor 3",datos.encuestas[i].ubicacion_sensor3]);
		rows.push(["Caídas previas",transformarDato(datos.encuestas[i].caidas_previas)]);
		rows.push(["Caídas transtorno marcha",transformarDato(datos.encuestas[i].caidas_transtorno_marcha)]);
		rows.push(["Polifarmacia",transformarDato(datos.encuestas[i].polifarmacia)]);
		rows.push(["Incontinencia urinaria",transformarDato(datos.encuestas[i].incontinencia_urinaria)]);
		rows.push(["Incontinencia fecal",transformarDato(datos.encuestas[i].incontinencia_fecal)]);
		rows.push(["Deterioro cognitivo",transformarDato(datos.encuestas[i].deterioro_cognitivo)]);
		rows.push(["Delirium",transformarDato(datos.encuestas[i].delirium)]);
		rows.push(["Transtorno de ánimo",transformarDato(datos.encuestas[i].transtorno_animo)]);
		rows.push(["Transtorno del sueño",transformarDato(datos.encuestas[i].transtorno_sueno)]);
		rows.push(["Malnutrición",transformarDato(datos.encuestas[i].malnutricion)]);
		rows.push(["Déficit sensorial",transformarDato(datos.encuestas[i].deficit_sensorial)]);
		rows.push(["Inmovilidad",transformarDato(datos.encuestas[i].inmovilidad)]);
		rows.push(["Úlcera por presión",transformarDato(datos.encuestas[i].ulcera_por_presion)]);
		rows.push(["Puntaje test Charlson",transformarDato(datos.encuestas[i].puntaje_test_charlson)]);
		rows.push(["Puntaje índice Barthel",transformarDato(datos.encuestas[i].puntaje_indice_barthel)]);
		rows.push(["Puntaje índice Lawton",transformarDato(datos.encuestas[i].puntaje_indice_lawton)]);
		rows.push(["Puntage GDS",transformarDato(datos.encuestas[i].puntaje_gds)]);
		rows.push(["NPI conducta anómala",transformarDato(datos.encuestas[i].npi_conducta_anomala)]);
		rows.push(["NPI frecuencia",transformarDato(datos.encuestas[i].npi_frecuencia)]);
		rows.push(["NPI gravedad",transformarDato(datos.encuestas[i].npi_gravedad)]);
		rows.push(["NPI angustia",transformarDato(datos.encuestas[i].npi_angustia)]);
		rows.push(["Puntaje MMSE",transformarDato(datos.encuestas[i].puntaje_mmse)]);
		rows.push(["",""]);
		
		
		doc.autoTable(columns, rows, {
			//startY: doc.autoTableEndPosY() + 40, 
			theme: 'grid'
		});
		
		if(con_graficos===false)
		{
			doc.addPage();
			rows=[];
			continue;
		}
		
		//datos de graficos
		
		var series_paciente1=[];
		var series_paciente2=[];
		var series_paciente3=[];
		
		var s1="";
		var s2="";
		var s3="";
		var dia_inicial=0;
		var dia_final=7;	
		
		//ubicacion para cantidad
		s1=datos.graficos[i].ubicaciones.s1;
		s2=datos.graficos[i].ubicaciones.s2;
		s3=datos.graficos[i].ubicaciones.s3;
		var datos1=null;
		var datos2=null;
		var datos3=null;
		
		for(var k=0;k<datos.cantidad_personas.length;k++)
		{
			if(datos.cantidad_personas[k].ubicacion==s1)
			{
				datos1=[{
					name: "Cantidad de personas",
					type: 'spline',
					yAxis: 0,
					data: datos.cantidad_personas[k].datos
				}];
			}
			if(datos.cantidad_personas[k].ubicacion==s2)
			{
				datos2=[{
					name: "Cantidad de personas",
					type: 'spline',
					yAxis: 0,
					data: datos.cantidad_personas[k].datos
				}];
			}
			if(datos.cantidad_personas[k].ubicacion==s3)
			{
				datos3=[{
					name: "Cantidad de personas",
					type: 'spline',
					yAxis: 0,
					data: datos.cantidad_personas[k].datos
				}];
			}
		}
		//datos generales
		$('#graf_cant1').highcharts({
			chart: {
				zoomType: 'xy',
				marginRight:150
				
			},
			legend: {
				margin:150,
				align:'right',
				verticalAlign: 'top',
				layout: 'vertical',
				x: 0,
				y: 100
			},
			title: {
				text: 'Cantidad de personas que abrieron la puerta'
			},
			subtitle: {
				text: s1
			},
			xAxis: [{
				categories: datos.categorias,
				crosshair: true
			}],
			yAxis: labels_paciente1,
			tooltip: {
				shared:true
					
			},
	
			series: datos1
		});
		$('#graf_cant2').highcharts({
			chart: {
				zoomType: 'xy',
				marginRight:150
				
			},
			legend: {
				margin:150,
				align:'right',
				verticalAlign: 'top',
				layout: 'vertical',
				x: 0,
				y: 100
			},
			title: {
				text: 'Cantidad de personas que abrieron la puerta'
			},
			subtitle: {
				text: s2
			},
			xAxis: [{
				categories: datos.categorias,
				crosshair: true
			}],
			yAxis: labels_paciente1,
			tooltip: {
				shared:true
					
			},
	
			series: datos2
		});
		$('#graf_cant3').highcharts({
			chart: {
				zoomType: 'xy',
				marginRight:150
				
			},
			legend: {
				margin:150,
				align:'right',
				verticalAlign: 'top',
				layout: 'vertical',
				x: 0,
				y: 100
			},
			title: {
				text: 'Cantidad de personas que abrieron la puerta'
			},
			subtitle: {
				text: s3
			},
			xAxis: [{
				categories: datos.categorias,
				crosshair: true
			}],
			yAxis: labels_paciente1,
			tooltip: {
				shared:true
					
			},
	
			series: datos3
		});
		doc.addPage();
		var chart1=$('#graf_cant1').highcharts();
		var img1=chart1.getSVG();
		canvg("graf",img1);
		var dataURL = canvas.toDataURL("image/jpeg");
		doc.addImage(dataURL,
			pos_x,30,
			ancho_2,alto_2);
	
		var chart2=$('#graf_cant2').highcharts();
		var img2=chart2.getSVG();
		canvg("graf",img2);
		var dataURL = canvas.toDataURL("image/jpeg");
		doc.addImage(dataURL,
			pos_x,290,
			ancho_2,alto_2);
		
		var chart3=$('#graf_cant3').highcharts();
		var img3=chart3.getSVG();
		canvg("graf",img3);
		var dataURL = canvas.toDataURL("image/jpeg");
		doc.addImage(dataURL,
			pos_x,540,
			ancho_2,alto_2);
		
		//buscar primeros 7 dias
		var graficos_s1=[];
		var graficos_s2=[];
		var graficos_s3=[];
		var subtitulos_s1=[];
		var subtitulos_s2=[];
		var subtitulos_s3=[];
		while(dia_final<=30)
		{
			
			for(var j=dia_inicial;j<dia_final;j++)
			{
				series_paciente1.push(
				{
					name: 'Día '+(j+1),
					type: 'spline',
					yAxis: 0,
					data: datos.graficos[i].aperturas.s1[j]
				}
				);
				series_paciente2.push(
					{
						name: 'Día '+(j+1),
						type: 'spline',
						yAxis: 0,
						data: datos.graficos[i].aperturas.s2[j]
					}
					);
				series_paciente3.push(
					{
						name: 'Día '+(j+1),
						type: 'spline',
						yAxis: 0,
						data: datos.graficos[i].aperturas.s3[j]
					}
					);
				
			}
			
			
			//ubicacion
			s1=datos.graficos[i].ubicaciones.s1;
			s2=datos.graficos[i].ubicaciones.s2;
			s3=datos.graficos[i].ubicaciones.s3;
			//buscar grafico general
			for(var j=0;j<datos.promedios.length;j++)
			{
				if(datos.promedios[j].ubicacion==s1)
				{
					series_paciente1.unshift(
					{
						name: 'promedio',
						type: 'spline',
						yAxis: 0,
						data: datos.promedios[j].datos
					}
					);
					series_paciente1.unshift(
					{
						name: 'desviación',
						type: 'arearange',
						yAxis: 0,
						data: datos.desviaciones[j].datos
					}
					);

				}
				if(datos.promedios[j].ubicacion==s2)
				{
					series_paciente2.unshift(
					{
						name: 'promedio',
						type: 'spline',
						yAxis: 0,
						data: datos.promedios[j].datos
					}
					);
					series_paciente2.unshift(
					{
						name: 'desviación',
						type: 'arearange',
						yAxis: 0,
						data: datos.desviaciones[j].datos
					}
					);
	
				}
				if(datos.promedios[j].ubicacion==s3)
				{
					series_paciente3.unshift(
					{
						name: 'promedio',
						type: 'spline',
						yAxis: 0,
						data: datos.promedios[j].datos
					}
					);
					series_paciente3.unshift(
					{
						name: 'desviación',
						type: 'arearange',
						yAxis: 0,
						data: datos.desviaciones[j].datos
					}
					);
	
				}
				
			}
			

			//datos por sensor	
			
			$('#graf_s1_'+dia_final).highcharts({
				chart: {
					zoomType: 'xy',
					marginRight:150
					
				},
				legend: {
					margin:150,
					align:'right',
					verticalAlign: 'top',
					layout: 'vertical',
					x: 0,
					y: 100
				},
				title: {
					text: 'Datos paciente'
				},
				subtitle: {
					text: s1+" "+(datos.graficos[i].ruido.s1?"Tiene ruido":"")
				},
				xAxis: [{
					categories: datos.categorias,
					crosshair: true
				}],
				yAxis: labels_paciente,
				tooltip: {
					shared:true
						
				},
		
				series: series_paciente1
			});
			$('#graf_s2_'+dia_final).highcharts({
				chart: {
					zoomType: 'xy',
					marginRight:150
				},
				
				legend: {
					margin:150,
					align:'right',
					verticalAlign: 'top',
					layout: 'vertical',
					x: 0,
					y: 100
				},
				title: {
					text: 'Datos paciente'
				},
				subtitle: {
					text: s2+" "+(datos.graficos[i].ruido.s2?"Tiene ruido":"")
				},
				xAxis: [{
					categories: datos.categorias,
					crosshair: true
				}],
				yAxis: labels_paciente,
				tooltip: {
					shared:true
						
				},
		
				series: series_paciente2
			});
			$('#graf_s3_'+dia_final).highcharts({
				chart: {
					zoomType: 'xy',
					marginRight:150
				},
				
				legend: {
					margin:150,
					align:'right',
					verticalAlign: 'top',
					layout: 'vertical',
					x: 0,
					y: 100
				},
				title: {
					text: 'Datos paciente'
				},
				subtitle: {
					text: s3+" "+(datos.graficos[i].ruido.s3?"Tiene ruido":"")
				},
				xAxis: [{
					categories: datos.categorias,
					crosshair: true
				}],
				yAxis: labels_paciente,
				tooltip: {
					shared:true
						
				},
			
				series: series_paciente3
			});
			
			series_paciente1=[];
			series_paciente2=[];
			series_paciente3=[];
		//	labels_paciente=[];
			var g1=$('#graf_s1_'+dia_final).highcharts();
			var g2=$('#graf_s2_'+dia_final).highcharts();
			var g3=$('#graf_s3_'+dia_final).highcharts();
			graficos_s1.push(g1);
			graficos_s2.push(g2);
			graficos_s3.push(g3);
			
			rows=[];
			
			dia_inicial=dia_final;
			dia_final+=7;
			if(dia_final>30)
				dia_final=30;
			if(dia_inicial==30)
				dia_final=31;
			
			
			
		}

		
		doc.addPage();
		console.log("agregando página para gráficos");
		
		//sensor1
		var chart1=graficos_s1[0];
		var img1=chart1.getSVG();
		canvg("graf",img1);
		var dataURL = canvas.toDataURL("image/jpeg");
		doc.addImage(dataURL,
			pos_x,50,
			ancho,alto);
		
		var chart1=graficos_s1[1];
		var img1=chart1.getSVG();
		canvg("graf",img1);
		var dataURL = canvas.toDataURL("image/jpeg");
		doc.addImage(dataURL,
			pos_x+ancho,50,
			ancho,alto);
		var chart1=graficos_s1[2];
		var img1=chart1.getSVG();
		canvg("graf",img1);
		var dataURL = canvas.toDataURL("image/jpeg");
		doc.addImage(dataURL,
			pos_x,50+alto,
			ancho,alto);
		var chart1=graficos_s1[3];
		var img1=chart1.getSVG();
		canvg("graf",img1);
		var dataURL = canvas.toDataURL("image/jpeg");
		doc.addImage(dataURL,
			pos_x+ancho,50+alto,
			ancho,alto);
		var chart1=graficos_s1[4];
		var img1=chart1.getSVG();
		canvg("graf",img1);
		var dataURL = canvas.toDataURL("image/jpeg");
		doc.addImage(dataURL,
			pos_x,50+alto*2,
			ancho,alto);
	doc.addPage();
//sensor2
		var chart1=graficos_s2[0];
		var img1=chart1.getSVG();
		canvg("graf",img1);
		var dataURL = canvas.toDataURL("image/jpeg");
		doc.addImage(dataURL,
			pos_x,50,
			ancho,alto);
		
		var chart1=graficos_s2[1];
		var img1=chart1.getSVG();
		canvg("graf",img1);
		var dataURL = canvas.toDataURL("image/jpeg");
		doc.addImage(dataURL,
			pos_x+ancho,50,
			ancho,alto);
		var chart1=graficos_s2[2];
		var img1=chart1.getSVG();
		canvg("graf",img1);
		var dataURL = canvas.toDataURL("image/jpeg");
		doc.addImage(dataURL,
			pos_x,50+alto,
			ancho,alto);
		var chart1=graficos_s2[3];
		var img1=chart1.getSVG();
		canvg("graf",img1);
		var dataURL = canvas.toDataURL("image/jpeg");
		doc.addImage(dataURL,
			pos_x+ancho,50+alto,
			ancho,alto);
		var chart1=graficos_s2[4];
		var img1=chart1.getSVG();
		canvg("graf",img1);
		var dataURL = canvas.toDataURL("image/jpeg");
		doc.addImage(dataURL,
			pos_x,50+alto*2,
			ancho,alto);
		
		doc.addPage();
//sensor3
		var chart1=graficos_s3[0];
		var img1=chart1.getSVG();
		canvg("graf",img1);
		var dataURL = canvas.toDataURL("image/jpeg");
		doc.addImage(dataURL,
			pos_x,50,
			ancho,alto);
		
		var chart1=graficos_s3[1];
		var img1=chart1.getSVG();
		canvg("graf",img1);
		var dataURL = canvas.toDataURL("image/jpeg");
		doc.addImage(dataURL,
			pos_x+ancho,50,
			ancho,alto);
		var chart1=graficos_s3[2];
		var img1=chart1.getSVG();
		canvg("graf",img1);
		var dataURL = canvas.toDataURL("image/jpeg");
		doc.addImage(dataURL,
			pos_x,50+alto,
			ancho,alto);
		var chart1=graficos_s3[3];
		var img1=chart1.getSVG();
		canvg("graf",img1);
		var dataURL = canvas.toDataURL("image/jpeg");
		doc.addImage(dataURL,
			pos_x+ancho,50+alto,
			ancho,alto);
		var chart1=graficos_s3[4];
		var img1=chart1.getSVG();
		canvg("graf",img1);
		var dataURL = canvas.toDataURL("image/jpeg");
		doc.addImage(dataURL,
			pos_x,50+alto*2,
			ancho,alto);
		
		
		doc.addPage();
		console.log("agregando página para el siguiente paciente");
	}
	if(!con_graficos)
		doc.save(nombreArchivo+' sin gráficos.pdf');
	else
		doc.save(nombreArchivo+'.pdf');
}
