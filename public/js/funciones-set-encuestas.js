function setTestGetUpInicial(datos){
	$('#timedGetUpInicial-tiempo-encuesta').val(datos.intento_practica);
	$('#timedGetUpInicial-p-1').val(datos.intento_uno);
	$('#timedGetUpInicial-p-2').val(datos.intento_dos);
	$('#timedGetUpInicial-p-3').val(datos.intento_tres);
	if(datos.fecha_test != null) $('#timedGetUpInicial-fecha-encuesta').data("DateTimePicker").date(new Date(datos.fecha_test+" "));
	calcularPromTestTimedGetUp('timedGetUpInicial-p-1', 'timedGetUpInicial-p-2', 'timedGetUpInicial-p-3', 'timedGetUpInicial-tiempo-encuesta');		
}

function setTestGetUpFinal(datos){
	$('#timedGetUpFinal-tiempo-encuesta').val(datos.intento_practica);
	$('#timedGetUpFinal-p-1').val(datos.intento_uno);
	$('#timedGetUpFinal-p-2').val(datos.intento_dos);
	$('#timedGetUpFinal-p-3').val(datos.intento_tres);
	if(datos.fecha_test != null) $('#timedGetUpFinal-fecha-encuesta').data("DateTimePicker").date(new Date(datos.fecha_test+" "));
	calcularPromTestTimedGetUp('timedGetUpFinal-p-1', 'timedGetUpFinal-p-2', 'timedGetUpFinal-p-3', 'timedGetUpFinal-tiempo-encuesta');		
}

//se usa 
function setSindromesGeriatricos(datos){

	//console.log("geriatrico llenando datos");
	if(datos.fecha_test != null) $('#sindromesGeriatricosInicial-fecha-encuesta').data("DateTimePicker").date(new Date(datos.fecha_test+" "));

	$('#sindromesGeriatricosInicial-caidas-ultimos-meses').val(datos.numero_caidas);
	$('#resultadoSinGer').text(datos.numero_caidas);

	$('#formSindromesGeriatricosInicial').find('input:radio[name="sindromesGeriatricos-p-1"]').filter('[value="'+datos.caidas_transtorno+'"]').trigger('click');	
	$('#sindromesGeriatricosInicial-c-1').val(datos.comentario_caidas_transtorno);

	$('#formSindromesGeriatricosInicial').find('input:radio[name="sindromesGeriatricos-p-2"]').filter('[value="'+datos.polifarmacia+'"]').trigger('click');	
	$('#sindromesGeriatricosInicial-c-2').val(datos.comentario_polifarmacia);

	$('#formSindromesGeriatricosInicial').find('input:radio[name="sindromesGeriatricos-p-3"]').filter('[value="'+datos.incontinencia_urinaria+'"]').trigger('click');	
	$('#sindromesGeriatricosInicial-c-3').val(datos.comentario_incontinencia_urinaria);

	$('#formSindromesGeriatricosInicial').find('input:radio[name="sindromesGeriatricos-p-4"]').filter('[value="'+datos.incontinencia_fecal+'"]').trigger('click');
	$('#sindromesGeriatricosInicial-c-4').val(datos.comentario_incontinencia_fecal);

	$('#formSindromesGeriatricosInicial').find('input:radio[name="sindromesGeriatricos-p-5"]').filter('[value="'+datos.deterioro_cognitivo+'"]').trigger('click');
	$('#sindromesGeriatricosInicial-c-5').val(datos.comentario_deterioro_cognitivo);

	$('#formSindromesGeriatricosInicial').find('input:radio[name="sindromesGeriatricos-p-6"]').filter('[value="'+datos.delirium+'"]').trigger('click');
	$('#sindromesGeriatricosInicial-c-6').val(datos.comentario_delirium);

	$('#formSindromesGeriatricosInicial').find('input:radio[name="sindromesGeriatricos-p-7"]').filter('[value="'+datos.trans_animo+'"]').trigger('click');
	$('#sindromesGeriatricosInicial-c-7').val(datos.comentario_trans_animo);

	$('#formSindromesGeriatricosInicial').find('input:radio[name="sindromesGeriatricos-p-8"]').filter('[value="'+datos.trans_sueno+'"]').trigger('click');
	$('#sindromesGeriatricosInicial-c-8').val(datos.comentario_trans_sueno);

	$('#formSindromesGeriatricosInicial').find('input:radio[name="sindromesGeriatricos-p-9"]').filter('[value="'+datos.malnutricion+'"]').trigger('click');
	$('#sindromesGeriatricosInicial-c-9').val(datos.comentario_malnutricion);
	
	$('#formSindromesGeriatricosInicial').find('input:radio[name="sindromesGeriatricos-p-10"]').filter('[value="'+datos.deficit_sensorial+'"]').trigger('click');
	$('#sindromesGeriatricosInicial-c-10').val(datos.comentario_deficit_sensorial);

	$('#formSindromesGeriatricosInicial').find('input:radio[name="sindromesGeriatricos-p-11"]').filter('[value="'+datos.inmovilidad+'"]').trigger('click');
	$('#sindromesGeriatricosInicial-c-11').val(datos.comentario_inmovilidad);

	$('#formSindromesGeriatricosInicial').find('input:radio[name="sindromesGeriatricos-p-12"]').filter('[value="'+datos.ulcera_presion+'"]').trigger('click');
	$('#sindromesGeriatricosInicial-c-12').val(datos.comentario_ulcera_presion);
}

function setSindromesGeriatricosFinal(datos){
	if(datos.fecha_test != null) $('#sindromesGeriatricosFinal-fecha-encuesta').data("DateTimePicker").date(new Date(datos.fecha_test+" "));

	$('#sindromesGeriatricosFinal-caidas-ultimos-meses').val(datos.numero_caidas);

	$('#formSindromesGeriatricosFinal').find('input:radio[name="sindromesGeriatricos-p-1"]').filter('[value="'+datos.caidas_transtorno+'"]').trigger('click');	
	$('#sindromesGeriatricosFinal-c-1').val(datos.comentario_caidas_transtorno);

	$('#formSindromesGeriatricosFinal').find('input:radio[name="sindromesGeriatricos-p-2"]').filter('[value="'+datos.polifarmacia+'"]').trigger('click');	
	$('#sindromesGeriatricosFinal-c-2').val(datos.comentario_polifarmacia);

	$('#formSindromesGeriatricosFinal').find('input:radio[name="sindromesGeriatricos-p-3"]').filter('[value="'+datos.incontinencia_urinaria+'"]').trigger('click');	
	$('#sindromesGeriatricosFinal-c-3').val(datos.comentario_incontinencia_urinaria);

	$('#formSindromesGeriatricosFinal').find('input:radio[name="sindromesGeriatricos-p-4"]').filter('[value="'+datos.incontinencia_fecal+'"]').trigger('click');
	$('#sindromesGeriatricosFinal-c-4').val(datos.comentario_incontinencia_fecal);

	$('#formSindromesGeriatricosFinal').find('input:radio[name="sindromesGeriatricos-p-5"]').filter('[value="'+datos.deterioro_cognitivo+'"]').trigger('click');
	$('#sindromesGeriatricosFinal-c-5').val(datos.comentario_deterioro_cognitivo);

	$('#formSindromesGeriatricosFinal').find('input:radio[name="sindromesGeriatricos-p-6"]').filter('[value="'+datos.delirium+'"]').trigger('click');
	$('#sindromesGeriatricosFinal-c-6').val(datos.comentario_delirium);

	$('#formSindromesGeriatricosFinal').find('input:radio[name="sindromesGeriatricos-p-7"]').filter('[value="'+datos.trans_animo+'"]').trigger('click');
	$('#sindromesGeriatricosFinal-c-7').val(datos.comentario_trans_animo);

	$('#formSindromesGeriatricosFinal').find('input:radio[name="sindromesGeriatricos-p-8"]').filter('[value="'+datos.trans_sueno+'"]').trigger('click');
	$('#sindromesGeriatricosFinal-c-8').val(datos.comentario_trans_sueno);

	$('#formSindromesGeriatricosFinal').find('input:radio[name="sindromesGeriatricos-p-9"]').filter('[value="'+datos.malnutricion+'"]').trigger('click');
	$('#sindromesGeriatricosFinal-c-9').val(datos.comentario_malnutricion);
	
	$('#formSindromesGeriatricosFinal').find('input:radio[name="sindromesGeriatricos-p-10"]').filter('[value="'+datos.deficit_sensorial+'"]').trigger('click');
	$('#sindromesGeriatricosFinal-c-10').val(datos.comentario_deficit_sensorial);

	$('#formSindromesGeriatricosFinal').find('input:radio[name="sindromesGeriatricos-p-11"]').filter('[value="'+datos.inmovilidad+'"]').trigger('click');
	$('#sindromesGeriatricosFinal-c-11').val(datos.comentario_inmovilidad);

	$('#formSindromesGeriatricosFinal').find('input:radio[name="sindromesGeriatricos-p-12"]').filter('[value="'+datos.ulcera_presion+'"]').trigger('click');
	$('#sindromesGeriatricosFinal-c-12').val(datos.comentario_ulcera_presion);
}

//se usa
function setIndiceBarthel(datos){
	//console.log("barthel llenando datos");
	if(datos.fecha_test != null) $('#indiceBarthelInicial-fecha-encuesta').data("DateTimePicker").date(new Date(datos.fecha_test+" "));
	$('#formIndiceBarthelInicial').find('input:radio[name="indiceBarthel-p-1"]').filter('[value='+datos.comer+']').trigger('click');
	$('#formIndiceBarthelInicial').find('input:radio[name="indiceBarthel-p-2"]').filter('[value="'+datos.lavarse+'"]').trigger('click');
	$('#formIndiceBarthelInicial').find('input:radio[name="indiceBarthel-p-3"]').filter('[value="'+datos.vestirse+'"]').trigger('click');
	$('#formIndiceBarthelInicial').find('input:radio[name="indiceBarthel-p-4"]').filter('[value="'+datos.arreglarse+'"]').trigger('click');
	$('#formIndiceBarthelInicial').find('input:radio[name="indiceBarthel-p-5"]').filter('[value="'+datos.deposiciones+'"]').trigger('click');
	$('#formIndiceBarthelInicial').find('input:radio[name="indiceBarthel-p-6"]').filter('[value="'+datos.miccion+'"]').trigger('click');
	$('#formIndiceBarthelInicial').find('input:radio[name="indiceBarthel-p-7"]').filter('[value="'+datos.uso_retrete+'"]').trigger('click');
	$('#formIndiceBarthelInicial').find('input:radio[name="indiceBarthel-p-8"]').filter('[value="'+datos.trasladarse+'"]').trigger('click');
	$('#formIndiceBarthelInicial').find('input:radio[name="indiceBarthel-p-9"]').filter('[value="'+datos.deambular+'"]').trigger('click');
	$('#formIndiceBarthelInicial').find('input:radio[name="indiceBarthel-p-10"]').filter('[value="'+datos.escalones+'"]').trigger('click');
}

function setIndiceBarthelFinal(datos){
	if(datos.fecha_test != null) $('#indiceBarthelFinal-fecha-encuesta').data("DateTimePicker").date(new Date(datos.fecha_test+" "));
	$('#formIndiceBarthelFinal').find('input:radio[name="indiceBarthel-p-1"]').filter('[value='+datos.comer+']').trigger('click');
	$('#formIndiceBarthelFinal').find('input:radio[name="indiceBarthel-p-2"]').filter('[value="'+datos.lavarse+'"]').trigger('click');
	$('#formIndiceBarthelFinal').find('input:radio[name="indiceBarthel-p-3"]').filter('[value="'+datos.vestirse+'"]').trigger('click');
	$('#formIndiceBarthelFinal').find('input:radio[name="indiceBarthel-p-4"]').filter('[value="'+datos.arreglarse+'"]').trigger('click');
	$('#formIndiceBarthelFinal').find('input:radio[name="indiceBarthel-p-5"]').filter('[value="'+datos.deposiciones+'"]').trigger('click');
	$('#formIndiceBarthelFinal').find('input:radio[name="indiceBarthel-p-6"]').filter('[value="'+datos.miccion+'"]').trigger('click');
	$('#formIndiceBarthelFinal').find('input:radio[name="indiceBarthel-p-7"]').filter('[value="'+datos.uso_retrete+'"]').trigger('click');
	$('#formIndiceBarthelFinal').find('input:radio[name="indiceBarthel-p-8"]').filter('[value="'+datos.trasladarse+'"]').trigger('click');
	$('#formIndiceBarthelFinal').find('input:radio[name="indiceBarthel-p-9"]').filter('[value="'+datos.deambular+'"]').trigger('click');
	$('#formIndiceBarthelFinal').find('input:radio[name="indiceBarthel-p-10"]').filter('[value="'+datos.escalones+'"]').trigger('click');
}

//se usa
function setLawtonBrody(datos){
	//console.log("lawton llenando datos");
	if(datos.fecha_test != null) $('#LawtonBrodyInicial-fecha-encuesta').data("DateTimePicker").date(new Date(datos.fecha_test+" "));
	$('#formLawtonBrodyInicial').find('input:radio[name="p-1"]').filter('[value="'+datos.telefono+'"]').trigger('click');
	$('#formLawtonBrodyInicial').find('input:radio[name="p-2"]').filter('[value="'+datos.compras+'"]').trigger('click');
	$('#formLawtonBrodyInicial').find('input:radio[name="p-3"]').filter('[value="'+datos.comida+'"]').trigger('click');
	$('#formLawtonBrodyInicial').find('input:radio[name="p-4"]').filter('[value="'+datos.tareas+'"]').trigger('click');
	$('#formLawtonBrodyInicial').find('input:radio[name="p-5"]').filter('[value="'+datos.ropa+'"]').trigger('click');
	$('#formLawtonBrodyInicial').find('input:radio[name="p-6"]').filter('[value="'+datos.transporte+'"]').trigger('click');
	$('#formLawtonBrodyInicial').find('input:radio[name="p-7"]').filter('[value="'+datos.medicacion+'"]').trigger('click');
	$('#formLawtonBrodyInicial').find('input:radio[name="p-8"]').filter('[value="'+datos.dinero+'"]').trigger('click');	
}

function setLawtonBrodyFinal(datos){
	if(datos.fecha_test != null) $('#LawtonBrodyFinal-fecha-encuesta').data("DateTimePicker").date(new Date(datos.fecha_test+" "));
	$('#formLawtonBrodyFinal').find('input:radio[name="p-1"]').filter('[value="'+datos.telefono+'"]').trigger('click');
	$('#formLawtonBrodyFinal').find('input:radio[name="p-2"]').filter('[value="'+datos.compras+'"]').trigger('click');
	$('#formLawtonBrodyFinal').find('input:radio[name="p-3"]').filter('[value="'+datos.comida+'"]').trigger('click');
	$('#formLawtonBrodyFinal').find('input:radio[name="p-4"]').filter('[value="'+datos.tareas+'"]').trigger('click');
	$('#formLawtonBrodyFinal').find('input:radio[name="p-5"]').filter('[value="'+datos.ropa+'"]').trigger('click');
	$('#formLawtonBrodyFinal').find('input:radio[name="p-6"]').filter('[value="'+datos.transporte+'"]').trigger('click');
	$('#formLawtonBrodyFinal').find('input:radio[name="p-7"]').filter('[value="'+datos.medicacion+'"]').trigger('click');
	$('#formLawtonBrodyFinal').find('input:radio[name="p-8"]').filter('[value="'+datos.dinero+'"]').trigger('click');
}

function setTinetiEquilibrioInicio(datos){
	if(datos.fecha_test != null) $('#TinetiInicial-fecha-encuesta').data("DateTimePicker").date(new Date(datos.fecha_test+" "));
	$('#formTinetiInicial').find('input:radio[name="p-1"]').filter('[value="'+datos.equilibrio_sentado+'"]').trigger('click');
	$('#formTinetiInicial').find('input:radio[name="p-2"]').filter('[value="'+datos.levantarse+'"]').trigger('click');
	$('#formTinetiInicial').find('input:radio[name="p-3"]').filter('[value="'+datos.intentos_levantarse+'"]').trigger('click');
	$('#formTinetiInicial').find('input:radio[name="p-4"]').filter('[value="'+datos.equilibrio_bip_inmediata+'"]').trigger('click');
	$('#formTinetiInicial').find('input:radio[name="p-5"]').filter('[value="'+datos.equilibrio_bip+'"]').trigger('click');
	$('#formTinetiInicial').find('input:radio[name="p-6"]').filter('[value="'+datos.empujar+'"]').trigger('click');
	$('#formTinetiInicial').find('input:radio[name="p-7"]').filter('[value="'+datos.ojos_cerrados+'"]').trigger('click');
	$('#formTinetiInicial').find('input:radio[name="p-8"]').filter('[value="'+datos.vuelta_continuo+'"]').trigger('click');
	$('#formTinetiInicial').find('input:radio[name="p-8b"]').filter('[value="'+datos.vuelta_estable+'"]').trigger('click');
	/*if(datos.vuelta_continuo != null) $('#formTinetiInicial').find('input:radio[name="p-8"]').filter('[value="'+datos.vuelta_continuo+'"]').trigger('click');
	if(datos.vuelta_estable != null ){
		var valor = (datos.vuelta_estable == 0) ? 2 : 3;
		$('#formTinetiInicial').find('input:radio[name="p-8"]').filter('[value="'+valor+'"]').trigger('click');
	} */
	$('#formTinetiInicial').find('input:radio[name="p-9"]').filter('[value="'+datos.sentarse+'"]').trigger('click');
}

function setTinetiEquilibrioFinal(datos){
	if(datos.fecha_test != null) $('#TinetiFinal-fecha-encuesta').data("DateTimePicker").date(new Date(datos.fecha_test+" "));
	$('#formTinetiFinal').find('input:radio[name="p-1"]').filter('[value="'+datos.equilibrio_sentado+'"]').trigger('click');
	$('#formTinetiFinal').find('input:radio[name="p-2"]').filter('[value="'+datos.levantarse+'"]').trigger('click');
	$('#formTinetiFinal').find('input:radio[name="p-3"]').filter('[value="'+datos.intentos_levantarse+'"]').trigger('click');
	$('#formTinetiFinal').find('input:radio[name="p-4"]').filter('[value="'+datos.equilibrio_bip_inmediata+'"]').trigger('click');
	$('#formTinetiFinal').find('input:radio[name="p-5"]').filter('[value="'+datos.equilibrio_bip+'"]').trigger('click');
	$('#formTinetiFinal').find('input:radio[name="p-6"]').filter('[value="'+datos.empujar+'"]').trigger('click');
	$('#formTinetiFinal').find('input:radio[name="p-7"]').filter('[value="'+datos.ojos_cerrados+'"]').trigger('click');
	$('#formTinetiFinal').find('input:radio[name="p-8"]').filter('[value="'+datos.vuelta_continuo+'"]').trigger('click');
	$('#formTinetiFinal').find('input:radio[name="p-8b"]').filter('[value="'+datos.vuelta_estable+'"]').trigger('click');
	/*if(datos.vuelta_continuo != null) $('#formTinetiFinal').find('input:radio[name="p-8"]').filter('[value="'+datos.vuelta_continuo+'"]').trigger('click');
	if(datos.vuelta_estable != null ){
		var valor = (datos.vuelta_estable == 0) ? 2 : 3;
		$('#formTinetiFinal').find('input:radio[name="p-8"]').filter('[value="'+valor+'"]').trigger('click');
	} */
	$('#formTinetiFinal').find('input:radio[name="p-9"]').filter('[value="'+datos.sentarse+'"]').trigger('click');
	
}

function setTinetiMarchaInicio(datos){
	$('#formTinetiInicial').find('input:radio[name="p-10"]').filter('[value="'+datos.iniciacion_marcha+'"]').trigger('click');
	/*if(datos.mov_pie_der_sep != null){
		var valor = (datos.mov_pie_der_sep == 0) ? 2 : 3;
		$('#formTinetiInicial').find('input:radio[name="p-11a"]').filter('[value="'+valor+'"]').trigger('click');
	}
	if(datos.mov_pie_izq != null) $('#formTinetiInicial').find('input:radio[name="p-11b"]').filter('[value="'+datos.mov_pie_izq+'"]').trigger('click');
	if(datos.mov_pie_izq_sep != null){
		var valor2 = (datos.mov_pie_izq_sep == 0) ? 2 : 3;
		$('#formTinetiInicial').find('input:radio[name="p-11b"]').filter('[value="'+valor2+'"]').trigger('click');

	}*/
	$('#formTinetiInicial').find('input:radio[name="p-11aa"]').filter('[value="'+datos.mov_pie_der+'"]').trigger('click');
	$('#formTinetiInicial').find('input:radio[name="p-11ab"]').filter('[value="'+datos.mov_pie_der_sep+'"]').trigger('click');
	$('#formTinetiInicial').find('input:radio[name="p-11ba"]').filter('[value="'+datos.mov_pie_izq+'"]').trigger('click');
	$('#formTinetiInicial').find('input:radio[name="p-11bb"]').filter('[value="'+datos.mov_pie_izq_sep+'"]').trigger('click');
	
	$('#formTinetiInicial').find('input:radio[name="p-12"]').filter('[value="'+datos.simetria_paso+'"]').trigger('click');
	$('#formTinetiInicial').find('input:radio[name="p-13"]').filter('[value="'+datos.fluidez_paso+'"]').trigger('click');
	$('#formTinetiInicial').find('input:radio[name="p-14"]').filter('[value="'+datos.trayectoria+'"]').trigger('click');
	$('#formTinetiInicial').find('input:radio[name="p-15"]').filter('[value="'+datos.tronco+'"]').trigger('click');
	$('#formTinetiInicial').find('input:radio[name="p-16"]').filter('[value="'+datos.postura+'"]').trigger('click');
}

function setTinetiMarchaFinal(datos){
	$('#formTinetiFinal').find('input:radio[name="p-10"]').filter('[value="'+datos.iniciacion_marcha+'"]').trigger('click');
		/*if(datos.mov_pie_der != null) $('#formTinetiFinal').find('input:radio[name="p-11a"]').filter('[value="'+datos.mov_pie_der+'"]').trigger('click');
	if(datos.mov_pie_der_sep != null){
		var valor = (datos.mov_pie_der_sep == 2) ? 0 : 1;
		$('#formTinetiFinal').find('input:radio[name="p-11a"]').filter('[value="'+valor+'"]').trigger('click');
	}
	if(datos.mov_pie_izq != null) $('#formTinetiFinal').find('input:radio[name="p-11b"]').filter('[value="'+datos.mov_pie_izq+'"]').trigger('click');
	if(datos.mov_pie_izq_sep != null){
		var valor2 = (datos.mov_pie_izq_sep == 2) ? 0 : 1;
		$('#formTinetiFinal').find('input:radio[name="p-11b"]').filter('[value="'+valor2+'"]').trigger('click');
	}*/
	$('#formTinetiFinal').find('input:radio[name="p-11aa"]').filter('[value="'+datos.mov_pie_der+'"]').trigger('click');
	$('#formTinetiFinal').find('input:radio[name="p-11ab"]').filter('[value="'+datos.mov_pie_der_sep+'"]').trigger('click');
	$('#formTinetiFinal').find('input:radio[name="p-11ba"]').filter('[value="'+datos.mov_pie_izq+'"]').trigger('click');
	$('#formTinetiFinal').find('input:radio[name="p-11bb"]').filter('[value="'+datos.mov_pie_izq_sep+'"]').trigger('click');
	
	$('#formTinetiFinal').find('input:radio[name="p-12"]').filter('[value="'+datos.simetria_paso+'"]').trigger('click');
	$('#formTinetiFinal').find('input:radio[name="p-13"]').filter('[value="'+datos.fluidez_paso+'"]').trigger('click');
	$('#formTinetiFinal').find('input:radio[name="p-14"]').filter('[value="'+datos.trayectoria+'"]').trigger('click');
	$('#formTinetiFinal').find('input:radio[name="p-15"]').filter('[value="'+datos.tronco+'"]').trigger('click');
	$('#formTinetiFinal').find('input:radio[name="p-16"]').filter('[value="'+datos.postura+'"]').trigger('click');
	
}

//se usa
function setCharlsonInicio(datos){
	//console.log("Charlson llenando datos");
	if(datos.fecha_test != null) $('#CharlsonInicial-fecha-encuesta').data("DateTimePicker").date(new Date(datos.fecha_test+" "));
	if(datos.infarto_miocardio) $('#charlsonInicial-p-1').click();
	if(datos.insuficiencia_cardiaca) $('#charlsonInicial-p-2').click();
	if(datos.enf_arterial) $('#charlsonInicial-p-3').click();
	if(datos.enf_cerebrovascular) $('#charlsonInicial-p-4').click();
	if(datos.demencia) $('#charlsonInicial-p-5').click();
	if(datos.enf_respiratoria) $('#charlsonInicial-p-6').click();
	if(datos.enf_tejido) $('#charlsonInicial-p-7').click();
	if(datos.ulcera_gastro) $('#charlsonInicial-p-8').click();
	if(datos.hep_leve) $('#charlsonInicial-p-9').click();
	if(datos.diabetes) $('#charlsonInicial-p-10').click();
	if(datos.hemiplejia) $('#charlsonInicial-p-11').click();
	if(datos.insuficiencia_moderada) $('#charlsonInicial-p-12').click();
	if(datos.diabetes_lesion) $('#charlsonInicial-p-13').click();
	if(datos.tumor) $('#charlsonInicial-p-14').click();
	if(datos.leucemia) $('#charlsonInicial-p-15').click();
	if(datos.linfoma) $('#charlsonInicial-p-16').click();
	if(datos.hepatopatia) $('#charlsonInicial-p-17').click();
	if(datos.tumor_metastasis) $('#charlsonInicial-p-18').click();
	if(datos.sida) $('#charlsonInicial-p-19').click();;
}

function setCharlsonFinal(datos){
	if(datos.fecha_test != null) $('#CharlsonFinal-fecha-encuesta').data("DateTimePicker").date(new Date(datos.fecha_test+" "));
	if(datos.infarto_miocardio) $('#charlsonFinal-p-1').click();
	if(datos.insuficiencia_cardiaca) $('#charlsonFinal-p-2').click();
	if(datos.enf_arterial) $('#charlsonFinal-p-3').click();
	if(datos.enf_cerebrovascular) $('#charlsonFinal-p-4').click();
	if(datos.demencia) $('#charlsonFinal-p-5').click();
	if(datos.enf_respiratoria) $('#charlsonFinal-p-6').click();
	if(datos.enf_tejido) $('#charlsonFinal-p-7').click();
	if(datos.ulcera_gastro) $('#charlsonFinal-p-8').click();
	if(datos.hep_leve) $('#charlsonFinal-p-9').click();
	if(datos.diabetes) $('#charlsonFinal-p-10').click();
	if(datos.hemiplejia) $('#charlsonFinal-p-11').click();
	if(datos.insuficiencia_moderada) $('#charlsonFinal-p-12').click();
	if(datos.diabetes_lesion) $('#charlsonFinal-p-13').click();
	if(datos.tumor) $('#charlsonFinal-p-14').click();
	if(datos.leucemia) $('#charlsonFinal-p-15').click();
	if(datos.linfoma) $('#charlsonFinal-p-16').click();
	if(datos.hepatopatia) $('#charlsonFinal-p-17').click();
	if(datos.tumor_metastasis) $('#charlsonFinal-p-18').click();
	if(datos.sida) $('#charlsonFinal-p-19').click();
}

function setIciqInicio (datos){
	if(datos.fecha_test != null) $('#ICIQ-SFInicial-fecha-encuesta').data("DateTimePicker").date(new Date(datos.fecha_test+" "));
	$('#formICIQ-SFInicial').find('input:radio[name="p-1"]').filter('[value='+datos.frecuencia_pierde_orina+']').trigger('click');
	$('#formICIQ-SFInicial').find('input:radio[name="p-2"]').filter('[value='+datos.cantidad_orina+']').trigger('click');
	$('#formICIQ-SFInicial').find('input:radio[name="p-3"]').filter('[value='+datos.escala_escape+']').trigger('click');

	if(datos.nunca) $('#indiceICIQ-SFInicial-p-22').click();
	if(datos.antes_wc) $('#indiceICIQ-SFInicial-p-23').click();
	if(datos.tose_estornuda) $('#indiceICIQ-SFInicial-p-24').click();
	if(datos.duerme) $('#indiceICIQ-SFInicial-p-25').click();
	if(datos.esfuerzo_ejercicio) $('#indiceICIQ-SFInicial-p-26').click();
	if(datos.vestido) $('#indiceICIQ-SFInicial-p-27').click();
	if(datos.sin_motivo) $('#indiceICIQ-SFInicial-p-28').click();
	if(datos.continua) $('#indiceICIQ-SFInicial-p-29').click();
}

function setIciqFinal (datos){
	if(datos.fecha_test != null) $('#ICIQ-SFFinal-fecha-encuesta').data("DateTimePicker").date(new Date(datos.fecha_test+" "));
	$('#formICIQ-SFFinal').find('input:radio[name="p-1"]').filter('[value='+datos.frecuencia_pierde_orina+']').trigger('click');
	$('#formICIQ-SFFinal').find('input:radio[name="p-2"]').filter('[value='+datos.cantidad_orina+']').trigger('click');
	$('#formICIQ-SFFinal').find('input:radio[name="p-3"]').filter('[value='+datos.escala_escape+']').trigger('click');

	if(datos.nunca) $('#indiceICIQ-SFFinal-p-22').click();
	if(datos.antes_wc) $('#indiceICIQ-SFFinal-p-23').click();
	if(datos.tose_estornuda) $('#indiceICIQ-SFFinal-p-24').click();
	if(datos.duerme) $('#indiceICIQ-SFFinal-p-25').click();
	if(datos.esfuerzo_ejercicio) $('#indiceICIQ-SFFinal-p-26').click();
	if(datos.vestido) $('#indiceICIQ-SFFinal-p-27').click();
	if(datos.sin_motivo) $('#indiceICIQ-SFFinal-p-28').click();
	if(datos.continua) $('#indiceICIQ-SFFinal-p-29').click();
}

function setEvaluacionCognitivaIni (datos){

	$("#EvaluacionCognitiva-fecha-encuesta").val(datos.fecha_test);
	$("#primer_numero").val(datos.primer_numero);
	$("#segundo_numero").val(datos.segundo_numero);
	$("#tercer_numero").val(datos.tercer_numero);
	$("#cuarto_numero").val(datos.cuarto_numero);
	$("#quinto_numero").val(datos.quinto_numero);
	$("#repeticiones").val(datos.numero_repeticiones_palabras);


	$('#formEvaluacionCognitivaInicial').find('input:radio[name="p-1"]').filter('[value='+datos.mes+']').prop("checked", true);
	$('#formEvaluacionCognitivaInicial').find('input:radio[name="p-2"]').filter('[value='+datos.dia_mes+']').prop("checked", true);
	$('#formEvaluacionCognitivaInicial').find('input:radio[name="p-3"]').filter('[value='+datos.anio+']').prop("checked", true);
	$('#formEvaluacionCognitivaInicial').find('input:radio[name="p-4"]').filter('[value='+datos.dia_semana+']').prop("checked", true);

	$('#formEvaluacionCognitivaInicial').find('input:radio[name="p-5"]').filter('[value='+datos.arbol1+']').prop("checked", true);
	$('#formEvaluacionCognitivaInicial').find('input:radio[name="p-6"]').filter('[value='+datos.mesa1+']').prop("checked", true);
	$('#formEvaluacionCognitivaInicial').find('input:radio[name="p-7"]').filter('[value='+datos.avion1+']').prop("checked", true);

	$('#formEvaluacionCognitivaInicial').find('input:radio[name="p-8"]').filter('[value='+datos.toma_papel+']').prop("checked", true);
	$('#formEvaluacionCognitivaInicial').find('input:radio[name="p-9"]').filter('[value='+datos.dobla_papel+']').prop("checked", true);
	$('#formEvaluacionCognitivaInicial').find('input:radio[name="p-10"]').filter('[value='+datos.coloca_papel+']').prop("checked", true);


	$('#formEvaluacionCognitivaInicial').find('input:radio[name="p-11"]').filter('[value='+datos.arbol2+']').prop("checked", true);
	$('#formEvaluacionCognitivaInicial').find('input:radio[name="p-12"]').filter('[value='+datos.mesa2+']').prop("checked", true);
	$('#formEvaluacionCognitivaInicial').find('input:radio[name="p-13"]').filter('[value='+datos.avion2+']').prop("checked", true);

	$('#formEvaluacionCognitivaInicial').find('input:radio[name="p-14"]').filter('[value='+datos.circulos+']').prop("checked", true);
}


function setEvaluacionCognitivaFin (datos){

	$("#EvaluacionCognitivaFin-fecha-encuesta").val(datos.fecha_test);
	$("#primer_numeroF").val(datos.primer_numero);
	$("#segundo_numeroF").val(datos.segundo_numero);
	$("#tercer_numeroF").val(datos.tercer_numero);
	$("#cuarto_numeroF").val(datos.cuarto_numero);
	$("#quinto_numeroF").val(datos.quinto_numero);
	$("#repeticionesF").val(datos.numero_repeticiones_palabras);


	$('#formEvaluacionCognitivaFinal').find('input:radio[name="p-1"]').filter('[value='+datos.mes+']').prop("checked", true);
	$('#formEvaluacionCognitivaFinal').find('input:radio[name="p-2"]').filter('[value='+datos.dia_mes+']').prop("checked", true);
	$('#formEvaluacionCognitivaFinal').find('input:radio[name="p-3"]').filter('[value='+datos.anio+']').prop("checked", true);
	$('#formEvaluacionCognitivaFinal').find('input:radio[name="p-4"]').filter('[value='+datos.dia_semana+']').prop("checked", true);

	$('#formEvaluacionCognitivaFinal').find('input:radio[name="p-5"]').filter('[value='+datos.arbol1+']').prop("checked", true);
	$('#formEvaluacionCognitivaFinal').find('input:radio[name="p-6"]').filter('[value='+datos.mesa1+']').prop("checked", true);
	$('#formEvaluacionCognitivaFinal').find('input:radio[name="p-7"]').filter('[value='+datos.avion1+']').prop("checked", true);

	$('#formEvaluacionCognitivaFinal').find('input:radio[name="p-8"]').filter('[value='+datos.toma_papel+']').prop("checked", true);
	$('#formEvaluacionCognitivaFinal').find('input:radio[name="p-9"]').filter('[value='+datos.dobla_papel+']').prop("checked", true);
	$('#formEvaluacionCognitivaFinal').find('input:radio[name="p-10"]').filter('[value='+datos.coloca_papel+']').prop("checked", true);


	$('#formEvaluacionCognitivaFinal').find('input:radio[name="p-11"]').filter('[value='+datos.arbol2+']').prop("checked", true);
	$('#formEvaluacionCognitivaFinal').find('input:radio[name="p-12"]').filter('[value='+datos.mesa2+']').prop("checked", true);
	$('#formEvaluacionCognitivaFinal').find('input:radio[name="p-13"]').filter('[value='+datos.avion2+']').prop("checked", true);

	$('#formEvaluacionCognitivaFinal').find('input:radio[name="p-14"]').filter('[value='+datos.circulos+']').prop("checked", true);
}

function setGDSInicial(datos){

	$("#formularioGDS #fecha_gds").val(moment(datos.fecha_test).format("DD-MM-YYYY"));
	
	$('#formularioGDS').find('input[name="gds"][value='+datos.gds+']').prop("checked", true);
	
}
function setGDSFinal(datos){

	$("#formularioGDSFinal #fecha_gds").val(moment(datos.fecha_test).format("DD-MM-YYYY"));
	
	$('#formularioGDSFinal').find('input[name="gds"][value='+datos.gds+']').prop("checked", true);
	
}
function setConductaMotrizInicial(datos){

	$("#EvaluacionConductaMotrizInicial-fecha-encuesta").val(moment(datos.fecha_test).format("DD-MM-YYYY"));
	
	$('#formConductaMotrizInicial').find('input:radio[name="p-0"]').filter('[value='+datos.conducta_anomala+']').prop("checked", true);
	$('#formConductaMotrizInicial').find('input:radio[name="p-1"]').filter('[value='+datos.preg1_npi+']').prop("checked", true);
	$('#formConductaMotrizInicial').find('input:radio[name="p-2"]').filter('[value='+datos.preg2_npi+']').prop("checked", true);
	$('#formConductaMotrizInicial').find('input:radio[name="p-3"]').filter('[value='+datos.preg3_npi+']').prop("checked", true);

	$('#formConductaMotrizInicial').find('input:radio[name="p-4"]').filter('[value='+datos.preg4_npi+']').prop("checked", true);
	$('#formConductaMotrizInicial').find('input:radio[name="p-5"]').filter('[value='+datos.preg5_npi+']').prop("checked", true);
	$('#formConductaMotrizInicial').find('input:radio[name="p-6"]').filter('[value='+datos.preg6_npi+']').prop("checked", true);

	$('#formConductaMotrizInicial').find('input:radio[name="p-7"]').filter('[value='+datos.preg7_npi+']').prop("checked", true);
	$('#formConductaMotrizInicial').find('input:radio[name="p-8"]').filter('[value='+datos.frecuencia_npi+']').prop("checked", true);
	$('#formConductaMotrizInicial').find('input:radio[name="p-9"]').filter('[value='+datos.gravedad_npi+']').prop("checked", true);
	$('#formConductaMotrizInicial').find('input:radio[name="p-10"]').filter('[value='+datos.angustia_npi+']').prop("checked", true);
	
}
function setConductaMotrizFinal(datos){

	$("#EvaluacionConductaMotrizFinal-fecha-encuesta").val(moment(datos.fecha_test).format("DD-MM-YYYY"));
	
	$('#formConductaMotrizFinal').find('input:radio[name="p-0"]').filter('[value='+datos.conducta_anomala+']').prop("checked", true);
	$('#formConductaMotrizFinal').find('input:radio[name="p-1"]').filter('[value='+datos.preg1_npi+']').prop("checked", true);
	$('#formConductaMotrizFinal').find('input:radio[name="p-2"]').filter('[value='+datos.preg2_npi+']').prop("checked", true);
	$('#formConductaMotrizFinal').find('input:radio[name="p-3"]').filter('[value='+datos.preg3_npi+']').prop("checked", true);

	$('#formConductaMotrizFinal').find('input:radio[name="p-4"]').filter('[value='+datos.preg4_npi+']').prop("checked", true);
	$('#formConductaMotrizFinal').find('input:radio[name="p-5"]').filter('[value='+datos.preg5_npi+']').prop("checked", true);
	$('#formConductaMotrizFinal').find('input:radio[name="p-6"]').filter('[value='+datos.preg6_npi+']').prop("checked", true);

	$('#formConductaMotrizFinal').find('input:radio[name="p-7"]').filter('[value='+datos.preg7_npi+']').prop("checked", true);
	$('#formConductaMotrizFinal').find('input:radio[name="p-8"]').filter('[value='+datos.frecuencia_npi+']').prop("checked", true);
	$('#formConductaMotrizFinal').find('input:radio[name="p-9"]').filter('[value='+datos.gravedad_npi+']').prop("checked", true);
	$('#formConductaMotrizFinal').find('input:radio[name="p-10"]').filter('[value='+datos.angustia_npi+']').prop("checked", true);
	
}

//se usa
function setWhoqol(datos){
	//console.log("whoqol llenando datos");
	$("#Whoqol-fecha-encuesta").val(moment(datos.fecha_test).format("DD-MM-YYYY"));
	$('#formWhoqol').find('input:radio[name="whoqol-1"]').filter('[value='+datos.apoyo_amigos+']').prop("checked", true);
	$('#formWhoqol').find('input:radio[name="whoqol-2"]').filter('[value='+datos.calidad_vida+']').prop("checked", true);
	$('#formWhoqol').find('input:radio[name="whoqol-3"]').filter('[value='+datos.salud+']').prop("checked", true);
	$('#formWhoqol').find('input:radio[name="whoqol-4"]').filter('[value='+datos.dolor+']').prop("checked", true);
	$('#formWhoqol').find('input:radio[name="whoqol-5"]').filter('[value='+datos.calidad_vida+']').prop("checked", true);
	$('#formWhoqol').find('input:radio[name="whoqol-6"]').filter('[value='+datos.alegria_vida+']').prop("checked", true);
	$('#formWhoqol').find('input:radio[name="whoqol-7"]').filter('[value='+datos.sentido_vida+']').prop("checked", true);
	$('#formWhoqol').find('input:radio[name="whoqol-8"]').filter('[value='+datos.concentracion+']').prop("checked", true);
	$('#formWhoqol').find('input:radio[name="whoqol-9"]').filter('[value='+datos.seguridad+']').prop("checked", true);
	$('#formWhoqol').find('input:radio[name="whoqol-10"]').filter('[value='+datos.ambiente_saludable+']').prop("checked", true);
	$('#formWhoqol').find('input:radio[name="whoqol-11"]').filter('[value='+datos.energia+']').prop("checked", true);
	$('#formWhoqol').find('input:radio[name="whoqol-12"]').filter('[value='+datos.apariencia_fisica+']').prop("checked", true);
	$('#formWhoqol').find('input:radio[name="whoqol-13"]').filter('[value='+datos.dinero+']').prop("checked", true);
	$('#formWhoqol').find('input:radio[name="whoqol-14"]').filter('[value='+datos.informacion+']').prop("checked", true);
	$('#formWhoqol').find('input:radio[name="whoqol-15"]').filter('[value='+datos.actividad_ocio+']').prop("checked", true);
	$('#formWhoqol').find('input:radio[name="whoqol-16"]').filter('[value='+datos.desplazamiento+']').prop("checked", true);
	$('#formWhoqol').find('input:radio[name="whoqol-17"]').filter('[value='+datos.sueno+']').prop("checked", true);
	$('#formWhoqol').find('input:radio[name="whoqol-18"]').filter('[value='+datos.actividad_diaria+']').prop("checked", true);
	$('#formWhoqol').find('input:radio[name="whoqol-19"]').filter('[value='+datos.capacidad_trabajo+']').prop("checked", true);
	$('#formWhoqol').find('input:radio[name="whoqol-20"]').filter('[value='+datos.mismo+']').prop("checked", true);
	$('#formWhoqol').find('input:radio[name="whoqol-21"]').filter('[value='+datos.relaciones_personales+']').prop("checked", true);
	$('#formWhoqol').find('input:radio[name="whoqol-22"]').filter('[value='+datos.vida_sexual+']').prop("checked", true);
	$('#formWhoqol').find('input:radio[name="whoqol-23"]').filter('[value='+datos.apoyo_amigos+']').prop("checked", true);
	$('#formWhoqol').find('input:radio[name="whoqol-24"]').filter('[value='+datos.lugar_vive+']').prop("checked", true);
	$('#formWhoqol').find('input:radio[name="whoqol-25"]').filter('[value='+datos.servicio_sanitario+']').prop("checked", true);
	$('#formWhoqol').find('input:radio[name="whoqol-26"]').filter('[value='+datos.transporte+']').prop("checked", true);
	$('#formWhoqol').find('input:radio[name="whoqol-27"]').filter('[value='+datos.sentimiento_negativo+']').prop("checked", true);

	$("#ayuda").val(datos.ayuda);
	$("#tiempo").val(datos.tiempo);
	$("#comentario").val(datos.comentario);
}


//se usa
function setMmseInicial(datos){
	console.log("mmse llenando datos");
	console.log("inicial"+datos);
	$("#EvaluacionMmseInicial-fecha-encuesta").val(moment(datos.fecha_test).format("DD-MM-YYYY"));
	$('#formMmseInicial').find('input:radio[name="p-1"]').filter('[value='+datos.fecha+']').prop("checked", true);
	$('#formMmseInicial').find('input:radio[name="p-2"]').filter('[value='+datos.mes+']').prop("checked", true);
	$('#formMmseInicial').find('input:radio[name="p-3"]').filter('[value='+datos.dia_semana+']').prop("checked", true);
	$('#formMmseInicial').find('input:radio[name="p-4"]').filter('[value='+datos.anio+']').prop("checked", true);
	$('#formMmseInicial').find('input:radio[name="p-5"]').filter('[value='+datos.estacion+']').prop("checked", true);
	$('#formMmseInicial').find('input:radio[name="p-6"]').filter('[value='+datos.lugar+']').prop("checked", true);
	$('#formMmseInicial').find('input:radio[name="p-7"]').filter('[value='+datos.piso+']').prop("checked", true);
	$('#formMmseInicial').find('input:radio[name="p-8"]').filter('[value='+datos.ciudad+']').prop("checked", true);
	$('#formMmseInicial').find('input:radio[name="p-9"]').filter('[value='+datos.comuna+']').prop("checked", true);
	$('#formMmseInicial').find('input:radio[name="p-10"]').filter('[value='+datos.pais+']').prop("checked", true);
	$('#formMmseInicial').find('input:radio[name="p-11"]').filter('[value='+datos.arbol1+']').prop("checked", true);
	$('#formMmseInicial').find('input:radio[name="p-12"]').filter('[value='+datos.mesa1+']').prop("checked", true);
	$('#formMmseInicial').find('input:radio[name="p-13"]').filter('[value='+datos.perro1+']').prop("checked", true);
	$('#formMmseInicial').find('input:radio[name="p-14"]').filter('[value='+datos.letra_o+']').prop("checked", true);
	$('#formMmseInicial').find('input:radio[name="p-15"]').filter('[value='+datos.letra_d+']').prop("checked", true);
	$('#formMmseInicial').find('input:radio[name="p-16"]').filter('[value='+datos.letra_n+']').prop("checked", true);
	$('#formMmseInicial').find('input:radio[name="p-17"]').filter('[value='+datos.letra_u+']').prop("checked", true);
	$('#formMmseInicial').find('input:radio[name="p-18"]').filter('[value='+datos.letra_m+']').prop("checked", true);
	$('#formMmseInicial').find('input:radio[name="p-19"]').filter('[value='+datos.arbol2+']').prop("checked", true);
	$('#formMmseInicial').find('input:radio[name="p-20"]').filter('[value='+datos.mesa2+']').prop("checked", true);
	$('#formMmseInicial').find('input:radio[name="p-21"]').filter('[value='+datos.perro2+']').prop("checked", true);
	$('#formMmseInicial').find('input:radio[name="p-22"]').filter('[value='+datos.lapiz+']').prop("checked", true);
	$('#formMmseInicial').find('input:radio[name="p-23"]').filter('[value='+datos.reloj+']').prop("checked", true);
	$('#formMmseInicial').find('input:radio[name="p-24"]').filter('[value='+datos.repetir_frase+']').prop("checked", true);
	$('#formMmseInicial').find('input:radio[name="p-25"]').filter('[value='+datos.tomar_papel+']').prop("checked", true);
	$('#formMmseInicial').find('input:radio[name="p-26"]').filter('[value='+datos.doblar_papel+']').prop("checked", true);
	$('#formMmseInicial').find('input:radio[name="p-27"]').filter('[value='+datos.dejar_papel+']').prop("checked", true);
	$('#formMmseInicial').find('input:radio[name="p-28"]').filter('[value='+datos.leer_obedecer+']').prop("checked", true);
	$('#formMmseInicial').find('input:radio[name="p-29"]').filter('[value='+datos.escribir_frase+']').prop("checked", true);
	$('#formMmseInicial').find('input:radio[name="p-30"]').filter('[value='+datos.hacer_dibujo+']').prop("checked", true);
	$('#formMmseInicial').find('input[id="EvaluacionMmseInicial-repeticion1"]').val(datos.numero_repeticiones_palabras);

	console.log(datos.numero_repeticiones_palabras);

}

//se usa
function setWhoqolA3(datos){
	console.log("whoqolA3 llenando datos");
	$("#WhoqolA3-fecha-encuesta").val(moment(datos.fecha_test).format("DD-MM-YYYY"));
	$('#formWhoqolA3').find('input:radio[name="whoqolA3-1"]').filter('[value='+datos.apoyo_amigos+']').prop("checked", true);
	$('#formWhoqolA3').find('input:radio[name="whoqolA3-2"]').filter('[value='+datos.calidad_vida+']').prop("checked", true);
	$('#formWhoqolA3').find('input:radio[name="whoqolA3-3"]').filter('[value='+datos.salud+']').prop("checked", true);
	$('#formWhoqolA3').find('input:radio[name="whoqolA3-4"]').filter('[value='+datos.dolor+']').prop("checked", true);
	$('#formWhoqolA3').find('input:radio[name="whoqolA3-5"]').filter('[value='+datos.calidad_vida+']').prop("checked", true);
	$('#formWhoqolA3').find('input:radio[name="whoqolA3-6"]').filter('[value='+datos.alegria_vida+']').prop("checked", true);
	$('#formWhoqolA3').find('input:radio[name="whoqolA3-7"]').filter('[value='+datos.sentido_vida+']').prop("checked", true);
	$('#formWhoqolA3').find('input:radio[name="whoqolA3-8"]').filter('[value='+datos.concentracion+']').prop("checked", true);
	$('#formWhoqolA3').find('input:radio[name="whoqolA3-9"]').filter('[value='+datos.seguridad+']').prop("checked", true);
	$('#formWhoqolA3').find('input:radio[name="whoqolA3-10"]').filter('[value='+datos.ambiente_saludable+']').prop("checked", true);
	$('#formWhoqolA3').find('input:radio[name="whoqolA3-11"]').filter('[value='+datos.energia+']').prop("checked", true);
	$('#formWhoqolA3').find('input:radio[name="whoqolA3-12"]').filter('[value='+datos.apariencia_fisica+']').prop("checked", true);
	$('#formWhoqolA3').find('input:radio[name="whoqolA3-13"]').filter('[value='+datos.dinero+']').prop("checked", true);
	$('#formWhoqolA3').find('input:radio[name="whoqolA3-14"]').filter('[value='+datos.informacion+']').prop("checked", true);
	$('#formWhoqolA3').find('input:radio[name="whoqolA3-15"]').filter('[value='+datos.actividad_ocio+']').prop("checked", true);
	$('#formWhoqolA3').find('input:radio[name="whoqolA3-16"]').filter('[value='+datos.desplazamiento+']').prop("checked", true);
	$('#formWhoqolA3').find('input:radio[name="whoqolA3-17"]').filter('[value='+datos.sueno+']').prop("checked", true);
	$('#formWhoqolA3').find('input:radio[name="whoqolA3-18"]').filter('[value='+datos.actividad_diaria+']').prop("checked", true);
	$('#formWhoqolA3').find('input:radio[name="whoqolA3-19"]').filter('[value='+datos.capacidad_trabajo+']').prop("checked", true);
	$('#formWhoqolA3').find('input:radio[name="whoqolA3-20"]').filter('[value='+datos.mismo+']').prop("checked", true);
	$('#formWhoqolA3').find('input:radio[name="whoqolA3-21"]').filter('[value='+datos.relaciones_personales+']').prop("checked", true);
	$('#formWhoqolA3').find('input:radio[name="whoqolA3-22"]').filter('[value='+datos.vida_sexual+']').prop("checked", true);
	$('#formWhoqolA3').find('input:radio[name="whoqolA3-23"]').filter('[value='+datos.apoyo_amigos+']').prop("checked", true);
	$('#formWhoqolA3').find('input:radio[name="whoqolA3-24"]').filter('[value='+datos.lugar_vive+']').prop("checked", true);
	$('#formWhoqolA3').find('input:radio[name="whoqolA3-25"]').filter('[value='+datos.servicio_sanitario+']').prop("checked", true);
	$('#formWhoqolA3').find('input:radio[name="whoqolA3-26"]').filter('[value='+datos.transporte+']').prop("checked", true);
	$('#formWhoqolA3').find('input:radio[name="whoqolA3-27"]').filter('[value='+datos.sentimiento_negativo+']').prop("checked", true);

	$("#ayudaA3").val(datos.ayuda);
	$("#tiempoA3").val(datos.tiempo);
	$("#comentarioA3").val(datos.comentario);
}

function setMmseFinal(datos){

	console.log("FINAL"+datos);
	$("#EvaluacionMmseFinal-fecha-encuesta").val(moment(datos.fecha_test).format("DD-MM-YYYY"));
	$('#formMmseFinal').find('input:radio[name="p-1"]').filter('[value='+datos.fecha+']').prop("checked", true);
	$('#formMmseFinal').find('input:radio[name="p-2"]').filter('[value='+datos.mes+']').prop("checked", true);
	$('#formMmseFinal').find('input:radio[name="p-3"]').filter('[value='+datos.dia_semana+']').prop("checked", true);
	$('#formMmseFinal').find('input:radio[name="p-4"]').filter('[value='+datos.anio+']').prop("checked", true);
	$('#formMmseFinal').find('input:radio[name="p-5"]').filter('[value='+datos.estacion+']').prop("checked", true);
	$('#formMmseFinal').find('input:radio[name="p-6"]').filter('[value='+datos.lugar+']').prop("checked", true);
	$('#formMmseFinal').find('input:radio[name="p-7"]').filter('[value='+datos.piso+']').prop("checked", true);
	$('#formMmseFinal').find('input:radio[name="p-8"]').filter('[value='+datos.ciudad+']').prop("checked", true);
	$('#formMmseFinal').find('input:radio[name="p-9"]').filter('[value='+datos.comuna+']').prop("checked", true);
	$('#formMmseFinal').find('input:radio[name="p-10"]').filter('[value='+datos.pais+']').prop("checked", true);
	$('#formMmseFinal').find('input:radio[name="p-11"]').filter('[value='+datos.arbol1+']').prop("checked", true);
	$('#formMmseFinal').find('input:radio[name="p-12"]').filter('[value='+datos.mesa1+']').prop("checked", true);
	$('#formMmseFinal').find('input:radio[name="p-13"]').filter('[value='+datos.perro1+']').prop("checked", true);
	$('#formMmseFinal').find('input:radio[name="p-14"]').filter('[value='+datos.letra_o+']').prop("checked", true);
	$('#formMmseFinal').find('input:radio[name="p-15"]').filter('[value='+datos.letra_d+']').prop("checked", true);
	$('#formMmseFinal').find('input:radio[name="p-16"]').filter('[value='+datos.letra_n+']').prop("checked", true);
	$('#formMmseFinal').find('input:radio[name="p-17"]').filter('[value='+datos.letra_u+']').prop("checked", true);
	$('#formMmseFinal').find('input:radio[name="p-18"]').filter('[value='+datos.letra_m+']').prop("checked", true);
	$('#formMmseFinal').find('input:radio[name="p-19"]').filter('[value='+datos.arbol2+']').prop("checked", true);
	$('#formMmseFinal').find('input:radio[name="p-20"]').filter('[value='+datos.mesa2+']').prop("checked", true);
	$('#formMmseFinal').find('input:radio[name="p-21"]').filter('[value='+datos.perro2+']').prop("checked", true);
	$('#formMmseFinal').find('input:radio[name="p-22"]').filter('[value='+datos.lapiz+']').prop("checked", true);
	$('#formMmseFinal').find('input:radio[name="p-23"]').filter('[value='+datos.reloj+']').prop("checked", true);
	$('#formMmseFinal').find('input:radio[name="p-24"]').filter('[value='+datos.repetir_frase+']').prop("checked", true);
	$('#formMmseFinal').find('input:radio[name="p-25"]').filter('[value='+datos.tomar_papel+']').prop("checked", true);
	$('#formMmseFinal').find('input:radio[name="p-26"]').filter('[value='+datos.doblar_papel+']').prop("checked", true);
	$('#formMmseFinal').find('input:radio[name="p-27"]').filter('[value='+datos.dejar_papel+']').prop("checked", true);
	$('#formMmseFinal').find('input:radio[name="p-28"]').filter('[value='+datos.leer_obedecer+']').prop("checked", true);
	$('#formMmseFinal').find('input:radio[name="p-29"]').filter('[value='+datos.escribir_frase+']').prop("checked", true);
	$('#formMmseFinal').find('input:radio[name="p-30"]').filter('[value='+datos.hacer_dibujo+']').prop("checked", true);
	$('#formMmseFinal').find('input[id="EvaluacionMmseFinal-repeticion1"]').val(datos.numero_repeticiones_palabras);
	console.log(datos.numero_repeticiones_palabras);


}

function setInformeAccionesRepetidas(datos){
$('#formConclusion').find('input[name="conclusion"][value='+datos.tiene_acciones_repetidas+']').prop("checked", true);
}
