var growl = {
      danger: function(msg){
             $.growl("<span class='msg-growl'>"+msg+"</span>", {
                    type: "danger",
                    animate: {
                           enter: 'animated fadeInRight',
                           exit: 'animated fadeOutRight'
                   },
                   delay: 5000
           });
     },
     success: function(msg){
             $.growl("<span class='msg-growl'>"+msg+"</span>", {
                    type: "success",
                    animate: {
                           enter: 'animated fadeInRight',
                           exit: 'animated fadeOutRight'
                   },
                   delay: 5000
           });
     },
     info: function(msg){
             $.growl("<span class='msg-growl'>"+msg+"</span>", {
                    type: "info",
                    animate: {
                           enter: 'animated fadeInRight',
                           exit: 'animated fadeOutRight'
                   },
                   delay: 5000
           });
     },
     alert: function(msg){
             $.growl("<span class='msg-growl'>"+msg+"</span>", {
                    type: "alert",
                    animate: {
                           enter: 'animated fadeInRight',
                           exit: 'animated fadeOutRight'
                   },
                   delay: 5000
           });
     },
     warning: function(msg){
             $.growl("<span class='msg-growl'>"+msg+"</span>", {
                    type: "warning",
                    animate: {
                           enter: 'animated fadeInRight',
                           exit: 'animated fadeOutRight'
                   },
                   delay: 5000
           });
     }
}