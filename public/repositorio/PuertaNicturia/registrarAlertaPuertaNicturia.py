# -*- coding: utf-8 -*-
import os,time,socket,json,urllib,urllib2
import sys

if len(sys.argv) >= 1:
    tipo = sys.argsv[1]
    for root, dirs, files in os.walk(tipo):
        for name in files:
            listDet = []
            archivo= open(os.path.join(root,name), "r")  
            for linea in archivo.readlines():
                hora,fecha,valor = linea.split(';')
                listDet.append(time.strftime("%y")+fecha[2:4]+fecha[0:2]+' '+hora)
            archivo.close()    
            parametros = urllib.urlencode({
                "tipo": tipo,
                'fecha_envio' : time.strftime("%Y-%m-%d %H:%M:%S"),
                'id_paciente' : name[:-4],
                'listado_eventos' : listDet
            })
                
            try: 
                respuesta = urllib2.urlopen(url='http://ehomeseniors.cl/enlace/registrarAlertaPuertaNicturia',data=parametros,timeout = 30).read()
                datResp = json.loads(respuesta)
            except urllib2.URLError, e: 
                #entra si no encuentra el servidor
                print e.reason
            except socket.timeout as e:
                #si el servidor no responde en una cantidad de segundos
                print e
else:
    print "Este programa necesita un parametro";

    
        