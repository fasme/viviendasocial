<?php namespace App\util{
use DateTime;
use Mail;
class Funciones {

	public static function soloRut($rut){
		$rut = substr($rut, 0, -1);
		$rut = str_replace(".", "", $rut);
		$rut = str_replace("-", "", $rut);
		return $rut;
	}

	public static function nombreMes($id){
		if($id==1) return 'Enero';
		if($id==2) return 'Febrero';
		if($id==3) return 'Marzo';
		if($id==4) return 'Abril';
		if($id==5) return 'Mayo';
		if($id==6) return 'Junio';
		if($id==7) return 'Julio';
		if($id==8) return 'Agosto';
		if($id==9) return 'Septiembre';
		if($id==10) return 'Octubre';
		if($id==11) return 'Noviembre';
		if($id==12) return 'Diciembre';
	}

	public static function selectRut(){
		return [
		"0" => 0,
		"1" => 1,
		"2" => 2,
		"3" => 3,
		"4" => 4,
		"5" => 5,
		"6" => 6,
		"7" => 7,
		"8" => 8,
		"9" => 9,
		"K" => "K",
		"" => ""
		];
	}

	public static function obtenerDV($rut) {
		$rut = strrev($rut);
		$aux = 1;
		$s=0;
		for($i=0;$i<strlen($rut);$i++){
			$aux++;
			$s += intval($rut[$i])*$aux;
			if($aux == 7) $aux=1;
		}
		$digit = 11-$s%11;
		if($digit == 11) return 0;
		if($digit == 10) return "K";
		return $digit;
	}

	public static function calcularEdad($fecha) {
		if(empty($fecha) || is_null($fecha)) return 0;
		$fecha=date("d-m-Y", strtotime($fecha));
		list($d, $m, $Y) = explode("-", $fecha);
		return( date("md") < $m.$d ? date("Y")-$Y-1 : date("Y")-$Y );
	}

	public static function toArray($datos){
		$response=[];
		foreach ($datos as $key => $value) {
			$fila=[];
			foreach ($datos[$key] as $att => $valor) {
				$fila[]=$valor;
			}
			$response[]=$fila;
		}
		return $response;
	}

	public static function quitarCaracteres($string){
		$string = trim($string);

		$string = str_replace(
			array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
			array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
			$string
		);

		$string = str_replace(
			array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
			array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
			$string
		);

		$string = str_replace(
			array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
			array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
			$string
		);

		$string = str_replace(
			array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
			array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
			$string
		);

		$string = str_replace(
			array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
			array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
			$string
		);

		$string = str_replace(
			array('ñ', 'Ñ', 'ç', 'Ç'),
			array('n', 'N', 'c', 'C',),
			$string
		);

		$string = str_replace(
			array("\\", "¨", "º", "-", "~",
				"#", "@", "|", "!", "\"",
				"·", "$", "%", "&", "/",
				"(", ")", "?", "'", "¡",
				"¿", "[", "^", "`", "]",
				"+", "}", "{", "¨", "´",
				">", "< ", ";", ",", ":",
				".", " "),
			'',
			$string
		);

		return $string;
	}

	public static function objectToArray($objeto){
		return json_decode(json_encode($objeto), true);
	}

	public static function selectGenero(){
		return [
		"M" => "Masculino",
		"F" => "Femenino",
		"D" => "Desconocido"
		];
	}

	public static function obtenerGenero($genero){
		if($genero == "M") return "Hombre";
		if($genero == "F") return "Mujer";
		if($genero == "D") return "Desconocido";
		return "";
	}

	public static function abreviarMes($mes){
		if($mes == 1) return "ENE";
		if($mes == 2) return "FEB";
		if($mes == 3) return "MAR";
		if($mes == 4) return "ABR";
		if($mes == 5) return "MAY";
		if($mes == 6) return "JUN";
		if($mes == 7) return "JUL";
		if($mes == 8) return "AGO";
		if($mes == 9) return "SEP";
		if($mes == 10) return "OCT";
		if($mes == 11) return "NOV";
		return "DIC";
	}

	public static function abreviarFecha($fecha){
		$dia = date("d", strtotime($fecha));
		$mes = date("m", strtotime($fecha));
		return $dia." - ".Funciones::abreviarMes($mes);
	}

	public static function comprarPorFecha($x, $y){

		if (date("Y-m-d His", strtotime($x['fecha'])) == date("Y-m-d His", strtotime($y['fecha'])))  return 0;
        if (date("Y-m-d His", strtotime($x['fecha'])) > date("Y-m-d His", strtotime($y['fecha'])))  return -1;
        return 1;
	}

	public static function compararDates($fecha, $fechaB){
		if($fecha[1] == $fechaB[1]) return 0;
		return strtotime($fecha[1]) - strtotime($fechaB[1]);
	}

	public static function ordernarEventosPuertas($arreglo){
		usort($arreglo, array('Funciones','compararDates'));
		return $arreglo;
	}

	public static function ordenar($arreglo){
		usort($arreglo, array('Funciones','comprarPorFecha'));
		return $arreglo;
	}

	private static function enviarNotificacionAndroid($idGcms, $mensaje){
		//$GOOGLE_API_KEY = "AIzaSyA7sMkn2PEt1j3MHN_KRV3-J1UYZ0qaDm8";
		$GOOGLE_API_KEY = "AIzaSyA-1iZYh0XmfdAcAlSTqjB_Epo-wZ4Ri_M";


		//$registrationIds = array( "c_fpxfIWIyw:APA91bHeAgFePbz-yxnY81FZD54nYT2Y7w9sKYCbkCeoyn6aNtsbsBaIEO_JR6mvf2g9XMdIzUJrlbESjQpQsN4ZADwFCqb9AVuh4fFaj6ewTG4J4x0uDUYJm7VYf0-7QhqVucL_d6my" );
		// prep the bundle
		//$registrationIds = array( "dh6I8epHUaQ:APA91bFYxnYjnrqxBmAtV2vyR88MYA7hCE04g9I2m6RksDcC0msD6NVKN8qoBcNVykPiU4w0Rt4NDbOEDxJQp05QIbezNARNaa7eS-KbqmcYAbUM-fdUyLojhpE4syGBftCJhN963N2P" );

		$msg = array
		(
		  'message'   => $mensaje,
		  'title'   => 'Notificación de caida',
		  'subtitle'  => 'dd-mm-aaaa hh:mm:ss',
		  'tickerText'  => 'Ticker text here...Ticker text here...Ticker text here',
		  'vibrate' => 1,
		  'sound'   => 1,
		  'largeIcon' => 'sociales',
		  'smallIcon' => 'sociales',
		  'iconColor' => 'gray'
		);
		$fields = array
		(
		  'registration_ids'  => $idGcms,
		  'data'      => $msg
		);

		$headers = array
		(
		  'Authorization: key=' . $GOOGLE_API_KEY,
		  'Content-Type: application/json'
		);

		$ch = curl_init();
		curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
		curl_setopt( $ch,CURLOPT_POST, true );
		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		$result = curl_exec($ch );
		curl_close( $ch );


		echo $result;
	}

	public static function enviarNotificacionIOS1($registatoin_ids, $message) {
		$passphrase = 'ehome';

		$ctx = stream_context_create();

		stream_context_set_option($ctx, 'ssl', 'local_cert', 'push/ck.pem');

		stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

		$fp = stream_socket_client(
		'ssl://gateway.push.apple.com:2195', $err,
		$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
		//
		if (!$fp)
			exit("Failed to connect: $err $errstr" . PHP_EOL);

		$body['aps'] = array(
			'alert' => $message,
			'sound' => 'default'
		);
		$payload = json_encode($body);

		//for ($i=0; $i < count($registatoin_ids) ; $i++) {

			$msg = chr(0) . pack('n', 32) . pack('H*', $registatoin_ids) . pack('n', strlen($payload)) . $payload;
			$result = fwrite($fp, $msg, strlen($msg));
			/*if (!$result)
				trigger_error('Message not delivered - ' . $message);
			else
				trigger_error ('Message successfully delivered' . $message);*/
		//}


	}


    public static function enviarNotificacionIOS($registatoin_ids, $message) {
            $pem1 = 'push/ck.pem';
            $pem2 = 'push/ck.pem';
            $passphrase = 'ehome';

            $json = Array(
                        'aps' => Array(
                                    'badge' => 0,
                                    'alert' => $message,
                                    'sound' => 'default'
                        )
            );
            $payload = json_encode($json);

            $logs = Array();
      $gateway = 'ssl://gateway.sandbox.push.apple.com:2195';
      for($i=0;$i<1;$i++){
        //
                $log = Array();
        $ctx = stream_context_create();
                $log['create'] = 1;
        if($i == 1){
          sleep(1); //ok
          stream_context_set_option($ctx, 'ssl', 'local_cert', dirname(__FILE__).'/'.$pem1);
          $gateway = 'ssl://gateway.sandbox.push.apple.com:2195';
        }
        else{
          stream_context_set_option($ctx, 'ssl', 'local_cert', dirname(__FILE__).'/'.$pem2);
          $gateway = 'ssl://gateway.push.apple.com:2195';
        }
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
        $log['set_option'] = 1;
        //EOS OJO
        //$continua = false;
        $continua = true;
        if($continua){
          // Open a connection to the APNS server
          $log['open'] = 1;
                    $fp = stream_socket_client(
            $gateway, $err,
            $errstr, 5, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx
          );

          if(!$fp){
            $log['error'] = Array(
                            'type' => $err,
                            'msg' => $errstr
                        );
                        $logs[] = $log;
                        return json_encode($logs);
          }

          $log['connected'] = 1;
          $msg = chr(0) . pack('n', 32) . pack('H*', $registatoin_ids) . pack('n', strlen($payload)) . $payload;

          $log['deviceToken'] = $registatoin_ids;
                    $log['payload'] = $payload;

          $result = fwrite($fp, $msg, strlen($msg));
          if (!$result){
            $log['success'] = 0;
                        $logs[] = $log;
                        fclose($fp);
            sleep(1);
            continue;
          }

          if($fp && $result){
            $log['success'] = 1;
                        $logs[] = $log;
                        $continua = false;
          }
          fclose($fp);
        }
                else{
                    //nada
                }
      }
        }





	public static function enviarNotificacion($idGcmsAndroid, $idGcmsIOS, $mensaje){

		self::enviarNotificacionAndroid($idGcmsAndroid, $mensaje);

		for ($i=0; $i < count($idGcmsIOS) ; $i++) {
			//var_dump($idGcmsIOS[$i]);
			//trigger_error("\n");
			//$mensaje2=$mensaje." - ".(string) $i;
			self::enviarNotificacionIOS($idGcmsIOS[$i], $mensaje);
		}

	}

	public static function enviarSmsAlerta($nombre,$telefono){

		$telefono = "569".substr($telefono, -8);


		if(strlen($telefono) == 11)
		{


		include(app_path() .'/util/lib/nusoap.php');

		$client = new nusoap_client('wsdl-server','wsdl');

		$err = $client->getError();
		if ($err) {	echo 'Error en Constructor' . $err ; }

		$params = array("credentials" => array("domainId"=>"demopr", "login"=>"fredes","passwd"=>"yptffndq"),
		    //"destination"=> array("+56954534563","+56998025766"),
			"destination"=>$telefono,
		    "message" => Array("msg"=>"Alerta de Caida")
		    );


		//$params = array("credentials" => array("domainId"=>"demopr", "login"=>"fredes","passwd"=>"yptffndq"));

		$result = $client->call('sendSms', array($params));
		//print_r($result);
		}


	}

	public static function enviarCorreoAlerta($nombre, $correo, $msg, $fecha, $id_alerta){

		$data = array(
			"nombre" => $nombre,
			"correo" => $correo,
			"msg" => $msg,
			"fecha" => $fecha
			);
		$asunto= $msg;
		$destinatario = "Soporte";
		$correos = array($correo, "soporte.raveno@uv.cl");
		Mail::send('Paciente.CorreoAlerta', $data, function($message) use ($correos, $destinatario, $asunto)
		{
		  $message->to($correos, $destinatario)
		          ->subject('Notificación - Viviendas Sociales - '.$asunto.' - '.$id_alerta);
		});
		$response=[];
		return response()->json(array("exito" => "El correo ha sido enviado"));
	}

	public static function obtenerTipoEvento($tipo){
		if($tipo == "PUERTAS") return EnumEvento::$PUERTA;
		if($tipo == "NICTURIA") return EnumEvento::$NICTURIA;
		if($tipo == "SONIDO")  return null;
		if($tipo == "TERMICO") return null;
		return null;
	}

	public static function obtenerNombreEvento($evento){
		if($evento == EnumEvento::$CAIDA) return "Caidas";
		if($evento == EnumEvento::$NICTURIA) return "Nicturia";
		if($evento == EnumEvento::$PUERTA) return "Acciones repetidas";
		return "";
	}

	public static function obtenerCoordenadas($direccion, $numero, $comuna){

		$address = "$direccion $numero, $comuna";
		$address = str_replace(" ", "+", $address);
		$json = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false");
		$json = json_decode($json);

		if(count($json->{"results"})==0){
			$lat=null;
			$long=null;
		}else{
			$lat = $json->{"results"}[0]->{"geometry"}->{"location"}->{"lat"};

			$long = $json->{"results"}[0]->{"geometry"}->{"location"}->{"lng"};
		}

		return [
			"lat" => $lat,
			"long" => $long
		];
	}

	public static function arrayColumn(array $input, $column_key, $index_key = null) {
		$result = array();
		foreach($input as $k => $v)
			$result[$index_key ? $v[$index_key] : $k] = $v[$column_key];
		return $result;
	}

	public static function getDiffMinutes($to, $from){
		$toTime = strtotime($to);
		$fromTime = strtotime($from);
		$minutes = round(abs($toTime - $fromTime) / 60, 2);
		return $minutes;
	}

	public static function toAlpha($n,$case = 'upper'){
		$alphabet   = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
		$n = $n-1;
		if($n <= 26){
			$alpha =  $alphabet[$n-1];
		} elseif($n > 26) {
			$dividend = ($n);
			$alpha = '';
			$modulo;
			while($dividend > 0){
				$modulo = ($dividend - 1) % 26;
				$alpha = $alphabet[$modulo].$alpha;
				$dividend = floor((($dividend - $modulo) / 26));
			}
		}

		if($case=='lower'){
			$alpha = strtolower($alpha);
		}
		return $alpha;

	}

	public static function soloNumeros($string){
		return preg_replace("/[^0-9]/","", $string);
	}

	public static function selectAlerta(){
		return[
		""  => "",
		"1" => "Caidas",
		"2" => "Acciones Repetidas",
		"3" => "Nicturia"
		];
	}

}
}
?>
