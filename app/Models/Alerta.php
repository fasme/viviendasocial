<?php
namespace App\models;
use Illuminate\Database\Eloquent\Model;
use Eloquent;
use DB;

class Alerta extends Model{
	
	protected $table = "alerta";
	protected $primaryKey = "id_alerta";
	public $timestamps = false;

	public static function obtenerAlertaCaidas($rut){
		$alertas = [];
		$datos = self::where("rut", "=", $rut)->where("id_evento", "=", EnumEvento::$CAIDA)->orderBy("fecha_hora", "asc")->get();
		foreach($datos as $dato){
			$fecha = date("d-m-Y H:i:s", strtotime($dato->fecha_hora));
			$icono = ($dato->estado_alerta == "pendiente" || $dato->estado_alerta == null) ? "alert-black.png" : "check-black.png";
			$revisada = ($dato->estado_alerta == EnumAlerta::$REVIDASO) ? true : false;
			$alertas[] = ["icono" => $icono, "fecha" => $fecha, "alerta" => $dato->id_alerta, "revisado" => $revisada];
		}
		return $alertas;
	}

	public static function obtenerAlertas($rut, $evento){
		$alertas = [];
		$datos = self::join("alerta_detalle as ad", "ad.id_alerta", "=", "alerta.id_alerta")
		->join("alerta_revisada as ar", "alerta.id_alerta", "=", "ar.id_alerta")
		->where("ad.rut_paciente", "=", $rut)->where("alerta.id_evento", "=", $evento)
		->groupBy("alerta.fecha_hora", "ar.revisada", "alerta.id_alerta", "ar.id_alerta_revisada")
		->select("alerta.fecha_hora", "ar.revisada", "alerta.id_alerta", "ar.id_alerta_revisada")
		->orderBy("alerta.fecha_hora", "desc")->get();
		foreach($datos as $dato){
			$fecha = date("d-m-Y H:i:s", strtotime($dato->fecha_hora));
			$icono = (!$dato->revisada) ? "alert-black.png" : "check-black.png";
			$revisada = $dato->revisada;
			$alertas[] = ["icono" => $icono, "fecha" => $fecha, "alerta" => $dato->id_alerta, "revisado" => $revisada, "alerta_revisada" => $dato->id_alerta_revisada];
		}
		return $alertas;
	}

	public static function obtenerAlertaPuertaNicturia($rutPaciente, $idEvento){
		$img = "<div class='purpura round-button'><a href='#'><img src='images/ico_lupa.png' style='margin-left: 3px;' /></a></div>";
		$alertas = [];
	/*	$datos = DB::table("alerta as a")->join("alerta_detalle as ad", "a.id_alerta", "=", "ad.id_alerta")
		->join("alerta_revisada as ar", "a.id_alerta", "=", "ar.id_alerta")
		->where("a.id_evento", "=", $idEvento)->where("ad.rut_paciente", "=", $rutPaciente)
		->select("a.id_alerta", "ar.id_alerta_revisada", "a.fecha_hora", DB::raw("count(*) as total"), "ar.id_contacto")
		->groupBy("a.id_alerta", "ar.id_alerta_revisada", "a.fecha_hora", "ar.id_contacto")->get();*/
		/*$datos=DB::select(DB::raw("SELECT DISTINCT SPLIT_PART(a.fecha_hora::text,' ',1)AS fecha, a.id_alerta,  a.fecha_hora, COUNT(*) AS total FROM alerta AS a
			INNER JOIN alerta_detalle AS ad ON a.id_alerta=ad.id_alerta
			WHERE a.id_evento=3
			AND ad.rut_paciente=$rutPaciente
			GROUP BY a.id_alerta,  a.fecha_hora
			ORDER BY fecha DESC"));*/
		$datos = DB::table("cartilla_micciones")
		->where("cartilla_visible", "=", TRUE)->where("rut_paciente", "=", $rutPaciente)
		->select("id_cartilla_micciones", "fecha_cartilla", "hora_acostarse")
		->orderBy("fecha_cartilla")->get();
		foreach($datos as $dato){
			$alertas[] = [
		//		date("d-m-Y H:i:s", strtotime($dato->fecha_hora)),
				date("d-m-Y", strtotime($dato->fecha_cartilla)),
				$dato->hora_acostarse,
				$dato->id_cartilla_micciones,
			//	$dato->id_alerta_revisada,
			//	$dato->id_contacto,
				$img
			];
		}
		return $alertas;
	}

	public static function obtenerTotalDeAlertas($fechas, $rut, $evento){
		$response = [];
		foreach($fechas as $fecha){
	/*		$query = self::join("alerta_detalle as ad", "alerta.id_alerta", "=", "ad.id_alerta")->where("alerta.id_evento", "=", $evento)
			->where("ad.rut_paciente", "=", $rut)->where("alerta.fecha_hora", "=", $fecha)
			->select(DB::raw("count(*) as total"))->first();*/
			$query=DB::select(DB::raw("SELECT COUNT(*) AS total FROM alerta
				INNER JOIN alerta_detalle AS ad ON alerta.id_alerta=ad.id_alerta
				WHERE alerta.id_evento=$evento
				AND ad.rut_paciente=$rut
				AND alerta.fecha_hora::text LIKE '$fecha%'"));

			$total = ($query == null) ? 0 : $query[0]->total;
			$response[] = [Funciones::abreviarFecha($fecha), $total];
		}
		return $response;
	}

	public static function obtenerUltimosDiasAlertas($rut, $dias, $evento){
		$response = [];
	/*	$datos = self::join("alerta_detalle as ad", "alerta.id_alerta", "=", "ad.id_alerta")->where("ad.rut_paciente", "=", $rut)
		->where("alerta.id_evento", "=", $evento)->where("alerta.fecha_hora", ">", DB::raw("current_date - interval '$dias' day"))
		->select("alerta.fecha_hora")
		->groupBy("alerta.fecha_hora")->orderBy("alerta.fecha_hora", "desc")->get();*/
		$datos=DB::select(DB::raw("SELECT DISTINCT SPLIT_PART(alerta.fecha_hora::text,' ',1)AS fecha FROM alerta 
			INNER JOIN alerta_detalle AS ad ON alerta.id_alerta=ad.id_alerta
			WHERE ad.rut_paciente=$rut
			AND alerta.id_evento=$evento
			AND alerta.fecha_hora>CURRENT_DATE - INTERVAL '$dias' DAY
			ORDER BY fecha DESC"));
		foreach($datos as $dato){
		//	$response[] = $dato->fecha_hora;
			$response[] = $dato->fecha;
		}
		return $response;
	}

	public static function obtenerTodasAlertasOrdenFecha($rut){
		$response = [];
		$datos = self::join("alerta_detalle as ad", "alerta.id_alerta", "=", "ad.id_alerta")
		->join("alerta_revisada as ar", "alerta.id_alerta", "=", "ar.id_alerta")
		->join("paciente as p", "ad.rut_paciente", "=", "p.rut")
		->join("usuario as u", "p.rut", "=", "u.rut")
		->join("contacto as c", "p.rut", "=", "c.rut_paciente")
		->where("c.rut_encargado", "=", $rut)
		->whereNull("c.fin")
		->select("u.nombres", "u.apellido_materno", "u.apellido_paterno", "alerta.fecha_hora", "ar.revisada", "alerta.id_evento", "ar.id_alerta_revisada")
		->groupBy("u.nombres", "u.apellido_materno", "u.apellido_paterno", "alerta.fecha_hora", "ar.revisada", "alerta.id_evento", "ar.id_alerta_revisada")
		->orderBy("alerta.fecha_hora", "desc")->get();
		foreach($datos as $dato){
			$nombre = ucwords($dato->nombres);
			$apellidoP = ucwords($dato->apellido_paterno);
			$apellidoM = ucwords($dato->apellido_materno);
			$response[] = [
				"nombre" => trim($nombre." ".$apellidoP." ".$apellidoM),
				"fecha" => date("d-m-Y H:i:s", strtotime($dato->fecha_hora)),
				"icono" => (!$dato->revisada) ? "alert-black.png" : "check-black.png",
				"evento" => $dato->id_evento,
				"revisado" => $dato->revisada,
				"idAlerta" => $dato->id_alerta_revisada
			];
		}
		return $response;
	}

	public static function obtenerTodasAlertasOrdenFechaDePaciente($rut){ //obtener evento de si mismo
		$response = [];
		$datos = self::join("alerta_detalle as ad", "alerta.id_alerta", "=", "ad.id_alerta")
		->join("alerta_revisada as ar", "alerta.id_alerta", "=", "ar.id_alerta")
		->join("paciente as p", "ad.rut_paciente", "=", "p.rut")
		->join("usuario as u", "p.rut", "=", "u.rut")
		->join("contacto as c", "p.rut", "=", "c.rut_paciente")
		->where("p.rut", "=", $rut)
		->select("u.nombres", "u.apellido_materno", "u.apellido_paterno", "alerta.fecha_hora", "ar.revisada", "alerta.id_evento")
		->groupBy("u.nombres", "u.apellido_materno", "u.apellido_paterno", "alerta.fecha_hora", "ar.revisada", "alerta.id_evento")
		->orderBy("alerta.fecha_hora", "desc")->get();
		foreach($datos as $dato){
			$nombre = ucwords($dato->nombres);
			$apellidoP = ucwords($dato->apellido_paterno);
			$apellidoM = ucwords($dato->apellido_materno);
			$response[] = [
				"nombre" => trim($nombre." ".$apellidoP." ".$apellidoM),
				"fecha" => date("d-m-Y H:i:s", strtotime($dato->fecha_hora)),
				"icono" => (!$dato->revisada) ? "alert-black.png" : "check-black.png",
				"evento" => $dato->id_evento,
				"revisado" => $dato->revisada
			];
		}
		return $response;
	}


}
