<?php

namespace App\models;

use Eloquent;

class Evento extends Eloquent{

	protected $table = 'evento';
	public $timestamps = false;
	protected $primaryKey = 'id_evento';

	public static function obtenerEventos(){
		return self::orderBy('id_evento')->pluck("nombre_evento", "id_evento");
	}

}