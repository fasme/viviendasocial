<?php

class HistorialDatosPaciente extends Eloquent {

	protected $table = 'historial_datos_paciente';
	public $timestamps = false;
	protected $primaryKey = 'id_historial_datos_paciente';

	public static function obtenerHistorialActual($rut){
		return self::where("rut_paciente", "=", $rut)->whereNull("fin")->first();
	}
	
}