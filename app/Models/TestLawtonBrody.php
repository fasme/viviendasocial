<?php
use Illuminate\Database\Eloquent\Model;
namespace App\models;
use Eloquent;

class TestLawtonBrody extends Eloquent{
	
	protected $table = 'test_lawton';
	public $timestamps = false;
	protected $primaryKey = 'id_test_lawton';


}