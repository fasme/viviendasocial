<?php

class Sensor extends Eloquent {

	protected $table = 'sensor';
	public $timestamps = false;
	protected $primaryKey = 'id_sensor';


	public static function obtenerUbicacionSensorEvento($evento,$rut){
		return self::leftJoin("ubicacion_sensor as u", "sensor.id_ubicacion_sensor", "=", "u.id_ubicacion_sensor")	
		->where("sensor.rut", "=", $rut)
		->select("sensor.id_sensor","u.ubicacion")->groupBy("sensor.id_sensor","u.ubicacion")->get()->toArray();

	}
	public static function obtenerUbicacion($rut,$nombre_sensor)//sensores de puertas
	{
		$resultado=DB::select(
			DB::raw(
				"
				SELECT 
				ubicacion 
				FROM ubicacion_sensor AS us
				INNER JOIN sensor AS s ON us.id_ubicacion_sensor=s.id_ubicacion_sensor
				WHERE s.rut=$rut
				AND s.identificador_sensor='$nombre_sensor'
				"
				)
			);
		if($resultado)
			return $resultado[0]->ubicacion;
		return $nombre_sensor;
	}
	public static function tieneRuido($rut,$nombre_sensor)//sensores de puertas
	{
		$resultado=DB::select(
			DB::raw(
				"
				SELECT estado1,estado2
				FROM sensor
				WHERE rut=$rut
				AND identificador_sensor='$nombre_sensor'
				"
				)
			);
		if($resultado)
		{
			
			if(($resultado[0]->estado1=="no mide bien")||($resultado[0]->estado2=="no mide bien"))
				return true;
			return false;
		}
		return false;
	}
	
}
