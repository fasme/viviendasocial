<?php

namespace App\models;
use Illuminate\Database\Eloquent\Model;
use DB;
use Funciones;
use Carbon\Carbon;

class Contacto extends Model{

	protected $table = 'contacto';
	public $timestamps = false;
	protected $primaryKey = 'id_contacto';

	public static function existeContacto($rut){
		$contacto = self::join("usuario as u", "u.rut", "=", "contacto.rut_encargado")
			->where("u.visible", "=", true)
			->where("u.rut", "=", $rut)
			->where(function ($query){
				$query->where("u.id_tipo_usuario", "=", "4")
				->orWhere("u.id_tipo_usuario", "=", "6");
			})
			->where(function ($query){
				$query->where("contacto.id_tipo_relacion", "=", "2")
				->orWhere("contacto.id_tipo_relacion", "=", "1");
			})->first();
		if($contacto != null) return true;
		return false;
	}

	public static function obtenerTipoUsuario($rut){
		$contacto = self::find($rut);
		return ($contacto == null) ? "" : $contacto->id_tipo_relacion;
	}

	//se usa
	public static function obtenerContactoPaciente($rutPaciente){
		$response=[];
		$datos=self::join("usuario", "usuario.rut", "=", "contacto.rut_encargado")
		->join("tipo_usuario","tipo_usuario.id_tipo_usuario","=","usuario.id_tipo_usuario")
		->leftjoin("relacion_familiar", "relacion_familiar.id_relacion_familiar","=","contacto.id_relacion_familiar")
		->where("contacto.rut_paciente", "=", $rutPaciente)
		->where("usuario.visible", "=", true)
		->whereNull("contacto.fin")
		->select("usuario.nombres", "usuario.apellido_paterno", "usuario.apellido_materno", "usuario.telefono", "usuario.correo", "usuario.rut", "usuario.id_tipo_usuario", "tipo_usuario.tipo", "contacto.prioridad","relacion_familiar.relacion")->get();
		
		foreach($datos as $dato){
			$response[]=[
				"nombre" => ucwords($dato->nombres),
				"apellidos" => ucwords($dato->apellido_paterno)." ".ucwords($dato->apellido_materno),
				"telefono" => $dato->telefono,
				"correo" => $dato->correo,
				"rut" => $dato->rut,
				"dv" => Funciones::obtenerDV($dato->rut),
				"id_tipo_usuario" => $dato->id_tipo_usuario,
				"tipo_usuario" =>  ucwords($dato->tipo),
				"prioridad" => $dato->prioridad,
				"relacion" =>$dato->relacion
			];
		}
		return $response;
	}

	//se usa
	// poner fecha de fin a contacto de un paciente
	public static function detenerRelacionContactoPaciente($rutPaciente, $rutContacto, $idTipoUsuario){
		//rutPaciente = ruts 
		//rutContacto = rutPaciente

		$contactos = self::where("contacto.rut_paciente" , "=", $rutPaciente)
						->where("contacto.rut_encargado", "=", $rutContacto)
						->whereNull("contacto.fin")->first();

		//return $contactos->id_contacto;
		if (count($contactos) > 0) {
			//$detener = self::where("id_contacto", "=", $contactos->id_contacto)
			//->update(array("fin" => ));

			$actualizar = Contacto::find( $contactos->id_contacto);
			$actualizar->fin =  date("d-m-Y H:i:s");
			$actualizar->save();

			
		}

			
		
	}

	//se usa
	public static function obtenerUltimaPrioridadPaciente($rutPaciente){

		$contactos = self::where("contacto.rut_paciente" , "=", $rutPaciente)
						->whereNull("contacto.fin")->max('prioridad');
		return $contactos;
		
	}

// poner fecha de fin a contacto de un usuario
	public static function detenerRelacionContactoMedico($ruts, $rutEncargado){
		$contactos = self::join("paciente as p", "p.rut", "=", "contacto.rut_paciente")
		->where("contacto.rut_encargado", "=", $rutEncargado)
		->whereNull("contacto.fin");
		foreach($ruts as $rut){
			$contactos = $contactos->where("rut_paciente", "!=", $rut);
		}
		$contactos = $contactos->get();

		foreach($contactos as $datos){
			$detener = self::where("id_contacto", "=", $datos->id_contacto)
			->update(array("fin" => date("d-m-Y H:i:s")));
		}
	}

	public static function obtenerContactosPorPaciente($rut){
		return self::where("rut_paciente", "=", $rut)->whereNull("fin")->get();
	}

	public static function obtenerContactoPacienteSms($rutPaciente){
		$response=[];
		$datos=self::join("usuario", "usuario.rut", "=", "contacto.rut_encargado")
		->where("contacto.rut_paciente", "=", $rutPaciente)
		->where("usuario.visible", "=", true)
		->whereNull("contacto.fin")
		->select("usuario.nombres", "usuario.apellido_paterno", "usuario.apellido_materno", "usuario.telefono", "usuario.correo", "usuario.rut")->get();
		foreach($datos as $dato){
			$response[]=[
				"nombre" => ucwords($dato->nombres),
				"apellidos" => ucwords($dato->apellido_paterno)." ".ucwords($dato->apellido_materno),
				"telefono" => $dato->telefono
			];
		}
		return $response;
	}

	public static function obtenerContactoPacienteCorreo($rutPaciente){
		$response=[];
		$datos=self::join("usuario", "usuario.rut", "=", "contacto.rut_encargado")

		->where("contacto.rut_paciente", "=", $rutPaciente)
		->where("usuario.visible", "=", true)
		->whereNull("contacto.fin")
		->select("usuario.nombres", "usuario.apellido_paterno", "usuario.apellido_materno", "usuario.correo")->get();
		foreach($datos as $dato){
			$response[]=[
				"nombre" => ucwords($dato->nombres),
				"apellidos" => ucwords($dato->apellido_paterno)." ".ucwords($dato->apellido_materno),
				"correo" => $dato->correo
			];
		}
		return $response;
	}


}
