<?php

namespace App\models;

use Illuminate\Http\Request;
use App\Models\DatosPersonales;
use Eloquent;
use DB;
use Funciones;


class FichaPacienteVista extends Eloquent{
	protected $table = 'ficha_paciente_vista';
	public $timestamps = false;
	
	public static function obtenerDatosPaciente($rut){
		$response=[];
		$datos = self::where("rut_paciente", "=", $rut)->first();


		$id_alerta = DB::table('usuario')
			->join('detalle_paciente', 'detalle_paciente.rut', '=', 'usuario.rut')
			->where('usuario.rut', $rut)
			->select("detalle_paciente.id_evento")->first();
		
		if($datos!=null){
			$fecha=($datos->fnac == null) ? "" : date("d-m-Y", strtotime($datos->fnac));
			$patologias=[];

			$response=[
				"rut" => $rut,
				"dv" => Funciones::obtenerDV($rut),
				"nombre" => ucwords($datos->nombre),
				"paterno" => ucwords($datos->apellidop),
				"materno" => ucwords($datos->apellidom),
				"telefono" => $datos->telefono,
				"correo" => $datos->correo,
				"fecha" => $fecha,
				"genero" => $datos->genero,
				"e_civil" => $datos->e_civil,
				"id_institucion" => $datos->id_institucion,
				"calle" => ucwords($datos->calle),
				"numero" => $datos->numero,
				"departamento" => $datos->departamento,
				"latitud" => $datos->latitud,
				"longitud" => $datos->longitud,
				"id_comuna" => $datos->id_comuna,
				"patologias" => $patologias,
				"id_alerta"=> $id_alerta->id_evento
			];
		}
		return $response;
	}

	public static function obtenerDatosPersonales($rut){
		$datosPersonales = new DatosPersonales;
		$datos = self::where("rut_paciente", "=", $rut)->first();

		$datosPersonales->alergia = $datos->alergia;
		$datosPersonales->fuma = ($datos->fuma) ? 1 : 0;
		$datosPersonales->fumaN = $datos->cigarros;
		$datosPersonales->alcohol = ($datos->alcohol) ? 1 : 0;
		$datosPersonales->alcoholN = $datos->n_vasos_alcohol;
		$datosPersonales->peso = $datos->peso;
		$datosPersonales->talla = $datos->talla;
		$datosPersonales->imc = $datos->imc;
		$datosPersonales->cintura = $datos->cintura;
		$datosPersonales->estatura = $datos->estatura;
		$datosPersonales->psistolica = $datos->sistolica;
		$datosPersonales->diastolica = $datos->diastolica;
		$datosPersonales->vivecon = ($datos->vive_con == null) ? "Sin información" : $datos->vive_con;

		return $datosPersonales;
	}
}