<?php

namespace App\Models;

use Funciones;
use Eloquent;
use DB;

class Encargado extends Eloquent {

	protected $table = 'encargado';
	public $timestamps = false;
	protected $primaryKey = 'rut';

	public static function existeEncargado($rut){
		$encargado = self::join("usuario as u", "u.rut", "=", "encargado.rut")
		->where("u.visible", "=", true)->where("u.rut", "=", $rut)->where(function ($query){
			$query->where("encargado.id_tipo_usuario", "=", EnumUsuario::$MEDICO)
			->orWhere("encargado.id_tipo_usuario", "=", EnumUsuario::$FAMILIAR);
		})->first();
		if($encargado != null) return true;
		return false;
	}

	public static function obtenerTipoUsuario($rut){
		$encargado = self::find($rut);
		return ($encargado == null) ? "" : $encargado->id_tipo_usuario;
	}

	//se usa
	// dado una parte del rut o nombre, y/o tipo de usuario del contacto busca una lista de sugerencias (usado en typeahead)
	public static function obtenerRutNombreContacto($consulta, $tipo_encargado){

		$pacientes=[];

		$datos=DB::table('usuario')
				->select("usuario.nombres", "usuario.apellido_paterno", "usuario.apellido_materno", "usuario.rut", "relacion_familiar.id_relacion_familiar", "usuario.correo")
				->join("contacto", "contacto.rut_encargado", "=", "usuario.rut")
				->join("relacion_familiar", "relacion_familiar.id_relacion_familiar", "=", "contacto.id_relacion_familiar")
				->where("usuario.id_tipo_usuario", "=", "6")
				->where("usuario.visible", "=", true)
				->where(function($query) use ($consulta){
					$query->where(DB::raw("to_char(usuario.rut, '99999999')"), "like", "%".$consulta."%")
					->orWhere(function($query) use ($consulta){
						$query->orWhere(DB::raw("upper(usuario.nombres)"), "like", "%".strtoupper($consulta)."%")
						->orWhere(DB::raw("upper(usuario.apellido_paterno)"), "like", "%".strtoupper($consulta)."%")
						->orWhere(DB::raw("upper(usuario.apellido_materno)"), "like", "%".strtoupper($consulta)."%");
					});
				})->get();

		
		foreach($datos as $dato){

			$agenda = DB::table("usuario")
					->select("agenda.telefono", "agenda.tipo_telefono")
					->leftjoin("agenda", "agenda.rut_usuario", "=","usuario.rut")
					->where("usuario.rut", $dato->rut)
					->get();

			$pacientes[]=[
				"rut" => $dato->rut."-".Funciones::obtenerDV($dato->rut),
				"nombre" => ucwords($dato->nombres),
				"primerApellido" => ucwords($dato->apellido_paterno),
				"segundoApellido" => ucwords($dato->apellido_materno),
				"parentesco" => $dato->id_relacion_familiar,
				"correo" => $dato->correo,
				"agenda" => $agenda


			];
		}

		//return response()->json(["pacientes" => $pacientes]);
		return $pacientes;
	}

}

