<?php 

namespace App\models;

use Eloquent;
use DB;
use DateTime;
use DatePeriod;
use DateInterval;

class Evaluacion extends Eloquent{
	protected $table = 'evaluacion_caidas';
	public $timestamps = false;
	protected $primaryKey = 'id_evaluacion_caidas';



	public static function obtenerEvaluacion($rut){
		
		$datos=DB::select( DB::raw("SELECT * from evaluacion_caidas where rut_paciente=$rut order by fecha_evaluacion")); 
		return $datos;
	}

	//Se usa
	public static function obtenerEvaluacionSemanal($rut){
		$fecha_actual=date("Y-m-d");
		//$fecha_inicial=DB::select( DB::raw("SELECT fecha_inicio_estudio from paciente where rut=$rut"))[0]->fecha_inicio_estudio;
		$fecha_inicial=$fecha_actual;
		$fecha_final=$fecha_actual;
		$r=DB::select( DB::raw("SELECT fecha_inicio_estudio,fecha_fin_estudio FROM detalle_paciente WHERE rut=$rut"));
		if($r)
		{
			$fecha_inicial=$r[0]->fecha_inicio_estudio;
			$fecha_final=$r[0]->fecha_fin_estudio;
		}
		
		$siete_dias=new DateInterval('P7D');
		$un_segundo=new DateInterval('PT1S');
		$un_segundo->invert=1;
		$date_fecha_actual = new DateTime($fecha_actual);
		$date_fecha_inicial=new DateTime($fecha_inicial);
		$date_fecha_final=new DateTime($fecha_final);
		$fecha_siguiente=new DateTime($date_fecha_inicial->format('Y-m-d'));
		
		$fecha_siguiente->add($siete_dias);
		$fecha_siguiente->add($un_segundo);

		
		$datos=array();
	
		while($date_fecha_final->getTimestamp()>$fecha_siguiente->getTimestamp())
		{
		
			$result=array((object)array(
				"fecha_evaluacion"=>$date_fecha_inicial->format('d-m-Y'),
				"cantidad"=>0)
				);
			$resultado=DB::select( DB::raw(
			"
			SELECT to_char('".$date_fecha_inicial->format('Y-m-d')."'::date,'DD-MM-YYYY' )AS fecha_evaluacion,(SELECT count(*) FROM alerta_caida WHERE alerta_caida.rut_paciente=$rut
			AND alerta_caida.fecha_hora BETWEEN '".$date_fecha_inicial->format('Y-m-d')."' AND '".$fecha_siguiente->format('Y-m-d')." 23:59:59' AND es_alerta=true)AS cantidad
			
			FROM alerta_caida
			WHERE alerta_caida.rut_paciente=$rut
			AND alerta_caida.fecha_hora BETWEEN '".$date_fecha_inicial->format('Y-m-d')."' AND '".$fecha_siguiente->format('Y-m-d')." 23:59:59' AND es_alerta=true
			GROUP BY alerta_caida.id_alerta_caida
			LIMIT 1
			"
			));
			if($resultado)
				$result=$resultado;
			$date_fecha_inicial->add($siete_dias);
			$fecha_siguiente->add($siete_dias);
	
			$datos=array_merge($datos,$result);
			
	
		}

		return $datos;
	}
}

?>
