<?php 

namespace App\models;
use Eloquent;
use App\Models\Usuario;

class Domicilio extends Eloquent{
	
	protected $table = 'domicilio';
	public $timestamps = false;
	protected $primaryKey = 'id_domicilio';

	public static function obtenerDomicilioActual($rut){
		return self::where("rut", "=", $rut)->whereNull("fin")->first();
	}

	public function guardar($rut, $calle, $numero, $departamento, $comuna, $coordenadas){
		$this->rut = $rut;
		$this->calle = $calle;
		$this->numero = $numero;
		$this->departamento = $departamento;
		$this->id_comuna = $comuna;
		$this->inicio = date("Y-m-d H:i:s");
		$this->latitud = $coordenadas["lat"];
		$this->longitud = $coordenadas["long"];
		$this->save();
	}

} 
?>