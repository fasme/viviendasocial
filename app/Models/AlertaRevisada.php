<?php
namespace App\models;
use Illuminate\Database\Eloquent\Model;
use DB;
class AlertaRevisada extends Model {

	protected $table = 'alerta_revisada';
	public $timestamps = false;
	protected $primaryKey = 'id_alerta_revisada';

	public static function marcarRevisada($idAlertaRevisada,$idUsuario){
		$alerta_revisada           = self::find($idAlertaRevisada);
		$alerta_revisada->revisada = true;
		$alerta_revisada->save();

		return $alerta_revisada;
	}

	public static function validarRevisada($idAlertaRevisada,$idUsuario){
		$alerta_revisada = self::find($idAlertaRevisada);
		return $alerta_revisada->revisada;
	}

}
