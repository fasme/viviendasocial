<?php

class ResultadoMicciones extends Eloquent{
	protected $table = 'resultado_micciones';
	public $timestamps = false;
	protected $primaryKey = 'id_resultado_micciones';
	
	public static function obtenerDatos($rut,$intervalo)
	{
		if($intervalo=="")
			$intervalo=0;

		$resultado["filtro"]=DB::select(DB::raw("
			SELECT 
			
			id_configuracion_nicturia,
			cmm.id_cartilla_micciones,
			ultrasonido AS ultrasonido_sensor,
			cmm.fecha_cartilla AS fecha_real,
			TO_CHAR(horario,'YYYY-MM-DD') AS fecha_cartilla,
			TO_CHAR(horario,'HH24:MI:SS') AS horario_cartilla,
			TO_CHAR(fecha,'DD-MM-YYYY HH24:MI:SS')AS fecha_sensor,
			fecha AS fecha_real_sensor,
			horario AS fecha_hora_cartilla,
			ABS((EXTRACT(EPOCH FROM fecha)-(EXTRACT(EPOCH FROM (horario)::TIMESTAMP))))AS diferencia
			
			FROM resultado_micciones AS rm
			
			INNER JOIN detalle_cartilla_micciones AS dcm ON rm.fecha::DATE BETWEEN dcm.horario::DATE - INTERVAL '1 day' AND dcm.horario::DATE + INTERVAL '1 day'
			INNER JOIN cartilla_micciones cmm ON dcm.id_cartilla_micciones=cmm.id_cartilla_micciones
			
			WHERE rm.rut_paciente=$rut
			AND rm.fecha::TIMESTAMP BETWEEN (dcm.horario::TEXT)::TIMESTAMP - INTERVAL '$intervalo minute' AND (dcm.horario::TEXT)::TIMESTAMP + INTERVAL '$intervalo minute'
			AND (
				SELECT fecha_cartilla FROM cartilla_micciones
				WHERE id_cartilla_micciones=dcm.id_cartilla_micciones
				AND rut_paciente=$rut
				AND cartilla_visible=true
			) IS NOT NULL
			
			ORDER BY fecha_cartilla,horario_cartilla,fecha_sensor,id_configuracion_nicturia

			"));
		
		$resultado["fechas_cartilla"]=DB::select(DB::raw(
			"
			SELECT 
			TO_CHAR(detalle_cartilla_micciones.horario,'YYYY-MM-DD')AS fecha_cartilla,
			detalle_cartilla_micciones.horario,
			cartilla_micciones.fecha_cartilla AS fecha_real 
			FROM cartilla_micciones
			INNER JOIN detalle_cartilla_micciones ON cartilla_micciones.id_cartilla_micciones=detalle_cartilla_micciones.id_cartilla_micciones
			WHERE rut_paciente=$rut
			AND cartilla_visible=TRUE
			ORDER BY fecha_cartilla,horario
			"
			));
		$resultado["fechas_cartilla_proceso"]=DB::select(DB::raw(
			"
			SELECT 
			COALESCE(TO_CHAR(detalle_cartilla_micciones.horario,'YYYY-MM-DD'),fecha_cartilla::TEXT)AS fecha_cartilla,
			COALESCE(detalle_cartilla_micciones.horario,fecha_cartilla)AS horario,
			cartilla_micciones.fecha_cartilla AS fecha_real
			FROM cartilla_micciones
			LEFT JOIN detalle_cartilla_micciones ON cartilla_micciones.id_cartilla_micciones=detalle_cartilla_micciones.id_cartilla_micciones
			WHERE rut_paciente=$rut
			AND cartilla_visible=TRUE
			union
			(
				SELECT 
				COALESCE(TO_CHAR(fecha_cartilla,'YYYY-MM-DD'),fecha_cartilla::TEXT)AS fecha_cartilla,
				(fecha_cartilla||' '||'00:00:00')::TIMESTAMP as horario, 
				fecha_cartilla as fecha_real
				FROM cartilla_micciones
				WHERE rut_paciente=$rut
				ORDER BY fecha_cartilla
				LIMIT 1
			)
			ORDER BY fecha_real,horario
			"
			));
		return $resultado;
	}
}
?>
