<?php
use Illuminate\Database\Eloquent\Model;
namespace App\models;
use Eloquent;

class TestWhoqol extends Eloquent{
	protected $table = 'test_whoqol_bref';
	public $timestamps = false;
	protected $primaryKey = 'id_test_whoqol_bref';
	

}