<?php
namespace App\models;
use Illuminate\Database\Eloquent\Model;
use DB;

class Gpsboton extends Model{
	
	protected $table = "gps_boton";
	protected $primaryKey = "id_gps_boton";
	public $timestamps = false;

}

?>