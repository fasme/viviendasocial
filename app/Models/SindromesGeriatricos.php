<?php
use Illuminate\Database\Eloquent\Model;
namespace App\models;
use Eloquent;


class SindromesGeriatricos extends Eloquent{
	
	protected $table = 'test_sindrome_geriatrico';
	public $timestamps = false;
	protected $primaryKey = 'id_test_sindrome_geriatrico';

}