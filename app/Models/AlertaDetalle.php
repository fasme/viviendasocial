<?php

class AlertaDetalle extends Eloquent{

	protected $table = 'alerta_detalle';
	public $timestamps = false;
	protected $primaryKey = 'id_alerta_detalle';

	public static function obtenerTotalAlertasPorGenero($evento, $genero, $institucion, $comuna){
		$query = self::join("alerta as a", "a.id_alerta", "=", "alerta_detalle.id_alerta")
		->join("paciente as p", "p.rut", "=", "alerta_detalle.rut_paciente")
		->join("usuario as u", "u.rut", "=", "p.rut");
		if($comuna != 0) $query = $query->join("domicilio as d", "u.rut", "=", "d.rut")->whereNull("d.fin")->where("d.id_comuna", "=", $comuna);
		if($institucion != 0) $query = $query->where("u.id_institucion", "=", $institucion);
		$result = $query->where("p.genero", "=", $genero)->where("a.id_evento", "=", $evento)->where("u.visible", "=", true)
		->select(DB::raw("count(*) as total"))->first();
		return $result->total;
	}

	public static function obtenerDetalleAlerta($idAlerta, $rutUsuario){
		$detalle = [];
		$datos = DB::table("alerta_detalle as ad")->join("alerta as a", "a.id_alerta", "=", "ad.id_alerta")
		->join("alerta_revisada as ar", "a.id_alerta", "=", "ar.id_alerta")
		->join("contacto as c", "ar.id_contacto", "=", "c.id_contacto")
		->where("ad.id_alerta", "=", $idAlerta)
		->orderBy("fecha_hora", "asc")
		->select("ad.fecha_hora", "ar.revisada", "ad.id_alerta_detalle", "ar.id_alerta_revisada")->get();
		foreach($datos as $dato){
			$detalle[] = [
				"idAlertaDetalle" => $dato->id_alerta_detalle,
				"fecha" => date("d-m-Y H:i:s", strtotime($dato->fecha_hora)),
				"revisado" => $dato->revisada,
				"icono" => (!$dato->revisada) ? "alert-black.png" : "check-black.png",
				"idAlertaRevisada" => $dato->id_alerta_revisada
			];
		}
		return $detalle;
	}

	public static function obtenerDetalleAlertaPorContacto($idAlerta, $idContacto){
		$detalle = [];
		$datos = DB::table("detalle_cartilla_micciones")
		->where("id_cartilla_micciones", "=", $idAlerta)
		->select("id_detalle_cartilla_micciones", DB::raw("to_char(horario,'HH24:MI') as fecha_detalle"), "volumen", "perdida_orina")->get();
		foreach($datos as $dato){
			$detalle[] = [
				"idAlertaDetalle" => $dato->id_detalle_cartilla_micciones,
				"fecha" => $dato->fecha_detalle,
				"volumen" => $dato->volumen,
				"perdida_orina" => ($dato->perdida_orina == TRUE) ? "Si" : "No";
			];
		}
		return $detalle;
	}

	public static function obtenerDetalleAlertaFecha($idAlerta){
		$detalle = [];
		$datos = self::where("id_alerta", "=", $idAlerta)->orderBy("fecha_hora", "desc")->get();
		foreach($datos as $dato){
			$detalle[] = [
				"fecha" => date("d-m-Y H:i:s", strtotime($dato->fecha_hora))
			];
		}
		return $detalle;
	}

	public static function obtenerDetallePuertaNicturia($rut, $evento){
		$detalle = [];
		$datos = DB::table("alerta_detalle as ad")->join("alerta as a", "a.id_alerta", "=", "ad.id_alerta")
		->where("a.rut", "=", $rut)->where("a.id_evento", "=", $evento)->orderBy("fecha_hora", "asc")
		->select("ad.fecha_hora", "a.estado_alerta", "ad.id_alerta_detalle", "a.id_alerta")->get();
		foreach($datos as $dato){
			$detalle[] = [
				"idAlertaDetalle" => $dato->id_alerta_detalle,
				"fecha" => date("d-m-Y H:i:s", strtotime($dato->fecha_hora)),
				"revisado" => ($dato->estado_alerta == EnumAlerta::$REVIDASO) ? true : false,
				"icono" => ($dato->estado_alerta == EnumAlerta::$PENDIENTE || $dato->estado_alerta == null || $dato->estado_alerta == "") ? "alert-black.png" : "check-black.png",
				"idAlerta" => $dato->id_alerta
			];
		}
		return $detalle;
	}

	public static function asociarAlertaADetalle($idAlerta){
		$detalles = self::whereNull("id_alerta")->get();
		foreach($detalles as $detalle){
			$detalle->id_alerta = $idAlerta;
			$detalle->save();
		}
	}

	public static function obtenerDetalleSinAlerta(){
		return self::whereNull("id_alerta")->get();
	}
	

}