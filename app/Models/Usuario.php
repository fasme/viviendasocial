<?php
namespace App\models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Auth;
use DB;

class Usuario extends Model implements AuthenticatableContract{


	protected $table = 'usuario';
	protected $primaryKey = 'rut';
	public $timestamps = false;

	protected $hidden = array('password', 'remember_token');




 public function getAuthIdentifierName(){
 	return 'id';
 }


		/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	public function getRememberToken(){
		 return $this->remember_token;
	}


public function setRememberToken($value)
{
    $this->remember_token = $value;
}

public function getRememberTokenName()
{
    return 'remember_token';
}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}



	public static function getUsuariosEncuesta($rutUsuario){
		$response=[];
		$datos=self::join("contacto","usuario.rut","=","contacto.rut_encargado")
					->select("rut",
							"nombres",
							"apellido_paterno",
							"apellido_materno")
					->where("id_tipo_usuario","=","7")
					->where("visible","=","TRUE")
					->where("usuario.rut",$rutUsuario)
					->orderBy("apellido_paterno")
					->get();


		if($datos!=null){
			$response = $datos;
		}

		return $response;
	}

	public static function pacientesUsuario($rutUsuario){
		$response=[];
		$datos=self::join("contacto","usuario.rut","=","contacto.rut_encargado")
					->select("rut",
							"nombres",
							"apellido_paterno",
							"apellido_materno")
					->where("id_tipo_usuario","=","7")
					->where("visible","=","TRUE")
					->where("usuario.rut",$rutUsuario)
					->orderBy("apellido_paterno")
					->get();


		if($datos!=null){
			$response = $datos;
		}

		return $response;
	}

	public static function obtenerUsuarios($perfil){
		$response=[];
		if($perfil=="0")
			$datos=self::select("usuario.nombres", "usuario.apellido_paterno", "usuario.apellido_materno", "usuario.telefono", "usuario.correo", "usuario.id_tipo_usuario", "usuario.rut")->where("usuario.visible", "=", "TRUE")->orderBy("usuario.apellido_paterno")->get();
		else
			$datos=self::select("usuario.nombres", "usuario.apellido_paterno", "usuario.apellido_materno", "usuario.telefono", "usuario.correo", "usuario.id_tipo_usuario", "usuario.rut")->where("usuario.id_tipo_usuario", "=", $perfil)->where("usuario.visible", "=", "TRUE")->orderBy("usuario.apellido_paterno")->get();
		foreach($datos as $dato){
			$asignarPaciente = ($dato->tipo == "administrador") ? '' : ' <a href="#modalAgregarPaciente" data-toggle="modal" onclick="agregarPaciente(\''.$dato->rut.'\')" class="btn btn-primary" title="Asignar pacientes"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Pacientes</a>' ;
			$response[] = [
				ucwords($dato->nombres),
				ucwords($dato->apellido_paterno)." ".ucwords($dato->apellido_materno),
				$dato->rut."-".Funciones::obtenerDV($dato->rut),
				$dato->telefono,
				$dato->correo,
				$dato->tipo,
				'<a href="#modalAgregarPaciente" data-toggle="modal" onclick="obtenerDatosUsuario(\''.$dato->rut.'\')" class="btn btn-primary" title="Editar usuario"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>'.' <button onclick="eliminarUsuario(\''.$dato->rut.'\',\'usuario\')" class="btn btn-primary" title="Eliminar usuario"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>'.$asignarPaciente
			];
		}
		return $response;
	}

	public static function obtenerUsuariosSoloAsignar($perfil){
		$response=[];
		if($perfil=="0")
			$datos=self::select("usuario.nombres", "usuario.apellido_paterno", "usuario.apellido_materno", "usuario.telefono", "usuario.correo", "tipo_usuario.tipo", "usuario.rut")->join("encargado","encargado.rut", "=", "usuario.rut")->join("tipo_usuario","tipo_usuario.id_tipo_usuario", "=", "encargado.id_tipo_usuario")->where("usuario.visible", "=", "TRUE")->orderBy("usuario.apellido_paterno")->get();
		else
			$datos=self::select("usuario.nombres", "usuario.apellido_paterno", "usuario.apellido_materno", "usuario.telefono", "usuario.correo", "tipo_usuario.tipo", "usuario.rut")->join("encargado","encargado.rut", "=", "usuario.rut")->join("tipo_usuario","tipo_usuario.id_tipo_usuario", "=", "encargado.id_tipo_usuario")->where("encargado.id_tipo_usuario", "=", $perfil)->where("usuario.visible", "=", "TRUE")->orderBy("usuario.apellido_paterno")->get();
		foreach($datos as $dato){
			$asignarPaciente = ($dato->tipo == "administrador") ? '' : " <a href='#'  onclick='agregarPaciente($dato->rut)'>Asociar pacientes</a>";
			$response[] = [
				ucwords($dato->nombres),
				ucwords($dato->apellido_paterno)." ".ucwords($dato->apellido_materno),
				$dato->rut."-".Funciones::obtenerDV($dato->rut),
				$dato->tipo,
				$asignarPaciente
			];
		}
		return $response;
	}

	public static function obtenerUsuario($rut){
		$usuario = self::find($rut);
		$usuario->apellido_paterno = ($usuario->apellido_paterno == null) ? "" : $usuario->apellido_paterno;
		$usuario->apellido_materno = ($usuario->apellido_materno == null) ? "" : $usuario->apellido_materno;
		return $usuario;
	}

	public static function obtenerDatosUsuario($rut){
		$response=[];
		$datos=self::join("encargado", "encargado.rut", "=", "usuario.rut")->where("encargado.rut", "=", $rut)
		->select("nombres", "apellido_paterno", "apellido_materno", "telefono", "correo", "id_institucion", "id_tipo_usuario", "alerta_push", "alerta_sms", "alerta_correo")->first();

		$domicilio=self::leftJoin("domicilio", "domicilio.rut", "=", "usuario.rut")
		->where("usuario.rut", "=", $rut)->whereNull("domicilio.fin")
		->select("calle", "numero", "departamento", "latitud", "longitud", "domicilio.id_comuna")->first();

		$patologias=self::leftJoin("patologia", "rut_encargado", "=", "rut")
		->where("rut", "=", $rut)
		->select("id_cie10", "id_patologia", "rut_paciente", DB::raw("to_char(fecha_diagnostico, 'dd-Mon-YYYY') as fecha_diagnostico"))->get();

		if($datos!=null){
			$response=[
				"rut" => $rut,
				"dv" => Funciones::obtenerDV($rut),
				"nombre" => ucwords($datos->nombres),
				"paterno" => ucwords($datos->apellido_paterno),
				"materno" => ucwords($datos->apellido_materno),
				"telefono" => $datos->telefono,
				"correo" => $datos->correo,
				//"fecha" => $fecha,
				//"genero" => $datos->genero,
				"id_institucion" => $datos->id_institucion,
				"calle" => ucwords($domicilio->calle),
				"numero" => $domicilio->numero,
				"departamento" => $domicilio->departamento,
				"latitud" => $domicilio->latitud,
				"longitud" => $domicilio->longitud,
				"id_comuna" => $domicilio->id_comuna,
				"patologias" => $patologias,
				"id_tipo_usuario" => $datos->id_tipo_usuario,
				"alerta_push" => $datos->alerta_push,
				"alerta_sms" => $datos->alerta_sms,
				"alerta_correo" => $datos->alerta_correo
			];
		}
		return $response;
	}

	//se usa
	public static function obtenerPacienteContacto($rutUsuario, $rutPaciente=null, $idTipoUsuario=null){
		$pacientes=[];

		$datos=DB::table("usuario as u");
		if($rutPaciente == null) $datos=$datos->join("contacto as c", "u.rut", "=", "c.rut_paciente");
		if($rutPaciente != null) $datos=$datos->join("contacto as c", "u.rut", "=", "c.rut_encargado");

		if($rutPaciente != null) $datos=$datos->where("c.rut_paciente", "=", $rutPaciente); // Saco los contactos del paciente
		if($rutPaciente == null) $datos=$datos->where("c.rut_encargado", "=", $rutUsuario); // Saco los pacientes del contacto

		if($idTipoUsuario!=null) $datos=$datos->where("u.id_tipo_usuario", "=", $idTipoUsuario); // Filtro por el id del contacto

		$datos=$datos->where("u.visible", "=", true)->get();

		foreach($datos as $dato){
			$pacientes[]=[
				trim(ucwords($dato->nombres). " " . ucwords($dato->apellido_paterno). " " . ucwords($dato->apellido_materno)),
				0,
				$dato->rut,
				Funciones::obtenerDV($dato->rut)
			];
		}
		return $pacientes;
	}

	public static function obtenerTipoUsuario($rut){
		$idTipoUsuario=self::select("usuario.id_tipo_usuario")->where("usuario.visible", "=", "TRUE")->where("usuario.rut", "=", $rut)->first();
		return isset($idTipoUsuario->id_tipo_usuario)? $idTipoUsuario->id_tipo_usuario : $idTipoUsuario;
	}
	public static function obtenerNombreTipoUsuario($rut){

		/*$idTipoUsuario=DB::select(DB::raw("
			SELECT encargado.id_tipo_usuario,tipo_usuario.tipo FROM usuario
			INNER JOIN encargado ON encargado.rut=usuario.rut
			INNER JOIN tipo_usuario ON tipo_usuario.id_tipo_usuario=encargado.id_tipo_usuario
			WHERE usuario.visible=TRUE
			AND usuario.rut=$rut
			"))[0];

		return isset($idTipoUsuario->tipo)? $idTipoUsuario->tipo : $idTipoUsuario;
		*/
		$idTipoUsuario = false;
		$datos=self::select("usuario.id_tipo_usuario", "usuario.id_tipo_usuario as tipo")
		->where("usuario.rut", "=", $rut)->where("usuario.visible", "=", "TRUE")->first();
		if($datos!=null){
			$idTipoUsuario = $datos->tipo;
		}

		return $idTipoUsuario;

	}

	public static function obtenerTipoEvento($rutUsuario){		
		$query = self::join("detalle_paciente", "detalle_paciente.rut", "=", "usuario.rut")->where("usuario.rut", "=", $rutUsuario)->select("detalle_paciente.id_evento")->first();
		return $query->id_evento;
	}
}
