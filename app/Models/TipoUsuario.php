<?php

class TipoUsuario extends Eloquent {

	protected $table = 'tipo_usuario';
	public $timestamps = false;
	protected $primaryKey = 'id_tipo_usuario';
	
	public static function obtenerSelect()
	{
		return self::select("id_tipo_usuario",DB::raw("INITCAP(REPLACE(tipo,'_',' '))AS nombre"))->lists('nombre','id_tipo_usuario');
	}
}
?>