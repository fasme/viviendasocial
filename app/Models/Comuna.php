<?php

namespace App\models;
use Illuminate\Database\Eloquent\Model;
use DB;

class Comuna extends Model{

	protected $table = 'comuna';
	public $timestamps = false;
	protected $primaryKey = 'id_comuna';

	public static function obtenerComunas(){
		return self::where("id_servicio_salud","=",6)
		->orWhere("id_servicio_salud","=",7)
		->orWhere("id_servicio_salud","=",8)
		->orderBy("nombre_comuna", "asc")
		->get();
	}

	public static function obtenerSelectComunas(){
		return self::orderBy("nombre_comuna", "asc")->pluck("nombre_comuna", "id_comuna");
	}

	public static function obtenerNombreComuna($idComuna){
		$comuna = self::find($idComuna);
		if($comuna == null) return "";
		return $comuna->nombre_comuna;
	}

	public static function obtenerNombresComunas(){
		return self::orderBy("nombre_comuna", "asc")->pluck("nombre_comuna");
	}

	public static function obtenerNombresComunasCaidas($institucion){
		$response[] = "";
		$query = self::join("domicilio as d", "d.id_comuna", "=", "comuna.id_comuna")
		->join("usuario as u", "u.rut", "=", "d.rut")
		->join("paciente as p", "p.rut", "=", "u.rut")
		->join("alerta_caida as a", "a.rut_paciente", "=", "p.rut");
		if($institucion != 0) $query = $query->where("u.id_institucion", "=", $institucion);
		$datos = $query->whereNull("d.fin")->select(DB::raw("count(*) as total"), "comuna.nombre_comuna")
		->orderBy("comuna.nombre_comuna", "asc")->groupBy("comuna.nombre_comuna")->get();

		foreach($datos as $dato){
			if($dato->total != 0) $response[] = $dato->nombre_comuna;
		}

		return $response;
	}

	public static function obtenerNombresComunasPuertasNicturia($institucion, $evento = null){
		$response[] = "";
		$query = self::join("domicilio as d", "d.id_comuna", "=", "comuna.id_comuna")
		->join("usuario as u", "u.rut", "=", "d.rut")
		->join("paciente as p", "p.rut", "=", "u.rut")
		->join("alerta_detalle as ad", "ad.rut_paciente", "=", "p.rut")
		->join("alerta as a", "ad.id_alerta", "=", "a.id_alerta")
		->whereNull("d.fin");
		if($evento != 0 || $evento != null) $query = $query->where("a.id_evento", "=", $evento);
		if($institucion != 0) $query = $query->where("u.id_institucion", "=", $institucion);
		$datos = $query->select(DB::raw("count(*) as total"), "comuna.nombre_comuna")
		->orderBy("comuna.nombre_comuna", "asc")->groupBy("comuna.nombre_comuna")->get();

		foreach($datos as $dato){
			if($dato->total != 0) $response[] = $dato->nombre_comuna;
		}

		return $response;
	}

	public static function obtenerTotalAlertasCaidas($comuna, $institucion){
		$query = self::join("domicilio as d", "d.id_comuna", "=", "comuna.id_comuna")
		->join("usuario as u", "u.rut", "=", "d.rut")
		->join("paciente as p", "p.rut", "=", "u.rut")
		->join("alerta_caida as a", "a.rut_paciente", "=", "p.rut");
		if($institucion != 0) $query = $query->where("u.id_institucion", "=", $institucion);
		$datos = $query->where("comuna.nombre_comuna", "=", $comuna)
		->whereNull("d.fin")->select(DB::raw("count(*) as total"))->first();

		return ($datos->total == null) ? 0 : $datos->total;
	}

	public static function obtenerTotalAlertas($evento, $comuna, $institucion){
		$query = self::join("domicilio as d", "d.id_comuna", "=", "comuna.id_comuna")
		->join("usuario as u", "u.rut", "=", "d.rut")
		->join("paciente as p", "p.rut", "=", "u.rut")
		->join("alerta_detalle as ad", "ad.rut_paciente", "=", "p.rut")
		->join("alerta as a", "ad.id_alerta", "=", "a.id_alerta")
		->where("comuna.nombre_comuna", "=", $comuna)
		->whereNull("d.fin");
		if($institucion != 0) $query = $query->where("u.id_institucion", "=", $institucion);
		$query = $query->where("a.id_evento", "=", $evento);
		$datos = $query->select(DB::raw("count(*) as total"))->first();

		return ($datos->total == null) ? 0 : $datos->total;
	}

}
