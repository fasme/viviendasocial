<?php

class DeteccionPuertas extends Eloquent{
	
	protected $table = "detalle_eventos_repetidos";
	public $timestamps = false;
	protected $primaryKey = 'id_detalle_eventos_repetidos';

	public static function generarGrafico2()
	{
		$rut=Input::get("rut");
		$fecha=Input::get("fecha");
		$persona=Input::get("persona");
		$datos=array();
		for($sensor=1;$sensor<=3;$sensor++)
		{
			$resultado=DB::select(
			DB::raw(
				"
				SELECT 
				TO_CHAR(fecha_detalle_eventos_repetidos,'HH24:MI:SS')AS HORA,
				(
					(
						EXTRACT(HOUR FROM fecha_detalle_eventos_repetidos)
					)*60*60+
					(
						EXTRACT(MINUTE FROM fecha_detalle_eventos_repetidos)
					)*60+
					(
						EXTRACT(SECOND FROM fecha_detalle_eventos_repetidos)
					)
				)AS segundos_dia,
				e$sensor
				FROM detalle_eventos_repetidos
				WHERE rut_paciente=$rut
				AND fecha_detalle_eventos_repetidos::TEXT LIKE '$fecha%'
				AND e{$sensor}_persona='$persona'
				"
				)
			);
			$datos["sensor$sensor"]=$resultado;
		}
		return $datos;
		
	}
	public static function generarGrafico()
	{
		set_time_limit(600);
		$rut=Input::get("rut");
		$fecha_inicial=Input::get("fecha_inicial");
		$fecha_final=Input::get("fecha_final");
		$rango=Input::get("rango");
		$tiempo=Input::get("tiempo");
		//$persona=Input::get("persona");
		$un_dia=new DateInterval('P1D');
		$date_fecha_inicial = new DateTime($fecha_inicial);
		$date_fecha_final = new DateTime($fecha_final);
		$date_fecha_siguiente=new DateTime($date_fecha_inicial->format('Y-m-d'));
		$datos=array();
		$contador=0;
		
		$nombre_sensor1=Sensor::obtenerUbicacion($rut,"e1");
		$nombre_sensor2=Sensor::obtenerUbicacion($rut,"e2");
		$nombre_sensor3=Sensor::obtenerUbicacion($rut,"e3");
		$datos["ubicaciones"]["s1"]=$nombre_sensor1;
		$datos["ubicaciones"]["s2"]=$nombre_sensor2;
		$datos["ubicaciones"]["s3"]=$nombre_sensor3;
		while($date_fecha_final->getTimestamp()>=$date_fecha_siguiente->getTimestamp())
		{
			$datos["datos"][$contador]=array_fill_keys(["sensor1","sensor2","sensor3"], array("cantidad"=>0,"segundos"=>0));
			$datos["datos"][$contador]=array_fill_keys(["fecha"], 0);
			
			DB::statement(
				DB::raw(
					"
					SET TIME ZONE  0;
					"
				)
			);
			
			$resultado=DB::select(
			DB::raw(
				"
				SELECT 
				e1,
				e2,
				e3,
				e1_persona,
				e2_persona,
				e3_persona,
				EXTRACT(EPOCH FROM fecha_detalle_eventos_repetidos)*1000 AS segundos_fecha,
				EXTRACT(EPOCH FROM fecha_detalle_eventos_repetidos::TIME) AS segundos_dia
				FROM detalle_eventos_repetidos
				WHERE rut_paciente=$rut
				AND fecha_detalle_eventos_repetidos BETWEEN '".$date_fecha_siguiente->format('Y-m-d')."' AND '".$date_fecha_siguiente->format('Y-m-d')." 23:59:59'
				ORDER BY fecha_detalle_eventos_repetidos ASC
				"
				)
			);

			
			
			$datos["datos"][$contador]=self::procesarDatos($resultado,$rango,$tiempo,$date_fecha_siguiente->format('Y-m-d'));
			$datos["datos"][$contador]["fecha"]=$date_fecha_siguiente->format('d-m-Y');
			
			
			$contador++;
			unset($resultado);
			$date_fecha_siguiente->add($un_dia);
		}
		return $datos;
	//	return self::detectarCambioPuerta($datos);
		
	}
	private static function iguales($arr)
	{
		if(count($arr)==0)
			return false;
		
		$valor=$arr[0];
		foreach($arr as $val)
		{
			if($val!=$valor)
				return false;
		}
		return true;
	}
	private static function persona($persona)
	{
		switch($persona)
		{
				case "nadie":
					return 0;
				case "objetivo1":
					return 1;
				case "no objetivo":
					return 2;
				case "objetivo2":
					return 3;
				default:
					return 1.5;
		}
	}
	private static function procesarDatos($r,$rango,$tiempo,$fecha)
	{
		$fi=new DateTime("$fecha 00:00:00");
		$ff=new DateTime("$fecha 23:59:59");
		$datos=array(
			"sensor1"=>array(
				"cantidad"=>0,
				"segundos"=>0,
				"frecuencia"=>0,
				"tiempo"=>array(
					array(
						0=>($fi->getTimestamp()+$fi->getOffset())*1000,
						1=>0
						),
					array(
						0=>($ff->getTimestamp()+$ff->getOffset())*1000,
						1=>0
						)
					),
				"persona"=>array(
					array(
						0=>($fi->getTimestamp()+$fi->getOffset())*1000,
						1=>self::persona("no_definido")
						),
					array(
						0=>($ff->getTimestamp()+$ff->getOffset())*1000,
						1=>self::persona("no_definido")
						)
					)
				),
			"sensor2"=>array(
				"cantidad"=>0,
				"segundos"=>0,
				"frecuencia"=>0,
				"tiempo"=>array(
					array(
						0=>($fi->getTimestamp()+$fi->getOffset())*1000,
						1=>0
						),
					array(
						0=>($ff->getTimestamp()+$ff->getOffset())*1000,
						1=>0
						)
					),
				"persona"=>array(
					array(
						0=>($fi->getTimestamp()+$fi->getOffset())*1000,
						1=>self::persona("no_definido")
						),
					array(
						0=>($ff->getTimestamp()+$ff->getOffset())*1000,
						1=>self::persona("no_definido")
						)
					)
				),
			"sensor3"=>array(
				"cantidad"=>0,
				"segundos"=>0,
				"frecuencia"=>0,
				"tiempo"=>array(
					array(
						0=>($fi->getTimestamp()+$fi->getOffset())*1000,
						1=>0
						),
					array(
						0=>($ff->getTimestamp()+$ff->getOffset())*1000,
						1=>0
						)
					),
				"persona"=>array(
					array(
						0=>($fi->getTimestamp()+$fi->getOffset())*1000,
						1=>self::persona("no_definido")
						),
					array(
						0=>($ff->getTimestamp()+$ff->getOffset())*1000,
						1=>self::persona("no_definido")
						)
					)
					
				)
			);
		$anterior=array(
			"s1"=>array(),
			"s2"=>array(),
			"s3"=>array()
			);
		$siguiente=array(
			"s1"=>array(),
			"s2"=>array(),
			"s3"=>array()
			);
		
		$datos_segundos=array(
			"s1"=>array(),
			"s2"=>array(),
			"s3"=>array()
			);
		
		$largo=count($r);
		$tiempo_abierta=array(
			"s1"=>0,
			"s2"=>0,
			"s3"=>0
			);
		$tiempo_cerrada=array(
			"s1"=>0,
			"s2"=>0,
			"s3"=>0
			);;
		//para obtener la frecuencia
		$seccion=array(
			"s1"=>-1,
			"s2"=>-1,
			"s3"=>-1
			);;
		$datos_frecuencia=array(
			"s1"=>array(),
			"s2"=>array(),
			"s3"=>array()
			);
		

		if($largo>=($rango*2+1))
		{
			for($i=0;$i<$rango;$i++)
			{
				$anterior["s1"][]=$r[$i]->e1;
				$anterior["s2"][]=$r[$i]->e2;
				$anterior["s3"][]=$r[$i]->e3;
			}
			for($i=$rango;$i<count($r);$i++)
			{
				//SENSOR1
				
				if(!self::iguales($anterior["s1"]))
				{
					array_shift($anterior["s1"]);
					$anterior["s1"][]=$r[$i]->e1;
					continue;
				}
				else// if(self::iguales($anterior))
				{
					if($i+$rango<$largo)
					{
						$siguiente["s1"]=array();
						for($j=$i;$j<($i+$rango);$j++)
						{
							$siguiente["s1"][]=$r[$j+1]->e1;
						}
						if(!self::iguales($siguiente["s1"]))
						{
							array_shift($anterior["s1"]);
							$anterior["s1"][]=$r[$i]->e1;
							continue;
						}
						else// if(self::iguales($siguiente)) 
						{
							if($siguiente["s1"][0]==="abierta"&&$anterior["s1"][0]==="cerrada"&&$r[$i]->e1==="cerrada")
							{
								
								$seccion_actual["s1"]=round($r[$i]->segundos_dia/$tiempo,0,PHP_ROUND_HALF_DOWN);

								if($seccion["s1"]==$seccion_actual["s1"])
								{
									$datos_frecuencia["s1"][$seccion["s1"]]++;
								}
								else
								{
									$seccion["s1"]=$seccion_actual["s1"];
									$datos_frecuencia["s1"][$seccion["s1"]]=1;
								}
								$datos["sensor1"]["cantidad"]++;
								$datos["sensor1"]["tiempo"][]=[(int)($r[$i]->segundos_fecha),1];
								$datos["sensor1"]["persona"][]=[(int)($r[$i]->segundos_fecha),self::persona($r[$i]->e1_persona)];
								
								
								$tiempo_abierta["s1"]=$r[$i]->segundos_dia;
								
							}
							else if($siguiente["s1"][0]==="cerrada"&&$anterior["s1"][0]==="abierta"&&$r[$i]->e1==="abierta"&&$tiempo_abierta["s1"]!=0)
							{
								$tiempo_cerrada["s1"]=$r[$i]->segundos_dia;
								$datos["sensor1"]["tiempo"][]=[(int)($r[$i]->segundos_fecha),0];
								$datos["sensor1"]["persona"][]=[(int)($r[$i]->segundos_fecha),self::persona($r[$i]->e1_persona)];
								$datos_segundos["s1"][]=$tiempo_cerrada["s1"]-$tiempo_abierta["s1"];
								$tiempo_abierta["s1"]=0;
								$tiempo_cerrada["s1"]=0;
							}
							array_shift($anterior["s1"]);
							$anterior["s1"][]=$r[$i]->e1;
						}
					}
				}
					
				//SENSOR2
			
				if(!self::iguales($anterior["s2"]))
				{
					array_shift($anterior["s2"]);
					$anterior["s2"][]=$r[$i]->e2;
					continue;
				}
				else// if(self::iguales($anterior))
				{
					if($i+$rango<$largo)
					{
						$siguiente["s2"]=array();
						for($j=$i;$j<($i+$rango);$j++)
						{
							$siguiente["s2"][]=$r[$j+1]->e2;
						}
						if(!self::iguales($siguiente["s2"]))
						{
							array_shift($anterior["s2"]);
							$anterior["s2"][]=$r[$i]->e2;
							continue;
						}
						else// if(self::iguales($siguiente)) 
						{
							if($siguiente["s2"][0]==="abierta"&&$anterior["s2"][0]==="cerrada"&&$r[$i]->e2==="cerrada")
							{
								
								$seccion_actual["s2"]=round($r[$i]->segundos_dia/$tiempo,0,PHP_ROUND_HALF_DOWN);

								if($seccion["s2"]==$seccion_actual["s2"])
								{
									$datos_frecuencia["s2"][$seccion["s2"]]++;
								}
								else
								{
									$seccion["s2"]=$seccion_actual["s2"];
									$datos_frecuencia["s2"][$seccion["s2"]]=1;
								}
								$datos["sensor2"]["cantidad"]++;
								$datos["sensor2"]["tiempo"][]=[(int)($r[$i]->segundos_fecha),1];
								$datos["sensor2"]["persona"][]=[(int)($r[$i]->segundos_fecha),self::persona($r[$i]->e2_persona)];
								$tiempo_abierta["s2"]=$r[$i]->segundos_dia;
								
							}
							else if($siguiente["s2"][0]==="cerrada"&&$anterior["s2"][0]==="abierta"&&$r[$i]->e2==="abierta"&&$tiempo_abierta["s2"]!=0)
							{
								$tiempo_cerrada["s2"]=$r[$i]->segundos_dia;
								$datos["sensor2"]["tiempo"][]=[(int)($r[$i]->segundos_fecha),0];
								$datos["sensor2"]["persona"][]=[(int)($r[$i]->segundos_fecha),self::persona($r[$i]->e2_persona)];

								$datos_segundos["s2"][]=$tiempo_cerrada["s2"]-$tiempo_abierta["s2"];
								$tiempo_abierta["s2"]=0;
								$tiempo_cerrada["s2"]=0;
							}
							array_shift($anterior["s2"]);
							$anterior["s2"][]=$r[$i]->e2;
						}
					}
				}
						
				//SENSOR3
				
				if(!self::iguales($anterior["s3"]))
				{
					array_shift($anterior["s3"]);
					$anterior["s3"][]=$r[$i]->e3;
					continue;
				}
				else// if(self::iguales($anterior))
				{
					if($i+$rango<$largo)
					{
						$siguiente["s3"]=array();
						for($j=$i;$j<($i+$rango);$j++)
						{
							$siguiente["s3"][]=$r[$j+1]->e3;
						}
						if(!self::iguales($siguiente["s3"]))
						{
							array_shift($anterior["s3"]);
							$anterior["s3"][]=$r[$i]->e3;
							continue;
						}
						else// if(self::iguales($siguiente)) 
						{
							if($siguiente["s3"][0]==="abierta"&&$anterior["s3"][0]==="cerrada"&&$r[$i]->e3==="cerrada")
							{
								
								$seccion_actual["s3"]=round($r[$i]->segundos_dia/$tiempo,0,PHP_ROUND_HALF_DOWN);

								if($seccion["s3"]==$seccion_actual["s3"])
								{
									$datos_frecuencia["s3"][$seccion["s3"]]++;
								}
								else
								{
									$seccion["s3"]=$seccion_actual["s3"];
									$datos_frecuencia["s3"][$seccion["s3"]]=1;
								}
								$datos["sensor3"]["cantidad"]++;
								$datos["sensor3"]["tiempo"][]=[(int)($r[$i]->segundos_fecha),1];
								$datos["sensor3"]["persona"][]=[(int)($r[$i]->segundos_fecha),self::persona($r[$i]->e3_persona)];
								$tiempo_abierta["s3"]=$r[$i]->segundos_dia;
								
							}
							else if($siguiente["s3"][0]==="cerrada"&&$anterior["s3"][0]==="abierta"&&$r[$i]->e3==="abierta"&&$tiempo_abierta["s3"]!=0)
							{
								$tiempo_cerrada["s3"]=$r[$i]->segundos_dia;
								$datos["sensor3"]["tiempo"][]=[(int)($r[$i]->segundos_fecha),0];
								$datos["sensor3"]["persona"][]=[(int)($r[$i]->segundos_fecha),self::persona($r[$i]->e3_persona)];

								$datos_segundos["s3"][]=$tiempo_cerrada["s3"]-$tiempo_abierta["s3"];
								$tiempo_abierta["s3"]=0;
								$tiempo_cerrada["s3"]=0;
							}
							array_shift($anterior["s3"]);
							$anterior["s3"][]=$r[$i]->e3;
						}
					}
					else
					{
						
						break;
					}
				}
			}
			$datos["sensor1"]["tiempo"][]=[(int)$r[count($r)-1]->segundos_fecha,0];
			$datos["sensor2"]["tiempo"][]=[(int)$r[count($r)-1]->segundos_fecha,0];
			$datos["sensor3"]["tiempo"][]=[(int)$r[count($r)-1]->segundos_fecha,0];
			
			$datos["sensor1"]["persona"][]=[(int)$r[count($r)-1]->segundos_fecha,"no_definido"];
			$datos["sensor2"]["persona"][]=[(int)$r[count($r)-1]->segundos_fecha,"no_definido"];
			$datos["sensor3"]["persona"][]=[(int)$r[count($r)-1]->segundos_fecha,"no_definido"];
			
			if(count($datos_segundos["s1"])!=0)
			{
				$datos["sensor1"]["segundos"]=array_sum($datos_segundos["s1"])/count($datos_segundos["s1"]);
			}
			if(count($datos_segundos["s2"])!=0)
			{
				$datos["sensor2"]["segundos"]=array_sum($datos_segundos["s2"])/count($datos_segundos["s2"]);
			}
			if(count($datos_segundos["s3"])!=0)
			{
				$datos["sensor3"]["segundos"]=array_sum($datos_segundos["s3"])/count($datos_segundos["s3"]);
			}
			
			
			if(count($datos_frecuencia["s1"])!=0)
			{
				$datos["sensor1"]["frecuencia"]=max($datos_frecuencia["s1"]);
				//$datos["frecuencia"]=array_sum($datos_frecuencia)/count($datos_frecuencia);
			}
			if(count($datos_frecuencia["s2"])!=0)
			{
				$datos["sensor2"]["frecuencia"]=max($datos_frecuencia["s2"]);
				//$datos["frecuencia"]=array_sum($datos_frecuencia)/count($datos_frecuencia);
			}
			if(count($datos_frecuencia["s3"])!=0)
			{
				$datos["sensor3"]["frecuencia"]=max($datos_frecuencia["s3"]);
				//$datos["frecuencia"]=array_sum($datos_frecuencia)/count($datos_frecuencia);
			}
			array_multisort($datos["sensor1"]["tiempo"]);
			array_multisort($datos["sensor1"]["persona"]);
			array_multisort($datos["sensor2"]["tiempo"]);
			array_multisort($datos["sensor2"]["persona"]);
			array_multisort($datos["sensor3"]["tiempo"]);
			array_multisort($datos["sensor3"]["persona"]);
		}
		return $datos;
	}	

}

?>
