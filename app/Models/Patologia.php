<?php

class Patologia extends Eloquent {

	protected $table = 'patologia';
	public $timestamps = false;
	protected $primaryKey = 'id_patologia';

	public static function obtenerPatologiasPorRut($rut){
		$response = [];
		$datos = DB::table("patologia as p")->join("cie10 as c", "p.id_cie10", "=", "c.id_cie10")
		->where("p.rut_paciente", "=", $rut)->where("p.vigente", "=", true)->get();
		foreach($datos as $dato){
			$response[] = ucwords($dato->nombre_cie10);
		}
		return $response;
	}

}