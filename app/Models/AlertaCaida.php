<?php

namespace App\Models;

use Eloquent;
use DB;

class AlertaCaida extends Eloquent{

	protected $table = 'alerta_caida';
	public $timestamps = false;
	protected $primaryKey = 'id_alerta_caida';

	public static function obtenerTotalDeCaidasPorGenero($genero, $institucion, $comuna){
		$query = self::join("paciente as p", "p.rut", "=", "alerta_caida.rut_paciente")
		->join("usuario as u", "u.rut", "=", "p.rut");
		if($comuna != 0) $query = $query->join("domicilio as d", "u.rut", "=", "d.rut")->whereNull("d.fin")->where("d.id_comuna", "=", $comuna);
		if($institucion != 0) $query = $query->where("u.id_institucion", "=", $institucion);
		$result = $query->where("p.genero", "=", $genero)->where("u.visible", "=", true)->select(DB::raw("count(*) as total"))->first();
		return $result->total;
	}

	public static function obtenerUltimosMesesCaidas($rut, $ubicaciones){ /* para graficos caida*/
		$response = [];
		foreach($ubicaciones as $ubicacion){
			$query = self::where("rut_paciente", "=", $rut)->where("id_sensor", "=", $ubicacion["id_sensor"])
			->where("id_evento", "=", EnumEvento::$CAIDA)
			->where("fecha_hora", ">", DB::raw("current_date - interval '12' month"))
			->where("es_alerta", "=", TRUE)
			->select(DB::raw("count(*) as total"))->first();
			$total = ($query == null) ? 0 : $query->total;
			$response[] = [$ubicacion["ubicacion"], $total];
		}
		return $response;
	}


	public static function obtenerUltimosDiasCaidas($rut, $dias){ /* para graficos caida*/
	
		$response = [];
		$fechas =[];
		$totales =[];
		$query = self::where("rut_paciente", "=", $rut)
		->where("fecha_hora", ">", DB::raw("(SELECT CURRENT_DATE- interval '".$dias." day')"))
		->where("es_alerta", "=", TRUE)
		->select(DB::raw("fecha_hora::date as fecha, count(*) as total"))
		->groupBy(DB::raw("(fecha_hora::date)"))
		->orderBy("fecha", "asc")
		->get();

		foreach($query as $dato){
			$total = ($dato == null) ? 0 : $dato->total;
			$fechas[] = date("d-m-Y", strtotime($dato->fecha));
			$totales[] = $total;
		}
		
		$response = [$fechas, $totales];
		
		return $response;
	}

	public static function obtenerUltimosDiasCaidasUbicacion($rut, $dias, $ubicaciones){ /* para graficos caida dias y ubicaciones */
	
		$response = [];
		$fechas =[];
		$totales =[];

		$fechas = [];

		$fechaInicial = date("d-m-Y");

		$fechas [] = $fechaInicial;

		$sensor[0] = [];
		$sensor[1] = [];
		$sensor[2] = [];
		

		for ($i=1; $i < $dias ; $i++) { 	 	
			$fechaInicial = date("d-m-Y", strtotime($fechaInicial . " - 1 day"));
			$fechas [] = $fechaInicial;
		}

		foreach($fechas as $fecha){
			for ($i=0; $i < count($ubicaciones); $i++) { 

				/*
				select fecha_hora::date as fecha, count(*) as total 
				from "alerta_caida" 
				where "rut_paciente" = '5216834' and "fecha_hora" > (SELECT CURRENT_DATE- interval '12 day') and "es_alerta" is true 
				group by (fecha_hora::date) 
				order by "fecha" asc
				*/
				$query = self::where("rut_paciente", "=", $rut)
				//->where("fecha_hora", ">", DB::raw("(SELECT CURRENT_DATE- interval '".$dias." day')"))
				->where(DB::raw("to_char(fecha_hora, 'DD-MM-YYYY')"), "=", $fecha)
				->where("es_alerta", "=", TRUE)
				->where("id_sensor", "=", $ubicaciones[$i]["id_sensor"])
				->select(DB::raw("fecha_hora::date as fecha, count(*) as total"))
				->groupBy(DB::raw("(fecha_hora::date)"))
				->first();

				$sensor[$i][] = ($query == NULL)? 0: $query->total;
			}
		}	

		
		$response = [$fechas, $sensor[0], $sensor[1], $sensor[2], $ubicaciones];
		
		return $response;
	}

	public static function obtenerAlertas($rut, $rutUsuario = null){ /* web: tabla estadisticas caidas , movil: en seguimiento por paciente*/
		$response = [];
		/*$datos = self::join("alerta_revisada as ar", "alerta_caida.id_alerta_caida", "=", "ar.id_alerta_caida")	
		->join("sensor as s", "alerta_caida.id_sensor", "=", "s.id_sensor")
		->join("ubicacion_sensor as us", "s.id_ubicacion_sensor", "=", "us.id_ubicacion_sensor")
		->join(DB::raw("(SELECT sin_repetidas.id_alerta_caida as id_caida, count(id_alerta_caida) AS numero_detalle
						FROM
						(SELECT DISTINCT(adc.id_alerta_caida, adc.fecha_hora) AS campo, adc.id_alerta_caida, adc.fecha_hora 
						FROM alerta_detalle_caida adc
						ORDER BY (adc.id_alerta_caida, adc.fecha_hora) DESC) sin_repetidas
						GROUP BY id_caida) AS cd"), 'cd.id_caida', '=', 'alerta_caida.id_alerta_caida', 'LEFT');	
		
		if($rutUsuario !=null){
			$datos = $datos->join("contacto as c", "c.id_contacto", "=", "ar.id_contacto")->where("c.rut_encargado","=",$rutUsuario);
		}
		$datos = $datos->where("alerta_caida.rut_paciente", "=", $rut)
		->where("alerta_caida.es_alerta", "=", TRUE)
		//->select("alerta_caida.id_alerta_caida", "alerta_caida.id_sensor", "alerta_caida.fecha_hora",	"alerta_caida.tipo", "ar.id_alerta_revisada",	"ar.revisada",  "s.id_sensor", "s.id_ubicacion_sensor", "s.identificador_sensor", "us.ubicacion")
		->orderBy("fecha_hora", "desc")->get();
		*/
		$consulta = '(SELECT *                                 
					FROM                                    
						(SELECT 
						ar.revisada, ar.id_alerta_revisada, ac.id_alerta_caida as id_caida                                        
						FROM alerta_caida ac                  
						INNER JOIN sensor AS s ON ac.id_sensor = s.id_sensor
						INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor = us.id_ubicacion_sensor
						INNER JOIN alerta_revisada AS ar ON ac.id_alerta_caida = ar.id_alerta_caida';
		if($rutUsuario !=null){
		$consulta .=  ' INNER JOIN contacto AS c ON c.id_contacto = ar.id_contacto';
		} 
		$consulta .=  ' WHERE ';
		if($rutUsuario !=null){
		$consulta .=  '	c.rut_encargado = '."'". $rutUsuario."'". ' AND ';
		} 
		$consulta .=  ' ac.rut_paciente = '."'". $rut."'". ' AND
						ac.es_alerta IS TRUE AND ac.es_alerta = TRUE
						AND c.fin is null
						order by ac.fecha_hora DESC
						) alerta_despues

						RIGHT JOIN
					  
					  	(SELECT * FROM  
					    	(SELECT  ac.fecha_hora, s.identificador_sensor, ac.tipo, us.ubicacion, id_alerta_caida as id_caida_todas,  row_number() over (partition by ac.rut_paciente, ac.fecha_hora, ac.id_alerta_caida ORDER BY ac.fecha_hora DESC ) AS numero_filas
						    FROM alerta_caida ac, contacto c, paciente p, sensor s, ubicacion_sensor us, usuario u
						    WHERE
						    ac.rut_paciente=c.rut_paciente AND ac.rut_paciente = '."'". $rut."'". ' AND
						    ac.es_alerta IS TRUE AND ac.rut_paciente = p.rut AND
						    ac.id_sensor = s.id_sensor AND s.id_ubicacion_sensor = us.id_ubicacion_sensor AND
						    p.rut = u.rut AND ac.es_alerta = TRUE 
						    ORDER BY ac.rut_paciente
						    ) at
						WHERE
						numero_filas = 1
						)alerta_todas

					    ON alerta_todas.id_caida_todas=alerta_despues.id_caida
					    
					    LEFT join 
							 
						(SELECT sin_repetidas.id_alerta_caida as id_caida, COALESCE(count(id_alerta_caida), 0) AS numero_detalle                                         
							 FROM                                            
							 	(SELECT DISTINCT(adc.id_alerta_caida, adc.fecha_hora) AS campo, adc.id_alerta_caida, adc.fecha_hora
							 	 FROM alerta_detalle_caida adc                                           
								 ORDER BY (adc.id_alerta_caida, adc.fecha_hora) DESC
								)sin_repetidas                                              
							 GROUP BY id_caida
						) AS cd on cd.id_caida = alerta_todas.id_caida_todas

					ORDER BY alerta_todas.fecha_hora DESC) as X';

		$datos = DB::table( DB::raw($consulta) )->get();

		foreach($datos as $dato){
			$fecha = date("d-m-Y H:i:s", strtotime($dato->fecha_hora));
			$img =(!$dato->revisada) ? "alert-black.png" : "check-black.png";
			$url = "";
			$icono = "<img src=\"http://" .$_SERVER['SERVER_NAME']. "/images/".$img."\"  class='img-resumen'/>";
			$response[] = ["linkicono" => $icono, 
							"icono" => (is_null($dato->revisada) ) ? "no-clic.png" : ((!$dato->revisada) ? "alert-black.png" : "check-black.png"),
							"fecha" => $fecha, 
							"alerta_revisada" => $dato->id_alerta_revisada, 
							"identificadorSensor" => $dato->identificador_sensor,
							"ubicacion" => (($dato->tipo == "caida")? "Caída" : "Ayuda") ." - ". $dato->ubicacion ,
							"revisado" => $dato->revisada,
							"validacion" => $dato->numero_detalle];
		}
		return $response;
	}

	/* Se usa para obtener las ultimas alertas de un paciente en el index web y movil */
	public static function obtenerTodasAlertas($rut){ 
		$response = [];

		$consulta = '(SELECT alerta_todas.rut_paciente, alerta_todas.nombres, alerta_todas.apellido_materno, alerta_todas.apellido_paterno, alerta_todas.fecha_hora,  alerta_despues.revisada, alerta_todas.id_evento, alerta_despues.id_alerta_revisada, alerta_todas.id_sensor, alerta_todas.id_ubicacion_sensor, alerta_todas.identificador_sensor, alerta_todas.ubicacion, alerta_todas.tipo
					FROM
					(SELECT ac.id_alerta_caida, u.nombres, u.apellido_materno, u.apellido_paterno, ac.fecha_hora, ar.revisada, ac.id_evento, ar.id_alerta_revisada, s.id_sensor, s.id_ubicacion_sensor, s.identificador_sensor, us.ubicacion, ac.tipo, ac.es_alerta
					  FROM alerta_caida ac 
					  INNER JOIN paciente AS p ON ac.rut_paciente = p.rut 
					  INNER JOIN sensor AS s ON ac.id_sensor = s.id_sensor 
					  INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor = us.id_ubicacion_sensor 
					  INNER JOIN usuario AS u ON p.rut = u.rut 
					  INNER JOIN contacto AS c ON p.rut = c.rut_paciente 
					  INNER JOIN alerta_revisada AS ar ON ac.id_alerta_caida = ar.id_alerta_caida AND ar.id_contacto = c.id_contacto 
					  WHERE
					  c.rut_encargado = '."'". $rut."'". ' AND 
					  ac.es_alerta IS TRUE AND
					  c.fin IS NULL AND
					  ac.es_alerta = TRUE
					  order by ac.fecha_hora DESC) alerta_despues
					RIGHT JOIN
					(SELECT ac.id_alerta_caida, ac.rut_paciente, u.nombres, u.apellido_materno, u.apellido_paterno, ac.fecha_hora,  0 AS revisada, ac.id_evento, 0 AS id_alerta_revisada, s.id_sensor, s.id_ubicacion_sensor, s.identificador_sensor, us.ubicacion, ac.tipo, ac.es_alerta, row_number() over (partition by ac.rut_paciente ORDER BY ac.fecha_hora DESC) AS numero_filas
					  FROM alerta_caida ac, contacto c, paciente p, sensor s, ubicacion_sensor us, usuario u
					  WHERE
					  ac.rut_paciente=c.rut_paciente AND
					  c.rut_encargado = '."'". $rut."'". ' AND 
					  ac.es_alerta IS TRUE AND
					  c.fin is NULL AND
					  ac.rut_paciente = p.rut AND
					  ac.id_sensor = s.id_sensor AND
					  s.id_ubicacion_sensor = us.id_ubicacion_sensor AND
					  p.rut = u.rut AND
					  ac.es_alerta = TRUE
					ORDER BY ac.rut_paciente) alerta_todas
					ON alerta_despues.id_alerta_caida=alerta_todas.id_alerta_caida
					WHERE
					numero_filas <= 5 
					ORDER BY fecha_hora
					DESC
					) as X';

		$datos = DB::table( DB::raw($consulta) )->get();
	
		foreach($datos as $dato){

			$nombre = ucwords($dato->nombres);
			$apellidoP = ucwords($dato->apellido_paterno);
			$apellidoM = ucwords($dato->apellido_materno);
			$response[] = [
				"nombre" => trim($nombre." ".$apellidoP." ".$apellidoM )." [". $dato->rut_paciente."-".Funciones::obtenerDV($dato->rut_paciente) ."]",
				"fecha" => date("d-m-Y H:i:s", strtotime($dato->fecha_hora)),
				"icono" => (is_null($dato->revisada) ) ? "no-clic.png" : ((!$dato->revisada) ? "alert-black.png" : "check-black.png"),
				"evento" => $dato->id_evento,
				"revisado" => $dato->revisada,
				"idAlerta" => $dato->id_alerta_revisada,
				"identificadorSensor" => $dato->identificador_sensor,
				"ubicacion" => (($dato->tipo == "caida")? "Caída" : "Ayuda") ." - ". $dato->ubicacion
			];
		}
		return $response;
	}

	public static function obtenerTodasAlertasDePaciente($rut){ // obtiene las alertas del mismo de caidas 
		$response = [];
		/* // original se repiten los datos
		$datos = self::join("paciente as p", "alerta_caida.rut_paciente", "=", "p.rut")
		->join("usuario as u", "p.rut", "=", "u.rut")
		->join("contacto as c", "p.rut", "=", "c.rut_paciente")
		->join("sensor as s", "alerta_caida.id_sensor", "=", "s.id_sensor")
		->join("ubicacion_sensor as us", "s.id_ubicacion_sensor", "=", "us.id_ubicacion_sensor")
		->join("alerta_revisada as ar",  function($join)
        {
            $join->on("alerta_caida.id_alerta_caida", "=", "ar.id_alerta_caida")
            ->on("ar.id_contacto", "=", "c.id_contacto");
        })
		->where("p.rut", "=", $rut)
		->where("alerta_caida.es_alerta", "=", TRUE)
		->select("u.nombres", "u.apellido_materno", "u.apellido_paterno", "alerta_caida.fecha_hora", "ar.revisada", "alerta_caida.id_evento", "ar.id_alerta_revisada", "s.id_sensor", "s.id_ubicacion_sensor", "s.identificador_sensor", "us.ubicacion")
		->orderBy("alerta_caida.fecha_hora", "desc")->get();
		*/

		/*
		select "u"."nombres", "u"."apellido_materno", "u"."apellido_paterno", "alerta_caida"."fecha_hora", "alerta_caida"."id_evento", "s"."id_sensor", "s"."id_ubicacion_sensor", "s"."identificador_sensor", "us"."ubicacion" 
		from "alerta_caida" 
		inner join "paciente" as "p" on "alerta_caida"."rut_paciente" = "p"."rut" 
		inner join "usuario" as "u" on "p"."rut" = "u"."rut" 
		inner join "sensor" as "s" on "alerta_caida"."id_sensor" = "s"."id_sensor" 
		inner join "ubicacion_sensor" as "us" on "s"."id_ubicacion_sensor" = "us"."id_ubicacion_sensor" 
		where "p"."rut" = '17626686' and "alerta_caida"."es_alerta" IS TRUE 
		order by "alerta_caida"."fecha_hora" desc 
		
		// las alertas no se repiten
		$datos = self::join("paciente as p", "alerta_caida.rut_paciente", "=", "p.rut")
		->join("usuario as u", "p.rut", "=", "u.rut")
		->join("sensor as s", "alerta_caida.id_sensor", "=", "s.id_sensor")
		->join("ubicacion_sensor as us", "s.id_ubicacion_sensor", "=", "us.id_ubicacion_sensor")
		->where("p.rut", "=", $rut)
		->where("alerta_caida.es_alerta", "=", TRUE)
		->select("u.nombres", "u.apellido_materno", "u.apellido_paterno", "alerta_caida.fecha_hora", "alerta_caida.id_evento",  "s.id_sensor", "s.id_ubicacion_sensor", "s.identificador_sensor", "us.ubicacion", "alerta_caida.tipo")
		->orderBy("alerta_caida.fecha_hora", "desc")->get();

		*/
		/*  // muestra si algun contacto ha visto la alerta
		SELECT u.nombres, u.apellido_materno, u.apellido_paterno, alerta_caida.fecha_hora, alerta_caida.id_evento, s.id_sensor, s.id_ubicacion_sensor, s.identificador_sensor, us.ubicacion, COALESCE(r.revision, FALSE) AS revisada
		FROM alerta_caida 
		INNER JOIN paciente AS p ON alerta_caida.rut_paciente = p.rut 
		INNER JOIN usuario AS u ON p.rut = u.rut 
		INNER JOIN sensor AS s ON alerta_caida.id_sensor = s.id_sensor 
		INNER JOIN ubicacion_sensor as us ON s.id_ubicacion_sensor = us.id_ubicacion_sensor 
		FULL OUTER JOIN (SELECT DISTINCT id_alerta_caida, TRUE AS revision FROM alerta_revisada WHERE revisada IS TRUE) AS r ON r.id_alerta_caida=alerta_caida.id_alerta_caida
		WHERE p.rut = '17626686' AND alerta_caida.es_alerta IS TRUE 
		ORDER BY alerta_caida.fecha_hora DESC 
		*/


		$datos = self::join("paciente as p", "alerta_caida.rut_paciente", "=", "p.rut")
		->join("usuario as u", "p.rut", "=", "u.rut")
		->join("sensor as s", "alerta_caida.id_sensor", "=", "s.id_sensor")
		->join("ubicacion_sensor as us", "s.id_ubicacion_sensor", "=", "us.id_ubicacion_sensor")
		->join(DB::raw("(SELECT DISTINCT id_alerta_caida, TRUE AS revision FROM alerta_revisada WHERE revisada IS TRUE) AS r"), 'r.id_alerta_caida', '=', 'alerta_caida.id_alerta_caida', 'FULL OUTER')
		->where("p.rut", "=", $rut)
		->where("alerta_caida.es_alerta", "=", TRUE)
		->select("u.nombres", "u.apellido_materno", "u.apellido_paterno", "alerta_caida.fecha_hora", "alerta_caida.id_evento",  "s.id_sensor", "s.id_ubicacion_sensor", "s.identificador_sensor", "us.ubicacion", "alerta_caida.tipo", DB::raw("COALESCE(r.revision, FALSE) AS revisada"))
		->orderBy("alerta_caida.fecha_hora", "desc")->get();

		foreach($datos as $dato){
			$nombre = ucwords($dato->nombres);
			$apellidoP = ucwords($dato->apellido_paterno);
			$apellidoM = ucwords($dato->apellido_materno);
			$response[] = [
				"nombre" => trim($nombre." ".$apellidoP." ".$apellidoM),
				"fecha" => date("d-m-Y H:i:s", strtotime($dato->fecha_hora)),
				"icono" => (!$dato->revisada) ? "alert-black.png" : "check-black.png",
				"evento" => $dato->id_evento,
				"revisado" => 1,//$dato->revisada,
				"idAlerta" => 1,//$dato->id_alerta_revisada,
				"identificadorSensor" => $dato->identificador_sensor,
				"ubicacion" => (($dato->tipo == "caida")? "Caída" : "Ayuda") ." - ". $dato->ubicacion
			];
		}
		return $response;
	}


	public static function AlertaPaciente($rut){ 

	$datos=DB::table( DB::raw("(SELECT count(*) as cuenta from alerta_caida where rut_paciente=$rut) as ra"))->get(); 
	
		foreach($datos as $valor)
		{
				$numero_alerta=$valor->cuenta;
		}

	return $numero_alerta;

	}

}
