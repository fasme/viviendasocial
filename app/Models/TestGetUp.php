<?php

namespace App\models;

class TestGetUp extends Eloquent{
	
	protected $table = 'test_get_up';
	public $timestamps = false;
	protected $primaryKey = 'id_test_get_up';

}