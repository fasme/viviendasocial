<?php

class HistorialHabitos extends Eloquent {

	protected $table = 'historial_habitos';
	public $timestamps = false;
	protected $primaryKey = 'id_historial_habitos';

	public static function obtenerHistorialActual($rut){
		return self::where("rut_paciente", "=", $rut)->whereNull("fin")->first();
	}

}