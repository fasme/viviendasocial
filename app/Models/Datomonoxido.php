<?php
namespace App\models;
use Illuminate\Database\Eloquent\Model;
use DB;

class Datomonoxido extends Model{
	
	protected $table = "dato_monoxido";
	protected $primaryKey = "id_dato_monoxido";
	public $timestamps = false;

}

?>