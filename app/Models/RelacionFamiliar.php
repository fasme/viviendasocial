<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use DB;
class RelacionFamiliar extends Model{

	protected $table = 'relacion_familiar';
	public $timestamps = false;
	protected $primaryKey = 'id_relacion_familiar';

	public static function obtenerRelacion(){

		return self::pluck('relacion', 'id_relacion_familiar');
		
	}

}

?>