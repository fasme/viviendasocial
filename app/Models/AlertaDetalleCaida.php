<?php

class AlertaDetalleCaida extends Eloquent{

	protected $table = 'alerta_detalle_caida';
	public $timestamps = false;
	protected $primaryKey = 'id_alerta_detalle_caida';

	public static function obtenerTotalAlertaValidacion($id_alerta_caida){
		
		//$query = self::where("id_alerta_caida", "=", $id_alerta_caida)->select(DB::raw("count(*) as total"))->first();
		
		$consulta = '(SELECT COUNT(id_alerta_caida) as total FROM (SELECT DISTINCT(adc.id_alerta_caida, adc.fecha_hora) AS campo, adc.id_alerta_caida, adc.fecha_hora FROM alerta_detalle_caida adc ORDER BY (adc.id_alerta_caida, adc.fecha_hora) DESC) sin_repetidas WHERE sin_repetidas.id_alerta_caida='.$id_alerta_caida.'GROUP BY id_alerta_caida) as X';

		$query = DB::table( DB::raw($consulta) )->first();
		return $query->total;
	}

}