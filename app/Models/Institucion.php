<?php

namespace App\models;
use Eloquent;

class Institucion extends Eloquent {

	protected $table = 'institucion';
	public $timestamps = false;
	protected $primaryKey = 'id_institucion';

	public static function obtenerInstitucionPorComuna($idComuna){
		return self::where("id_comuna", "=", $idComuna)->where("visible", "=", true)->orderBy("nombre_institucion")->get();
	}

	public static function obtenerInstituciones(){
		return self::orderBy("nombre_institucion")->pluck('nombre_institucion', 'id_institucion');
	}

}