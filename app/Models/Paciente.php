<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;

use DB;
use Funciones;

class Paciente extends Model{
	
	protected $table = 'usuario';
	public $timestamps = false;
	protected $primaryKey = 'rut';



	

	public static function obtenerPacientes($filtro){
		$pacientes=[];
		//if(Encargado::find(Auth::user()->rut)->id_tipo_usuario!=3 || $filtro==1)
		if($filtro==1)
			$datos= DB::table("detalle_paciente")
			->select("usuario.nombres", "usuario.apellido_paterno", "usuario.apellido_materno", "detalle_paciente.fecha_nacimiento", "usuario.visible as existe", "detalle_paciente.fecha_ingreso", "usuario.rut")
			->where("usuario.visible", "=", true)
			->where("c.rut_encargado", "=", Auth::user()->rut)
			->join("usuario","usuario.rut", "=", "detalle_paciente.rut")
			->join("contacto as c", "c.rut_paciente", "=", "usuario.rut")
			->whereNull("c.fin")
			->get();
		else 
			$datos= DB::table("detalle_paciente")
			->select("usuario.nombres", "usuario.apellido_paterno", "usuario.apellido_materno", "detalle_paciente.fecha_nacimiento", "usuario.visible as existe", "detalle_paciente.fecha_ingreso", "usuario.rut")
			->where("usuario.visible", "=", true)
			->join("usuario","usuario.rut", "=", "detalle_paciente.rut")
			->get();

		foreach($datos as $dato){
			$fecha =($dato->fecha_nacimiento == null) ? "" : date("d-m-Y", strtotime($dato->fecha_nacimiento));
			$monitoreo =($dato->fecha_ingreso == null) ? "" : date("d-m-Y", strtotime($dato->fecha_ingreso));
			$edad =(empty($fecha)) ? "" : Funciones::calcularEdad($fecha);
			$href = URL::to("paciente/detallePaciente/$dato->rut");

			$habilitado = (((bool)$dato->existe ) != 1) ? "" : "disabled";
			
			$pacientes[]=[
				ucwords($dato->nombres)." ".ucwords($dato->apellido_paterno)." ".ucwords($dato->apellido_materno),
				//$dato->rut."-".Funciones::obtenerDV($dato->rut),
				$fecha,
				$monitoreo,
				"<a href='$href' class='btn btn-primary' title='Detalle de paciente'><span class='glyphicon glyphicon-search' aria-hidden='true'></span></a>"
				.' <button onclick="eliminarUsuario(\''.$dato->rut.'\',\'paciente\')" class="btn btn-primary" title="Eliminar paciente" '.$habilitado.'><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>'
				.' <button onclick="agregarAlerta(\''.$dato->rut.'\',\'paciente\')" class="btn btn-primary" title="Agregar alerta" ><span class="glyphicon glyphicon-flag" ></span></button>'
			];
		}
		return  $pacientes;
	}


	public static function obtenerPacientesMedico($evento, $rutPaciente){
		$pacientes = [];
		$datos = DB::table("paciente as p")->join("usuario as u", "u.rut", "=", "p.rut")
		->join("alerta as a", "p.rut", "=", "a.rut");
		if($rutPaciente != "") $datos = $datos->where("p.rut", "=", $rutPaciente);
		$datos = $datos->where("a.id_evento", "=", $evento)
		->select("u.nombres", "u.apellido_paterno", "u.apellido_materno", DB::raw("count(*) as total"), "p.rut", "a.id_evento", "p.fecha_nacimiento")
		->groupBy("u.nombres", "u.apellido_paterno", "u.apellido_materno", "p.rut", "a.id_evento", "p.fecha_nacimiento")->get();
		foreach($datos as $dato){
			$nombre = ($dato->nombres == null) ? "" : $dato->nombres;
			$apellido_paterno = ($dato->apellido_paterno == null) ? "" : $dato->apellido_paterno;
			$apellido_materno = ($dato->apellido_materno == null) ? "" : $dato->apellido_materno;
			$link = ($dato->id_evento == EnumEvento::$CAIDA) ? "#alertaCaida" : "#alertaPuertaNicturia";
			$pacientes[] = [
				trim($nombre." ".$apellido_paterno." ".$apellido_materno),
				$dato->rut."-".Funciones::obtenerDV($dato->rut),
				($dato->fecha_nacimiento == null) ? "" : date("d-m-Y", strtotime($dato->fecha_nacimiento)),
				$dato->total,
				$dato->rut,
				$link
			];
		}
		return $pacientes;
	}

	public static function obtenerPacientesFamiliar($evento, $rutUsuario, $rutPaciente){
		$pacientes = [];
		$datos = DB::table("paciente as p")->join("usuario as u", "u.rut", "=", "p.rut")
		->join("contacto as c", "c.rut_paciente", "=", "u.rut")
		->join("alerta as a", "p.rut", "=", "a.rut");
		if($rutPaciente != "") $datos = $datos->where("p.rut", "=", $rutPaciente);
		//$datos = $datos->where("u.visible", "=", "TRUE");
		$datos = $datos->where("a.id_evento", "=", $evento)->where("c.rut_encargado", "=", $rutUsuario)
		->select("u.nombres", "u.apellido_paterno", "u.apellido_materno", DB::raw("count(*) as total"), "c.rut_paciente", "a.id_evento", "p.fecha_nacimiento")
		->groupBy("u.nombres", "u.apellido_paterno", "u.apellido_materno", "c.rut_paciente", "a.id_evento", "p.fecha_nacimiento")->get();
		foreach($datos as $dato){
			$nombre = ($dato->nombres == null) ? "" : $dato->nombres;
			$apellido_paterno = ($dato->apellido_paterno == null) ? "" : $dato->apellido_paterno;
			$apellido_materno = ($dato->apellido_materno == null) ? "" : $dato->apellido_materno;
			$link = ($dato->id_evento == EnumEvento::$CAIDA) ? "#alertaCaida" : "#alertaPuertaNicturia";
			$pacientes[] = [
				trim($nombre." ".$apellido_paterno." ".$apellido_materno),
				$dato->rut_paciente."-".Funciones::obtenerDV($dato->rut_paciente),
				($dato->fecha_nacimiento == null) ? "" : date("d-m-Y", strtotime($dato->fecha_nacimiento)),
				$dato->total,
				$dato->rut_paciente,
				$link
			];
		}
		return $pacientes;
	}

	public static function obtenerDatosPaciente($rut){
		$response=[];
		$datos=self::join("usuario", "usuario.rut", "=", "paciente.rut")->where("paciente.rut", "=", $rut)
		->select("nombres", "apellido_paterno", "apellido_materno", "telefono", "correo", "fecha_nacimiento", "genero", "id_institucion")->first();
		
		if($datos!=null){
			$fecha=($datos->fecha_nacimiento == null) ? "" : date("d-m-Y", strtotime($datos->fecha_nacimiento));

			$domicilio=self::leftJoin("domicilio", "domicilio.rut", "=", "paciente.rut")
			->where("paciente.rut", "=", $rut)
			->select("calle", "numero", "departamento", "latitud", "longitud", "domicilio.id_comuna")->first();

			$patologias=self::leftJoin("patologia", "rut_paciente", "=", "rut")
			->where("rut", "=", $rut)
			->select("id_cie10", "id_patologia", "rut_encargado", DB::raw("to_char(fecha_diagnostico, 'dd-Mon-YYYY') as fecha_diagnostico"))->get();

			$response=[
				"rut" => $rut,
				"dv" => Funciones::obtenerDV($rut),
				"nombre" => ucwords($datos->nombres),
				"paterno" => ucwords($datos->apellido_paterno),
				"materno" => ucwords($datos->apellido_materno),
				"telefono" => $datos->telefono,
				"correo" => $datos->correo,
				"fecha" => $fecha,
				"genero" => $datos->genero,
				"id_institucion" => $datos->id_institucion,
				"calle" => ucwords($domicilio->calle),
				"numero" => $domicilio->numero,
				"departamento" => $domicilio->departamento,
				"latitud" => $domicilio->latitud,
				"longitud" => $domicilio->longitud,
				"id_comuna" => $domicilio->id_comuna,
				"patologias" => $patologias
			];
		}
		return $response;
	}


	public static function obtenerRutNombrePacientes($query){
		$pacientes=[];
		$datos=self::select("usuario.nombres", "usuario.apellido_paterno", "usuario.apellido_materno", "usuario.rut")
		->join("usuario", "usuario.rut", "=", "paciente.rut")
		->where("paciente.paciente_visible", "=", true)
		->where(function($consulta) use ($query){
			$consulta->where(DB::raw('TO_CHAR(usuario.rut, \'99999999\')'), 'LIKE', '%'.$query.'%')
			->orWhere(function($consulta) use ($query){
				$consulta->orWhere(DB::raw('upper(usuario.nombres)'), 'LIKE', '%'.strtoupper($query).'%')
				->orWhere(DB::raw('upper(usuario.apellido_paterno)'), 'LIKE', '%'.strtoupper($query).'%')
				->orWhere(DB::raw('upper(usuario.apellido_materno)'), 'LIKE', '%'.strtoupper($query).'%');
			});
		})->get();
	
		foreach($datos as $dato){
			$pacientes[]=[
				"rut" => $dato->rut."-".Funciones::obtenerDV($dato->rut),
				"nombre" => ucwords($dato->nombres)." ".ucwords($dato->apellido_paterno)." ".ucwords($dato->apellido_materno)
			];
		}
		return  $pacientes;
	}

	public static function obtenerPacientesPuertaNicturia($evento, $rutUsuario, $rutPaciente){
		$pacientes = [];
		$datos = DB::table("paciente as p")->join("usuario as u", "u.rut", "=", "p.rut")
		->join("contacto as c", "c.rut_paciente", "=", "u.rut");
		//->join("alerta_detalle as ad", "p.rut", "=", "ad.rut_paciente")
		//->join("alerta as a", "a.id_alerta", "=", "ad.id_alerta");
		if($rutPaciente != "") $datos = $datos->where("p.rut", "=", $rutPaciente);
		$datos = $datos->where("p.id_evento", "=", $evento)->where("c.rut_encargado", "=", $rutUsuario)->whereNull("c.fin")->where("u.visible", "=", TRUE)
		->select("u.nombres", "u.apellido_paterno", "u.apellido_materno", DB::raw("count(*) as total"), "c.rut_paciente", "p.id_evento", "p.fecha_nacimiento")
		->groupBy("u.nombres", "u.apellido_paterno", "u.apellido_materno", "c.rut_paciente", "p.id_evento", "p.fecha_nacimiento")->get();
		foreach($datos as $dato){
			$nombre = ($dato->nombres == null) ? "" : $dato->nombres;
			$apellido_paterno = ($dato->apellido_paterno == null) ? "" : $dato->apellido_paterno;
			$apellido_materno = ($dato->apellido_materno == null) ? "" : $dato->apellido_materno;
			$link = ($dato->id_evento == EnumEvento::$CAIDA) ? "#alertaCaida" : "#alertaPuertaNicturia";
			$pacientes[] = [
				trim($nombre." ".$apellido_paterno." ".$apellido_materno),
				$dato->rut_paciente."-".Funciones::obtenerDV($dato->rut_paciente),
				($dato->fecha_nacimiento == null) ? "" : date("d-m-Y", strtotime($dato->fecha_nacimiento)),
				$dato->total,
				$dato->rut_paciente,
				$link
			];
		}
		return $pacientes;
	}

	public static function obtenerPacientesCaida($evento, $rutUsuario, $rutPaciente){
		$pacientes = [];
		$datos = DB::table("paciente as p")->join("usuario as u", "u.rut", "=", "p.rut")
		->join("contacto as c", "c.rut_paciente", "=", "u.rut")
		->join("alerta_caida as ac", "p.rut", "=", "ac.rut_paciente");
		if($rutPaciente != "") $datos = $datos->where("p.rut", "=", $rutPaciente);
		$datos = $datos->where("ac.id_evento", "=", $evento)->where("c.rut_encargado", "=", $rutUsuario)
		->select("u.nombres", "u.apellido_paterno", "u.apellido_materno", DB::raw("count(*) as total"), "c.rut_paciente", "ac.id_evento", "p.fecha_nacimiento")
		->groupBy("u.nombres", "u.apellido_paterno", "u.apellido_materno", "c.rut_paciente", "ac.id_evento", "p.fecha_nacimiento")->get();
		foreach($datos as $dato){
			$nombre = ($dato->nombres == null) ? "" : $dato->nombres;
			$apellido_paterno = ($dato->apellido_paterno == null) ? "" : $dato->apellido_paterno;
			$apellido_materno = ($dato->apellido_materno == null) ? "" : $dato->apellido_materno;
			$link = ($dato->id_evento == EnumEvento::$CAIDA) ? "#alertaCaida" : "#alertaPuertaNicturia";
			$pacientes[] = [
				trim($nombre." ".$apellido_paterno." ".$apellido_materno),
				$dato->rut_paciente."-".Funciones::obtenerDV($dato->rut_paciente),
				($dato->fecha_nacimiento == null) ? "" : date("d-m-Y", strtotime($dato->fecha_nacimiento)),
				$dato->total,
				$dato->rut_paciente,
				$link
			];
		}
		return $pacientes;
	}

	public static function obtenerRutUsuario($institucion, $idComuna, $rutUsuario){
		$response[] = ["rut" => 0, "rutConDV" => "Todos"];
		$query = self::join("usuario as u", "paciente.rut", "=", "u.rut")
		->join("contacto as e", "e.rut_paciente", "=", "paciente.rut");
		if($idComuna != 0) $query = $query->join("domicilio as d", "d.rut", "=", "u.rut");
		$query = $query->where("e.rut_encargado", "=", $rutUsuario)->whereNull("e.fin");
		if($idComuna != 0) $query = $query->where("d.id_comuna", "=", $idComuna)->whereNull("d.fin");
		if($institucion != 0) $query = $query->where("u.id_institucion", "=", $institucion);
		$datos = $query->select("u.rut", "nombres", "apellido_paterno", "apellido_materno")->orderBy("nombres", "apellido_paterno", "apellido_materno")->get();
		
		foreach($datos as $dato){
			$dv = Funciones::obtenerDV($dato->rut);
			$response[] = ["rut" => $dato->rut, "rutConDV" => $dato->rut."-".$dv];
		}

		return $response;
	}

	public static function obtenerTipoEvento($rutUsuario){		
		$query = self::where("rut", "=", $rutUsuario)->select("id_evento")->first();
		return $query->id_evento;
	}

}