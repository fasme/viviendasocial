<?php

namespace App\models;

use Eloquent;
use DB;

class ConfiguracionNicturia extends Eloquent{
	protected $table = 'configuracion_nicturia';
	public $timestamps = false;
	protected $primaryKey = 'id_configuracion_nicturia';
	
	public static function obtenerIDConfiguracion($umbral_ultrasonido,$minutos_electrodo,$umbral_electrodo)
	{
		$resultado=DB::select(DB::raw("SELECT id_configuracion_nicturia FROM configuracion_nicturia 
							WHERE configuracion_electrodo=$minutos_electrodo
							AND configuracion_electrodo_umbral=$umbral_electrodo
							AND configuracion_ultrasonido=$umbral_ultrasonido"));
		if($resultado)
			return $resultado[0]->id_configuracion_nicturia;
		return null;
	}
	public static function obtenerConfiguraciones()
	{
		return DB::select(DB::raw("SELECT * FROM configuracion_nicturia
			ORDER BY configuracion_electrodo"));
	}
}
?>
