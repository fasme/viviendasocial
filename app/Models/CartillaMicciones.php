<?php

class CartillaMicciones extends Eloquent{
	protected $table = 'cartilla_micciones';
	public $timestamps = false;
	protected $primaryKey = 'id_cartilla_micciones';



	public static function obtenerCartillaPaciente($rut){
		
		$datos=DB::table( DB::raw("(SELECT * from cartilla_micciones where rut_paciente=$rut) as ra"))->get(); 
		return $datos;
	}
	public static function grafico()
	{
		$rut=Input::get("rut");
		DB::statement(
			DB::raw(
				"
				SET TIME ZONE  0;
				"
			)
		);
		$resultado=DB::select(
			DB::raw(
				"
				SELECT
				EXTRACT(EPOCH FROM cartilla.fecha_cartilla)*1000 AS milisegundos_fecha,
				cartilla.veces
				FROM
				(
					SELECT
					cm.fecha_cartilla,
					(
						SELECT
						COUNT(*)AS veces
						FROM detalle_cartilla_micciones
						WHERE detalle_cartilla_micciones.id_cartilla_micciones=cm.id_cartilla_micciones
					)AS veces,
					cm.rut_paciente
					
					FROM cartilla_micciones AS cm
				
				)AS cartilla
				WHERE rut_paciente=$rut
				
				ORDER BY milisegundos_fecha
				"
				)
			);
		

		$datos=array();
		foreach($resultado as $r)
		{
			$datos["tiempo_dias"][]=array((int)$r->milisegundos_fecha,$r->veces);	
		}
		
		return $datos;
	}
	
}
