<?php

class ConfiguracionEvento extends Eloquent{

	protected $table = 'configuracion_evento';
	public $timestamps = false;
	protected $primaryKey = 'id_configuracion_evento';

	public static function obtenerConfiguracion($rutPaciente, $tipoEvento){
		return self::join("encargado as e", "configuracion_evento.rut_encargado", "=", "e.rut")
		->join("contacto as c", "e.rut", "=", "c.rut_encargado")->where("c.rut_paciente", "=", $rutPaciente)
		->where("configuracion_evento.id_evento", "=", $tipoEvento)->first();
	}

}