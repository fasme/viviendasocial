<?php
namespace App\models;
use Illuminate\Database\Eloquent\Model;
use DB;

class Datocaidas extends Model{
	
	protected $table = "dato_caidas";
	protected $primaryKey = "id_dato_caidas";
	public $timestamps = false;

}

?>