<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class NoticiaLabitec extends Model{

	protected $connection = 'pgsqlLabitec';
	protected $table = 'noticias';
	public $timestamps = false;
	protected $primaryKey = 'id_noticias';

}