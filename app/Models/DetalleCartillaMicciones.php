<?php
namespace App\models;

use Eloquent;
use DB;

class DetalleCartillaMicciones extends Eloquent{
	protected $table = 'detalle_cartilla_micciones';
	public $timestamps = false;
	protected $primaryKey = 'id_detalle_cartilla_micciones';

	public static function obtenerDetallePaciente($rut){
	
	$datos=DB::table( DB::raw("(SELECT c.id_cartilla_micciones,c.rut_paciente,c.fecha_cartilla,c.hora_acostarse,d.id_detalle_cartilla_micciones,d.id_cartilla_micciones,d.horario,TO_CHAR(horario,'HH24:MI:SS') AS hora_detalle,d.volumen,d.perdida_orina::text from detalle_cartilla_micciones as d,
	cartilla_micciones as c where d.id_cartilla_micciones=c.id_cartilla_micciones and c.rut_paciente=$rut order by c.id_cartilla_micciones) as ra"))->get(); 
	return $datos;
}
	
}
