<?php
use Illuminate\Database\Eloquent\Model;
namespace App\models;
use Eloquent;


class IndiceBarthel extends Eloquent{
	
	protected $table = 'test_barthel';
	public $timestamps = false;
	protected $primaryKey = 'id_test_barthel';

}