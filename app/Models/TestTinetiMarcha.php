<?php

namespace App\models;

class TestTinetiMarcha extends Eloquent{
	
	protected $table = 'test_tinetti_marcha';
	public $timestamps = false;
	protected $primaryKey = 'id_test_tinetti_marcha';

	public static function existeEncuesta($rut, $inicial){
		$datos=self::select("id_test_tinetti_marcha")
		->where("rut_paciente", "=", $rut)
		->where("inicial", "=", $inicial)
		->first();
		if($datos == null) return false;
		else return $datos->id_test_tinetti_marcha;
	}	

}