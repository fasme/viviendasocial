<?php 
use Illuminate\Database\Eloquent\Model;
namespace App\models;
use Eloquent;

class TestCharlson extends Eloquent{
	protected $table = 'test_charlson';
	public $timestamps = false;
	protected $primaryKey = 'id_test_charlson';
}

?>