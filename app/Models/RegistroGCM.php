<?php
namespace App\models;
use Illuminate\Database\Eloquent\Model;
use DB;

class RegistroGCM extends Model{

	protected $table = 'registro_gcm';
	public $timestamps = false;

	public static function obtenerGCMPorPaciente($rut, $device){
		$response = [];
		$datos = DB::table("contacto as c")
		->join("usuario as u", "u.rut", "=", "c.rut_paciente")
		->join("registro_gcm as r", "r.rut", "=", "c.rut_encargado")
		->where("u.visible", "=", true)
		->whereNull("c.fin")
		->where("u.rut", "=", $rut)
		->where("dispositivo_movil", "=", $device)
		->select(DB::raw("distinct(r.id_gcm)"))->get();
		foreach($datos as $dato) $response[] = $dato->id_gcm;
		return $response;
	}

}
