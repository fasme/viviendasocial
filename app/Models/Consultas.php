<?php

namespace App\models;

use DB;

class Consultas{

	public static function obtenerEnum($nombre){
		$response=array();
		$enums=DB::table("pg_enum as e")
		->join("pg_type as t", "e.enumtypid", "=", "t.oid")
		->select("e.enumlabel")
		->where("t.typname", "=", $nombre)->get();
		foreach ($enums as $enum) {
			$response[$enum->enumlabel]=ucwords($enum->enumlabel);
		}
		return $response;
	}
	
}

?>