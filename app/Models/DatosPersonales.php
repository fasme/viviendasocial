<?php

namespace App\models;

class DatosPersonales {
	public $alergia = "";
	public $fuma = "";
	public $fumaN = "";
	public $alcohol = "";
	public $alcoholN = "";
	public $peso = "";
	public $talla = "";
	public $imc = "";
	public $cintura = "";
	public $estatura = "";
	public $psistolica = "";
	public $diastolica = "";
	public $vivecon = "";
}