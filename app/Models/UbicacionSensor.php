<?php

class UbicacionSensor extends Eloquent {

	protected $table = 'ubicacion_sensor';
	public $timestamps = false;
	protected $primaryKey = 'id_ubicacion_sensor';
	
}