<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Alerta;
use App\Models\Datomonoxido;
use App\Models\Datocaidas;
use DB;
use App\Models\Gpsboton;
use App\Models\Usuario;
use App\Models\Contacto;
use App\Models\RegistroGCM;
use Funciones;

class AlertaController extends Controller {

    public function registrarAlertaCaida(Request $request){


        $id_pieza=$request->pieza;
        $rut_paciente=$request->rut_paciente;
        $prediction=$request->prediction;
        $dato=$request->dato;
        $seq = $request->seq;
        $fecha=date("Y-m-d H:i:s");
        $id_casa = $request->id_casa;
        $id_evento = 2;
        $id_sensor = $request->id_sensor;
        $es_alerta = $request->es_alerta;



        if( $dato != null ){
			$dato = str_replace("[", "(", $dato);
			$dato = str_replace("]", ")", $dato);
			//$alerta->features1 = $features1;
		}

		if( $seq != null ){
			//hay que agregar comillas dobles por cada arreglo
			$seq = str_replace("[", '"(', $seq);
			$seq = str_replace("]", ')"', $seq);
			//$alerta->features1 = $features1;
			//hay que eliominar el primer y ultimo caracter
			$seq = substr($seq, 1);
			$seq = substr($seq, 0, -1);
		}



		$dato_caidas = new Datocaidas;
		$dato_caidas->rut_paciente = $rut_paciente;
		$dato_caidas->id_sensor = $id_sensor;
		$dato_caidas->dato = $dato;
		$dato_caidas->prediccion = $prediction;
		$dato_caidas->medicion = $seq;
		$dato_caidas->save();
    //return $dato_caidas->id_dato_caidas;


        if($es_alerta == 'true'){
          $alerta = new Alerta;
          $alerta->id_evento = $id_evento;
          $alerta->fecha = $fecha;
          $alerta->rut_paciente = $rut_paciente;
          $alerta->id_sensor = $id_sensor;
          $alerta->save();

          //si es alerta se actualiza la tabla de datos con su alerta correspondiente
          $dato_caidas->id_alerta = $alerta->id_alerta;
          $dato_caidas->save();

          return $this->enviarAlerta($rut_paciente,"caidas");
        }

        return "solo dato  caidas";

    }

    public function registrarAlertaGases(Request $request, $rut_paciente,$id_sensor,$alerta_monoxido,$gas1,$gas2,$gas3,$humedad,$temperatura){
    	//echo json_encode(array("ok"=>$rut_paciente));
    	//return "a";
    	//return $request->all();
        $id_sensor=$id_sensor;
        $gas1 = $gas1;
        $gas2 = $gas2;
        $gas3 = $gas3;
        $humedad = $humedad;
        $temperatura = $temperatura;
        $fecha=date("Y-m-d H:i:s");
        $rut_paciente = $rut_paciente;
        $id_evento = 3;
        $alerta_monoxido = $alerta_monoxido;

        $datomonoxido = new Datomonoxido;
        $datomonoxido->rut_paciente = $rut_paciente;
        $datomonoxido->id_sensor = $id_sensor;
        $datomonoxido->id_alerta = null;
        $datomonoxido->fecha_dato = $fecha;
        $datomonoxido->gas1 = $gas1;
        $datomonoxido->gas2 = $gas2;
        $datomonoxido->gas3 = $gas3;
        $datomonoxido->temperatura = $temperatura;
        $datomonoxido->humedad = $humedad;
        $datomonoxido->alerta_monoxido = $alerta_monoxido;

        $datomonoxido->save();



        if($alerta_monoxido == 'true'){
	        $alerta = new Alerta;
	        $alerta->id_evento = $id_evento;
	        $alerta->fecha = $fecha;
	        $alerta->rut_paciente = $rut_paciente;
	        $alerta->id_sensor = $id_sensor;
	        $alerta->save();

	        $datomonoxido = Datomonoxido::find($datomonoxido->id_dato_monoxido);
	        $datomonoxido->id_alerta = $alerta->id_alerta;
	        $datomonoxido->save();

          return $this->enviarAlerta($rut_paciente,"monoxido");
    	}


    	echo json_encode(array("ok"=>$rut_paciente));

    }

    public function registrarAlertaBotonPanico(Request $request){
    	$latitud = $request->latitud;
    	$longitud = $request->longitud;
    	$longitud = $request->longitud;
    	$rut_paciente = $request->rut_paciente;
    	$id_boton = $request->id_boton;
    	$id_evento = 6;
    	$es_alerta = $request->es_alerta;
    	$fecha=date("Y-m-d H:i:s");

    	$gpsboton = new Gpsboton;
    	$gpsboton->latitud = $latitud;
    	$gpsboton->longitud = $longitud;
    	$gpsboton->rut_paciente = $rut_paciente;
    	$gpsboton->id_boton = $id_boton;
    	$gpsboton->save();

    	if($es_alerta == 'true'){
    		$alerta = new Alerta;
    		$alerta->id_evento = $id_evento;
    		$alerta->fecha = $fecha;
    		$alerta->rut_paciente = $rut_paciente;
    		$alerta->id_sensor = $id_boton;
    		$alerta->save();

        return $this->enviarAlerta($rut_paciente,"boton");
    	}



    	echo json_encode(array("ok"=>$alerta));
    }


    public function enviarAlerta($rut,$tipo){
      try{


      $rut = $rut;
      $tipo = $tipo;
      $fecha=date("Y-m-d H:i:s");

      $paciente = Usuario::find($rut);
      $contactosSms    = Contacto::obtenerContactoPacienteSms($rut);
      $contactosCorreo = Contacto::obtenerContactoPacienteCorreo($rut);
      $idGcmsAndroid   = RegistroGCM::obtenerGCMPorPaciente($rut, "android");
			$idGcmsIOS       = RegistroGCM::obtenerGCMPorPaciente($rut, "ios");
      $enviarAlerta = false;

      if($paciente == null) {
				DB::commit();
				return Response::json(["envio" => false, "msg" => "El paciente no se encuentra registrado"]);
			}


      $msg = "";
			$msgCorreo = "";

      if($tipo == "caidas"){
        $msg = "Alerta de caída";
        $enviarAlerta =true;
      }
      if($tipo == "monoxido"){
        $msg = "Alerta de monóxido";
        $enviarAlerta =true;
      }
      if($tipo == "boton"){
        $msg = "Alerta de pánico";
        $enviarAlerta =true;
      }

      $alerta = Alerta::find('rut_paciente')->orderBy('id_alerta', 'desc') ->first();

			/*if($tipo == "puertas") $msg = "Alerta de puerta";
			if($tipo == "nicturia") $msg = "Alerta de nicturia";
			if($tipo == "caida") $msg = "Alerta de caída";
			if($tipo == "ayuda") $msg = "Alerta el paciente esta solicitando ayuda";
      */
			$msgCorreo = $msg;
      $correoEnviado = false;
			$usuario = Usuario::find($rut);
			$dv = Funciones::obtenerDV($rut);
      $msg .= " - Paciente: " . $usuario->nombres . " " . $usuario->apellido_paterno ." [".$rut."-".$dv."]";
			$msg .= " - Fecha: " . date("d-m-Y H:i:s", strtotime($fecha));			if( ( count($contactosCorreo) != 0 ) && $enviarAlerta ){
				foreach ($contactosCorreo as $item)
				{
					$correoEnviado = true;
				    $nombreContactoCorreo = $item['nombre']." ".$item['apellidos'];
				    $correoContactoCorreo = $item['correo'];
				 	  Funciones::enviarCorreoAlerta($usuario->nombres . " " . $usuario->apellido_paterno ." [".$rut."-".$dv."]", $correoContactoCorreo, $msgCorreo, date("d-m-Y H:i:s", strtotime($fecha)), $alerta->id_alerta);
				}
			}

        $entroGCM = count($idGcmsAndroid);

				error_log(json_encode($idGcmsAndroid),3,"fall.log");
			if( (count($idGcmsAndroid) != 0 || count($idGcmsIOS) != 0  ) && $enviarAlerta){
				$entroGCM = "SI";

				Funciones::enviarNotificacion($idGcmsAndroid, $idGcmsIOS, $msg);

			}

      return response()->json(["envio" => true, "correoEnviado" => $correoEnviado, "enviarAlerta" => $enviarAlerta,  "GCMIN" =>$entroGCM ]);
      		}catch(Exception $ex){
      			DB::rollback();
      			$correoEnviado=false;
      			return response()->json(["envio" => false, "exception" => $ex->getMessage(), "correoEnviado" => $correoEnviado]);
      		}
    }

}
