<?php

namespace App\Http\Controllers;

use App\Models\FichaPacienteVista;
use Illuminate\Http\Request;

use Response;

class FichaPacienteVistaController extends BaseController{

	public function obtenerDatosPaciente(Request $request){

		$rut= $request->input('rut');

		return response()->json(FichaPacienteVista::obtenerDatosPaciente($rut));
		//return Response::json(FichaPacienteVista::obtenerDatosPaciente($rut));
	}
}