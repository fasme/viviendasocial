<?php

class ConfiguracionEventoController extends BaseController{

	public function obtenerEventos(){
		$rutUsuario=Auth::user()->rut;
		
		$datosNicturia = ConfiguracionEvento::select(DB::raw("to_char(hora_inicio, 'HH24:MI') as hora_inicio"), 
			DB::raw("to_char(hora_fin, 'HH24:MI') as hora_fin"),
			"numero_repeticiones")
		->where("rut_encargado", "=", $rutUsuario)->where("id_evento", "=", "3")
		->first();

		$datosPuertas = ConfiguracionEvento::where("rut_encargado", "=", $rutUsuario)->where("id_evento", "=", "2")
		->first();

		$inicio = ($datosNicturia == null) ? "21:00" : $datosNicturia->hora_inicio;
		$fin = ($datosNicturia == null) ? "6:00" : $datosNicturia->hora_fin;
		$nicturias = ($datosNicturia == null) ? "10": $datosNicturia->numero_repeticiones;

		$intervalo = ($datosPuertas == null) ? "1" : $datosPuertas->intervalo;
		$puertas = ($datosPuertas == null) ? "10": $datosPuertas->numero_repeticiones;

		return Response::json([
			"inicio" => $inicio, 
			"fin" => $fin,
			"puertas" => $puertas,
			"nicturias" => $nicturias,
			"intervalo" => $intervalo
		]);
	}

	public function editarEvento(){
		$rutUsuario=Auth::user()->rut;
		try{
			DB::beginTransaction();
			if(Input::has("puertas")){
				$numero = Input::get("puertas");
				$intervalo = Input::get("intervalo");
				$puerta = ConfiguracionEvento::where("rut_encargado", "=", $rutUsuario)->where("id_evento", "=", "2")->first();
				if($puerta == null) $puerta = new ConfiguracionEvento;
				$puerta->id_evento = "2";
				$puerta->rut_encargado = $rutUsuario;
				$puerta->numero_repeticiones = $numero;
				$puerta->intervalo = $intervalo;
				$puerta->save();
			}else{
				$inicio = Input::get("nic-inicio");
				$fin = Input::get("nic-fin");
				$repeticiones = Input::get("nic-repeticiones");
				$nicturia = ConfiguracionEvento::where("rut_encargado", "=", $rutUsuario)->where("id_evento", "=", "3")->first();
				if($nicturia == null) $nicturia = new ConfiguracionEvento;
				$nicturia->id_evento = "3";
				$nicturia->rut_encargado = $rutUsuario;
				$nicturia->hora_inicio = $inicio;
				$nicturia->hora_fin = $fin;
				$nicturia->numero_repeticiones = $repeticiones;
				$nicturia->save();
			}
			DB::commit();
			return Response::json(["exito" => "La configuración ha sido editada"]);
		}catch(Exception $ex){
			DB::rollback();
			return Response::json(["error" => "Error al editar la configuración", "msg" => $ex->getMessage()]);
		}
	}


}