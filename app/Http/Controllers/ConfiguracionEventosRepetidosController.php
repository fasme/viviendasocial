<?php

class ConfiguracionEventosRepetidosController extends BaseController{

	public static function guardar()
	{
		$fecha_inicial=Input::get("fecha_inicial");
		$rut_paciente=Input::get("rut");
		$rut_usuario=Auth::user()->rut;
		$margen=Input::get("margen");
		$frecuencia=Input::get("frecuencia");
		
		
		$config=ConfiguracionEventosRepetidos::where("rut_paciente","=",$rut_paciente)->where("rut_usuario","=",$rut_usuario)->first();
		
		if($config)
		{
			$config->fecha_inicial_er=$fecha_inicial;
			$config->margen_error_er=$margen;
			$config->frecuencia_er=$frecuencia;
			$config->save();

		}
		else
		{
			$conf=new ConfiguracionEventosRepetidos;
			$conf->fecha_inicial_er=$fecha_inicial;
			$conf->margen_error_er=$margen;
			$conf->frecuencia_er=$frecuencia;
			$conf->rut_paciente=$rut_paciente;
			$conf->rut_usuario=$rut_usuario;
			$conf->save();
		}
		return Response::json(["exito"=>"Se ha guardado su configuración."]);
	}
	
	public static function cargar()
	{
		$rut=Input::get("rut");
		$rut_usuario=Input::get("rut_usuario");
		$rut_usuario= ($rut_usuario=="") ? Auth::user()->rut : $rut_usuario;
		
		$r=DB::select(
			DB::raw(
				"
				SELECT
				fecha_inicial_er,
				margen_error_er,
				frecuencia_er
				FROM configuracion_eventos_repetidos
				WHERE rut_paciente=$rut
				AND rut_usuario=$rut_usuario
				"
				));
		$datos=array();
		if($r)
		{
			$resultado=$r[0];
			
			$datos["fecha_inicial"]=$resultado->fecha_inicial_er;
			$datos["margen"]=$resultado->margen_error_er;
			$datos["frecuencia"]=$resultado->frecuencia_er;
			
			return Response::json($datos);
		}
		$fecha_inicio=$r=DB::select( DB::raw("SELECT fecha_inicio_estudio FROM paciente WHERE rut=$rut"));
		if($fecha_inicio)
		{
			$fecha_inicio=$fecha_inicio[0]->fecha_inicio_estudio;
			$datos["fecha_estudio"]=$fecha_inicio;
			return Response::json($datos);
		}
		return Response::json(["error"=>"No se han encontrado datos"]); //no es necesario mostrarlo
	}

}
?>
