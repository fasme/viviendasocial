<?php

class ArchivoController extends BaseController {
    /**
     * Recibe un archivos y los guarda en public/files
     */
  
    public static function recibir() {

        $retorno = false;
     //   $url = 'http://ehomeseniors.cl/enlace/registrarAlertaPuertaNicturia';
      $url = 'http://127.0.0.1:8081/enlace/registrarAlertaPuertaNicturia';

        if( count($_FILES) == 0 ) return Response::json( $retorno );
        try {
            $carpetaArchivos = "/public/files/";
            foreach( $_FILES as $archivo ){
                $rutaArchivos = base_Path().$carpetaArchivos.$archivo['name'];

                rename( $archivo['tmp_name'], $rutaArchivos );
                chmod($rutaArchivos, 0777);
                //$data[] = $rutaArchivos;
                
                $name = explode(".", $archivo['name']);
                $id = $name[0];
                $type = $name[1];

                if($type=="txt"||$type=="csv")
                {
                	self::leerCSV($rutaArchivos,$id);
                }
                else if($type=="xls")
                {
                	self::leerXLS($rutaArchivos);
                }
              //S  var_dump($archivo['name']);

				//obtener valores de formulas
				//$objPHPExcel->getActiveSheet()->getCell('A6')->getFormattedValue();
				//$objPHPExcel->getActiveSheet()->getCell('A4')->getCalculatedValue();
                
            }
        } catch (Exception $e) { $retorno = $e; }
/*        
        $results = Paciente::Select('id_evento')->Where('rut','=',$id)->first();
        if($results['id_evento']==3){
            $tipo = 'nicturia';
        }else{
            $tipo = 'puertas';
        }
        
        $jsonData = array(
            'tipo' => $tipo,
            'fecha_envio' => date("d-m-y H:i:s"),
            'id_paciente' => $id,
            'listado_eventos' => $listDet
        );

        //Iniciar cURL.
        
        $ch = curl_init($url);
        
        //Codificar array en JSON.
        $jsonDataEncoded = json_encode($jsonData);
        
        //Decir a cURL que queremos enviar una solicitud POST.
        curl_setopt($ch, CURLOPT_POST, 1);
        
        
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
      //  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST	, 0);
       // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER	, 0);
        
        //Adjuntar nuestra cadena JSON codificada a los campos de la POST.
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
       
        
        //Establecer el tipo de contenido a application/json
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
       
                 
        //Ejecutar la solicitud
        $resultData  = curl_exec($ch);
       
        /*
        $python = base_Path().$carpetaArchivos."ejemplo.py";
        $result = shell_exec('python '.$python.' ' . escapeshellarg(json_encode($data)));
        $resultData = json_decode($result, true);

        // This will contain: array('status' => 'Yes!')
        //var_dump($resultData);


        //exec("/usr/bin/python ".base_Path().$carpetaArchivos."ejemplo.py");
        */
        return Response::json( true );
    }

    private static function leerRut($sheet)
    {
    	return $sheet->getCellByColumnAndRow(self::$RUT,1)->getValue();
    }
    private static function leerTitulo($sheet)
    {
    	return $sheet->getTitle();
    }
    private static function formatoHora($hora)
    {
    	$hora_array=preg_split("/\D/",$hora);
    	if(count($hora_array)>=3)
    	{
    		$hora_array[0]=(int)$hora_array[0];
    		$hora_array[1]=(int)$hora_array[1];
    		$hora_array[2]=(int)$hora_array[2];
    	}
    	if(strpos($hora,"PM")===true)
    	{
    		
    		$hora_nueva=($hora_array[0]+12);
    		$minuto_nuevo=$hora_array[1]<10?"0".$hora_array[1]:$hora_array[1];
    		$segundo_nuevo=$hora_array[2]<10?"0".$hora_array[2]:$hora_array[2];
    		return "$hora_nueva:$minuto_nuevo:$segundo_nuevo";
    	}
    	else
    	{
    		$hora_nueva=$hora_array[0]<10?"0".$hora_array[0]:$hora_array[0];
    		$minuto_nuevo=$hora_array[1]<10?"0".$hora_array[1]:$hora_array[1];
    		$segundo_nuevo=$hora_array[2]<10?"0".$hora_array[2]:$hora_array[2];
    		return "$hora_nueva:$minuto_nuevo:$segundo_nuevo";
    	}
    }
    private static function leerFechaCSV($fecha)
    {
    	$dia=substr($fecha,0,2);
    	$mes=substr($fecha,2,2);
    	$año=date("Y");
    	return "$año-$mes-$dia";
    }
    private static function leerIDCSV($id)
    {
    	$id_nuevo=substr($id,0);
    	
    	return $id_nuevo;
    }
    private static function leerCSV($rutaArchivos/*nombre con ruta completa y extension*/,$nombreArchivo/*solo el nombre, sin extension*/)
    {
    	$datos=explode("_",$nombreArchivo);
    	
    	$ultrasonido=$datos[2];
    	$electrodo_minuto=$datos[3];
    	$electrodo_umbral=$datos[4];
    	$rut=$datos[5];
    	$fp = fopen($rutaArchivos, "r");
    	
    	$id_config=ConfiguracionNicturia::obtenerIDConfiguracion($ultrasonido,$electrodo_minuto,$electrodo_umbral);
		while(($linea = fgetcsv($fp, 50, "\t")) !== FALSE){
			if(!is_numeric($linea[0]))
				continue;
			$resultadoMicciones=new ResultadoMicciones;
			$resultadoMicciones->id=self::leerIDCSV($linea[0]);
			$resultadoMicciones->id_configuracion_nicturia=$id_config;
			$resultadoMicciones->ultrasonido=$linea[3]=="FALSE"?false:true;
			$resultadoMicciones->fecha=self::leerFechaCSV($linea[2])." ".$linea[1];
			$resultadoMicciones->rut_paciente=$rut;
			$resultadoMicciones->save();
			
		}
		fclose($fp);
		unlink($rutaArchivos);
    }
    private static function leerFechaXLS($fecha)
    {
    	$f=preg_split("/\D/",$fecha);
    	$f[0]=(int)$f[0];
    	$f[1]=(int)$f[1];
    	$f[2]=(int)$f[2];
    	$fecha_nueva=$f[2]."-".($f[1]<10?"0".$f[1]:$f[1])."-".($f[0]<10?"0".$f[0]:$f[0]);
    	return $fecha_nueva;
    }
    private static function leerXLS($rutaArchivos)
    {
		$datos=PHPExcel_IOFactory::load($rutaArchivos);
		//echo "leyendo archivo\n";
		foreach($datos->getAllSheets() as $sheet){
			
			$titulo=explode("_",$sheet->getTitle());
	//		echo "leyendo pestaña:".$sheet->getTitle()."\n";
			if(count($titulo)!=2)
				continue;
			$rut_paciente=$titulo[1];
			if(!is_numeric($rut_paciente))
				continue;
		//	echo "rut:".$rut_paciente."<br>";
			
			$fecha_leida=false;
			
			$fecha=null;
			$hora_acostarse=null;
			$detalles=array();
			$agregar_detalle=false;
			$un_dia=new DateInterval('P1D');
			$fecha_detalle=new DateTime();
			$dia_sumado=false;
			foreach ($sheet->getRowIterator() as $row) {
	//			echo "leyendo filas\n";
	//			echo "leyendo valor columna 0(A):".$sheet->getCellByColumnAndRow(0,$row->getRowIndex())->getValue()."\n";
				if($sheet->getCellByColumnAndRow(0,$row->getRowIndex())->getValue()==="Fecha")
				{
					$dia_sumado=false;
	//				echo $sheet->getCellByColumnAndRow(0,$row->getRowIndex())->getValue()." es igual a Fecha\n";
					
					$fecha_leida=true;
					$sheet->getStyleByColumnAndRow(1,$row->getRowIndex())
						->getNumberFormat()
						->setFormatCode(
							PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY
						);
					$asd=$sheet->getCellByColumnAndRow(0,$row->getRowIndex())->getFormattedValue();
					$fec=$sheet->getCellByColumnAndRow(1,$row->getRowIndex())->getValue();
			
			
					if(is_numeric($fec))
					{
						
						$fec=$sheet->getCellByColumnAndRow(1,$row->getRowIndex())->getFormattedValue();
			
					}
			//		echo "fecha leída:".$fec."\n";
					$fecha=self::leerFechaXLS($sheet->getCellByColumnAndRow(1,$row->getRowIndex())->getFormattedValue());
					$f=preg_split("/\D/",$fecha);
					$fecha_detalle->setDate($f[0],$f[1],$f[2]);
			//		echo "fecha leída para guardar:".$fecha."\n";
					
				}
				
				if($fecha_leida)
				{
					
			//		echo "leyendo valor:".$sheet->getCellByColumnAndRow(0,$row->getRowIndex())->getValue()."\n";
					if($sheet->getCellByColumnAndRow(0,$row->getRowIndex())->getFormattedValue()=="")
					{
			//			echo $sheet->getCellByColumnAndRow(0,$row->getRowIndex())->getValue()." es igual a ''\n";
					//	echo "guardando  registro\n";
					//	echo "fecha:$fecha\n";
					//	echo "hora acostarse:$hora_acostarse\n";
				//		echo "rut:$rut_paciente\n";
						$fecha_leida=false;
						$agregar_detalle=false;
						try{
							$cartillaMicciones=new CartillaMicciones;
							$cartillaMicciones->fecha_cartilla=$fecha;
							$cartillaMicciones->hora_acostarse=$hora_acostarse;
							$cartillaMicciones->rut_paciente=$rut_paciente;
							$cartillaMicciones->save();
				//			echo "cartilla micciones guardada\n";
							foreach($detalles as $detalle)
							{
								try{
									$detalleMicciones=new DetalleCartillaMicciones;
									$detalleMicciones->id_cartilla_micciones=$cartillaMicciones->id_cartilla_micciones;
									$detalleMicciones->horario=empty($detalle["horario"])?null:$detalle["horario"];
									$detalleMicciones->volumen=$detalle["volumen"];
									$perdida=null;
									if($detalle["perdida"]=="No")
										$perdida=false;
									if($detalle["perdida"]=="Si")
										$perdida=true;
									
									$detalleMicciones->perdida_orina=$perdida;
									$detalleMicciones->save();
			//						echo "detalle guardado\n";
								}catch(Exception $e){
									echo "error:",($e->getMessage());
								}
							}
						}catch(Exception $e){
							echo "error2:",($e->getMessage());
						}
						$fecha=null;
						$hora_acostarse=null;
						$detalles=array();

					}
					
					if($agregar_detalle)
					{
				//		echo "leyendo detalles\n";
						$sheet->getStyleByColumnAndRow(0,$row->getRowIndex())
						->getNumberFormat()
						->setFormatCode(
							PHPExcel_Style_NumberFormat::FORMAT_DATE_TIME4
						);
						$hr=$sheet->getCellByColumnAndRow(0,$row->getRowIndex())->getValue();
				/*		var_dump($rut_paciente);
						var_dump($hr);
						var_dump($fecha_detalle);
					*/		
						if(is_numeric($hr))
						{
							
							$hr=$sheet->getCellByColumnAndRow(0,$row->getRowIndex())->getFormattedValue();
							
							$h=preg_split("/\D/",$hr);
							if($h[0]<12&&!$dia_sumado)
							{
								$fecha_detalle->add($un_dia);
								$dia_sumado=true;
							}
							$fecha_detalle->setTime($h[0],$h[1],$h[2]);
							//		echo "horario:".$fecha_detalle->format("Y-m-d H:i:s")."\n<br>";
							$det=array("horario"=>$fecha_detalle->format("Y-m-d H:i:s"),
										"volumen"=>$sheet->getCellByColumnAndRow(1,$row->getRowIndex())->getFormattedValue(),
										"perdida"=>$sheet->getCellByColumnAndRow(2,$row->getRowIndex())->getValue());
							$detalles[]=$det;
						}
				
						
					}

					if($sheet->getCellByColumnAndRow(0,$row->getRowIndex())->getValue()==="Hora de Acostarse")
					{
				//		echo "leyendo Hora de Acostarse\n";
						$hora_acostarse=$sheet->getCellByColumnAndRow(1,$row->getRowIndex())->getFormattedValue();
						if($hora_acostarse==""||$hora_acostarse=="-")
							$hora_acostarse="20:00:00";
					}
					
					
					if($sheet->getCellByColumnAndRow(0,$row->getRowIndex())->getValue()==="Horario")
					{
						
		//				echo "Tiene detalle\n";
						$agregar_detalle=true;
					}
				}
				
			}
			if(!$fecha)
				continue;
			//se repite para guardar el último registro leído
			//echo "guardando  registro final\n";
			$fecha_leida=false;
			$agregar_detalle=false;
			
			$cartillaMicciones=new CartillaMicciones;
			$cartillaMicciones->fecha_cartilla=$fecha;
			$cartillaMicciones->hora_acostarse=$hora_acostarse;
			$cartillaMicciones->rut_paciente=$rut_paciente;
			$cartillaMicciones->save();
	
			foreach($detalles as $detalle)
			{
				$detalleMicciones=new DetalleCartillaMicciones;
				$detalleMicciones->id_cartilla_micciones=$cartillaMicciones->id_cartilla_micciones;
				$detalleMicciones->horario=empty($detalle["horario"])?null:$detalle["horario"];
				$detalleMicciones->volumen=$detalle["volumen"];
				$perdida=null;
				if($detalle["perdida"]=="No")
					$perdida=false;
				if($detalle["perdida"]=="Si")
					$perdida=true;
				
				$detalleMicciones->perdida_orina=$perdida;
				$detalleMicciones->save();
			}
			$fecha=null;
			$hora_acostarse=null;
			$detalles=array();
			
		}
		unlink($rutaArchivos);
    }


        public static function recibir2() {
        	
        $rut = Input::get("rut_paciente");
        
        $Espaciente=Paciente::find($rut);
        if($Espaciente==null){
        	echo "Error rut invalido";
        	return;}
        

        $retorno = false;

        if( count($_FILES) == 0 ) return Response::json( $retorno );
        try {
            $carpetaArchivos = "/public/detecciones/";
            foreach( $_FILES as $archivo ){
                $rutaArchivos = base_Path().$carpetaArchivos.$archivo['name'];

                rename( $archivo['tmp_name'], $rutaArchivos );
                chmod($rutaArchivos, 0777);
                //$data[] = $rutaArchivos;
                
                $name = explode(".", $archivo['name']);
                $id = $name[0];
                $type = $name[1];

                if($type=="txt"||$type=="TXT")
                {	
                	self::leerSensor($rutaArchivos,$id,$rut);
                }

                fclose($fp);
            }
        } catch (Exception $e) { $retorno = $e; }

        return Response::json( true );
    }

     private static function leerSensor($rutaArchivos,$nombreArchivo,$rut)
    {
    	set_time_limit(4800);
    	if(strlen($nombreArchivo)>5)
		{
			$dia = substr($nombreArchivo,2,2); 
			$mes = substr($nombreArchivo,4,2);
		}
		else
		{
			$dia = substr($nombreArchivo,0,2); 
			$mes = substr($nombreArchivo,2,2);	
		}
    	$year=date("Y");
    	$buffer=array();

    	$fp = fopen($rutaArchivos, "r");
    	
		$contador=0;
		$limite=2000;
		
		while(($linea = fgets($fp)) !== FALSE){
		
			$buffer[]=$linea;
			
			$contador++;
			if($contador==$limite)
			{
				
				$contador=0;
				
				try{
					self::procesarBuffer($buffer,$dia,$mes,$year,$rut);
				}catch (Exception $e) { return $e->getMessage().", línea:".$e->getLine().", archivo:".$e->getFile();}
				unset($buffer);
				$buffer=array();
				
			}
		}
		
		try{
			self::procesarBuffer($buffer,$dia,$mes,$year,$rut);
		}catch (Exception $e) { return $e->getMessage().", línea:".$e->getLine().", archivo:".$e->getFile();}
		unset($buffer);
	
		
		fclose($fp);
		
		unlink($rutaArchivos);
    }
    private static function procesarBuffer($buffer,$d,$m,$y,$rut)
    {
    	
    	$datos=array_unique($buffer);
    	    	
		foreach ($datos as $data) {

			
			$linea = explode(";", $data);
			
			$fecha=$y."-".$m."-".$d." ".$linea[1];
			
			$puerta1 = substr($linea[0],0,1); 
			if($puerta1==0)$puerta1='abierta'; 
			if($puerta1==1)$puerta1='cerrada'; 
			$persona1  = substr($linea[0],1,1);
			if($persona1==0)$persona1='nadie'; 
			if($persona1==1)$persona1='objetivo1'; 
			if($persona1==2)$persona1='no objetivo'; 
			if($persona1==3)$persona1='objetivo2'; 

			$puerta2 = substr($linea[0],2,1); 
			if($puerta2==0)$puerta2='abierta'; 
			if($puerta2==1)$puerta2='cerrada'; 
			$persona2  = substr($linea[0],3,1); 
			if($persona2==0)$persona2='nadie'; 
			if($persona2==1)$persona2='objetivo1'; 
			if($persona2==2)$persona2='no objetivo'; 
			if($persona2==3)$persona2='objetivo2'; 

			$puerta3 = substr($linea[0],4,1); 
			if($puerta3==0)$puerta3='abierta'; 
			if($puerta3==1)$puerta3='cerrada'; 
			$persona3  = substr($linea[0],5,1); 
			if($persona3==0)$persona3='nadie'; 
			if($persona3==1)$persona3='objetivo1'; 
			if($persona3==2)$persona3='no objetivo'; 
			if($persona3==3)$persona3='objetivo2'; 


			$ingresarDeteccion=new DeteccionPuertas();
			$ingresarDeteccion->rut_paciente=$rut;
			$ingresarDeteccion->fecha_detalle_eventos_repetidos=$fecha;
			$ingresarDeteccion->e1=$puerta1;
			$ingresarDeteccion->e1_persona=$persona1;
			$ingresarDeteccion->e2=$puerta2;
			$ingresarDeteccion->e2_persona=$persona2;
			$ingresarDeteccion->e3=$puerta3;
			$ingresarDeteccion->e3_persona=$persona3;
		//	$ingresarDeteccion->numero_eventos=$contador;
			$ingresarDeteccion->save();
			

		}
    }


}
