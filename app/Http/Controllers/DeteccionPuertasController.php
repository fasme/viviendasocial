<?php

class DeteccionPuertasController extends BaseController{

	
	public static function analizarDatos()
	{
		set_time_limit(7200);
		ini_set('memory_limit','300M');
		$rut=Input::get("rut");
		//DB::table('resumen_acciones_repetidas')->truncate();
		$resultado_pacientes=DB::select(
			DB::raw(
				"
				SELECT 
				rut,
				fecha_inicio_estudio,
				objetivo
				FROM paciente
				WHERE paciente_visible=TRUE
				AND id_evento=2
				AND rut=$rut
				"
				)
			);
		$un_dia=new DateInterval('P1D');
		$treinta_dias=new DateInterval('P30D');

		foreach($resultado_pacientes as $paciente)
		{

			$date_fecha_inicial1 = new DateTime($paciente->fecha_inicio_estudio);
			$date_fecha_inicial = new DateTime($paciente->fecha_inicio_estudio);
			
			$date_fecha_final = $date_fecha_inicial1->add($treinta_dias);
			$date_fecha_siguiente=new DateTime($date_fecha_inicial->format('Y-m-d'));
			$contador=0;
			$rut=$paciente->rut;

			$objetivo="";
			switch($paciente->objetivo)
			{
				case 1:
					$objetivo="objetivo1";
					break;
				case 2:
					$objetivo="objetivo2";
					break;
				default:
					$objetivo="";
					break;
			}
			if($rut==2927208||
				$rut==3674973||
				$rut==5039885||
				$rut==5133999)
			$objetivo="nadie";
			

			
			while($date_fecha_final->getTimestamp()>=$date_fecha_siguiente->getTimestamp())
			{			

				DB::statement(
					DB::raw(
						"
						SET TIME ZONE  0;
						"
					)
				);
				
				$resultado=DB::select(
				DB::raw(
					"
					SELECT 
					e1,
					e2,
					e3,
					e1_persona,
					e2_persona,
					e3_persona,
					fecha_detalle_eventos_repetidos AS fecha
					FROM detalle_eventos_repetidos
					WHERE rut_paciente=$rut
					AND fecha_detalle_eventos_repetidos BETWEEN '".$date_fecha_siguiente->format('Y-m-d')."' AND '".$date_fecha_siguiente->format('Y-m-d')." 23:59:59'
					ORDER BY fecha_detalle_eventos_repetidos, id_detalle_eventos_repetidos ASC
					"
					)
				);
	
	
				try{
					self::procesarDatos($resultado,$objetivo,$rut);
				}catch(Exception $e){
					return Response::json($e->getMessage().", linea:".$e->getLine().", archivo:".$e->getFile());
				}
	
		
				unset($resultado);
				$contador++;
				$date_fecha_siguiente->add($un_dia);

			}

			unset($date_fecha_inicial1);
			unset($date_fecha_inicial);
			unset($date_fecha_final);
			unset($date_fecha_siguiente);
			unset($contador);
			unset($rut);
			unset($objetivo);
		
		}
		
		unset($resultado_pacientes);

		return Response::json("listo");
	
	}
	private static function buscarObjetivo($array,$objetivo,$sensor,$indice_actual)
	{
		if($objetivo=="nadie")
			return true;
		$nombre_sensor="e{$sensor}_persona";
		$inicio=$indice_actual-15;
		$fin=$indice_actual+15;
		
		if($inicio<0)
			$inicio=0;
		if($fin>count($array))
			$fin=count($array);
		for($i=$inicio;$i<$fin;$i++)
		{
			if($array[$i]->$nombre_sensor==$objetivo)
			{
				unset($array);
				return true;
			}
		}
		unset($array);
		return false;
	}
	private static function procesarDatos($r,$objetivo,$rut)
	{
		
		$largo=count($r);

		$datos=array(
			"sensor1"=>array(
				"cantidad"=>0,
				"tiempo"=>array()
				),
			"sensor2"=>array(
				"cantidad"=>0,
				"tiempo"=>array()
				),
			"sensor3"=>array(
				"cantidad"=>0,
				"tiempo"=>array()					
				)
			);
		$rango=50;

		if($largo>=$rango)
		{
			$estado_puerta=$r[0]->e1;
			$objetivo_actual=$r[0]->e1_persona;
			$contador_estado_puerta=0;
			$indice_inicial=0;
			$indice_final=$rango;
			$diferentes=false;
			$segundo_anterior=-1;

			while($indice_final<=$largo)
			{
				for($i=$indice_inicial;$i<$indice_final;$i++)
				{
					
					$estado_puerta_actual=$r[$i]->e1;
	
					if($estado_puerta_actual!=$estado_puerta)
					{
						$diferentes=true;
						break;
					}
					
				}
				if($diferentes)
				{

					$estado_anterior=$estado_puerta;
					
					for($i=$indice_inicial;$i<$indice_final;$i++)
					{
						$registro=$r[$i];
	
						if($estado_anterior==$registro->e1)
						{
	
							$segundo_anterior=$registro->fecha;
							$estado_anterior=$registro->e1;
							continue;
						}
						if($segundo_anterior!=$registro->fecha)
						{
							if($estado_anterior=="cerrada")
							{

								if($registro->e1_persona!=$objetivo)
								{
	
									if(self::buscarObjetivo($r,$objetivo,1,$i))
									{
										$objetivo_actual=$objetivo;
									}
									else
									{
	
										$objetivo_actual=$registro->e1_persona;
										$estado_anterior=$estado_puerta;
										$segundo_anterior=$registro->fecha;
										continue;
									}
								}
	
								$datos["sensor1"]["cantidad"]++;
								$datos["sensor1"]["tiempo"][]=($registro->fecha);
								$estado_puerta="abierta";
								
							}
							else
							{
								$estado_puerta="cerrada";
								
								
							}

							$estado_anterior=$estado_puerta;
							$segundo_anterior=$registro->fecha;
							
						}
					}
				}
				$indice_inicial=$indice_final;
				$indice_final+=$rango;
			}
			//-------------------------SENSOR 2
			$estado_puerta=$r[0]->e2;
			$objetivo_actual=$r[0]->e2_persona;
			$contador_estado_puerta=0;
			$indice_inicial=0;
			$indice_final=$rango;
			$diferentes=false;
			$segundo_anterior=-1;

			while($indice_final<=$largo)
			{
				for($i=$indice_inicial;$i<$indice_final;$i++)
				{
					
					$estado_puerta_actual=$r[$i]->e2;

					if($estado_puerta_actual!=$estado_puerta)
					{
						$diferentes=true;
						break;
					}
					
				}
				if($diferentes)
				{

					$estado_anterior=$estado_puerta;
					
					for($i=$indice_inicial;$i<$indice_final;$i++)
					{
						$registro=$r[$i];
	
						if($estado_anterior==$registro->e2)
						{
	
							$segundo_anterior=$registro->fecha;
							$estado_anterior=$registro->e2;
							continue;
						}
						if($segundo_anterior!=$registro->fecha)
						{
							if($estado_anterior=="cerrada")
							{
	
								if($registro->e2_persona!=$objetivo)
								{
	
									if(self::buscarObjetivo($r,$objetivo,2,$i))
									{
										$objetivo_actual=$objetivo;
									}
									else
									{
		
										$objetivo_actual=$registro->e2_persona;
										$estado_anterior=$estado_puerta;
										$segundo_anterior=$registro->fecha;
										continue;
									}
								}
	
								$datos["sensor2"]["cantidad"]++;
								$datos["sensor2"]["tiempo"][]=($registro->fecha);
								$estado_puerta="abierta";
								
								
							}
							else
							{
								$estado_puerta="cerrada";
								
								
							}

							$estado_anterior=$estado_puerta;
							$segundo_anterior=$registro->fecha;
							
						}
					}
				}
				$indice_inicial=$indice_final;
				$indice_final+=$rango;
			}
			
			//-------------------------SENSOR 3
			$estado_puerta=$r[0]->e3;
			$objetivo_actual=$r[0]->e3_persona;
			$contador_estado_puerta=0;
			$indice_inicial=0;
			$indice_final=$rango;
			$diferentes=false;
			$segundo_anterior=-1;

			while($indice_final<=$largo)
			{
				for($i=$indice_inicial;$i<$indice_final;$i++)
				{
					
					$estado_puerta_actual=$r[$i]->e3;

					if($estado_puerta_actual!=$estado_puerta)
					{
						$diferentes=true;
						break;
					}
					
				}
				if($diferentes)
				{

					$estado_anterior=$estado_puerta;
					
					for($i=$indice_inicial;$i<$indice_final;$i++)
					{
						$registro=$r[$i];

						if($estado_anterior==$registro->e3)
						{
		
							$segundo_anterior=$registro->fecha;
							$estado_anterior=$registro->e3;
							continue;
						}
						if($segundo_anterior!=$registro->fecha)
						{
							if($estado_anterior=="cerrada")
							{

								if($registro->e3_persona!=$objetivo)
								{
	
									if(self::buscarObjetivo($r,$objetivo,3,$i))
									{
										$objetivo_actual=$objetivo;
									}
									else
									{

										$objetivo_actual=$registro->e3_persona;
										$estado_anterior=$estado_puerta;
										$segundo_anterior=$registro->fecha;
										continue;
									}
								}
	
								$datos["sensor3"]["cantidad"]++;
								$datos["sensor3"]["tiempo"][]=($registro->fecha);
								$estado_puerta="abierta";
								
								
							}
							else
							{
								$estado_puerta="cerrada";
								
								
							}

							$estado_anterior=$estado_puerta;
							$segundo_anterior=$registro->fecha;
							
						}
					}
				}
				$indice_inicial=$indice_final;
				$indice_final+=$rango;
			}
		}
	//	$datos["sensor1"]["tiempo"][]=[(int)$r[count($r)-1]->segundos_fecha,0];

		array_multisort($datos["sensor1"]["tiempo"]);
		
		
		
		array_multisort($datos["sensor2"]["tiempo"]);
		
		
		
		array_multisort($datos["sensor3"]["tiempo"]);
		

		for($i=0;$i<count($datos["sensor1"]["tiempo"]);$i++)
		{
			$resumen=new ResumenAccionesRepetidas;
			$resumen->rut_paciente=$rut;
			$resumen->fecha_apertura=$datos["sensor1"]["tiempo"][$i];
			$resumen->identificador_sensor_puerta="e1";
			$resumen->save();
			unset($resumen);
		}
		for($i=0;$i<count($datos["sensor2"]["tiempo"]);$i++)
		{
			$resumen=new ResumenAccionesRepetidas;
			$resumen->rut_paciente=$rut;
			$resumen->fecha_apertura=$datos["sensor2"]["tiempo"][$i];
			$resumen->identificador_sensor_puerta="e2";
			$resumen->save();
			unset($resumen);
		}
		for($i=0;$i<count($datos["sensor3"]["tiempo"]);$i++)
		{
			$resumen=new ResumenAccionesRepetidas;
			$resumen->rut_paciente=$rut;
			$resumen->fecha_apertura=$datos["sensor3"]["tiempo"][$i];
			$resumen->identificador_sensor_puerta="e3";
			$resumen->save();
			unset($resumen);
		}

		unset($estado_puerta);
		unset($objetivo_actual);
		unset($contador_estado_puerta);
		unset($indice_inicial);
		unset($indice_final);
		unset($diferentes);
		unset($segundo_anterior);
		unset($r);
		
		unset($datos);

	}

}
?>
