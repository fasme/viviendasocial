<?php

namespace App\Http\Controllers;

use App\Model;
use App\Models\Usuario;
use App\Models\Noticia;
use App\Models\NoticiaLabitec;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Routing\Controller;
use Session;
use Funciones;
use DB;
use Response;
use Illuminate\Support\Facades\Input;
use Redirect;

class SesionController extends BaseController {

	public function doLogin(Request $request){
		//return  $request->input('remember-me');
		$rut=Funciones::soloRut(trim($request->input('rut')));
		$password=trim($request->input('password'));
	
		if ($request->input('remember-me') == '' || $request->input('remember-me') != "on" ) {
			$remember="off";
		}else{
			$remember= $request->input('remember-me');
		}
		//$remember=request::has("remember-me");

		$userData=["rut" => $rut, "password" => $password];

		$visible = DB::table("usuario")
					->where("rut", $rut)
					->first()->visible;
		
		if(Auth::attempt($userData, $remember) && $visible){
			$user = Auth::user();
			Session::put("password", $password);

			$response=["href" => "index"];
		}
		else $response=["error" => "Usuario y/o contrase? inv?ida"];
		//return Response::json($response);
		return response()->json($response);
	}

	public function cerrarSesion(){
		Auth::logout();
		Session::flush();
		return Redirect::to('/');
	}

	/* correo que se envia en el formulario de contacto del login */
	public function enviarCorreoContacto(){
		$nombres=trim(Input::get("nombreContacto"));
		$correo=trim(Input::get("correoContacto"));
		$comentariocorreo=trim(Input::get("comentarioContacto"));
		$asuntocorreo = (Input::has("asuntoContacto")) ? trim(Input::get("asuntoContacto")) : null;

		$comentario = ($asuntocorreo != null) ? $asuntocorreo."<br><br>".$comentariocorreo : $comentariocorreo;		

		$data = array( 
			"nombre" => $nombres, 
			"correo" => $correo,			
			"texto" => $comentario
			);
		$asunto= "Contacto eHomeseniors";
		$destinatario = "soporte";
		$correos = array($correo, "soporte.raveno@uv.cl");
		Mail::send('Sesion.Correos', $data, function($message) use ($correos, $destinatario, $asunto)
		{
		  $message->to($correos, $destinatario)
		          ->subject($asunto);
		});
		$response=[];
		return Response::json(array("exito" => "El correo ha sido enviado"));
	}

/* Carga todas las noticias*/
	public function cargarTodasNoticias(){
		$response=[];
		$labitec ="labitec";
		$ehome   = "ehome";
		//$datos        = Noticia::orderBy("fecha_publicacion_noticia", "desc")->get();
		$datosLabitec = NoticiaLabitec::orderBy("fecha","desc")
									->where("etiqueta","=","viviendas")
									->where("visible","=","true")
									->get();
		
		foreach ($datosLabitec as $datoLabitec){
			$dia = date("d", strtotime($datoLabitec->fecha));
			$mes = Funciones::nombreMes(date("m", strtotime($datoLabitec->fecha)));
			$ano = date("Y", strtotime($datoLabitec->fecha));
			//list($width, $height) = getimagesize('images/noticias/'.$datoLabitec->imagen_noticia);
			$responseLabitec[]= array(
				"id_noticia"=> $datoLabitec->id_noticias,
				"titulo_noticia"=> $datoLabitec->titulo,
		       	"texto_noticia"=> $datoLabitec->cuerpo,
		        "resumen_noticia"=> $datoLabitec->lead,
			    "imagen_noticia"=> $datoLabitec->foto,
			    "origen" => $labitec,
			    "fechaOrden" => $datoLabitec->fecha,
			    "fecha"=> $dia ." de ". $mes ." de ". $ano
				//"img_alto" => $height,
				//"img_ancho" => $width
			);
		}
		/*foreach($datos as $dato){
			$dia = date("d", strtotime($dato->fecha_publicacion_noticia));
			$mes = Funciones::nombreMes(date("m", strtotime($dato->fecha_publicacion_noticia)));
			$ano = date("Y", strtotime($dato->fecha_publicacion_noticia));
			
			//list($width, $height) = getimagesize('images/noticias/'.$dato->imagen_noticia);
			$response[]= array(
				"id_noticia"=> $dato->id_noticia,
				"rut_encargado"=> $dato->rut_encargado,
				"titulo_noticia"=> $dato->titulo_noticia,
		       	"texto_noticia"=> $dato->texto_noticia,
		        "resumen_noticia"=> $dato->resumen_noticia,
			    "imagen_noticia"=> $dato->imagen_noticia,
			    "origen" => $ehome,
			    "fechaOrden" => $dato->fecha_publicacion_noticia,
			    "fecha"=> $dia ." de ". $mes ." de ". $ano

			    //"img_alto" => $height,

			    //"img_ancho" => $width

			);
		}*/
		$response = array($responseLabitec);
		return Response::json($response);
	}

/* busca una en particular y carga las distitas a ella*/
	public function cargarNoticia(){
		$response=[];
		$responseUno=[];
		$responseLabitec=[];
		$datosUno =[];
		$datosDos  =[];

		$ehome   = "ehome";
		$labitec ="labitec";

		$datosUno = Noticia::orderBy("fecha_publicacion_noticia", "desc")->where("id_noticia", "=", Input::get("idNoticia"))->first();

		$datosDos = Noticia::orderBy("fecha_publicacion_noticia", "desc")->where("id_noticia", "!=", Input::get("idNoticia"))->get();


		$dia = date("d", strtotime($datosUno->fecha_publicacion_noticia));
		$mes = Funciones::nombreMes(date("m", strtotime($datosUno->fecha_publicacion_noticia)));
		$ano = date("Y", strtotime($datosUno->fecha_publicacion_noticia));
		$responseUno = array(
			"id_noticia"=> $datosUno->id_noticia,
			"rut_encargado"=> $datosUno->rut_encargado,
			"titulo_noticia"=> $datosUno->titulo_noticia,
			"texto_noticia"=> $datosUno->texto_noticia,
			"resumen_noticia"=> $datosUno->resumen_noticia,
			"imagen_noticia"=> $datosUno->imagen_noticia,
			"origen" => $ehome,
			"fechaOrden" => $datosUno->fecha_publicacion_noticia,
			"fecha"=> $dia ." de ". $mes ." de ". $ano
		);


		foreach($datosDos as $dato){
			$dia = date("d", strtotime($dato->fecha_publicacion_noticia));
			$mes = Funciones::nombreMes(date("m", strtotime($dato->fecha_publicacion_noticia)));
			$ano = date("Y", strtotime($dato->fecha_publicacion_noticia));
			$response[]= array(
				"id_noticia"=> $dato->id_noticia,
				"rut_encargado"=> $dato->rut_encargado,
				"titulo_noticia"=> $dato->titulo_noticia,
		       	"texto_noticia"=> $dato->texto_noticia,
		        "resumen_noticia"=> $dato->resumen_noticia,
			    "imagen_noticia"=> $dato->imagen_noticia,
			    "origen" => $ehome,
			    "fechaOrden" => $dato->fecha_publicacion_noticia,
			    "fecha"=> $dia ." de ". $mes ." de ". $ano
			);
		}
		//Rescatar todas las noticas de labitec
		$datosLabitec = NoticiaLabitec::orderBy("fecha","desc")
									->where("etiqueta","=","ehome")
									->where("visible","=","true")
									->get();
		foreach ($datosLabitec as $datoLabitec){
			$dia = date("d", strtotime($datoLabitec->fecha));
			$mes = Funciones::nombreMes(date("m", strtotime($datoLabitec->fecha)));
			$ano = date("Y", strtotime($datoLabitec->fecha));
			//list($width, $height) = getimagesize('images/noticias/'.$datoLabitec->imagen_noticia);
			$responseLabitec[]= array(
				"id_noticia"=> $datoLabitec->id_noticias,
				"titulo_noticia"=> $datoLabitec->titulo,
		       	"texto_noticia"=> $datoLabitec->cuerpo,
		        "resumen_noticia"=> $datoLabitec->lead,
			    "imagen_noticia"=> $datoLabitec->foto,
			    "origen" => $labitec,
			    "fechaOrden" => $datoLabitec->fecha,
			    "fecha"=> $dia ." de ". $mes ." de ". $ano
			//    "img_alto" => $height,
			//    "img_ancho" => $width
			);
		}

		$response = array_merge($response,$responseLabitec);
		return Response::json(array(
			"lanoticia" => $responseUno, 
			"noticias" => $response));
	}

	public function cargarNoticiaLabitec(){
		$response=[];
		$responseUno=[];
		$responseEhome=[];
		$datosUno =[];
		$datosDos  =[];

		$labitec ="labitec";
		$ehome   = "ehome";
		
		$datosUno = NoticiaLabitec::orderBy("fecha", "desc")->where("id_noticias", "=", Input::get("idNoticia"))->first();

		$datosDos = NoticiaLabitec::orderBy("fecha","desc")
									->where("etiqueta","=","viviendas")
									->where("visible","=","true")
									->where("id_noticias","!=",Input::get("idNoticia"))
									->get();


		$dia = date("d", strtotime($datosUno->fecha));
		$mes = Funciones::nombreMes(date("m", strtotime($datosUno->fecha)));
		$ano = date("Y", strtotime($datosUno->fecha));
		$responseUno = array(
			"id_noticia"=> $datosUno->id_noticias,
			"titulo_noticia"=> $datosUno->titulo,
		    "texto_noticia"=> $datosUno->cuerpo,
		    "resumen_noticia"=> $datosUno->lead,
			"imagen_noticia"=> $datosUno->foto,
			"origen" => $labitec,
			"fechaOrden" => $datosUno->fecha,
			"fecha"=> $dia ." de ". $mes ." de ". $ano
		);


		foreach($datosDos as $dato){
			$dia = date("d", strtotime($dato->fecha));
			$mes = Funciones::nombreMes(date("m", strtotime($dato->fecha)));
			$ano = date("Y", strtotime($dato->fecha));
			$response[]= array(
				"id_noticia"=> $dato->id_noticias,
				"titulo_noticia"=> $dato->titulo,
		    	"texto_noticia"=> $dato->cuerpo,
		    	"resumen_noticia"=> $dato->lead,
				"imagen_noticia"=> $dato->foto,
				"origen" => $labitec,
				"fechaOrden" => $dato->fecha,
				"fecha"=> $dia ." de ". $mes ." de ". $ano
			);
		}
		//rescatar todas las noticias de ehome
		//$datosEhome = Noticia::orderBy("fecha_publicacion_noticia", "desc")->get();

		/*foreach($datosEhome as $datoEhome){
			$dia = date("d", strtotime($datoEhome->fecha_publicacion_noticia));
			$mes = Funciones::nombreMes(date("m", strtotime($datoEhome->fecha_publicacion_noticia)));
			$ano = date("Y", strtotime($datoEhome->fecha_publicacion_noticia));
			//list($width, $height) = getimagesize('images/noticias/'.$datoEhome->imagen_noticia);
			$responseEhome[]= array(
				"id_noticia"=> $datoEhome->id_noticia,
				"rut_encargado"=> $datoEhome->rut_encargado,
				"titulo_noticia"=> $datoEhome->titulo_noticia,
		       	"texto_noticia"=> $datoEhome->texto_noticia,
		        "resumen_noticia"=> $datoEhome->resumen_noticia,
			    "imagen_noticia"=> $datoEhome->imagen_noticia,
			    "origen" => $ehome,
			    "fechaOrden" => $datoEhome->fecha_publicacion_noticia,
			    "fecha"=> $dia ." de ". $mes ." de ". $ano
			//    "img_alto" => $height,
			//    "img_ancho" => $width
			);
		}*/

		$response = array_merge($response);
		return Response::json(array(
			"lanoticia" => $responseUno, 
			"noticias" => $response));
	}

}
