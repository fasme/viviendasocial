<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Contacto;
use App\Models\Agenda;

use Auth;
use DB;
use Response;


class ContactoController extends BaseController{

	//se usa
	public function obtenerContactoPaciente(Request $request){ 
		$rut=$request->input("rut");
		$datos= Contacto::obtenerContactoPaciente($rut);
		$response=[];
		foreach($datos as $dato){
			$response[]=[
			$dato["prioridad"]." <span class='glyphicon glyphicon-menu-down'></span><span class='glyphicon glyphicon-menu-up'></span>",
			$dato["nombre"]." ".$dato["apellidos"],
			//$dato["rut"]."-".$dato["dv"],
			$dato["relacion"],
			$dato["telefono"],
			'<button onclick="modificarContacto(\''.$dato["rut"].'\', \''. Auth::user()->rut .'\' )" class="btn btn-primary" title="Modificar paciente" ><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></button>'
			.' <button onclick="eliminarContacto(\''.$dato["rut"].'\')" class="btn btn-primary" title="Eliminar paciente" ><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>'
			//$dato["correo"],
			//$dato["tipo_usuario"],
			//"enviar correo"
			];
		}
		return Response::json($response);
	}

	//se usa
	public function agregarContacto(Request $request){ // guarda los contactos de un paciente
		//antes
		//$rutContacto = $rutPaciente
		//$rutPaciente es ahora rutContacto
		
		try{
			DB::beginTransaction();

			$rutContacto = explode("-", $request->input("agregarPacienteRut"));			
			$idTipoUsuario = $request->input("idTipoUsuario");
			$parentesco = $request->input("parentesco");
			
			$agenda = Agenda::select('id_agenda')->where('rut_usuario',$rutContacto[0])->get();						
			
			//elimino la agenda anterior
			foreach ($agenda as $id) {
				Agenda::destroy($id->id_agenda);
			}

			//ingreso nuevos numeros
			foreach ($request->telefono as $key => $telefono) {
				$nuevaAgenda = new Agenda;
				$nuevaAgenda->rut_usuario = $rutContacto[0];
				$nuevaAgenda->telefono = $telefono;
				$nuevaAgenda->fecha_insertado = date("d-m-Y H:i:s");
				$nuevaAgenda->tipo_telefono = $request->tipo[$key];
				$nuevaAgenda->save();
			}
			
			$rutPaciente = $request->input("rutPaciente");
			//$lista = ($request->input("listaPacientes")) ? $request->input("listaPacientes") : array();
			
			$eso = Contacto::detenerRelacionContactoPaciente($rutPaciente, $rutContacto[0], $idTipoUsuario);
			//return $eso;
			$prioridad = Contacto::obtenerUltimaPrioridadPaciente($rutPaciente)+1;


			$contacto = new Contacto;
			$contacto->inicio = date("d-m-Y H:i:s");
			$contacto->rut_paciente = $rutPaciente;
			$contacto->rut_encargado = $rutContacto[0];
			$contacto->id_relacion_familiar = $parentesco;
			$contacto->prioridad = $prioridad;
			$contacto->save();					
			
			
			DB::commit();
			return response()->json(["exito" => "El contacto ha sido editado"]);
		}catch(Exception $ex){
			DB::rollback();
			return response()->json(["error" => "Error al registrar el contacto", "msg" => $ex->getMessage()]);
		}
	}

	public function asociarPacienteAUsuario(){ // guarda los pacientes de un contacto
		try{
			DB::beginTransaction();
			$rutUsuario=Input::get("rutUsuario_hdn");
			if(Input::has("listaPacientes")){
				$lista = Input::get("listaPacientes");

				Contacto::detenerRelacionContactoMedico($lista, $rutUsuario);

				foreach($lista as $rutPaciente){		
					$contacto = Contacto::where("rut_encargado", "=", $rutUsuario)
					->where("rut_paciente", "=", $rutPaciente)
					->orderBy("inicio","desc")
					->first();

					if($contacto == null || $contacto->fin != null) {		
						$contacto=new Contacto;
						$contacto->inicio = date("d-m-Y H:i:s");
					}
					$contacto->rut_paciente=$rutPaciente;
					$contacto->rut_encargado=$rutUsuario;

					$contacto->save();
					
				}		
			}
			DB::commit();
			return Response::json(["exito" => "La lista ha sido actualizada"]);
		}catch(Exception $ex){
			DB::rollback();
			return Response::json(["error" => "Error al registrar el contacto", "msg" => $ex->getMessage()]);
		}
	}

	

}