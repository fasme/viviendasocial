<?php

namespace App\Http\Controllers;

use App\Models\Paciente;
use App\Models\Usuario;
use App\Models\Domicilio;
use App\Models\DetallePaciente;
use App\Models\TestGetUp;
use App\Models\SindromesGeriatricos;
use App\Models\IndiceBarthel;
use App\Models\TestLawtonBrody;
use App\Models\TestTinetiEquilibrio;
use App\Models\TestTinetiMarcha;
use App\Models\TestCharlson;
use App\Models\TestIciq;
use App\Models\TestEvaluacionCognitiva;
use App\Models\GDS;
use App\Models\TestMmse;
use App\Models\TestNpi;
use App\Models\InformeAccionesRepetidas;
use App\Models\TestWhoqol;

use DB;
use Illuminate\Http\Request;


class PacienteController extends Controller{

	
	//se usa
	public function modificarContacto(Request $request){

		$contacto = DB::table('contacto')
					->join("usuario", "usuario.rut", "=", "contacto.rut_encargado")
					->where("contacto.rut_encargado", "=", $request->rutContacto)
					->where("usuario.visible", "=", true)
					->whereNull("contacto.fin")
					->select("usuario.nombres", "usuario.apellido_paterno", "usuario.apellido_materno", "usuario.telefono", "usuario.correo", "usuario.rut", "usuario.id_tipo_usuario", "contacto.prioridad")->get();

		return $contacto;
	}

	//se usa
	public function eliminarContacto(Request $request){


		return "hola";
	}

	//se usa
	public function obtenerPacientes(){
		
		$filtro = 0 ;

		return response()->json(["exito" => "Los datos del paciente han sido actualizado", "datos" => Paciente::obtenerPacientes($filtro)]);
		
		//return Response::json(Paciente::obtenerPacientes($filtro));


		//return Response::json(Paciente::obtenerPacientes($filtro));
	}

	public function conclusion(){
		$rut_paciente = Input::get("paciente");
		$usuario = Input::get("usuario");
		$Re_acciones_repetidas = Input::get("valor");

		$acciones_repetidas = InformeAccionesRepetidas::where("rut_paciente", "=", $rut_paciente)
		->where("rut_usuario","=",$usuario)
		->first();

        if($acciones_repetidas == null) 
            {	
               $acciones_repetidas = new InformeAccionesRepetidas;                
            }

        $acciones_repetidas->rut_usuario =$usuario;
        $acciones_repetidas->rut_paciente =$rut_paciente;
        $acciones_repetidas->tiene_acciones_repetidas=$Re_acciones_repetidas;
        $acciones_repetidas->fecha_informe = date("Y-m-d H:i:s");
        $acciones_repetidas->save();
        
        return Response::json(["exito"=>"Se ha guardado correctamente"]);

	}

	//se usa
	public function agregarPaciente(Request $request){
		try{
			DB::beginTransaction();

			$rut= $request->input("rut");
			$nombre= $request->input("nombre");
			$apellido_paterno= $request->input("apellido_p");
			$telefono= $request->input("telefono");
			$correo= $request->input("correo");
			$genero = $request->input("genero");
 			
 			$usuario = Usuario::find($rut);
			if($usuario == null){ 
				$usuario = new Usuario;	
			}					
		
			$usuario->rut = $rut;
		
			$usuario->nombres=ucwords(strtolower($nombre));

			$usuario->apellido_paterno=ucwords(strtolower($apellido_paterno));
	
			if($request->input("apellido_m")) $usuario->apellido_materno=ucwords(strtolower($request->input("apellido_m")));
	
			if($request->input("telefono")) $usuario->telefono= $request->input("telefono");
			if($request->input("correo")) $usuario->correo= $request->input("correo");
			if($request->input("password")) $usuario->password=Hash::make( $request->input("password"));

			$usuario->save();

			$paciente=Paciente::find($rut);
			if($paciente == null){
				$paciente=new Paciente;
				$paciente->fecha_ingreso = date("Y-m-d");
				$paciente->paciente_visible=true;
				$accion = "agregado";
			} else $accion = "editado";

			if($request->input("paciente-tipoAlerta")) $paciente->id_evento = $request->input("paciente-tipoAlerta");

			$paciente->rut=$rut;	

			if($request->input("comuna")) $paciente->id_comuna = $request->input("comuna");
			if($request->input("calle")) $paciente->calle = ucwords($request->input("calle"));
			if($request->input("numero")) $paciente->numero = $request->input("numero");
			
			$paciente->save();

			$detalle = DetallePaciente::find($rut);
			if($request->input("fechaNacimiento")) $detalle->fecha_nacimiento = $request->input("fechaNacimiento");
			$detalle->genero = $genero;

			$detalle->save();
	
			DB::commit();
			return response()->json(["exito" => "El Paciente ha sido $accion"]);
		}catch(Exception $ex){
			DB::rollback();
			return response()->json(["error" => "Error al registrar el paciente", "msg" => $ex->getMessage()]);
		}
	}

	public function obtenerNombreRutPacientes($query){
		$datos=Paciente::obtenerRutNombrePacientes($query);
		$response=[];
		foreach($datos as $dato){
			$response[]=["rut" => $dato["rut"],
			"nombre" => $dato["nombre"]];
		}
		return Response::json($response);
	}

	public function existePaciente(){
		$rut=Input::get("rutPaciente");
		$paciente = Paciente::obtenerDatosPaciente($rut);

		if($paciente == null) return Response::json(["resultado" => false]);
		return Response::json(["resultado" => true, "msj" => "El paciente ya existe."]);
	}
	

	public function obtenerAlertasCaidas(){ /* tabla estadisticas caidas */
		$response = [];
		$rut = Input::get("rut");
		$usuario = Auth::user();
		$alertas = AlertaCaida::obtenerAlertas($rut, $usuario->rut);

		foreach($alertas as $alerta){
			$response[]=[$alerta["linkicono"] ." ". $alerta["fecha"] ." ".$alerta["ubicacion"] ." [". $alerta["identificadorSensor"] ."] (validaciones: ".(($alerta["validacion"] ==null)?0:$alerta["validacion"]).")" ];
		}

		return Response::json(["aaData" => $response]);
	}

	public function obtenerAlertasPuertaNicturia(){
		$response = [];
		$rut = Input::get("rut");
		$evento = Input::get("evento");

		$alertas = Alerta::obtenerAlertaPuertaNicturia($rut, $evento);

		foreach($alertas as $alerta){
			$response[] = [$alerta[0], $alerta[1], "<a href='#' onclick='verDetalleAlerta($alerta[2], $evento);'>Ver detalle alerta</a>"];
		}

		return Response::json(["aaData" => $response]);
	}

	public function obtenerDetalleAlerta(){
		$response = [];
		$idAlerta = Input::get("idAlerta");
		$rutUsuario = Auth::user()->rut;

		$alertas = AlertaDetalle::obtenerDetalleAlerta($idAlerta, $rutUsuario);

		foreach($alertas as $alerta){
			$response[] = [$alerta["fecha"]];
		}

		return Response::json($response);
	}

	public function editarDatosPersonales(){
		try{

			DB::beginTransaction();

			$rut = Input::get("rut");
			$alergia = Input::get("alergia");
			$fuma = Input::get("fuma");
			$fumaN = Input::get("fumaN");
			$alcohol = Input::get("alcohol");
			$alcoholN = Input::get("alcoholN");
			$peso = Input::get("peso");
			$talla = Input::get("talla");
			$imc = Input::get("imc");
			$cintura = Input::get("cintura");
			$estatura = Input::get("estatura");
			$psistolica = Input::get("psistolica");
			$diastolica = Input::get("diastolica");
			$dialisis = Input::get("dialisis");
			$vivecon = Input::get("vivecon");

			$alergiaPaciente = AlergiaPaciente::where("rut_paciente", "=", $rut)->first();
			if($alergiaPaciente == null) {
				$alergiaPaciente = new AlergiaPaciente;
				$alergiaPaciente->rut_paciente = $rut;
			}
			$alergiaPaciente->alergia = $alergia;
			$alergiaPaciente->save();

			$historialHabitos = HistorialHabitos::obtenerHistorialActual($rut);
			if($historialHabitos != null){
				$historialHabitos->fin = date("Y-m-d H:i:s");	
				$historialHabitos->save();	
			}

			$nuevoHistorial = new HistorialHabitos;
			$nuevoHistorial->rut_paciente = $rut;
			$nuevoHistorial->fuma = ($fuma == 1) ? true : false;
			$nuevoHistorial->n_cigarros = ($fumaN == "") ? 0 : $fumaN;
			$nuevoHistorial->alcohol = ($alcohol == 1) ? true : false;
			$nuevoHistorial->n_vasos_alcohol = ($alcoholN == "") ? null : $alcoholN;
			$nuevoHistorial->vive_con = $vivecon;
			$nuevoHistorial->inicio = date("Y-m-d H:i:s");
			$nuevoHistorial->save();

			$historialDatos = HistorialDatosPaciente::obtenerHistorialActual($rut);
			if($historialDatos != null){
				$historialDatos->fin = date("Y-m-d H:i:s");	
				$historialDatos->save();	
			}

			$nuevoHistorial = new HistorialDatosPaciente;
			$nuevoHistorial->rut_paciente = $rut;
			$nuevoHistorial->peso = ($peso == "") ? null : $peso;
			$nuevoHistorial->talla = ($talla == "") ? null : $talla;
			$nuevoHistorial->imc = ($imc == "") ? null : $imc;
			$nuevoHistorial->cintura = ($cintura == "") ? null : $cintura;
			$nuevoHistorial->estatura = ($estatura == "") ? null : $estatura;
			$nuevoHistorial->p_sistolica = ($psistolica == "") ? null : $psistolica;
			$nuevoHistorial->p_diastolica = ($diastolica == "") ? null : $diastolica;
			$nuevoHistorial->inicio = date("Y-m-d H:i:s");
			$nuevoHistorial->save();

			DB::commit();

			return Response::json(["exito" => "Los antececentes personales han sido editados"]);
		} catch(Exception $ex){
			DB::rollback();
			return Response::json(["error" => "Error al editar los antecedentes personales", "msg" => $ex->getMessage()]);
		}



	}

	private function registrarPaciente($rut, $genero){
		$usuario = Usuario::find($rut);
		if($usuario == null){
			$usuario = new Usuario;
			$usuario->rut = $rut;
			$usuario->save();

			$paciente = new Paciente;
			$paciente->rut = $rut;
			$paciente->genero = $genero;
			$paciente->save();
		}
	}

	public function editarEncuestasTimedGetUp(){
		try{

			DB::beginTransaction();

			$rut = Input::get("rut");
			$tipoEncuesta = Input::get("tipo-encuesta");
			$inicial = Input::get("inicio");
			$genero = Input::get("genero-paciente");

			$this->registrarPaciente($rut, $genero);

			$testGetUp = TestGetUp::where("rut_paciente", "=", $rut)->where("inicial", "=", $inicial)->first();
			if($testGetUp == null) {
				$testGetUp = new TestGetUp;
				
			}
			$testGetUp->rut_paciente = $rut;
			if(Input::has("fecha-encuesta")) $testGetUp->fecha_test = Input::get("fecha-encuesta");
			$testGetUp->inicial = $inicial;
			if(Input::has("tiempo-encuesta")) $testGetUp->intento_practica = Input::get("tiempo-encuesta");
			if(Input::has("p-1")) $testGetUp->intento_uno = Input::get("p-1");
			if(Input::has("p-2")) $testGetUp->intento_dos = Input::get("p-2");
			if(Input::has("p-3")) $testGetUp->intento_tres = Input::get("p-3");
	
			$testGetUp->save();
	

			DB::commit();

			return Response::json(["exito" => "Los datos han sido guardados"]);
		} catch(Exception $ex){
			DB::rollback();
			return Response::json(["error" => "Error al guardar los datos", "msg" => $ex->getMessage()]);
		}


	}

	public function editarEncuestasSindromesGeriatricos(){
		try{

			DB::beginTransaction();

			$rut = Input::get("rut");
			$tipoEncuesta = Input::get("tipo-encuesta");
			$inicial = Input::get("inicio");
			$genero = Input::get("genero-paciente");

			$this->registrarPaciente($rut, $genero);
			
			$sindromesGeriatricos = SindromesGeriatricos::where("rut_paciente", "=", $rut)->where("inicial", "=", $inicial)->first();
			if($sindromesGeriatricos == null) {
				$sindromesGeriatricos = new SindromesGeriatricos;				
			}
			$sindromesGeriatricos->rut_paciente = $rut;
			if(Input::has("sindromesGeriatricos-fecha-encuesta")) $sindromesGeriatricos->fecha_test = Input::get("sindromesGeriatricos-fecha-encuesta");
			$sindromesGeriatricos->inicial = $inicial;
			if(input::has("sindromesGeriatricos-caidas-ultimos-meses")) $sindromesGeriatricos->numero_caidas = Input::get("sindromesGeriatricos-caidas-ultimos-meses");
			if(Input::has("sindromesGeriatricos-p-1")) $sindromesGeriatricos->caidas_transtorno = Input::get("sindromesGeriatricos-p-1");
			if(Input::has("sindromesGeriatricos-c-1")) $sindromesGeriatricos->comentario_caidas_transtorno = Input::get("sindromesGeriatricos-c-1");
			if(Input::has("sindromesGeriatricos-p-2")) $sindromesGeriatricos->polifarmacia = Input::get("sindromesGeriatricos-p-2");
			if(Input::has("sindromesGeriatricos-c-2")) $sindromesGeriatricos->comentario_polifarmacia = Input::get("sindromesGeriatricos-c-2");
			if(Input::has("sindromesGeriatricos-p-3")) $sindromesGeriatricos->incontinencia_urinaria = Input::get("sindromesGeriatricos-p-3");
			if(Input::has("sindromesGeriatricos-c-3")) $sindromesGeriatricos->comentario_incontinencia_urinaria = Input::get("sindromesGeriatricos-c-3");
			if(Input::has("sindromesGeriatricos-p-4")) $sindromesGeriatricos->incontinencia_fecal = Input::get("sindromesGeriatricos-p-4");
			if(Input::has("sindromesGeriatricos-c-4")) $sindromesGeriatricos->comentario_incontinencia_fecal = Input::get("sindromesGeriatricos-c-4");
			if(Input::has("sindromesGeriatricos-p-5")) $sindromesGeriatricos->deterioro_cognitivo = Input::get("sindromesGeriatricos-p-5");
			if(Input::has("sindromesGeriatricos-c-5")) $sindromesGeriatricos->comentario_deterioro_cognitivo = Input::get("sindromesGeriatricos-c-5");
			if(Input::has("sindromesGeriatricos-p-6")) $sindromesGeriatricos->delirium = Input::get("sindromesGeriatricos-p-6");
			if(Input::has("sindromesGeriatricos-c-6")) $sindromesGeriatricos->comentario_delirium = Input::get("sindromesGeriatricos-c-6");
			if(Input::has("sindromesGeriatricos-p-7")) $sindromesGeriatricos->trans_animo = Input::get("sindromesGeriatricos-p-7");
			if(Input::has("sindromesGeriatricos-c-7")) $sindromesGeriatricos->comentario_trans_animo = Input::get("sindromesGeriatricos-c-7");
			if(Input::has("sindromesGeriatricos-p-8")) $sindromesGeriatricos->trans_sueno = Input::get("sindromesGeriatricos-p-8");
			if(Input::has("sindromesGeriatricos-c-8")) $sindromesGeriatricos->comentario_trans_sueno = Input::get("sindromesGeriatricos-c-8");
			if(Input::has("sindromesGeriatricos-p-9")) $sindromesGeriatricos->malnutricion = Input::get("sindromesGeriatricos-p-9");
			if(Input::has("sindromesGeriatricos-c-9")) $sindromesGeriatricos->comentario_malnutricion = Input::get("sindromesGeriatricos-c-9");
			if(Input::has("sindromesGeriatricos-p-10")) $sindromesGeriatricos->deficit_sensorial = Input::get("sindromesGeriatricos-p-10");
			if(Input::has("sindromesGeriatricos-c-10")) $sindromesGeriatricos->comentario_deficit_sensorial = Input::get("sindromesGeriatricos-c-10");
			if(Input::has("sindromesGeriatricos-p-11")) $sindromesGeriatricos->inmovilidad = Input::get("sindromesGeriatricos-p-11");
			if(Input::has("sindromesGeriatricos-c-11")) $sindromesGeriatricos->comentario_inmovilidad = Input::get("sindromesGeriatricos-c-11");
			if(Input::has("sindromesGeriatricos-p-12")) $sindromesGeriatricos->ulcera_presion = Input::get("sindromesGeriatricos-p-12");
			if(Input::has("sindromesGeriatricos-c-12")) $sindromesGeriatricos->comentario_ulcera_presion = Input::get("sindromesGeriatricos-c-12");
			
			$sindromesGeriatricos->save();
	

			DB::commit();

			return Response::json(["exito" => "Los datos han sido guardados"]);
		} catch(Exception $ex){
			DB::rollback();
			return Response::json(["error" => "Error al guardar los datos", "msg" => $ex->getMessage()]);
		}


	}

	public function editarEncuestasIndiceBarthel(){
		try{

			DB::beginTransaction();

			$rut = Input::get("rut");
			$tipoEncuesta = Input::get("tipo-encuesta");
			$inicial = Input::get("inicio");
			$genero = Input::get("genero-paciente");

			$this->registrarPaciente($rut, $genero);

			$indiceBarthel = IndiceBarthel::where("rut_paciente", "=", $rut)->where("inicial", "=", $inicial)->first();
			if($indiceBarthel == null) {
				$indiceBarthel = new IndiceBarthel;
				
			}
			$indiceBarthel->rut_paciente = $rut;
			if(Input::has("fecha-encuesta")) $indiceBarthel->fecha_test = Input::get("fecha-encuesta");
			$indiceBarthel->inicial = $inicial;

			if(Input::has("indiceBarthel-p-1")) $indiceBarthel->comer = Input::get("indiceBarthel-p-1");
			if(Input::has("indiceBarthel-p-2")) $indiceBarthel->lavarse = Input::get("indiceBarthel-p-2");
			if(Input::has("indiceBarthel-p-3")) $indiceBarthel->vestirse = Input::get("indiceBarthel-p-3");
			if(Input::has("indiceBarthel-p-4")) $indiceBarthel->arreglarse = Input::get("indiceBarthel-p-4");
			if(Input::has("indiceBarthel-p-5")) $indiceBarthel->deposiciones = Input::get("indiceBarthel-p-5");
			if(Input::has("indiceBarthel-p-6")) $indiceBarthel->miccion = Input::get("indiceBarthel-p-6");
			if(Input::has("indiceBarthel-p-7")) $indiceBarthel->uso_retrete = Input::get("indiceBarthel-p-7");
			if(Input::has("indiceBarthel-p-8")) $indiceBarthel->trasladarse = Input::get("indiceBarthel-p-8");
			if(Input::has("indiceBarthel-p-9")) $indiceBarthel->deambular = Input::get("indiceBarthel-p-9");
			if(Input::has("indiceBarthel-p-10")) $indiceBarthel->escalones = Input::get("indiceBarthel-p-10");
				
			$indiceBarthel->save();
	

			DB::commit();

			return Response::json(["exito" => "Los datos han sido guardados"]);
		} catch(Exception $ex){
			DB::rollback();
			return Response::json(["error" => "Error al guardar los datos", "msg" => $ex->getMessage()]);
		}

	}


	public function editarEncuestasLawtonBrody(){
        try{

            DB::beginTransaction();

            $rut = Input::get("rut");
            $tipoEncuesta = Input::get("tipo-encuesta");
            $inicial = Input::get("inicio");
            $genero = Input::get("genero-paciente");

			$this->registrarPaciente($rut, $genero);
            
            $testLawtonBrody = TestLawtonBrody::where("rut_paciente", "=", $rut)->where("inicial", "=", $inicial)->first();

            if($testLawtonBrody == null) {
                $testLawtonBrody = new TestLawtonBrody;                
            }

            $testLawtonBrody->rut_paciente = $rut;
            $testLawtonBrody->inicial = $inicial;
            if(Input::has("fecha-encuesta")) $testLawtonBrody->fecha_test = Input::get("fecha-encuesta");
                        
            if(Input::has("p-1")){ $testLawtonBrody->telefono = Input::get("p-1");}
            if(Input::has("p-2")){ $testLawtonBrody->compras = Input::get("p-2");}
            if(Input::has("p-3")){ $testLawtonBrody->comida = Input::get("p-3");}
            if(Input::has("p-4")){ $testLawtonBrody->tareas = Input::get("p-4");}
            if(Input::has("p-5")){ $testLawtonBrody->ropa = Input::get("p-5");}
            if(Input::has("p-6")){ $testLawtonBrody->transporte = Input::get("p-6");}
            if(Input::has("p-7")){ $testLawtonBrody->medicacion = Input::get("p-7");}
            if(Input::has("p-8")){ $testLawtonBrody->dinero = Input::get("p-8");}
    
            $testLawtonBrody->save();    

            DB::commit();

            return Response::json(["exito" => "Los datos han sido guardados"]);
        } catch(Exception $ex){
            DB::rollback();
            return Response::json(["error" => "Error al guardar los datos", "msg" => $ex->getMessage()]);
        }


    }

    public function editarEncuestasTinetiMarcha(){
    	try{

    		DB::beginTransaction();

    		$rut = Input::get("rut");
    		$tipoEncuesta = Input::get("tipo-encuesta");
    		$inicial = Input::get("inicio");
    		$genero = Input::get("genero-paciente");

    		$this->registrarPaciente($rut, $genero);

    		$testTinetiMarcha = TestTinetiMarcha::where("rut_paciente", "=", $rut)->where("inicial", "=", $inicial)->first();
    		if($testTinetiMarcha == null) {
    			$testTinetiMarcha = new TestTinetiMarcha;

    		}
    		$testTinetiMarcha->rut_paciente = $rut;
    		if(Input::has("fecha-encuesta")){ $testTinetiMarcha->fecha_test = Input::get("fecha-encuesta");}
    		$testTinetiMarcha->inicial = $inicial;

    		if(Input::has("p-10")) $testTinetiMarcha->iniciacion_marcha = Input::get("p-10");

    		if(Input::has("p-11a")){ 
    			if(Input::get("p-11a")==2) $testTinetiMarcha->mov_pie_der_sep = 0;
    			elseif(Input::get("p-11a")==3) $testTinetiMarcha->mov_pie_der_sep = 1;
    			else  $testTinetiMarcha->mov_pie_der_sep = null;
    			if(Input::get("p-11a")==0 ||Input::get("p-11a")==1)  $testTinetiMarcha->mov_pie_der = Input::get("p-11a");
    			else $testTinetiMarcha->mov_pie_der = null;
    		}
    		if(Input::has("p-11b")){ 
    			if(Input::get("p-11b")==2) $testTinetiMarcha->mov_pie_izq_sep = 0;
    			elseif(Input::get("p-11b")==3) $testTinetiMarcha->mov_pie_izq_sep = 1;
    			else  $testTinetiMarcha->mov_pie_izq_sep = null;
    			if(Input::get("p-11b")==0 ||Input::get("p-11b")==1) $testTinetiMarcha->mov_pie_izq = Input::get("p-11b");
    			else $testTinetiMarcha->mov_pie_izq = null;
    		}
    		if(Input::has("p-12")) $testTinetiMarcha->simetria_paso = Input::get("p-12");
    		if(Input::has("p-13")) $testTinetiMarcha->fluidez_paso = Input::get("p-13");
    		if(Input::has("p-14")) $testTinetiMarcha->trayectoria = Input::get("p-14");
    		if(Input::has("p-15")) $testTinetiMarcha->tronco = Input::get("p-15");
    		if(Input::has("p-16")) $testTinetiMarcha->postura = Input::get("p-16");

    		$testTinetiMarcha->save();
    		DB::commit();

    		return Response::json(["exito" => "Los datos han sido guardados"]);
    	} catch(Exception $ex){
    		DB::rollback();
    		return Response::json(["error" => "Error al guardar los datos", "msg" => $ex->getMessage()]);
    	}
    }

    public function editarEncuestasTinetiEquilibrio(){
    	try{

    		DB::beginTransaction();

    		$rut = Input::get("rut");
    		$tipoEncuesta = Input::get("tipo-encuesta");
    		$inicial = Input::get("inicio");
			$genero = Input::get("genero-paciente");

			$this->registrarPaciente($rut, $genero);
			
			$testTinetiEquilibrio = TestTinetiEquilibrio::where("rut_paciente", "=", $rut)->where("inicial", "=", $inicial)->first();
            if($testTinetiEquilibrio == null) {
                $testTinetiEquilibrio = new TestTinetiEquilibrio;
                
            }
            $testTinetiEquilibrio->rut_paciente = $rut;
            if(Input::has("fecha-encuesta")){ $testTinetiEquilibrio->fecha_test = Input::get("fecha-encuesta");}
            $testTinetiEquilibrio->inicial = $inicial;
            
            if(Input::has("p-1")) $testTinetiEquilibrio->equilibrio_sentado = Input::get("p-1");
            if(Input::has("p-2")) $testTinetiEquilibrio->levantarse = Input::get("p-2");
            if(Input::has("p-3")) $testTinetiEquilibrio->intentos_levantarse = Input::get("p-3");
            if(Input::has("p-4")) $testTinetiEquilibrio->equilibrio_bip_inmediata = Input::get("p-4");
            if(Input::has("p-5")) $testTinetiEquilibrio->equilibrio_bip = Input::get("p-5");
            if(Input::has("p-6")) $testTinetiEquilibrio->empujar = Input::get("p-6");
            if(Input::has("p-7")) $testTinetiEquilibrio->ojos_cerrados = Input::get("p-7");
            if(Input::has("p-8")){ 
            	if(Input::get("p-8")==2) $testTinetiEquilibrio->vuelta_estable = 0;
            	elseif(Input::get("p-8")==3) $testTinetiEquilibrio->vuelta_estable = 1;
            	else $testTinetiEquilibrio->vuelta_estable = null;
	            if(Input::get("p-8")==0 ||Input::get("p-8")==1) $testTinetiEquilibrio->vuelta_continuo = Input::get("p-8");
	        	else $testTinetiEquilibrio->vuelta_continuo = null;
	    	} 

            if(Input::has("p-8b")){ $testTinetiEquilibrio->vuelta_estable = Input::get("p-8b");}
            if(Input::has("p-9")) $testTinetiEquilibrio->sentarse = Input::get("p-9");

            $testTinetiEquilibrio->save();
                        
			DB::commit();

			return Response::json(["exito" => "Los datos han sido guardados"]);
		} catch(Exception $ex){
			DB::rollback();
			return Response::json(["error" => "Error al guardar los datos", "msg" => $ex->getMessage()]);
		}

    }

            //Encuesta Tineti
    public function editarEncuestasTineti(){
		try{

			DB::beginTransaction();

			$rut = Input::get("rut");
			$tipoEncuesta = Input::get("tipo-encuesta");
			$inicial = Input::get("inicio");
			$genero = Input::get("genero-paciente");

			$this->registrarPaciente($rut, $genero);
			
			$testTinetiEquilibrio = TestTinetiEquilibrio::where("rut_paciente", "=", $rut)->where("inicial", "=", $inicial)->first();
            if($testTinetiEquilibrio == null) {
                $testTinetiEquilibrio = new TestTinetiEquilibrio;
                
            }
            $testTinetiEquilibrio->rut_paciente = $rut;
            if(Input::has("fecha-encuesta")){ $testTinetiEquilibrio->fecha_test = Input::get("fecha-encuesta");}
            $testTinetiEquilibrio->inicial = $inicial;
            
            if(Input::has("p-1")) $testTinetiEquilibrio->equilibrio_sentado = Input::get("p-1");
            if(Input::has("p-2")) $testTinetiEquilibrio->levantarse = Input::get("p-2");
            if(Input::has("p-3")) $testTinetiEquilibrio->intentos_levantarse = Input::get("p-3");
            if(Input::has("p-4")) $testTinetiEquilibrio->equilibrio_bip_inmediata = Input::get("p-4");
            if(Input::has("p-5")) $testTinetiEquilibrio->equilibrio_bip = Input::get("p-5");
            if(Input::has("p-6")) $testTinetiEquilibrio->empujar = Input::get("p-6");
            if(Input::has("p-7")) $testTinetiEquilibrio->ojos_cerrados = Input::get("p-7");
                        /*if(Input::has("p-8")){ 

            	if(Input::get("p-8")==2) $testTinetiEquilibrio->vuelta_estable = 0;
            	elseif(Input::get("p-8")==3) $testTinetiEquilibrio->vuelta_estable = 1;
            	else $testTinetiEquilibrio->vuelta_estable = null;
	            if(Input::get("p-8")==0 ||Input::get("p-8")==1) $testTinetiEquilibrio->vuelta_continuo = Input::get("p-8");
	        	else $testTinetiEquilibrio->vuelta_continuo = null;
	    	} */

	    	if(Input::has("p-8")) $testTinetiEquilibrio->vuelta_continuo = Input::get("p-8");

            if(Input::has("p-8b")) $testTinetiEquilibrio->vuelta_estable = Input::get("p-8b");

            if(Input::has("p-9")) $testTinetiEquilibrio->sentarse = Input::get("p-9");

            $testTinetiEquilibrio->save();


            $testTinetiMarcha = TestTinetiMarcha::where("rut_paciente", "=", $rut)->where("inicial", "=", $inicial)->first();
            if($testTinetiMarcha == null) {
                $testTinetiMarcha = new TestTinetiMarcha;
                
            }
            $testTinetiMarcha->rut_paciente = $rut;
            if(Input::has("fecha-encuesta")){ $testTinetiMarcha->fecha_test = Input::get("fecha-encuesta");}
            $testTinetiMarcha->inicial = $inicial;
            
            if(Input::has("p-10")) $testTinetiMarcha->iniciacion_marcha = Input::get("p-10");
            
             /*if(Input::has("p-11a")){ 

            	if(Input::get("p-11a")==2) $testTinetiMarcha->mov_pie_der_sep = 0;
            	elseif(Input::get("p-11a")==3) $testTinetiMarcha->mov_pie_der_sep = 1;
            	else  $testTinetiMarcha->mov_pie_der_sep = null;
	            if(Input::get("p-11a")==0 ||Input::get("p-11a")==1)  $testTinetiMarcha->mov_pie_der = Input::get("p-11a");
	            else $testTinetiMarcha->mov_pie_der = null;
	        }
	        if(Input::has("p-11b")){ 
            	if(Input::get("p-11b")==2) $testTinetiMarcha->mov_pie_izq_sep = 0;
            	elseif(Input::get("p-11b")==3) $testTinetiMarcha->mov_pie_izq_sep = 1;
            	else  $testTinetiMarcha->mov_pie_izq_sep = null;
	            if(Input::get("p-11b")==0 ||Input::get("p-11b")==1) $testTinetiMarcha->mov_pie_izq = Input::get("p-11b");
	            else $testTinetiMarcha->mov_pie_izq = null;
	        } */

	        if(Input::has("p-11aa")) $testTinetiMarcha->mov_pie_der = Input::get("p-11aa");
	        if(Input::has("p-11ab")) $testTinetiMarcha->mov_pie_der_sep = Input::get("p-11ab");
			if(Input::has("p-11ba")) $testTinetiMarcha->mov_pie_izq = Input::get("p-11ba");
			if(Input::has("p-11bb")) $testTinetiMarcha->mov_pie_izq_sep = Input::get("p-11bb");
			
            if(Input::has("p-12")) $testTinetiMarcha->simetria_paso = Input::get("p-12");
            if(Input::has("p-13")) $testTinetiMarcha->fluidez_paso = Input::get("p-13");
            if(Input::has("p-14")) $testTinetiMarcha->trayectoria = Input::get("p-14");
            if(Input::has("p-15")) $testTinetiMarcha->tronco = Input::get("p-15");
            if(Input::has("p-16")) $testTinetiMarcha->postura = Input::get("p-16");
                        
            $testTinetiMarcha->save();
			DB::commit();

			return Response::json(["exito" => "Los datos han sido guardados"]);
		} catch(Exception $ex){
			DB::rollback();
			return Response::json(["error" => "Error al guardar los datos", "msg" => $ex->getMessage()]);
		}


	}

	public function editarEncuestasCharlson(){
        try{

            DB::beginTransaction();

            $rut = Input::get("rut");
            $tipoEncuesta = Input::get("tipo-encuesta");
            $inicial = Input::get("inicio");
            $genero = Input::get("genero-paciente");

			$this->registrarPaciente($rut, $genero);
            
            $testCharlson = TestCharlson::where("rut_paciente", "=", $rut)->where("inicial", "=", $inicial)->first();

            if($testCharlson == null) {
                $testCharlson = new TestCharlson;                
            }

            $testCharlson->rut_paciente = $rut;
            $testCharlson->inicial = $inicial;
            if(Input::has("fecha-encuesta")) $testCharlson->fecha_test = Input::get("fecha-encuesta");
                        
            if(Input::has("p-1")) $testCharlson->infarto_miocardio = Input::get("p-1"); else $testCharlson->infarto_miocardio = false;
            if(Input::has("p-2")) $testCharlson->insuficiencia_cardiaca = Input::get("p-2"); else $testCharlson->insuficiencia_cardiaca = false;
            if(Input::has("p-3")) $testCharlson->enf_arterial = Input::get("p-3"); else $testCharlson->enf_arterial = false;
            if(Input::has("p-4")) $testCharlson->enf_cerebrovascular = Input::get("p-4"); else $testCharlson->enf_cerebrovascular = false;
            if(Input::has("p-5")) $testCharlson->demencia = Input::get("p-5"); else $testCharlson->demencia = false;
            if(Input::has("p-6")) $testCharlson->enf_respiratoria = Input::get("p-6"); else $testCharlson->enf_respiratoria = false;
            if(Input::has("p-7")) $testCharlson->enf_tejido = Input::get("p-7"); else $testCharlson->enf_tejido = false;
            if(Input::has("p-8")) $testCharlson->ulcera_gastro = Input::get("p-8"); else $testCharlson->ulcera_gastro = false;
            if(Input::has("p-9")) $testCharlson->hep_leve = Input::get("p-9"); else $testCharlson->hep_leve = false;
            if(Input::has("p-10")) $testCharlson->diabetes = Input::get("p-10"); else $testCharlson->diabetes = false;
            if(Input::has("p-11")) $testCharlson->hemiplejia = Input::get("p-11"); else $testCharlson->hemiplejia = false;
            if(Input::has("p-12")) $testCharlson->insuficiencia_moderada = Input::get("p-12"); else $testCharlson->insuficiencia_moderada = false;
            if(Input::has("p-13")) $testCharlson->diabetes_lesion = Input::get("p-13"); else $testCharlson->diabetes_lesion = false;
            if(Input::has("p-14")) $testCharlson->tumor = Input::get("p-14"); else $testCharlson->tumor = false;
            if(Input::has("p-15")) $testCharlson->leucemia = Input::get("p-15"); else $testCharlson->leucemia = false;
            if(Input::has("p-16")) $testCharlson->linfoma = Input::get("p-16"); else $testCharlson->linfoma = false;
            if(Input::has("p-17")) $testCharlson->hepatopatia = Input::get("p-17"); else $testCharlson->hepatopatia = false;
            if(Input::has("p-18")) $testCharlson->tumor_metastasis = Input::get("p-18"); else $testCharlson->tumor_metastasis = false;
            if(Input::has("p-19")) $testCharlson->sida = Input::get("p-19"); else $testCharlson->sida = false;
    
            $testCharlson->save();    

            DB::commit();

            return Response::json(["exito" => "Los datos han sido guardados"]);
        } catch(Exception $ex){
            DB::rollback();
            return Response::json(["error" => "Error al guardar los datos", "msg" => $ex->getMessage()]);
        }


    }

    public function editarEncuestasWhoqol(){
   //      try{

   //          DB::beginTransaction();

   //          $rut = Input::get("rut");
   //          $tipoEncuesta = Input::get("tipo-encuesta");
   //          $inicial = Input::get("inicio");
   //          $genero = Input::get("genero-paciente");

			// $this->registrarPaciente($rut, $genero);
            
   //          $testCharlson = TestCharlson::where("rut_paciente", "=", $rut)->where("inicial", "=", $inicial)->first();

   //          if($testCharlson == null) {
   //              $testCharlson = new TestCharlson;                
   //          }

   //          $testCharlson->rut_paciente = $rut;
   //          $testCharlson->inicial = $inicial;
   //          if(Input::has("fecha-encuesta")) $testCharlson->fecha_test = Input::get("fecha-encuesta");
                        
   //          if(Input::has("p-1")) $testCharlson->infarto_miocardio = Input::get("p-1"); else $testCharlson->infarto_miocardio = false;
   //          if(Input::has("p-2")) $testCharlson->insuficiencia_cardiaca = Input::get("p-2"); else $testCharlson->insuficiencia_cardiaca = false;
   //          if(Input::has("p-3")) $testCharlson->enf_arterial = Input::get("p-3"); else $testCharlson->enf_arterial = false;
   //          if(Input::has("p-4")) $testCharlson->enf_cerebrovascular = Input::get("p-4"); else $testCharlson->enf_cerebrovascular = false;
   //          if(Input::has("p-5")) $testCharlson->demencia = Input::get("p-5"); else $testCharlson->demencia = false;
   //          if(Input::has("p-6")) $testCharlson->enf_respiratoria = Input::get("p-6"); else $testCharlson->enf_respiratoria = false;
   //          if(Input::has("p-7")) $testCharlson->enf_tejido = Input::get("p-7"); else $testCharlson->enf_tejido = false;
   //          if(Input::has("p-8")) $testCharlson->ulcera_gastro = Input::get("p-8"); else $testCharlson->ulcera_gastro = false;
   //          if(Input::has("p-9")) $testCharlson->hep_leve = Input::get("p-9"); else $testCharlson->hep_leve = false;
   //          if(Input::has("p-10")) $testCharlson->diabetes = Input::get("p-10"); else $testCharlson->diabetes = false;
   //          if(Input::has("p-11")) $testCharlson->hemiplejia = Input::get("p-11"); else $testCharlson->hemiplejia = false;
   //          if(Input::has("p-12")) $testCharlson->insuficiencia_moderada = Input::get("p-12"); else $testCharlson->insuficiencia_moderada = false;
   //          if(Input::has("p-13")) $testCharlson->diabetes_lesion = Input::get("p-13"); else $testCharlson->diabetes_lesion = false;
   //          if(Input::has("p-14")) $testCharlson->tumor = Input::get("p-14"); else $testCharlson->tumor = false;
   //          if(Input::has("p-15")) $testCharlson->leucemia = Input::get("p-15"); else $testCharlson->leucemia = false;
   //          if(Input::has("p-16")) $testCharlson->linfoma = Input::get("p-16"); else $testCharlson->linfoma = false;
   //          if(Input::has("p-17")) $testCharlson->hepatopatia = Input::get("p-17"); else $testCharlson->hepatopatia = false;
   //          if(Input::has("p-18")) $testCharlson->tumor_metastasis = Input::get("p-18"); else $testCharlson->tumor_metastasis = false;
   //          if(Input::has("p-19")) $testCharlson->sida = Input::get("p-19"); else $testCharlson->sida = false;
    
   //          $testCharlson->save();    

   //          DB::commit();

   //          return Response::json(["exito" => "Los datos han sido guardados"]);
   //      } catch(Exception $ex){
   //          DB::rollback();
   //          return Response::json(["error" => "Error al guardar los datos", "msg" => $ex->getMessage()]);
   //      }


    }

    public function editarEncuestasICIQ(){
        try{
        	DB::beginTransaction();

            $rut = Input::get("rut");
            $tipoEncuesta = Input::get("tipo-encuesta");
            $inicial = Input::get("inicio");
            $genero = Input::get("genero-paciente");

			$this->registrarPaciente($rut, $genero);
            
            $testIciq = TestIciq::where("rut_paciente", "=", $rut)->where("inicial", "=", $inicial)->first();

            if($testIciq == null) {	
                $testIciq = new TestIciq;                
            }

            $testIciq->rut_paciente = $rut;
            $testIciq->inicial = $inicial;
            if(Input::has("fecha-encuesta")) $testIciq->fecha_test = Input::get("fecha-encuesta");
                        
            if(Input::has("p-1")) $testIciq->frecuencia_pierde_orina = Input::get("p-1");
            if(Input::has("p-2")) $testIciq->cantidad_orina = Input::get("p-2");
            if(Input::has("p-3")) $testIciq->escala_escape = Input::get("p-3");

            if(Input::has("p-4")) $testIciq->nunca = Input::get("p-4"); else $testIciq->nunca = false;
            if(Input::has("p-5")) $testIciq->antes_wc = Input::get("p-5"); else $testIciq->antes_wc = false;
            if(Input::has("p-6")) $testIciq->tose_estornuda = Input::get("p-6"); else $testIciq->tose_estornuda = false;
            if(Input::has("p-7")) $testIciq->duerme = Input::get("p-7"); else $testIciq->duerme = false;
            if(Input::has("p-8")) $testIciq->esfuerzo_ejercicio = Input::get("p-8"); else $testIciq->esfuerzo_ejercicio = false;
            if(Input::has("p-9")) $testIciq->vestido = Input::get("p-9"); else $testIciq->vestido = false;
            if(Input::has("p-10")) $testIciq->sin_motivo = Input::get("p-10"); else $testIciq->sin_motivo = false;
            if(Input::has("p-11")) $testIciq->continua = Input::get("p-11"); else $testIciq->continua = false;
    
            $testIciq->save();    

            DB::commit();

            return Response::json(["exito" => "Los datos han sido guardados"]);
        } catch(Exception $ex){
            DB::rollback();
            return Response::json(["error" => "Error al guardar los datos", "msg" => $ex->getMessage()]);
        }


    }  	

    public function mmse(){
    		DB::beginTransaction();

    		$inicial = Input::get("inicio");
    		$fecha_test = input::get("fecha_test");
    		$rut = Input::get("rut");
    		$fecha=Input::get("p-1");
        	$mes=Input::get("p-2");
        	$dia_semana=Input::get("p-3");
        	$anio=Input::get("p-4");
        	$estacion=Input::get("p-5");

        	$lugar=Input::get("p-6");
        	$piso=Input::get("p-7");
        	$ciudad=Input::get("p-8");
        	$comuna=Input::get("p-9");
        	$pais=Input::get("p-10");

        	$arbol1=Input::get("p-11");
        	$mesa1=Input::get("p-12");
        	$perro1=Input::get("p-13");

        	$letra_o=Input::get("p-14");
        	$letra_d=Input::get("p-15");
        	$letra_n=Input::get("p-16");
        	$letra_u=Input::get("p-17");
        	$letra_m=Input::get("p-18");

        	$arbol2=Input::get("p-19");
        	$mesa2=Input::get("p-20");
        	$perro2=Input::get("p-21");


        	$lapiz=Input::get("p-22");
        	$reloj=Input::get("p-23");
        	$repetir_frase=Input::get("p-24");
        	$papel1=Input::get("p-25"); //tomar
        	$papel2=Input::get("p-26"); // doblar
        	$papel3=Input::get("p-27"); // dejar
        	$ojos_cerrados=Input::get("p-28");
        	$escribir_frase=Input::get("p-29");
        	$dibujo=Input::get("p-30");

        	$repeticion1 = Input::get("repeticion1");


        	$testMmse = TestMmse::where("rut_paciente", "=", $rut)->where("inicial", "=", $inicial)->first();

        	if($testMmse == null){
        		$testMmse = new TestMmse;
        	}
        	$testMmse->inicial = $inicial;
        	$testMmse->rut_paciente=$rut;
        	//$testMmse->fecha_test= $fecha_test;
        	if(Input::has("fecha_test"))$testMmse->fecha_test=$fecha_test;
        	$testMmse->fecha = $fecha;
        	$testMmse->mes = $mes;
        	$testMmse->dia_semana = $dia_semana;
        	$testMmse->anio = $anio;
        	$testMmse->estacion = $estacion;
        	$testMmse->lugar = $lugar;
        	$testMmse->piso = $piso;
        	$testMmse->ciudad = $ciudad;
        	$testMmse->comuna = $comuna;
        	$testMmse->pais = $pais;
        	$testMmse->numero_repeticiones_palabras = $repeticion1;
        	$testMmse->arbol1 = $arbol1;
        	$testMmse->mesa1 = $mesa1;
        	$testMmse->perro1 = $perro1;
        	$testMmse->letra_o = $letra_o;
        	$testMmse->letra_d = $letra_d;
        	$testMmse->letra_n = $letra_n;
        	$testMmse->letra_u = $letra_u;
        	$testMmse->letra_m = $letra_m;
        	$testMmse->arbol2 = $arbol2;
        	$testMmse->mesa2 = $mesa2;
        	$testMmse->perro2 = $perro2;
        	$testMmse->lapiz = $lapiz;
        	$testMmse->reloj = $reloj;
        	$testMmse->tomar_papel = $papel1;
        	$testMmse->dejar_papel = $papel3;
        	$testMmse->doblar_papel = $papel2;
        	$testMmse->repetir_frase = $repetir_frase;
        	$testMmse->leer_obedecer = $ojos_cerrados;
        	$testMmse->escribir_frase = $escribir_frase;
        	$testMmse->hacer_dibujo = $dibujo;

        	$testMmse->save();
        	DB::commit();



    	return Response::json(["exito" => "Los datos han sido guardados"]);
    }

 public function conductaMotriz(){

			$inicial = Input::get("inicio");
    		$fecha_test = input::get("fecha_test");
    		$rut = Input::get("rut");

    		$p0=Input::get("p-0");  //conducta anomala
        	if($p0 == null) $p0 = "false";
    		$p1=Input::get("p-1");  //conducta anomala
        	if($p1 == null) $p1 = "false";
        	$p2=Input::get("p-2");  
        	if($p2 == null) $p2 = "false";
        	$p3=Input::get("p-3");
        	if($p3 == null) $p3 = "false";
        	$p4=Input::get("p-4");
        	if($p4 == null) $p4 = "false";
        	$p5=Input::get("p-5");
        	if($p5 == null) $p5 = "false";
        	$p6=Input::get("p-6");
        	if($p6 == null) $p6 = "false";
        	$p7=Input::get("p-7");
        	if($p7 == null) $p7 = "false";
        	$p8=Input::get("p-8");
        	if($p8 == null) $p8 = 0;
        	$p9=Input::get("p-9");
        	if($p9 == null) $p9 = 0;
        	$p10=Input::get("p-10");
        	if($p10 == null) $p10 = 0;

        	$todo = Input::all();

    		$testNpi = TestNpi::where("rut_paciente", "=", $rut)->where("inicial", "=", $inicial)->first();
    		if($testNpi == null){
        		$testNpi = new TestNpi;
        	}

    	try{

    		DB::beginTransaction();

        	if(Input::has("fecha_test"))$testNpi->fecha_test=$fecha_test;

        	

        	$testNpi->rut_paciente=$rut;
        	
        	$testNpi->inicial = $inicial;

        	$testNpi->conducta_anomala = $p0;
        	$testNpi->preg1_npi = $p1;
        	$testNpi->preg2_npi = $p2;
        	$testNpi->preg3_npi = $p3;
        	$testNpi->preg4_npi = $p4;
        	$testNpi->preg5_npi = $p5;
        	$testNpi->preg6_npi = $p6;
        	$testNpi->preg7_npi = $p7;
        	

        	$testNpi->frecuencia_npi = $p8;
        	$testNpi->gravedad_npi = $p9;
        	$testNpi->angustia_npi =$p10;

        	$testNpi->save();

    		DB::commit();

    		return Response::json(["exito" => "Datos guardados"]);
        } catch(Exception $ex){
            DB::rollback();
            return Response::json(["error" => "Error al guardar los datos", "msg" => $ex->getMessage(), "fecha"=>$todo]);
        }
    }
    public function EvaluacionCognitiva(){
        try{
        	DB::beginTransaction();

        	$rut = Input::get("rut");
        	$inicial = Input::get("inicio");
        	$fecha_test = Input::get("fecha_test");
        	$mes=Input::get("p-1");
        	$dia=Input::get("p-2");
        	$anio=Input::get("p-3");
        	$dia_semana=Input::get("p-4");
        	$arbol1=Input::get("p-5");
        	$mesa1=Input::get("p-6");
        	$avion1=Input::get("p-7");
        	$toma_papel=Input::get("p-8");
        	$dobla_papel=Input::get("p-9");
        	$coloca_papel=Input::get("p-10");
        	$arbol2=Input::get("p-11");
        	$mesa2=Input::get("p-12");
        	$avion2=Input::get("p-13");
        	$circulos=Input::get("p-14");
        	$numero_repeticiones_palabras=Input::get("repeticiones");
        	$primer_numero=Input::get("primer_numero");
        	$segundo_numero=Input::get("segundo_numero");
        	$tercer_numero=Input::get("tercer_numero");
        	$cuarto_numero=Input::get("cuarto_numero");
        	$quinto_numero=Input::get("quinto_numero");

            $testCognitivo = TestEvaluacionCognitiva::where("rut_paciente", "=", $rut)->where("inicial", "=", $inicial)->first();

            if($testCognitivo == null) 
            {	
               $testCognitivo = new TestEvaluacionCognitiva;                
            }

         	$testCognitivo->rut_paciente=$rut;
         	if(Input::has("fecha_test"))$testCognitivo->fecha_test=$fecha_test;
         	$testCognitivo->inicial=$inicial;
         	$testCognitivo->mes=$mes;
         	$testCognitivo->dia_mes=$dia;
         	$testCognitivo->anio=$anio;
        	$testCognitivo->dia_semana=$dia_semana;
         	$testCognitivo->arbol1=$arbol1;
         	$testCognitivo->mesa1=$mesa1;
         	$testCognitivo->avion1=$avion1;
      		$testCognitivo->toma_papel=$toma_papel;
         	$testCognitivo->dobla_papel=$dobla_papel;
         	$testCognitivo->coloca_papel=$coloca_papel;
         	$testCognitivo->arbol2=$arbol2;
         	$testCognitivo->mesa2=$mesa2;
         	$testCognitivo->avion2=$avion2;
         	$testCognitivo->circulos=$circulos;
         	$testCognitivo->numero_repeticiones_palabras=$numero_repeticiones_palabras;
         	if(Input::has("primer_numero"))$testCognitivo->primer_numero=$primer_numero;
         	if(Input::has("segundo_numero"))$testCognitivo->segundo_numero=$segundo_numero;
         	if(Input::has("tercer_numero"))$testCognitivo->tercer_numero=$tercer_numero;
         	if(Input::has("cuarto_numero"))$testCognitivo->cuarto_numero=$cuarto_numero;
         	if(Input::has("quinto_numero"))$testCognitivo->quinto_numero=$quinto_numero;

        	$testCognitivo->save();

         	DB::commit();
    		return Response::json(["exito" => "Los datos han sido guardados"]);
        } catch(Exception $ex){
            DB::rollback();
            return Response::json(["error" => "Error al guardar los datos", "msg" => $ex->getMessage()]);
        }
    }
    public function formularioGDS(){
        try{
        	DB::beginTransaction();

        	$rut = Input::get("rut");
        	$inicial = Input::get("inicial");
        	$fecha_test = Input::get("fecha_gds");
        	$gds=Input::get("gds");
        	
            $test_gds = GDS::where("rut_paciente", "=", $rut)->where("inicial", "=", $inicial)->first();

            if($test_gds == null) 
            {	
               $test_gds = new GDS;                
            }

         	$test_gds->rut_paciente=$rut;
         	if(Input::has("fecha_gds"))$test_gds->fecha_test=$fecha_test;
         	$test_gds->inicial=$inicial;
         	$test_gds->gds=$gds;

        	$test_gds->save();

         	DB::commit();
    		return Response::json(["exito" => "Los datos han sido guardados"]);
        } catch(Exception $ex){
            DB::rollback();
            return Response::json(["error" => "Error al guardar los datos", "msg" => $ex->getMessage()]);
        }
    }
    public function IngresarEvaluacion(){
        try{
        	DB::beginTransaction();

			/*Evaluacion antigua actualizar*/
		/*	$id_evaluacion_caidas = Input::get("id_evaluacion_caidas");
			$fecha_evaluacion2    = Input::get("fecha_evaluacion2");
			$caida2 			  = Input::get("caida2");
			$evaluacion2  	      =	Input::get("evaluacion2");
			$alertas2 	          =	Input::get("alertas2");
			$comentario2  	      =	Input::get("comentario2");

			for($i=0; $i<count($fecha_evaluacion2); $i++){
				$ActualizaEvaluacion=Evaluacion::find($id_evaluacion_caidas[$i]);
				if($fecha_evaluacion2[$i]!=null){

					$ActualizaEvaluacion->fecha_evaluacion = $fecha_evaluacion2[$i];
					$ActualizaEvaluacion->caida 		   = strtolower($caida2[$i]);
					$ActualizaEvaluacion->comentario 	   = $comentario2[$i];
					$ActualizaEvaluacion->numero_alertas   = $alertas2[$i];

					if ($evaluacion2[$i]=='Si')$ActualizaEvaluacion->evaluacion='TRUE';
             		else $ActualizaEvaluacion->evaluacion='FALSE';

					$ActualizaEvaluacion->save();
				}
			}
*/
        	/*Evaluacion nueva*/
			$rut_paciente  	  = Input::get("rut");
			$fecha_evaluacion = Input::get("_fecha_evaluacion");
			$caida 			  = Input::get("caida");
			$comentario  	  =	Input::get("comentario");
			$evaluacion 	  =	Input::get("evaluacion");
			$alertas	  	  =	Input::get("_alertas");
			$id_evaluacion	  =	Input::get("id_evaluacion_caidas");
			$ingresarEvaluacion=null;
			
			$frecuencia_mascota=Input::get("frecuencia_mascota");
			$numero_mascota=Input::get("numero_mascota");
			$numero_personas=Input::get("numero_personas");
			
			$paciente=Paciente::find($rut_paciente);
			if($paciente)
			{
				if(Input::has("frecuencia_mascota"))
					$paciente->frecuencia_mascota_hogar=$frecuencia_mascota;
				if(Input::has("numero_mascota"))
					$paciente->n_mascotas_vive=$numero_mascota;
				if(Input::has("numero_personas"))
					$paciente->n_personas_vive=$numero_personas;
				$paciente->save();
			}
			

			for($i=0; $i<count($fecha_evaluacion); $i++){
				if($id_evaluacion[$i]!=0)
					$ingresarEvaluacion=Evaluacion::find($id_evaluacion[$i]);
				else
					$ingresarEvaluacion=new Evaluacion();

				if($fecha_evaluacion[$i]!=null){
					$ingresarEvaluacion->rut_paciente=$rut_paciente;
					$ingresarEvaluacion->fecha_evaluacion=$fecha_evaluacion[$i];
					if(empty($caida))
					{
						if($ingresarEvaluacion->caida=="sin dato")
						{
							$ingresarEvaluacion->caida="sin dato";
						}
					}
					else
						$ingresarEvaluacion->caida=strtolower($caida[$i]);
					if(empty($comentario))
					{
						if($ingresarEvaluacion->comentario=="")
							$ingresarEvaluacion->comentario="";
					}
					else
						$ingresarEvaluacion->comentario=$comentario[$i];
					$ingresarEvaluacion->numero_alertas=$alertas[$i];

					if(empty($evaluacion))
					{
						if($ingresarEvaluacion->evaluacion==false)
							$ingresarEvaluacion->evaluacion=false;
					}
					else
					{
						if ($evaluacion[$i]=='Si')$ingresarEvaluacion->evaluacion='TRUE';
						else $ingresarEvaluacion->evaluacion='FALSE';
					}
					

					$ingresarEvaluacion->save();
				}
			}

        	 DB::commit();
    		return Response::json(["exito" => "Los datos han sido guardados"]);
        } catch(Exception $ex){
            DB::rollback();
            return Response::json(["error" => "Error al guardar los datos", "msg" => $ex->getMessage()]);
        }
    }

    public function IngresarMicciones(){
        try{
        	DB::beginTransaction();

        	/*Micciones nueva*/
			$fecha_micciones = Input::get("fecha_micciones");
			$rut_paciente  	  = Input::get("rut");
			$hora_acostarse  = Input::get("hora_acostarse");
			$horario   		 = Input::get("horario");
			$volumen  	     = Input::get("volumen");
			$perdidaOrina 	 = Input::get("perdidaOrina");
			$contador		=Input::get("contador");
			$id_cartilla	=Input::get("idcartilla");          
			$id_detalle_cartilla	=Input::get("iddetalle");
			$cont_inicial	=0;
			$cont_final		=0;
			$un_dia=new DateInterval('P1D');
			$fecha_detalle=new DateTime();
			$dia_sumado=false;
			for($i=0;$i<count($fecha_micciones);$i++)
			{
				$IngresarMicciones=new CartillaMicciones();
				if($id_cartilla[$i]!=0)
					$IngresarMicciones=CartillaMicciones::find($id_cartilla[$i]);
				$dia_sumado=false;	
				if($hora_acostarse[$i]!=null)
				{
					$f=preg_split("/\D/",$fecha_micciones[$i]);
					$fecha_detalle->setDate($f[2],$f[1],$f[0]);
					$IngresarMicciones->rut_paciente=$rut_paciente;
					$IngresarMicciones->hora_acostarse=$hora_acostarse[$i];
					$IngresarMicciones->fecha_cartilla=$fecha_micciones[$i];
					$IngresarMicciones->save();
					$cont_inicial=$cont_final;
					$cont_final=$cont_final+$contador[$i];
				}
				$IdMiccion=$IngresarMicciones->id_cartilla_micciones;
	
				if($IdMiccion==null)
					continue;
				for($j=$cont_inicial; $j<$cont_final; $j++){
					$IngresarDataMicciones=new DetalleCartillaMicciones();
					if($id_detalle_cartilla[$j]!=0)
						$IngresarDataMicciones=DetalleCartillaMicciones::find($id_detalle_cartilla[$j]);
					if($horario[$j]!=null){
						$h=preg_split("/\D/",$horario[$j]);
						if($h[0]<12&&!$dia_sumado)
						{
							$fecha_detalle->add($un_dia);
							$dia_sumado=true;
						}
						$fecha_detalle->setTime($h[0],$h[1]);
						$IngresarDataMicciones->id_cartilla_micciones=$IdMiccion;
						$IngresarDataMicciones->horario=$fecha_detalle->format("Y-m-d H:i:s");
						$IngresarDataMicciones->volumen=$volumen[$j];
	
						if ($perdidaOrina[$j]=='si')$IngresarDataMicciones->perdida_orina='TRUE';
						if ($perdidaOrina[$j]=='no')$IngresarDataMicciones->perdida_orina='FALSE';
						if ($perdidaOrina[$j]=='sn')$IngresarDataMicciones->perdida_orina=null;
	
						$IngresarDataMicciones->save();
					}
				}
			}

        	DB::commit();
    		return Response::json(["exito" => "Los datos han sido guardados"]);
        } catch(Exception $ex){
            DB::rollback();
            return Response::json(["error" => "Error al guardar los datos", "msg" => $ex->getMessage()]);
        }
    }

    //se usa
	public function obtenerEncuestas(Request $request){
		$encuestas = [];
		$testGetUp = [];
		$testGetUpFinal = [];
		$sindromesGeriatricos = [];
		$sindromesGeriatricosFinal = [];
		$indiceBarthel = [];
		$indiceBarthelFinal = [];
		$testLawtonBrody = [];
		$testLawtonBrodyFinal = [];
		$testTinetiEquilibrio = [];
		$testTinetiEquilibrioFinal = [];
		$testTinetiMarcha = [];
		$testTinetiMarchaFinal = [];
		$testCharlson = [];
		$testCharlsonFinal = [];
		$testICIQ = [];
		$testICIQFinal = [];
		$evaluacionCognitivaIni=[];
		$evaluacionCognitivaFinal=[];
		$gdsInicial=[];
		$gdsFinal=[];
		$mmseInicial=[];
		$mmseFinal=[];
		$ConductaMotrizInicial=[];
		$ConductaMotrizFinal=[];
		$Conclu_acciones_repetidas=[];
		$testWhoqol=[];

		$rut = $request->input("rut");

		//return $rut;

		$testCharlson = TestCharlson::where("rut_paciente", "=", $rut)->first();		
		$encuestas["testCharlson"] = $testCharlson;

		$indiceBarthel = IndiceBarthel::where("rut_paciente", "=", $rut)->first();
		$encuestas["indiceBarthel"] = $indiceBarthel;	

		$testLawtonBrody = TestLawtonBrody::where("rut_paciente", "=", $rut)->first();
		$encuestas["testLawtonBrody"] = $testLawtonBrody;	

		$sindromesGeriatricos = SindromesGeriatricos::where("rut_paciente", "=", $rut)->first();
		$encuestas["sindromesGeriatricos"] = $sindromesGeriatricos;	

		$mmseInicial = TestMmse::where("rut_paciente", "=", $rut)->first();		
		$encuestas["mmseInicial"] = $mmseInicial;

		$Whoqol =  testWhoqol::where("rut_paciente", "=", $rut)->orderBy("fecha_test","desc")->get();
		$encuestas["testWhoqol"] = $Whoqol[0];
		
		if (count($Whoqol) > 1) {
			$encuestas["testWhoqolA3"] = $Whoqol[1];
		}
		

		return $encuestas;

		$testGetUp = TestGetUp::where("rut_paciente", "=", $rut)->where("inicial", "=", true)->first();		
		$encuestas["testGetUp"] = $testGetUp;
		
		$testGetUpFinal = TestGetUp::where("rut_paciente", "=", $rut)->where("inicial", "=", false)->first();		
		$encuestas["testGetUpFinal"] = $testGetUpFinal;		

		

		$sindromesGeriatricosFinal = SindromesGeriatricos::where("rut_paciente", "=", $rut)->where("inicial", "=", false)->first();
		$encuestas["sindromesGeriatricosFinal"] = $sindromesGeriatricosFinal;
		
		

		$indiceBarthelFinal = IndiceBarthel::where("rut_paciente", "=", $rut)->where("inicial", "=", false)->first();
		$encuestas["indiceBarthelFinal"] = $indiceBarthelFinal;	

		

		$testLawtonBrodyFinal = TestLawtonBrody::where("rut_paciente", "=", $rut)->where("inicial", "=", false)->first();
		$encuestas["testLawtonBrodyFinal"] = $testLawtonBrodyFinal;	

		$testTinetiEquilibrio = TestTinetiEquilibrio::where("rut_paciente", "=", $rut)->where("inicial", "=", true)->first();
		$encuestas["testTinetiEquilibrio"] = $testTinetiEquilibrio;	

		$testTinetiEquilibrioFinal = TestTinetiEquilibrio::where("rut_paciente", "=", $rut)->where("inicial", "=", false)->first();
		$encuestas["testTinetiEquilibrioFinal"] = $testTinetiEquilibrioFinal;	

		$testTinetiMarcha = TestTinetiMarcha::where("rut_paciente", "=", $rut)->where("inicial", "=", true)->first();
		$encuestas["testTinetiMarcha"] = $testTinetiMarcha;	

		$testTinetiMarchaFinal = TestTinetiMarcha::where("rut_paciente", "=", $rut)->where("inicial", "=", false)->first();
		$encuestas["testTinetiMarchaFinal"] = $testTinetiMarchaFinal;	

		//$testCharlson = TestCharlson::where("rut_paciente", "=", $rut)->where("inicial", "=", true)->first();		
		//$encuestas["testCharlson"] = $testCharlson;
		
		//$testCharlsonFinal = TestCharlson::where("rut_paciente", "=", $rut)->where("inicial", "=", false)->first();		
		//$encuestas["testCharlsonFinal"] = $testCharlsonFinal;

		$testIciq = TestIciq::where("rut_paciente", "=", $rut)->where("inicial", "=", true)->first();		
		$encuestas["testIciq"] = $testIciq;

		$testIciqFinal = TestIciq::where("rut_paciente", "=", $rut)->where("inicial", "=", false)->first();		
		$encuestas["testIciqFinal"] = $testIciqFinal;

		$evaluacionCognitivaIni = TestEvaluacionCognitiva::where("rut_paciente", "=", $rut)->where("inicial", "=", true)->first();		
		$encuestas["evaluacionCognitivaIni"] = $evaluacionCognitivaIni;

		$evaluacionCognitivaFinal = TestEvaluacionCognitiva::where("rut_paciente", "=", $rut)->where("inicial", "=", false)->first();		
		$encuestas["evaluacionCognitivaFinal"] = $evaluacionCognitivaFinal;

		
		$gdsInicial = GDS::where("rut_paciente", "=", $rut)->where("inicial", "=", true)->first();		
		$encuestas["gdsInicial"] = $gdsInicial;
		
		$gdsFinal = GDS::where("rut_paciente", "=", $rut)->where("inicial", "=", false)->first();		
		$encuestas["gdsFinal"] = $gdsFinal;

		
		$mmseFinal = TestMmse::where("rut_paciente", "=", $rut)->where("inicial", "=", false)->first();		
		$encuestas["mmseFinal"] = $mmseFinal;

		$ConductaMotrizInicial = TestNpi::where("rut_paciente", "=", $rut)->where("inicial", "=", true)->first();		
		$encuestas["ConductaMotrizInicial"] = $ConductaMotrizInicial;
		
		$ConductaMotrizFinal = TestNpi::where("rut_paciente", "=", $rut)->where("inicial", "=", false)->first();		
		$encuestas["ConductaMotrizFinal"] = $ConductaMotrizFinal;

		$Conclu_acciones_repetidas=InformeAccionesRepetidas::where("rut_paciente", "=", $rut)->first();
		$encuestas["Conclu_acciones_repetidas"] = $Conclu_acciones_repetidas;
		
		return $encuestas;
	}
	public static function obtenerDatos($rut=null,$intervalo=0)
	{
		
		return ResultadoMicciones::obtenerDatos(Input::has("rut")?Input::get("rut"):$rut,Input::has("intervalo")?Input::get("intervalo"):$intervalo);
	}
	public static function obtenerEvaluacion()
	{
		
		return Evaluacion::obtenerEvaluacion(Input::get("rut"));
	}
	public static function obtenerDatosHogar()
	{
		$rut=Input::get("rut");
		return Paciente::select("frecuencia_mascota_hogar","n_mascotas_vive","n_personas_vive")
		->where("rut","=",$rut)
		->first();
	}
	public function obtenerPacientesNicturia() {
		$contadorPaciente  = 0;
		$diasDiferencia    = 0;
		$segundos          = 0;
		$diferenciaDiasMax = 0;
		$head              = "";
		$body              = "";
		$columnaHead       = "";
		$colspan           = "";
		$auxPaciente       = 0;
		$cantidadDias      = 0;
		$contadorAux	   = 0;
		$diferencia			=0;
		$style_izq='style="border-left-style:solid;border-left-width:2px;"';
		$style_der='style="border-right-style:solid;border-right-width:2px;"';
		$sqlMaxDias = "SELECT
						MAX( cartilla.nro_dias ) AS max_cantidad_dias
						FROM
						(SELECT 
						mc.rut_paciente,
						COUNT(mc.fecha_cartilla) AS nro_dias
						FROM cartilla_micciones mc
						GROUP by mc.rut_paciente) cartilla
						";
		$maxDias = DB::select($sqlMaxDias);

		$sqlInformeNicturias = "SELECT
			cartilla.rut_paciente,
			cartilla.fecha_cartilla,
			TO_CHAR(cartilla.fecha_cartilla,'DD-MM-YYYY')AS fecha_cartilla2,
			cartilla.nro_cartilla,
			cartilla.id_cartilla_micciones,
			cartilla.nicturia,
			(
				SELECT numero_alertas FROM configuracion_cartilla_micciones
				WHERE cartilla.id_cartilla_micciones=configuracion_cartilla_micciones.id_cartilla_micciones
				AND rut_usuario=".Auth::user()->rut."
			)AS cantidad_alertas,
			(SELECT COUNT(*) from cartilla_micciones
			WHERE rut_paciente=cartilla.rut_paciente
			AND cartilla_visible=TRUE)AS cantidad_dias
			FROM
			(	SELECT 
				mc.rut_paciente,
				mc.fecha_cartilla,
				mc.id_cartilla_micciones,
				mc.nicturia,
				(SELECT COUNT(id_detalle_cartilla_micciones)
				FROM detalle_cartilla_micciones
				WHERE id_cartilla_micciones = mc.id_cartilla_micciones
				AND mc.cartilla_visible=TRUE
				) AS nro_cartilla
			FROM cartilla_micciones mc
			WHERE mc.cartilla_visible=TRUE) cartilla
			ORDER BY cartilla.rut_paciente,
			cartilla.fecha_cartilla";
		$dataNicturias = DB::select($sqlInformeNicturias);

		foreach ($dataNicturias as $key => $paciente) {
			if($key == 0){ //asigno pacienteAcutal, junto con su fecha al primero de la lista.
				$pacienteActual = $paciente->rut_paciente;
				$fechaMinima = $paciente->fecha_cartilla;
				$fechaMinima2 = $paciente->fecha_cartilla2;
				$diferencia=$paciente->cantidad_dias;
				
			}elseif ($pacienteActual != $paciente->rut_paciente) {//cuando encuentre un paciente 
				$pacienteActual = $paciente->rut_paciente;	      //distinto lo asigno como
				$contadorPaciente ++;							  //acutal, cambio la fechaMinima
				$fechaMinima = $paciente->fecha_cartilla;		  //y aumento el contador
				$fechaMinima2 = $paciente->fecha_cartilla2;
				$diferencia=$paciente->cantidad_dias;
				
				$cant_alertas=[];
			}

			$cant_alertas[]=$paciente->cantidad_alertas;;
			$fechaMinimaPorPaciente[$contadorPaciente] = [
				"rutPaciente"    => $pacienteActual,
				"fechaMínima"    => $fechaMinima,
				"fechaMínima2"    => $fechaMinima2,
				"diferencia"     => $diferencia,
				"num_alertas"     => $cant_alertas

			];
			
		}
		

		//obtener la diferencia en días mas grande de los pacientes
		foreach ($fechaMinimaPorPaciente as $key => $paciente) {
			if ( $key == 0 )
				$diferenciaDiasMax = $paciente['diferencia'];
			if( $diferenciaDiasMax < $paciente['diferencia'] )
				$diferenciaDiasMax = $paciente['diferencia'];
		}
		$tipoPaciente = Usuario::obtenerNombreTipoUsuario(Auth::user()->rut);
		$evaluador_2=false;
		if ($tipoPaciente == "evaluador_2")
		{
			$evaluador_2=true;
		}
		//Generar el Head a enviar a datatables
		
		for ($i=1;$i<=$diferenciaDiasMax;$i++){
			if($evaluador_2)
				$colspan     = $colspan.'<th colspan="3"  style="text-align:center;border-style:solid solid none solid;border-width:1px 2px 1px 2px;">Día '.$i.'</th>';
			else $colspan     = $colspan.'<th colspan="2"  style="text-align:center;border-style:solid solid none solid;border-width:1px 2px 1px 2px;">Día '.$i.'</th>';
			if( $i>1 )
			{
				if($evaluador_2)
				{
					$columnaHead = $columnaHead.'<th '.$style_izq.'>Nº cartilla</th>
							             	 <th>Nicturia</th>
							             	 <th '.$style_der.'>Cant. detecciones</th>';
				}
				else
				{
					$columnaHead = $columnaHead.'<th '.$style_izq.'>Nº cartilla</th>
							             	 <th '.$style_der.'>Nicturia</th>';
				}
				
			}
		}
		if($evaluador_2)
		{
			$head = $head.' <tr>
							<th></th>
							<th></th>
							'.$colspan.'

						</tr>
						<tr>
							<th>Rut Paciente</th>
							<th>Fecha de inicio</th>
							<th '.$style_izq.'>Nº cartilla</th>
							<th>Nicturia</th>
							<th '.$style_der.'>Cant. detecciones</th>
							'.$columnaHead.'
							
						</tr>';
		}
		else
		{
			$head = $head.' <tr>
							<th></th>
							<th></th>
							'.$colspan.'
							<th>Informe</th>
						</tr>
						<tr>
							<th>Rut Paciente</th>
							<th>Fecha de inicio</th>
							<th '.$style_izq.'>Nº cartilla</th>
							<th '.$style_der.'>Nicturia</th>
							'.$columnaHead.'
							<th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Administración&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
						</tr>';
		}
		

		
		//Generar el Body a enviar a Datatables
		foreach ($fechaMinimaPorPaciente as $key => $paciente) {
			
			//Voy recorriendo por cada paciente...
			$body = $body.'<tr id="fila'.$paciente['rutPaciente'].'">
						      <th>'.$paciente['rutPaciente']."-".Funciones::obtenerDV($paciente['rutPaciente']).'</th>
						      <th>'.$paciente['fechaMínima2'].'</th>';
			foreach ($dataNicturias as $aux => $detalleCartillaPaciente) {
				if( $detalleCartillaPaciente->rut_paciente == $paciente['rutPaciente'] ){
					$contadorAux++;
					$cantidadDias++;
					$body = $body.'<th '.$style_izq.'>'.$detalleCartillaPaciente->nro_cartilla.'</th>';
				//	//determinar el tipo de paciente ,si no es dos checkbock = disabed.
				//	$tipoPaciente = Encargado::obtenerTipoUsuario($paciente['rutPaciente']);
					$tipoPaciente = Usuario::obtenerNombreTipoUsuario(Auth::user()->rut);
					if ($tipoPaciente != "evaluador_1")
						$disabled = "disabled";
					else
						$disabled = "";
				/*	if ($tipoPaciente != 1)
						$disabled = "disabled";
					else
						$disabled = "";
*/
					
					if($detalleCartillaPaciente->nicturia == false)
						$body = $body.'<th >
										<input type="checkbox" class="chk'.$paciente['rutPaciente'].'" value="'.$detalleCartillaPaciente->nicturia.'" id="check_'.$contadorAux.'_'.$paciente['rutPaciente'].'" name="'.$detalleCartillaPaciente->id_cartilla_micciones.'"  '.$disabled.'></input>
									   </th>';
					else
						$body = $body.'<th>
										<input type="checkbox" class="chk'.$paciente['rutPaciente'].'" value="'.$detalleCartillaPaciente->nicturia.'" id="check_'.$contadorAux.'_'.$paciente['rutPaciente'].'" name="'.$detalleCartillaPaciente->id_cartilla_micciones.'" '.$disabled.' checked></input>
					               	  </th>';
					if($evaluador_2)
					{
						$body.='<th '.$style_der.'>'.($paciente["num_alertas"][$contadorAux-1]?$paciente["num_alertas"][$contadorAux-1]:0).'</th>';
					}
					
				}
			}
			$contadorAux = 0;
			//si el paciente tiene menos dias llenar la tabla con espacios vacios
			if($cantidadDias < $diferenciaDiasMax){
				for ($i=$cantidadDias; $i<$diferenciaDiasMax ; $i++) { 
					
					
					
					if($evaluador_2)
					{
						$body = $body."<th ".$style_izq."></th><th></th>";
						$body.="<th .$style_der.></th>";
					}
					else
					{
						$body = $body."<th ".$style_izq."></th>";
						$body.="<th .$style_der.></th>";
					}
				}
			}
			$cantidadDias = 0;
			if($evaluador_2)
			{
				$body.='</tr>';
			}
			else
			{
				$body = $body.'<th>
						<div class="btnInformeGuardar row">
							<a  class="btn informeNicturia" onCLick="generarInforme('.$paciente['rutPaciente'].')">
								<span class="glyphicon glyphicon-file"></span> 
							Informe</a>
			
							<a onCLick="guardarNicturia('.$paciente['rutPaciente'].')" class="btn informeNicturia2"  value="'.$paciente['rutPaciente'].'">
								<span class="glyphicon glyphicon-floppy-disk"></span> 
							Guardar</a>
						</div>
					</th>
				</tr>';
			}
		}
		return Response::json([$maxDias, $dataNicturias,$fechaMinimaPorPaciente,$head,$body]);
	}

	public function guardarInformeNicturia() {
		try{
        	DB::beginTransaction();

            $data = Input::get("data");
            foreach ($data as $cartilla) {
            	$id_cartilla_micciones = $cartilla['id_cartilla_micciones'];
            	$checked               = $cartilla['checked'];
            	$cartilla_micciones = CartillaMicciones::where("id_cartilla_micciones", "=", $id_cartilla_micciones)->first();

	            if($cartilla_micciones != null) { 
	                $cartilla_micciones->nicturia = $checked;
	                $cartilla_micciones->save();
	            }
	    
	             
            }

            DB::commit();

            return Response::json(["exito" => "Los datos han sido guardados","data"=>$data]);
        } catch(Exception $ex){
            DB::rollback();
            return Response::json(["error" => "Error al guardar los datos", "msg" => $ex->getMessage()]);
        }
	}

	public function generarInformePDF() {

		try{
        	DB::beginTransaction();

            $rut_paciente = Input::get("rut_pacientes");
            $nombre_paciente="";
            $apellido_pa="";
            $apellido_ma="";
            $sexo="";
            $fechas=array();
            $respuestaP = Input::get("respuestaP");
            $rut_usuario=Auth::user()->rut;
			$usuario=Usuario::obtenerDatosUsuario($rut_usuario);
			$NombreUsuario=$usuario['nombre']." ".$usuario['paterno']." ".$usuario['materno'];
            $rut_usuario=$rut_usuario."-".Funciones::obtenerDV($rut_usuario);
            $contador=0;


        $cartilla=DB::table( DB::raw(
             "(select ca.id_cartilla_micciones,ca.fecha_cartilla,ca.rut_paciente,u.nombres,u.apellido_paterno,u.apellido_materno,p.genero from cartilla_micciones as ca,paciente as p,usuario as u
               where p.rut=$rut_paciente and ca.rut_paciente=p.rut and u.rut=p.rut and ca.rut_paciente=u.rut order by ca.fecha_cartilla asc) as re"
         ))->get();

        foreach ($cartilla as $datosCartilla) {
        	$nombre_paciente=$datosCartilla->nombres;
            $apellido_pa=$datosCartilla->apellido_paterno;
            $apellido_ma=$datosCartilla->apellido_materno;
            $sexo=$datosCartilla->genero;
            $fechas[]=$datosCartilla->fecha_cartilla;
            $contador++;
        }
       $fecha_inicial=$fechas[0];
       $fecha_final=$fechas[$contador-1];

        $nombre_paciente=$nombre_paciente." ".$apellido_pa." ".$apellido_ma;
        $rut_paciente=$rut_paciente."-".Funciones::obtenerDV($rut_paciente);
        DB::commit();

        return Response::json(["exito" => "Los datos han sido guardados","rut_paciente"=>$rut_paciente,"rut_usuario"=>$rut_usuario,"NombreUsuario"=>$NombreUsuario
        	  ,"nombre_paciente"=>$nombre_paciente,"fecha_inicial"=>$fecha_inicial,"fecha_final"=>$fecha_final,"genero_paciente"=>$sexo]);

        } catch(Exception $ex){
            DB::rollback();
            return Response::json(["error" => "Error al generar documento", "msg" => $ex->getMessage()]);
        }

	}
	public function guardarRespuestaInformeNicturia()
	{
		$rut_paciente=Input::get("rut");
		$rut_usuario=Auth::user()->rut;
		$tiene_nicturia=Input::get("tiene_nicturia");
		$fecha=date("Y-m-d H:i:s");
		$tiene_nicturia=($tiene_nicturia=="si"?true:false);
	
		
		try{
			$informe=InfNicturia::where("rut_paciente","=",$rut_paciente)->where("rut_usuario","=",$rut_usuario)->first();
			
			if(!$informe)
			{
				$informe=new InfNicturia;
			}
			
			$informe->rut_usuario=$rut_usuario;
			$informe->rut_paciente=$rut_paciente;
			$informe->tiene_nicturia=$tiene_nicturia;
			$informe->fecha_informe=$fecha;
			$informe->save();
		}catch(Exception $e){
			return Response::json(
				[
					"error"=>$e->getMessage().", línea:".$e->getLine().", archivo:".$e->getFile()
				]
				);
		}
		return Response::json(
				[
					"exito"=>"Se ha guardado correctamente"
				]
				); 
	}
	public function cargarDatosInformeNicturia() //relacionada a ↑
	{
		$usuario=Input::get("usuario");
		if($usuario==null)
			return Response::json(array());
		$resultado=DB::select(
			DB::raw(
				"
				SELECT 
				paciente.rut AS rut,
				(
				COALESCE((usuario.nombres), ' ')
				||' '||
				COALESCE((usuario.apellido_paterno), ' ')
				||' '||
				COALESCE((usuario.apellido_materno),' ')
				)AS nombre_completo,
				informe_nicturia.tiene_nicturia
				FROM
				paciente
				LEFT JOIN informe_nicturia ON informe_nicturia.rut_paciente=paciente.rut
				INNER JOIN evento ON evento.id_evento=paciente.id_evento
				INNER JOIN usuario ON usuario.rut=paciente.rut
				WHERE 
				evento.nombre_evento='Nicturia'
				AND 
				(informe_nicturia.rut_usuario=$usuario) and 
				paciente_visible=TRUE
				UNION
				SELECT p.rut  AS rut,
				(
				COALESCE((usuario.nombres), ' ')
				||' '||
				COALESCE((usuario.apellido_paterno), ' ')
				||' '||
				COALESCE((usuario.apellido_materno),' ')
				)AS nombre_completo,
				NULL as tiene_nicturia
				from paciente p
				INNER JOIN evento ON evento.id_evento=p.id_evento
				INNER JOIN usuario ON usuario.rut=p.rut
				WHERE 
				evento.nombre_evento='Nicturia' and 
				p.rut not in (select rut_paciente from informe_nicturia where rut_usuario=$usuario)
				and paciente_visible=TRUE
				ORDER BY rut
				"
				)
			);
		
		return Response::json($resultado);
	}
	public function cargarUsuarios()
	{
		$tipoUsuario=Input::get("tipo");
		$resultado=DB::select(
			DB::raw(
				"
				SELECT
				usuario.rut,
				(usuario.nombres||' '||usuario.apellido_paterno||' '||usuario.apellido_materno)AS nombre
				FROM usuario
				INNER JOIN encargado ON usuario.rut=encargado.rut
				INNER JOIN tipo_usuario ON tipo_usuario.id_tipo_usuario=encargado.id_tipo_usuario
				WHERE tipo_usuario.tipo='$tipoUsuario'
				AND visible=TRUE
				"
				)
			);
		
		return Response::json($resultado);
	}
	public function procesarDatosTabla1()
	{
		return self::procesarDatosTabla1Falso();
	//	return;
	/*	$usuario=Auth::user()->rut;
		$resultado=DB::select(
			DB::raw(
				"
				SELECT
				cartilla.rut_paciente,
				cartilla.fecha_cartilla,
				TO_CHAR(cartilla.fecha_cartilla,'DD-MM-YYYY')AS fecha_cartilla2,
				cartilla.nro_cartilla,
				cartilla.id_cartilla_micciones,
				cartilla.nicturia,
				(
					SELECT numero_alertas FROM configuracion_cartilla_micciones
					WHERE cartilla.id_cartilla_micciones=configuracion_cartilla_micciones.id_cartilla_micciones
					AND rut_usuario=$usuario
				)AS cantidad_alertas,
				(
					SELECT COUNT(*) from cartilla_micciones
					WHERE rut_paciente=cartilla.rut_paciente
					AND cartilla_visible=TRUE
				)AS cantidad_dias
				FROM
				(	SELECT 
					mc.rut_paciente,
					mc.fecha_cartilla,
					mc.id_cartilla_micciones,
					mc.nicturia,
					(
						SELECT COUNT(id_detalle_cartilla_micciones)
						FROM detalle_cartilla_micciones
						WHERE id_cartilla_micciones = mc.id_cartilla_micciones
						AND mc.cartilla_visible=TRUE
					) AS nro_cartilla
					FROM cartilla_micciones mc
					WHERE mc.cartilla_visible=TRUE
				) cartilla
				
				ORDER BY cartilla.rut_paciente,
				cartilla.fecha_cartilla
				"
				)
			);
		$paciente="";
		$numeroPaciente=0;
		$dia="";
		$numeroDia=0;
		
		$informe=new PHPExcel();
		$objWorksheet = $informe->getActiveSheet();
		$objWorksheet->setTitle("Tabla 1");
		$data=array(
				array("Paciente/Día","nEp(c)","nE(s)")
			);
		foreach($resultado as $dato)
		{
			if($paciente!=$dato->rut_paciente)
			{
				$paciente=$dato->rut_paciente;
				$numeroPaciente++;
				$numeroDia=0;
			}
			if($dia!=$dato->fecha_cartilla)
			{
				$dia=$dato->fecha_cartilla;
				$numeroDia++;
			}
			
			$a=array(
					"P$numeroPaciente D$numeroDia",
					$dato->nicturia?$dato->nro_cartilla:null,
					$dato->nicturia?(is_null($dato->cantidad_alertas)?"0":$dato->cantidad_alertas):null
				);
			$data[]=$a;
		}
		$objWorksheet->fromArray(
				$data,  // The data to set
				NULL,        // Array values with this value will not be set
				'A1'         // Top left coordinate of the worksheet range where
                     //    we want to set these values (default is A1)
                );
        
        $objWriter = new PHPExcel_Writer_Excel2007($informe);

		$objWriter->setOffice2003Compatibility(true);
		$objWriter->setIncludeCharts(TRUE);
		$objWriter->save("public/files/reporte.xlsx");
		
		return Response::download("public/files/reporte.xlsx");*/
	}
	public function procesarDatosTabla1Falso($retornar=false)
	{
		$pacientes=DB::select(
			DB::raw(
				"
		--		SELECT
		--		DISTINCT(rut_paciente)
		--		FROM cartilla_micciones
		--		ORDER BY rut_paciente
				SELECT c.rut_paciente
				FROM contacto AS c
				INNER JOIN paciente AS p ON p.rut=c.rut_paciente
				INNER JOIN evento AS e ON e.id_evento=p.id_evento
				WHERE e.nombre_evento='Nicturia'
				AND c.rut_encargado=".Auth::user()->rut."
				ORDER BY c.rut_paciente
				"
				)
			);
		$paciente_rut="";
		$numeroPaciente=0;
		$dia="";
		$numeroDia=0;
		$datos_retorno=array();
		if(!$retornar)
		{
			$informe=new PHPExcel();
			$objWorksheet = $informe->getActiveSheet();
			$objWorksheet->setTitle("Tabla 1");
		}
		$dataexcel=array(
				array("Paciente/Día","nEp(c)","nE(s)","nE(u)")
			);
		foreach($pacientes as $paciente)
		{
			//echo $paciente->rut_paciente."<br>";
			
			if(empty($paciente->rut_paciente))
				continue;
			$configuracion=(ConfiguracionCartillaMiccionesController::cargarConfiguracionMicciones($paciente->rut_paciente));
			
			if(isset($configuracion->getData()->error))
			{
				//echo $configuracion->getData()->error."<br>";
				continue;
			}
			if(isset($configuracion->getData()->minutos))
			{
				//echo "Hay configuracion<br>";
				$datos=self::obtenerDatos($paciente->rut_paciente,$configuracion->getData()->minutos);
				
				$cartilla=new Cartilla();
				$fecha_inicial="";
				$fecha_final="";
				$fechas=$datos["fechas_cartilla_proceso"];
				$data=$datos["filtro"];
				$fecha_anterior="";
				$tabla=array();
				if(!$fechas[0])
					return;
				
				$fecha_inicial=$configuracion->getData()->fecha_inicio;
					
				$fecha_final=$configuracion->getData()->fecha_final;
			 
			
				
				$contadores=[new Contador(1),
							 new Contador(2)];
				
				for($i=0;$i<count($fechas);$i++)
				{
					
					if(!$cartilla->contieneFecha($fechas[$i]->fecha_cartilla))
					{
						$fec=new FechaCartilla($fechas[$i]->fecha_cartilla);
						$fec->fecha_real=$fechas[$i]->fecha_real;
						$fec->agregarHora(new Hora($fechas[$i]->horario));
						$cartilla->agregarFecha($fec);
						
					}
					else
					{
						$cartilla->agregarHora($fechas[$i]->fecha_cartilla,new Hora($fechas[$i]->horario));
					}
					
					for($j=0;$j<count($data);$j++)
					{
						
						if($data[$j]->fecha_cartilla==$fechas[$i]->fecha_cartilla)
							$cartilla->asignarSensor($data[$j]->fecha_cartilla,$data[$j]->fecha_hora_cartilla,$data[$j]);
							
					}
					
					
				}
				for($j=0;$j<count($data);$j++)
				{
					
					$contadores[$data[$j]->id_configuracion_nicturia-1]->agregarValor($data[$j]->id_cartilla_micciones);	
				}
				
				$cartilla->ordenarSensores();
				
				for($i=0;$i<count($cartilla->fechas);$i++)
				{
					$tabla[]=array(
						$i+1,
							$cartilla->fechas[$i]->fecha,
							"",
							"",
							"",
							""
						);
					$horas=$cartilla->fechas[$i]->horas;
					for($j=0;$j<count($horas);$j++)
					{
						$tabla[]=array(
							
							"",
							$cartilla->fechas[$i]->fecha." ".$horas[$j]->hora,
							"",
							"",
							"",
							""
							
							);
						
						for($k=0;$k<count($horas[$j]->sensor1);$k++)
						{
							$cercano1=($horas[$j]->sensor1[$k]->cercano?true:false);
							$cercano2=($horas[$j]->sensor2[$k]->cercano?true:false);
							$cercanou1=($horas[$j]->sensor1[$k]->cercanou?true:false);
							$cercanou2=($horas[$j]->sensor2[$k]->cercanou?true:false);
							$tabla[]=array(
								
								"",
								"",
								($cercano1?"<span>":"").
								($horas[$j]->sensor1[$k]->fecha_sensor?$horas[$j]->sensor1[$k]->fecha_sensor:"")
								.($cercano1?" <span class='glyphicon glyphicon-ok'></span></span>":"")
								,
								!$horas[$j]->sensor1[$k]->fecha_sensor?"":($horas[$j]->sensor1[$k]->ultrasonido_sensor?(($cercanou1?"<span>1 <span class='glyphicon glyphicon-ok'></span></span>":"1")):"0"),
								($cercano2?"<span>":"").
								($horas[$j]->sensor2[$k]->fecha_sensor?$horas[$j]->sensor2[$k]->fecha_sensor:"")
								.($cercano2?" <span class='glyphicon glyphicon-ok'></span></span>":"")
								,
								!$horas[$j]->sensor2[$k]->fecha_sensor?"":($horas[$j]->sensor2[$k]->ultrasonido_sensor?(($cercanou2?"<span>1 <span class='glyphicon glyphicon-ok'></span></span>":"1")):"0")
								
								
								);
						}
					}
				}
				$cartilla->calcularPorcentaje($fecha_inicial,$fecha_final);
				//$tabla.draw();
				$con_datos=true;
				/*$("#guardar_configuracion").prop("disabled",!($("[name=intervalo]").val()!=""&&con_datos));
				*/
				
				self::mostrarTabla($tabla);
				
				$cartilla->mostrarGrupos($contadores);
				
				
			//	$datosProcesados=self::datosFinales($contadores[$configuracion->getData()->sensor-1]);
				$datosProcesados=self::datosFinales($contadores[1]);
				
				foreach($datosProcesados as $dato)
				{
			//		echo $paciente->rut_paciente." ".$dato["fecha"]."<br>";
					if($paciente->rut_paciente=="")
						continue;
					if($paciente_rut!=$paciente->rut_paciente)
					{
						$paciente_rut=$paciente->rut_paciente;
						$numeroPaciente++;
						$numeroDia=0;
					}
					$numeroDia++;
					$r=DB::select(
						DB::raw(
							"
							SELECT
							nicturia
							FROM cartilla_micciones
							WHERE cartilla_visible=TRUE
							AND fecha_cartilla='".$dato["fecha"]."'
							AND rut_paciente=".$paciente->rut_paciente."
							"
							)
						);
					$r2=DB::select(
						DB::raw(
							"
							SELECT
							(
								SELECT COUNT(*) FROM detalle_cartilla_micciones
								WHERE cartilla_micciones.id_cartilla_micciones=detalle_cartilla_micciones.id_cartilla_micciones
							)AS cantidad
							FROM cartilla_micciones
							
							WHERE cartilla_visible=TRUE
							AND fecha_cartilla='".$dato["fecha"]."'
							AND rut_paciente=".$paciente->rut_paciente."
							AND nicturia=TRUE
							"
							)
						);
					$nicturia=$r?$r[0]->nicturia:false;
					$cant_cartilla=$r2?$r2[0]->cantidad:"0";
					
					$a=array(
							"P$numeroPaciente D$numeroDia",
							$nicturia?$cant_cartilla:null,//cantidad cartilla
							
						//	$nicturia?(empty($dato["cantidad"])?"0":$dato["cantidad"]):null,//cantidad sensor
						//	$nicturia?(is_null($dato["cantidad_u"])?"0":$dato["cantidad_u"]):null//cantidad sensor
							(empty($dato["cantidad"])?"0":$dato["cantidad"]),//cantidad sensor
							(is_null($dato["cantidad_u"])?"0":$dato["cantidad_u"])//cantidad sensor

							
						);
					$b=array(
						$paciente->rut_paciente,
						$nicturia?(empty($dato["cantidad"])?"0":$dato["cantidad"]):null
						
						);
					unset($r);
					unset($r2);
					$dataexcel[]=$a;
					$datos_retorno[]=$b;
				}
				$numeroDia=0;
				if(!$retornar)
				{
					$objWorksheet->fromArray(
						$dataexcel,  // The data to set
						NULL,        // Array values with this value will not be set
						'A1'         // Top left coordinate of the worksheet range where
							 //    we want to set these values (default is A1)
						);
				}
				
				
				
		
				
				
				
				
			}
			
		}
		if($retornar)
		{
			return $datos_retorno;
		}
		
		$objWriter = new PHPExcel_Writer_Excel2007($informe);
		$objWriter->setOffice2003Compatibility(true);
		$objWriter->setIncludeCharts(TRUE);
		$objWriter->save("/var/www/html/eHomeseniors/public/files/tabla1.xlsx");
		chmod("/var/www/html/eHomeseniors/public/files/tabla1.xlsx",0777);
		return Response::download("/var/www/html/eHomeseniors/public/files/tabla1.xlsx");
		
	}
	static function mostrarTabla($tabla)
	{
	//	echo "<table>";
		foreach($tabla as $fila)
		{
	//		echo "<tr>";
			foreach($fila as $celda)
			{
	//			echo "<td style='border-width:1px;border-style:solid;'>$celda</td>";
			}
	//		echo "</tr>";
		}
	//	echo "</table>";
	}
	static function datosFinales($contador)
	{
		$datos=array();
		$datos_grupos=array();
		$datos_grupos=$contador->lista_grupo;
		
		$datosJsonGrupo=array();
	//	echo "------------------------";
		for($i=0;$i<count($datos_grupos);$i++)
		{
			$datosJsonGrupo[]=array(
					
						"id_cartilla_micciones"=>$datos_grupos[$i],
						"cantidad"=>$datos_grupos[$i][0],
						"cantidad_u"=>$datos_grupos[$i][1],
						"fecha"=>$datos_grupos[$i][2],
						"a"=>"a"
						
				);
	//		echo "cantidad:".$datos_grupos[$i][0];
	//		echo "<br>";
	//		echo "fecha:".$datos_grupos[$i][2];
	//		echo "<br>";
		}
		
		if(count($datos_grupos)==0)
		{
			$datosJsonGrupo[]=array(
					
						"id_cartilla_micciones"=>null,
						"cantidad"=>-1,
						"cantidad_u"=>-1,
						"fecha"=>"",
						"v"=>"v"
						
				);
		}
		
		//echo "<table>";
		//echo "<caption>Grupos</caption>";
		$num=0;
	/*	foreach($datosJsonGrupo as $data)
		{
			//echo "<tr>";
			//echo "<td>$num</td>";
			$num++;
			foreach($data as $celdas)
			{
				
				//echo "<td style='border-width:1px;border-style:solid;'>$celdas</td>";
			}
			//echo "</tr>";
		}
		//echo "</table>";*/
		
		return $datosJsonGrupo;
	/*	
		for($i=0;$i<count($contadores);$i++)
		{
			$datos[]=$contadores[$i]->lista;
			
		}
		
		$datosJson=array();
		for($i=0;$i<count($datos[0]);$i++)
		{
			$datosJson[]=array(
					
						"id_cartilla_micciones"=>$datos[0][$i][0],
						"cantidad"=>$datos[0][$i][1]
						
				);
		}
		for($i=0;$i<count($datos[1]);$i++)
		{
			$datosJson[]=array(
					
						"id_cartilla_micciones"=>$datos[1][$i][0],
						"cantidad"=>$datos[1][$i][1]
				);
		}
		if(count($datos[0])==0)
		{
			$datosJson[]=array(
					
						"id_cartilla_micciones"=>null,
						"cantidad"=>0
					/*	id_electrodo:electrodo,
						minutos:intervalo,
						fecha_inicio:$("[name=fecha_inicial]").val(),
						fecha_final:$("[name=fecha_final]").val()*/
	/*				
				);
		}
		if(count($datos[1])==0)
		{
			$datosJson[]=array(
					
						"id_cartilla_micciones"=>null,
						"cantidad"=>0
					/*	id_electrodo:electrodo,
						minutos:intervalo,
						fecha_inicio:$("[name=fecha_inicial]").val(),
						fecha_final:$("[name=fecha_final]").val()*/
					
	/*			);
		}
		//echo "<table>";
		foreach($datosJson as $data)
		{
			//echo "<tr>";
			foreach($data as $celdas)
			{
				
				//echo "<td style='border-width:1px;border-style:solid;'>$celdas</td>";
			}
			//echo "</tr>";
		}
		//echo "</table>";*/
	}
	private function existePacienteArray($rut,$array,$cantidad/*en palabras*/)
	{
		foreach($array as $dato)
		{
			
			if($dato->$cantidad==$rut)
				return true;
		}
		return false;
	}
	private function existeSensorArray($rut,$array,$cantidad/*numero*/)
	{
		foreach($array as $dato)
		{
			
			if($dato[0]==$rut)
			{
				if($cantidad<3)
				{
					if($dato[1]==$cantidad)
						return true;
				}
				else
				{
					if($dato[1]>=$cantidad)
						return true;
				}
					
			}
				
		}
		return false;
	}
	public function procesarDatosTabla2()
	{
		$datos_sensor=$this->procesarDatosTabla1Falso(true);
		
		$datos_pacientes=DB::select(
			DB::raw(
				"
				SELECT *
				FROM info_pacientes_nicturia_vista
				WHERE rut_paciente IN
				(

					SELECT c.rut_paciente
					FROM contacto AS c
					INNER JOIN paciente AS p ON p.rut=c.rut_paciente
					INNER JOIN evento AS e ON e.id_evento=p.id_evento
					INNER JOIN cartilla_micciones AS cm ON cm.rut_paciente=c.rut_paciente
					INNER JOIN resultado_micciones AS r ON r.rut_paciente=c.rut_paciente
					WHERE e.nombre_evento='Nicturia'
					AND c.rut_encargado=".Auth::user()->rut."
					AND p.paciente_visible=TRUE
					ORDER BY c.rut_paciente
				)
				ORDER BY rut_paciente
				"
				)
			);
		$contador=1;
		$datosexcel[]=array(
			"Paciente",
			"Edad",
			"Género",
			"Caídas previas",
			"Caídas transtorno marcha",
			"Polifarmacia",
			"Incontinencia urinaria", 
			"Incontinencia fecal",
			"Deterioro cognitivo",
			"Delirium",
			"Transtorno de ánimo",
			"Transtorno de sueño",
			"Malnutricion",
			"Déficit sensorial",
			"Inmovilidad",
			"Úlcera por presión",
			"Puntaje test charlson",
			"Puntaje índice barthel", 
			"Puntaje test iciq",
			"Nc=1",
			"Ns=1",
			"Nc=2",
			"Ns=2",
			"Nc≥3",
			"Ns≥3"
			);
		$datos_cartilla=DB::select(
				DB::raw(
					"
					SELECT
						UNNEST(
							ARRAY(
								SELECT 
								DISTINCT(rut_paciente)
								FROM
								(
									SELECT rut_paciente,
									(
										SELECT COUNT(*)
										FROM detalle_cartilla_micciones AS dcm
										WHERE cm.id_cartilla_micciones=dcm.id_cartilla_micciones
									)AS cantidad
									FROM cartilla_micciones AS cm
									WHERE cm.nicturia=TRUE
									AND cm.cartilla_visible=TRUE
									AND cm.rut_paciente IN
									(
										SELECT c.rut_paciente
										FROM contacto AS c
										INNER JOIN paciente AS p ON p.rut=c.rut_paciente
										INNER JOIN evento AS e ON e.id_evento=p.id_evento
										WHERE e.nombre_evento='Nicturia'
										AND c.rut_encargado=".Auth::user()->rut."
										AND p.paciente_visible=TRUE
										ORDER BY c.rut_paciente
									)
					
								)AS datos
					
								WHERE cantidad=1
								ORDER BY rut_paciente
					
							)
						)AS uno,
						UNNEST(	
							ARRAY(
								SELECT 
								DISTINCT(rut_paciente)
								FROM
								(
									SELECT rut_paciente,
									(
										SELECT COUNT(*)
										FROM detalle_cartilla_micciones AS dcm
										WHERE cm.id_cartilla_micciones=dcm.id_cartilla_micciones
									)AS cantidad
									FROM cartilla_micciones AS cm
									WHERE cm.nicturia=TRUE
									AND cm.cartilla_visible=TRUE
									AND cm.rut_paciente IN
									(
										SELECT c.rut_paciente
										FROM contacto AS c
										INNER JOIN paciente AS p ON p.rut=c.rut_paciente
										INNER JOIN evento AS e ON e.id_evento=p.id_evento
										WHERE e.nombre_evento='Nicturia'
										AND c.rut_encargado=".Auth::user()->rut."
										AND p.paciente_visible=TRUE
										ORDER BY c.rut_paciente
									)
					
								)AS datos
					
								WHERE cantidad=2
								ORDER BY rut_paciente
					
							)
						)AS dos,
						UNNEST(	
							ARRAY(
								SELECT 
								DISTINCT(rut_paciente)
								FROM
								(
									SELECT rut_paciente,
									(
										SELECT COUNT(*)
										FROM detalle_cartilla_micciones AS dcm
										WHERE cm.id_cartilla_micciones=dcm.id_cartilla_micciones
									)AS cantidad
									FROM cartilla_micciones AS cm
									WHERE cm.nicturia=TRUE
									AND cm.cartilla_visible=TRUE
									AND cm.rut_paciente IN
									(
										SELECT c.rut_paciente
										FROM contacto AS c
										INNER JOIN paciente AS p ON p.rut=c.rut_paciente
										INNER JOIN evento AS e ON e.id_evento=p.id_evento
										WHERE e.nombre_evento='Nicturia'
										AND c.rut_encargado=".Auth::user()->rut."
										AND p.paciente_visible=TRUE
										ORDER BY c.rut_paciente
									)
					
								)AS datos
					
								WHERE cantidad>=3
								ORDER BY rut_paciente
					
							)
						)AS tres
					

					"
					)
			);
		
		foreach($datos_pacientes as $dato_paciente)
		{
		
			$datosexcel[]=array(
				"P$contador",
				$dato_paciente->edad,
				$dato_paciente->genero,
				$dato_paciente->caidas_previas,
				(is_null($dato_paciente->caidas_transtorno_marcha)?"Sin datos":($dato_paciente->caidas_transtorno_marcha?"Sí":"No")),
				(is_null($dato_paciente->polifarmacia)?"Sin datos":($dato_paciente->polifarmacia?"Sí":"No")),
				(is_null($dato_paciente->incontinencia_urinaria)?"Sin datos":($dato_paciente->incontinencia_urinaria?"Sí":"No")),
				(is_null($dato_paciente->incontinencia_fecal)?"Sin datos":($dato_paciente->incontinencia_fecal?"Sí":"No")),
				(is_null($dato_paciente->deterioro_cognitivo)?"Sin datos":($dato_paciente->deterioro_cognitivo?"Sí":"No")),
				(is_null($dato_paciente->delirium)?"Sin datos":($dato_paciente->delirium?"Sí":"No")),
				(is_null($dato_paciente->transtorno_animo)?"Sin datos":($dato_paciente->transtorno_animo?"Sí":"No")),
				(is_null($dato_paciente->transtorno_sueno)?"Sin datos":($dato_paciente->transtorno_sueno?"Sí":"No")),
				(is_null($dato_paciente->malnutricion)?"Sin datos":($dato_paciente->malnutricion?"Sí":"No")),
				(is_null($dato_paciente->deficit_sensorial)?"Sin datos":($dato_paciente->deficit_sensorial?"Sí":"No")),
				(is_null($dato_paciente->inmovilidad)?"Sin datos":($dato_paciente->inmovilidad?"Sí":"No")),
				(is_null($dato_paciente->ulcera_por_presion)?"Sin datos":($dato_paciente->ulcera_por_presion?"Sí":"No")),
				(is_null($dato_paciente->puntaje_test_charlson)?"Sin datos":(string)$dato_paciente->puntaje_test_charlson),
				(is_null($dato_paciente->puntaje_indice_barthel)?"Sin datos":(string)$dato_paciente->puntaje_indice_barthel),
				(is_null($dato_paciente->puntaje_test_iciq)?"Sin datos":(string)$dato_paciente->puntaje_test_iciq),
				$this->existePacienteArray($dato_paciente->rut_paciente,$datos_cartilla,"uno")?"1":"0",
				$this->existeSensorArray($dato_paciente->rut_paciente,$datos_sensor,1)?"1":"0",
				$this->existePacienteArray($dato_paciente->rut_paciente,$datos_cartilla,"dos")?"1":"0",
				$this->existeSensorArray($dato_paciente->rut_paciente,$datos_sensor,2)?"1":"0",
				$this->existePacienteArray($dato_paciente->rut_paciente,$datos_cartilla,"tres")?"1":"0",
				$this->existeSensorArray($dato_paciente->rut_paciente,$datos_sensor,3)?"1":"0"
				
				);
			$contador++;
		}
		$informe=new PHPExcel();
		$objWorksheet = $informe->getActiveSheet();
		//$objWorksheet->getDefaultRowDimension()->setRowHeight(80);
		//$objWorksheet->getDefaultColumnDimension()->setWidth(20);
	// 	$objWorksheet->getColumnDimension('B')->setWidth(50);
		$objWorksheet->setTitle("Tabla 2");
		
		$objWorksheet->fromArray(
						$datosexcel,  // The data to set
						NULL,        // Array values with this value will not be set
						'A1'         // Top left coordinate of the worksheet range where
							 //    we want to set these values (default is A1)
						);
		
		$objWriter = new PHPExcel_Writer_Excel2007($informe);
		$objWriter->setOffice2003Compatibility(true);
		$objWriter->setIncludeCharts(TRUE);
		$objWriter->save("/var/www/html/eHomeseniors/public/files/tabla2.xlsx");
		chmod("/var/www/html/eHomeseniors/public/files/tabla2.xlsx",0777);
		return Response::download("/var/www/html/eHomeseniors/public/files/tabla2.xlsx");
	
		
		
	}
	public function procesarDatosAccionesRepetidas()
	{
		$respuesta=array();
		$datos_pacientes=DB::select(
			DB::raw(
				"
				SELECT *
				FROM info_pacientes_acciones_repetidas_vista
				WHERE rut_paciente IN
				(
					SELECT c.rut_paciente
					FROM contacto AS c
					INNER JOIN paciente AS p ON p.rut=c.rut_paciente
					INNER JOIN evento AS e ON e.id_evento=p.id_evento
					WHERE e.id_evento=2
					AND c.rut_encargado=".Auth::user()->rut."
					AND p.paciente_visible=TRUE
					ORDER BY c.rut_paciente
				)
				ORDER BY rut_paciente
				"
				)
			);
		for($i=0;$i<count($datos_pacientes);$i++)
		{
			$paciente=$datos_pacientes[$i];
			$respuesta["encuestas"][$i]["rut"]=$paciente->rut_paciente;
			$respuesta["encuestas"][$i]["edad"]=$paciente->edad;
			$respuesta["encuestas"][$i]["genero"]=$paciente->genero;
			$respuesta["encuestas"][$i]["ubicacion_sensor1"]=$paciente->ubicacion_sensor1;
			$respuesta["encuestas"][$i]["ubicacion_sensor2"]=$paciente->ubicacion_sensor2;
			$respuesta["encuestas"][$i]["ubicacion_sensor3"]=$paciente->ubicacion_sensor3;
			$respuesta["encuestas"][$i]["nombre"]=$paciente->nombre;
			$respuesta["encuestas"][$i]["caidas_previas"]=$paciente->caidas_previas;
			$respuesta["encuestas"][$i]["caidas_transtorno_marcha"]=$paciente->caidas_transtorno_marcha;
			$respuesta["encuestas"][$i]["polifarmacia"]=$paciente->polifarmacia;
			$respuesta["encuestas"][$i]["incontinencia_urinaria"]=$paciente->incontinencia_urinaria;
			$respuesta["encuestas"][$i]["incontinencia_fecal"]=$paciente->incontinencia_fecal;
			$respuesta["encuestas"][$i]["deterioro_cognitivo"]=$paciente->deterioro_cognitivo;
			$respuesta["encuestas"][$i]["delirium"]=$paciente->delirium;
			$respuesta["encuestas"][$i]["transtorno_animo"]=$paciente->transtorno_animo;
			$respuesta["encuestas"][$i]["transtorno_sueno"]=$paciente->transtorno_sueno;
			$respuesta["encuestas"][$i]["malnutricion"]=$paciente->malnutricion;
			$respuesta["encuestas"][$i]["deficit_sensorial"]=$paciente->deficit_sensorial;
			$respuesta["encuestas"][$i]["inmovilidad"]=$paciente->inmovilidad;
			$respuesta["encuestas"][$i]["ulcera_por_presion"]=$paciente->ulcera_por_presion;
			$respuesta["encuestas"][$i]["puntaje_test_charlson"]=$paciente->puntaje_test_charlson;
			$respuesta["encuestas"][$i]["puntaje_indice_barthel"]=$paciente->puntaje_indice_barthel;
			$respuesta["encuestas"][$i]["puntaje_indice_lawton"]=$paciente->puntaje_indice_lawton;
			$respuesta["encuestas"][$i]["puntaje_gds"]=$paciente->puntaje_gds;
			$respuesta["encuestas"][$i]["npi_conducta_anomala"]=$paciente->npi_conducta_anomala;
			$respuesta["encuestas"][$i]["npi_frecuencia"]=$paciente->npi_frecuencia;
			$respuesta["encuestas"][$i]["npi_gravedad"]=$paciente->npi_gravedad;
			$respuesta["encuestas"][$i]["npi_angustia"]=$paciente->npi_angustia;
			$respuesta["encuestas"][$i]["puntaje_mmse"]=$paciente->puntaje_mmse;
			
		}
		
		$ubicaciones=DB::select(
			DB::raw(
				"
				SELECT 
				DISTINCT(ubicacion) 
				FROM ubicacion_sensor AS us
				INNER JOIN sensor AS s ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
				INNER JOIN paciente AS p ON p.rut=s.rut
				WHERE p.id_evento=2
				"
				)
			);
		$categorias=array(
			"00:00",
			"01:00",
			"02:00",
			"03:00",
			"04:00",
			"05:00",
			"06:00",
			"07:00",
			"08:00",
			"09:00",
			"10:00",
			"11:00",
			"12:00",
			"13:00",
			"14:00",
			"15:00",
			"16:00",
			"17:00",
			"18:00",
			"19:00",
			"20:00",
			"21:00",
			"22:00",
			"23:00"
			
			);
		$promedios=array();
		$desviaciones=array();
		$cantidades=array();
		
		foreach($ubicaciones as $ub)
		{
			$ubicacion=$ub->ubicacion;
			$promedio=ResumenAccionesRepetidasController::obtenerPromedioDesviacionTotal($ubicacion);
			$cantidad_personas=ResumenAccionesRepetidasController::obtenerCantidadPersonasPorHora($ubicacion);
			if($promedio)
			{
				$array_promedios=array(
					$promedio[0]->promedio_00,
					$promedio[0]->promedio_01,
					$promedio[0]->promedio_02,
					$promedio[0]->promedio_03,
					$promedio[0]->promedio_04,
					$promedio[0]->promedio_05,
					$promedio[0]->promedio_06,
					$promedio[0]->promedio_07,
					$promedio[0]->promedio_08,
					$promedio[0]->promedio_09,
					$promedio[0]->promedio_10,
					$promedio[0]->promedio_11,
					$promedio[0]->promedio_12,
					$promedio[0]->promedio_13,
					$promedio[0]->promedio_14,
					$promedio[0]->promedio_15,
					$promedio[0]->promedio_16,
					$promedio[0]->promedio_17,
					$promedio[0]->promedio_18,
					$promedio[0]->promedio_19,
					$promedio[0]->promedio_20,
					$promedio[0]->promedio_21,
					$promedio[0]->promedio_22,
					$promedio[0]->promedio_23
					);
				$array_acumulados=array(
					array_sum(array_slice($array_promedios,0,1)),
					array_sum(array_slice($array_promedios,0,2)),
					array_sum(array_slice($array_promedios,0,3)),
					array_sum(array_slice($array_promedios,0,4)),
					array_sum(array_slice($array_promedios,0,5)),
					array_sum(array_slice($array_promedios,0,6)),
					array_sum(array_slice($array_promedios,0,7)),
					array_sum(array_slice($array_promedios,0,8)),
					array_sum(array_slice($array_promedios,0,9)),
					array_sum(array_slice($array_promedios,0,10)),
					array_sum(array_slice($array_promedios,0,11)),
					array_sum(array_slice($array_promedios,0,12)),
					array_sum(array_slice($array_promedios,0,13)),
					array_sum(array_slice($array_promedios,0,14)),
					array_sum(array_slice($array_promedios,0,15)),
					array_sum(array_slice($array_promedios,0,16)),
					array_sum(array_slice($array_promedios,0,17)),
					array_sum(array_slice($array_promedios,0,18)),
					array_sum(array_slice($array_promedios,0,19)),
					array_sum(array_slice($array_promedios,0,20)),
					array_sum(array_slice($array_promedios,0,21)),
					array_sum(array_slice($array_promedios,0,22)),
					array_sum(array_slice($array_promedios,0,23)),
					array_sum(array_slice($array_promedios,0,24))
					);
				$promedios[]=array(
					"datos"=>array(
						$array_acumulados[0],
						$array_acumulados[1],
						$array_acumulados[2],
						$array_acumulados[3],
						$array_acumulados[4],
						$array_acumulados[5],
						$array_acumulados[6],
						$array_acumulados[7],
						$array_acumulados[8],
						$array_acumulados[9],
						$array_acumulados[10],
						$array_acumulados[11],
						$array_acumulados[12],
						$array_acumulados[13],
						$array_acumulados[14],
						$array_acumulados[15],
						$array_acumulados[16],
						$array_acumulados[17],
						$array_acumulados[18],
						$array_acumulados[19],
						$array_acumulados[20],
						$array_acumulados[21],
						$array_acumulados[22],
						$array_acumulados[23]
						),
					"ubicacion"=>$ubicacion
					);
				$desviaciones[]=array(
						"datos"=>array(
							array(
								(double)($array_acumulados[0]-$promedio[0]->desviacion_00),
								(double)($array_acumulados[0]+$promedio[0]->desviacion_00)
							),
							array(
								(double)($array_acumulados[1]-$promedio[0]->desviacion_01),
								(double)($array_acumulados[1]+$promedio[0]->desviacion_01)
							),
							array(
								(double)($array_acumulados[2]-$promedio[0]->desviacion_02),
								(double)($array_acumulados[2]+$promedio[0]->desviacion_02)
							),
							array(
								(double)($array_acumulados[3]-$promedio[0]->desviacion_03),
								(double)($array_acumulados[3]+$promedio[0]->desviacion_03)
							),
							array(
								(double)($array_acumulados[4]-$promedio[0]->desviacion_04),
								(double)($array_acumulados[4]+$promedio[0]->desviacion_04)
							),
							array(
								(double)($array_acumulados[5]-$promedio[0]->desviacion_05),
								(double)($array_acumulados[5]+$promedio[0]->desviacion_05)
							),
							array(
								(double)($array_acumulados[6]-$promedio[0]->desviacion_06),
								(double)($array_acumulados[6]+$promedio[0]->desviacion_06)
							),
							array(
								(double)($array_acumulados[7]-$promedio[0]->desviacion_07),
								(double)($array_acumulados[7]+$promedio[0]->desviacion_07)
							),
							array(
								(double)($array_acumulados[8]-$promedio[0]->desviacion_08),
								(double)($array_acumulados[8]+$promedio[0]->desviacion_08)
							),
							array(
								(double)($array_acumulados[9]-$promedio[0]->desviacion_09),
								(double)($array_acumulados[9]+$promedio[0]->desviacion_09)
							),
							array(
								(double)($array_acumulados[10]-$promedio[0]->desviacion_10),
								(double)($array_acumulados[10]+$promedio[0]->desviacion_10)
							),
							array(
								(double)($array_acumulados[11]-$promedio[0]->desviacion_11),
								(double)($array_acumulados[11]+$promedio[0]->desviacion_11)
							),
							array(
								(double)($array_acumulados[12]-$promedio[0]->desviacion_12),
								(double)($array_acumulados[12]+$promedio[0]->desviacion_12)
							),
							array(
								(double)($array_acumulados[13]-$promedio[0]->desviacion_13),
								(double)($array_acumulados[13]+$promedio[0]->desviacion_13)
							),
							array(
								(double)($array_acumulados[14]-$promedio[0]->desviacion_14),
								(double)($array_acumulados[14]+$promedio[0]->desviacion_14)
							),
							array(
								(double)($array_acumulados[15]-$promedio[0]->desviacion_15),
								(double)($array_acumulados[15]+$promedio[0]->desviacion_15)
							),
							array(
								(double)($array_acumulados[16]-$promedio[0]->desviacion_16),
								(double)($array_acumulados[16]+$promedio[0]->desviacion_16)
							),
							array(
								(double)($array_acumulados[17]-$promedio[0]->desviacion_17),
								(double)($array_acumulados[17]+$promedio[0]->desviacion_17)
							),
							array(
								(double)($array_acumulados[18]-$promedio[0]->desviacion_18),
								(double)($array_acumulados[18]+$promedio[0]->desviacion_18)
							),
							array(
								(double)($array_acumulados[19]-$promedio[0]->desviacion_19),
								(double)($array_acumulados[19]+$promedio[0]->desviacion_19)
							),
							array(
								(double)($array_acumulados[20]-$promedio[0]->desviacion_20),
								(double)($array_acumulados[20]+$promedio[0]->desviacion_20)
							),
							array(
								(double)($array_acumulados[21]-$promedio[0]->desviacion_21),
								(double)($array_acumulados[21]+$promedio[0]->desviacion_21)
							),
							array(
								(double)($array_acumulados[22]-$promedio[0]->desviacion_22),
								(double)($array_acumulados[22]+$promedio[0]->desviacion_22)
							),
							array(
								(double)($array_acumulados[23]-$promedio[0]->desviacion_23),
								(double)($array_acumulados[23]+$promedio[0]->desviacion_23)
							)
						),
					"ubicacion"=>$ubicacion
					);
			}
			if($cantidad_personas)
			{
				$cantidades[]=array(
					"datos"=>$cantidad_personas,
					"ubicacion"=>$ubicacion
					);
			}
		}
		$respuesta["cantidad_personas"]=$cantidades;
		$respuesta["promedios"]=$promedios;
		$respuesta["desviaciones"]=$desviaciones;
		$respuesta["categorias"]=$categorias;
		$respuesta["graficos"]=$this->graficoPorDiaAccionesRepetidas();
		return Response::json($respuesta);
	}
	private function graficoPorDiaAccionesRepetidas()//se usa acá ↑
	{
		$pacientes=DB::select(
			DB::raw(
				"
			--	SELECT
			--	rut,
			--	fecha_inicio_estudio
			--	FROM paciente
			--	WHERE id_evento=2
			--	ORDER BY rut
				SELECT 
				c.rut_paciente AS rut,
				p.fecha_inicio_estudio
				FROM contacto AS c
				INNER JOIN paciente AS p ON p.rut=c.rut_paciente
				INNER JOIN evento AS e ON e.id_evento=p.id_evento
				WHERE e.id_evento=2
				AND c.rut_encargado=".Auth::user()->rut."
				AND p.paciente_visible=TRUE
				ORDER BY c.rut_paciente
				"
				)
			);
		$un_dia=new DateInterval('P1D');
		$treinta_dias=new DateInterval('P30D');
		
		$datos=array();
		$contador=0;
		foreach($pacientes as $paciente)
		{
			$rut=$paciente->rut;
			
			
			$date_fecha_inicial = new DateTime($paciente->fecha_inicio_estudio);
			$date_fecha_inicial1 = new DateTime($paciente->fecha_inicio_estudio);
			$date_fecha_final = $date_fecha_inicial1->add($treinta_dias);
			$date_fecha_siguiente=new DateTime($date_fecha_inicial->format('Y-m-d'));
			
			
			$nombre_sensor1=Sensor::obtenerUbicacion($rut,"e1");
			$nombre_sensor2=Sensor::obtenerUbicacion($rut,"e2");
			$nombre_sensor3=Sensor::obtenerUbicacion($rut,"e3");
			
			$ruido_sensor1=Sensor::tieneRuido($rut,"e1");
			$ruido_sensor2=Sensor::tieneRuido($rut,"e2");
			$ruido_sensor3=Sensor::tieneRuido($rut,"e3");
			
			$datos[$contador]["ubicaciones"]["s1"]=$nombre_sensor1;
			$datos[$contador]["ubicaciones"]["s2"]=$nombre_sensor2;
			$datos[$contador]["ubicaciones"]["s3"]=$nombre_sensor3;
			
			$datos[$contador]["ruido"]["s1"]=$ruido_sensor1;
			$datos[$contador]["ruido"]["s2"]=$ruido_sensor2;
			$datos[$contador]["ruido"]["s3"]=$ruido_sensor3;
			
			while($date_fecha_final->getTimestamp()>$date_fecha_siguiente->getTimestamp())
			{			

				$aperturas_e1=ResumenAccionesRepetidasController::obtenerAperturasAgrupadasDia($rut,$date_fecha_siguiente->format('Y-m-d'),"e1");
				$aperturas_e2=ResumenAccionesRepetidasController::obtenerAperturasAgrupadasDia($rut,$date_fecha_siguiente->format('Y-m-d'),"e2");
				$aperturas_e3=ResumenAccionesRepetidasController::obtenerAperturasAgrupadasDia($rut,$date_fecha_siguiente->format('Y-m-d'),"e3");
			
				$datos[$contador]["aperturas"]["s1"][]=$aperturas_e1;
				$datos[$contador]["aperturas"]["s2"][]=$aperturas_e2;
				$datos[$contador]["aperturas"]["s3"][]=$aperturas_e3;
		
				
				$date_fecha_siguiente->add($un_dia);	
			}
			$contador++;
		}
		return $datos;
	}
	public function procesarInformeAccionesRepetidas()
	{
		$resultado=DB::select(
			DB::raw(
				"
				SELECT 
				c.rut_paciente AS rut,
				p.fecha_inicio_estudio
				FROM contacto AS c
				INNER JOIN paciente AS p ON p.rut=c.rut_paciente
				INNER JOIN evento AS e ON e.id_evento=p.id_evento
				WHERE e.id_evento=2
				AND c.rut_encargado=".Auth::user()->rut."
				AND p.paciente_visible=TRUE
				ORDER BY c.rut_paciente
				"
				)
			);
		if($resultado)
		{
			$paciente_actual="";
			$datosexcel=array(
				array("Paciente/Día","S1","Ubicacion S1","S2","Ubicacion S2","S3","Ubicacion S3","Resultado geriatra con sensor","Resultado geriatra sin sensor")
			);
			$contador_paciente=0;
			$contador_dia=1;
			$un_dia=new DateInterval('P1D');
			$treinta_dias=new DateInterval('P30D');
			foreach($resultado as $paciente)
			{
				if($paciente_actual!=$paciente->rut)
				{
					$contador_paciente++;
					$contador_dia=1;
				}
				
				$fecha_inicio=new DateTime($paciente->fecha_inicio_estudio);
				$fecha_siguiente=new DateTime($paciente->fecha_inicio_estudio);
				$fecha_final=$fecha_inicio->add($treinta_dias);
				
					
				while($fecha_final->getTimestamp()>$fecha_siguiente->getTimestamp())
				{
					
					$resultado=DB::select(
						DB::raw(
							"
							-- buscar ubicacion del sensor y cantidad de aperturas en un día
							-- rut y fecha variables
							
							SELECT
							(
								SELECT 
								COUNT(*) 
								FROM resumen_acciones_repetidas
								WHERE identificador_sensor_puerta='e1'
								AND fecha_apertura::DATE='".$fecha_siguiente->format("Y-m-d")."'
								AND rut_paciente=".$paciente->rut."
							)AS cantidad_e1,
							(
								SELECT 
								us.ubicacion
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE s.identificador_sensor='e1'
								AND s.rut=".$paciente->rut."
							)AS ubicacion_e1,
							(
								SELECT 
								COUNT(*) 
								FROM resumen_acciones_repetidas
								WHERE identificador_sensor_puerta='e2'
								AND fecha_apertura::DATE='".$fecha_siguiente->format("Y-m-d")."'
								AND rut_paciente=".$paciente->rut."
							)AS cantidad_e2,
							(
								SELECT 
								us.ubicacion
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE s.identificador_sensor='e2'
								AND s.rut=".$paciente->rut."
							)AS ubicacion_e2,
							(
								SELECT 
								COUNT(*) 
								FROM resumen_acciones_repetidas
								WHERE identificador_sensor_puerta='e3'
								AND fecha_apertura::DATE='".$fecha_siguiente->format("Y-m-d")."'
								AND rut_paciente=".$paciente->rut."
							)AS cantidad_e3,
							(
								SELECT 
								us.ubicacion
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE s.identificador_sensor='e3'
								AND s.rut=".$paciente->rut."
							)AS ubicacion_e3,
							(
								SELECT 
								tiene_acciones_repetidas
								FROM informe_acciones_repetidas
								WHERE rut_paciente=".$paciente->rut."
								LIMIT 1
							)AS tiene_acciones_repetidas,
							(
								SELECT 
								tiene_acciones_repetidas_encuesta
								FROM informe_acciones_repetidas
								WHERE rut_paciente=".$paciente->rut."
								LIMIT 1
							)AS tiene_acciones_repetidas_encuesta

							"
							)
						);
					if($resultado)
					{
						$datosexcel[]=array(
							"P$contador_paciente D$contador_dia",
							$resultado[0]->cantidad_e1?$resultado[0]->cantidad_e1:"0",
							$resultado[0]->ubicacion_e1,
							$resultado[0]->cantidad_e2?$resultado[0]->cantidad_e2:"0",
							$resultado[0]->ubicacion_e2,
							$resultado[0]->cantidad_e3?$resultado[0]->cantidad_e3:"0",
							$resultado[0]->ubicacion_e3,
							is_null($resultado[0]->tiene_acciones_repetidas)?"":($resultado[0]->tiene_acciones_repetidas?"Sí":"No"),
							is_null($resultado[0]->tiene_acciones_repetidas_encuesta)?"":($resultado[0]->tiene_acciones_repetidas_encuesta?"Sí":"No")
							);
					}
					$contador_dia++;
					$fecha_siguiente->add($un_dia);
				}
			}
		}
		$informe=new PHPExcel();
		$objWorksheet = $informe->getActiveSheet();
		$objWorksheet->setTitle("Acciones Repetidas");
		
		
		$objWorksheet->fromArray(
				$datosexcel,  // The data to set
				NULL,        // Array values with this value will not be set
				'A1'         // Top left coordinate of the worksheet range where
                     //    we want to set these values (default is A1)
                );
        
        $objWriter = new PHPExcel_Writer_Excel2007($informe);

		$objWriter->setOffice2003Compatibility(true);
		$objWriter->setIncludeCharts(TRUE);
		$objWriter->save("/var/www/html/eHomeseniors/public/files/informe_acciones_repetidas.xlsx");
		
		return Response::download("/var/www/html/eHomeseniors/public/files/informe_acciones_repetidas.xlsx");
	}
	public static function obtenerResumenEncuesta($rut)
	{
		return DB::select(DB::raw("SELECT * FROM info_pacientes_acciones_repetidas_vista WHERE rut_paciente=$rut"));
	}
	public function guardarResumenEncuestas()
	{
		try{
			$rut_paciente=Input::get("rut");
			$respuesta=Input::get("respuesta");
			$usuario=Auth::user()->rut;
			
			$informe=InformeAccionesRepetidas::where("rut_usuario","=",$usuario)
			->where("rut_paciente","=",$rut_paciente)
			->first();
			
			if(!$informe)
			{
				$informe=new InformeAccionesRepetidas();
			}
			$informe->rut_paciente=$rut_paciente;
			$informe->rut_usuario=$usuario;
			$informe->fecha_informe=date("Y-m-d H:i:s");
			$informe->tiene_acciones_repetidas_encuesta=$respuesta==="true"?true:false;
			$informe->save();
			
			return Response::json(array("exito"=>"Se ha guardado correctamente"));
		}catch(Exception $e){
			return Response::json(array("error"=>"Error:".$e->getMessage()));
		}
		
	}
	public function obtenerResumenEncuestas()
	{
		try{
			$rut_paciente=Input::get("rut");
			$usuario=Auth::user()->rut;
			
			$informe=InformeAccionesRepetidas::where("rut_usuario","=",$usuario)
			->where("rut_paciente","=",$rut_paciente)
			->first();
			$r=null;
			if($informe)
				$r=$informe->tiene_acciones_repetidas_encuesta;
			return Response::json(array("respuesta"=>$r));
		}catch(Exception $e){
			return Response::json(array("error"=>"Error:".$e->getMessage()));
		}
		
	}

	
}