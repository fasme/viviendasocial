<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Encargado;

use Response;
use DB;
use Funciones;

class EncargadoController extends BaseController {

	//se usa
	public function obtenerRutNombreFamiliar($query){	

		$datos=Encargado::obtenerRutNombreContacto($query, "1");
		//return "EncargadoController ".$datos;
		//return $datos;
		$response=[];
		foreach($datos as $dato){
			// return $dato;
			$response[]=[
			"rut" => $dato["rut"],
			"nombre" => $dato["nombre"],
			"primerApellido" => $dato["primerApellido"],
			"segundoApellido" => $dato["segundoApellido"],
			"parentesco" => $dato["parentesco"],
			"correo" => $dato["correo"],
			"agenda" => $dato["agenda"]

			];
		}
		return response()->json($response);
	}

	public function obtenerRutNombreMedico($query){	
		$datos=Encargado::obtenerRutNombreContacto($query, "2");
		$response=[];
		foreach($datos as $dato){
			$response[]=["rut" => $dato["rut"],
			"nombre" => $dato["nombre"]];
		}
		return Response::json($response);
	}

	//se usa
	public function cargarDatosPaciente(Request $request){

		$serparacion = explode("-",$request->rut);

		$response = false;

		if (count($serparacion) > 1) {
			$dato = DB::table('usuario')
				->select("usuario.nombres", "usuario.apellido_paterno", "usuario.apellido_materno", "usuario.rut", "relacion_familiar.id_relacion_familiar", "usuario.correo")
				->join("contacto", "contacto.rut_encargado", "=", "usuario.rut")
				->join("relacion_familiar", "relacion_familiar.id_relacion_familiar", "=", "contacto.id_relacion_familiar")
				->where("usuario.id_tipo_usuario", "=", "6")
				->where("usuario.visible", "=", true)
				->where("usuario.rut", "=", $serparacion[0])
				->first();

			$agenda = DB::table("usuario")
						->select("agenda.telefono", "agenda.tipo_telefono")
						->leftjoin("agenda", "agenda.rut_usuario", "=","usuario.rut")
						->where("usuario.rut", $serparacion[0])
						->get();

			$dv = Funciones::obtenerDV($serparacion[0]);

			if ($serparacion[1] == $dv ) {
				$response=[
					"rut" => $dato->rut."-".$dv,
					"nombre" => $dato->nombres,
					"primerApellido" => $dato->apellido_paterno,
					"segundoApellido" => $dato->apellido_materno,
					"parentesco" => $dato->id_relacion_familiar,
					"correo" => $dato->correo,
					"agenda" => $agenda	
		 		];
			}
			
		}

		

		return response()->json($response);
	}
}
