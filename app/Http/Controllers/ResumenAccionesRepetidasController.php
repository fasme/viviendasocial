<?php

class ResumenAccionesRepetidasController extends BaseController{

	public static function generarGrafico()
	{
		set_time_limit(600);
		$rut=Input::get("rut");
		$fecha_inicial=Input::get("fecha_inicial");
		$fecha_final=Input::get("fecha_final");
		$tiempo=Input::get("tiempo");
		
		$un_dia=new DateInterval('P1D');
		$date_fecha_inicial = new DateTime($fecha_inicial);
		$date_fecha_final = new DateTime($fecha_final);
		$date_fecha_siguiente=new DateTime($date_fecha_inicial->format('Y-m-d'));
		$datos=array();
		$contador=0;
		
		$nombre_sensor1=Sensor::obtenerUbicacion($rut,"e1");
		$nombre_sensor2=Sensor::obtenerUbicacion($rut,"e2");
		$nombre_sensor3=Sensor::obtenerUbicacion($rut,"e3");
		
		$ruido_sensor1=Sensor::tieneRuido($rut,"e1");
		$ruido_sensor2=Sensor::tieneRuido($rut,"e2");
		$ruido_sensor3=Sensor::tieneRuido($rut,"e3");
		
		$datos["ubicaciones"]["s1"]=$nombre_sensor1;
		$datos["ubicaciones"]["s2"]=$nombre_sensor2;
		$datos["ubicaciones"]["s3"]=$nombre_sensor3;
		
		$datos["ruido"]["s1"]=$ruido_sensor1;
		$datos["ruido"]["s2"]=$ruido_sensor2;
		$datos["ruido"]["s3"]=$ruido_sensor3;
		
		while($date_fecha_final->getTimestamp()>=$date_fecha_siguiente->getTimestamp())
		{			
			$datos["datos"][$contador]=array_fill_keys(["sensor1","sensor2","sensor3"], array("cantidad"=>0,"segundos"=>0));
			$datos["datos"][$contador]=array_fill_keys(["fecha"], 0);
			
			DB::statement(
				DB::raw(
					"
					SET TIME ZONE  0;
					"
				)
			);
			
			$resultado=DB::select(
			DB::raw(
				"
				SELECT 
				*,
				TO_CHAR(fecha_apertura,'DD-MM-YYYY') AS fecha,
				EXTRACT(EPOCH FROM fecha_apertura::TIME)AS segundos_dia,
				EXTRACT(EPOCH FROM fecha_apertura)*1000 AS segundos_fecha
				FROM resumen_acciones_repetidas
				WHERE rut_paciente=$rut
				AND fecha_apertura BETWEEN '".$date_fecha_siguiente->format('Y-m-d')."' AND '".$date_fecha_siguiente->format('Y-m-d')." 23:59:59'
				ORDER BY fecha_apertura, id_resumen_acciones_repetidas ASC
				"
				)
			);
		
			$datos["datos"][$contador]=self::procesarFila($resultado,$tiempo,$date_fecha_siguiente->format('Y-m-d'));
			
				
			$datos["datos"][$contador]["fecha"]=$date_fecha_siguiente->format('d-m-Y');
		//	$datos["datos"][$contador]["fecha"]=$fila->fecha;
	
			unset($resultado);
			$contador++;
			$date_fecha_siguiente->add($un_dia);
		
		}
		$paciente=new PacienteController();
		$datos["cantidad_aperturas1"]=self::obtenerCantidadPersonasPorHora($datos["ubicaciones"]["s1"]);
		$datos["cantidad_aperturas2"]=self::obtenerCantidadPersonasPorHora($datos["ubicaciones"]["s2"]);
		$datos["cantidad_aperturas3"]=self::obtenerCantidadPersonasPorHora($datos["ubicaciones"]["s3"]);
		return $datos;
	
		
	}
	public static function procesarFila($r,$tiempo,$fecha)
	{
		$fi=new DateTime($fecha." 00:00:00");
		$ff=new DateTime($fecha." 23:59:59");
		$datos=array(
			"sensor1"=>array(
				"frecuencia"=>0,
				"cantidad"=>0,
				"acumulado"=>array(
					array(
						($fi->getTimestamp()+$fi->getOffset())*1000,
						0
						),
					array(
						($ff->getTimestamp()+$fi->getOffset())*1000,
						0
						)
					)
				),
			"sensor2"=>array(
				"frecuencia"=>0,
				"cantidad"=>0,
				"acumulado"=>array(
					array(
						($fi->getTimestamp()+$fi->getOffset())*1000,
						0
						),
					array(
						($ff->getTimestamp()+$fi->getOffset())*1000,
						0
						)
					)
				),
			"sensor3"=>array(
				"frecuencia"=>0,
				"cantidad"=>0,
				"acumulado"=>array(
					array(
						($fi->getTimestamp()+$fi->getOffset())*1000,
						0
						),
					array(
						($ff->getTimestamp()+$fi->getOffset())*1000,
						0
						)
					)
				),
			);
		$seccion_actual=array(
			"e1"=>0,
			"e2"=>0,
			"e3"=>0
			);
		$seccion=array(
			"e1"=>-1,
			"e2"=>-1,
			"e3"=>-1
			);
		$datos_frecuencia=array(
			"e1"=>array(),
			"e2"=>array(),
			"e3"=>array()
			);
		$acumulado=array(
			"e1"=>0,
			"e2"=>0,
			"e3"=>0
			);
		
		
		for($i=0;$i<count($r);$i++)
		{
			$sensor=$r[$i]->identificador_sensor_puerta;
			
			if($sensor=="e1")
			{
				$datos["sensor1"]["acumulado"][]=[(int)($r[$i]->segundos_fecha),++$acumulado["e1"]];
				$datos["sensor1"]["cantidad"]++;
			}
			else if($sensor=="e2")
			{
				$datos["sensor2"]["acumulado"][]=[(int)($r[$i]->segundos_fecha),++$acumulado["e2"]];
				$datos["sensor2"]["cantidad"]++;
			}
			else if($sensor=="e3")
			{
				$datos["sensor3"]["acumulado"][]=[(int)($r[$i]->segundos_fecha),++$acumulado["e3"]];
				$datos["sensor3"]["cantidad"]++;
			}
			
			$seccion_actual[$sensor]=round($r[$i]->segundos_dia/$tiempo,0,PHP_ROUND_HALF_DOWN);

			if($seccion[$sensor]==$seccion_actual[$sensor])
			{
				$datos_frecuencia[$sensor][$seccion[$sensor]]++;
			}
			else
			{
				$seccion[$sensor]=$seccion_actual[$sensor];
				$datos_frecuencia[$sensor][$seccion[$sensor]]=1;
			}
		}
		
		
		if(count($datos_frecuencia["e1"])>0)
			$datos["sensor1"]["frecuencia"]=max($datos_frecuencia["e1"]);
		if(count($datos_frecuencia["e2"])>0)
			$datos["sensor2"]["frecuencia"]=max($datos_frecuencia["e2"]);
		if(count($datos_frecuencia["e3"])>0)
			$datos["sensor3"]["frecuencia"]=max($datos_frecuencia["e3"]);
		
		
		$datos["sensor1"]["acumulado"][]=[(int)($ff->getTimestamp()+$ff->getOffset())*1000,$acumulado["e1"]];
		$datos["sensor2"]["acumulado"][]=[(int)($ff->getTimestamp()+$ff->getOffset())*1000,$acumulado["e2"]];
		$datos["sensor3"]["acumulado"][]=[(int)($ff->getTimestamp()+$ff->getOffset())*1000,$acumulado["e3"]];
	
		array_multisort($datos["sensor1"]["acumulado"]);
		
	
		array_multisort($datos["sensor2"]["acumulado"]);
		
	
		array_multisort($datos["sensor3"]["acumulado"]);
		
		
		return $datos;
		
	}
	public static function obtenerAperturasAgrupadasDia($rut,$fecha,$sensor)
	{
		$resultado=DB::select(
			DB::raw(
				"
				SELECT 
				(
					SELECT COUNT(*)
					FROM
					(
						SELECT 
						fecha_apertura
						FROM resumen_acciones_repetidas
						WHERE rut_paciente=$rut
						AND fecha_apertura BETWEEN '$fecha 00:00:00' AND '$fecha 00:59:59'
						AND identificador_sensor_puerta='$sensor'
						GROUP BY fecha_apertura,id_resumen_acciones_repetidas
						ORDER BY fecha_apertura,id_resumen_acciones_repetidas
					)AS a
				)AS h00,
				(
					SELECT COUNT(*)
					FROM
					(
						SELECT 
						fecha_apertura
						FROM resumen_acciones_repetidas
						WHERE rut_paciente=$rut
						AND fecha_apertura BETWEEN '$fecha 00:00:00' AND '$fecha 01:59:59'
						AND identificador_sensor_puerta='$sensor'
						GROUP BY fecha_apertura,id_resumen_acciones_repetidas
						ORDER BY fecha_apertura,id_resumen_acciones_repetidas
					)AS a
				)AS h01,
				(
					SELECT COUNT(*)
					FROM
					(
						SELECT 
						fecha_apertura
						FROM resumen_acciones_repetidas
						WHERE rut_paciente=$rut
						AND fecha_apertura BETWEEN '$fecha 00:00:00' AND '$fecha 02:59:59'
						AND identificador_sensor_puerta='$sensor'
						GROUP BY fecha_apertura,id_resumen_acciones_repetidas
						ORDER BY fecha_apertura,id_resumen_acciones_repetidas
					)AS a
				)AS h02,
				(
					SELECT COUNT(*)
					FROM
					(
						SELECT 
						fecha_apertura
						FROM resumen_acciones_repetidas
						WHERE rut_paciente=$rut
						AND fecha_apertura BETWEEN '$fecha 00:00:00' AND '$fecha 03:59:59'
						AND identificador_sensor_puerta='$sensor'
						GROUP BY fecha_apertura,id_resumen_acciones_repetidas
						ORDER BY fecha_apertura,id_resumen_acciones_repetidas
					)AS a
				)AS h03,
				(
					SELECT COUNT(*)
					FROM
					(
						SELECT 
						fecha_apertura
						FROM resumen_acciones_repetidas
						WHERE rut_paciente=$rut
						AND fecha_apertura BETWEEN '$fecha 00:00:00' AND '$fecha 04:59:59'
						AND identificador_sensor_puerta='$sensor'
						GROUP BY fecha_apertura,id_resumen_acciones_repetidas
						ORDER BY fecha_apertura,id_resumen_acciones_repetidas
					)AS a
				)AS h04,
				(
					SELECT COUNT(*)
					FROM
					(
						SELECT 
						fecha_apertura
						FROM resumen_acciones_repetidas
						WHERE rut_paciente=$rut
						AND fecha_apertura BETWEEN '$fecha 00:00:00' AND '$fecha 05:59:59'
						AND identificador_sensor_puerta='$sensor'
						GROUP BY fecha_apertura,id_resumen_acciones_repetidas
						ORDER BY fecha_apertura,id_resumen_acciones_repetidas
					)AS a
				)AS h05,
				(
					SELECT COUNT(*)
					FROM
					(
						SELECT 
						fecha_apertura
						FROM resumen_acciones_repetidas
						WHERE rut_paciente=$rut
						AND fecha_apertura BETWEEN '$fecha 00:00:00' AND '$fecha 06:59:59'
						AND identificador_sensor_puerta='$sensor'
						GROUP BY fecha_apertura,id_resumen_acciones_repetidas
						ORDER BY fecha_apertura,id_resumen_acciones_repetidas
					)AS a
				)AS h06,
				(
					SELECT COUNT(*)
					FROM
					(
						SELECT 
						fecha_apertura
						FROM resumen_acciones_repetidas
						WHERE rut_paciente=$rut
						AND fecha_apertura BETWEEN '$fecha 00:00:00' AND '$fecha 07:59:59'
						AND identificador_sensor_puerta='$sensor'
						GROUP BY fecha_apertura,id_resumen_acciones_repetidas
						ORDER BY fecha_apertura,id_resumen_acciones_repetidas
					)AS a
				)AS h07,
				(
					SELECT COUNT(*)
					FROM
					(
						SELECT 
						fecha_apertura
						FROM resumen_acciones_repetidas
						WHERE rut_paciente=$rut
						AND fecha_apertura BETWEEN '$fecha 00:00:00' AND '$fecha 08:59:59'
						AND identificador_sensor_puerta='$sensor'
						GROUP BY fecha_apertura,id_resumen_acciones_repetidas
						ORDER BY fecha_apertura,id_resumen_acciones_repetidas
					)AS a
				)AS h08,
				(
					SELECT COUNT(*)
					FROM
					(
						SELECT 
						fecha_apertura
						FROM resumen_acciones_repetidas
						WHERE rut_paciente=$rut
						AND fecha_apertura BETWEEN '$fecha 00:00:00' AND '$fecha 09:59:59'
						AND identificador_sensor_puerta='$sensor'
						GROUP BY fecha_apertura,id_resumen_acciones_repetidas
						ORDER BY fecha_apertura,id_resumen_acciones_repetidas
					)AS a
				)AS h09,
				(
					SELECT COUNT(*)
					FROM
					(
						SELECT 
						fecha_apertura
						FROM resumen_acciones_repetidas
						WHERE rut_paciente=$rut
						AND fecha_apertura BETWEEN '$fecha 00:00:00' AND '$fecha 10:59:59'
						AND identificador_sensor_puerta='$sensor'
						GROUP BY fecha_apertura,id_resumen_acciones_repetidas
						ORDER BY fecha_apertura,id_resumen_acciones_repetidas
					)AS a
				)AS h10,
				(
					SELECT COUNT(*)
					FROM
					(
						SELECT 
						fecha_apertura
						FROM resumen_acciones_repetidas
						WHERE rut_paciente=$rut
						AND fecha_apertura BETWEEN '$fecha 00:00:00' AND '$fecha 11:59:59'
						AND identificador_sensor_puerta='$sensor'
						GROUP BY fecha_apertura,id_resumen_acciones_repetidas
						ORDER BY fecha_apertura,id_resumen_acciones_repetidas
					)AS a
				)AS h11,
				(
					SELECT COUNT(*)
					FROM
					(
						SELECT 
						fecha_apertura
						FROM resumen_acciones_repetidas
						WHERE rut_paciente=$rut
						AND fecha_apertura BETWEEN '$fecha 00:00:00' AND '$fecha 12:59:59'
						AND identificador_sensor_puerta='$sensor'
						GROUP BY fecha_apertura,id_resumen_acciones_repetidas
						ORDER BY fecha_apertura,id_resumen_acciones_repetidas
					)AS a
				)AS h12,
				(
					SELECT COUNT(*)
					FROM
					(
						SELECT 
						fecha_apertura
						FROM resumen_acciones_repetidas
						WHERE rut_paciente=$rut
						AND fecha_apertura BETWEEN '$fecha 00:00:00' AND '$fecha 13:59:59'
						AND identificador_sensor_puerta='$sensor'
						GROUP BY fecha_apertura,id_resumen_acciones_repetidas
						ORDER BY fecha_apertura,id_resumen_acciones_repetidas
					)AS a
				)AS h13,
				(
					SELECT COUNT(*)
					FROM
					(
						SELECT 
						fecha_apertura
						FROM resumen_acciones_repetidas
						WHERE rut_paciente=$rut
						AND fecha_apertura BETWEEN '$fecha 00:00:00' AND '$fecha 14:59:59'
						AND identificador_sensor_puerta='$sensor'
						GROUP BY fecha_apertura,id_resumen_acciones_repetidas
						ORDER BY fecha_apertura,id_resumen_acciones_repetidas
					)AS a
				)AS h14,
				(
					SELECT COUNT(*)
					FROM
					(
						SELECT 
						fecha_apertura
						FROM resumen_acciones_repetidas
						WHERE rut_paciente=$rut
						AND fecha_apertura BETWEEN '$fecha 00:00:00' AND '$fecha 15:59:59'
						AND identificador_sensor_puerta='$sensor'
						GROUP BY fecha_apertura,id_resumen_acciones_repetidas
						ORDER BY fecha_apertura,id_resumen_acciones_repetidas
					)AS a
				)AS h15,
				(
					SELECT COUNT(*)
					FROM
					(
						SELECT 
						fecha_apertura
						FROM resumen_acciones_repetidas
						WHERE rut_paciente=$rut
						AND fecha_apertura BETWEEN '$fecha 00:00:00' AND '$fecha 16:59:59'
						AND identificador_sensor_puerta='$sensor'
						GROUP BY fecha_apertura,id_resumen_acciones_repetidas
						ORDER BY fecha_apertura,id_resumen_acciones_repetidas
					)AS a
				)AS h16,
				(
					SELECT COUNT(*)
					FROM
					(
						SELECT 
						fecha_apertura
						FROM resumen_acciones_repetidas
						WHERE rut_paciente=$rut
						AND fecha_apertura BETWEEN '$fecha 00:00:00' AND '$fecha 17:59:59'
						AND identificador_sensor_puerta='$sensor'
						GROUP BY fecha_apertura,id_resumen_acciones_repetidas
						ORDER BY fecha_apertura,id_resumen_acciones_repetidas
					)AS a
				)AS h17,
				(
					SELECT COUNT(*)
					FROM
					(
						SELECT 
						fecha_apertura
						FROM resumen_acciones_repetidas
						WHERE rut_paciente=$rut
						AND fecha_apertura BETWEEN '$fecha 00:00:00' AND '$fecha 18:59:59'
						AND identificador_sensor_puerta='$sensor'
						GROUP BY fecha_apertura,id_resumen_acciones_repetidas
						ORDER BY fecha_apertura,id_resumen_acciones_repetidas
					)AS a
				)AS h18,
				(
					SELECT COUNT(*)
					FROM
					(
						SELECT 
						fecha_apertura
						FROM resumen_acciones_repetidas
						WHERE rut_paciente=$rut
						AND fecha_apertura BETWEEN '$fecha 00:00:00' AND '$fecha 19:59:59'
						AND identificador_sensor_puerta='$sensor'
						GROUP BY fecha_apertura,id_resumen_acciones_repetidas
						ORDER BY fecha_apertura,id_resumen_acciones_repetidas
					)AS a
				)AS h19,
				(
					SELECT COUNT(*)
					FROM
					(
						SELECT 
						fecha_apertura
						FROM resumen_acciones_repetidas
						WHERE rut_paciente=$rut
						AND fecha_apertura BETWEEN '$fecha 00:00:00' AND '$fecha 20:59:59'
						AND identificador_sensor_puerta='$sensor'
						GROUP BY fecha_apertura,id_resumen_acciones_repetidas
						ORDER BY fecha_apertura,id_resumen_acciones_repetidas
					)AS a
				)AS h20,
				(
					SELECT COUNT(*)
					FROM
					(
						SELECT 
						fecha_apertura
						FROM resumen_acciones_repetidas
						WHERE rut_paciente=$rut
						AND fecha_apertura BETWEEN '$fecha 00:00:00' AND '$fecha 21:59:59'
						AND identificador_sensor_puerta='$sensor'
						GROUP BY fecha_apertura,id_resumen_acciones_repetidas
						ORDER BY fecha_apertura,id_resumen_acciones_repetidas
					)AS a
				)AS h21,
				(
					SELECT COUNT(*)
					FROM
					(
						SELECT 
						fecha_apertura
						FROM resumen_acciones_repetidas
						WHERE rut_paciente=$rut
						AND fecha_apertura BETWEEN '$fecha 00:00:00' AND '$fecha 22:59:59'
						AND identificador_sensor_puerta='$sensor'
						GROUP BY fecha_apertura,id_resumen_acciones_repetidas
						ORDER BY fecha_apertura,id_resumen_acciones_repetidas
					)AS a
				)AS h22,
				(
					SELECT COUNT(*)
					FROM
					(
						SELECT 
						fecha_apertura
						FROM resumen_acciones_repetidas
						WHERE rut_paciente=$rut
						AND fecha_apertura BETWEEN '$fecha 00:00:00' AND '$fecha 23:59:59'
						AND identificador_sensor_puerta='$sensor'
						GROUP BY fecha_apertura,id_resumen_acciones_repetidas
						ORDER BY fecha_apertura,id_resumen_acciones_repetidas
					)AS a
				)AS h23
				"
				)
			);
		$datos=array_fill(0,24,0);
		if($resultado)
		{
			$datos[0]=$resultado[0]->h00;
			$datos[1]=$resultado[0]->h01;
			$datos[2]=$resultado[0]->h02;
			$datos[3]=$resultado[0]->h03;
			$datos[4]=$resultado[0]->h04;
			$datos[5]=$resultado[0]->h05;
			$datos[6]=$resultado[0]->h06;
			$datos[7]=$resultado[0]->h07;
			$datos[8]=$resultado[0]->h08;
			$datos[9]=$resultado[0]->h09;
			$datos[10]=$resultado[0]->h10;
			$datos[11]=$resultado[0]->h11;
			$datos[12]=$resultado[0]->h12;
			$datos[13]=$resultado[0]->h13;
			$datos[14]=$resultado[0]->h14;
			$datos[15]=$resultado[0]->h15;
			$datos[16]=$resultado[0]->h16;
			$datos[17]=$resultado[0]->h17;
			$datos[18]=$resultado[0]->h18;
			$datos[19]=$resultado[0]->h19;
			$datos[20]=$resultado[0]->h20;
			$datos[21]=$resultado[0]->h21;
			$datos[22]=$resultado[0]->h22;
			$datos[23]=$resultado[0]->h23;
		}
		return $datos;
		
	}
	public static function obtenerPromedioDesviacionTotal($ubicacion)
	{
		$resultado=DB::select(
			DB::raw(
				"
				-- obtener el promedio del sensor
				-- variables: sensor,ubicacion
				SELECT
				(
					SELECT 
					COALESCE(SUM(cantidad)/cantidad_pacientes,0) AS promedio
					FROM
					(
						SELECT COUNT(*)AS cantidad,
						rut_paciente,
						cantidad_pacientes
						FROM
						(
							SELECT 
							rar.fecha_apertura,
							rar.rut_paciente,
							(
							SELECT COUNT(*) from
								(SELECT DISTINCT(s.rut)
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE ubicacion='$ubicacion'
								ORDER BY s.rut
								)AS a
							)AS cantidad_pacientes
							FROM resumen_acciones_repetidas AS rar
							INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
							INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
							WHERE rut_paciente IN 
							(
								SELECT DISTINCT(s.rut)
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE ubicacion='$ubicacion'
								ORDER BY s.rut
							)
							AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
							AND (rar.fecha_apertura::TIME BETWEEN '00:00:00' AND '00:59:59')
							AND rar.identificador_sensor_puerta=
							(
								SELECT s.identificador_sensor
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE us.ubicacion='$ubicacion'
								AND s.rut=rar.rut_paciente
							)
							GROUP BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas,rar.rut_paciente
							ORDER BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas
						)AS total
						GROUP BY rut_paciente,cantidad_pacientes
					) AS a
					GROUP BY cantidad_pacientes
				)::REAL AS promedio_00,
				(
					select coalesce(stddev(datos),0) FROM(
					select unnest(
					array_cat(
						array(
							SELECT COUNT(*)AS cantidad
							FROM
							(
								SELECT 
								rar.fecha_apertura,
								rar.rut_paciente
								FROM resumen_acciones_repetidas AS rar
								INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
								INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
								WHERE rut_paciente IN 
								(
									SELECT DISTINCT(s.rut)
									FROM sensor AS s
									INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
									WHERE ubicacion='$ubicacion'
									ORDER BY s.rut
								)
								AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
								AND rar.fecha_apertura::TIME BETWEEN '00:00:00' AND '00:59:59'
								AND rar.identificador_sensor_puerta=
								(
									SELECT s.identificador_sensor
									FROM sensor AS s
									INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
									WHERE us.ubicacion='$ubicacion'
									AND s.rut=rar.rut_paciente
								)
								GROUP BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas,rar.rut_paciente
								ORDER BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas
							)AS total
							GROUP BY rut_paciente
						)::INT[]
					,
					array_fill(0,array[
						(
						SELECT COUNT(*) from
						(
							SELECT DISTINCT(s.rut)
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE ubicacion='$ubicacion'
							ORDER BY s.rut
						)AS a)::INT
					])
						)
					)AS datos
				
					limit (SELECT COUNT(*) from
						(SELECT DISTINCT(s.rut)
						FROM sensor AS s
						INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
						WHERE ubicacion='$ubicacion'
						ORDER BY s.rut
						)AS a)
					)AS b
						
				)::REAL AS desviacion_00,
				(
					SELECT 
					COALESCE(SUM(cantidad)/cantidad_pacientes,0) AS promedio
					FROM
					(
						SELECT COUNT(*)AS cantidad,
						rut_paciente,
						cantidad_pacientes
						FROM
						(
							SELECT 
							rar.fecha_apertura,
							rar.rut_paciente,
							(
							SELECT COUNT(*) from
								(SELECT DISTINCT(s.rut)
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE ubicacion='$ubicacion'
								ORDER BY s.rut
								)AS a
							)AS cantidad_pacientes
							FROM resumen_acciones_repetidas AS rar
							INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
							INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
							WHERE rut_paciente IN 
							(
								SELECT DISTINCT(s.rut)
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE ubicacion='$ubicacion'
								ORDER BY s.rut
							)
							AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
							AND (rar.fecha_apertura::TIME BETWEEN '01:00:00' AND '01:59:59')
							AND rar.identificador_sensor_puerta=
							(
								SELECT s.identificador_sensor
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE us.ubicacion='$ubicacion'
								AND s.rut=rar.rut_paciente
							)
							GROUP BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas,rar.rut_paciente
							ORDER BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas
						)AS total
						GROUP BY rut_paciente,cantidad_pacientes
					) AS a
					GROUP BY cantidad_pacientes
				)::REAL AS promedio_01,
				(
					select coalesce(stddev(datos),0) FROM(
					select unnest(
					array_cat(
						array(
							SELECT COUNT(*)AS cantidad
							FROM
							(
								SELECT 
								rar.fecha_apertura,
								rar.rut_paciente
								FROM resumen_acciones_repetidas AS rar
								INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
								INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
								WHERE rut_paciente IN 
								(
									SELECT DISTINCT(s.rut)
									FROM sensor AS s
									INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
									WHERE ubicacion='$ubicacion'
									ORDER BY s.rut
								)
								AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
								AND rar.fecha_apertura::TIME BETWEEN '01:00:00' AND '01:59:59'
								AND rar.identificador_sensor_puerta=
								(
									SELECT s.identificador_sensor
									FROM sensor AS s
									INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
									WHERE us.ubicacion='$ubicacion'
									AND s.rut=rar.rut_paciente
								)
								GROUP BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas,rar.rut_paciente
								ORDER BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas
							)AS total
							GROUP BY rut_paciente
						)::INT[]
					,
					array_fill(0,array[
						(
						SELECT COUNT(*) from
						(
							SELECT DISTINCT(s.rut)
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE ubicacion='$ubicacion'
							ORDER BY s.rut
						)AS a)::INT
					])
						)
					)AS datos
				
					limit (SELECT COUNT(*) from
						(SELECT DISTINCT(s.rut)
						FROM sensor AS s
						INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
						WHERE ubicacion='$ubicacion'
						ORDER BY s.rut
						)AS a)
					)AS b
				)::REAL AS desviacion_01,
				(
					SELECT 
					COALESCE(SUM(cantidad)/cantidad_pacientes,0) AS promedio
					FROM
					(
						SELECT COUNT(*)AS cantidad,
						rut_paciente,
						cantidad_pacientes
						FROM
						(
							SELECT 
							rar.fecha_apertura,
							rar.rut_paciente,
							(
							SELECT COUNT(*) from
								(SELECT DISTINCT(s.rut)
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE ubicacion='$ubicacion'
								ORDER BY s.rut
								)AS a
							)AS cantidad_pacientes
							FROM resumen_acciones_repetidas AS rar
							INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
							INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
							WHERE rut_paciente IN 
							(
								SELECT DISTINCT(s.rut)
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE ubicacion='$ubicacion'
								ORDER BY s.rut
							)
							AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
							AND (rar.fecha_apertura::TIME BETWEEN '02:00:00' AND '02:59:59')
							AND rar.identificador_sensor_puerta=
							(
								SELECT s.identificador_sensor
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE us.ubicacion='$ubicacion'
								AND s.rut=rar.rut_paciente
							)
							GROUP BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas,rar.rut_paciente
							ORDER BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas
						)AS total
						GROUP BY rut_paciente,cantidad_pacientes
					) AS a
					GROUP BY cantidad_pacientes
				)::REAL AS promedio_02,
				(
					select coalesce(stddev(datos),0) FROM(
					select unnest(
					array_cat(
						array(
							SELECT COUNT(*)AS cantidad
							FROM
							(
								SELECT 
								rar.fecha_apertura,
								rar.rut_paciente
								FROM resumen_acciones_repetidas AS rar
								INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
								INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
								WHERE rut_paciente IN 
								(
									SELECT DISTINCT(s.rut)
									FROM sensor AS s
									INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
									WHERE ubicacion='$ubicacion'
									ORDER BY s.rut
								)
								AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
								AND rar.fecha_apertura::TIME BETWEEN '02:00:00' AND '02:59:59'
								AND rar.identificador_sensor_puerta=
								(
									SELECT s.identificador_sensor
									FROM sensor AS s
									INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
									WHERE us.ubicacion='$ubicacion'
									AND s.rut=rar.rut_paciente
								)
								GROUP BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas,rar.rut_paciente
								ORDER BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas
							)AS total
							GROUP BY rut_paciente
						)::INT[]
					,
					array_fill(0,array[
						(
						SELECT COUNT(*) from
						(
							SELECT DISTINCT(s.rut)
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE ubicacion='$ubicacion'
							ORDER BY s.rut
						)AS a)::INT
					])
						)
					)AS datos
				
					limit (SELECT COUNT(*) from
						(SELECT DISTINCT(s.rut)
						FROM sensor AS s
						INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
						WHERE ubicacion='$ubicacion'
						ORDER BY s.rut
						)AS a)
					)AS b
				)::REAL AS desviacion_02,
				(
					SELECT 
					COALESCE(SUM(cantidad)/cantidad_pacientes,0) AS promedio
					FROM
					(
						SELECT COUNT(*)AS cantidad,
						rut_paciente,
						cantidad_pacientes
						FROM
						(
							SELECT 
							rar.fecha_apertura,
							rar.rut_paciente,
							(
							SELECT COUNT(*) from
								(SELECT DISTINCT(s.rut)
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE ubicacion='$ubicacion'
								ORDER BY s.rut
								)AS a
							)AS cantidad_pacientes
							FROM resumen_acciones_repetidas AS rar
							INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
							INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
							WHERE rut_paciente IN 
							(
								SELECT DISTINCT(s.rut)
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE ubicacion='$ubicacion'
								ORDER BY s.rut
							)
							AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
							AND (rar.fecha_apertura::TIME BETWEEN '03:00:00' AND '03:59:59')
							AND rar.identificador_sensor_puerta=
							(
								SELECT s.identificador_sensor
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE us.ubicacion='$ubicacion'
								AND s.rut=rar.rut_paciente
							)
							GROUP BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas,rar.rut_paciente
							ORDER BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas
						)AS total
						GROUP BY rut_paciente,cantidad_pacientes
					) AS a
					GROUP BY cantidad_pacientes
				)::REAL AS promedio_03,
				(
					select coalesce(stddev(datos),0) FROM(
					select unnest(
					array_cat(
						array(
							SELECT COUNT(*)AS cantidad
							FROM
							(
								SELECT 
								rar.fecha_apertura,
								rar.rut_paciente
								FROM resumen_acciones_repetidas AS rar
								INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
								INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
								WHERE rut_paciente IN 
								(
									SELECT DISTINCT(s.rut)
									FROM sensor AS s
									INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
									WHERE ubicacion='$ubicacion'
									ORDER BY s.rut
								)
								AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
								AND rar.fecha_apertura::TIME BETWEEN '03:00:00' AND '03:59:59'
								AND rar.identificador_sensor_puerta=
								(
									SELECT s.identificador_sensor
									FROM sensor AS s
									INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
									WHERE us.ubicacion='$ubicacion'
									AND s.rut=rar.rut_paciente
								)
								GROUP BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas,rar.rut_paciente
								ORDER BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas
							)AS total
							GROUP BY rut_paciente
						)::INT[]
					,
					array_fill(0,array[
						(
						SELECT COUNT(*) from
						(
							SELECT DISTINCT(s.rut)
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE ubicacion='$ubicacion'
							ORDER BY s.rut
						)AS a)::INT
					])
						)
					)AS datos
				
					limit (SELECT COUNT(*) from
						(SELECT DISTINCT(s.rut)
						FROM sensor AS s
						INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
						WHERE ubicacion='$ubicacion'
						ORDER BY s.rut
						)AS a)
					)AS b
				)::REAL AS desviacion_03,
				(
					SELECT 
					COALESCE(SUM(cantidad)/cantidad_pacientes,0) AS promedio
					FROM
					(
						SELECT COUNT(*)AS cantidad,
						rut_paciente,
						cantidad_pacientes
						FROM
						(
							SELECT 
							rar.fecha_apertura,
							rar.rut_paciente,
							(
							SELECT COUNT(*) from
								(SELECT DISTINCT(s.rut)
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE ubicacion='$ubicacion'
								ORDER BY s.rut
								)AS a
							)AS cantidad_pacientes
							FROM resumen_acciones_repetidas AS rar
							INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
							INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
							WHERE rut_paciente IN 
							(
								SELECT DISTINCT(s.rut)
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE ubicacion='$ubicacion'
								ORDER BY s.rut
							)
							AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
							AND (rar.fecha_apertura::TIME BETWEEN '04:00:00' AND '04:59:59')
							AND rar.identificador_sensor_puerta=
							(
								SELECT s.identificador_sensor
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE us.ubicacion='$ubicacion'
								AND s.rut=rar.rut_paciente
							)
							GROUP BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas,rar.rut_paciente
							ORDER BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas
						)AS total
						GROUP BY rut_paciente,cantidad_pacientes
					) AS a
					GROUP BY cantidad_pacientes
				)::REAL AS promedio_04,
				(
					select coalesce(stddev(datos),0) FROM(
					select unnest(
					array_cat(
						array(
							SELECT COUNT(*)AS cantidad
							FROM
							(
								SELECT 
								rar.fecha_apertura,
								rar.rut_paciente
								FROM resumen_acciones_repetidas AS rar
								INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
								INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
								WHERE rut_paciente IN 
								(
									SELECT DISTINCT(s.rut)
									FROM sensor AS s
									INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
									WHERE ubicacion='$ubicacion'
									ORDER BY s.rut
								)
								AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
								AND rar.fecha_apertura::TIME BETWEEN '04:00:00' AND '04:59:59'
								AND rar.identificador_sensor_puerta=
								(
									SELECT s.identificador_sensor
									FROM sensor AS s
									INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
									WHERE us.ubicacion='$ubicacion'
									AND s.rut=rar.rut_paciente
								)
								GROUP BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas,rar.rut_paciente
								ORDER BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas
							)AS total
							GROUP BY rut_paciente
						)::INT[]
					,
					array_fill(0,array[
						(
						SELECT COUNT(*) from
						(
							SELECT DISTINCT(s.rut)
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE ubicacion='$ubicacion'
							ORDER BY s.rut
						)AS a)::INT
					])
						)
					)AS datos
				
					limit (SELECT COUNT(*) from
						(SELECT DISTINCT(s.rut)
						FROM sensor AS s
						INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
						WHERE ubicacion='$ubicacion'
						ORDER BY s.rut
						)AS a)
					)AS b
				)::REAL AS desviacion_04,
				(
					SELECT 
					COALESCE(SUM(cantidad)/cantidad_pacientes,0) AS promedio
					FROM
					(
						SELECT COUNT(*)AS cantidad,
						rut_paciente,
						cantidad_pacientes
						FROM
						(
							SELECT 
							rar.fecha_apertura,
							rar.rut_paciente,
							(
							SELECT COUNT(*) from
								(SELECT DISTINCT(s.rut)
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE ubicacion='$ubicacion'
								ORDER BY s.rut
								)AS a
							)AS cantidad_pacientes
							FROM resumen_acciones_repetidas AS rar
							INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
							INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
							WHERE rut_paciente IN 
							(
								SELECT DISTINCT(s.rut)
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE ubicacion='$ubicacion'
								ORDER BY s.rut
							)
							AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
							AND (rar.fecha_apertura::TIME BETWEEN '05:00:00' AND '05:59:59')
							AND rar.identificador_sensor_puerta=
							(
								SELECT s.identificador_sensor
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE us.ubicacion='$ubicacion'
								AND s.rut=rar.rut_paciente
							)
							GROUP BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas,rar.rut_paciente
							ORDER BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas
						)AS total
						GROUP BY rut_paciente,cantidad_pacientes
					) AS a
					GROUP BY cantidad_pacientes
				)::REAL AS promedio_05,
				(
					select coalesce(stddev(datos),0) FROM(
					select unnest(
					array_cat(
						array(
							SELECT COUNT(*)AS cantidad
							FROM
							(
								SELECT 
								rar.fecha_apertura,
								rar.rut_paciente
								FROM resumen_acciones_repetidas AS rar
								INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
								INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
								WHERE rut_paciente IN 
								(
									SELECT DISTINCT(s.rut)
									FROM sensor AS s
									INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
									WHERE ubicacion='$ubicacion'
									ORDER BY s.rut
								)
								AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
								AND rar.fecha_apertura::TIME BETWEEN '05:00:00' AND '05:59:59'
								AND rar.identificador_sensor_puerta=
								(
									SELECT s.identificador_sensor
									FROM sensor AS s
									INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
									WHERE us.ubicacion='$ubicacion'
									AND s.rut=rar.rut_paciente
								)
								GROUP BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas,rar.rut_paciente
								ORDER BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas
							)AS total
							GROUP BY rut_paciente
						)::INT[]
					,
					array_fill(0,array[
						(
						SELECT COUNT(*) from
						(
							SELECT DISTINCT(s.rut)
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE ubicacion='$ubicacion'
							ORDER BY s.rut
						)AS a)::INT
					])
						)
					)AS datos
				
					limit (SELECT COUNT(*) from
						(SELECT DISTINCT(s.rut)
						FROM sensor AS s
						INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
						WHERE ubicacion='$ubicacion'
						ORDER BY s.rut
						)AS a)
					)AS b
				)::REAL AS desviacion_05,
				(
					SELECT 
					COALESCE(SUM(cantidad)/cantidad_pacientes,0) AS promedio
					FROM
					(
						SELECT COUNT(*)AS cantidad,
						rut_paciente,
						cantidad_pacientes
						FROM
						(
							SELECT 
							rar.fecha_apertura,
							rar.rut_paciente,
							(
							SELECT COUNT(*) from
								(SELECT DISTINCT(s.rut)
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE ubicacion='$ubicacion'
								ORDER BY s.rut
								)AS a
							)AS cantidad_pacientes
							FROM resumen_acciones_repetidas AS rar
							INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
							INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
							WHERE rut_paciente IN 
							(
								SELECT DISTINCT(s.rut)
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE ubicacion='$ubicacion'
								ORDER BY s.rut
							)
							AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
							AND (rar.fecha_apertura::TIME BETWEEN '06:00:00' AND '06:59:59')
							AND rar.identificador_sensor_puerta=
							(
								SELECT s.identificador_sensor
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE us.ubicacion='$ubicacion'
								AND s.rut=rar.rut_paciente
							)
							GROUP BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas,rar.rut_paciente
							ORDER BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas
						)AS total
						GROUP BY rut_paciente,cantidad_pacientes
					) AS a
					GROUP BY cantidad_pacientes
				)::REAL AS promedio_06,
				(
					select coalesce(stddev(datos),0) FROM(
					select unnest(
					array_cat(
						array(
							SELECT COUNT(*)AS cantidad
							FROM
							(
								SELECT 
								rar.fecha_apertura,
								rar.rut_paciente
								FROM resumen_acciones_repetidas AS rar
								INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
								INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
								WHERE rut_paciente IN 
								(
									SELECT DISTINCT(s.rut)
									FROM sensor AS s
									INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
									WHERE ubicacion='$ubicacion'
									ORDER BY s.rut
								)
								AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
								AND rar.fecha_apertura::TIME BETWEEN '06:00:00' AND '06:59:59'
								AND rar.identificador_sensor_puerta=
								(
									SELECT s.identificador_sensor
									FROM sensor AS s
									INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
									WHERE us.ubicacion='$ubicacion'
									AND s.rut=rar.rut_paciente
								)
								GROUP BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas,rar.rut_paciente
								ORDER BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas
							)AS total
							GROUP BY rut_paciente
						)::INT[]
					,
					array_fill(0,array[
						(
						SELECT COUNT(*) from
						(
							SELECT DISTINCT(s.rut)
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE ubicacion='$ubicacion'
							ORDER BY s.rut
						)AS a)::INT
					])
						)
					)AS datos
				
					limit (SELECT COUNT(*) from
						(SELECT DISTINCT(s.rut)
						FROM sensor AS s
						INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
						WHERE ubicacion='$ubicacion'
						ORDER BY s.rut
						)AS a)
					)AS b
				)::REAL AS desviacion_06,
				(
					SELECT 
					COALESCE(SUM(cantidad)/cantidad_pacientes,0) AS promedio
					FROM
					(
						SELECT COUNT(*)AS cantidad,
						rut_paciente,
						cantidad_pacientes
						FROM
						(
							SELECT 
							rar.fecha_apertura,
							rar.rut_paciente,
							(
							SELECT COUNT(*) from
								(SELECT DISTINCT(s.rut)
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE ubicacion='$ubicacion'
								ORDER BY s.rut
								)AS a
							)AS cantidad_pacientes
							FROM resumen_acciones_repetidas AS rar
							INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
							INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
							WHERE rut_paciente IN 
							(
								SELECT DISTINCT(s.rut)
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE ubicacion='$ubicacion'
								ORDER BY s.rut
							)
							AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
							AND (rar.fecha_apertura::TIME BETWEEN '07:00:00' AND '07:59:59')
							AND rar.identificador_sensor_puerta=
							(
								SELECT s.identificador_sensor
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE us.ubicacion='$ubicacion'
								AND s.rut=rar.rut_paciente
							)
							GROUP BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas,rar.rut_paciente
							ORDER BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas
						)AS total
						GROUP BY rut_paciente,cantidad_pacientes
					) AS a
					GROUP BY cantidad_pacientes
				)::REAL AS promedio_07,
				(
					select coalesce(stddev(datos),0) FROM(
					select unnest(
					array_cat(
						array(
							SELECT COUNT(*)AS cantidad
							FROM
							(
								SELECT 
								rar.fecha_apertura,
								rar.rut_paciente
								FROM resumen_acciones_repetidas AS rar
								INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
								INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
								WHERE rut_paciente IN 
								(
									SELECT DISTINCT(s.rut)
									FROM sensor AS s
									INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
									WHERE ubicacion='$ubicacion'
									ORDER BY s.rut
								)
								AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
								AND rar.fecha_apertura::TIME BETWEEN '07:00:00' AND '07:59:59'
								AND rar.identificador_sensor_puerta=
								(
									SELECT s.identificador_sensor
									FROM sensor AS s
									INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
									WHERE us.ubicacion='$ubicacion'
									AND s.rut=rar.rut_paciente
								)
								GROUP BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas,rar.rut_paciente
								ORDER BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas
							)AS total
							GROUP BY rut_paciente
						)::INT[]
					,
					array_fill(0,array[
						(
						SELECT COUNT(*) from
						(
							SELECT DISTINCT(s.rut)
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE ubicacion='$ubicacion'
							ORDER BY s.rut
						)AS a)::INT
					])
						)
					)AS datos
				
					limit (SELECT COUNT(*) from
						(SELECT DISTINCT(s.rut)
						FROM sensor AS s
						INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
						WHERE ubicacion='$ubicacion'
						ORDER BY s.rut
						)AS a)
					)AS b
				)::REAL AS desviacion_07,
				(
					SELECT 
					COALESCE(SUM(cantidad)/cantidad_pacientes,0) AS promedio
					FROM
					(
						SELECT COUNT(*)AS cantidad,
						rut_paciente,
						cantidad_pacientes
						FROM
						(
							SELECT 
							rar.fecha_apertura,
							rar.rut_paciente,
							(
							SELECT COUNT(*) from
								(SELECT DISTINCT(s.rut)
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE ubicacion='$ubicacion'
								ORDER BY s.rut
								)AS a
							)AS cantidad_pacientes
							FROM resumen_acciones_repetidas AS rar
							INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
							INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
							WHERE rut_paciente IN 
							(
								SELECT DISTINCT(s.rut)
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE ubicacion='$ubicacion'
								ORDER BY s.rut
							)
							AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
							AND (rar.fecha_apertura::TIME BETWEEN '08:00:00' AND '08:59:59')
							AND rar.identificador_sensor_puerta=
							(
								SELECT s.identificador_sensor
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE us.ubicacion='$ubicacion'
								AND s.rut=rar.rut_paciente
							)
							GROUP BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas,rar.rut_paciente
							ORDER BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas
						)AS total
						GROUP BY rut_paciente,cantidad_pacientes
					) AS a
					GROUP BY cantidad_pacientes
				)::REAL AS promedio_08,
				(
					select coalesce(stddev(datos),0) FROM(
					select unnest(
					array_cat(
						array(
							SELECT COUNT(*)AS cantidad
							FROM
							(
								SELECT 
								rar.fecha_apertura,
								rar.rut_paciente
								FROM resumen_acciones_repetidas AS rar
								INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
								INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
								WHERE rut_paciente IN 
								(
									SELECT DISTINCT(s.rut)
									FROM sensor AS s
									INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
									WHERE ubicacion='$ubicacion'
									ORDER BY s.rut
								)
								AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
								AND rar.fecha_apertura::TIME BETWEEN '08:00:00' AND '08:59:59'
								AND rar.identificador_sensor_puerta=
								(
									SELECT s.identificador_sensor
									FROM sensor AS s
									INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
									WHERE us.ubicacion='$ubicacion'
									AND s.rut=rar.rut_paciente
								)
								GROUP BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas,rar.rut_paciente
								ORDER BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas
							)AS total
							GROUP BY rut_paciente
						)::INT[]
					,
					array_fill(0,array[
						(
						SELECT COUNT(*) from
						(
							SELECT DISTINCT(s.rut)
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE ubicacion='$ubicacion'
							ORDER BY s.rut
						)AS a)::INT
					])
						)
					)AS datos
				
					limit (SELECT COUNT(*) from
						(SELECT DISTINCT(s.rut)
						FROM sensor AS s
						INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
						WHERE ubicacion='$ubicacion'
						ORDER BY s.rut
						)AS a)
					)AS b
				)::REAL AS desviacion_08,
				(
					SELECT 
					COALESCE(SUM(cantidad)/cantidad_pacientes,0) AS promedio
					FROM
					(
						SELECT COUNT(*)AS cantidad,
						rut_paciente,
						cantidad_pacientes
						FROM
						(
							SELECT 
							rar.fecha_apertura,
							rar.rut_paciente,
							(
							SELECT COUNT(*) from
								(SELECT DISTINCT(s.rut)
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE ubicacion='$ubicacion'
								ORDER BY s.rut
								)AS a
							)AS cantidad_pacientes
							FROM resumen_acciones_repetidas AS rar
							INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
							INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
							WHERE rut_paciente IN 
							(
								SELECT DISTINCT(s.rut)
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE ubicacion='$ubicacion'
								ORDER BY s.rut
							)
							AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
							AND (rar.fecha_apertura::TIME BETWEEN '09:00:00' AND '09:59:59')
							AND rar.identificador_sensor_puerta=
							(
								SELECT s.identificador_sensor
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE us.ubicacion='$ubicacion'
								AND s.rut=rar.rut_paciente
							)
							GROUP BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas,rar.rut_paciente
							ORDER BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas
						)AS total
						GROUP BY rut_paciente,cantidad_pacientes
					) AS a
					GROUP BY cantidad_pacientes
				)::REAL AS promedio_09,
				(
					select coalesce(stddev(datos),0) FROM(
					select unnest(
					array_cat(
						array(
							SELECT COUNT(*)AS cantidad
							FROM
							(
								SELECT 
								rar.fecha_apertura,
								rar.rut_paciente
								FROM resumen_acciones_repetidas AS rar
								INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
								INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
								WHERE rut_paciente IN 
								(
									SELECT DISTINCT(s.rut)
									FROM sensor AS s
									INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
									WHERE ubicacion='$ubicacion'
									ORDER BY s.rut
								)
								AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
								AND rar.fecha_apertura::TIME BETWEEN '09:00:00' AND '09:59:59'
								AND rar.identificador_sensor_puerta=
								(
									SELECT s.identificador_sensor
									FROM sensor AS s
									INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
									WHERE us.ubicacion='$ubicacion'
									AND s.rut=rar.rut_paciente
								)
								GROUP BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas,rar.rut_paciente
								ORDER BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas
							)AS total
							GROUP BY rut_paciente
						)::INT[]
					,
					array_fill(0,array[
						(
						SELECT COUNT(*) from
						(
							SELECT DISTINCT(s.rut)
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE ubicacion='$ubicacion'
							ORDER BY s.rut
						)AS a)::INT
					])
						)
					)AS datos
				
					limit (SELECT COUNT(*) from
						(SELECT DISTINCT(s.rut)
						FROM sensor AS s
						INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
						WHERE ubicacion='$ubicacion'
						ORDER BY s.rut
						)AS a)
					)AS b
				)::REAL AS desviacion_09,
				(
					SELECT 
					COALESCE(SUM(cantidad)/cantidad_pacientes,0) AS promedio
					FROM
					(
						SELECT COUNT(*)AS cantidad,
						rut_paciente,
						cantidad_pacientes
						FROM
						(
							SELECT 
							rar.fecha_apertura,
							rar.rut_paciente,
							(
							SELECT COUNT(*) from
								(SELECT DISTINCT(s.rut)
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE ubicacion='$ubicacion'
								ORDER BY s.rut
								)AS a
							)AS cantidad_pacientes
							FROM resumen_acciones_repetidas AS rar
							INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
							INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
							WHERE rut_paciente IN 
							(
								SELECT DISTINCT(s.rut)
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE ubicacion='$ubicacion'
								ORDER BY s.rut
							)
							AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
							AND (rar.fecha_apertura::TIME BETWEEN '10:00:00' AND '10:59:59')
							AND rar.identificador_sensor_puerta=
							(
								SELECT s.identificador_sensor
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE us.ubicacion='$ubicacion'
								AND s.rut=rar.rut_paciente
							)
							GROUP BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas,rar.rut_paciente
							ORDER BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas
						)AS total
						GROUP BY rut_paciente,cantidad_pacientes
					) AS a
					GROUP BY cantidad_pacientes
				)::REAL AS promedio_10,
				(
					select coalesce(stddev(datos),0) FROM(
					select unnest(
					array_cat(
						array(
							SELECT COUNT(*)AS cantidad
							FROM
							(
								SELECT 
								rar.fecha_apertura,
								rar.rut_paciente
								FROM resumen_acciones_repetidas AS rar
								INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
								INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
								WHERE rut_paciente IN 
								(
									SELECT DISTINCT(s.rut)
									FROM sensor AS s
									INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
									WHERE ubicacion='$ubicacion'
									ORDER BY s.rut
								)
								AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
								AND rar.fecha_apertura::TIME BETWEEN '10:00:00' AND '10:59:59'
								AND rar.identificador_sensor_puerta=
								(
									SELECT s.identificador_sensor
									FROM sensor AS s
									INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
									WHERE us.ubicacion='$ubicacion'
									AND s.rut=rar.rut_paciente
								)
								GROUP BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas,rar.rut_paciente
								ORDER BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas
							)AS total
							GROUP BY rut_paciente
						)::INT[]
					,
					array_fill(0,array[
						(
						SELECT COUNT(*) from
						(
							SELECT DISTINCT(s.rut)
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE ubicacion='$ubicacion'
							ORDER BY s.rut
						)AS a)::INT
					])
						)
					)AS datos
				
					limit (SELECT COUNT(*) from
						(SELECT DISTINCT(s.rut)
						FROM sensor AS s
						INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
						WHERE ubicacion='$ubicacion'
						ORDER BY s.rut
						)AS a)
					)AS b
				)::REAL AS desviacion_10,
				(
					SELECT 
					COALESCE(SUM(cantidad)/cantidad_pacientes,0) AS promedio
					FROM
					(
						SELECT COUNT(*)AS cantidad,
						rut_paciente,
						cantidad_pacientes
						FROM
						(
							SELECT 
							rar.fecha_apertura,
							rar.rut_paciente,
							(
							SELECT COUNT(*) from
								(SELECT DISTINCT(s.rut)
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE ubicacion='$ubicacion'
								ORDER BY s.rut
								)AS a
							)AS cantidad_pacientes
							FROM resumen_acciones_repetidas AS rar
							INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
							INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
							WHERE rut_paciente IN 
							(
								SELECT DISTINCT(s.rut)
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE ubicacion='$ubicacion'
								ORDER BY s.rut
							)
							AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
							AND (rar.fecha_apertura::TIME BETWEEN '11:00:00' AND '11:59:59')
							AND rar.identificador_sensor_puerta=
							(
								SELECT s.identificador_sensor
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE us.ubicacion='$ubicacion'
								AND s.rut=rar.rut_paciente
							)
							GROUP BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas,rar.rut_paciente
							ORDER BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas
						)AS total
						GROUP BY rut_paciente,cantidad_pacientes
					) AS a
					GROUP BY cantidad_pacientes
				)::REAL AS promedio_11,
				(
					select coalesce(stddev(datos),0) FROM(
					select unnest(
					array_cat(
						array(
							SELECT COUNT(*)AS cantidad
							FROM
							(
								SELECT 
								rar.fecha_apertura,
								rar.rut_paciente
								FROM resumen_acciones_repetidas AS rar
								INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
								INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
								WHERE rut_paciente IN 
								(
									SELECT DISTINCT(s.rut)
									FROM sensor AS s
									INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
									WHERE ubicacion='$ubicacion'
									ORDER BY s.rut
								)
								AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
								AND rar.fecha_apertura::TIME BETWEEN '11:00:00' AND '11:59:59'
								AND rar.identificador_sensor_puerta=
								(
									SELECT s.identificador_sensor
									FROM sensor AS s
									INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
									WHERE us.ubicacion='$ubicacion'
									AND s.rut=rar.rut_paciente
								)
								GROUP BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas,rar.rut_paciente
								ORDER BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas
							)AS total
							GROUP BY rut_paciente
						)::INT[]
					,
					array_fill(0,array[
						(
						SELECT COUNT(*) from
						(
							SELECT DISTINCT(s.rut)
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE ubicacion='$ubicacion'
							ORDER BY s.rut
						)AS a)::INT
					])
						)
					)AS datos
				
					limit (SELECT COUNT(*) from
						(SELECT DISTINCT(s.rut)
						FROM sensor AS s
						INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
						WHERE ubicacion='$ubicacion'
						ORDER BY s.rut
						)AS a)
					)AS b
				)::REAL AS desviacion_11,
				(
					SELECT 
					COALESCE(SUM(cantidad)/cantidad_pacientes,0) AS promedio
					FROM
					(
						SELECT COUNT(*)AS cantidad,
						rut_paciente,
						cantidad_pacientes
						FROM
						(
							SELECT 
							rar.fecha_apertura,
							rar.rut_paciente,
							(
							SELECT COUNT(*) from
								(SELECT DISTINCT(s.rut)
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE ubicacion='$ubicacion'
								ORDER BY s.rut
								)AS a
							)AS cantidad_pacientes
							FROM resumen_acciones_repetidas AS rar
							INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
							INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
							WHERE rut_paciente IN 
							(
								SELECT DISTINCT(s.rut)
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE ubicacion='$ubicacion'
								ORDER BY s.rut
							)
							AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
							AND (rar.fecha_apertura::TIME BETWEEN '12:00:00' AND '12:59:59')
							AND rar.identificador_sensor_puerta=
							(
								SELECT s.identificador_sensor
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE us.ubicacion='$ubicacion'
								AND s.rut=rar.rut_paciente
							)
							GROUP BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas,rar.rut_paciente
							ORDER BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas
						)AS total
						GROUP BY rut_paciente,cantidad_pacientes
					) AS a
					GROUP BY cantidad_pacientes
				)::REAL AS promedio_12,
				(
					select coalesce(stddev(datos),0) FROM(
					select unnest(
					array_cat(
						array(
							SELECT COUNT(*)AS cantidad
							FROM
							(
								SELECT 
								rar.fecha_apertura,
								rar.rut_paciente
								FROM resumen_acciones_repetidas AS rar
								INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
								INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
								WHERE rut_paciente IN 
								(
									SELECT DISTINCT(s.rut)
									FROM sensor AS s
									INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
									WHERE ubicacion='$ubicacion'
									ORDER BY s.rut
								)
								AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
								AND rar.fecha_apertura::TIME BETWEEN '12:00:00' AND '12:59:59'
								AND rar.identificador_sensor_puerta=
								(
									SELECT s.identificador_sensor
									FROM sensor AS s
									INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
									WHERE us.ubicacion='$ubicacion'
									AND s.rut=rar.rut_paciente
								)
								GROUP BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas,rar.rut_paciente
								ORDER BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas
							)AS total
							GROUP BY rut_paciente
						)::INT[]
					,
					array_fill(0,array[
						(
						SELECT COUNT(*) from
						(
							SELECT DISTINCT(s.rut)
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE ubicacion='$ubicacion'
							ORDER BY s.rut
						)AS a)::INT
					])
						)
					)AS datos
				
					limit (SELECT COUNT(*) from
						(SELECT DISTINCT(s.rut)
						FROM sensor AS s
						INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
						WHERE ubicacion='$ubicacion'
						ORDER BY s.rut
						)AS a)
					)AS b
				)::REAL AS desviacion_12,
				(
					SELECT 
					COALESCE(SUM(cantidad)/cantidad_pacientes,0) AS promedio
					FROM
					(
						SELECT COUNT(*)AS cantidad,
						rut_paciente,
						cantidad_pacientes
						FROM
						(
							SELECT 
							rar.fecha_apertura,
							rar.rut_paciente,
							(
							SELECT COUNT(*) from
								(SELECT DISTINCT(s.rut)
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE ubicacion='$ubicacion'
								ORDER BY s.rut
								)AS a
							)AS cantidad_pacientes
							FROM resumen_acciones_repetidas AS rar
							INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
							INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
							WHERE rut_paciente IN 
							(
								SELECT DISTINCT(s.rut)
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE ubicacion='$ubicacion'
								ORDER BY s.rut
							)
							AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
							AND (rar.fecha_apertura::TIME BETWEEN '13:00:00' AND '13:59:59')
							AND rar.identificador_sensor_puerta=
							(
								SELECT s.identificador_sensor
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE us.ubicacion='$ubicacion'
								AND s.rut=rar.rut_paciente
							)
							GROUP BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas,rar.rut_paciente
							ORDER BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas
						)AS total
						GROUP BY rut_paciente,cantidad_pacientes
					) AS a
					GROUP BY cantidad_pacientes
				)::REAL AS promedio_13,
				(
					select coalesce(stddev(datos),0) FROM(
					select unnest(
					array_cat(
						array(
							SELECT COUNT(*)AS cantidad
							FROM
							(
								SELECT 
								rar.fecha_apertura,
								rar.rut_paciente
								FROM resumen_acciones_repetidas AS rar
								INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
								INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
								WHERE rut_paciente IN 
								(
									SELECT DISTINCT(s.rut)
									FROM sensor AS s
									INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
									WHERE ubicacion='$ubicacion'
									ORDER BY s.rut
								)
								AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
								AND rar.fecha_apertura::TIME BETWEEN '13:00:00' AND '13:59:59'
								AND rar.identificador_sensor_puerta=
								(
									SELECT s.identificador_sensor
									FROM sensor AS s
									INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
									WHERE us.ubicacion='$ubicacion'
									AND s.rut=rar.rut_paciente
								)
								GROUP BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas,rar.rut_paciente
								ORDER BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas
							)AS total
							GROUP BY rut_paciente
						)::INT[]
					,
					array_fill(0,array[
						(
						SELECT COUNT(*) from
						(
							SELECT DISTINCT(s.rut)
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE ubicacion='$ubicacion'
							ORDER BY s.rut
						)AS a)::INT
					])
						)
					)AS datos
				
					limit (SELECT COUNT(*) from
						(SELECT DISTINCT(s.rut)
						FROM sensor AS s
						INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
						WHERE ubicacion='$ubicacion'
						ORDER BY s.rut
						)AS a)
					)AS b
				)::REAL AS desviacion_13,
				(
					SELECT 
					COALESCE(SUM(cantidad)/cantidad_pacientes,0) AS promedio
					FROM
					(
						SELECT COUNT(*)AS cantidad,
						rut_paciente,
						cantidad_pacientes
						FROM
						(
							SELECT 
							rar.fecha_apertura,
							rar.rut_paciente,
							(
							SELECT COUNT(*) from
								(SELECT DISTINCT(s.rut)
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE ubicacion='$ubicacion'
								ORDER BY s.rut
								)AS a
							)AS cantidad_pacientes
							FROM resumen_acciones_repetidas AS rar
							INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
							INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
							WHERE rut_paciente IN 
							(
								SELECT DISTINCT(s.rut)
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE ubicacion='$ubicacion'
								ORDER BY s.rut
							)
							AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
							AND (rar.fecha_apertura::TIME BETWEEN '14:00:00' AND '14:59:59')
							AND rar.identificador_sensor_puerta=
							(
								SELECT s.identificador_sensor
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE us.ubicacion='$ubicacion'
								AND s.rut=rar.rut_paciente
							)
							GROUP BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas,rar.rut_paciente
							ORDER BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas
						)AS total
						GROUP BY rut_paciente,cantidad_pacientes
					) AS a
					GROUP BY cantidad_pacientes
				)::REAL AS promedio_14,
				(
					select coalesce(stddev(datos),0) FROM(
					select unnest(
					array_cat(
						array(
							SELECT COUNT(*)AS cantidad
							FROM
							(
								SELECT 
								rar.fecha_apertura,
								rar.rut_paciente
								FROM resumen_acciones_repetidas AS rar
								INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
								INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
								WHERE rut_paciente IN 
								(
									SELECT DISTINCT(s.rut)
									FROM sensor AS s
									INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
									WHERE ubicacion='$ubicacion'
									ORDER BY s.rut
								)
								AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
								AND rar.fecha_apertura::TIME BETWEEN '14:00:00' AND '14:59:59'
								AND rar.identificador_sensor_puerta=
								(
									SELECT s.identificador_sensor
									FROM sensor AS s
									INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
									WHERE us.ubicacion='$ubicacion'
									AND s.rut=rar.rut_paciente
								)
								GROUP BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas,rar.rut_paciente
								ORDER BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas
							)AS total
							GROUP BY rut_paciente
						)::INT[]
					,
					array_fill(0,array[
						(
						SELECT COUNT(*) from
						(
							SELECT DISTINCT(s.rut)
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE ubicacion='$ubicacion'
							ORDER BY s.rut
						)AS a)::INT
					])
						)
					)AS datos
				
					limit (SELECT COUNT(*) from
						(SELECT DISTINCT(s.rut)
						FROM sensor AS s
						INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
						WHERE ubicacion='$ubicacion'
						ORDER BY s.rut
						)AS a)
					)AS b
				)::REAL AS desviacion_14,
				(
					SELECT 
					COALESCE(SUM(cantidad)/cantidad_pacientes,0) AS promedio
					FROM
					(
						SELECT COUNT(*)AS cantidad,
						rut_paciente,
						cantidad_pacientes
						FROM
						(
							SELECT 
							rar.fecha_apertura,
							rar.rut_paciente,
							(
							SELECT COUNT(*) from
								(SELECT DISTINCT(s.rut)
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE ubicacion='$ubicacion'
								ORDER BY s.rut
								)AS a
							)AS cantidad_pacientes
							FROM resumen_acciones_repetidas AS rar
							INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
							INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
							WHERE rut_paciente IN 
							(
								SELECT DISTINCT(s.rut)
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE ubicacion='$ubicacion'
								ORDER BY s.rut
							)
							AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
							AND (rar.fecha_apertura::TIME BETWEEN '15:00:00' AND '15:59:59')
							AND rar.identificador_sensor_puerta=
							(
								SELECT s.identificador_sensor
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE us.ubicacion='$ubicacion'
								AND s.rut=rar.rut_paciente
							)
							GROUP BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas,rar.rut_paciente
							ORDER BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas
						)AS total
						GROUP BY rut_paciente,cantidad_pacientes
					) AS a
					GROUP BY cantidad_pacientes
				)::REAL AS promedio_15,
				(
					select coalesce(stddev(datos),0) FROM(
					select unnest(
					array_cat(
						array(
							SELECT COUNT(*)AS cantidad
							FROM
							(
								SELECT 
								rar.fecha_apertura,
								rar.rut_paciente
								FROM resumen_acciones_repetidas AS rar
								INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
								INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
								WHERE rut_paciente IN 
								(
									SELECT DISTINCT(s.rut)
									FROM sensor AS s
									INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
									WHERE ubicacion='$ubicacion'
									ORDER BY s.rut
								)
								AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
								AND rar.fecha_apertura::TIME BETWEEN '15:00:00' AND '15:59:59'
								AND rar.identificador_sensor_puerta=
								(
									SELECT s.identificador_sensor
									FROM sensor AS s
									INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
									WHERE us.ubicacion='$ubicacion'
									AND s.rut=rar.rut_paciente
								)
								GROUP BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas,rar.rut_paciente
								ORDER BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas
							)AS total
							GROUP BY rut_paciente
						)::INT[]
					,
					array_fill(0,array[
						(
						SELECT COUNT(*) from
						(
							SELECT DISTINCT(s.rut)
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE ubicacion='$ubicacion'
							ORDER BY s.rut
						)AS a)::INT
					])
						)
					)AS datos
				
					limit (SELECT COUNT(*) from
						(SELECT DISTINCT(s.rut)
						FROM sensor AS s
						INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
						WHERE ubicacion='$ubicacion'
						ORDER BY s.rut
						)AS a)
					)AS b
				)::REAL AS desviacion_15,
				(
					SELECT 
					COALESCE(SUM(cantidad)/cantidad_pacientes,0) AS promedio
					FROM
					(
						SELECT COUNT(*)AS cantidad,
						rut_paciente,
						cantidad_pacientes
						FROM
						(
							SELECT 
							rar.fecha_apertura,
							rar.rut_paciente,
							(
							SELECT COUNT(*) from
								(SELECT DISTINCT(s.rut)
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE ubicacion='$ubicacion'
								ORDER BY s.rut
								)AS a
							)AS cantidad_pacientes
							FROM resumen_acciones_repetidas AS rar
							INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
							INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
							WHERE rut_paciente IN 
							(
								SELECT DISTINCT(s.rut)
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE ubicacion='$ubicacion'
								ORDER BY s.rut
							)
							AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
							AND (rar.fecha_apertura::TIME BETWEEN '16:00:00' AND '16:59:59')
							AND rar.identificador_sensor_puerta=
							(
								SELECT s.identificador_sensor
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE us.ubicacion='$ubicacion'
								AND s.rut=rar.rut_paciente
							)
							GROUP BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas,rar.rut_paciente
							ORDER BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas
						)AS total
						GROUP BY rut_paciente,cantidad_pacientes
					) AS a
					GROUP BY cantidad_pacientes
				)::REAL AS promedio_16,
				(
					select coalesce(stddev(datos),0) FROM(
					select unnest(
					array_cat(
						array(
							SELECT COUNT(*)AS cantidad
							FROM
							(
								SELECT 
								rar.fecha_apertura,
								rar.rut_paciente
								FROM resumen_acciones_repetidas AS rar
								INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
								INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
								WHERE rut_paciente IN 
								(
									SELECT DISTINCT(s.rut)
									FROM sensor AS s
									INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
									WHERE ubicacion='$ubicacion'
									ORDER BY s.rut
								)
								AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
								AND rar.fecha_apertura::TIME BETWEEN '16:00:00' AND '16:59:59'
								AND rar.identificador_sensor_puerta=
								(
									SELECT s.identificador_sensor
									FROM sensor AS s
									INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
									WHERE us.ubicacion='$ubicacion'
									AND s.rut=rar.rut_paciente
								)
								GROUP BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas,rar.rut_paciente
								ORDER BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas
							)AS total
							GROUP BY rut_paciente
						)::INT[]
					,
					array_fill(0,array[
						(
						SELECT COUNT(*) from
						(
							SELECT DISTINCT(s.rut)
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE ubicacion='$ubicacion'
							ORDER BY s.rut
						)AS a)::INT
					])
						)
					)AS datos
				
					limit (SELECT COUNT(*) from
						(SELECT DISTINCT(s.rut)
						FROM sensor AS s
						INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
						WHERE ubicacion='$ubicacion'
						ORDER BY s.rut
						)AS a)
					)AS b
				)::REAL AS desviacion_16,
				(
					SELECT 
					COALESCE(SUM(cantidad)/cantidad_pacientes,0) AS promedio
					FROM
					(
						SELECT COUNT(*)AS cantidad,
						rut_paciente,
						cantidad_pacientes
						FROM
						(
							SELECT 
							rar.fecha_apertura,
							rar.rut_paciente,
							(
							SELECT COUNT(*) from
								(SELECT DISTINCT(s.rut)
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE ubicacion='$ubicacion'
								ORDER BY s.rut
								)AS a
							)AS cantidad_pacientes
							FROM resumen_acciones_repetidas AS rar
							INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
							INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
							WHERE rut_paciente IN 
							(
								SELECT DISTINCT(s.rut)
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE ubicacion='$ubicacion'
								ORDER BY s.rut
							)
							AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
							AND (rar.fecha_apertura::TIME BETWEEN '17:00:00' AND '17:59:59')
							AND rar.identificador_sensor_puerta=
							(
								SELECT s.identificador_sensor
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE us.ubicacion='$ubicacion'
								AND s.rut=rar.rut_paciente
							)
							GROUP BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas,rar.rut_paciente
							ORDER BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas
						)AS total
						GROUP BY rut_paciente,cantidad_pacientes
					) AS a
					GROUP BY cantidad_pacientes
				)::REAL AS promedio_17,
				(
					select coalesce(stddev(datos),0) FROM(
					select unnest(
					array_cat(
						array(
							SELECT COUNT(*)AS cantidad
							FROM
							(
								SELECT 
								rar.fecha_apertura,
								rar.rut_paciente
								FROM resumen_acciones_repetidas AS rar
								INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
								INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
								WHERE rut_paciente IN 
								(
									SELECT DISTINCT(s.rut)
									FROM sensor AS s
									INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
									WHERE ubicacion='$ubicacion'
									ORDER BY s.rut
								)
								AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
								AND rar.fecha_apertura::TIME BETWEEN '17:00:00' AND '17:59:59'
								AND rar.identificador_sensor_puerta=
								(
									SELECT s.identificador_sensor
									FROM sensor AS s
									INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
									WHERE us.ubicacion='$ubicacion'
									AND s.rut=rar.rut_paciente
								)
								GROUP BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas,rar.rut_paciente
								ORDER BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas
							)AS total
							GROUP BY rut_paciente
						)::INT[]
					,
					array_fill(0,array[
						(
						SELECT COUNT(*) from
						(
							SELECT DISTINCT(s.rut)
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE ubicacion='$ubicacion'
							ORDER BY s.rut
						)AS a)::INT
					])
						)
					)AS datos
				
					limit (SELECT COUNT(*) from
						(SELECT DISTINCT(s.rut)
						FROM sensor AS s
						INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
						WHERE ubicacion='$ubicacion'
						ORDER BY s.rut
						)AS a)
					)AS b
				)::REAL AS desviacion_17,
				(
					SELECT 
					COALESCE(SUM(cantidad)/cantidad_pacientes,0) AS promedio
					FROM
					(
						SELECT COUNT(*)AS cantidad,
						rut_paciente,
						cantidad_pacientes
						FROM
						(
							SELECT 
							rar.fecha_apertura,
							rar.rut_paciente,
							(
							SELECT COUNT(*) from
								(SELECT DISTINCT(s.rut)
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE ubicacion='$ubicacion'
								ORDER BY s.rut
								)AS a
							)AS cantidad_pacientes
							FROM resumen_acciones_repetidas AS rar
							INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
							INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
							WHERE rut_paciente IN 
							(
								SELECT DISTINCT(s.rut)
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE ubicacion='$ubicacion'
								ORDER BY s.rut
							)
							AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
							AND (rar.fecha_apertura::TIME BETWEEN '18:00:00' AND '18:59:59')
							AND rar.identificador_sensor_puerta=
							(
								SELECT s.identificador_sensor
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE us.ubicacion='$ubicacion'
								AND s.rut=rar.rut_paciente
							)
							GROUP BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas,rar.rut_paciente
							ORDER BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas
						)AS total
						GROUP BY rut_paciente,cantidad_pacientes
					) AS a
					GROUP BY cantidad_pacientes
				)::REAL AS promedio_18,
				(
					select coalesce(stddev(datos),0) FROM(
					select unnest(
					array_cat(
						array(
							SELECT COUNT(*)AS cantidad
							FROM
							(
								SELECT 
								rar.fecha_apertura,
								rar.rut_paciente
								FROM resumen_acciones_repetidas AS rar
								INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
								INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
								WHERE rut_paciente IN 
								(
									SELECT DISTINCT(s.rut)
									FROM sensor AS s
									INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
									WHERE ubicacion='$ubicacion'
									ORDER BY s.rut
								)
								AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
								AND rar.fecha_apertura::TIME BETWEEN '18:00:00' AND '18:59:59'
								AND rar.identificador_sensor_puerta=
								(
									SELECT s.identificador_sensor
									FROM sensor AS s
									INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
									WHERE us.ubicacion='$ubicacion'
									AND s.rut=rar.rut_paciente
								)
								GROUP BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas,rar.rut_paciente
								ORDER BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas
							)AS total
							GROUP BY rut_paciente
						)::INT[]
					,
					array_fill(0,array[
						(
						SELECT COUNT(*) from
						(
							SELECT DISTINCT(s.rut)
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE ubicacion='$ubicacion'
							ORDER BY s.rut
						)AS a)::INT
					])
						)
					)AS datos
				
					limit (SELECT COUNT(*) from
						(SELECT DISTINCT(s.rut)
						FROM sensor AS s
						INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
						WHERE ubicacion='$ubicacion'
						ORDER BY s.rut
						)AS a)
					)AS b
				)::REAL AS desviacion_18,
				(
					SELECT 
					COALESCE(SUM(cantidad)/cantidad_pacientes,0) AS promedio
					FROM
					(
						SELECT COUNT(*)AS cantidad,
						rut_paciente,
						cantidad_pacientes
						FROM
						(
							SELECT 
							rar.fecha_apertura,
							rar.rut_paciente,
							(
							SELECT COUNT(*) from
								(SELECT DISTINCT(s.rut)
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE ubicacion='$ubicacion'
								ORDER BY s.rut
								)AS a
							)AS cantidad_pacientes
							FROM resumen_acciones_repetidas AS rar
							INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
							INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
							WHERE rut_paciente IN 
							(
								SELECT DISTINCT(s.rut)
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE ubicacion='$ubicacion'
								ORDER BY s.rut
							)
							AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
							AND (rar.fecha_apertura::TIME BETWEEN '19:00:00' AND '19:59:59')
							AND rar.identificador_sensor_puerta=
							(
								SELECT s.identificador_sensor
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE us.ubicacion='$ubicacion'
								AND s.rut=rar.rut_paciente
							)
							GROUP BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas,rar.rut_paciente
							ORDER BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas
						)AS total
						GROUP BY rut_paciente,cantidad_pacientes
					) AS a
					GROUP BY cantidad_pacientes
				)::REAL as promedio_19,
				(
					select coalesce(stddev(datos),0) FROM(
					select unnest(
					array_cat(
						array(
							SELECT COUNT(*)AS cantidad
							FROM
							(
								SELECT 
								rar.fecha_apertura,
								rar.rut_paciente
								FROM resumen_acciones_repetidas AS rar
								INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
								INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
								WHERE rut_paciente IN 
								(
									SELECT DISTINCT(s.rut)
									FROM sensor AS s
									INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
									WHERE ubicacion='$ubicacion'
									ORDER BY s.rut
								)
								AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
								AND rar.fecha_apertura::TIME BETWEEN '19:00:00' AND '19:59:59'
								AND rar.identificador_sensor_puerta=
								(
									SELECT s.identificador_sensor
									FROM sensor AS s
									INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
									WHERE us.ubicacion='$ubicacion'
									AND s.rut=rar.rut_paciente
								)
								GROUP BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas,rar.rut_paciente
								ORDER BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas
							)AS total
							GROUP BY rut_paciente
						)::INT[]
					,
					array_fill(0,array[
						(
						SELECT COUNT(*) from
						(
							SELECT DISTINCT(s.rut)
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE ubicacion='$ubicacion'
							ORDER BY s.rut
						)AS a)::INT
					])
						)
					)AS datos
				
					limit (SELECT COUNT(*) from
						(SELECT DISTINCT(s.rut)
						FROM sensor AS s
						INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
						WHERE ubicacion='$ubicacion'
						ORDER BY s.rut
						)AS a)
					)AS b
				)::REAL AS desviacion_19,
				(
					SELECT 
					COALESCE(SUM(cantidad)/cantidad_pacientes,0) AS promedio
					FROM
					(
						SELECT COUNT(*)AS cantidad,
						rut_paciente,
						cantidad_pacientes
						FROM
						(
							SELECT 
							rar.fecha_apertura,
							rar.rut_paciente,
							(
							SELECT COUNT(*) from
								(SELECT DISTINCT(s.rut)
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE ubicacion='$ubicacion'
								ORDER BY s.rut
								)AS a
							)AS cantidad_pacientes
							FROM resumen_acciones_repetidas AS rar
							INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
							INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
							WHERE rut_paciente IN 
							(
								SELECT DISTINCT(s.rut)
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE ubicacion='$ubicacion'
								ORDER BY s.rut
							)
							AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
							AND (rar.fecha_apertura::TIME BETWEEN '20:00:00' AND '20:59:59')
							AND rar.identificador_sensor_puerta=
							(
								SELECT s.identificador_sensor
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE us.ubicacion='$ubicacion'
								AND s.rut=rar.rut_paciente
							)
							GROUP BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas,rar.rut_paciente
							ORDER BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas
						)AS total
						GROUP BY rut_paciente,cantidad_pacientes
					) AS a
					GROUP BY cantidad_pacientes
				)::REAL AS promedio_20,
				(
					select coalesce(stddev(datos),0) FROM(
					select unnest(
					array_cat(
						array(
							SELECT COUNT(*)AS cantidad
							FROM
							(
								SELECT 
								rar.fecha_apertura,
								rar.rut_paciente
								FROM resumen_acciones_repetidas AS rar
								INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
								INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
								WHERE rut_paciente IN 
								(
									SELECT DISTINCT(s.rut)
									FROM sensor AS s
									INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
									WHERE ubicacion='$ubicacion'
									ORDER BY s.rut
								)
								AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
								AND rar.fecha_apertura::TIME BETWEEN '20:00:00' AND '20:59:59'
								AND rar.identificador_sensor_puerta=
								(
									SELECT s.identificador_sensor
									FROM sensor AS s
									INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
									WHERE us.ubicacion='$ubicacion'
									AND s.rut=rar.rut_paciente
								)
								GROUP BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas,rar.rut_paciente
								ORDER BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas
							)AS total
							GROUP BY rut_paciente
						)::INT[]
					,
					array_fill(0,array[
						(
						SELECT COUNT(*) from
						(
							SELECT DISTINCT(s.rut)
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE ubicacion='$ubicacion'
							ORDER BY s.rut
						)AS a)::INT
					])
						)
					)AS datos
				
					limit (SELECT COUNT(*) from
						(SELECT DISTINCT(s.rut)
						FROM sensor AS s
						INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
						WHERE ubicacion='$ubicacion'
						ORDER BY s.rut
						)AS a)
					)AS b
				)::REAL AS desviacion_20,
				(
					SELECT 
					COALESCE(SUM(cantidad)/cantidad_pacientes,0) AS promedio
					FROM
					(
						SELECT COUNT(*)AS cantidad,
						rut_paciente,
						cantidad_pacientes
						FROM
						(
							SELECT 
							rar.fecha_apertura,
							rar.rut_paciente,
							(
							SELECT COUNT(*) from
								(SELECT DISTINCT(s.rut)
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE ubicacion='$ubicacion'
								ORDER BY s.rut
								)AS a
							)AS cantidad_pacientes
							FROM resumen_acciones_repetidas AS rar
							INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
							INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
							WHERE rut_paciente IN 
							(
								SELECT DISTINCT(s.rut)
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE ubicacion='$ubicacion'
								ORDER BY s.rut
							)
							AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
							AND (rar.fecha_apertura::TIME BETWEEN '21:00:00' AND '21:59:59')
							AND rar.identificador_sensor_puerta=
							(
								SELECT s.identificador_sensor
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE us.ubicacion='$ubicacion'
								AND s.rut=rar.rut_paciente
							)
							GROUP BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas,rar.rut_paciente
							ORDER BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas
						)AS total
						GROUP BY rut_paciente,cantidad_pacientes
					) AS a
					GROUP BY cantidad_pacientes
				)::REAL AS promedio_21,
				(
					select coalesce(stddev(datos),0) FROM(
					select unnest(
					array_cat(
						array(
							SELECT COUNT(*)AS cantidad
							FROM
							(
								SELECT 
								rar.fecha_apertura,
								rar.rut_paciente
								FROM resumen_acciones_repetidas AS rar
								INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
								INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
								WHERE rut_paciente IN 
								(
									SELECT DISTINCT(s.rut)
									FROM sensor AS s
									INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
									WHERE ubicacion='$ubicacion'
									ORDER BY s.rut
								)
								AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
								AND rar.fecha_apertura::TIME BETWEEN '21:00:00' AND '21:59:59'
								AND rar.identificador_sensor_puerta=
								(
									SELECT s.identificador_sensor
									FROM sensor AS s
									INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
									WHERE us.ubicacion='$ubicacion'
									AND s.rut=rar.rut_paciente
								)
								GROUP BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas,rar.rut_paciente
								ORDER BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas
							)AS total
							GROUP BY rut_paciente
						)::INT[]
					,
					array_fill(0,array[
						(
						SELECT COUNT(*) from
						(
							SELECT DISTINCT(s.rut)
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE ubicacion='$ubicacion'
							ORDER BY s.rut
						)AS a)::INT
					])
						)
					)AS datos
				
					limit (SELECT COUNT(*) from
						(SELECT DISTINCT(s.rut)
						FROM sensor AS s
						INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
						WHERE ubicacion='$ubicacion'
						ORDER BY s.rut
						)AS a)
					)AS b
				)::REAL AS desviacion_21,
				(
					SELECT 
					COALESCE(SUM(cantidad)/cantidad_pacientes,0) AS promedio
					FROM
					(
						SELECT COUNT(*)AS cantidad,
						rut_paciente,
						cantidad_pacientes
						FROM
						(
							SELECT 
							rar.fecha_apertura,
							rar.rut_paciente,
							(
							SELECT COUNT(*) from
								(SELECT DISTINCT(s.rut)
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE ubicacion='$ubicacion'
								ORDER BY s.rut
								)AS a
							)AS cantidad_pacientes
							FROM resumen_acciones_repetidas AS rar
							INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
							INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
							WHERE rut_paciente IN 
							(
								SELECT DISTINCT(s.rut)
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE ubicacion='$ubicacion'
								ORDER BY s.rut
							)
							AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
							AND (rar.fecha_apertura::TIME BETWEEN '22:00:00' AND '22:59:59')
							AND rar.identificador_sensor_puerta=
							(
								SELECT s.identificador_sensor
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE us.ubicacion='$ubicacion'
								AND s.rut=rar.rut_paciente
							)
							GROUP BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas,rar.rut_paciente
							ORDER BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas
						)AS total
						GROUP BY rut_paciente,cantidad_pacientes
					) AS a
					GROUP BY cantidad_pacientes
				)::REAL AS promedio_22,
				(
					select coalesce(stddev(datos),0) FROM(
					select unnest(
					array_cat(
						array(
							SELECT COUNT(*)AS cantidad
							FROM
							(
								SELECT 
								rar.fecha_apertura,
								rar.rut_paciente
								FROM resumen_acciones_repetidas AS rar
								INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
								INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
								WHERE rut_paciente IN 
								(
									SELECT DISTINCT(s.rut)
									FROM sensor AS s
									INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
									WHERE ubicacion='$ubicacion'
									ORDER BY s.rut
								)
								AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
								AND rar.fecha_apertura::TIME BETWEEN '22:00:00' AND '22:59:59'
								AND rar.identificador_sensor_puerta=
								(
									SELECT s.identificador_sensor
									FROM sensor AS s
									INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
									WHERE us.ubicacion='$ubicacion'
									AND s.rut=rar.rut_paciente
								)
								GROUP BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas,rar.rut_paciente
								ORDER BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas
							)AS total
							GROUP BY rut_paciente
						)::INT[]
					,
					array_fill(0,array[
						(
						SELECT COUNT(*) from
						(
							SELECT DISTINCT(s.rut)
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE ubicacion='$ubicacion'
							ORDER BY s.rut
						)AS a)::INT
					])
						)
					)AS datos
				
					limit (SELECT COUNT(*) from
						(SELECT DISTINCT(s.rut)
						FROM sensor AS s
						INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
						WHERE ubicacion='$ubicacion'
						ORDER BY s.rut
						)AS a)
					)AS b
				)::REAL AS desviacion_22,
				(
					SELECT 
					COALESCE(SUM(cantidad)/cantidad_pacientes,0) AS promedio
					FROM
					(
						SELECT COUNT(*)AS cantidad,
						rut_paciente,
						cantidad_pacientes
						FROM
						(
							SELECT 
							rar.fecha_apertura,
							rar.rut_paciente,
							(
							SELECT COUNT(*) from
								(SELECT DISTINCT(s.rut)
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE ubicacion='$ubicacion'
								ORDER BY s.rut
								)AS a
							)AS cantidad_pacientes
							FROM resumen_acciones_repetidas AS rar
							INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
							INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
							WHERE rut_paciente IN 
							(
								SELECT DISTINCT(s.rut)
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE ubicacion='$ubicacion'
								ORDER BY s.rut
							)
							AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
							AND (rar.fecha_apertura::TIME BETWEEN '23:00:00' AND '23:59:59')
							AND rar.identificador_sensor_puerta=
							(
								SELECT s.identificador_sensor
								FROM sensor AS s
								INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
								WHERE us.ubicacion='$ubicacion'
								AND s.rut=rar.rut_paciente
							)
							GROUP BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas,rar.rut_paciente
							ORDER BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas
						)AS total
						GROUP BY rut_paciente,cantidad_pacientes
					) AS a
					GROUP BY cantidad_pacientes
				)::REAL AS promedio_23,
				(
					select coalesce(stddev(datos),0) FROM(
					select unnest(
					array_cat(
						array(
							SELECT COUNT(*)AS cantidad
							FROM
							(
								SELECT 
								rar.fecha_apertura,
								rar.rut_paciente
								FROM resumen_acciones_repetidas AS rar
								INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
								INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
								WHERE rut_paciente IN 
								(
									SELECT DISTINCT(s.rut)
									FROM sensor AS s
									INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
									WHERE ubicacion='$ubicacion'
									ORDER BY s.rut
								)
								AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
								AND rar.fecha_apertura::TIME BETWEEN '23:00:00' AND '23:59:59'
								AND rar.identificador_sensor_puerta=
								(
									SELECT s.identificador_sensor
									FROM sensor AS s
									INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
									WHERE us.ubicacion='$ubicacion'
									AND s.rut=rar.rut_paciente
								)
								GROUP BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas,rar.rut_paciente
								ORDER BY rar.fecha_apertura,rar.id_resumen_acciones_repetidas
							)AS total
							GROUP BY rut_paciente
						)::INT[]
					,
					array_fill(0,array[
						(
						SELECT COUNT(*) from
						(
							SELECT DISTINCT(s.rut)
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE ubicacion='$ubicacion'
							ORDER BY s.rut
						)AS a)::INT
					])
						)
					)AS datos
				
					limit (SELECT COUNT(*) from
						(SELECT DISTINCT(s.rut)
						FROM sensor AS s
						INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
						WHERE ubicacion='$ubicacion'
						ORDER BY s.rut
						)AS a)
					)AS b
				)::REAL AS desviacion_23
				"
				)
			);
		return $resultado;
	}
	public static function obtenerCantidadPersonasPorHora($ubicacion)
	{
		$resultado=DB::select(
			DB::raw(
				"
				--busca la cantidad de personas que abrieron la puerta en rango de tiempo
				--variables: sensor
				SELECT
				(
					SELECT COUNT(*)AS cantidad
					FROM
					(
						SELECT 
						DISTINCT(rar.rut_paciente)
						FROM resumen_acciones_repetidas AS rar
						INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
						INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
						WHERE rut_paciente IN 
						(
							SELECT DISTINCT(s.rut)
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE ubicacion='$ubicacion'
							ORDER BY s.rut
						)
						AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
						AND (rar.fecha_apertura::TIME BETWEEN '00:00:00' AND '00:59:59')
						AND rar.identificador_sensor_puerta=
						(
							SELECT s.identificador_sensor
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE us.ubicacion='$ubicacion'
							AND s.rut=rar.rut_paciente
						)
					)AS total
				)AS h00,
				(
					SELECT COUNT(*)AS cantidad
					FROM
					(
						SELECT 
						DISTINCT(rar.rut_paciente)
						FROM resumen_acciones_repetidas AS rar
						INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
						INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
						WHERE rut_paciente IN 
						(
							SELECT DISTINCT(s.rut)
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE ubicacion='$ubicacion'
							ORDER BY s.rut
						)
						AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
						AND (rar.fecha_apertura::TIME BETWEEN '01:00:00' AND '01:59:59')
						AND rar.identificador_sensor_puerta=
						(
							SELECT s.identificador_sensor
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE us.ubicacion='$ubicacion'
							AND s.rut=rar.rut_paciente
						)
					)AS total
				)AS h01,
				(
					SELECT COUNT(*)AS cantidad
					FROM
					(
						SELECT 
						DISTINCT(rar.rut_paciente)
						FROM resumen_acciones_repetidas AS rar
						INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
						INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
						WHERE rut_paciente IN 
						(
							SELECT DISTINCT(s.rut)
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE ubicacion='$ubicacion'
							ORDER BY s.rut
						)
						AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
						AND (rar.fecha_apertura::TIME BETWEEN '02:00:00' AND '02:59:59')
						AND rar.identificador_sensor_puerta=
						(
							SELECT s.identificador_sensor
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE us.ubicacion='$ubicacion'
							AND s.rut=rar.rut_paciente
						)
					)AS total
				)AS h02,
				(
					SELECT COUNT(*)AS cantidad
					FROM
					(
						SELECT 
						DISTINCT(rar.rut_paciente)
						FROM resumen_acciones_repetidas AS rar
						INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
						INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
						WHERE rut_paciente IN 
						(
							SELECT DISTINCT(s.rut)
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE ubicacion='$ubicacion'
							ORDER BY s.rut
						)
						AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
						AND (rar.fecha_apertura::TIME BETWEEN '03:00:00' AND '03:59:59')
						AND rar.identificador_sensor_puerta=
						(
							SELECT s.identificador_sensor
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE us.ubicacion='$ubicacion'
							AND s.rut=rar.rut_paciente
						)
					)AS total
				)AS h03,
				(
					SELECT COUNT(*)AS cantidad
					FROM
					(
						SELECT 
						DISTINCT(rar.rut_paciente)
						FROM resumen_acciones_repetidas AS rar
						INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
						INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
						WHERE rut_paciente IN 
						(
							SELECT DISTINCT(s.rut)
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE ubicacion='$ubicacion'
							ORDER BY s.rut
						)
						AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
						AND (rar.fecha_apertura::TIME BETWEEN '04:00:00' AND '04:59:59')
						AND rar.identificador_sensor_puerta=
						(
							SELECT s.identificador_sensor
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE us.ubicacion='$ubicacion'
							AND s.rut=rar.rut_paciente
						)
					)AS total
				)AS h04,
				(
					SELECT COUNT(*)AS cantidad
					FROM
					(
						SELECT 
						DISTINCT(rar.rut_paciente)
						FROM resumen_acciones_repetidas AS rar
						INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
						INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
						WHERE rut_paciente IN 
						(
							SELECT DISTINCT(s.rut)
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE ubicacion='$ubicacion'
							ORDER BY s.rut
						)
						AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
						AND (rar.fecha_apertura::TIME BETWEEN '05:00:00' AND '05:59:59')
						AND rar.identificador_sensor_puerta=
						(
							SELECT s.identificador_sensor
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE us.ubicacion='$ubicacion'
							AND s.rut=rar.rut_paciente
						)
					)AS total
				)AS h05,
				(
					SELECT COUNT(*)AS cantidad
					FROM
					(
						SELECT 
						DISTINCT(rar.rut_paciente)
						FROM resumen_acciones_repetidas AS rar
						INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
						INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
						WHERE rut_paciente IN 
						(
							SELECT DISTINCT(s.rut)
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE ubicacion='$ubicacion'
							ORDER BY s.rut
						)
						AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
						AND (rar.fecha_apertura::TIME BETWEEN '06:00:00' AND '06:59:59')
						AND rar.identificador_sensor_puerta=
						(
							SELECT s.identificador_sensor
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE us.ubicacion='$ubicacion'
							AND s.rut=rar.rut_paciente
						)
					)AS total
				)AS h06,
				(
					SELECT COUNT(*)AS cantidad
					FROM
					(
						SELECT 
						DISTINCT(rar.rut_paciente)
						FROM resumen_acciones_repetidas AS rar
						INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
						INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
						WHERE rut_paciente IN 
						(
							SELECT DISTINCT(s.rut)
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE ubicacion='$ubicacion'
							ORDER BY s.rut
						)
						AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
						AND (rar.fecha_apertura::TIME BETWEEN '07:00:00' AND '07:59:59')
						AND rar.identificador_sensor_puerta=
						(
							SELECT s.identificador_sensor
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE us.ubicacion='$ubicacion'
							AND s.rut=rar.rut_paciente
						)
					)AS total
				)AS h07,
				(
					SELECT COUNT(*)AS cantidad
					FROM
					(
						SELECT 
						DISTINCT(rar.rut_paciente)
						FROM resumen_acciones_repetidas AS rar
						INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
						INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
						WHERE rut_paciente IN 
						(
							SELECT DISTINCT(s.rut)
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE ubicacion='$ubicacion'
							ORDER BY s.rut
						)
						AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
						AND (rar.fecha_apertura::TIME BETWEEN '08:00:00' AND '08:59:59')
						AND rar.identificador_sensor_puerta=
						(
							SELECT s.identificador_sensor
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE us.ubicacion='$ubicacion'
							AND s.rut=rar.rut_paciente
						)
					)AS total
				)AS h08,
				(
					SELECT COUNT(*)AS cantidad
					FROM
					(
						SELECT 
						DISTINCT(rar.rut_paciente)
						FROM resumen_acciones_repetidas AS rar
						INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
						INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
						WHERE rut_paciente IN 
						(
							SELECT DISTINCT(s.rut)
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE ubicacion='$ubicacion'
							ORDER BY s.rut
						)
						AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
						AND (rar.fecha_apertura::TIME BETWEEN '09:00:00' AND '09:59:59')
						AND rar.identificador_sensor_puerta=
						(
							SELECT s.identificador_sensor
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE us.ubicacion='$ubicacion'
							AND s.rut=rar.rut_paciente
						)
					)AS total
				)AS h09,
				(
					SELECT COUNT(*)AS cantidad
					FROM
					(
						SELECT 
						DISTINCT(rar.rut_paciente)
						FROM resumen_acciones_repetidas AS rar
						INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
						INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
						WHERE rut_paciente IN 
						(
							SELECT DISTINCT(s.rut)
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE ubicacion='$ubicacion'
							ORDER BY s.rut
						)
						AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
						AND (rar.fecha_apertura::TIME BETWEEN '10:00:00' AND '10:59:59')
						AND rar.identificador_sensor_puerta=
						(
							SELECT s.identificador_sensor
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE us.ubicacion='$ubicacion'
							AND s.rut=rar.rut_paciente
						)
					)AS total
				)AS h10,
				(
					SELECT COUNT(*)AS cantidad
					FROM
					(
						SELECT 
						DISTINCT(rar.rut_paciente)
						FROM resumen_acciones_repetidas AS rar
						INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
						INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
						WHERE rut_paciente IN 
						(
							SELECT DISTINCT(s.rut)
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE ubicacion='$ubicacion'
							ORDER BY s.rut
						)
						AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
						AND (rar.fecha_apertura::TIME BETWEEN '11:00:00' AND '11:59:59')
						AND rar.identificador_sensor_puerta=
						(
							SELECT s.identificador_sensor
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE us.ubicacion='$ubicacion'
							AND s.rut=rar.rut_paciente
						)
					)AS total
				)AS h11,
				(
					SELECT COUNT(*)AS cantidad
					FROM
					(
						SELECT 
						DISTINCT(rar.rut_paciente)
						FROM resumen_acciones_repetidas AS rar
						INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
						INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
						WHERE rut_paciente IN 
						(
							SELECT DISTINCT(s.rut)
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE ubicacion='$ubicacion'
							ORDER BY s.rut
						)
						AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
						AND (rar.fecha_apertura::TIME BETWEEN '12:00:00' AND '12:59:59')
						AND rar.identificador_sensor_puerta=
						(
							SELECT s.identificador_sensor
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE us.ubicacion='$ubicacion'
							AND s.rut=rar.rut_paciente
						)
					)AS total
				)AS h12,
				(
					SELECT COUNT(*)AS cantidad
					FROM
					(
						SELECT 
						DISTINCT(rar.rut_paciente)
						FROM resumen_acciones_repetidas AS rar
						INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
						INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
						WHERE rut_paciente IN 
						(
							SELECT DISTINCT(s.rut)
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE ubicacion='$ubicacion'
							ORDER BY s.rut
						)
						AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
						AND (rar.fecha_apertura::TIME BETWEEN '13:00:00' AND '13:59:59')
						AND rar.identificador_sensor_puerta=
						(
							SELECT s.identificador_sensor
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE us.ubicacion='$ubicacion'
							AND s.rut=rar.rut_paciente
						)
					)AS total
				)AS h13,
				(
					SELECT COUNT(*)AS cantidad
					FROM
					(
						SELECT 
						DISTINCT(rar.rut_paciente)
						FROM resumen_acciones_repetidas AS rar
						INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
						INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
						WHERE rut_paciente IN 
						(
							SELECT DISTINCT(s.rut)
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE ubicacion='$ubicacion'
							ORDER BY s.rut
						)
						AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
						AND (rar.fecha_apertura::TIME BETWEEN '14:00:00' AND '14:59:59')
						AND rar.identificador_sensor_puerta=
						(
							SELECT s.identificador_sensor
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE us.ubicacion='$ubicacion'
							AND s.rut=rar.rut_paciente
						)
					)AS total
				)AS h14,
				(
					SELECT COUNT(*)AS cantidad
					FROM
					(
						SELECT 
						DISTINCT(rar.rut_paciente)
						FROM resumen_acciones_repetidas AS rar
						INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
						INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
						WHERE rut_paciente IN 
						(
							SELECT DISTINCT(s.rut)
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE ubicacion='$ubicacion'
							ORDER BY s.rut
						)
						AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
						AND (rar.fecha_apertura::TIME BETWEEN '15:00:00' AND '15:59:59')
						AND rar.identificador_sensor_puerta=
						(
							SELECT s.identificador_sensor
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE us.ubicacion='$ubicacion'
							AND s.rut=rar.rut_paciente
						)
					)AS total
				)AS h15,
				(
					SELECT COUNT(*)AS cantidad
					FROM
					(
						SELECT 
						DISTINCT(rar.rut_paciente)
						FROM resumen_acciones_repetidas AS rar
						INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
						INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
						WHERE rut_paciente IN 
						(
							SELECT DISTINCT(s.rut)
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE ubicacion='$ubicacion'
							ORDER BY s.rut
						)
						AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
						AND (rar.fecha_apertura::TIME BETWEEN '16:00:00' AND '16:59:59')
						AND rar.identificador_sensor_puerta=
						(
							SELECT s.identificador_sensor
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE us.ubicacion='$ubicacion'
							AND s.rut=rar.rut_paciente
						)
					)AS total
				)AS h16,
				(
					SELECT COUNT(*)AS cantidad
					FROM
					(
						SELECT 
						DISTINCT(rar.rut_paciente)
						FROM resumen_acciones_repetidas AS rar
						INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
						INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
						WHERE rut_paciente IN 
						(
							SELECT DISTINCT(s.rut)
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE ubicacion='$ubicacion'
							ORDER BY s.rut
						)
						AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
						AND (rar.fecha_apertura::TIME BETWEEN '17:00:00' AND '17:59:59')
						AND rar.identificador_sensor_puerta=
						(
							SELECT s.identificador_sensor
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE us.ubicacion='$ubicacion'
							AND s.rut=rar.rut_paciente
						)
					)AS total
				)AS h17,
				(
					SELECT COUNT(*)AS cantidad
					FROM
					(
						SELECT 
						DISTINCT(rar.rut_paciente)
						FROM resumen_acciones_repetidas AS rar
						INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
						INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
						WHERE rut_paciente IN 
						(
							SELECT DISTINCT(s.rut)
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE ubicacion='$ubicacion'
							ORDER BY s.rut
						)
						AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
						AND (rar.fecha_apertura::TIME BETWEEN '18:00:00' AND '18:59:59')
						AND rar.identificador_sensor_puerta=
						(
							SELECT s.identificador_sensor
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE us.ubicacion='$ubicacion'
							AND s.rut=rar.rut_paciente
						)
					)AS total
				)AS h18,
				(
					SELECT COUNT(*)AS cantidad
					FROM
					(
						SELECT 
						DISTINCT(rar.rut_paciente)
						FROM resumen_acciones_repetidas AS rar
						INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
						INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
						WHERE rut_paciente IN 
						(
							SELECT DISTINCT(s.rut)
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE ubicacion='$ubicacion'
							ORDER BY s.rut
						)
						AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
						AND (rar.fecha_apertura::TIME BETWEEN '19:00:00' AND '19:59:59')
						AND rar.identificador_sensor_puerta=
						(
							SELECT s.identificador_sensor
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE us.ubicacion='$ubicacion'
							AND s.rut=rar.rut_paciente
						)
					)AS total
				)AS h19,
				(
					SELECT COUNT(*)AS cantidad
					FROM
					(
						SELECT 
						DISTINCT(rar.rut_paciente)
						FROM resumen_acciones_repetidas AS rar
						INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
						INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
						WHERE rut_paciente IN 
						(
							SELECT DISTINCT(s.rut)
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE ubicacion='$ubicacion'
							ORDER BY s.rut
						)
						AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
						AND (rar.fecha_apertura::TIME BETWEEN '20:00:00' AND '20:59:59')
						AND rar.identificador_sensor_puerta=
						(
							SELECT s.identificador_sensor
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE us.ubicacion='$ubicacion'
							AND s.rut=rar.rut_paciente
						)
					)AS total
				)AS h20,
				(
					SELECT COUNT(*)AS cantidad
					FROM
					(
						SELECT 
						DISTINCT(rar.rut_paciente)
						FROM resumen_acciones_repetidas AS rar
						INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
						INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
						WHERE rut_paciente IN 
						(
							SELECT DISTINCT(s.rut)
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE ubicacion='$ubicacion'
							ORDER BY s.rut
						)
						AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
						AND (rar.fecha_apertura::TIME BETWEEN '21:00:00' AND '21:59:59')
						AND rar.identificador_sensor_puerta=
						(
							SELECT s.identificador_sensor
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE us.ubicacion='$ubicacion'
							AND s.rut=rar.rut_paciente
						)
					)AS total
				)AS h21,
				(
					SELECT COUNT(*)AS cantidad
					FROM
					(
						SELECT 
						DISTINCT(rar.rut_paciente)
						FROM resumen_acciones_repetidas AS rar
						INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
						INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
						WHERE rut_paciente IN 
						(
							SELECT DISTINCT(s.rut)
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE ubicacion='$ubicacion'
							ORDER BY s.rut
						)
						AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
						AND (rar.fecha_apertura::TIME BETWEEN '22:00:00' AND '22:59:59')
						AND rar.identificador_sensor_puerta=
						(
							SELECT s.identificador_sensor
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE us.ubicacion='$ubicacion'
							AND s.rut=rar.rut_paciente
						)
					)AS total
				)AS h22,
				(
					SELECT COUNT(*)AS cantidad
					FROM
					(
						SELECT 
						DISTINCT(rar.rut_paciente)
						FROM resumen_acciones_repetidas AS rar
						INNER JOIN paciente AS p ON p.rut=rar.rut_paciente
						INNER JOIN sensor AS s ON s.rut=rar.rut_paciente
						WHERE rut_paciente IN 
						(
							SELECT DISTINCT(s.rut)
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE ubicacion='$ubicacion'
							ORDER BY s.rut
						)
						AND rar.fecha_apertura BETWEEN p.fecha_inicio_estudio AND p.fecha_inicio_estudio + INTERVAL '30 days'
						AND (rar.fecha_apertura::TIME BETWEEN '23:00:00' AND '23:59:59')
						AND rar.identificador_sensor_puerta=
						(
							SELECT s.identificador_sensor
							FROM sensor AS s
							INNER JOIN ubicacion_sensor AS us ON s.id_ubicacion_sensor=us.id_ubicacion_sensor
							WHERE us.ubicacion='$ubicacion'
							AND s.rut=rar.rut_paciente
						)
					)AS total
				)AS h23
				"
				)
			);
		$datos=array_fill(0,24,0);
		if($resultado)
		{
			$datos[0]=$resultado[0]->h00;
			$datos[1]=$resultado[0]->h01;
			$datos[2]=$resultado[0]->h02;
			$datos[3]=$resultado[0]->h03;
			$datos[4]=$resultado[0]->h04;
			$datos[5]=$resultado[0]->h05;
			$datos[6]=$resultado[0]->h06;
			$datos[7]=$resultado[0]->h07;
			$datos[8]=$resultado[0]->h08;
			$datos[9]=$resultado[0]->h09;
			$datos[10]=$resultado[0]->h10;
			$datos[11]=$resultado[0]->h11;
			$datos[12]=$resultado[0]->h12;
			$datos[13]=$resultado[0]->h13;
			$datos[14]=$resultado[0]->h14;
			$datos[15]=$resultado[0]->h15;
			$datos[16]=$resultado[0]->h16;
			$datos[17]=$resultado[0]->h17;
			$datos[18]=$resultado[0]->h18;
			$datos[19]=$resultado[0]->h19;
			$datos[20]=$resultado[0]->h20;
			$datos[21]=$resultado[0]->h21;
			$datos[22]=$resultado[0]->h22;
			$datos[23]=$resultado[0]->h23;
		}
		return $datos;
	}
}
?>
