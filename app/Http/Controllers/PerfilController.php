<?php

class PerfilController extends BaseController{

	public function editarPerfil(){
		$rut=Input::get("rut");
		$nombre=Input::get("nombre");
		$apellidoPaterno=Input::get("apellido_p");
		$apellidoMaterno=Input::get("apellido_m");
		$correo=Input::get("correo");
		$password=Input::get("password");
		$telefono=Input::get("telefono");

		try{
			$usuario=Usuario::find($rut);
			$usuario->nombres=ucwords(strtolower($nombre));
			$usuario->apellido_paterno=ucwords(strtolower($apellidoPaterno));
			$usuario->apellido_materno=ucwords(strtolower($apellidoMaterno));
			$usuario->correo=$correo;
			$usuario->password=Hash::make($password);
			$usuario->telefono=$telefono;
			$usuario->save();

			Session::put("password", $password);
			
			return Response::json(["exito" => "Los datos han sido guardados"]);
		}catch(Exception $ex){
			return Response::json(["error" => "Error al guardar los datos del perfil", "msg" => $ex->getMessage()]);
		}
	}

}