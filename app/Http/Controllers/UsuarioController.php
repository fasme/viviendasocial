<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Usuario;

use Response;

class UsuarioController extends BaseController{

	public function obtenerUsuarios(){
		$perfil = Input::get("perfil");		
		return Response::json(Usuario::obtenerUsuarios($perfil));
	}

	public function agregarUsuario(){
		try{
			DB::beginTransaction();

			$rut=Input::get("rut");
			$enviarCorreo = false;
			
			$perfil = Input::get("selectPerfilCorto");
			//$pass = Input::get("password");
			//$pass = Hash::make($pass);
			
			$usuario=Usuario::find($rut);
			if($usuario == null) {
				$usuario=new Usuario;
				$msj = "El Usuario ha sido registrado";
				$enviarCorreo = true;
			}else $msj = "El Usuario ha sido editado";
			$usuario->rut=$rut;
			if(Input::has("nombre")) $usuario->nombres=ucwords(strtolower(Input::get("nombre")));
			if(Input::has("apellido_p")) $usuario->apellido_paterno=ucwords(strtolower(Input::get("apellido_p")));
			if(Input::has("apellido_m")) $usuario->apellido_materno=ucwords(strtolower(Input::get("apellido_m")));
			if(Input::has("telefono")) $usuario->telefono=Input::get("telefono");
			if(Input::has("correo")){
				$usuario->correo=Input::get("correo");
			}else{
				$enviarCorreo = false; 
			}		
			$pass = Input::get("password");
			if(Input::has("password")) $usuario->password=Hash::make($pass);
			$usuario->visible="TRUE";

			(Input::has('alertaPush')) ? $usuario->alerta_push=true : $usuario->alerta_push=false;
			(Input::has('alertaSms')) ? $usuario->alerta_sms=true : $usuario->alerta_sms=false;
			(Input::has('alertaCorreo')) ? $usuario->alerta_correo=true : $usuario->alerta_correo=false;


			/*$usuario->alerta_push   = $alertaPush;
			$usuario->alerta_sms    = $alertaSms;
			$usuario->alerta_correo =$alertaCorreo;*/
			$usuario->save();
			
			$encargado=Encargado::find($rut);
			if($encargado == null) $encargado=new Encargado;
			$encargado->rut = $rut;
			$encargado->id_tipo_usuario=$perfil;
			$encargado->estado_solicitud = "aceptado";
			$encargado->save();

			if(Input::has("comuna") || Input::has("calle") || Input::has("dpto") || Input::has("numero")){

				$coordenadas = [];
				$domActual = Domicilio::obtenerDomicilioActual($rut);
				if($domActual != null) {
					$domActual->fin = date("Y-m-d H:i:s");
					$domActual->save();
				}

				$coordenadas = (!Input::has("calle") && !Input::has("numero") && !Input::has("comuna")) ? null : Funciones::obtenerCoordenadas(Input::get("calle"), Input::get("numero"), Input::get("comuna"));
 
				$domicilio = new Domicilio;
				$domicilio->rut=$rut;
				$domicilio->inicio = date("Y-m-d H:i:s");
				if(Input::has("comuna"))$domicilio->id_comuna = Input::get("comuna");
				if(Input::has("calle"))$domicilio->calle = ucwords(Input::get("calle"));
				if(Input::has("dpto"))$domicilio->departamento = Input::get("dpto");
				if(Input::has("numero"))$domicilio->numero = Input::get("numero");
				if($coordenadas != null){
					$domicilio->latitud = $coordenadas["lat"];
					$domicilio->longitud = $coordenadas["long"];
				}
				$domicilio->save();
			}

			if(Input::has("rut-paciente-grupo")){
				$rutPaciente=Input::get("rut-paciente-grupo");
				$Contacto=Contacto::where("rut_encargado", "=", $rut)->where("rut_paciente", "=", $rutPaciente)->first();
				if($Contacto == null){ 
					$Contacto=new Contacto;
					$Contacto->rut_paciente=$rutPaciente;
					$Contacto->rut_encargado=$rut;
					$Contacto->save();
				}
			}
			if($enviarCorreo){
				$this->enviarCorreoUsuario($usuario->nombres." ".$usuario->apellido_paterno, $usuario->correo, $pass);
			}
			DB::commit();
			return Response::json(["exito" => $msj]);
		}catch(Exception $ex){
			DB::rollback();
			return Response::json(["error" => "Error al registrar el usuario", "msg" => $ex->getMessage()]);
		}
	}

	public function obtenerDatosUsuario(){
		$rut=Input::get("rut");
		return Response::json(Usuario::obtenerDatosUsuario($rut));
	}

	public function eliminarUsuario(){
		try{
			DB::beginTransaction();

			$rut=Input::get("rut");
			$tipoUsuario=Input::get("tipoUsuario");
			if($tipoUsuario=='paciente')
			{
				$paciente=Paciente::find($rut);
				$paciente->paciente_visible="FALSE";
				$paciente->save();
			}
			else{
				$usuario=Usuario::find($rut);
				$usuario->visible="FALSE";
				$usuario->save();
			}
			

			Contacto::where("rut_encargado", "=", $rut)->whereNull("fin")->update(["fin" => date("Y-m-d H:i:s")]);

			DB::commit();
			return Response::json(["exito" => "El ".$tipoUsuario." ha sido eliminado"]);
		}catch(Exception $ex){
			DB::rollback();
			return Response::json(["error" => "Error al eliminar el ".$tipoUsuario, "msg" => $ex->getMessage()]);
		}
	}

	//se usa
	public function obtenerPacientesDeContacto(Request $request){
		return "UsuarioController";
		$rutUsuario = ($request->input("rutUsuario")) ? $request->input("rutUsuario") : null;
		$rutPaciente = ($request->input("rutPaciente")) ? $request->input("rutPaciente") : null;
		$idTipoUsuario = ($request->input("idTipoUsuario")) ? $request->input("idTipoUsuario") : null;
		$datos=Usuario::obtenerPacienteContacto($rutUsuario, $rutPaciente, $idTipoUsuario);
		return response()->json($datos);
	}

	public function existeEncargado(){
		$rut=Input::get("rutUsuario");
		$encargado = Usuario::obtenerDatosUsuario($rut);

		if($encargado == null) return Response::json(["resultado" => false]);
		else return Response::json(["resultado" => true, "msj" => "El usuario ya existe."]);
	}

	public function existeUsuario(){
		$rut = Input::get("rut");
		$existe = true;
		$message = "";

		$usuario = Encargado::find($rut);
		if($usuario != null){
			$existe = false;
			$message = "El usuario se encuentra registrado";
		}

		return ["valid" => $existe, "message" => $message];
	}

	public function enviarCorreoUsuario($nombre, $correo, $pass){
		
		$data = array(  
			"nombre" => $nombre,
			"correo" => $correo,			
			"pass" => $pass
			);
		$asunto= "Bienvenido al sistema eHomeseniors";
		$destinatario = "Soporte";
		$correos = array($correo, "soporte.raveno@uv.cl");
		Mail::send('Paciente.CorreoBienvenida', $data, function($message) use ($correos, $destinatario, $asunto)
		{
		  $message->to($correos, $destinatario)
		          ->subject('Correo de prueba - '.$asunto);
		});
		$response=[];
		//return Response::json(array("exito" => "El correo ha sido enviado"));
	}

}

?>