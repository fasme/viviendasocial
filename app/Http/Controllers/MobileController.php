<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\AlertaRevisada;
use App\Models\AlertaCaida;
use App\Models\Alerta;
use App\Models\Datomonoxido;
use App\Models\Datocaidas;
use DB;
use App\Models\Gpsboton;
use App\Models\Usuario;
use App\Models\Contacto;
use App\Models\RegistroGCM;
use Funciones;
use Response;
use Hash;
use App\Models\Agenda;
use App\Models\Comuna;
use Auth;
use App\Models\MensajeUsuario;
use App\Models\SindromesGeriatricos;
use App\Models\TestLawtonBrody;
use App\Models\TestCharlson;
use App\Models\IndiceBarthel;
use App\Models\TestMmse;
use App\Models\TestWhoqol;
use App\Models\TestEq;



class MobileController extends Controller {

  public function editarPerfil(Request $request){

    $rut              = $request->input("rutUsuario");
    $nombres          = $request->input("nombres");
    $apellido_paterno = $request->input("apellidoPaterno");
    $apellido_materno = $request->input("apellidoMaterno");
    $correo           = $request->input("correo");
    $password         = $request->input("contrasena");
    $telefono         = $request->input("telefono");
    $tipo             = $request->input("tipo");

    $agenda = DB::table('agenda')
          ->where("rut_usuario","=",$rut)
          ->delete();

    foreach($telefono as $key => $value){
      if( $value != "" ){
          $agendaUno                = new Agenda;
          $agendaUno->telefono      = $value;
          $agendaUno->tipo_telefono = $tipo[$key];
          $agendaUno->rut_usuario   = $rut;
            $agendaUno->save();
      }

    }

    $usuario                   = Usuario::find($rut);
    $usuario->nombres          = $nombres;
    $usuario->apellido_paterno = $apellido_paterno;
    $usuario->apellido_materno = $apellido_materno;
    $usuario->correo           = $correo;

    if( $password != "" )
      $usuario->password=Hash::make($password);

    $usuario->save();

    $agenda = DB::table('agenda')
          ->where("rut_usuario","=",$rut)
          ->get();

    $usuario->agenda = $agenda;

    return response()->json(["usuario" => $usuario]);
  }


  	public function existeUsuario(Request $request){
		$rut         = $request->input("rutValidado");
		$password    = $request->input("password");

		//$encargado = Encargado::existeEncargado($rut);
		$encargado   = Contacto::existeContacto($rut);

		return response()->json(["valid" => true,"encargado" => $encargado]);
		
		$usuario     = ($encargado) ? Usuario::find($rut) : null;

		if($usuario == null)
			return response()->json(["valid" => false, "msg" => "Usuario y/o contraseña inválida"]);
		if(!Hash::check($password, $usuario->password))
			return response()->json(["valid" => false, "msg" => "La contraseña ingresada es incorrecta","encargado"=>$encargado]);

		return response()->json(["valid" => true,"encargado" => $encargado]);
	}

  public function buscarUsuario(Request $request){
		$response = false;
		$rut = $request->input("rut");

		$usuario = Usuario::where("rut", "=", $rut)->where("visible", "=", true)->first();

		if($usuario == null) 
			return response()->json($response);

		/*$domicilio = Domicilio::obtenerDomicilioActual($rut);*/

		$agenda = DB::table('agenda')
					->where("rut_usuario","=",$rut)
					->get();

		$response = [
			"correo" => $usuario->correo,
			"telefono" => $usuario->telefono,
			"nombres" => $usuario->nombres,
			"apellido_paterno" => $usuario->apellido_paterno,
			"apellido_materno" => $usuario->apellido_materno,
			"rut" => $usuario->rut,
			"calle" => $usuario->calle,
			"numero" => $usuario->numero,
			/*"deptartamento" => ($domicilio == null) ? "" : $domicilio->departamento,*/
			"id_comuna" => $usuario->id_comuna,
			"observacion_direccion" => $usuario->observacion_direccion,
			"agenda" => $agenda,
			"id_tipo_usuario" => $usuario->id_tipo_usuario,
			"rutSinDigito" => $usuario->rut
		];

		return response()->json($response);
	}


  public function login(Request $request){
		$telefono = $request->input("telefono");
		$tipo = $request->input("tipo");
		$correo = $request->input("correo");
		$rut = $request->input("rut");
		$password = $request->input("password");
		$calle = $request->input("calle");
		$numero = $request->input("numero");
		$observacion_direccion = $request->input("observacion_direccion");
		$comuna = $request->input("comuna");

		$nombreComuna = Comuna::obtenerNombreComuna($comuna);
		$coordenadas = Funciones::obtenerCoordenadas($calle, $numero, $nombreComuna);

		$rut = Funciones::soloNumeros($rut);

		$userData=["rut" => $rut, "password" => $password];

		if(Auth::attempt($userData)) {

			$usuario = Usuario::find($rut);
			//$usuario->telefono              = $telefono;
			$usuario->correo                = $correo;

			$usuario->calle                 = $calle;
			$usuario->numero                = $numero;
			$usuario->id_comuna             = $comuna;
			$usuario->observacion_direccion = $observacion_direccion;

			$usuario->save();

			//Guargar telefonos en la agenda....

			$agenda = DB::table('agenda')
						->where("rut_usuario","=",$rut)
						->delete();


			foreach($telefono as $key => $value){
				$agendaUno                = new Agenda;
				$agendaUno->telefono      = $value;
				$agendaUno->tipo_telefono = $tipo[$key];
				$agendaUno->rut_usuario   = $rut;
			    $agendaUno->save();

			    $agenda = DB::table('agenda')
			    		->where("rut_usuario","=",$rut)
			    		->get();

			    $usuario->agenda = $agenda;
			}



			/*$domicilio = Domicilio::obtenerDomicilioActual($rut);
			if($domicilio == null) {
				$domicilio = new Domicilio;
				$domicilio->guardar($rut, $calle, $numero, $departamento, $comuna, $coordenadas);
			}
			elseif($domicilio->calle != $calle || $domicilio->numero != $numero || $domicilio->departamento != $departamento
				|| $domicilio->id_comuna != $comuna){
				$domicilio->fin = date("Y-m-d H:i:s");
				$domicilio->save();

				$nuevoDomicilio = new Domicilio;
				$nuevoDomicilio->guardar($rut, $calle, $numero, $departamento, $comuna, $coordenadas);
			}
			*/
			/*$tipoUsuario = Contacto::obtenerTipoUsuario($rut);*/

			/*$usuario = Funciones::objectToArray(Auth::user());*/
			/*$usuario["apellido_paterno"] = ($usuario["apellido_paterno"] == null) ? "" : $usuario["apellido_paterno"];
			$usuario["apellido_materno"] = ($usuario["apellido_materno"] == null) ? "" : $usuario["apellido_materno"];
			$usuario["id_tipo_usuario"] = $tipoUsuario;*/
			return response()->json($usuario);
		}
    else{
      return "ERROR";
    }
		return response()->json(["error" => "Usuario y/o contraseña inválida"]);
	}


  public function obtenerComunas(){
    return response()->json(Comuna::obtenerComunas());
  }

  	public function getPacientesUsuario(Request $request){
		$rutUsuario = $request->input("rutUsuario");
		$pacientes = Usuario::pacientesUsuario($rutUsuario);

		foreach($pacientes as $key => $value)
		{
		    $value["rutFormato"] =  number_format($value->rut , 0, ',', '.');
		    $value["rut"] = $value->rut;
		    $value["dv"]     = Funciones::obtenerDV($value->rut);
		    $value["search"] = $value->nombres." ".$value->apellido_paterno." ".$value->apellido_materno." ".$value->rut;
		}

		return response()->json($pacientes);
	}

	public function getUsuariosEncuesta(Request $request){
		$rutUsuario = $request->input("rutUsuario");
		$pacientes = Usuario::getUsuariosEncuesta($rutUsuario);

		foreach($pacientes as $key => $value)
		{
		    $value["rutFormato"] =  number_format($value->rut , 0, ',', '.');
		    $value["rut"] = $value->rut;
		    $value["dv"]     = Funciones::obtenerDV($value->rut);
		    $value["search"] = $value->nombres." ".$value->apellido_paterno." ".$value->apellido_materno." ".$value->rut;
		}

		return response()->json($pacientes);
	}


  public function ultimosEventosPacientes(Request $request){
		$response = [];
		$rut = $request->input("rut");
		//$position = $request->input("position");

		/*$alertas_paciente = DB::select(DB::raw("select * from alertas_revisadas_vista AS ap
			JOIN contacto AS c ON c.rut_paciente = ap.rut
			WHERE c.rut_encargado= '$rut'
			ORDER BY fecha DESC"));
			OFFSET '$position' ROWS
					FETCH NEXT 10 ROWS ONLY*/

			$alertas_paciente = DB::select(DB::raw("select * from alertas_revisadas_vista
					WHERE rut_encargado= '$rut'
					ORDER BY fecha DESC
					"));

		foreach($alertas_paciente as $key => $value)
		{
		    $value->rutFormato = number_format($value->rut , 0, ',', '.');
		    $value->dv = Funciones::obtenerDV($value->rut);
		}

		/*$rut = Funciones::soloNumeros($rut);

		$alertasCaidas = AlertaCaida::obtenerTodasAlertas($rut);
		$alertasPuertaNicturia = Alerta::obtenerTodasAlertasOrdenFecha($rut);

		$alertas = array_merge($alertasCaidas, $alertasPuertaNicturia);

		$alertas = Funciones::ordenar($alertas);
		$alertas = array_slice($alertas, 0, 19);*/

		//return response()->json($alertas);
		//
		//return response()->json(array("posicion_actual" => $position2, "alertas" => $alertas_paciente));
		return response()->json($alertas_paciente);
	}

  public function buscarFichaMedica(Request $request){
		$rut_paciente = $request->input("rut_paciente");
		$fichaPaciente = DB::select(DB::raw("select * from ficha_paciente_vista WHERE rut_paciente= '$rut_paciente'"));

		foreach($fichaPaciente as $key => $value)
		{
		    $value->rutFormato = number_format($value->rut_paciente , 0, ',', '.');
		    $value->dv = Funciones::obtenerDV($value->rut_paciente);
		}

		$agenda = DB::table('agenda')
					->where("rut_usuario","=",$rut_paciente)
					->get();

		$fichaPaciente[0]->agenda = $agenda;

		return response()->json($fichaPaciente);

	}

  public function buscarAlertasCaidas(Request $request){
    $rutPaciente = $request->input("rutPaciente");
    $rutUsuario = $request->input("rutUsuario");

    $alertas = DB::select(DB::raw("select * from alertas_revisadas_vista WHERE rut = '$rutPaciente'
      AND id_evento = 2
      AND rut_encargado = $rutUsuario
      ORDER BY fecha DESC
      "));
    
    foreach($alertas as $key => $value)
    {
        $value->dv = Funciones::obtenerDV($value->rut);
    }
    /*$rutUsuario = $request->input("rutUsuario");
    $rutPaciente = $request->input("rutPaciente");

    $rutUsuario = Funciones::soloNumeros($rutUsuario);
    $rutPaciente = Funciones::soloNumeros($rutPaciente);

    $alertas = AlertaCaida::obtenerAlertas($rutPaciente, $rutUsuario);*/
    
    return response()->json($alertas);
  }

  public function buscarAlertasMonoxido(Request $request){
    $rutPaciente = $request->input("rutPaciente");
    $rutUsuario = $request->input("rutUsuario");
   

    $alertas = DB::select(DB::raw("select * from alertas_revisadas_vista WHERE rut = '$rutPaciente'
      AND id_evento = 3
      AND rut_encargado = $rutUsuario
      ORDER BY fecha DESC"));

    foreach($alertas as $key => $value)
    {
        $value->dv = Funciones::obtenerDV($value->rut);
    }
     
    return response()->json($alertas);
  }

  public function buscarAlertasPanico(Request $request){
    $rutPaciente = $request->input("rutPaciente");
    $rutUsuario = $request->input("rutUsuario");

    $alertas = DB::select(DB::raw("select * from alertas_revisadas_vista WHERE rut = '$rutPaciente'
      AND id_evento = 6
      AND rut_encargado = $rutUsuario
      ORDER BY fecha DESC"));

    foreach($alertas as $key => $value)
    {
        $value->dv = Funciones::obtenerDV($value->rut);
    }

    return response()->json($alertas);
  }


  public function revisarAlerta(Request $request){
    try{
      $idAlertaRevisada = $request->input("idAlerta");
      $idUsuario        = $request->input("idUsuario");

      $revisada         = AlertaRevisada::validarRevisada($idAlertaRevisada,$idUsuario);
      if( $revisada ){
      	return response()->json(["exito" => "La alerta ya está revisada"]);
      }else{

      	$alerta_revisada  = AlertaRevisada::marcarRevisada($idAlertaRevisada,$idUsuario);
      	/*$alerta = AlertaRevisada::find($idAlerta);
      	$alerta->revisada = true;
      	$alerta->save();*/

      	return response()->json(["exito" => "La alerta ha sido marcada como revisada","alerta_revisada"=>$alerta_revisada]);	
      }

    }catch(Exception $ex){
      return response()->json(["error" => "Error al revisar la alerta", "msg" => $ex->getMessage()]);
    }
  }


  public function guardarGCM(Request $request){
    try{
      $rut = $request->input("rut");
      $rut = Funciones::soloNumeros($rut);
      $idDevice = $request->input("idDevice");
      $idGCM = $request->input("gcm");
      $device = $request->input("device");

      $gcm = RegistroGCM::where("rut", "=", $rut)->where("id_device", "=", $idDevice)->first();
      if($gcm == null) {
        $gcm = new RegistroGCM;
        $gcm->rut = $rut;
        $gcm->id_device = $idDevice;
      }
      $gcm->dispositivo_movil = strtolower($device);
      $gcm->id_gcm = $idGCM;
      $gcm->save();

      return response()->json(["exito" => "GCM guardado"]);
    }catch(Exception $ex){
      return response()->json(["error" => $ex->getMessage()]);
    }
  }

  public function ultimosEventosPacientesDePaciente(Request $request){ // obtiene los eventos del mismo paciente
		$response = [];
		$rut = $request->input('rut');

		$rut = Funciones::soloNumeros($rut);

		$alertasCaidas = AlertaCaida::obtenerTodasAlertasDePaciente($rut); /* solo las caidas y si se han revisado*/
		$alertasPuertaNicturia = Alerta::obtenerTodasAlertasOrdenFechaDePaciente($rut);

		$alertas = array_merge($alertasCaidas, $alertasPuertaNicturia);
		
		$alertas = Funciones::ordenar($alertas);
		$alertas = array_slice($alertas, 0, 19);

		return Response::json($alertas);
	}

    public function guardarMensaje(Request $request){
      $rut = $request->input("rut");
      $rut = Funciones::soloNumeros($rut);
      $mensaje = $request->input("mensaje_usuario");
      $opinion = $request->input("emocion");

      $m = DB::table('mensaje_usuario')
          ->where("usuario_rut","=",$rut)
          ->get();

	  $m = new MensajeUsuario;
      $m->usuario_rut = $rut;
      $m->opinion = $opinion;
      $m->mensaje = $mensaje;
      $m->save();

	  return response()->json($m);
    }

    public function editarEncuestasSindromesGeriatricosMovil(Request $request){
    	try{

    		DB::beginTransaction();

			$rut          = $request->input("rut");
			$tipoEncuesta = $request->input("tipo-encuesta");
			$inicial      = $request->input("inicio");
			$genero       = $request->input("genero-paciente");

    		//$this->registrarPaciente($rut, $genero);
    		
    		$sindromesGeriatricos = SindromesGeriatricos::where("rut_paciente", "=", $rut)->first();
    		/*if($sindromesGeriatricos == null) {
    			$sindromesGeriatricos = new SindromesGeriatricos;
    		}*/

    		$sindromesGeriatricos = new SindromesGeriatricos;
    		$sindromesGeriatricos->rut_paciente = $rut;
    		if($request->input("sindromesGeriatricos-fecha-encuesta")) $sindromesGeriatricos->fecha_test = $request->input("sindromesGeriatricos-fecha-encuesta");
    		//$sindromesGeriatricos->inicial = $inicial;
    		if($request->input("sindromesGeriatricos-caidas-ultimos-meses")) $sindromesGeriatricos->numero_caidas = $request->input("sindromesGeriatricos-caidas-ultimos-meses");
    		if($request->input("sindromesGeriatricos-p-1")) $sindromesGeriatricos->caidas_transtorno = $request->input("sindromesGeriatricos-p-1");
    		if($request->input("sindromesGeriatricos-c-1")) $sindromesGeriatricos->comentario_caidas_transtorno = $request->input("sindromesGeriatricos-c-1");
    		if($request->input("sindromesGeriatricos-p-2")) $sindromesGeriatricos->polifarmacia = $request->input("sindromesGeriatricos-p-2");
    		if($request->input("sindromesGeriatricos-c-2")) $sindromesGeriatricos->comentario_polifarmacia = $request->input("sindromesGeriatricos-c-2");
    		if($request->input("sindromesGeriatricos-p-3")) $sindromesGeriatricos->incontinencia_urinaria = $request->input("sindromesGeriatricos-p-3");
    		if($request->input("sindromesGeriatricos-c-3")) $sindromesGeriatricos->comentario_incontinencia_urinaria = $request->input("sindromesGeriatricos-c-3");
    		if($request->input("sindromesGeriatricos-p-4")) $sindromesGeriatricos->incontinencia_fecal = $request->input("sindromesGeriatricos-p-4");
    		if($request->input("sindromesGeriatricos-c-4")) $sindromesGeriatricos->comentario_incontinencia_fecal = $request->input("sindromesGeriatricos-c-4");
    		if($request->input("sindromesGeriatricos-p-5")) $sindromesGeriatricos->deterioro_cognitivo = $request->input("sindromesGeriatricos-p-5");
    		if($request->input("sindromesGeriatricos-c-5")) $sindromesGeriatricos->comentario_deterioro_cognitivo = $request->input("sindromesGeriatricos-c-5");
    		if($request->input("sindromesGeriatricos-p-6")) $sindromesGeriatricos->delirium = $request->input("sindromesGeriatricos-p-6");
    		if($request->input("sindromesGeriatricos-c-6")) $sindromesGeriatricos->comentario_delirium = $request->input("sindromesGeriatricos-c-6");
    		if($request->input("sindromesGeriatricos-p-7")) $sindromesGeriatricos->trans_animo = $request->input("sindromesGeriatricos-p-7");
    		if($request->input("sindromesGeriatricos-c-7")) $sindromesGeriatricos->comentario_trans_animo = $request->input("sindromesGeriatricos-c-7");
    		if($request->input("sindromesGeriatricos-p-8")) $sindromesGeriatricos->trans_sueno = $request->input("sindromesGeriatricos-p-8");
    		if($request->input("sindromesGeriatricos-c-8")) $sindromesGeriatricos->comentario_trans_sueno = $request->input("sindromesGeriatricos-c-8");
    		if($request->input("sindromesGeriatricos-p-9")) $sindromesGeriatricos->malnutricion = $request->input("sindromesGeriatricos-p-9");
    		if($request->input("sindromesGeriatricos-c-9")) $sindromesGeriatricos->comentario_malnutricion = $request->input("sindromesGeriatricos-c-9");
    		if($request->input("sindromesGeriatricos-p-10")) $sindromesGeriatricos->deficit_sensorial = $request->input("sindromesGeriatricos-p-10");
    		if($request->input("sindromesGeriatricos-c-10")) $sindromesGeriatricos->comentario_deficit_sensorial = $request->input("sindromesGeriatricos-c-10");
    		if($request->input("sindromesGeriatricos-p-11")) $sindromesGeriatricos->inmovilidad = $request->input("sindromesGeriatricos-p-11");
    		if($request->input("sindromesGeriatricos-c-11")) $sindromesGeriatricos->comentario_inmovilidad = $request->input("sindromesGeriatricos-c-11");
    		if($request->input("sindromesGeriatricos-p-12")) $sindromesGeriatricos->ulcera_presion = $request->input("sindromesGeriatricos-p-12");
    		if($request->input("sindromesGeriatricos-c-12")) $sindromesGeriatricos->comentario_ulcera_presion = $request->input("sindromesGeriatricos-c-12");
    		
    		$sindromesGeriatricos->save();
    

    		DB::commit();

    		return Response::json(["exito" => "Los datos han sido guardados"]);
    	} catch(Exception $ex){
    		DB::rollback();
    		return Response::json(["error" => "Error al guardar los datos", "msg" => $ex->getMessage()]);
    	}


    }

    public function editarEncuestasLawtonBrody(Request $request){
        try{

            DB::beginTransaction();

            $rut = $request->input("rut");
            $tipoEncuesta = $request->input("tipo-encuesta");
            $inicial = $request->input("inicio");
            $genero = $request->input("genero-paciente");

			//$this->registrarPaciente($rut, $genero);
            
            $testLawtonBrody = TestLawtonBrody::where("rut_paciente", "=", $rut)->first();

            /*if($testLawtonBrody == null) {
                $testLawtonBrody = new TestLawtonBrody;
            }*/

            $testLawtonBrody = new TestLawtonBrody;
            $testLawtonBrody->rut_paciente = $rut;
            //$testLawtonBrody->inicial = $inicial;
            if($request->input("fecha-encuesta")) $testLawtonBrody->fecha_test = $request->input("fecha-encuesta");
                        
			if($request->input("p-1")){ $testLawtonBrody->telefono   = $request->input("p-1");}
			if($request->input("p-2")){ $testLawtonBrody->compras    = $request->input("p-2");}
			if($request->input("p-3")){ $testLawtonBrody->comida     = $request->input("p-3");}
			if($request->input("p-4")){ $testLawtonBrody->tareas     = $request->input("p-4");}
			if($request->input("p-5")){ $testLawtonBrody->ropa       = $request->input("p-5");}
			if($request->input("p-6")){ $testLawtonBrody->transporte = $request->input("p-6");}
			if($request->input("p-7")){ $testLawtonBrody->medicacion = $request->input("p-7");}
			if($request->input("p-8")){ $testLawtonBrody->dinero     = $request->input("p-8");}
    
            $testLawtonBrody->save();    

            DB::commit();

            return Response::json(["exito" => "Los datos han sido guardados"]);
        } catch(Exception $ex){
            DB::rollback();
            return Response::json(["error" => "Error al guardar los datos", "msg" => $ex->getMessage()]);
        }
    }

    public function editarEncuestasCharlson(Request $request){
        try{

            DB::beginTransaction();

            $rut = $request->input("rut");
            $tipoEncuesta = $request->input("tipo-encuesta");
            $inicial = $request->input("inicio");
            $genero = $request->input("genero-paciente");

			//$this->registrarPaciente($rut, $genero);
            
            $testCharlson = TestCharlson::where("rut_paciente", "=", $rut)->first();

            /*if($testCharlson == null) {
                $testCharlson = new TestCharlson;
            }*/

            $testCharlson = new TestCharlson;
            $testCharlson->rut_paciente = $rut;
            //$testCharlson->inicial = $inicial;
            if($request->input("fecha-encuesta")) $testCharlson->fecha_test = $request->input("fecha-encuesta");
                        
            if($request->input("p-1")) $testCharlson->infarto_miocardio = $request->input("p-1"); else $testCharlson->infarto_miocardio = false;
            if($request->input("p-2")) $testCharlson->insuficiencia_cardiaca = $request->input("p-2"); else $testCharlson->insuficiencia_cardiaca = false;
            if($request->input("p-3")) $testCharlson->enf_arterial = $request->input("p-3"); else $testCharlson->enf_arterial = false;
            if($request->input("p-4")) $testCharlson->enf_cerebrovascular = $request->input("p-4"); else $testCharlson->enf_cerebrovascular = false;
            if($request->input("p-5")) $testCharlson->demencia = $request->input("p-5"); else $testCharlson->demencia = false;
            if($request->input("p-6")) $testCharlson->enf_respiratoria = $request->input("p-6"); else $testCharlson->enf_respiratoria = false;
            if($request->input("p-7")) $testCharlson->enf_tejido = $request->input("p-7"); else $testCharlson->enf_tejido = false;
            if($request->input("p-8")) $testCharlson->ulcera_gastro = $request->input("p-8"); else $testCharlson->ulcera_gastro = false;
            if($request->input("p-9")) $testCharlson->hep_leve = $request->input("p-9"); else $testCharlson->hep_leve = false;
            if($request->input("p-10")) $testCharlson->diabetes = $request->input("p-10"); else $testCharlson->diabetes = false;
            if($request->input("p-11")) $testCharlson->hemiplejia = $request->input("p-11"); else $testCharlson->hemiplejia = false;
            if($request->input("p-12")) $testCharlson->insuficiencia_moderada = $request->input("p-12"); else $testCharlson->insuficiencia_moderada = false;
            if($request->input("p-13")) $testCharlson->diabetes_lesion = $request->input("p-13"); else $testCharlson->diabetes_lesion = false;
            if($request->input("p-14")) $testCharlson->tumor = $request->input("p-14"); else $testCharlson->tumor = false;
            if($request->input("p-15")) $testCharlson->leucemia = $request->input("p-15"); else $testCharlson->leucemia = false;
            if($request->input("p-16")) $testCharlson->linfoma = $request->input("p-16"); else $testCharlson->linfoma = false;
            if($request->input("p-17")) $testCharlson->hepatopatia = $request->input("p-17"); else $testCharlson->hepatopatia = false;
            if($request->input("p-18")) $testCharlson->tumor_metastasis = $request->input("p-18"); else $testCharlson->tumor_metastasis = false;
            if($request->input("p-19")) $testCharlson->sida = $request->input("p-19"); else $testCharlson->sida = false;
    
            $testCharlson->save();    

            DB::commit();

            return Response::json(["exito" => "Los datos han sido guardados"]);
        } catch(Exception $ex){
            DB::rollback();
            return Response::json(["error" => "Error al guardar los datos", "msg" => $ex->getMessage()]);
        }


    }

    public function editarEncuestasIndiceBarthel(Request $request){
		try{

			DB::beginTransaction();

			$rut = $request->input("rut");
			$tipoEncuesta = $request->input("tipo-encuesta");
			$inicial = $request->input("inicio");
			$genero = $request->input("genero-paciente");

			//$this->registrarPaciente($rut, $genero);

			$indiceBarthel = IndiceBarthel::where("rut_paciente", "=", $rut)->first();
			/*if($indiceBarthel == null) {
				$indiceBarthel = new IndiceBarthel;
				
			}*/

			$indiceBarthel = new IndiceBarthel;
			$indiceBarthel->rut_paciente = $rut;
			if($request->input("fecha-encuesta")) $indiceBarthel->fecha_test = $request->input("fecha-encuesta");
			//$indiceBarthel->inicial = $inicial;

			if($request->input("indiceBarthel-p-1")) $indiceBarthel->comer = $request->input("indiceBarthel-p-1");
			if($request->input("indiceBarthel-p-2")) $indiceBarthel->lavarse = $request->input("indiceBarthel-p-2");
			if($request->input("indiceBarthel-p-3")) $indiceBarthel->vestirse = $request->input("indiceBarthel-p-3");
			if($request->input("indiceBarthel-p-4")) $indiceBarthel->arreglarse = $request->input("indiceBarthel-p-4");
			if($request->input("indiceBarthel-p-5")) $indiceBarthel->deposiciones = $request->input("indiceBarthel-p-5");
			if($request->input("indiceBarthel-p-6")) $indiceBarthel->miccion = $request->input("indiceBarthel-p-6");
			if($request->input("indiceBarthel-p-7")) $indiceBarthel->uso_retrete = $request->input("indiceBarthel-p-7");
			if($request->input("indiceBarthel-p-8")) $indiceBarthel->trasladarse = $request->input("indiceBarthel-p-8");
			if($request->input("indiceBarthel-p-9")) $indiceBarthel->deambular = $request->input("indiceBarthel-p-9");
			if($request->input("indiceBarthel-p-10")) $indiceBarthel->escalones = $request->input("indiceBarthel-p-10");
				
			$indiceBarthel->save();
	

			DB::commit();

			return Response::json(["exito" => "Los datos han sido guardados"]);
		} catch(Exception $ex){
			DB::rollback();
			return Response::json(["error" => "Error al guardar los datos", "msg" => $ex->getMessage()]);
		}

	}

	public function editarEncuestasMmse(Request $request){
    		DB::beginTransaction();

			//$inicial        = $request->input("inicio");
			$fecha_test     = $request->input("fecha_test");
			$rut            = $request->input("rut");
			$fecha          = $request->input("p-1");
			$mes            = $request->input("p-2");
			$dia_semana     = $request->input("p-3");
			$anio           = $request->input("p-4");
			$estacion       = $request->input("p-5");
			
			$lugar          = $request->input("p-6");
			$piso           = $request->input("p-7");
			$ciudad         = $request->input("p-8");
			$comuna         = $request->input("p-9");
			$pais           = $request->input("p-10");
			
			$arbol1         = $request->input("p-11");
			$mesa1          = $request->input("p-12");
			$perro1         = $request->input("p-13");
			
			$letra_o        = $request->input("p-14");
			$letra_d        = $request->input("p-15");
			$letra_n        = $request->input("p-16");
			$letra_u        = $request->input("p-17");
			$letra_m        = $request->input("p-18");
			
			$arbol2         = $request->input("p-19");
			$mesa2          = $request->input("p-20");
			$perro2         = $request->input("p-21");
			
			
			$lapiz          = $request->input("p-22");
			$reloj          = $request->input("p-23");
			$repetir_frase  = $request->input("p-24");
			$papel1         = $request->input("p-25"); //tomar
			$papel2         = $request->input("p-26"); // doblar
			$papel3         = $request->input("p-27"); // dejar
			$ojos_cerrados  = $request->input("p-28");
			$escribir_frase = $request->input("p-29");
			$dibujo         = $request->input("p-30");

        	$repeticion1    = $request->input("repeticion1");


        	$testMmse = TestMmse::where("rut_paciente", "=", $rut)->first();

        	/*if($testMmse == null){
        		$testMmse = new TestMmse;
        	}*/

        	$testMmse = new TestMmse;
        	//$testMmse->inicial = $inicial;
        	$testMmse->rut_paciente=$rut;
        	//$testMmse->fecha_test= $fecha_test;
        	if($request->input("fecha_test"))$testMmse->fecha_test=$fecha_test;
        	$testMmse->fecha = $fecha;
        	$testMmse->mes = $mes;
        	$testMmse->dia_semana = $dia_semana;
        	$testMmse->anio = $anio;
        	$testMmse->estacion = $estacion;
        	$testMmse->lugar = $lugar;
        	$testMmse->piso = $piso;
        	$testMmse->ciudad = $ciudad;
        	$testMmse->comuna = $comuna;
        	$testMmse->pais = $pais;
        	$testMmse->numero_repeticiones_palabras = $repeticion1;
        	$testMmse->arbol1 = $arbol1;
        	$testMmse->mesa1 = $mesa1;
        	$testMmse->perro1 = $perro1;
        	$testMmse->letra_o = $letra_o;
        	$testMmse->letra_d = $letra_d;
        	$testMmse->letra_n = $letra_n;
        	$testMmse->letra_u = $letra_u;
        	$testMmse->letra_m = $letra_m;
        	$testMmse->arbol2 = $arbol2;
        	$testMmse->mesa2 = $mesa2;
        	$testMmse->perro2 = $perro2;
        	$testMmse->lapiz = $lapiz;
        	$testMmse->reloj = $reloj;
        	$testMmse->tomar_papel = $papel1;
        	$testMmse->dejar_papel = $papel3;
        	$testMmse->doblar_papel = $papel2;
        	$testMmse->repetir_frase = $repetir_frase;
        	$testMmse->leer_obedecer = $ojos_cerrados;
        	$testMmse->escribir_frase = $escribir_frase;
        	$testMmse->hacer_dibujo = $dibujo;

        	$testMmse->save();
        	DB::commit();



    	return Response::json(["exito" => "Los datos han sido guardados"]);
    }

    public function editarEncuestasWhoqol(Request $request){
		try{

			DB::beginTransaction();

			$rut = $request->input("rut");
			$tipoEncuesta = $request->input("tipo-encuesta");
			$inicial = $request->input("inicio");
			//$genero = $request->input("genero-paciente");

			//$this->registrarPaciente($rut, $genero);
			//return Response::json(["exito" => "Los datos han sido guardados","rut" => $rut]);
			
			$testWhoqol = TestWhoqol::where("rut_paciente", "=", $rut)->first();
			/*if($testWhoqol == null) {
				$testWhoqol = new TestWhoqol;
				
			}*/

			$testWhoqol = new TestWhoqol;
			$testWhoqol->rut_paciente = $rut;
			if($request->input("fecha-encuesta")) $testWhoqol->fecha_test = $request->input("fecha-encuesta");
			//$testWhoqol->inicial = $inicial;

			if($request->input("whoqol-1")) $testWhoqol->apoyo_amigos           = $request->input("whoqol-1");
			if($request->input("whoqol-2")) $testWhoqol->calidad_vida           = $request->input("whoqol-2");
			if($request->input("whoqol-3")) $testWhoqol->salud                  = $request->input("whoqol-3");
			if($request->input("whoqol-4")) $testWhoqol->dolor                  = $request->input("whoqol-4");
			if($request->input("whoqol-5")) $testWhoqol->calidad_vida           = $request->input("whoqol-5");
			if($request->input("whoqol-6")) $testWhoqol->alegria_vida           = $request->input("whoqol-6");
			if($request->input("whoqol-7")) $testWhoqol->sentido_vida           = $request->input("whoqol-7");
			if($request->input("whoqol-8")) $testWhoqol->concentracion          = $request->input("whoqol-8");
			if($request->input("whoqol-9")) $testWhoqol->seguridad              = $request->input("whoqol-9");
			if($request->input("whoqol-10")) $testWhoqol->ambiente_saludable    = $request->input("whoqol-10");
			if($request->input("whoqol-11")) $testWhoqol->energia               = $request->input("whoqol-11");
			if($request->input("whoqol-12")) $testWhoqol->apariencia_fisica     = $request->input("whoqol-12");
			if($request->input("whoqol-13")) $testWhoqol->dinero                = $request->input("whoqol-13");
			if($request->input("whoqol-14")) $testWhoqol->informacion           = $request->input("whoqol-14");
			if($request->input("whoqol-15")) $testWhoqol->actividad_ocio        = $request->input("whoqol-15");
			if($request->input("whoqol-16")) $testWhoqol->desplazamiento        = $request->input("whoqol-16");
			if($request->input("whoqol-17")) $testWhoqol->sueno                 = $request->input("whoqol-17");
			if($request->input("whoqol-18")) $testWhoqol->actividad_diaria      = $request->input("whoqol-18");
			if($request->input("whoqol-19")) $testWhoqol->capacidad_trabajo     = $request->input("whoqol-19");
			if($request->input("whoqol-20")) $testWhoqol->mismo                 = $request->input("whoqol-20");
			if($request->input("whoqol-21")) $testWhoqol->relaciones_personales = $request->input("whoqol-21");
			if($request->input("whoqol-22")) $testWhoqol->vida_sexual           = $request->input("whoqol-22");
			if($request->input("whoqol-23")) $testWhoqol->apoyo_amigos          = $request->input("whoqol-23");
			if($request->input("whoqol-24")) $testWhoqol->lugar_vive            = $request->input("whoqol-24");
			if($request->input("whoqol-25")) $testWhoqol->servicio_sanitario    = $request->input("whoqol-25");
			if($request->input("whoqol-26")) $testWhoqol->transporte            = $request->input("whoqol-26");
			if($request->input("whoqol-27")) $testWhoqol->sentimiento_negativo  = $request->input("whoqol-27");
			
			if($request->input("ayuda")) $testWhoqol->ayuda                     = $request->input("ayuda");
			if($request->input("tiempo")) $testWhoqol->tiempo                   = $request->input("tiempo");
			if($request->input("comentario")) $testWhoqol->comentario           = $request->input("comentario");
			
				
			$testWhoqol->save();
	

			DB::commit();

			return Response::json(["exito" => "Los datos han sido guardados"]);
		} catch(Exception $ex){
			DB::rollback();
			return Response::json(["error" => "Error al guardar los datos", "msg" => $ex->getMessage()]);
		}

	}

	public function editarEncuestasEq(Request $request){
		try{

			DB::beginTransaction();

			$rut = $request->input("rut");
			$tipoEncuesta = $request->input("tipo-encuesta");
			$inicial = $request->input("inicio");

			/*$testEq = TestEq::where("rut_paciente", "=", $rut)->first();
			if($testEq == null) {
				$testEq = new TestEq;
			}*/

			$testEq = new TestEq;
			$testEq->rut_paciente = $rut;
			if($request->input("fecha-encuesta")) $testEq->fecha_test = $request->input("fecha-encuesta");
			//$testEq->inicial = $inicial;

			if($request->input("eq-p-1")) $testEq->movilidad             = $request->input("eq-p-1");
			if($request->input("eq-p-2")) $testEq->cuidado_personal      = $request->input("eq-p-2");
			if($request->input("eq-p-3")) $testEq->actividad             = $request->input("eq-p-3");
			if($request->input("eq-p-4")) $testEq->dolor_malestar        = $request->input("eq-p-4");
			if($request->input("eq-p-5")) $testEq->ansiedad_depresion    = $request->input("eq-p-5");
			//if($request->input("eq-p-7")) $testEq->escala                = $request->input("");
			if($request->input("eq-p-6")) $testEq->enfermedad            = $request->input("eq-p-6");
			if($request->input("eq-p-7")) $testEq->enfermedad_familia    = $request->input("eq-p-7");
			if($request->input("eq-p-8")) $testEq->enfermedad_otros      = $request->input("eq-p-8");
			if($request->input("eq-p-16")) $testEq->edad                 = $request->input("eq-p-16");
			if($request->input("eq-p-9")) $testEq->genero                = $request->input("eq-p-9");
			if($request->input("eq-p-10")) $testEq->fuma                 = $request->input("eq-p-10");
			if($request->input("eq-p-11")) $testEq->trabajo_salud_social = $request->input("eq-p-11");
			if($request->input("eq-p-23")) $testEq->detalle_trabajo      = $request->input("eq-p-23");
			if($request->input("eq-p-24")) $testEq->actividad_principal  = $request->input("eq-p-24");
			if($request->input("eq-p-14")) $testEq->nivel_estudio        = $request->input("eq-p-14");
			if($request->input("eq-p-15")) $testEq->codigo_postal        = $request->input("eq-p-15");
			
				
			$testEq->save();
	

			DB::commit();

			return Response::json(["exito" => "Los datos han sido guardados"]);
		} catch(Exception $ex){
			DB::rollback();
			return Response::json(["error" => "Error al guardar los datos", "msg" => $ex->getMessage()]);
		}

	}

}
