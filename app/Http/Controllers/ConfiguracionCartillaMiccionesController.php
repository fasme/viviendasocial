<?php

class ConfiguracionCartillaMiccionesController extends BaseController{

	public static function guardarConfiguracionMicciones()
	{
		try{
			$datos=Input::get("datos");
			$rut=Input::get("rut");
			$rut_usuario=Auth::user()->rut;

			$r=DB::statement(
				DB::raw(
					"
					DELETE FROM configuracion_cartilla_micciones
					WHERE id_configuracion_cartilla_micciones IN(
					SELECT id_configuracion_cartilla_micciones
					FROM configuracion_cartilla_micciones as ccm
					INNER JOIN cartilla_micciones as cm ON ccm.id_cartilla_micciones=cm.id_cartilla_micciones
					WHERE cm.rut_paciente=$rut
					AND ccm.rut_usuario=$rut_usuario
					)
					"
					)
				);
			
			foreach($datos as $config)
			{
				
				
				$id_config_cartilla=new ConfiguracionCartillaMicciones;
				$id_config_cartilla->id_cartilla_micciones=$config["id_cartilla_micciones"]==""?null:$config["id_cartilla_micciones"];
				$id_config_cartilla->id_configuracion_nicturia=$config["id_electrodo"];
				$id_config_cartilla->tiempo=$config["minutos"];
				$id_config_cartilla->numero_alertas=$config["cantidad"];
				$id_config_cartilla->inicio_configuracion=$config["fecha_inicio"];
				$id_config_cartilla->fin_configuracion=$config["fecha_final"];
				$id_config_cartilla->rut_usuario=$rut_usuario;
				$id_config_cartilla->save();
			}
			$r=DB::statement(
				DB::raw(
					"
					UPDATE cartilla_micciones 
					SET
					cartilla_visible=TRUE
					WHERE fecha_cartilla BETWEEN '".$config["fecha_inicio"]."' AND '".$config["fecha_final"]."'
					AND rut_paciente=$rut
					
					"
					)
				);
			$r=DB::statement(
				DB::raw(
					"
					UPDATE cartilla_micciones 
					SET
					cartilla_visible=FALSE
					WHERE fecha_cartilla NOT BETWEEN '".$config["fecha_inicio"]."' AND '".$config["fecha_final"]."'
					AND rut_paciente=$rut
					
					"
					)
				);
			return Response::json(["exito"=>"La configuración se ha guardado correctamente."]);
		}catch(Exception $e)
		{
			return Response::json(["error"=>"Ha ocurrido un error.","msg"=>$e->getMessage()]);	
		}
	}
	
	public static function cargarConfiguracionMicciones($rutPaciente=null)
	{
		
		$rut=Input::has("rut")?Input::get("rut"):$rutPaciente;
		$rut_usuario=Auth::user()->rut;

		$r=DB::select(
			DB::raw(
				"
				SELECT tiempo,inicio_configuracion,fin_configuracion,id_configuracion_nicturia 
				FROM configuracion_cartilla_micciones as ccm
				INNER JOIN cartilla_micciones as cm ON ccm.id_cartilla_micciones=cm.id_cartilla_micciones
				WHERE cm.rut_paciente=$rut
				AND ccm.rut_usuario=$rut_usuario
				LIMIT 1
				"
				));
		
		if($r)
		{
			$resultado=$r[0];
			
			$datos=[
			"minutos"=>$resultado->tiempo,
			"sensor"=>$resultado->id_configuracion_nicturia,
			"fecha_inicio"=>$resultado->inicio_configuracion,
			"fecha_final"=>$resultado->fin_configuracion
			];
			return Response::json($datos);
		}
		return Response::json(["error"=>"No se han encontrado datos"]); //no es necesario mostrarlo
	}

}
?>
