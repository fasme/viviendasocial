<?php

class EstadisticasController extends BaseController {

	private $eventos = [1, 2, 3];

	public function obtenerRut(){
		$institucion = Input::get("institucion");
		$comuna = Input::get("comuna");
		$rutUsuario = Auth::user()->rut;

		$rut = Paciente::obtenerRutUsuario($institucion, $comuna, $rutUsuario);
		return Response::json($rut);

	}

	private function obtenerComunasConAlertas($institucion, $evento, $comuna){
		if($evento == 0){
			$comunasCaidas = Comuna::obtenerNombresComunasCaidas($institucion);
			$comunasPuertaNicturia = Comuna::obtenerNombresComunasPuertasNicturia($institucion, null);
			$comunas = array_merge($comunasCaidas, $comunasPuertaNicturia);
			$comunas = array_unique($comunas);
		}
		if($evento == EnumEvento::$CAIDA) $comunas = Comuna::obtenerNombresComunasCaidas($institucion);
		if($evento == EnumEvento::$PUERTA || $evento == EnumEvento::$NICTURIA) $comunas = Comuna::obtenerNombresComunasPuertasNicturia($institucion, $evento);

		$comunas = ($comuna != 0) ? ["", Comuna::obtenerNombreComuna($comuna)] : $comunas;

		return $comunas;
	}

	private function generarGraficoCantComuna($objPHPExcel, $comuna, $institucion, $rutPaciente, $evento){
		$objWorksheet = $objPHPExcel->getActiveSheet();
		$nombreInstitucion = ($institucion == 0) ? "Todas las instituciones" : Institucion::find($institucion)->nombre_institucion;

		$comunas = $this->obtenerComunasConAlertas($institucion, $evento, $comuna);

		$data = [$comunas];

		if($evento != 0){
			$totalAlerta = [Funciones::obtenerNombreEvento($evento)];
			
			foreach($comunas as $nombre){
				if(empty($nombre)) continue;
				$total = ($evento == EnumEvento::$CAIDA) ? Comuna::obtenerTotalAlertasCaidas($nombre, $institucion) : Comuna::obtenerTotalAlertas($evento, $nombre, $institucion);
				if($total != "" || $total != null || $total != 0) $totalAlerta[] = $total;
			}
			$data[] = $totalAlerta;
		}
		else{
			foreach($this->eventos as $tipoEvento){
				$totalAlerta = [Funciones::obtenerNombreEvento($tipoEvento)];
				foreach($comunas as $nombre){
					if(empty($nombre)) continue;
					$total = ($tipoEvento == EnumEvento::$CAIDA) ? Comuna::obtenerTotalAlertasCaidas($nombre, $institucion) : Comuna::obtenerTotalAlertas($tipoEvento, $nombre, $institucion);
					if($total != 0) $totalAlerta[] = $total;
				}
				$data[] = $totalAlerta;
			}
		}

		$lengthData = count($data);
		$totalComunas = count($comunas) + 1;
		$rangoTotalComunas = range(3, $totalComunas);

		$objWorksheet->fromArray($data);

		$dataseriesLabels = array();

		foreach($rangoTotalComunas as $numero){
			$letra = Funciones::toAlpha($numero);
			$dataseriesLabels[] = new PHPExcel_Chart_DataSeriesValues('String', 'CantidadPorComuna!$'.$letra.'$1', NULL, 1);
		}

		$xAxisTickValues = array(
			new PHPExcel_Chart_DataSeriesValues('String', 'CantidadPorComuna!$A$2:$A$'.$lengthData, NULL, 4),  
			);

		$dataSeriesValues = array();

		foreach($rangoTotalComunas as $numero){
			$letra = Funciones::toAlpha($numero);
			$dataSeriesValues[] = new PHPExcel_Chart_DataSeriesValues('Number', 'CantidadPorComuna!$'.$letra.'$2:$'.$letra.'$'.$lengthData, NULL, 1);
		}

		$series = new PHPExcel_Chart_DataSeries(
			PHPExcel_Chart_DataSeries::TYPE_BARCHART,       
			PHPExcel_Chart_DataSeries::GROUPING_STANDARD,   
			range(0, count($dataSeriesValues)-1),          
			$dataseriesLabels,                              
			$xAxisTickValues,                               
			$dataSeriesValues                               
			);

		$series->setPlotDirection(PHPExcel_Chart_DataSeries::DIRECTION_COL);

		$plotarea = new PHPExcel_Chart_PlotArea(NULL, array($series));

		$legend = new PHPExcel_Chart_Legend(PHPExcel_Chart_Legend::POSITION_RIGHT, NULL, false);

		$title = new PHPExcel_Chart_Title("Cantidad de alertas por comuna en $nombreInstitucion");
		$yAxisLabel = new PHPExcel_Chart_Title('Cantidad');

		$chart = new PHPExcel_Chart(
			'chart1',      
			$title,         
			$legend,        
			$plotarea,      
			true,           
			0,              
			NULL,           
			$yAxisLabel     
			);

		$chart->setTopLeftPosition('A7');
		$chart->setBottomRightPosition('H20');

		$objWorksheet->addChart($chart);

		$objPHPExcel->getActiveSheet()->setTitle('CantidadPorComuna');

	}

	private function generarGraficoGenero($objPHPExcel, $comuna, $institucion, $rutPaciente, $evento){
		$objWorksheet = $objPHPExcel->createSheet(2);
		$objWorksheet->setTitle("Genero");

		$nombreInstitucion = ($institucion == 0) ? "Todas las instituciones" : Institucion::find($institucion)->nombre_institucion;

		$comunas = $this->obtenerComunasConAlertas($institucion, $evento, $comuna);

		$generos = [["nombre" => "Masculino", "genero" => "M"], ["nombre" => "Femenino", "genero" => "F"]];

		$data = [$comunas];

		if($evento != 0){
			foreach ($generos as $genero) {
				$totalAlerta = [$genero["nombre"]." ".Funciones::obtenerNombreEvento($evento)];
				foreach($comunas as $nombre){
					if(empty($nombre)) continue;
					$idComuna = Comuna::where("nombre_comuna", "=", $nombre)->first()->id_comuna;
					$total = ($evento == EnumEvento::$CAIDA) ? AlertaCaida::obtenerTotalDeCaidasPorGenero($genero["genero"], $institucion, $idComuna) : AlertaDetalle::obtenerTotalAlertasPorGenero($evento, $genero["genero"], $institucion, $idComuna);
					if($total != "" || $total != null) $totalAlerta[] = $total;
				}
				$data[] = $totalAlerta;
			}
		}
		else{
			foreach($this->eventos as $tipoEvento){
				foreach($generos as $genero){
					$totalAlerta = [$genero["nombre"]." ".Funciones::obtenerNombreEvento($tipoEvento)];
					foreach($comunas as $nombre){
						if(empty($nombre)) continue;
						$idComuna = Comuna::where("nombre_comuna", "=", $nombre)->first()->id_comuna;
						$total = ($tipoEvento == EnumEvento::$CAIDA) ? AlertaCaida::obtenerTotalDeCaidasPorGenero($genero["genero"], $institucion, $idComuna) : AlertaDetalle::obtenerTotalAlertasPorGenero($tipoEvento, $genero["genero"], $institucion, $idComuna);
						if($total != 0) $totalAlerta[] = $total;
					}
					$data[] = $totalAlerta;
				}
			}
		}

		$lengthData = count($data);
		$lenSubData = count($data[0]);
		$lenComunas = count($comunas);

		$totalComunas = count($comunas) + 1;
		$rangoTotalComunas = range(3, $lengthData + 1);
		$rangeSubData = range(2, $lenSubData);

		$objWorksheet->fromArray($data);

		$dataseriesLabels = array();

		for($i = 2; $i < $lengthData+1; $i++)
			$dataseriesLabels[] = new PHPExcel_Chart_DataSeriesValues('String', 'Genero!$A$'.$i, NULL, 1);

		$xAxisTickValues = array(
			new PHPExcel_Chart_DataSeriesValues('String', 'Genero!$B$1:$'.Funciones::toAlpha($totalComunas+1).'$1', NULL, 4),  
			);

		$dataSeriesValues = array();

		for($i = 2; $i <= count($data); $i++){;
			$letraFin = Funciones::toAlpha($lenSubData + 1);
			$celda = 'Genero!$B$'.$i.':$'.$letraFin.'$'.$i;
			$dataSeriesValues[] = new PHPExcel_Chart_DataSeriesValues('Number', $celda, NULL, 1);
		}

		$series = new PHPExcel_Chart_DataSeries(
			PHPExcel_Chart_DataSeries::TYPE_BARCHART,       
			PHPExcel_Chart_DataSeries::GROUPING_STANDARD,   
			range(0, count($dataSeriesValues)-1),          
			$dataseriesLabels,                              
			$xAxisTickValues,                               
			$dataSeriesValues                               
			);

		$series->setPlotDirection(PHPExcel_Chart_DataSeries::DIRECTION_COL);

		$plotarea = new PHPExcel_Chart_PlotArea(NULL, array($series));

		$legend = new PHPExcel_Chart_Legend(PHPExcel_Chart_Legend::POSITION_RIGHT, NULL, false);

		$title = new PHPExcel_Chart_Title("Cantidad de alertas por comuna en $nombreInstitucion");
		$yAxisLabel = new PHPExcel_Chart_Title('Cantidad');

		$chart = new PHPExcel_Chart(
			'chart2',      
			$title,         
			$legend,        
			$plotarea,      
			true,           
			0,              
			NULL,           
			$yAxisLabel     
			);

		$chart->setTopLeftPosition('A'.($lengthData + 3));
		$chart->setBottomRightPosition('H30');

		$objWorksheet->addChart($chart);
	}

	public function descargarReporte(){
		$objPHPExcel = new PHPExcel();
		
		$comuna = Input::get("comuna");
		$institucion = Input::get("institucion");
		$rutPaciente = Input::get("rut");
		$evento = Input::get("evento");

		$this->generarGraficoCantComuna($objPHPExcel, $comuna, $institucion, $rutPaciente, $evento);
		$this->generarGraficoGenero($objPHPExcel, $comuna, $institucion, $rutPaciente, $evento);

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Reporte.xlsx"');
		header('Cache-Control: max-age=0');

		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);

		$objWriter->setOffice2003Compatibility(true);
		$objWriter->setIncludeCharts(TRUE);
		$objWriter->save("php://output");
	}

}