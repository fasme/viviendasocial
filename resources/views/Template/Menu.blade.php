<div class="col-md-2 pad-der-10 menuDerecha sidebar-offcanvas" id="sidebar">
	<div class="panel-group  float-der sidebar-offcanvas sidebar-offcanvas" id="accordion" role="tablist" aria-multiselectable="true">

    <div class="row" style="background-color: #EC3A53;">

        <a href="{{URL::to('index')}}">
            <img src="{{ asset('images/logo.png') }}" style="width: 100%; margin-top: 10%; margin-bottom: 10%; padding-right: 10px; padding-left: 10px;"> 
        </a>

       
    </div>

    <div class="row" style="background-color: #EC3A53;">
        <div class="col-md-4">
             <img src="{{ asset('images/sinfoto.png') }}" class="img-circle" alt="Cinque Terre" width="100%">
        </div>

        <div class="col-md-8">
            {{Auth::user()->nombres}} {{Auth::user()->apellido_paterno}}  {{Auth::user()->apellido_materno}} 
        </div>
    </div>

    <div class="row" style="background-color: #2A698E; height: 100%">

        <div class="panel-heading" role="tab">
            <h4 class="panel-title" style="color: white; padding-bottom: 20px;">
                Menú
            </h4>
        </div>

        <div class="panel-heading" role="tab">
            <h4 class="panel-title" style="color: white;">
                <a data-parent="#accordion" href="{{URL::to('index')}}">
                    INICIO
                </a>
            </h4>
        </div>
        <div class="panel-heading" role="tab">
            <h4 class="panel-title" style="color: white;">
                <a data-parent="#accordion" href="{{URL::to('paciente/detallePaciente/'.Auth::user()->rut)}}">
                    ALERTAS
                </a>
            </h4>
        </div>
        <div class="panel-heading" role="tab">
            <h4 class="panel-title" style="color: white;">
                <a data-parent="#accordion" href="{{URL::to('paciente/pacientes')}}">
                    PACIENTES
                </a>
            </h4>
        </div>
        <div class="panel-heading" role="tab">
            <h4 class="panel-title" style="color: white;">
                <a data-parent="#accordion" href="{{URL::to('paciente/detallePaciente/'.Auth::user()->rut)}}">
                    USUARIOS
                </a>
            </h4>
        </div>
        <div class="panel-heading" role="tab">
            <h4 class="panel-title" style="color: white;">
                <a data-parent="#accordion" href="{{URL::to('paciente/detallePaciente/'.Auth::user()->rut)}}">
                    SERVICIOS EMERGEN.
                </a>
            </h4>
        </div>
        <div class="panel-heading" role="tab">
            <h4 class="panel-title" style="color: white;">
                <a data-parent="#accordion" href="{{URL::to('paciente/detallePaciente/'.Auth::user()->rut)}}">
                    CONTACTO ADMIN.
                </a>
            </h4>
        </div>
        <div class="panel-heading" role="tab">
            <h4 class="panel-title" style="color: white;">
                <a data-parent="#accordion" href="{{URL::to('paciente/detallePaciente/'.Auth::user()->rut)}}">
                    NICTURIA
                </a>
            </h4>
        </div>
        <div class="panel-heading" role="tab">
            <h4 class="panel-title" style="color: white;">
                <a data-parent="#accordion" href="{{URL::to('paciente/detallePaciente/'.Auth::user()->rut)}}">
                    CAIDAS
                </a>
            </h4>
        </div>
        <div class="panel-heading" role="tab">
            <h4 class="panel-title" style="color: white;">
                <a data-parent="#accordion" href="{{URL::to('paciente/detallePaciente/'.Auth::user()->rut)}}">
                    BOTÓN PÁNICO
                </a>
            </h4>
        </div>
        <div class="panel-heading" role="tab">
            <h4 class="panel-title" style="color: white;">
                <a data-parent="#accordion" href="{{URL::to('paciente/detallePaciente/'.Auth::user()->rut)}}">
                    MONÓXIDO DE CARBO.
                </a>
            </h4>
        </div>
        <div class="panel-heading" role="tab">
            <h4 class="panel-title" style="color: white;">
                <a data-parent="#accordion" href="{{URL::to('paciente/detallePaciente/'.Auth::user()->rut)}}">
                    HUMEDAD
                </a>
            </h4>
        </div>
        <div class="panel-heading" role="tab">
            <h4 class="panel-title" style="color: white;">
                <a data-parent="#accordion" href="{{URL::to('paciente/detallePaciente/'.Auth::user()->rut)}}">
                    TEMPERATURA
                </a>
            </h4>
        </div>
    </div>
		



<!-- PACIENTE -->
        
<!-- /PACIENTE -->

<!-- USUARIO -->
        @if(App\Models\Usuario::obtenerTipoUsuario(Auth::user()->rut) == 2 || App\Models\Usuario::obtenerTipoUsuario(Auth::user()->rut) == 3)
        <div class="panel panel-default">
            <div class="panel-heading" role="tab">
                <h4 class="panel-title">
                    <a data-parent="#accordion" href="{{URL::to('usuario/usuarios')}}">
                        Usuarios
                    </a>
                </h4>
            </div>
        </div>
        @endif
<!-- /USUARIO -->

<!-- CONFIGURACIÓN -->
@if(true==false)
        @if (Encargado::find(Auth::user()->rut) != null &&  Encargado::find(Auth::user()->rut)->id_tipo_usuario=="2")
        <div class="panel panel-default">
            <div class="panel-heading" role="tab">
                <h4 class="panel-title">
                    <a data-parent="#accordion" href="{{URL::to('evento/eventos')}}">
                        Configuración Alertas
                    </a>
                </h4>
            </div>
        </div>
        @endif
@endif
<!-- /CONFIGURACIÓN -->

<!-- ESTADÍSTICAS -->
@if(true==false)
        @if (Encargado::find(Auth::user()->rut) != null &&  Encargado::find(Auth::user()->rut)->id_tipo_usuario != 3&&  Encargado::find(Auth::user()->rut)->id_tipo_usuario != 1)
        <div class="panel panel-default">
            <div class="panel-heading" role="tab">
                <h4 class="panel-title">
                    <a data-parent="#accordion" href="{{URL::to('estadisticas/reportes')}}">
                        Estadísticas
                    </a>
                </h4>
            </div>
        </div>
        @endif
@endif
<!-- /ESTADÍSTICAS -->
    </div>
</div>



        
