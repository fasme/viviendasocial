<nav class="col-md-12 navbar navbar-top navbar-default marg-abajo display-flex" role="navigation">
	<div class="navbar-header">
		<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
			aria-expanded="false" aria-controls="navbar">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
	</div>
	<div id="navbar" class="collapse navbar-collapse" style="margin: 0; padding: 0;" aria-expanded="false">
		<div class="col-md-12">
			<ul class="nav navbar-nav">
				<li >
					<a href="{{URL::to('documentos') }}"><span class="glyphicon glyphicon-file"></span> MANUAL</a>
				</li>
				<ol class="breadcrumb" style="background: none;">
					<li><a href="{{URL::to('index')}}"><span class="glyphicon glyphicon-home"></span> INICIO</a></li>
					@yield("miga")
				</ol>


				<li>
					<a href="{{ URL::previous() }}" data-toggle="modal"><span class="glyphicon glyphicon-circle-arrow-left"></span> ACERCA DE</a>
				</li>

				<li>
					<a href="" data-toggle="modal"><span class="glyphicon glyphicon-circle-arrow-left"></span> CONTACTO</a>
				</li>
				
				
				<li>
					<a href="{{ URL::to('sesion/cerrarSesion') }}"><span class="glyphicon glyphicon-log-out"></span> Salir</a>
				</li>

			</ul>
				
		</div>
	</div>
</nav>

<div class="modal fade" id="modalPerfil" tabindex="-1" role="dialog" aria-labelledby="myModalLabelPerfil" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabelPerfil">Perfil</h4>
			</div>
			<div class="modal-body" >
				{{ Form::open(array('url' => 'perfil/editarPerfil', 'method' => 'post', 'class' => 'form-horizontal', 'role' => 'form', 'id' => 'formPerfil', "style" => 'width: 100%;')) }}
				{{ Form::hidden('rut', Auth::user()->rut) }}
				<div class="col-md-12">
					<div class="form-group">
						<label for="motivo" class="col-sm-2 control-label">Nombres: </label>
						<div class="col-sm-10">
							{{Form::text('nombre', null, array('id' => 'nombrePerfil', 'class' => 'form-control', 'onkeypress' => 'return textoEspacioGuion(event)'))}}
						</div>
					</div>
					<div class="form-group">
						<label for="motivo" class="col-sm-2 control-label">Apellido paterno: </label>
						<div class="col-sm-10">
							{{Form::text('apellido_p', null, array('id' => 'apellidoPaternoPerfil', 'class' => 'form-control', 'onkeypress' => 'return textoEspacioGuion(event)'))}}
						</div>
					</div>
					<div class="form-group">
						<label for="motivo" class="col-sm-2 control-label">Apellido materno: </label>
						<div class="col-sm-10">
							{{Form::text('apellido_m', null, array('id' => 'apellidoMaternoPerfil', 'class' => 'form-control', 'onkeypress' => 'return textoEspacioGuion(event)'))}}
						</div>
					</div>
					<div class="form-group">
						<label for="motivo" class="col-sm-2 control-label">Correo: </label>
						<div class="col-sm-10">
							{{Form::text('correo', null, array('id' => 'correoPerfil', 'class' => 'form-control'))}}
						</div>
					</div>
					<div class="form-group">
						<label for="motivo" class="col-sm-2 control-label">Teléfono: </label>
						<div class="col-sm-10">
							{{Form::text('telefono', null, array('id' => 'telefonoPerfil', 'class' => 'form-control'))}}
						</div>
					</div>
					<div class="form-group error">
						<label class="col-sm-2 control-label">Contraseña: </label>
						<div class="col-sm-10">
							{{ Form::password('password', array('class' => 'form-control', 'id' => 'passwordPerfil')) }}
						</div>
					</div>
					<div class="form-group error">
						<label class="col-sm-2 control-label">Confirmar contraseña: </label>
						<div class="col-sm-10">
							{{ Form::password('confirmPassword', array('class' => 'form-control', 'id' => 'confirmPasswordPerfil')) }}
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-12">
						<button type="submit" class="btn btn-primary">Aceptar</button>
						<button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
					</div>
				</div>
				{{ Form::close() }}
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">

$(function(){
	$("#formPerfil").formValidation({
		excluded: ':disabled',
		framework: 'bootstrap',
		fields: {
			nombre: {
				validators:{
					notEmpty: {
						message: 'El nombre  es obligatorio'
					}
				}
			},
			apellido: {
				validators:{
					notEmpty: {
						message: 'El apellido  es obligatorio'
					}
				}
			},
			correo: {
                validators: {
                    emailAddress: {
                        message: 'El correo no es valido'
                    },
                    notEmpty: {
						message: 'El correo  es obligatorio'
					}
                }
            },
            password: {
            	validators:{
            		notEmpty: {
            			message: 'La contraseña es obligatoria'
            		},
            		identical: {
            			field: 'confirmPassword',
            			message: 'Las contraseñas deben coincidir'
            		}
            	}
            },
            telefono: {
            	validators:{
            		notEmpty: {
            			message: 'El teléfono es obligatorio'
            		},
            		 integer: {
                        message: 'debe ingresar un valor númerico'
                    }
            	}
            },
            confirmPassword: {
            	validators: {
            		identical: {
            			field: 'password',
            			message: 'Las contraseñas deben coincidir'
            		}
            	}
            },
		}
	}).on('err.field.fv', function(e, data) {
		if (data.fv.getSubmitButton()) data.fv.disableSubmitButtons(false);
	}).on('success.field.fv', function(e, data) {
		if (data.fv.getSubmitButton()) data.fv.disableSubmitButtons(false);
	}).on("success.form.fv", function(evt){
		evt.preventDefault(evt);
		var $form = $(evt.target);
		$.ajax({
			url: $form.prop("action"),
			data: $form.serialize(),
			type: "post",
			dataType: "json",
			success: function(data){
				$('#modalPerfil').modal("hide");
				if(data.exito){
					bootbox.alert("<h4>"+data.exito+"</h4>", function(){ location.reload(); });
				}
				if(data.error){
					bootbox.alert("<h4>"+data.error+"</h4>");
				}
			},
			error: function(error){
				console.log(error);
			}
		});
	});

	$('#modalPerfil').on('hidden.bs.modal', function (e) {
		$("#formPerfil").formValidation("resetForm", true);
	});

	$('#modalPerfil').on('show.bs.modal', function (e) {
		$("#formPerfil #nombrePerfil").val("{{ Auth::user()->nombres }}");
		$("#formPerfil #apellidoPaternoPerfil").val("{{ Auth::user()->apellido_paterno }}");
		$("#formPerfil #apellidoMaternoPerfil").val("{{ Auth::user()->apellido_materno }}");
		$("#formPerfil #correoPerfil").val("{{ Auth::user()->correo }}");
		$("#formPerfil #telefonoPerfil").val("{{ Auth::user()->telefono }}");
		$("#formPerfil #passwordPerfil").val("{{ Session::get('password') }}");
		$("#formPerfil #confirmPasswordPerfil").val("{{ Session::get('password') }}");
	});

});

</script>