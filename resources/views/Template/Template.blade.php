<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{URL::asset('favicon.ico') }}" type="image/x-icon" rel="shortcut icon"/>

    {{ HTML::style('css/bootstrap.css') }}
    {{ HTML::style('css/estilos.css') }}
    {{ HTML::style('css/bootstrap-datetimepicker.css') }}
    {{ HTML::style('css/offcanvas.css') }}
    {{ HTML::style('css/breadcrumb.css') }}
    {{ HTML::style('css/jquery.dataTables.min.css') }}
    {{ HTML::style('css/dataTables.responsive.css') }}
    {{ HTML::style('css/bootstrap-duallistbox.min.css') }}
    {{ HTML::style('css/fullcalendar.css') }}
    {{ HTML::style('css/typeahead.css') }}
    {{ HTML::style('css/jquery.timepicker.css') }}
    {{ HTML::style('css/bootstrap.vertical-tabs.css') }}  <!-- para tab vertical en encuestas-->
    {{ HTML::style('css/simple-sidebar.css') }} <!-- para menu que se contrae en encuestas-->

    {{ HTML::style('css/fileinput.min.css') }}

    {{ HTML::script('js/jspdf.debug.js') }} <!-- Libreria para realizar impresiones en PDF-->
    {{ HTML::script('js/html2canvas.js') }}
    {{ HTML::script('js/jspdf.plugin.autotable.js') }}
    {{ HTML::script('js/jquery-1.11.2.min.js') }}
    {{ HTML::script('js/moment.min.js') }}
    {{ HTML::script('js/es.js') }}
    {{ HTML::script('js/bootstrap.min.js') }}
    {{ HTML::script('js/bootbox.min.js') }}
    {{ HTML::script('js/formValidation.min.js') }}
    {{ HTML::script('js/bfv.js') }}
     <!-- HTML::script('js/language/es_ES.js') -->
    {{ HTML::script('js/bootstrap-datetimepicker.min.js') }}
    {{ HTML::script('js/bootstrap-growl.min.js') }}
    {{ HTML::script('js/funciones-growl.js') }}
    {{ HTML::script('js/funciones.js') }}
    {{ HTML::script('js/funciones-set-encuestas.js') }}    
    {{ HTML::script('js/jquery.dataTables.min.js') }}
    {{ HTML::script('js/dataTables.bootstrap.js') }}
    {{ HTML::script('js/dataTables.responsive.js') }}
    {{ HTML::script('js/jquery.bootstrap-duallistbox.min.js') }}
    {{ HTML::script('js/typeahead.jquery.min.js') }}
    {{ HTML::script('js/bloodhound.js') }}
    {{ HTML::script('js/jquery.timepicker.min.js') }}
    
    {{ HTML::script('js/highstock.js') }}
    {{ HTML::script('js/highcharts-more.js') }}
    {{ HTML::script('js/moment.min.js') }}

    {{ HTML::script('js/fileinput.min.js') }}
    {{ HTML::script('js/fileinput_locale_es.js') }}
    {{ HTML::script('js/jquery.maskedinput.min.js') }}
	{{ HTML::script('js/rgbcolor.js') }}
	{{ HTML::script('js/StackBlur.js') }}
	{{ HTML::script('js/canvg.js') }}
	{{ HTML::script('js/exporting.js') }}
    
    
  

    <style type="text/css" media="screen">
       #dvLoading{
            background-color:rgba(188,188,188,0.4);
            height: 150px;
            width: 300px;
            position: fixed;
            z-index: 1000;
            left: 50%;
            top: 50%;
            margin: -25px 0 0 -25px;
            text-align: center;
            margin-left:-150px;
        }

        .classLoadingText{
            position: relative;
            z-index: 1001;
            top: 25%; 
            font-size: 14px;
        }

        #map {
            height: 300px;
        }

        html,body{
            height: 100%;
        }
    </style>

    <title>@yield("titulo")</title>
    <script>
        var resize = function () {
            var width = $("#main").width();
            $(".miga").width(width);
        }

        var resizeBreadCrumb = function () {
            var height = $("#navbar ").height();
            if (height > 50) $("#b2").css("float", "left");
            else $("#b2").css("float", "right");
        }

        $(function() {

            resizeBreadCrumb();

            $("#bread").width($(".navbar-top").width());

            bootbox.setDefaults({locale: "es"});

            $('[data-toggle="offcanvas"]').click(function () {
                $('.row-offcanvas').toggleClass('active');
            });

            $(window).on('resize', function () {
                $("#bread").width($(".navbar-top").width());
                resizeBreadCrumb();
            });

        });

        var marker;

              function initMap() {
                var map = new google.maps.Map(document.getElementById('map'), {
                  zoom: 13,
                  center: {lat: 59.325, lng: 18.070}
                });

                marker = new google.maps.Marker({
                  map: map,
                  draggable: true,
                  animation: google.maps.Animation.DROP,
                  position: {lat: 59.327, lng: 18.067}
                });
                marker.addListener('click', toggleBounce);
              }

              function toggleBounce() {
                if (marker.getAnimation() !== null) {
                  marker.setAnimation(null);
                } else {
                  marker.setAnimation(google.maps.Animation.BOUNCE);
                }
              }

    </script>

    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAKUCTycz-4g3GNG0uRjY6rXGui2PurUAM&callback=initMap">
    </script>
    @yield("script")
    @yield("css")
</head>
<body>
    <div class="row" style="margin-top: 1px;">
        
        <div class="col-md-12 pad-izq">
            <div class="col-md-12 text-der" style="background-color: #1E698C ; ">
                <div class="fondo" style="top: 2.5%; letter-spacing: -3px; font-family: 'Antipasto','Open Sans', sans-serif;" >
                   
                    @if(Session::has("nombreEstablecimiento"))
                    <h3> {{ Session::get("nombreEstablecimiento") }} </h3>
                    @endif
                </div>
            </div>

            @include("Template/Header")
        </div>
    </div>
    <div class="container-fluid">

        <div class="row row-offcanvas row-offcanvas-left">

            @include("Template/Menu")
            <div class="col-md-10 main">
                <p class="pull-left visible-xs">
                    <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Mostrar menú</button>
                </p>
                <br>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    @yield("section")
                </div>
                <div id="dvLoading" style="display: none;">
                    <span class="classLoadingText" id="texto-div-cargando">Cargando página, por favor espere </span><br>
                    {{ HTML::image('images/ajax-loader.gif', '', array("style" => " margin-top: 50px;")) }}
                </div>
            </div>
        </div>
    </div>
</body>
</html>
