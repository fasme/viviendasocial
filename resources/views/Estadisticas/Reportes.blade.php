@extends("Template/Template")

@section("titulo")
Reportes
@stop

@section("miga")
<li><a href="#">Estadísticas</a></li>
<li><a href="#">Reportes</a></li>
@stop

@section("script")

<script>

var obtenerRut = function(){
	$.ajax({
		url: "obtenerRut",
		data: {institucion: $("#institucion").val(), comuna: $("#comuna").val()},
		type: "post",
		dataType: "json",
		success: function(data){
			$("#formReporte select[name='rut']").empty();
			for(var i = 0; i < data.length; i++){
				var option="<option value='" + data[i].rut + "'>" + data[i].rutConDV + "</option>";
				$("#formReporte select[name='rut']").append(option);
			}
		},
		error: function(error){
			console.log(error);
		}
	});
}

$(function(){

	$("#institucion, #comuna").on("change", function(){
		obtenerRut();
	});

});	

</script>

@stop

@section("section")

<fieldset>
	<legend>Reportes</legend>
	{{ Form::open(array('url' => 'estadisticas/descargarReporte', 'method' => 'post', 'class' => 'form-horizontal', 'role' => 'form', 'id' => 'formReporte')) }}

		<div class="form-group">
			<label for="evento" class="col-sm-2 control-label">Tipo de evento:</label>
			<div class="col-sm-10">
				<select id="evento" name="evento" class="form-control">
					<option value="0">Todos</option>
					<option value="1">Caídas</option>
					<option value="2">Acciones repetidas</option>
					<option value="3">Nicturia</option>
				</select>
			</div>
		</div>

		<div class="form-group">
			<label for="institucion" class="col-sm-2 control-label">Institución:</label>
			<div class="col-sm-10">
				{{ Form::select('institucion', $instituciones, 0, array('class' => 'form-control', 'id' => 'institucion')) }}
			</div>
		</div>

		<div class="form-group">
			<label for="comuna" class="col-sm-2 control-label">Comuna:</label>
			<div class="col-sm-10">
				{{ Form::select('comuna', $comunas, 0, array('class' => 'form-control', 'id' => 'comuna')) }}
			</div>
		</div>

		<div class="form-group">
			<label for="rut" class="col-sm-2 control-label">Rut paciente:</label>
			<div class="col-sm-10">
				<select name="rut" class="form-control">
					<option value="0">Todos</option>
				</select>
			</div>
		</div>
		{{ Form::submit('Descargar reporte', ["class" => "btn btn-primary"]) }}

	{{ Form::close() }}
</fieldset>

@stop
