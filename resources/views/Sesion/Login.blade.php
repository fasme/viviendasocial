<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link href="{{ URL::asset('favicon.ico') }}" type="image/x-icon" rel="shortcut icon" />

        {{ HTML::style('css/bootstrap.css') }}
        {{ HTML::style('css/login.css') }}

        {{ HTML::script('js/jquery-1.11.2.min.js') }}
        {{ HTML::script('js/funciones.js') }}
        {{ HTML::script('js/bootstrap.min.js') }}
        {{ HTML::script('js/formValidation.min.js') }}
        {{ HTML::script('js/bfv.js') }}
        {{ HTML::script('js/bootstrap-growl.min.js') }}
        {{ HTML::script('js/funciones-growl.js') }}

        {{ HTML::script('js/bootbox.min.js') }}
        <!-- login -->
        {{ HTML::style('css/estiloLogin.css') }}
        <!-- login 
        {{ HTML::style('css/style.css') }}
        {{ HTML::style('css/font-awesome.css') }}
        {{ HTML::style('css/animate.css') }}

        {{ HTML::script('js/jquery-scrolltofixed.js') }}
        {{ HTML::script('js/jquery.nav.js') }}
        {{ HTML::script('js/jquery.easing.1.3.js') }}
        {{ HTML::script('js/jquery.isotope.js') }}
        {{ HTML::script('js/wow.js') }}
        {{ HTML::script('js/custom.js') }}
         /login -->
        <title>eHomeseniors</title>

        <script>
            //ordenar json mediante la fecha
            function custom_sort(a, b) {
              return new Date(b.fechaOrden).getTime() - new Date(a.fechaOrden).getTime();
            }
            function cargarTodasNoticias(){
              console.log("cargando noticias");
              $.ajax({
                    url: "sesion/cargarTodasNoticias",
                    type: "post",
                    dataType: "json",
                    headers: {        
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    async: false,
                    success: function(data){
                      //console.log("respuesta", data[0].length);

                      //ordenar las noticias mediante la fecha
                      data.sort(custom_sort);

                      var contenido = "";

                      for (var i = 0; i < data[0].length; i++) {
                          contenido += '<div class="row not-detalle-noticia col-sm-4 width31"><span id="not-detalle-titulo" class="col-sm-12 h3 negrita manito" onclick="cargarUnaNoticia('+data[0][i]["id_noticia"]+',\''+data[0][i]["origen"]+'\');" ><span>'+data[0][i]["titulo_noticia"]+'</span></span><span id="not-detalle-fecha" class="col-sm-7">'+data[0][i]["fecha"]+'</span><span id="not-detalle-ver" class="col-sm-5 texto-derecha" ><a class="manito" onclick="cargarUnaNoticia('+data[0][i]["id_noticia"]+',\''+data[0][i]["origen"]+'\');">ver más</a></span>';
                          contenido += '<span id="not-detalle-contenido" class="col-sm-12 manito" ';
                          contenido += ' style="background-image: url(\'images/noticias/'+data[0][i]["imagen_noticia"]+'\')" alt="'+data[0][i]["titulo_noticia"]+'" title="'+data[0][i]["titulo_noticia"]+'" onclick="cargarUnaNoticia('+data[0][i]["id_noticia"]+',\''+data[0][i]["origen"]+'\');"';
                          contenido += '>';                          
                          //contenido += '<img src="images/noticias/'+data[i]["imagen_noticia"]+'" alt="'+data[i]["titulo_noticia"]+'" title="'+data[i]["titulo_noticia"]+'" onclick="cargarUnaNoticia('+data[i]["id_noticia"]+');" class="manito">';                      
                          contenido += '</span></div>';                            
                      };
                      $('#div-no-oculto').html(contenido);
                      $("#dvLoading").hide();
                    },
                    error: function(error){
                      console.log(error);
                      $("#dvLoading").hide();
                    }
              });
            }

            function cargarUnaNoticia(idNoticia,origen){
              $('#div-oculto').show(); 
              $('#div-no-oculto').hide();
              $('#volver-not').show();
              console.log("cargando una noticia");

              //evaluar a que origen pertenece la noticia si a ehome o a labitec
              if(origen == "labitec")
                origen = "Labitec";
              else
                origen = "";
              $.ajax({
                    url: "sesion/cargarNoticia"+origen+"",
                    type: "post",
                    data: { idNoticia : idNoticia },
                    headers: {        
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    dataType: "json",
                    async: false,
                    success: function(data){
                      console.log("seleccionar una noticia", data);

                      data.noticias.sort(custom_sort);

                      var contenido = "";
                      var imagen = "";
                      var noticias = "";
                      imagen = '<img src="images/noticias/'+data.lanoticia.imagen_noticia+'" alt="'+data.lanoticia.titulo_noticia+'" title="'+data.lanoticia.titulo_noticia+'">';
                      contenido = '<br>'+data.lanoticia.texto_noticia;
                      //console.log(contenido);
                      $('#not-principal-titulo').html(data.lanoticia.titulo_noticia);
                      $('#not-principal-resumen').html(data.lanoticia.resumen_noticia);
                      $('#not-principal-fecha').html(data.lanoticia.fecha);
                      $('#not-principal-imagen').html(imagen);
                      $('#not-principal-contenido').html(contenido);

                      for (var i = 0; i < data.noticias.length; i++) {
                          noticias += '<div class="row not-detalle-noticia"><span id="not-detalle-titulo" class="col-sm-12 h3 negrita manito" onclick="cargarUnaNoticia('+data.noticias[i]["id_noticia"]+',\''+data.noticias[i]["origen"]+'\');"><span>'+data.noticias[i]["titulo_noticia"]+'</span></span><span id="not-detalle-fecha" class="col-sm-7">'+data.noticias[i]["fecha"]+'</span><span id="not-detalle-ver" class="col-sm-5 texto-derecha" ><a class="manito" onclick="cargarUnaNoticia('+data.noticias[i]["id_noticia"]+',\''+data.noticias[i]["origen"]+'\');">ver más</a></span><span id="not-detalle-contenido" class="col-sm-12"';
                          noticias += ' style="background-image: url(\'images/noticias/'+data.noticias[i]["imagen_noticia"]+'\')" alt="'+data.noticias[i]["titulo_noticia"]+'" title="'+data.noticias[i]["titulo_noticia"]+'" onclick="cargarUnaNoticia('+data.noticias[i]["id_noticia"]+',\''+data.noticias[i]["origen"]+'\');"';
                          noticias += '>';
                          //noticias += '<img src="images/noticias/'+data.noticias[i]["imagen_noticia"]+'" alt="'+data.noticias[i]["titulo_noticia"]+'" title="'+data.noticias[i]["titulo_noticia"]+'" onclick="cargarUnaNoticia('+data.noticias[i]["id_noticia"]+');" class="manito">';
                          noticias += '</span></div>';                            
                      };

                      $('#not-detalle').html(noticias);
                      $("#dvLoading").hide();
                    },
                    error: function(error){
                      console.log(error);
                      $("#dvLoading").hide();
                    }
              });
            }

            $(function() {
 
                $("#frmLogin").formValidation({
                    fields: {
                        rut: {
                            validators: {
                                notEmpty: {
                                    message: 'El rut es obligatorio'
                                },
                                callback: {
                                    callback: function(value, validator, $field) {
                                        if (value === '') {
                                            return true;
                                        }
                                        var rut = value.substring(0, value.length - 1);
                                        rut=rut.replace(/\./g, "").replace("-", "");
                                        var dv = value[value.length - 1];
                                        var valido = esRutValido(rut, dv);
                                        if (!valido) {
                                            return {
                                                valid: false,
                                                message: 'Rut no coincide con dígito verificador'
                                            }
                                        }
                                        return true;
                                    }
                                }
                            }
                        },
                        password: {
                            validators: {
                                notEmpty: {
                                    message: 'La contraseña es obligatoria'
                                }
                            }
                        }
                    }
                }).on("success.form.fv", function(evt) {

                	$("#frmLogin button[type='submit']").prop("disabled", false);
                    evt.preventDefault(evt);
                    var $form = $(evt.target);
                    console.log("esto: ", $form.serialize());
                    $.ajax({
                        url: "sesion/doLogin",
                        headers: {        
                          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data: $form.serialize(),
                        dataType: "json",
                        type: "post",
                        success: function(data) {
                          console.log("dataaaa: ",data);
                            if (data.error) {
                                growl.danger(data.error);
                            }
                            else location.href = data.href;
                        },
                        error: function(error) {
                            console.log(error);
                        }
                    });
                })
                
                /* Da efecto de scroll lento */
                $('.scroll-link').click(function (event){
                  var idLink = $(this).attr('href').replace('#', '');
                  event.stopPropagation();
                  var Position = jQuery('[id="'+idLink+'"]').offset().top;
                  jQuery('html, body').animate({ scrollTop: Position }, 500);
                  return false;
                });


                $("#formCorreoContacto").formValidation({
                  excluded: ':disabled',
                  framework: 'bootstrap',
                  fields: {                    
                    nombreContacto: {
                      validators:{
                        notEmpty: {
                          message: 'El nombre es obligatorio'
                        }
                      }
                    },                    
                    correoContacto: {
                      validators:{
                        notEmpty: {
                          message: 'El correo es obligatorio'
                        },
                        emailAddress: {
                          message: "El formato del correo es inválido"
                        }
                      }
                    },
                    comentarioContacto: {
                      validators:{
                        notEmpty: {
                          message: 'El comentario es obligatorio'
                        }
                      }
                    }
                  }
                }).on('err.field.fv', function(e, data) {
                  if (data.fv.getSubmitButton()) data.fv.disableSubmitButtons(false);
                }).on('success.field.fv', function(e, data) {
                  if (data.fv.getSubmitButton()) data.fv.disableSubmitButtons(false);
                  console.log(data.field);
                }).on("success.form.fv", function(evt){
                  $("#formCorreoContacto input[type='submit']").prop("disabled", false);
                  evt.preventDefault(evt);
                  $("#dvLoading").show();
                  var $form = $(evt.target);
                  $.ajax({
                    url: $form.prop("action"),
                    data: $form.serialize(),
                    type: "post",
                    dataType: "json",
                    success: function(data){
                      console.log(data);
                      if(data.exito) bootbox.alert("<h4>"+data.exito+"</h4>", function(){ 
                        location.reload();
                      });
                      if(data.error) bootbox.alert("<h4>"+data.error+"</h4>");
                      $("#dvLoading").hide();
                    },
                    error: function(error){
                      console.log(error);
                      $("#dvLoading").hide();
                    }
                  });
                });
                

                $( ".scroll-link" ).click(function() {
                  console.log("click scroll-link");
                  $( "#link-activo" ).removeClass( "link-activo" );                 

                });

                $( ".cambiar-seccion" ).click(function() {
                  console.log("click cambiar-seccion");
                  var value = $(this).attr("seccion");
                  $("#"+value).show();
                  console.log(value);
                  $( ".contenido" ).each(function() {
                      var id = $( this ).attr("id");
                      console.log(id);
                      if( value != id ) $( this ).hide();
                  });

                });

                cargarTodasNoticias();


          

            });
        </script>
    </head>
    <body>

        <!--Header_section-->
        <header id="header_wrapper">
          <div class="container">
            <!--<div class="header_box">-->
              <div class="logo"><a href="#"><img src="images/eHomeseniorsLogoGRND_titulo.png" alt="logo" style="width: 205px;"></a></div>
              <nav class="navbar navbar-inverse" role="navigation">
                <!--<div id="main-nav" class="collapse navbar-collapse navStyle">-->
                <div id="main-nav" class="navStyle">
                  <ul class="nav navbar-nav" id="mainNav">
                    <li class="liA" ><a id="link-activo" href="#hero_section" class="scroll-link link-activo cambiar-seccion" seccion="seccion-inicio">Inicio</a></li>
                    <li class="liB" ><a href="#hero_section" class="scroll-link cambiar-seccion" seccion="seccion-qee">Qué es eHomeseniors</a></li>
                    <li class="liC" ><a href="#hero_section" class="scroll-link cambiar-seccion" seccion="seccion-qs">Quienes somos</a></li>
                    <li class="liD" ><a href="#hero_section" class="scroll-link cambiar-seccion" seccion="seccion-descargas">Descargas</a></li>
                    <li class="liE" ><a href="#hero_section" class="scroll-link cambiar-seccion" seccion="seccion-contacto">Contacto</a></li>
                  </ul>
                </div>
              </nav>
            <!--</div>-->
          </div>
        </header>
        <!--Header_section--> 

        <!--Hero_Section-->
        <section id="hero_section">
          <div class="hero_wrapper">
            <div class="container">
              <div class="hero_section">
                <div class="row " >
                  <div class="col-sm-3 login">                      
                    <form id="frmLogin" autocomplete='off'>
                      <h4 style="margin-top: 0px; margin-bottom: 15px; font-weight: bold;">Iniciar sesión</h4>

                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <div class="form-group">
                        <label for="rut" class="sr-only">Rut</label>
                        <input type="text" id="rut" class="form-control" placeholder="Rut" required autofocus name="rut" autocomplete="off" onkeypress="return textoRut(event);">
                      </div>
                      <div class="form-group">
                        <label for="inputPassword" class="sr-only">Password</label>
                        <input type="password" id="inputPassword" class="form-control" placeholder="Contraseña" required name="password" autocomplete="off">
                      </div>
                      <div class="checkbox col-sm-7 padL0" style="padding-right: 0px; margin-top: 0px;">
                        <input type="checkbox" name="remember-me" id="check-redondo" style="color:black;">
                        <label for="check-redondo" class="recordarcontrasena"><span></span> Recordar contraseña</label> 
                      </div>
                      <div class="col-sm-5 padR0 texto-derecha btnIngresar" >
                        <button class="btn btn-primary-verde" type="submit">Ingresar</button>
                      </div>                           
                    </form>    
                  </div>      
                   
<!--  seccion inicio -->                             
                  <div class="col-sm-9 contenido" id="seccion-inicio">                     
                    <div class="loginComplemento">
                        <!-- <h1 >eHomeseniors</h1>
                        <h2 style="margin-top: 15px;" > Hogar inteligente para mejorar la calidad de vida de adultos mayores</h2>-->
                        <!-- info viviendas sociales inteligentes-->
                        <div class="col-sm-12" >
                          <span id="" class="col-sm-12 h1b">Nuevo proyecto en San Antonio: Piloto de viviendas sociales inteligentes</span>
                          <span id="" class="col-sm-12 h3">La Municipalidad de San Antonio en conjunto con la Universidad de Valparaíso desarrollarán el Proyecto EHomeseniors “Piloto de viviendas sociales inteligentes para adultos mayores de la Comuna de San Antonio”.</span>
                          <!--<span id="not-principal-fecha" class="col-sm-12 h6">18 de Mayo de 2018</span>-->
                          <span id="" class="col-sm-12 h3"><img src="images/noticias/5aff319f14d0eehome2.jpg" alt="Nuevo proyecto en San Antonio: Piloto de viviendas sociales inteligentes" title="Nuevo proyecto en San Antonio: Piloto de viviendas sociales inteligentes"></span>
                          <span id="" class="col-sm-12"><br><b>EHomeseniors</b>, es un sistema de monitoreo no invasivo de adultos mayores en su casa, con el objetivo de acompañarlos y cuidarlos en su vida cotidiana. Este sistema permitirá monitorear y alertar tempranamente a su entorno social y clínico en caso de accidente o de la detección de alguna situación a riesgo, permitiendo al equipo médico entregar diagnósticos más certeros en base a los datos y evidencia recolectados
                          <br>
                          Más específicamente el sistema se compone de cuatro partes principales:
                          <br>
                          1.  Sistema de detección de signos clínicos anormales
                          <br>
                          2.  Detección de caídas
                          <br>
                          3.  Monitoreo de variables ambientales (calidad del aire, humedad, temperatura)
                          <br>
                          4.  Botón de alerta
                          <br><br>

                          <b>Detección de caídas</b>
                          <br><br>
                          Diversas  investigaciones han demostrado que las lesiones inducidas por caída se encuentran entre las causas más comunes de limitaciones de la actividad y discapacidad entre adultos mayores 

                          Mediante el uso de sensores no invasivos, este sistema es capaz de detectar cuando una persona sufre una caída dentro de su hogar. El sistema utiliza sensores de radiación infrarroja, cubriendo un área de 4x4 metros sin vulnerar la privacidad del usuario.
                          <br><br>
                          <b>Sistema de detección de signos clínicos anormales</b>
                          <br><br>
                          Las conductas repetitivas podrían reflejar la presencia de signos psicológicos y conductuales de la Demencia. Este sistema detecta patrones anormales de comportamiento en los adultos mayores que puedan indicar presencia de enfermedades neurodegenerativas como Alzheimer. Además se detecta nicturia, esto es un aumento en la producción de orina, la que puede estar relacionada con enfermedades como: diabetes mellitus, insuficiencia cardiaca, mal nutrición, síndrome nefrótico, entre otros.
                          Los sistemas funcionan de manera no invasiva respetando la necesidad de los usuarios
                          <br><br>
                          <b>Botón de alerta</b>
                          <br><br>
                          Este sistema fue diseñado para solicitar ayuda de forma remota, al presionar el botón SOS una alerta es enviada al entorno social y clínico del usuario junto a la ubicación desde donde fue presionado el botón. Este dispositivo a su vez, permite llamar, en caso de necesidad, a 3 contactos predefinidos. Es un sistema no invasivo, fácil de usar que no vulnera la privacidad del usuario.
                          <br><br>
                          <b>Monitoreo de variables ambientales (calidad del aire, humedad, Tº)</b>
                          <br><br>
                          Este sistema está diseñado para el monitoreo de gases nocivos para la salud, específicamente monóxido de carbono(CO), junto con el sensado de variables ambientales (temperatura y humedad) en los hogares de adultos mayores. Está diseñado para ser no intrusivo, no invasivo y no vulnera la privacidad del paciente. Tendrá un tamaño no mayor a 10X5X5cm y será ubicado en lugares estratégicos de las habitaciones que puedan presentar fugas de gas y/o peligros de incendio
                          <br><br>
                          </span>
                        </div>
                      <!-- info viviendas sociales inteligentes-->

                    </div>
                  </div>  
<!--  /seccion inicio -->   

<!--  seccion que es e -->   
                  <div class="col-sm-9 contenido centrado" id="seccion-qee" style="display:none;">  
                    <div class="loginComplemento"> 
                      <h2 class="titulo-seccion">Qué es eHomeseniors</h2>
                      <div >                                        
                        <p class="texto-justificado contenidoEquipo">
                        <iframe  width='560' height='315'   src="https://www.youtube.com/embed/zbZLL6H9eGE" frameborder="0" allowfullscreen style="text-align: center; border: 1px solid rgb(76,215,200); padding: 5px; margin: 0px; width: 100%; "></iframe>
                        <br><br>
                        El proyecto consiste en desarrollar un sistema integrado para ambientes inteligentes con central de monitoreo, a través de una arquitectura tecnológica especializada y distribuida pero a pequeña escala, enfocada en generar un piloto de habitación para personas de la tercera edad,estableciendo las condiciones para propiciar una mayor autonomía en sus estilos de vida y la de sus respectivas redes de apoyo directas (familiares principalmente), otorgandoasistencia de manera de disminuir el estado de vigilia permanente, generando un ecosistema de apoyo consciente del estadode los usuarios en tiempo real, y analizando las variables capturadas para alertar tempranamente sobre posibles condiciones o estados que estén desarrollando los usuarios del sistema, permitiendo además al equipo médico a entregar un diagnóstico más certero en base a los datos y evidencia recolectados.El modelo se basa en los sistemas de control y monitoreo tradicionales utilizados en diversas industrias, adaptado a éste requerimiento en específico. 
                        <br><br>
                        La construcción del piloto del sistema proyectado se basará en componentes de hardware y software en principio estándar (ya sea con productos existentes en el mercado, o a través del desarrollo de nuevos dispositivos basados en arquitecturas abiertas), de manera de propiciar menores costos de mantenimiento futuro. Creemos que el proyecto debe abordarse con este principio básico, ya que no es el objetivo del mismo el desarrollo y patentamiento de estructuras específicas, sino el estudio y la investigación de alto nivel de sistemas que permitan la asistencia remota y autonomía de la población de la tercera edad. Además, la arquitectura estándar permitirá la escalabilidad futura de los sistemas implementados, de manera de ofrecer siempre costos decrecientes a medida que la plataforma vaya creciendo y aumentando en complejidad, tanto en usuarios, ambientes, como en variables a monitorear y controlar.
                        <br><br>
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/OQWCoFU-iDk" frameborder="0" allowfullscreen style="text-align: center; border: 1px solid rgb(76,215,200); padding: 5px; margin: 0px; width: 100%; "></iframe>
                        </p>                     
                      </div>   
                    </div> 
                  </div> 
<!--  /seccion que es e --> 

<!--  seccion quienes somos -->   
                  <div class="col-sm-9 contenido" id="seccion-qs" style="display:none;"> 
                    <div class="loginComplemento">    
                      <h2 class="titulo-seccion" style="padding-bottom: 0px;">Equipo</h2>

                      <div class=" margenQuienesSomos">                           
                          <div class="centrado margenQSImagen pad0"><img src="images/carlat.png" class="author-avatar" alt="Carla Taramasco"></div>
                          <div class=" margenQSContenido">                              
                            <p class="nombreEquipo">Dra. Carla Taramasco Toro</p>
                            <p class="texto-justificado contenidoEquipo">
                              Directora de Proyecto. Académica e investigadora Escuela Ingeneria Civil Informática Universidad de Valparaíso. PhD Sistemas Complejos (École polytechnique, París) Master en Ciencias Cognitivas (École Normale Superieure, París). Ingeniera Informática (Universidad de Valparaíso).
                            </p>                              
                            <div class="member-social-link"><a href="http://www.carlataramasco.cl" target="_BLANK" title="Visitar web personal" class="twitter-btn"><span class="glyphicon glyphicon-paperclip" aria-hidden="true"></span></a></div>
                          </div>
                      </div> 

                      <div class=" margenQuienesSomos">                           
                          <div class=" centrado margenQSImagen pad0"><img src="images/gabriel.png" class="author-avatar" alt="Carla Taramasco"></div>
                          <div class=" margenQSContenido">                              
                            <p class="nombreEquipo">Gabriel Astudillo</p>
                            <p class="texto-justificado contenidoEquipo">
                              Investigador Asociado. Académico Escuela de ingeniería Civil Informática Universidad de Valparaíso. Magister en Ciencias de  la ingeniería Electrónica (UTFSM). Ingeniero Civil Electrónico (UTFSM).
                            </p>                              
                            <div class="member-social-link"><a href="http://www.decom-uv.cl/~gabriel/" target="_BLANK" title="Visitar web personal" class="twitter-btn"><span class="glyphicon glyphicon-paperclip" aria-hidden="true"></span></a></div>
                          </div>
                      </div>   

                      <div class=" margenQuienesSomos">                           
                          <div class=" centrado margenQSImagen pad0"><img src="images/ing01-leo.jpg" class="author-avatar" alt="Leonardo Zambra"></div>
                          <div class=" margenQSContenido">                              
                            <p class="nombreEquipo">Leonardo Zambra</p>
                            <p class="texto-justificado contenidoEquipo">
                              Desarrollador. Ingeniero de desarrollo en proyecto de Fondo de Innovación para la Competitividad adjudicado por la Universidad de Valparaíso. Ingeniero Civil en Informática (U. de Atacama).
                            </p>                              
                            <div class="member-social-link"><a href="https://cl.linkedin.com/pub/leonardo-zambra-alcayaga/b5/376/597" target="_BLANK" title="Visitar web personal" class="twitter-btn"><span class="glyphicon glyphicon-paperclip" aria-hidden="true"></span></a></div>
                          </div>
                      </div>  

                      <!--<div class=" margenQuienesSomos">                           
                          <div class=" centrado margenQSImagen pad0"><img src="images/gonzalo.jpg" class="author-avatar" alt="Gonzalo Rodríguez"></div>
                          <div class=" margenQSContenido">                              
                            <p class="nombreEquipo">Gonzalo Rodríguez</p>
                            <p class="texto-justificado contenidoEquipo">
                              Desarrollador. Ingeniero de desarrollo en proyecto de Fondo de Innovación para la Competitividad adjudicado por la Universidad de Valparaíso. Ingeniero en Software (UVM).
                            </p>                              
                          </div>
                      </div>-->

                      <div class=" margenQuienesSomos">                           
                          <div class=" centrado margenQSImagen pad0"><img src="images/tomas.jpg" class="author-avatar" alt="Tomas Rodenas Espinoza"></div>
                          <div class=" margenQSContenido">                              
                            <p class="nombreEquipo">Tomas Rodenas Espinoza</p>
                            <p class="texto-justificado contenidoEquipo">
                              Desarrollador. Ingeniero de desarrollo en proyecto de Fondo de Innovación para la Competitividad adjudicado por la Universidad de Valparaíso. Licenciado en ciencias de la Ingeniería Electrónica (UTFSM).
                            </p>                              
                          </div>
                      </div>
                      
                      <div class=" margenQuienesSomos">                           
                          <div class=" centrado margenQSImagen pad0"><img src="images/ing02-erne.JPG" class="author-avatar" alt="Ernesto Fredes"></div>
                          <div class=" margenQSContenido">                              
                            <p class="nombreEquipo">Ernesto Fredes</p>
                            <p class="texto-justificado contenidoEquipo">
                              Desarrollador. Ingeniero de desarrollo en proyecto de Fondo de Innovación para la Competitividad adjudicado por la Universidad de Valparaíso. Magister en ciencias de la ingeniería electrónica en UTFSM. Ingeniero Civil Electrónico (UTFSM).
                            </p>                              
                            <!--<div class="member-social-link"><a href="https://cl.linkedin.com/pub/leonardo-zambra-alcayaga/b5/376/597" target="_BLANK" title="Visitar web personal" class="twitter-btn"><span class="glyphicon glyphicon-paperclip" aria-hidden="true"></span></a></div>-->
                          </div>
                      </div>
                      
                      <div class=" margenQuienesSomos">                           
                          <div class=" centrado margenQSImagen pad0"><img src="images/juanito.jpg" class="author-avatar" alt="Juanito Alvarez"></div>
                          <div class=" margenQSContenido">                              
                            <p class="nombreEquipo">Juanito Alvarez</p>
                            <p class="texto-justificado contenidoEquipo">
                              Desarrollador. Técnico Electrónico en proyecto de Fondo de Innovación para la Competitividad adjudicado por la Universidad de Valparaíso. Técnico en Electrónica (UTFSM).
                            </p>                              
                            <!--<div class="member-social-link"><a href="" target="_BLANK" title="Visitar web personal" class="twitter-btn"><span class="glyphicon glyphicon-paperclip" aria-hidden="true"></span></a></div>-->
                          </div>
                      </div>
                      
                      <div class=" margenQuienesSomos">                           
                          <div class=" centrado margenQSImagen pad0"><img src="images/pedro.png" class="author-avatar" alt="Pedro González"></div>
                          <div class=" margenQSContenido">                              
                            <p class="nombreEquipo">Pedro González</p>
                            <p class="texto-justificado contenidoEquipo">
                              Desarrollador. Ingeniero de desarrollo en proyecto de Fondo de Innovación para la Competitividad adjudicado por la Universidad de Valparaíso. Ingeniero Electrónico (PUCV).
                            </p>                              
                            <!--<div class="member-social-link"><a href="" target="_BLANK" title="Visitar web personal" class="twitter-btn"><span class="glyphicon glyphicon-paperclip" aria-hidden="true"></span></a></div>-->
                          </div>
                      </div> 
<br>
                      <h2 class="titulo-seccion" style="padding-bottom: 0px;">Instituciones asociadas</h2>

                      <div class=" margenQuienesSomos">                           
                          <div class="centrado margenQSImagen pad0"><img src="images/ssvq.jpg" class="author-avatar" alt="Servicio de Salud Viña del Mar Quillota" style="border-radius: 0% !important;"></div>
                          <div class=" margenQSContenido">                              
                            <p class="nombreEquipo">Servicio de Salud Viña del Mar Quillota</p>
                            <p class="texto-justificado contenidoEquipo">
                              El SSVQ aporta con espacios e infraestructura, pertenecientes al Hospital Geriátrico "Paz de la tarde" de Limache. Es de gran interés para el SSVQ la modernización del hospital para mejorar la calidad de vida de sus pacientes, a través del monitoreo, registro y análisis de sus patrones de conductas con el propósito de minimizar la dependencia, lo que se prevé tendrá un positivo impacto en la detección temprana de eventos que afectan principalmente la salud y calidad de vida de los adultos mayores.
                            </p>                              
                            <div class="member-social-link"><a href="http://ssviqui.redsalud.gob.cl/" target="_BLANK" title="Visitar web personal" class="twitter-btn"><span class="glyphicon glyphicon-paperclip" aria-hidden="true"></span></a></div>
                          </div>
                      </div>

                      <div class=" margenQuienesSomos">                           
                          <div class="centrado margenQSImagen pad0"><img src="images/logo_hospital.png" class="author-avatar" alt="Hospital Geriátrico Paz de la tarde" style="border-radius: 0% !important;"></div>
                          <div class=" margenQSContenido">                              
                            <p class="nombreEquipo">Hospital Geriátrico "Paz de la tarde"</p>
                            <p class="texto-justificado contenidoEquipo">
                              El piloto de hogares inteligentes, se está desarrollando en los hogares de algunos pacientes del Hospital Geriátrico la Paz de la Tarde de Limache, bajo la supervisión y el apoyo de profesionales del Hospital. 
                            </p>                              
                            <div class="member-social-link"><a href="http://hospitalpazdelatarde.cl/" target="_BLANK" title="Visitar web personal" class="twitter-btn"><span class="glyphicon glyphicon-paperclip" aria-hidden="true"></span></a></div>
                          </div>
                      </div>

                      <div class=" margenQuienesSomos">                           
                          <div class="centrado margenQSImagen pad0"><img src="images/logo-agim.png" class="author-avatar" alt="Laboratoire AGIM CNRS – Universite Joseph Fourier Grenoble"></div>
                          <div class=" margenQSContenido">                              
                            <p class="nombreEquipo">Laboratoire AGIM CNRS – Universite Joseph Fourier Grenoble</p>
                            <p class="texto-justificado contenidoEquipo">
                              La participación de LABORATOIRE AGIM CNRS - UNIVERSITE Joseph Fourier Grenoble, tiene relación con sus 20 años de experiencia en el desarrollo de tecnología e investigación en el área de ambientes inteligentes en Europa, por tanto se contará con la colaboración científico tecnológica, en el monitoreo de sistemas inteligentes implementados por el AGIM; en Francia.
                            </p>                              
                            <div class="member-social-link"><a href="http://www.agim.eu/" target="_BLANK" title="Visitar web personal" class="twitter-btn"><span class="glyphicon glyphicon-paperclip" aria-hidden="true"></span></a></div>
                          </div>
                      </div>
                                    
                    </div>
                  </div> 
<!--  /seccion quienes somos --> 

<!--  seccion descargas -->   
                  <div class="col-sm-9 contenido" id="seccion-descargas" style="display:none;">                     
                    <!--Service-->
                    <section  id="descargas" >
                        <h2 class="titulo-seccion">Descargas</h2>
                       
                    </section>
                    <div>
                        <h5>Manual de Usuario de Paciente</h5>
                        <a href="{{URL::to('docs/4')}}">Descargar</a>
                        <h5>Manual de Usuario de Familiar</h5>
                        <a href="{{URL::to('docs/1')}}">Descargar</a>
                        <h5>Manual de Usuario de Médico</h5>
                        <a href="{{URL::to('docs/2')}}">Descargar</a>
                        <h5>Manual de Usuario de Administrador</h5>
                        <a href="{{URL::to('docs/3')}}">Descargar</a>

                    </div>
                    <!--Service-->
                  </div> 
<!--  /seccion descargas --> 

<!--  seccion contacto -->   
                  <div class="col-sm-9 contenido" id="seccion-contacto" style="display:none;">                                       
                    <div class="loginComplemento" >
                      <h2 class="titulo-seccion">Contáctanos</h2>  
                      <div id="sub-div-seccion-contacto">
                        <div class="seccion-contacto-a">                        
                          {{ Form::open(array('url' => 'sesion/enviarCorreoContacto', 'method' => 'post', 'class' => 'form-horizontal', 'role' => 'form', 'id' => 'formCorreoContacto')) }}
                    
                          <div  style="min-height: 100px;" >
                            
                            <div class="error marg20b form-group">
                              <input id="nombreContacto" name="nombreContacto" class="form-control" type="text" placeholder="Nombre *"/>
                            </div>
                            <div class="error marg20b form-group">
                              <input id="correoContacto" name="correoContacto" class="form-control" type="text" placeholder="Correo electrónico*"/>
                            </div>
                            <div class="error marg20b form-group">
                              <input id="asuntoContacto" name="asuntoContacto" class="form-control" type="text" placeholder="Asunto"/>
                            </div>
                            <div class="error marg20b form-group">
                              <textarea id="comentarioContacto" name="comentarioContacto" class="form-control" placeholder="Mensaje *" rows="2"></textarea>
                            </div>
                                     
                            <button type="submit" class="btn btn-primary-gris fDerecha">Enviar</button>
                            <button type="reset" class="btn btn-primary-morado fIzquierda">Cancelar</button>
                          </div>
                          {{ Form::close() }}
                        </div>

                        <div class="seccion-contacto-b">
                          Proyecto FIC-GORE Valparaíso<br>
                          Hogar inteligente para mejorar la calidad de vida de adultos mayores<br>
                          Código: 30350830-0<br><br>
                          eHomeseniors<br><br>
                          Escuela de Ingeniería Civil Informática<br>
                          Facultad de Ingeniería<br>
                          Universidad de Valparaíso<br><br>
                          Avda. Gran Bretaña #1111<br>
                          Playa Ancha, Valparaíso<br><br>
                          (56) 032 2507000<br><br>
                          soporte.raveno@uv.cl
                        </div> 
                      </div>
                          
                    </div>
                  </div> 
<!--  /seccion contacto --> 

                  <div id="noticias-div" class="col-sm-12" style="">
                    <h2 class="negrita">Noticias</h2>
                    <div id="div-oculto" class="row" style="display:none;">
                      <div id="not-principal" class="col-sm-8">
                        <span id="not-principal-titulo" class="col-sm-12 h1b"></span>
                        <span id="not-principal-resumen" class="col-sm-12 h3"></span>
                        <span id="not-principal-fecha" class="col-sm-12 h6"></span>
                        <span id="not-principal-imagen" class="col-sm-12 h3"></span>
                        <span id="not-principal-contenido" class="col-sm-12"></span>
                      </div>
                      
                      <div id="not-detalle" class="col-sm-4"></div>
                      
                    </div>
                    <span id="volver-not" onclick="$('#div-oculto').hide(); $('#div-no-oculto').show(); $('#volver-not').hide(); $('#div-no-oculto').focus();" style="display:none;">volver a noticias</span>


                    <div id="div-no-oculto" class="row" ></div>                  

                  </div>

                  

                </div>
              </div>
            </div>
          </div>
        </section>
        <!--Hero_Section--> 

        



        

        
















        <!--Footer-->
        <footer class="footer_wrapper">          
          <div class="container" style="display: inline-flex;">
            <div class="footer_bottom" style = "width: 30%;">
              <a href="#" style="margin: 10px;"><img src="images/logo_hospital.png" alt="logo" height="100px"></a>
            </div>
            <div class="footer_bottom" style = "width: 40%;">
              <a href="#" style="margin: 10px;"><img src="images/Logo_face_down_icon.png" alt="logo" height="25px"></a>
              <a href="https://twitter.com/ehomeseniors" style="margin: 10px;" target="_BLANC"><img src="images/Twitter_logo_white.png" alt="logo" height="25px"></a>
              <h4 style="margin-top: 50px;">www.ehomeseniors.cl</h4>
            </div>
            <div class="footer_bottom" style = "width: 30%;">
              <a href="#" style="margin: 10px;"><img src="images/logo_uv_sinfindo.png" alt="logo" height="70px"></a>
            </div>
          </div>
        </footer>

    </body>
</html>