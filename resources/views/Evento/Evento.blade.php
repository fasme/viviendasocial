@extends("Template/Template")

@section("titulo")
Configuración de Alertas
@stop

@section("script")

<script>
var obtenerDatosEventos = function(){
	$.ajax({
		url: "obtenerEventos",
		data: {rut: 16469206},
		type: "post",
		dataType: "json",
		success: function(data){
			$('#nic-inicio').val(data.inicio);
			$('#nic-fin').val(data.fin);
			$('#puertas').val(data.puertas);
			$('#nic-repeticiones').val(data.nicturias);
			$('#intervalo').val(data.intervalo);
		}
	});
}

var verDatosEventos = function(){
	obtenerDatosEventos();
	$('#formEditarEventoPuertas , #formEditarEventoNic').find('input[type=number], input[type=text]').attr('disabled','disabled'); 
}

$(function(){	 
	verDatosEventos();

	$(".hora").timepicker({ 'step': 30, 'timeFormat': 'H:i' });
	$("#btn-editar-nic").on("click", function(){
		$('#formEditarEventoNic').find('input[type=number], input[type=text]').removeAttr('disabled'); 
		$('#btn-editar-nic').hide();
		$('#btn-guardar-nic').show();
	});

	$("#formEditarEventoNic").submit(function(){
      	var form=$(this);
      	bootbox.confirm("<h4>¿Seguro que quiere modificar la configuración?</h4>", function(result) {
	        if (result) {
	          	$.ajax({
		            url: form.prop("action"),
		            data: form.serialize(),
		            type: form.prop("method"),
		            dataType: "json",
		            success: function(data){
		              	if(data.error) bootbox.alert("<h4>"+data.error+"</h4>");
		              	if(data.exito) bootbox.alert("<h4>"+data.exito+"</h4>", function(){
		                	verDatosEventos();
		                	$('#btn-editar-nic').show();
							$('#btn-guardar-nic').hide();
		              	});
		            }, 
		            error: function(error){
		              console.log(error);
		            }
	          	});
	        }
      	});
      	return false;
    });

    $("#btn-editar-puertas").on("click", function(){
		$('#formEditarEventoPuertas').find('input[type=number], input[type=text]').removeAttr('disabled'); 
		$('#btn-editar-puertas').hide();
		$('#btn-guardar-puertas').show();
	});

	$("#formEditarEventoPuertas").submit(function(){
      	var form=$(this);
      	bootbox.confirm("<h4>¿Seguro que quiere modificar la configuración?</h4>", function(result) {
	        if (result) {
	          	$.ajax({
		            url: form.prop("action"),
		            data: form.serialize(),
		            type: form.prop("method"),
		            dataType: "json",
		            success: function(data){
		              	if(data.error) bootbox.alert("<h4>"+data.error+"</h4>");
		              	if(data.exito) bootbox.alert("<h4>"+data.exito+"</h4>", function(){
		                	verDatosEventos();
		                	$('#btn-editar-puertas').show();
							$('#btn-guardar-puertas').hide();
		              	});
		            }, 
		            error: function(error){
		              console.log(error);
		            }
	          	});
	        }
      	});
      	return false;
    });
});

</script>
<style type="text/css">
.ui-timepicker-wrapper {width: 8em;}
</style>
@stop

@section("section")

<fieldset>
	<legend>Nicturia</legend>
	<div style="text-align: left;">
	{{ Form::open(array('url' => 'evento/editarEvento', 'method' => 'post', 'class' => '', 'role' => 'form', 'id' => 'formEditarEventoNic')) }}
		<div class="form-group error">
			<label for="nic-inicio" class="col-sm-2 control-label">Inicio: </label>
			<div class="col-sm-3">
				<input id="nic-inicio" name="nic-inicio" class="form-control hora" type="text" />
			</div>
		
			<label for="nic-fin" class="col-sm-2 control-label col-sm-offset-2" >Fin: </label>
			<div class="col-sm-3">
				<input id="nic-fin" name="nic-fin" class="form-control hora" type="text" />
			</div>
		</div>
		<br><br>
		<div class="form-group error" style="margin-top: 20px;">
			<label for="nic-repeticiones" class="col-sm-2 control-label">Número de repeticiones: </label>
			<div class="col-sm-3">
				<input id="nic-repeticiones" name="nic-repeticiones" class="form-control" type="number" min="0"/>
			</div>
		</div>

		<br><br><br>
		
		<button id="btn-editar-nic" type="button" class="btn btn-primary pull-right">Editar</button>
		<button id="btn-guardar-nic" type="submit" class="btn btn-primary pull-right" style="display:none;">Guardar</button>	
	{{ Form::close() }}
	</div>
</fieldset>


<fieldset>
	<legend>Acciones repetidas</legend>
	<div style="text-align: left;">
	{{ Form::open(array('url' => 'evento/editarEvento', 'method' => 'post', 'class' => '', 'role' => 'form', 'id' => 'formEditarEventoPuertas')) }}
		<div class="form-group error">
			<label for="intervalo" class="col-sm-2 control-label">Intervalo (minutos): </label>
			<div class="col-sm-3">
				<input id="intervalo" name="intervalo" class="form-control" type="number" min="0"/>
			</div>
		
			<label for="puertas-fin" class="col-sm-2 control-label col-sm-offset-2" >Número de repeticiones: </label>
			<div class="col-sm-3">
				<input id="puertas" name="puertas" class="form-control" type="number" min="0"/>
			</div>
		</div>

		<br><br><br>
		
		<button id="btn-editar-puertas" type="button" class="btn btn-primary pull-right">Editar</button>
		<button id="btn-guardar-puertas" type="submit" class="btn btn-primary pull-right" style="display:none;">Guardar</button>	
	{{ Form::close() }}
</fieldset>

@stop
