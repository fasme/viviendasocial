@extends("Template/Template")

@section("titulo")
Inicio
@stop

@section("script")

<script>

var obtenerUltimosEventos = function(){
	$.ajax({
		url: "index/ultimosEventosPacientes",
		data: {rut: "{{$rut}}"},
		type: "post",
		dataType: "json",
		success: function(data){
			$("#table-ultimos-eventos").empty();
			for(var i = 0; i < data.length; i++){
				var colorTexto = (data[i].revisado) ? "texto-gris" : "texto-rojo";
				var fondo = (data[i].evento == eventos.CAIDA) ? "caida" : (data[i].evento == eventos.NICTURIA) ? "nicturia" : "puertas";
				var tr = "<tr>";
				var url = "{{ URL::to('/') }}";
				var img = "<img src='"+url+"/images/"+data[i].icono+"'  class='img-resumen'/>";
				var texto = "<h3>"+data[i].nombre+"</h3>";
				texto += "<h4>"+data[i].ubicacion+" ["+data[i].identificadorSensor+"]</h4>";
				texto += "<p class='"+colorTexto+" sombra-blanca'>"+data[i].fecha+"</p>";
				var td="<td class='"+fondo+" td-resumen'>"+img+"</td>";
				td+="<td class='"+fondo+"'>"+texto+"</td>";
				tr+=td+"</tr>";
				$("#table-ultimos-eventos").append(tr);
			}
		},
		error: function(error){
			console.log(error);
		}
	});
};

var obtenerUltimosEventosDePaciente = function(){	
	$.ajax({
		url: "index/ultimosEventosPacientesDePaciente",
		data: {rut: "{{$rut}}"},
		headers: {        
		    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		type: "post",
		dataType: "json",
		success: function(data){
			$("#table-ultimos-eventos").empty();
			for(var i = 0; i < data.length; i++){
				var colorTexto = (data[i].revisado) ? "texto-gris" : "texto-rojo";
				var fondo = (data[i].evento == eventos.CAIDA) ? "caida" : (data[i].evento == eventos.NICTURIA) ? "nicturia" : "puertas";
				var tr = "<tr>";
				var url = "{{ URL::to('/') }}";
				var img = "<img src='"+url+"/images/"+data[i].icono+"'  class='img-resumen'/>";
				var texto = "<h3>"+data[i].nombre+"</h3>";
				texto += "<h4>"+data[i].ubicacion+" ["+data[i].identificadorSensor+"]</h4>";
				texto += "<p class='"+colorTexto+" sombra-blanca'>"+data[i].fecha+"</p>";
				var td="<td class='"+fondo+" td-resumen'>"+img+"</td>";
				td+="<td class='"+fondo+"'>"+texto+"</td>";
				tr+=td+"</tr>";
				$("#table-ultimos-eventos").append(tr);
			}
		},
		error: function(error){
			console.log(error);
		}
	});
};

$(function(){		
	@if(App\Models\Usuario::obtenerTipoUsuario(Auth::user()->rut) == 1 || App\Models\Usuario::obtenerTipoUsuario(Auth::user()->rut)==2)
		obtenerUltimosEventos();
	@elseif(App\Models\Usuario::obtenerTipoUsuario(Auth::user()->rut) == NULL)
		obtenerUltimosEventosDePaciente();
	@endif	
});

</script>

@stop

@section("css")

<style type="text/css">

.td-resumen {
	text-align: center;
	width: 2%;
}

.img-resumen {
	margin-top: 34px;
}
.icono-simbologia{
	width: 50px;
	text-align: center;
}
.titulo-simbologia{
	font-size: medium;
}
#simbologia-ultimos-eventos{
	padding: 2%;
    border: 2px solid white;
    border-radius: 4px;
    background: white;
}

table#tabla-simbologia-ultimos-eventos {
    border-collapse: collapse;
    width: 100%;
}

table#tabla-simbologia-ultimos-eventos th{
    border-bottom: 1px solid #4CD7C8;
    background: #9398D8;
    color: white;
    padding: 10px;
}

table#tabla-simbologia-ultimos-eventos td{
    border-bottom: 1px solid gray;
	padding: 3px 3px 3px 3px;
}
</style>

@stop

@section("section")

@if(App\Models\Usuario::obtenerTipoUsuario(Auth::user()->rut) != 3
&& App\Models\Usuario::obtenerTipoUsuario(Auth::user()->rut) != 5
	&& App\Models\Usuario::obtenerTipoUsuario(Auth::user()->rut) != 6)
<fieldset>
	<legend>Últimos eventos</legend>
	<table id="table-ultimos-eventos" class="table table-hover table-condensed table-striped">
	</table>
</fieldset>
<div id="simbologia-ultimos-eventos">
	<label class="titulo-simbologia">Simbología</label>
	<table id="tabla-simbologia-ultimos-eventos">
		<tr class="titulo-simbologia">
			<th>Ícono</th>
			<th colspan=2>Descripción</th>
		</tr>
		<tr>
			<td style="width: 10%;"><img src="images/img-simb-rojo.png" class="icono-simbologia"></td>
			<td style="width: 20%;">Fondo color rojo</td>
			<td style="width: 70%;">Indica que es un evento de caídas</td>
		</tr>
		<tr>
			<td style="width: 10%;"><img src="images/img-simb-verde.png" class="icono-simbologia"></td>
			<td style="width: 20%;">Fondo color verde</td>
			<td style="width: 70%;">Indica que es un evento de acciones repetidas</td>
		</tr>
		<tr>
			<td style="width: 10%;"><img src="images/img-simb-celeste.png" class="icono-simbologia"></td>
			<td style="width: 20%;">Fondo color celeste</td>
			<td style="width: 70%;">Indica que es un evento de nicturia</td>
		</tr>
		<tr>
			<td style="width: 10%;"><img src="images/img-simb-visto.png" class="icono-simbologia"></td>
			<td style="width: 20%;">Ícono visto bueno</td>
			<td style="width: 70%;">Indica que el evento ha sido visto por el usuario</td>
		</tr>
		<tr>
			<td style="width: 10%;"><img src="images/img-simb-exclamacion.png" class="icono-simbologia"></td>
			<td style="width: 20%;">Ícono exclamación</td>
			<td style="width: 70%;">Indica que el evento no ha sido visto por el usuario</td>
		</tr>
	</table>
</div>
<br>
@else 
{{ HTML::image('images/eHomeseniorsLogoGRND.png', null, ['class' => 'img-responsive center-block']) }}
@endif

@stop
