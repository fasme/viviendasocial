@extends("Template/Template")

@section("titulo")
Usuarios
@stop

@section("script")

<script type="text/javascript">
var tableUsuarios=null;

var obtenerUsuarios = function(perfil){
	$.ajax({
		url: "obtenerUsuarios",
		data: {perfil: ""+perfil+""},
		type: "post",
		dataType: "json",
		success: function(data){
			tableUsuarios.clear().draw();
			tableUsuarios.rows.add(data).draw();
		}
	});
}

var obtenerDatosUsuario = function(rut){
	$('#alertaSms').attr('checked', false);
	$('#alertaPush').attr('checked', false);
	$('#alertaCorreo').attr('checked', false);

	$.ajax({
		url: "obtenerDatosUsuario",
		data: {rut: rut},
		type: "post",
		dataType: "json",
		success: function(data){
			$('#myModalLabelAgregarUsuario').text("Editar usuario");
			$("#accion").val(0);
			$('#formAgregarUsuario').trigger("reset");
			$('#nombre').val(data.nombre);
			$('#apellido_p').val(data.paterno);
			$('#apellido_m').val(data.materno);
			$('#rut').val(data.rut);
			$('#dv').val(data.dv);
			$('#telefono').val(data.telefono);
			$('#correo').val(data.correo);
			$('#genero').val(data.genero);
			$('#fechanacimiento').val(data.fecha);	
			$('#comuna').val(data.id_comuna);
			$('#calle').val(data.calle);
			$('#dpto').val(data.departamento);	
			$('#numero').val(data.numero);	
			$('#selectPerfilCorto').val(data.id_tipo_usuario);
			$('#alertaPush').val(data.alerta_push);
			$('#alertaSms').val(data.alerta_sms);
			$('#alertaCorreo').val(data.alerta_correo);

			if (data.alerta_push){
				$('#alertaPush').attr('checked', true);
			}
			if (data.alerta_sms){
				$('#alertaSms').attr('checked', true);
			}
			if (data.alerta_correo){
				$('#alertaCorreo').attr('checked', true);
			}

			$('.passdiv').hide();
			$('.pass').prop("disabled", true);
			$("#modalAgregarUsuario").modal("show");
		}
	});
}



var crearUsuario = function(){
	$('#alertaSms').attr('checked', false);
	$('#alertaPush').attr('checked', false);
	$('#alertaCorreo').attr('checked', false);

	$('#myModalLabelAgregarUsuario').text("Agregar usuario");
	$('.passdiv').show();
	$('.pass').prop( "disabled", false );
	$('#formAgregarUsuario').trigger("reset");
	$("#modalAgregarUsuario").modal("show");
}

var agregarPaciente = function(rut){
	$('#formAgregarContacto').trigger("reset");
	cargarListaPacientes(rut, "listaPacientes");
	$('#rutUsuario_hdn').val(rut);
	$('#modalAgregarContacto').modal("show");
}

var listaPacientes = new Bloodhound({
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('rut'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
    	url: '%QUERY/obtenerNombreRutPacientes',
    	filter: function(response) {
    		return response;
        }
    },
    limit: 10
});
listaPacientes.initialize();


var cargarListaPacientes = function(rutUsuario, idSelect){
	$.ajax({
    url: "obtenerPacientesDeContacto",
    data: { rutUsuario: rutUsuario },
    type: "post",
    async: false, 
    dataType: "json",
    success: function(data){
      	$("#"+idSelect).empty();
      	if (data.length>0){
      	for (var i = 0; i < data.length; i++) {        
	        $("#listaPacientes").append('<option value="'+data[i][2]+'" selected="selected"><strong>'+data[i][2]+"-"+data[i][3]+"</strong> - "+data[i][0]+'</option>');
	      };
	    }
            
    }, error : function(error){ console.log(error); }
  });
}

var existeEncargado = function(rutUsuario){
	var existe = false;
	$.ajax({
	    url: "existeEncargado",
	    data: { rutUsuario: rutUsuario },
	    type: "post",
	    async: false, 
	    dataType: "json",
	    success: function(data){
	      	if(data.resultado) bootbox.alert("<h4>"+data.msj+"</h4>", function(){ 
				$('#rut').val("");
				$('#dv').val("");
			});
	            
	    }, error : function(error){ console.log(error); }
    });
}

$(function(){
	tableUsuarios = $('#usuarios').DataTable({	
		"iDisplayLength": 10,
		"bJQueryUI": true,
		"searching": true,
		"ordering": false,
		"info": true,
		"bAutoWidth" : false,
		"responsive": true,
		"oLanguage": {
			"sUrl": "{{URL::to('/')}}/js/spanish.json"
		},
		"columns": [
		    null,
		    null,
		    null,
		    null,
		    null,
		    null,
		    { "visible": false }
		]
	});	

	obtenerUsuarios("0");

	$("#buscar-rut").typeahead(null,{
		source: listaPacientes.ttAdapter(),
		display: 'rut',
		templates: {
		    empty: [
		      '<div class="empty-message">',
		        'No hay resultados',
		      '</div>'
		    ].join('\n'),
		    suggestion: function(data) {
		    	$('#agregarAListaDePacientes_btn').prop("disabled", true);
    			return '<p><strong>' + data.rut + '</strong> – ' + data.nombre + '</p>';
			}
	  	} 
	}).on('typeahead:selected', function(event, selection) {
		var rutdv = selection.rut;
		rutdv = rutdv.split("-");
		$('#rut_hdn').val(rutdv[0]);
		$('#dv_hdn').val(rutdv[1]);
		$('#nombre_hdn').val(selection.nombre);
		$('#agregarAListaDePacientes_btn').prop("disabled", false);
	});

	$("#selectPerfil").on("change", function(){
		obtenerUsuarios($(this).val());
	});

	$('#modalAgregarUsuario').on('hidden.bs.modal', function (e) {
		$("#accion").val(1);
		$("#formAgregarUsuario").data('formValidation').resetForm();
	});

	$("#formAgregarUsuario").formValidation({
		excluded: ':disabled',
		framework: 'bootstrap',
		fields: {
			rut: {
				validators:{
					notEmpty: {
						message: 'El rut es obligatorio'
					},
					callback: {
						callback: function(value, validator, $field){
							$("#dv").val('');
							return true;
						}
					}
				}
			},
			dv: {
				validators:{
					callback: {
						callback: function(value, validator, $field){
							var field_rut = $("#rut");
							var dv = $("#dv");
							if(field_rut.val() == '' && dv.val() == '') {
								return true;
							}
							if(field_rut.val() != '' && dv.val() == ''){
								return {valid: false, message: "Debe ingresar el dígito verificador"};
							}
							if(field_rut.val() == '' && dv.val() != ''){
								return {valid: false, message: "Debe ingresar el rut"};
							}
							var rut = $.trim(field_rut.val());
							var esValido=esRutValido(field_rut.val(), dv.val());
							if(!esValido){
								return {valid: false, message: "Dígito verificador no coincide con el rut"};
							}
							return true;
						}
					},
					remote: {
						url: "existeUsuario",
						data: function(validator, $field, value) {
                            return {
                                rut: validator.getFieldElements('rut').val()
                            };
                        },
						type: "post"
					}
				}
			},
			nombre: {
				validators:{
					notEmpty: {
						message: 'El nombre es obligatorio'
					}
				}
			},
			apellido_p: {
				validators:{
					notEmpty: {
						message: 'El apellido paterno es obligatorio'
					}
				}
			},
			/*apellido_m: {
				validators:{
					notEmpty: {
						message: 'El apellido materno es obligatorio'
					}
				}
			},*/
			password: {
				validators:{
					notEmpty: {
						message: 'La contraseña es obligatoria'
					},
					identical: {
						field: 'confirmPassword',
						message: 'Las contraseñas deben coincidir'
					}
				}
			},
			confirmPassword: {
				validators: {
					identical: {
						field: 'password',
						message: 'Las contraseñas deben coincidir'
					}
				}
			},
			correo: {
				validators:{
					/*notEmpty: {
						message: 'El correo es obligatorio'
					},*/
					emailAddress: {
						message: "El formato del correo es inválido"
					},
					callback:{
						callback: function(value, validator, $field){	
							var field_correo = $("#correo");
							var alertaCorreo =	$('#alertaCorreo').is(':checked');		
							if( alertaCorreo == true && field_correo.val()=='' ) 
								return	 {valid: false, message: "Debe ingresar correo electrónico"};
							return true;
						}
					}
				}
			},
			telefono: {
				validators:{
					/*notEmpty: {
						message: 'El teléfono es obligatorio'
					},*/
					integer: {
						message: "Debe ingresar solo números"
					},
					callback:{
						callback: function(value, validator, $field){	
							var field_telefono = $("#telefono");
							var alertaSMS =	$('#alertaSms').is(':checked');		
							if( alertaSMS == true && field_telefono.val()=='' ) 
								return {valid: false, message: "Debe ingresar Número de teléfono"};
							return true;
						}
					}
				}
			},
			selectPerfilCorto: {
				validators:{
					notEmpty:{
						message: 'Debe seleccionar un tipo de usuario'
					},
					callback:{
						callback: function(value, validator, $field){				
							if(value=="0") return {valid: false, message: "Debe seleccionar un tipo de usuario"};
							return true;
						}
					}
				}
			}
		}
	}).on('click', function() {
        $('#formAgregarUsuario')
            .formValidation('revalidateField', 'correo')
            .formValidation('revalidateField', 'telefono');
    }).on('err.field.fv', function(e, data) {
		if (data.fv.getSubmitButton()) data.fv.disableSubmitButtons(false);
	}).on('success.field.fv', function(e, data) {
		if (data.fv.getSubmitButton()) data.fv.disableSubmitButtons(false);
	}).on("success.form.fv", function(evt){
		$("#formRegistrarUsuario input[type='submit']").prop("disabled", false);
		evt.preventDefault(evt);
		$("#dvLoading").show();
		var $form = $(evt.target);
		$.ajax({
			url: $form.prop("action"),
			data: $form.serialize() + "&rut=" + $("#rut").val(),
			type: "post",
			dataType: "json",
			success: function(data){
				if(data.exito) bootbox.alert("<h4>"+data.exito+"</h4>", function(){ 
					$("#modalAgregarUsuario").modal("hide");
					location.reload();
				});
				if(data.error) bootbox.alert("<h4>"+data.error+"</h4>");
				$("#dvLoading").hide();
			},
			error: function(error){
				console.log(error);
				$("#dvLoading").hide();
			}
		});
	});


	// Elimina todos los option de la lista 
	$("#limpiarListaPacientes").click(function(event){
		$("#listaPacientes").empty();
	});

	// Elimina uno o más option seleccionados de la lista 
	$("#eliminarPacientes").click(function(event){
		$("#listaPacientes").each(function() {
			$(this).find('option:selected').remove();
		});
	});

	// Agrega un paciente a la lista de pacientes
	$("#agregarAListaDePacientes_btn").click(function(event){
		$('#agregarAListaDePacientes_btn').prop("disabled", true);
		var rut = $('#rut_hdn').val();
		var dv = $('#dv_hdn').val();
		var nombre = $('#nombre_hdn').val();
		if($('#buscar-rut').val() == rut+"-"+dv && rut!=""){		
			if($('#listaPacientes option[value="'+rut+'"]').length == 0){
				$("#listaPacientes").append('<option value="'+rut+'" selected="selected"><strong>'+rut+"-"+dv+"</strong> - "+nombre+'</option>');
			}
		}else{
			bootbox.alert("<h4>Seleccione un paciente de la lista de sugerencias</h4>");
		}
		$('#buscar-rut').val("");
	});

	$("#formAgregarContacto").submit(function(){
      	var form=$(this);
      	bootbox.confirm("<h4>¿Seguro que quiere modificar la lista de pacientes?</h4>", function(result) {
	        if (result) {
	        	$('#modalAgregarContacto').modal("hide");
	          	$("#listaPacientes").each(function(){
	            	$('#listaPacientes option').prop('selected',true);
	          	});

	          	$.ajax({
		            url: form.prop("action"),
		            data: form.serialize(),
		            type: form.prop("method"),
		            dataType: "json",
		            success: function(data){
		              	if(data.error) bootbox.alert("<h4>"+data.error+"</h4>");
		              	if(data.exito) bootbox.alert("<h4>"+data.exito+"</h4>", function(){
		                	location.reload();
		              	});
		            }, 
		            error: function(error){
		              console.log(error);
		            }
	          	});
	        }
      	});
      	return false;
    });

});


</script>

@stop

@section("section")

<fieldset>
	<legend>Lista de usuarios</legend>
	{{ Form::select('selectPerfil', ['0'=>'Todos','1'=>'Familiar','2'=>'Médico','3'=>'Administrador'], null, ['class' => 'form-control', 'id' => 'selectPerfil']) }}
	<br>
	<table id="usuarios" class="table table-bordered table-striped">
		<thead>
			<tr>
				<th>Nombre</th>
				<th>Apellido</th>
				<th>Rut</th>
				<th>Teléfono</th>
				<th>Correo</th>
				<th>Tipo de usuario</th>
				<th>Acciones</th>
				<th style="display:none;"></th>
			</tr>
		</thead>
		<tbody></tbody>
		<tfoot>
			<tr>
				<td colspan="7">
					<a  class="btn btn-primary" data-toggle="modal" onclick="crearUsuario();" title="Agregar nuevo usuario"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Usuario</a>
				</td>
			</tr>
		</tfoot>
	</table>

</fieldset>


<div class="modal fade" id="modalAgregarUsuario" tabindex="-1" role="dialog" aria-labelledby="myModalLabelAgregarUsuario" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabelAgregarUsuario"></h4>
			</div>
			{{ Form::open(array('url' => 'usuario/agregarUsuario', 'method' => 'post', 'class' => 'form-horizontal', 'role' => 'form', 'id' => 'formAgregarUsuario')) }}
			<input id="accion" type="hidden" name="accion" value="1" />
			<div class="modal-body" style="min-height: 100px;">
				<div class="form-group error">
					<label for="rut" class="col-sm-2 control-label">Rut (*): </label>
					<div class="col-sm-10">
						<div class="input-group">
							{{Form::text('rut', null, array('id' => 'rut', 'class' => 'form-control pass', 'autofocus'))}}
							<span class="input-group-addon"> - </span>
							{{ Form::select('dv', $selectRut, null, array('class' => 'form-control pass', 'id' => 'dv')) }}
						</div>
					</div>
				</div>
				<div class="form-group error">
					<label for="nombre" class="col-sm-2 control-label">Nombre (*): </label>
					<div class="col-sm-10">
						<input id="nombre" name="nombre" class="form-control" type="text" onkeypress="return textoEspacioGuion(event);"/>
					</div>
				</div>
				<div class="form-group error">
					<label for="apellido_p" class="col-sm-2 control-label">Apellido paterno(*): </label>
					<div class="col-sm-10">
						<input id="apellido_p" name="apellido_p" class="form-control" type="text" onkeypress="return textoEspacioGuion(event);"/>
					</div>
				</div>
				<div class="form-group error">
					<label for="apellido_m" class="col-sm-2 control-label">Apellido materno(*): </label>
					<div class="col-sm-10">
						<input id="apellido_m" name="apellido_m" class="form-control" type="text" onkeypress="return textoEspacioGuion(event);"/>
					</div>
				</div>
				<div class="form-group error">
					<label for="telefono" class="col-sm-2 control-label">Télefono (*): </label>
					<div class="col-sm-10">
						<input id="telefono" name="telefono" class="form-control" type="text" maxlength="10" onkeypress="return soloNumeros(event);"/>
					</div>
				</div>
				<div class="form-group error">
					<label for="correo" class="col-sm-2 control-label">Correo (*): </label>
					<div class="col-sm-10">
						<input id="correo" name="correo" class="form-control" type="text" />
					</div>
				</div>
				<div class="form-group error passdiv">
					<label for="password" class="col-sm-2 control-label">Contraseña (*): </label>
					<div class="col-sm-10">
						<input id="password" name="password" class="form-control pass" type="password" />
					</div>
				</div>
				<div class="form-group error passdiv">
					<label for="confirmPassword" class="col-sm-2 control-label">Confirmar contraseña: </label>
					<div class="col-sm-10">
						<input id="confirmPassword" name="confirmPassword" class="form-control pass" type="password" />
					</div>
				</div>
				<div class="form-group error">
					<label for="selectPerfilCorto" class="col-sm-2 control-label">Tipo de usuario (*): </label>
					<div class="col-sm-10">
						{{ Form::select('selectPerfilCorto', $selectTipoUsuario, null, ['class' => 'form-control', 'id' => 'selectPerfilCorto']) }}
						 						
					</div>
				</div>

				<div class="form-group error">
					<label  class="col-sm-2 control-label">Tipo de Alerta: </label>
					
					<label for="alertaPush" class="col-sm-3 control-label">Mensaje Push: 
						<input id="alertaPush" name="alertaPush" type="checkbox" class="col-md-offset-1"/>
					</label>

					<label for="alertaSms" class="col-sm-2 control-label">SMS:
						<input id="alertaSms" name="alertaSms" type="checkbox" class="col-md-offset-1"/>
					</label>

					<label for="alertaCorreo" class="col-sm-4 control-label">Correo electrónico: 
						<input id="alertaCorreo" name="alertaCorreo" type="checkbox" class="col-md-offset-1"/>
					</label>
				</div>

				<div class="form-group error">
					<label for="comuna" class="col-sm-2 control-label">Comuna: </label>
					<div class="col-sm-10">
						{{ Form::select('comuna', $selectComunas, null, array('class' => 'form-control', 'id' => 'comuna')) }}	
					</div>
				</div>
				<div class="form-group error">
					<label for="calle" class="col-sm-2 control-label">Calle: </label>
					<div class="col-sm-10">
						<input id="calle" name="calle" class="form-control" type="text" />
					</div>
				</div>
				<div class="form-group error">
					<label for="dpto" class="col-sm-2 control-label">Departamento: </label>
					<div class="col-sm-10">
						<input id="dpto" name="dpto" class="form-control" type="text" />
					</div>
				</div>
				<div class="form-group error">
					<label for="numero" class="col-sm-2 control-label">Número: </label>
					<div class="col-sm-10">
						<input id="numero" name="numero" class="form-control" type="text" onkeypress="return numerosSinPunto(event);"/>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary">Aceptar</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
			</div>
			{{ Form::close() }}
		</div>
	</div>
</div>



<div class="modal fade" id="modalAgregarContacto" tabindex="-1" role="dialog" aria-labelledby="myModalLabelAgregarContacto" aria-hidden="true">
	<div class="modal-dialog" style="width: 60%;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabelAgregarContacto">Agregar pacientes</h4>
			</div>
			{{ Form::open(array('url' => 'usuario/asociarPacienteAUsuario', 'method' => 'post', 'class' => 'form-horizontal', 'role' => 'form', 'id' => 'formAgregarContacto')) }}			
			<div class="modal-body" style="min-height: 100px;">

				<div class="form-group error">
					<label for="rut" class="col-sm-3 control-label">Buscar por RUT o Nombre de paciente: </label>
					<div class="col-sm-9">
						<div class="input-group">
							{{Form::text('rut', null, array('id' => 'buscar-rut', 'class' => 'form-control', 'autofocus'))}}
							<span class="input-group-btn">
								<button type="button" class="btn btn-primary" title="Agregar a lista de pacientes" id="agregarAListaDePacientes_btn" style="top: -2px;"><span class="glyphicon glyphicon-plus"></span> A lista</button>
							</span> 
							{{ Form::hidden('rut_hdn', 'null', array('id' => 'rut_hdn')) }}
							{{ Form::hidden('dv_hdn', 'null', array('id' => 'dv_hdn')) }}
							{{ Form::hidden('nombre_hdn', 'null', array('id' => 'nombre_hdn')) }}
							{{ Form::hidden('rutUsuario_hdn', 'null', array('id' => 'rutUsuario_hdn')) }}
						</div>
					</div>
				</div> 

				<div class="form-group error">
					<label for="rut" class="col-sm-3 control-label">Lista de pacientes: </label>
					<div class="col-sm-9 ">
						<div class="">
							<select id="listaPacientes" multiple="multiple" name="listaPacientes[]" class="nav nav-list form-control" display="none" style="margin-top: 2px; width: 100%;"></select>
							<button type="button" id="limpiarListaPacientes" class="btn btn-danger" style="margin-top: 3px;" title="Eliminar todos los pacientes de la lista"><span class="glyphicon glyphicon-remove-circle"></span> Limpiar lista</button>&nbsp;
							<button type="button" id="eliminarPacientes" class="btn btn-danger" style="margin-top: 3px;" title="Eliminar el paciente seleccionado"><span class="glyphicon glyphicon-remove-circle"></span> Eliminar</button> 
						</div>
					</div>
				</div> 	

			</div>
			<div class="modal-footer">	
				<button type="submit" class="btn btn-primary">Guardar lista de pacientes</button>		
				<button type="button" class="btn btn-danger" data-dismiss="modal">Salir</button>
			</div>
			{{ Form::close() }}
		</div>
	</div>
</div>
@stop
