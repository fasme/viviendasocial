@extends("Template/Template")

@section("titulo")
Contactos
@stop

@section("script")

<script>
var tableContactos=null;

var obtenerContactoPaciente = function(){
	$.ajax({
		url: "obtenerContactoPaciente",
		data: {rut: "{{$rut}}", "idEvento": $("#selectEvento").val()},
		type: "post",
		dataType: "json",
		success: function(data){
			tableContactos.clear().draw();
			tableContactos.rows.add(data).draw();
		}
	});
}

$(function(){
	$("#menuPaciente").collapse();

	tableContactos = $('#contactos').DataTable({	
		"iDisplayLength": 10,
		"bJQueryUI": true,
		"searching": true,
		"ordering": false,
		"info": true,
		"bAutoWidth" : false,
		"responsive": true,
		"oLanguage": {
			"sUrl": "{{URL::to('/')}}/js/spanish.json"
		}
	});

	$("#selectEvento").on("change", function(){
		obtenerContactoPaciente();
		$("#idEvento").val($(this).val());
	});

	$("#selectEvento").trigger("change");

	$('#modalAgregarContacto').on('hidden.bs.modal', function (e) {
		$("#formAgregarContacto").data('formValidation').resetForm();
	});

	$("#formAgregarContacto").formValidation({
		excluded: ':disabled',
		framework: 'bootstrap',
		fields: {
			rut: {
				validators:{
					notEmpty: {
						message: 'El rut es obligatorio'
					},
					callback: {
						callback: function(value, validator, $field){
							var field_rut = $field;
							var dv = $("#dv");
							if (!esRutValido(field_rut.val(), dv.val())){
								dv.val("");
							}
							return true;
						}
					}
				}
			},
			dv:{
				validators:{
					notEmpty: {
						message: 'El dígito verificador es obligatorio'
					},
					callback: {
						callback: function(value, validator, $field){
							var field_rut = $("#rut");
							var dv = $field;
							if(field_rut.val() == '' && dv.val() == '') {
								return true;
							}
							if(field_rut.val() != '' && dv.val() == ''){
								return {valid: false, message: "Debe ingresar el dígito verificador"};
							}
							if(field_rut.val() == '' && dv.val() != ''){
								return {valid: false, message: "Debe ingresar el rut"};
							}
							var rut = $.trim(field_rut.val());
							var esValido=esRutValido(field_rut.val(), dv.val());
							if(!esValido){
								return {valid: false, message: "Dígito verificador no coincide con el rut"};
							}
							return true;
						}
					},
				}
			},
			nombre: {
				validators:{
					notEmpty: {
						message: 'El nombre es obligatorio'
					}
				}
			},
			apellido: {
				validators:{
					notEmpty: {
						message: 'El apellido es obligatorio'
					}
				}
			},
			password: {
				validators:{
					notEmpty: {
						message: 'La contraseña es obligatoria'
					},
					identical: {
						field: 'confirmPassword',
						message: 'Las contraseñas deben coincidir'
					}
				}
			},
			confirmPassword: {
				validators: {
					identical: {
						field: 'password',
						message: 'Las contraseñas deben coincidir'
					}
				}
			},
			correo: {
				validators:{
					notEmpty: {
						message: 'El correo es obligatorio'
					},
					emailAddress: {
						message: "El formato del correo es inválido"
					}
				}
			},
			telefono: {
				validators:{
					notEmpty: {
						message: 'El teléfono es obligatorio'
					},
					integer: {
						message: "Debe ingresar solo números"
					}
				}
			},
			"evento[]": {
				validators:{
					notEmpty: {
						message: 'Debe seleccionar un evento'
					}
				}
			}

		}
	}).on('err.field.fv', function(e, data) {
		if (data.fv.getSubmitButton()) data.fv.disableSubmitButtons(false);
	}).on('success.field.fv', function(e, data) {
		if (data.fv.getSubmitButton()) data.fv.disableSubmitButtons(false);
	}).on("success.form.fv", function(evt){
		$("#formRegistrarUsuario input[type='submit']").prop("disabled", false);
		evt.preventDefault(evt);
		$("#dvLoading").show();
		var $form = $(evt.target);
		$.ajax({
			url: $form.prop("action"),
			data: $form.serialize(),
			type: "post",
			dataType: "json",
			success: function(data){
				if(data.exito) bootbox.alert("<h4>"+data.exito+"</h4>", function(){ 
					$("#modalAgregarContacto").modal("hide");
					obtenerContactoPaciente(); 
				});
				if(data.error) bootbox.alert("<h4>"+data.error+"</h4>");
				$("#dvLoading").hide();
			},
			error: function(error){
				console.log(error);
				$("#dvLoading").hide();
			}
		});
	});
});

</script>

@stop

@section("section")

<fieldset>
	<legend>Contactos del pacientes ----</legend>
	{{ Form::select('selectEvento', $selectEvento, null, ['class' => 'form-control', 'id' => 'selectEvento']) }}
	<br><br>
	<table id="contactos" class="table table-bordered table-striped">
		<thead>
			<tr>
				<th>Nombre</th>
				<th>Apellido</th>
				<th>Teléfono</th>
				<th>Correo</th>
				<th>Acciones</th>
			</tr>
		</thead>
		<tbody></tbody>
		<tfoot>
			<tr>
				<td colspan="5">
					<a href="#modalAgregarContacto" class="btn btn-primary" data-toggle="modal" title="Agregar contacto"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Contacto</a>
				</td>
			</tr>
		</tfoot>
	</table>

</fieldset>

<div class="modal fade" id="modalAgregarContacto" tabindex="-1" role="dialog" aria-labelledby="myModalLabelAgregarContacto" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabelAgregarContacto">Agregar contacto</h4>
			</div>
			{{ Form::open(array('url' => 'contactos/agregarContacto', 'method' => 'post', 'class' => 'form-horizontal', 'role' => 'form', 'id' => 'formAgregarContacto')) }}
			{{ Form::hidden('idEvento', '1', array('id' => 'idEvento')) }}
			{{ Form::hidden('rutPaciente', "$rut", array('id' => 'rutPaciente')) }}
			<div class="modal-body" style="min-height: 100px;">
				<div class="form-group error">
					<label for="fecha" class="col-sm-2 control-label">Rut: </label>
					<div class="col-sm-10">
						<div class="input-group">
							{{Form::text('rut', null, array('id' => 'rut', 'class' => 'form-control', 'autofocus'))}}
							<span class="input-group-addon"> - </span>
							{{ Form::select('dv', $selectRut, null, array('class' => 'form-control', 'id' => 'dv')) }}
						</div>
					</div>
				</div>
				<div class="form-group error">
					<label for="fecha" class="col-sm-2 control-label">Nombre: </label>
					<div class="col-sm-10">
						<input id="nombre" name="nombre" class="form-control" type="text" />
					</div>
				</div>
				<div class="form-group error">
					<label for="fecha" class="col-sm-2 control-label">Apellido: </label>
					<div class="col-sm-10">
						<input id="apellido" name="apellido" class="form-control" type="text" />
					</div>
				</div>
				<div class="form-group error">
					<label for="fecha" class="col-sm-2 control-label">Télefono: </label>
					<div class="col-sm-10">
						<input id="telefono" name="telefono" class="form-control" type="text" />
					</div>
				</div>
				<div class="form-group error">
					<label for="fecha" class="col-sm-2 control-label">Correo: </label>
					<div class="col-sm-10">
						<input id="correo" name="correo" class="form-control" type="text" />
					</div>
				</div>
				<div class="form-group error">
					<label for="fecha" class="col-sm-2 control-label">Evento: </label>
					<div class="col-sm-10">
						<label><input name="evento[]" type="checkbox" value="1"/> Caidas</label>
						<label style="margin-left: 10px;"><input name="evento[]" type="checkbox" value="2"/> Apertura de puertas</label>
						<label style="margin-left: 10px;"><input name="evento[]" type="checkbox" value="3"/> Nicturia</label>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary">Aceptar</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
			</div>
			{{ Form::close() }}
		</div>
	</div>
</div>

@stop
