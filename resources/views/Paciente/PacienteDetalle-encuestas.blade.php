

<style type="text/css">
	
	.tg  {border-collapse:collapse;border-spacing:0;}
	.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
	.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
	.tg .tg-us36{border-color:inherit;vertical-align:top}
	.tg .tg-xxzo{background-color:#c0c0c0;border-color:inherit;vertical-align:top}



</style>

<fieldset >
	<legend style="font-size: 16px;">Encuestas</legend>

	<!-- <ul class="nav nav-tabs" id="tab-encuestas">	
				  
		<li class="active"><a id="encuestasIniciales" href="#tab-encuestasIniciales"  data-toggle="tab">Encuestas Iniciales</a></li>	
		<li><a id="encuestasFinales" href="#tab-encuestasFinales"  data-toggle="tab">Encuestas Finales</a></li>	
			    
	</ul> -->

	<div class="tab-content container-fluid"  style="background-color: white;" >
		<div class="row">
			<table class="tg">
			  <tr>
			    <th class="tg-xxzo">Inicio</th>
			    <th class="tg-xxzo">Resultado</th>
			  </tr>
			  <tr>
			    <td class="tg-us36" id="charlson" ><a>Charlson</a> </td>
			    <td class="tg-us36" id="resultadoCharlson"></td>
			  </tr>
			  <tr>
			    <td class="tg-us36" id="barthel"><a>Barthel</a></td>
			    <td class="tg-us36" id="resultadoBarthel"></td>
			  </tr>
			  <tr>
			    <td class="tg-us36" id="lawton"><a>Lawton</a></td>
			    <td class="tg-us36" id="resultadoLawton"></td>
			  </tr>
			  <tr>
			    <td class="tg-us36" id="sinGer"><a>Sindromes geriátricos</a></td>
			    <td class="tg-us36" id="resultadoSinGer"></td>
			  </tr>
			  <tr>
			    <td class="tg-us36" id="mmse"><a>MMSE</a></td>
			    <td class="tg-us36" id="resultadoMmse"></td>
			  </tr>
			</table>
		</div>

		<br>

		<div class="row">
			<table class="tg">
			  <tr>
			    <th class="tg-xxzo">Inicio</th>
			    <th class="tg-xxzo">Resultado</th>
			    <th class="tg-xxzo">A 3 meses</th>
			    <th class="tg-xxzo">Resultado</th>
			  </tr>
			  <tr>
			    <td class="tg-us36" id="whoqol"><a>Whoqol-bref</a> <br></td>
			    <td class="tg-us36" id="resultadoWhoqol"></td>
			    <td class="tg-us36" id="whoqolA3"><a>Whoqol-bref</a><br></td>
			    <td class="tg-us36" id="resultadoWhoqolA3"></td>
			  </tr>
			</table>
		</div>

		<br>

		<div class="row">
			<table class="tg">
			  <tr>
			    <th class="tg-xxzo">Inicio</th>
			    <th class="tg-xxzo">Resultado</th>
			    <th class="tg-xxzo">A 1,5 meses</th>
			    <th class="tg-xxzo">Resultado</th>
			    <th class="tg-xxzo">A 3 meses</th>
			    <th class="tg-xxzo">Resultado</th>
			  </tr>
			  <tr>
			    <td class="tg-us36" id="EQ5D"><a>EQ-5D</a><br></td>
			    <td class="tg-us36" id="resultadoEQ5D"></td>
			    <td class="tg-us36" id="EQ5D-segundo"><a>EQ-5D</a><br></td>
			    <td class="tg-us36" id="resultadoEQ5D-segundo"></td>
			    <td class="tg-us36" id="EQ5D-tercero"><a>EQ-5D</a><br></td>
			    <td class="tg-us36" id="resultadoEQ5D-tercero"></td>
			  </tr>
			</table>
		</div>
	</div>

	

		
	<div class="tab-content container-fluid"  style="background-color: white;" >
		<br>
<!-- Iniciales -->
		<div class="tab-pane fade active in" id="tab-encuestasIniciales" >

			<div class="panel-group" id="accordionEncuestasIniciales"> 

			<!--  Charlson -->			    	
					    <!-- <div class="panel panel-default encuestas-caidas " id="ac-charlson">
					        <a data-toggle="collapse" data-parent="#accordionEncuestasIniciales" href="#acc-test-charlson">
					        	<div class="panel-heading morado-blanco">
					            	<h4 class="panel-title"> Charlson</h4>
					        	</div>
					        </a>
					        <div id="acc-test-charlson" class="panel-collapse collapse">
					            <div class="panel-body ">
					                include('Paciente/PacienteDetalle-encuestas-charlsonInicial')
					            </div>
					        </div>
					    </div> -->

			<!--  Índice de Barthel -->			    	
				    <!-- <div class="panel panel-default encuestas-caidas" id="ac-barthel">
				        <a data-toggle="collapse" data-parent="#accordionEncuestasIniciales" href="#acc-test-barthel">
				        	<div class="panel-heading morado-blanco">
				            	<h4 class="panel-title"> Índice de Barthel </h4>
				        	</div>
				        </a>
				        <div id="acc-test-barthel" class="panel-collapse collapse">
				            <div class="panel-body">
				                include('Paciente/PacienteDetalle-encuestas-indiceBarthelInicial')
				            </div>
				        </div>
				    </div>	 -->

			<!--  Índice de Lawton & Brody -->			    	
				    <!-- <div class="panel panel-default encuestas-caidas" id="ac-lawton">
				        <a data-toggle="collapse" data-parent="#accordionEncuestasIniciales" href="#acc-test-lawton">
				        	<div class="panel-heading morado-blanco">
				            	<h4 class="panel-title"> Índice de Lawton & Brody </h4>
				        	</div>
				        </a>
				        <div id="acc-test-lawton" class="panel-collapse collapse">
				            <div class="panel-body">
				                include('Paciente/PacienteDetalle-encuestas-lawtonBrodyInicial')
				            </div>
				        </div>
				    </div> -->

			<!--  Síndromes Geriátricos -->			    	
				    <!-- <div class="panel panel-default encuestas-caidas" id="ac-geriatrico">
				        <a data-toggle="collapse" data-parent="#accordionEncuestasIniciales" href="#acc-test-sindromes">
				        	<div class="panel-heading morado-blanco">
				            	<h4 class="panel-title"> Síndromes Geriátricos </h4>
				        	</div>
				        </a>
				        <div id="acc-test-sindromes" class="panel-collapse collapse">
				            <div class="panel-body">
				                include('Paciente/PacienteDetalle-encuestas-sindromesGeriatricosInicial')
				            </div>
				        </div>
				    </div> -->
					
			<!-- MMSE -->

				    <!--< div class="panel panel-default encuestas-caidas" id="ac-mmse">
				        <a data-toggle="collapse" data-parent="#accordionEncuestasIniciales" href="#acc-test-mmse">
				        	<div class="panel-heading morado-blanco">
				            	<h4 class="panel-title">MMSE</h4>
				        	</div>
				        </a>
				        <div id="acc-test-mmse" class="panel-collapse collapse">
				            <div class="panel-body">
				                include('Paciente/PacienteDetalle-encuestas-mmse')
				            </div>
				        </div>
				    </div> -->

		<!--  Timed Get Up and Go Test -->
			    <!-- <div class="panel panel-default encuestas-caidas" id="ac-timeGetUp">
			        <a data-toggle="collapse" data-parent="#accordionEncuestasIniciales" href="#acc-test-timeGetUp">
			        	<div class="panel-heading morado-blanco">
			            	<h4 class="panel-title"> Timed Get Up and Go Test </h4>
			        	</div>
			        </a>
			        <div id="acc-test-timeGetUp" class="panel-collapse collapse">
			            <div class="panel-body">
			                include('Paciente/PacienteDetalle-encuestas-timedGetUpInicial')
			            </div>
			        </div>
			    </div> -->
		
					
		
		<!--  Tinetti: Evaluación de la marcha y el equilibrio -->			    	
			   <!--  <div class="panel panel-default encuestas-caidas" id="ac-tinetti">
			        <a data-toggle="collapse" data-parent="#accordionEncuestasIniciales" href="#acc-test-tinetti">
			        	<div class="panel-heading morado-blanco">
			            	<h4 class="panel-title"> Tinetti: Evaluación de la marcha y el equilibrio </h4>
			        	</div>
			        </a>
			        <div id="acc-test-tinetti" class="panel-collapse collapse">
			            <div class="panel-body">
			                include('Paciente/PacienteDetalle-encuestas-tinetiInicial')
			            </div>
			        </div>
			    </div> -->
		
		<!--  ICIQ-SF -->			    	
			   <!--  <div class="panel panel-default encuestas-caidas" id="ac-iciq">
			        <a data-toggle="collapse" data-parent="#accordionEncuestasIniciales" href="#acc-test-ICIQ-SF">
			        	<div class="panel-heading morado-blanco">
			            	<h4 class="panel-title"> ICIQ-SF</h4>
			        	</div>
			        </a>
			        <div id="acc-test-ICIQ-SF" class="panel-collapse collapse">
			            <div class="panel-body">
			                include('Paciente/PacienteDetalle-encuestas-ICIQ-SFInicial')
			            </div>
			        </div>
			    </div> -->

		<!--  Evaluacion Cognitiva -->			    	
			    <!-- <div class="panel panel-default encuestas-caidas" id="ac-cognitiva">
			        <a data-toggle="collapse" data-parent="#accordionEncuestasIniciales" href="#acc-test-cognitivo">
			        	<div class="panel-heading morado-blanco">
			            	<h4 class="panel-title">Evaluación Cognitiva </h4>
			        	</div>
			        </a>
			        <div id="acc-test-cognitivo" class="panel-collapse collapse">
			            <div class="panel-body">
			                include('Paciente/PacienteDetalle-encuestas-evaluacionCognitiva')
			            </div>
			        </div>
			    </div> -->
	 <!-- GDS-->		
	 			<!-- <div class="panel panel-default encuestas-caidas" id="ac-gds">
			        <a data-toggle="collapse" data-parent="#accordionEncuestasIniciales" href="#acc-test-gds">
			        	<div class="panel-heading morado-blanco">
			            	<h4 class="panel-title">GDS </h4>
			        	</div>
			        </a>
			        <div id="acc-test-gds" class="panel-collapse collapse">
			            <div class="panel-body">
			                include('Paciente/PacienteDetalle-encuestas-GDS')
			            </div>
			        </div>
			    </div> -->

			    	

	<!--Conducta Motriz Anomala  -->
				<!-- <div class="panel panel-default encuestas-caidas" id="ac-conductamotriz">
				       <a data-toggle="collapse" data-parent="#accordionEncuestasIniciales" href="#acc-test-conductamotriz">
				       	<div class="panel-heading morado-blanco">
				           	<h4 class="panel-title">Conducta motriz anómala</h4>
				       	</div>
				       </a>
				       <div id="acc-test-conductamotriz" class="panel-collapse collapse">
				           <div class="panel-body">
				               include('Paciente/PacienteDetalle-encuestas-conducta-motriz')
				           </div>
				       </div>
				  </div>	 -->
			    
			</div>

		</div>


<!-- Finales -->
		<div class="tab-pane fade" id="tab-encuestasFinales" >

			<div class="panel-group" id="accordionEncuestasFinales"> 
		<!--  Timed Get Up and Go Test -->
			   <!--  <div class="panel panel-default encuestas-caidas" id="ac-timeGetUpFinal">
			       <a data-toggle="collapse" data-parent="#accordionEncuestasFinales" href="#acc-test-timeGetUpFinal"> 
			       		<div class="panel-heading morado-blanco">
			            	<h4 class="panel-title">Timed Get Up and Go Test</h4>
			        	</div>
			        </a>
			        <div id="acc-test-timeGetUpFinal" class="panel-collapse collapse">
			            <div class="panel-body">
			                include('Paciente/PacienteDetalle-encuestas-timedGetUpFinal')
			            </div>
			        </div>
			    </div> -->
		<!--  Síndromes Geriátricos -->			    	
			   <!--  <div class="panel panel-default encuestas-caidas" id="ac-geriatricoFinal">
			        <a data-toggle="collapse" data-parent="#accordionEncuestasFinales" href="#acc-test-sindromesFinal">
			        	<div class="panel-heading morado-blanco">
			            	<h4 class="panel-title"> Síndromes Geriátricos </h4>
			        	</div>
			        </a>
			        <div id="acc-test-sindromesFinal" class="panel-collapse collapse">
			            <div class="panel-body">
			                include('Paciente/PacienteDetalle-encuestas-sindromesGeriatricosFinal')
			            </div>
			        </div>
			    </div> -->
		
		<!--  Índice de Lawton & Brody -->			    	
			    <!-- <div class="panel panel-default encuestas-caidas" id="ac-lawtonFinal">
			        <a data-toggle="collapse" data-parent="#accordionEncuestasFinales" href="#acc-test-lawtonFinal">
			        	<div class="panel-heading morado-blanco">
			            	<h4 class="panel-title"> Índice de Lawton & Brody </h4>
			        	</div>
			        </a>
			        <div id="acc-test-lawtonFinal" class="panel-collapse collapse">
			            <div class="panel-body">
			                include('Paciente/PacienteDetalle-encuestas-lawtonBrodyFinal')
			            </div>
			        </div>
			    </div> -->
		<!--  Tinetti: Evaluación de la marcha y el equilibrio -->			    	
			    <!-- <div class="panel panel-default encuestas-caidas" id="ac-tinettiFinal">
			        <a data-toggle="collapse" data-parent="#accordionEncuestasFinales" href="#acc-test-tinettiFinal">
			        	<div class="panel-heading morado-blanco">
			            	<h4 class="panel-title"> Tinetti: Evaluación de la marcha y el equilibrio</h4>
			        	</div>
			        </a>
			        <div id="acc-test-tinettiFinal" class="panel-collapse collapse">
			            <div class="panel-body">
			                include('Paciente/PacienteDetalle-encuestas-tinetiFinal')
			            </div>
			        </div>
			    </div> -->
		<!--  Charlson -->			    	
			   <!--  <div class="panel panel-default encuestas-caidas" id="ac-charlsonFinal">
			        <a data-toggle="collapse" data-parent="#accordionEncuestasFinales" href="#acc-test-charlsonFinal">
			        	<div class="panel-heading morado-blanco">
			            	<h4 class="panel-title"> Charlson </h4>
			        	</div>
			        </a>
			        <div id="acc-test-charlsonFinal" class="panel-collapse collapse">
			            <div class="panel-body">
			                include('Paciente/PacienteDetalle-encuestas-charlsonFinal')
			            </div>
			        </div>
			    </div> -->
		<!--  ICIQ-SF -->			    	
			    <!-- <div class="panel panel-default encuestas-caidas" id="ac-iciqFinal">
			        <a data-toggle="collapse" data-parent="#accordionEncuestasFinales" href="#acc-test-ICIQ-SFFinal">
			        	<div class="panel-heading morado-blanco">
			            	<h4 class="panel-title"> ICIQ-SF</h4>
			        	</div>
			        </a>
			        <div id="acc-test-ICIQ-SFFinal" class="panel-collapse collapse">
			            <div class="panel-body">
			                include('Paciente/PacienteDetalle-encuestas-ICIQ-SFFinal')
			            </div>
			        </div>
			    </div>
 -->
		<!--  Evaluacion Cognitiva Final-->			    	
			    <!-- <div class="panel panel-default encuestas-caidas" id="ac-cognitivaFinal">
			        <a data-toggle="collapse" data-parent="#accordionEncuestasIniciales" href="#acc-test-cognitivoFinal">
			        	<div class="panel-heading morado-blanco">
			            	<h4 class="panel-title">Evaluación Cognitiva</h4>
			        	</div>
			        </a>
			        <div id="acc-test-cognitivoFinal" class="panel-collapse collapse">
			            <div class="panel-body">
			                include('Paciente/PacienteDetalle-encuestas-evaluacionCognitivaFinal')
			            </div>
			        </div>
			    </div> -->
	<!-- GDS final-->		
	 			<!-- <div class="panel panel-default encuestas-caidas" id="ac-gdsFinal">
			        <a data-toggle="collapse" data-parent="#accordionEncuestasIniciales" href="#acc-test-gdsFinal">
			        	<div class="panel-heading morado-blanco">
			            	<h4 class="panel-title">GDS </h4>
			        	</div>
			        </a>
			        <div id="acc-test-gdsFinal" class="panel-collapse collapse">
			            <div class="panel-body">
			                include('Paciente/PacienteDetalle-encuestas-GDSFinal')
			            </div>
			        </div>
			    </div> -->

			    	<!-- MMSE final -->
			    <!-- <div class="panel panel-default encuestas-caidas" id="ac-mmseFinal">
			        <a data-toggle="collapse" data-parent="#accordionEncuestasIniciales" href="#acc-test-mmseFinal">
			        	<div class="panel-heading morado-blanco">
			            	<h4 class="panel-title">MMSE</h4>
			        	</div>
			        </a>
			        <div id="acc-test-mmseFinal" class="panel-collapse collapse">
			            <div class="panel-body">
			                include('Paciente/PacienteDetalle-encuestas-mmseFinal')
			            </div>
			        </div>
			    </div>
 -->
			   <!--Conducta Motriz Anomala final  -->
				<!-- <div class="panel panel-default encuestas-caidas" id="ac-conductamotrizFinal">
				       <a data-toggle="collapse" data-parent="#accordionEncuestasIniciales" href="#acc-test-conductamotrizFinal">
				       	<div class="panel-heading morado-blanco">
				           	<h4 class="panel-title">Conducta motriz anómala</h4>
				       	</div>
				       </a>
				       <div id="acc-test-conductamotrizFinal" class="panel-collapse collapse">
				           <div class="panel-body">
				               include('Paciente/PacienteDetalle-encuestas-conducta-motrizFinal')
				           </div>
				       </div>
				  </div> -->


			</div>

		</div>
				
	</div>



	<div id="modalMMSE" class="modal dialog" role="dialog">
	  <div class="modal-dialog">

	    <!-- Modal content-->
	    <div class="modal-content">
	      
	      <div class="modal-header modal-headerNicturia">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">MMSE</h4>
	      </div>

	      <div class="modal-body">
	      	@include('Paciente/PacienteDetalle-encuestas-mmse')
	      </div>
	      	
	      </div>

	      <div class="modal-footer">
	      	<button type="button" class="btn btn-primary" data-dismiss="modal" >Cerrar</button>
	      </div>
	    </div>

	  </div>
	</div>

	<div id="modalLawton" class="modal dialog" role="dialog">
	  <div class="modal-dialog">

	    <!-- Modal content-->
	    <div class="modal-content">
	      
	      <div class="modal-header modal-headerNicturia">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Lawton</h4>
	      </div>

	      <div class="modal-body">
	      	@include('Paciente/PacienteDetalle-encuestas-lawtonBrodyInicial')
	      </div>
	      	
	      </div>

	      <div class="modal-footer">
	      	<button type="button" class="btn btn-primary" data-dismiss="modal" >Cerrar</button>
	      </div>
	    </div>

	  </div>
	</div>

	<div id="modalCharlson" class="modal dialog" role="dialog">
	  <div class="modal-dialog modal-lg">

	    <!-- Modal content-->
	    <div class="modal-content">
	      
	      <div class="modal-header modal-headerNicturia">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Charlson</h4>
	      </div>

	      <div class="modal-body">
	      	@include('Paciente/PacienteDetalle-encuestas-charlsonInicial')
	      </div>
	      	
	      </div>

	      <div class="modal-footer">
	      	<button type="button" class="btn btn-primary" data-dismiss="modal" >Cerrar</button>
	      </div>
	    </div>

	  </div>
	</div>

	<div id="modalWhoqol" class="modal dialog" role="dialog">
	  <div class="modal-dialog modal-lg">

	    <!-- Modal content-->
	    <div class="modal-content">
	      
	      <div class="modal-header modal-headerNicturia">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Whoqol-Bref</h4>
	      </div>

	      <div class="modal-body">
	      	@include('Paciente/PacienteDetalle-encuestas-whoqol')
	      </div>
	      	
	      </div>

	      <div class="modal-footer">
	      	<button type="button" class="btn btn-primary" data-dismiss="modal" >Cerrar</button>
	      </div>
	    </div>

	  </div>
	</div>


	<div id="modalWhoqolA3" class="modal dialog" role="dialog">
	  <div class="modal-dialog modal-lg">

	    <!-- Modal content-->
	    <div class="modal-content">
	      
	      <div class="modal-header modal-headerNicturia">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Whoqol-Bref a 3 meses</h4>
	      </div>

	      <div class="modal-body">
	      	@include('Paciente/PacienteDetalle-encuestas-whoqolA3')
	      </div>
	      	
	      </div>

	      <div class="modal-footer">
	      	<button type="button" class="btn btn-primary" data-dismiss="modal" >Cerrar</button>
	      </div>
	    </div>

	  </div>
	</div>

	<div id="modalBarthel" class="modal dialog" role="dialog">
	  <div class="modal-dialog">

	    <!-- Modal content-->
	    <div class="modal-content">
	      
	      <div class="modal-header modal-headerNicturia">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Barthel</h4>
	      </div>

	      <div class="modal-body">
	      	@include('Paciente/PacienteDetalle-encuestas-indiceBarthelInicial')
	      </div>
	      	
	      </div>

	      <div class="modal-footer">
	      	<button type="button" class="btn btn-primary" data-dismiss="modal" >Cerrar</button>
	      </div>
	    </div>

	  </div>
	</div>
	


	<div id="modalSinGer" class="modal dialog" role="dialog">
	  <div class="modal-dialog">

	    <!-- Modal content-->
	    <div class="modal-content">
	      
	      <div class="modal-header modal-headerNicturia">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Sindromes geriátricos</h4>
	      </div>

	      <div class="modal-body">
	      	@include('Paciente/PacienteDetalle-encuestas-sindromesGeriatricosInicial')
	      </div>
	      	
	      </div>

	      <div class="modal-footer">
	      	<button type="button" class="btn btn-primary" data-dismiss="modal" >Cerrar</button>
	      </div>
	    </div>

	  </div>
	</div>

	<div id="modalEQ5D-primero" class="modal dialog" role="dialog">
	  <div class="modal-dialog">

	    <!-- Modal content-->
	    <div class="modal-content">
	      
	      <div class="modal-header modal-headerNicturia">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">EQ-5D</h4>
	      </div>

	      <div class="modal-body">
	      	@include('Paciente/PacienteDetalle-encuestas-eq5d-primero')
	      </div>
	      	
	      </div>

	      <div class="modal-footer">
	      	<button type="button" class="btn btn-primary" data-dismiss="modal" >Cerrar</button>
	      </div>
	    </div>

	  </div>
	</div>

	

</fieldset>



