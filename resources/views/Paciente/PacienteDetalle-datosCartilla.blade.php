<style>
#tabla_datos th{
	text-align:center;
	vertical-align: middle;
}
</style>

<div id="datos_cartilla">
	<div class="row">
		{{ Form::open(array('url' => 'paciente/detallePaciente/buscarResultadosMicciones', 'method' => 'post', 'role' => 'form', 'id' => 'formularioDatosCartilla')) }}
			<div class="row" style="margin-left:10px;">
				<div class="form-group">
					<div class="col-sm-2">
						<label class="control-label">Intervalo de tiempo</label>
					</div>
					<div class="col-sm-2">
						<input type="text" name="intervalo" class="form-control" onkeypress="return soloNumeros(event);">
					</div>
					<div class="col-sm-2">
						<label class="control-label">minutos</label>
					</div>
					<div class="col-sm-2">
						<button id="buscar" type="submit" class="btn btn-primary">Buscar</button>
					</div>
					<div class="col-sm-2 pull-right">
						<button type="button" class="btn btn-primary" id="guardar_configuracion" disabled>Guardar esta configuración</button>
					</div>
					
				</div>
			</div>
			<br>
			<div class="row" style="margin-left:10px;">
				<div class="col-sm-1">
					<div class="form-group">
						<label class="control-label">Fecha inicial</label>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="form-group">
						<input type="text" name="fecha_inicial" id="fecha_inicial" class="form-control">		
					</div>
				</div>
				<div class="col-sm-1">
					<div class="form-group">
						<label class="control-label">Fecha final</label>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="form-group">
						<input type="text" name="fecha_final" id="fecha_final" class="form-control">		
					</div>
				</div>
			</div>
		{{ Form::close() }}
	</div>
	<br>
	<br>
	<table id="tabla_datos" class="table table-bordered">
		<thead>
			<tr>
				<th rowspan="3">Día</th>
				<th rowspan="3">Fecha hora</th>
				<th>Electrodo&nbsp;<label style="color:green;"><input type="radio" name="elec[]" id="elec1" value="<?php echo ConfiguracionNicturia::obtenerIDConfiguracion(300,6,50);?>" >Seleccionar este electrodo</label></th>
				<th>Ultrasonido</th>
				<th>Electrodo&nbsp;<label style="color:green;"><input type="radio" name="elec[]" id="elec2" value="<?php echo ConfiguracionNicturia::obtenerIDConfiguracion(500,10,30);?>" checked>Seleccionar este electrodo</label></th>
				<th>Ultrasonido</th>
			</tr>
			<tr>
				<th>{{$configuracionNicturia[0]->configuracion_electrodo}} min.</th>
				<th rowspan="2">{{$configuracionNicturia[0]->configuracion_ultrasonido}}</th>
				<th>{{$configuracionNicturia[1]->configuracion_electrodo}} min.</th>
				<th rowspan="2">{{$configuracionNicturia[1]->configuracion_ultrasonido}}</th>
			</tr>
			<tr>
				<th>{{$configuracionNicturia[0]->configuracion_electrodo_umbral}}</th>
				<th>{{$configuracionNicturia[1]->configuracion_electrodo_umbral}}</th>
			</tr>
		</thead>
		<tbody>
			
		</tbody>
		<tfoot>
			<tr>
				<th></th>
				<th>Total: <span id="totalRegistros"></span></th>
				<th>Porcentaje:<span id="porcentaje1">0</span>%</th>
				<th>Porcentaje:<span id="porcentajeu1">0</span>%</th>
				<th>Porcentaje:<span id="porcentaje2">0</span>%</th>
				<th>Porcentaje:<span id="porcentajeu2">0</span>%</th>
			</tr>
		</tfoot>
	</table>
</div>
<script>
var con_datos=false;
var  contadores=[];
var mostrar_info=true;
$(function(){
		
	$("#fecha_inicial,#fecha_final").datetimepicker({
		locale: "es",
		format: "DD-MM-YYYY",
		sideBySide: true,
		stepping:1
	});
	
	$("#fecha_inicial,#fecha_final").datetimepicker().focusout(function()
		{
			mostrar_info=false;
			$("#guardar_configuracion").trigger("click");
			$("#formularioDatosCartilla").trigger("submit");
			mostrar_info=true;
		});

	contadores=[new Contador(1),
			 new Contador(2)];
			 
	$tabla=$("#tabla_datos").DataTable({
		//"iDisplayLength": 100,
		"paging":false,
		"bJQueryUI": true,
		"searching": true,
		"ordering": false,
		"info": true,
		"bAutoWidth" : false,
		"responsive": true,
		"oLanguage": {
			"sUrl": "{{URL::to('/')}}/js/spanish.json"
		},
		"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {

		if ( aData[0] != "" )
		{
			$('td', nRow).css('background-color', '#dddddd');
		}
		if ( aData[0] == ""&& aData[1] != "")
		{
			$('td', nRow).css('background-color', '#eeeeee');
		}
		
		if ( aData[2].startsWith("<span")||(aData[3]&&aData[3].startsWith("<span"))||aData[4].startsWith("<span")||(aData[5]&&aData[5].startsWith("<span")))
		{
			$('td span', nRow).css('font-weight', 'bold');
			
		}
		
	}
	});
	
	$("[name=intervalo]").on("input",function()
		{
			con_datos=false;
			$("#guardar_configuracion").prop("disabled",!($("[name=intervalo]").val()!=""&&con_datos));
			
		});
	
	$("#guardar_configuracion").click(function()
		{
			
			var intervalo=$("[name=intervalo]").val();
			var electrodo=$("[name='elec[]']:checked").val();
			var datos=[];
			for(var i=0;i<contadores.length;i++)
			{
				if(contadores[i].configuracion==electrodo)
				{
					datos=contadores[i].lista;
					break;
				}
			}
			var datosJson=[];
			for(var i=0;i<datos.length;i++)
			{
				datosJson.push(
						{
							id_cartilla_micciones:datos[i][0],
							cantidad:datos[i][1],
							id_electrodo:electrodo,
							minutos:intervalo,
							fecha_inicio:$("[name=fecha_inicial]").val(),
							fecha_final:$("[name=fecha_final]").val()
						}
					);
			}
			if(datos.length==0)
			{
				datosJson.push(
						{
							id_cartilla_micciones:null,
							cantidad:0,
							id_electrodo:electrodo,
							minutos:intervalo,
							fecha_inicio:$("[name=fecha_inicial]").val(),
							fecha_final:$("[name=fecha_final]").val()
						}
					);
			}
			
			$.ajax({
		            url: "guardarConfiguracionMicciones",
		            data: {datos:datosJson,rut:"{{$rut}}"},
		            type: "post",
		            dataType: "json",
		            async:false,
		            success: function(data){
		            	if(mostrar_info)
		            	{
							if(data.exito)
							{
								
								bootbox.alert(data.exito);
							}
							else if(data.error)
							{
								bootbox.alert(data.error);
								console.log(data.msg);
							}
		            	}
		            	console.log(data.msg?data.msg:"");
		            		
		            }, 
		            
		            error: function(error){
		              console.log(error);
		            }
	          	});
		});
	//cargar configuración
	$.ajax({
		url: "cargarConfiguracionMicciones",
		data: {rut:"{{$rut}}"},
		type: "post",
		dataType: "json",
		async:false,
		success: function(data){
			
			if(data.error)
			{
				console.log(data.error);		
				return;
			}
			$("[name=intervalo]").val(data.minutos);
			$("[name='elec[]'][value='"+data.sensor+"']").prop("checked",true);
			if(data.fecha_inicio)
			{
				
		
				$("[name=fecha_inicial]").data("DateTimePicker").defaultDate(darVueltaFechaString(data.fecha_inicio));
				
			}
			if(data.fecha_final)
			{
				
		
				$("[name=fecha_final]").data("DateTimePicker").defaultDate(darVueltaFechaString(data.fecha_final));
			}
		    
			
		}, 
		
		error: function(error){
		  console.log(error);
		}
	});
	$("#buscar").click(function(){
		mostrar_info=false;
		$("#guardar_configuracion").trigger("click");
		mostrar_info=true;
	});
	$("#formularioDatosCartilla").submit(function(evt){
			evt.preventDefault();
			
			var form=$(this);
      	      	var datos = form.serializeArray(); 
				datos.push({name: "rut",value:{{$rut}} });
				var contador=0;
				$tabla.clear();
	          	$.ajax({
		            url: form.prop("action"),
		            data: datos,
		            type: form.prop("method"),
		            dataType: "json",
		            async:false,
		            beforeSend:function(){
		            	$("#dvLoading").show();
		            },
		            success: function(datos){
		            	
		            	
		              	var cartilla=new Cartilla();
		              	var fecha_inicial="";
		              	var fecha_final="";
		              	var fechas=datos.fechas_cartilla;
		              	var data=datos.filtro;
		              	var fecha_anterior="";
		              	if(!fechas[0])
		              		return;
		              	
		              	if($("[name=fecha_inicial]").val()=="")
		              	{
		              		fecha_inicial=fechas[0].fecha_cartilla;
		              		
		               		$("[name=fecha_inicial]").data("DateTimePicker").defaultDate(darVueltaFechaString(fecha_inicial));
		              	}
		              	else
		              	{
		              		fecha_inicial=darVueltaFechaString($("[name=fecha_inicial]").val());
		              	}
		              	if($("[name=fecha_final]").val()=="")
		              	{
		              		fecha_final=fechas[fechas.length-1].fecha_cartilla;
		             
		              		$("[name=fecha_final]").data("DateTimePicker").defaultDate(darVueltaFechaString(fecha_final));
		              		
		              	}
		              	else
		              	{
		              		fecha_final=darVueltaFechaString($("[name=fecha_final]").val());
		              	}
		              	
		              	
						contadores=[new Contador(1),
									 new Contador(2)];
		              	
		              	for(var i=0;i<fechas.length;i++)
		              	{
		              		if(!cartilla.contieneFecha(fechas[i].fecha_cartilla))
		              		{
		              			var fec=new FechaCartilla(fechas[i].fecha_cartilla);
		              			fec.fecha_real=fechas[i].fecha_real;
		              			fec.agregarHora(new Hora(fechas[i].horario));
		              			cartilla.agregarFecha(fec);
		              			
		              		}
		              		else
		              		{
		              			cartilla.agregarHora(fechas[i].fecha_cartilla,new Hora(fechas[i].horario));
		              		}
		              		
		              		for(var j=0;j<data.length;j++)
							{
								
								if(data[j].fecha_cartilla==fechas[i].fecha_cartilla)
									cartilla.asignarSensor(data[j].fecha_cartilla,data[j].fecha_hora_cartilla,data[j]);
									
							}
							
		              		
		              	}
		              	for(var j=0;j<data.length;j++)
						{
							
							contadores[data[j].id_configuracion_nicturia-1].agregarValor(data[j].id_cartilla_micciones);	
						}
		              	
		              	cartilla.ordenarSensores();
		              	
		              	for(var i=0;i<cartilla.fechas.length;i++)
		              	{
		              		$tabla.row.add(
		              			[
		              				i+1,
		              				moment(cartilla.fechas[i].fecha).format("DD-MM-YYYY"),
		              				"",
		              				"",
		              				"",
		              				""
		              			]
		              			);
		              		var horas=cartilla.fechas[i].horas;
		              		for(var j=0;j<horas.length;j++)
		              		{
		              			$tabla.row.add(
		              				[
		              				"",
		              				moment(cartilla.fechas[i].fecha).format("DD-MM-YYYY")+" "+moment(horas[j].hora).format("HH:mm:ss"),
		              				"",
		              				"",
		              				"",
		              				""
		              				]
		              				);
		              			
		              			for(var k=0;k<horas[j].sensor1.length;k++)
								{
									var cercano1=(horas[j].sensor1[k].cercano?true:false)
									var cercano2=(horas[j].sensor2[k].cercano?true:false)
									var cercanou1=(horas[j].sensor1[k].cercanou?true:false)
									var cercanou2=(horas[j].sensor2[k].cercanou?true:false)
									$tabla.row.add(
										[
										"",
										"",
										(cercano1?"<span>":"")+
										(horas[j].sensor1[k].fecha_sensor?horas[j].sensor1[k].fecha_sensor:"")
										+(cercano1?" <span class='glyphicon glyphicon-ok'></span></span>":"")
										,
										!horas[j].sensor1[k].fecha_sensor?"":(horas[j].sensor1[k].ultrasonido_sensor?((cercanou1?"<span>1 <span class='glyphicon glyphicon-ok'></span></span>":"1")):"0"),
										(cercano2?"<span>":"")+
										(horas[j].sensor2[k].fecha_sensor?horas[j].sensor2[k].fecha_sensor:"")
										+(cercano2?" <span class='glyphicon glyphicon-ok'></span></span>":"")
										,
										!horas[j].sensor2[k].fecha_sensor?"":(horas[j].sensor2[k].ultrasonido_sensor?((cercanou2?"<span>1 <span class='glyphicon glyphicon-ok'></span></span>":"1")):"0")
										
										]
										);
								}
		              		}
		              	}
		              	cartilla.calcularPorcentaje(fecha_inicial,fecha_final);
		              	$tabla.draw();
		              	con_datos=true;
		              	$("#guardar_configuracion").prop("disabled",!($("[name=intervalo]").val()!=""&&con_datos));
		              		
		            }, 
		            
		            error: function(error){
		              console.log(error);
		            },
		            complete:function(xhr,status){
		            	$("#dvLoading").hide();
		            }
	          	});
	        
    });
$("#formularioDatosCartilla").submit();
function Cartilla()
{
	this.fechas=[];
	this.ordenarSensores=function()
	{
		for(var i=0;i<this.fechas.length;i++)
		{
			for(var j=0;j<this.fechas[i].horas.length;j++)
			{
				this.fechas[i].horas[j].ordenarSensores();
			}
		}
	}
	this.contieneFecha=function(fecha)
	{
		for(var i=0;i<this.fechas.length;i++)
		{
			if(this.fechas[i].fecha==fecha)
			{
				return true;
			}
		}
		return false;
	};
	this.agregarFecha=function(fecha)
	{
		this.fechas.push(fecha);
	};
	this.agregarHora=function(fecha,hora)
	{
		for(var i=0;i<this.fechas.length;i++)
		{
			if(this.fechas[i].fecha==fecha)
			{
				this.fechas[i].agregarHora(hora);
				break;
			}
		}
	};
	this.asignarSensor=function(fecha,hora,datos)
	{
		if(this.contieneHora(fecha,hora))
		{
			for(var i=0;i<this.fechas.length;i++)
			{
				if(this.fechas[i].fecha==fecha)
				{
					for(var j=0;j<this.fechas[i].horas.length;j++)
					{
						if(this.fechas[i].horas[j].hora==hora)
						{
							if(!this.fechas[i].horas[j].contieneSensor(datos))
								this.fechas[i].horas[j].agregarSensor(datos);
						}
							
							
					}
				}
			}
		}
		
	};
	this.contieneHora=function(fecha,hora)
	{
		for(var i=0;i<this.fechas.length;i++)
		{
			if(this.fechas[i].fecha==fecha)
			{
				for(var j=0;j<this.fechas[i].horas.length;j++)
				{
					if(this.fechas[i].horas[j].hora==hora)
						return true;
				}
					
			}
		}
		return false;
	};
	this.buscarFechaCartilla=function(fecha)
	{
		for(var i=0;i<this.fechas.length;i++)
		{
			if(this.fechas[i].fecha==fecha)
			{
				return this.fechas[i];
			}
		}
		return null;
	};
	this.calcularPorcentaje=function(fi,ff)
	{
		var contador1=0;
		var contador2=0;
		var contadoru1=0;
		var contadoru2=0;
		var contadorTotal=0;
		
		for(var i=0;i<this.fechas.length;i++)
		{
			
			var horas=this.fechas[i].horas;
		
			for(var j=0;j<horas.length;j++)
			{
			//	var fecha=horas[j].hora.substr(0,10);
				if(this.estaEntre(this.fechas[i].fecha_real,fi,ff))
				{
					
					contadorTotal++;
					
					if(horas[j].hayCoincidencia(1))
						contador1++;
					if(horas[j].hayCoincidencia(2))
						contador2++;
					if(horas[j].hayCoincidenciaUltrasonido(1))
						contadoru1++;
					if(horas[j].hayCoincidenciaUltrasonido(2))
						contadoru2++;
				}
				
				
			}
		}
		
		$("#totalRegistros").text(contadorTotal);
		$("#porcentaje1").text((Math.round(contador1*10000/contadorTotal))/100);
		$("#porcentaje2").text((Math.round(contador2*10000/contadorTotal))/100);
		$("#porcentajeu1").text((Math.round(contadoru1*10000/contador1))/100);
		$("#porcentajeu2").text((Math.round(contadoru2*10000/contador2))/100);
	};
	this.estaEntre=function(fecha,finicial,ffinal)
	{
		var fe=new Date(fecha).getTime();
		var fi=new Date(finicial).getTime();
		var ff=new Date(ffinal).getTime();
		
		return fe>=fi&&fe<=ff;
		
		
	};
	
}
function FechaCartilla(fec)
{
	this.horas=[];
	this.fecha=fec;
	this.fecha_real="";
	
	this.ponerFecha=function(fec)
	{
		this.fecha=fec;
	};
	this.agregarHora=function(hora)
	{
		this.horas.push(hora);
	};
	this.contieneHora=function(hora)
	{
		for(var i=0;i<this.horas.length;i++)
		{
			if(this.horas[i].hora==hora)
			{
				return true;
			}
		}
		return false;
	};
	
}
function Hora(hora)
{
	this.hora=hora;
	this.sensor1=[];
	this.sensor2=[];
	this.sensores=[];
	
	this.agregarSensor=function(data)
	{
		
		this.sensores.push(data);
		
	};
	this.ordenarSensores=function()
	{
		var anterior="";
		
		for(var i=0;i<this.sensores.length;i++)
		{
			
			var data=this.sensores[i];
	
			if(anterior=="")
			{
				anterior=data;
				continue;
			}
			if(anterior!="")
			{
				if(anterior.fecha_sensor==data.fecha_sensor&&
					anterior.id_configuracion_nicturia!=data.id_configuracion_nicturia)
				{
					if(anterior.id_configuracion_nicturia==1)
					{
						this.sensor1.push(anterior);
						this.sensor2.push(data);
						anterior="";
					}
					
				}
				else
				{
					
					if(anterior.id_configuracion_nicturia==1&&
						data.id_configuracion_nicturia==2)
					{
						this.sensor1.push(anterior);
						this.sensor2.push("");
						this.sensor1.push("");
						this.sensor2.push(data);
						anterior="";
					}
					else if(anterior.id_configuracion_nicturia==1&&
						data.id_configuracion_nicturia==1)
					{
						this.sensor1.push(anterior);
						this.sensor2.push("");
						anterior=data;
					}
					else if(anterior.id_configuracion_nicturia==2)
					{
						this.sensor1.push("");
						this.sensor2.push(anterior);
						anterior=data;
					}
				}
			}
			
		}
		if(anterior)
		{
			if(anterior.id_configuracion_nicturia==1)
			{
				this.sensor1.push(anterior);
				this.sensor2.push("");
				
			}
			else
			{
				this.sensor1.push("");
				this.sensor2.push(anterior);
			}
		//	this.sensor1.push("");
		//	this.sensor2.push("");
		
			anterior="";
		}
		//obtener el mas cercano segun la fecha
		var menor1 = {diferencia:1000000};
		var menor2 = {diferencia:1000000};
		for(var i=0;i<this.sensor1.length;i++)
		{
			var sensor1=this.sensor1[i];
			var sensor2=this.sensor2[i];

			menor1 = parseInt(sensor1.diferencia)<parseInt(menor1.diferencia)?sensor1:menor1;
			menor2 = parseInt(sensor2.diferencia)<parseInt(menor2.diferencia)?sensor2:menor2;
		}
		menor1.cercano=true;
		menor2.cercano=true;
		
		if(menor1.ultrasonido_sensor==0)
			menor1.diferencia=1000000;
		if(menor2.ultrasonido_sensor==0)
			menor2.diferencia=1000000;
		for(var i=0;i<this.sensor1.length;i++)
		{
			var sensor1=this.sensor1[i];
			var sensor2=this.sensor2[i];
			if(parseInt(sensor1.diferencia)<parseInt(menor1.diferencia))
			{
				if(this.estaEntre(sensor1.fecha_sensor,this.agregarMinutos(menor1.fecha_sensor,-6),this.agregarMinutos(menor1.fecha_sensor,6))
					)
				menor1=sensor1;
				
			}
			if(parseInt(sensor2.diferencia)<parseInt(menor2.diferencia))
			{
				if(this.estaEntre(sensor2.fecha_sensor,this.agregarMinutos(menor2.fecha_sensor,-10),this.agregarMinutos(menor2.fecha_sensor,10))
					)
				menor2=sensor2;
				
			}
			
		}
		
		menor1.cercanou=true;
		menor2.cercanou=true;


	};
	this.contieneSensor=function(data)
	{
		for(var i=0;i<this.sensores.length;i++)
		{
			if(this.sensores[i].id_configuracion_nicturia==data.id_configuracion_nicturia&&
				this.sensores[i].ultrasonido_sensor==data.ultrasonido_sensor&&
				this.sensores[i].fecha_cartilla==data.fecha_cartilla&&
				this.sensores[i].horario_cartilla==data.horario_cartilla&&
				this.sensores[i].fecha_sensor==data.fecha_sensor&&
				this.sensores[i].fecha_hora_cartilla==data.fecha_hora_cartilla)
			{
				return true;
			}
		}
		return false;
	};
	this.hayCoincidencia=function(sensor)
	{
		for(var i=0;i<this.sensor1.length;i++)
		{
			if(sensor==1)
			{
				if(this.sensor1[i].cercano)
				{
					return true;
				}
			}
			else if(sensor==2)
			{
				if(this.sensor2[i].cercano)
				{
					return true;
				}
			}
		}
		return false;
	};
	this.hayCoincidenciaUltrasonido=function(sensor)
	{
		for(var i=0;i<this.sensor1.length;i++)
		{
			if(sensor==1)
			{
				if(this.sensor1[i].cercanou&&this.sensor1[i].ultrasonido_sensor)
				{
					return true;
				}
			}
			else if(sensor==2)
			{
				if(this.sensor2[i].cercanou&&this.sensor2[i].ultrasonido_sensor)
				{
					return true;
				}
			}
		}
		return false;
	};
	this.agregarMinutos=function(hora, minutos) {
		return new Date(new Date(hora).getTime() + minutos*60000);
	};
	this.estaEntre=function(fecha,finicial,ffinal)
	{
		var fe=new Date(fecha).getTime();
		var fi=new Date(finicial).getTime();
		var ff=new Date(ffinal).getTime();
		
		return fe>=fi&&fe<=ff;
		
		
	};
}

function Contador(conf)
{
	this.configuracion=conf;
	this.lista=[];
	
	this.agregarValor=function(id_cartilla)
	{
		var encontrado=false;
		for(var i=0;i<this.lista.length;i++)
		{
			
			if(this.lista[i][0]==id_cartilla)
			{
				this.lista[i][1]++;
				encontrado=true;
				break;
			}
		}
		if(!encontrado)
			this.lista.push([id_cartilla,1]);
		
	};
}

});

</script>
