<div>
	{{ Form::open(array('url' => 'paciente/formularioGDS', 'method' => 'post', 'role' => 'form', 'id' => 'formularioGDS')) }}
		<input type="hidden" value="1" name="inicial">
		<div>
			<div class="col-sm-3 fila">
				<label class="control-label">Fecha</label>
				<input type="text" id="fecha_gds" class="form-control fecha_gds" name="fecha_gds">
			</div>
		</div>
		<table class="table table-bordered"  style="margin-left:15px;">
			<thead>
				<tr>
					<th>
						Seleccione
					</th>
					<th>
						Estadio GDS
					</th>
					<th>
						Estadio FAST y diagnóstico clínico
					</th>
					<th>
						Características
					</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>
						<input id="GDS-p-1" type="radio" name="gds" value="gds1">
					</td>
					<td>
						GDS 1. Ausencia de alteración cognitiva
					</td>
					<td>
						1. Adulto normal
					</td>
					<td>
						Ausencia de dificultades objetivas o subjetivas
					</td>
				</tr>
				<tr>
					<td>
						<input id="GDS-p-2" type="radio" name="gds" value="gds2">
					</td>
					<td>
						GDS 2. Defecto
						cognitivo muy
						leve
					</td>
					<td>
						2. Adulto normal
						de edad 
					</td>
					<td>
						Quejas de pérdida de memoria. No se
						objetiva déficit en el examen clínico. Hay
						pleno conocimiento y valoración de la
						sintomatología
					</td>
				</tr>
				<tr>
					<td>
						<input id="GDS-p-3" type="radio" name="gds" value="gds3">
					</td>
					<td>
						GDS 3. Defecto
						cognitivo leve
					</td>
					<td>
						3. EA incipiente 
					</td>
					<td>
						Primeros defectos claros<br>
						Manifestación en una o más de estas áreas:
						<ul>
							<li>Haberse perdido en un lugar no familiar</li>
							<li>Evidencia de rendimiento laboral pobre</li>
							<li>Dificultad incipiente para evocar nombres de persona</li>
							<li>Tras la lectura retiene escaso material</li>
							<li>Olvida la ubicación, pierde o coloca erróneamente objetos de valor</li>
							<li>Escasa capacidad para recordar a personas nuevas que ha conocido</li>
							<li>Disminución de la capacidad organizativa</li>
						</ul>
						Se observa evidencia objetiva de defectos
						de memoria únicamente en una entrevista
						intensiva.
					</td>
				</tr>
				<tr>
					<td>
						<input id="GDS-p-4" type="radio" name="gds" value="gds4">
					</td>
					<td>
						GDS 4. Defecto
						cognitivo
						moderado
					</td>
					<td>
						4. EA leve
					</td>
					<td>
						Disminución de la capacidad para realizar
						tareas complejas. <br>
						Defectos claramente definidos en una
						entrevista clínica cuidadosa:
						<ul>
							<li>Conocimiento disminuído de acontecimientos actuales y recientes</li>
							<li>El paciente puede presentar cierto déficit en el recuerdo de su historia personal</li>
							<li>Dificultad de concentración evidente en la sustracción seriada</li>
							<li>Capacidad disminuída para viajar, controlar su economía, etc.</li>
						</ul>
						Frecuentemente no hay defectos en:
						<ul>
							<li>Orientación en tiempo y persona</li>
							<li>Reconocimiento de caras y personas familiares</li>
							<li>Capacidad de viajar a lugares conocidos</li>
						</ul>
						La negación es el mecanismo de defensa
						predominante
					</td>
				</tr>
				<tr>
					<td>
						<input id="GDS-p-5" type="radio" name="gds" value="gds5">
					</td>
					<td>
						GDS 5. Defecto
						cognitivo
						moderadamente
						grave
					</td>
					<td>
						5. EA moderada
					</td>
					<td>
						El paciente no puede sobrevivir mucho
						tiempo sin alguna asistencia. Requiere
						asistencia para escoger su ropa. Es incapaz
						de recordar aspectos importantes de
						su vida cotidiana (dirección, teléfono,
						nombres de familiares). Es frecuente
						cierta desorientación en tiempo o en lugar.
						Dificultad para contar al revés desde 40 de
						4 en 4 o desde 20 de 2 en 2. Sabe su nombre
						y generalmente el de su esposa e hijos.
					</td>
				</tr>
				<tr>
					<td rowspan="6">
						<input id="GDS-p-6" type="radio" name="gds" value="gds6">
					</td>
					<td rowspan="6">
						GDS 6. Defecto
						cognitivo grave
					</td>
					<td>
						6. EA moderada-grave
					</td>
					<td>
						Se viste incorrectamente sin asistencia o
						indicaciones. Olvida a veces el nombre de su
						esposa de quien depende para vivir. Retiene
						algunos datos del pasado. Desorientación
						temporoespacial. Dificultad para contar
						de 10 en 10 en orden inverso o directo.
						Recuerda su nombre y diferencia los
						familiares de los desconocidos. Ritmo diurno
						frecuentemente alterado. Presenta cambios
						de la personalidad y la afectividad (delirio,
						síntomas obsesivos, ansiedad, agitación o
						agresividad y abulia cognoscitiva).
					</td>
				</tr>
				<tr>
					<td>
						6a
					</td>
					<td>
						 Se viste incorrectamente sin asistencia
						 o indicaciones
					</td>
				</tr>
				<tr>
					<td>
						6b
					</td>
					<td>
						 Incapaz de bañarse correctamente
					</td>
				</tr>
				<tr>
					<td>
						6c
					</td>
					<td>
						Incapaz de utilizar el váter
					</td>
				</tr>
				<tr>
					<td>
						6d
					</td>
					<td>
						 Incontinencia urinaria
					</td>
				</tr>
				<tr>
					<td>
						6e
					</td>
					<td>
						Incontinencia fecal 
					</td>
				</tr>
				<tr>
					<td rowspan="7">
						<input id="GDS-p-7" type="radio" name="gds" value="gds7">
					</td>
					<td rowspan="7">
						GDS 7. Defecto
						cognitivo muy
						grave
					</td>
					<td>
						7. EA grave 
					</td>
					<td>
						Pérdida progresiva de todas las capacidades
						verbales y motoras. Con frecuencia se
						observan signos neurológicos
					</td>
				</tr>
				<tr>
					<td>
						7a
					</td>
					<td>
						Incapaz de decir más de media docena
						de palabras
					</td>
				</tr>
				<tr>
					<td>
						7b
					</td>
					<td>
						Sólo es capaz de decir una palabra inteligible
					</td>
				</tr>
				<tr>
					<td>
						7c
					</td>
					<td>
						Incapacidad de deambular sin ayuda
					</td>
				</tr>
				<tr>
					<td>
						7d
					</td>
					<td>
						Incapacidad para mantenerse sentado sin
						ayuda
					</td>
				</tr>
				<tr>
					<td>
						7e
					</td>
					<td>
						Pérdida de capacidad de sonreír
					</td>
				</tr>
				<tr>
					<td>
						7f
					</td>
					<td>
						 Pérdida de capacidad de mantener la cabeza
						 erguida
					</td>
				</tr>
			</tbody>
		</table>
		<div class="row">
			<!--<button id="editarGDS" type="button" class="btn btn-primary pull-right" onclick="habilitarBoton('formularioGDS', 'editarGDS', 'guardarGDS')">Editar</button>
			<button type="button" class="btn btn-success pull-right" id="imprimirformularioGDS" onclick="imprimirPDF('formularioGDS')" >PDF</button>
			<button id="guardarGDS" type="submit" class="btn btn-primary pull-right" style="display:none;">Guardar</button>
         -->
			<button type="button" class="btn btn-primary pull-right" onclick="habilitarBoton('formularioGDS', 'editarGDS', 'guardarGDS')" id="editarGDS">Editar</button>
			<button type="button" class="btn btn-success pull-right" id="imprimirformularioGDS" onclick="imprimirPDF('formularioGDS')" >PDF</button>
			<button type="submit" class="btn btn-primary pull-right" id="guardarGDS" style="display:none;">Guardar</button>
		</div>
	{{ Form::close() }}
</div>
<script>
	
$(function(){
	$("#formularioGDS").formValidation({
		excluded: ':disabled',
		framework: 'bootstrap',
		fields: {
			"fecha_gds": {
				validators:{
					/*notEmpty: {
						message: 'El nombre es obligatorio'
					},*/
                    date: {
                        format: 'DD-MM-YYYY',
                        //min: '01/01/2010',
                        //max: '12/30/2020',
                        message: 'La fecha no es válida'
                    }
				}
			}
                        
		}
                       
	}).on('err.field.fv', function(e, data) {
		if (data.fv.getSubmitButton()) data.fv.disableSubmitButtons(false);
	}).on('success.field.fv', function(e, data) {
		if (data.fv.getSubmitButton()) data.fv.disableSubmitButtons(false);
	}).on("success.form.fv", function(evt){

		$("#formularioGDS input[type='submit']").prop("disabled", false);
		//$('#fecha_gds').prop("disabled", false);
		evt.preventDefault();
		$("#dvLoading").show();
		var $form = $(evt.target);
		
		var form = $(this).serializeArray();
		form.push({name:"rut", value:"{{ $rut }}"});

		$.ajax({
			url: $form.prop("action"),
			data: form,
			type: "post",
			dataType: "json",
			success: function(data){
				if(data.exito) bootbox.alert("<h4>"+data.exito+"</h4>", function(){
					verDatosPaciente();
					deshabilitarBoton('formularioGDS', 'editarGDS', 'guardarGDS');
				});
				if(data.error){ bootbox.alert("<h4>"+data.error+"</h4>");console.log(data.msg);}
				$("#dvLoading").hide();
			},
			error: function(error){
				console.log(error);
				$("#dvLoading").hide();
			}
		});
		return false;
	});
	

});
</script>
