@extends("Template/Template")

@section("titulo")
Paciente
@stop

@section("script")

<script>
var tablePaciente = null;

var existePaciente = function(rutPaciente){
	var existe = false;
	$.ajax({
	    url: "existePaciente",
	    data: { rutPaciente: rutPaciente },
	    type: "post",
	    dataType: "json",
	    async: false,
	    success: function(data){
	    	existe = data.resultado;
	    }, error : function(error){ console.log(error); }
    });
    return existe;
}

var obtenerPacientes = function(filtro){
	$.ajax({
		url: "obtenerPacientes",
		data: {filtro: ""+filtro+""},
		headers: {        
		    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		type: "post",
		dataType: "json",
		success: function(data){
			tablePaciente.clear().draw();
			tablePaciente.rows.add(data.datos).draw();
		}
	});
}

$(function(){
	$("#menuPaciente").collapse();
	
	tablePaciente = $('#pacientes').DataTable({	
		"iDisplayLength": 10,
		"bJQueryUI": true,
		"searching": true,
		"ordering": false,
		"info": true,
		"bAutoWidth" : false,
		/*"ajax": {
			"url": "obtenerPacientes",
			"type": "post"
		},*/
		"responsive": true,
		"oLanguage": {
			"sUrl": "{{URL::to('/')}}/js/spanish.json"
		}
	});

	$('#comuna').val("");
	$('#genero').val("D");
	if( "{{$tipoUsuario}}" !=3 )
		obtenerPacientes(1);
	else 
		obtenerPacientes(0);

	$('#fechaNacimiento').datetimepicker({
		locale: "es",
		format: "DD-MM-YYYY",
		maxDate: moment().format("YYYY/MM/DD")
	})

	$('#modalAgregarPaciente').on('hidden.bs.modal', function (e) {
		$("#formAgregarPaciente").data('formValidation').resetForm();
	});

	$("#formAgregarPaciente").formValidation({
		excluded: ':disabled',
		framework: 'bootstrap',
		verbose: false,
		fields: {
			rut: {
				validators:{
					notEmpty: {
						message: 'El rut es obligatorio'
					},
					callback: {
						callback: function(value, validator, $field){
							var field_rut = $field;
							var dv = $("#dv");
							if (!esRutValido(field_rut.val(), dv.val())){
								dv.val("");
							}
							return true;
						}
					}
				}
			},
			dv:{
				validators:{
					notEmpty: {
						message: 'El dígito verificador es obligatorio'
					},
					callback: {
						callback: function(value, validator, $field){
							var field_rut = $("#rut");
							var dv = $field;

							if(field_rut.val() == '' && dv.val() == '') {
								return true;
							}
							if(field_rut.val() != '' && dv.val() == ''){
								return {valid: false, message: "Debe ingresar el dígito verificador"};
							}
							if(field_rut.val() == '' && dv.val() != ''){
								return {valid: false, message: "Debe ingresar el rut"};
							}
							var rut = $.trim(field_rut.val());
							var esValido=esRutValido(field_rut.val(), dv.val());
							if(!esValido){
								return {valid: false, message: "Dígito verificador no coincide con el rut"};
							}
							var existe = existePaciente(field_rut.val());

							if(existe) return {valid: false, message: "El paciente se encuentra registrado"};
							return true;
						}
					}
				}
			},
			nombre: {
				validators:{
					notEmpty: {
						message: 'El nombre es obligatorio'
					}
				}
			},
			apellido_p: {
				validators:{
					notEmpty: {
						message: 'El apellido paterno es obligatorio'
					}
				}
			}/*,
			apellido_m: {
				validators:{
					notEmpty: {
						message: 'El apellido materno es obligatorio'
					}
				}
			},
			fechaNacimiento:{
				validators:{
					notEmpty: {
						message: 'La fecha de nacimiento es obligatoria'
					}
				}
			}*/,
			password: {
				validators:{
					notEmpty: {
						message: 'La contraseña es obligatoria'
					},
					identical: {
						field: 'confirmPassword',
						message: 'Las contraseñas deben coincidir'
					}
				}
			},
			confirmPassword: {
				validators: {
					identical: {
						field: 'password',
						message: 'Las contraseñas deben coincidir'
					}
				}
			},
			correo: {
				validators:{
					/*notEmpty: {
						message: 'El correo es obligatorio'
					},*/
					emailAddress: {
						message: "El formato del correo es inválido"
					}
				}
			},
			telefono: {
				validators:{
					/*notEmpty: {
						message: 'El teléfono es obligatorio'
					},*/
					integer: {
						message: "Debe ingresar solo números"
					}
				}
			}
		}
	}).on('err.field.fv', function(e, data) {
		if (data.fv.getSubmitButton()) data.fv.disableSubmitButtons(false);
	}).on('success.field.fv', function(e, data) {
		if (data.fv.getSubmitButton()) data.fv.disableSubmitButtons(false);
	}).on("success.form.fv", function(evt){
		$("#formAgregarPaciente input[type='submit']").prop("disabled", false);
		evt.preventDefault(evt);
		$("#dvLoading").show();
		var $form = $(evt.target);
		$.ajax({
			url: $form.prop("action"),
			data: $form.serialize(),
			type: "post",
			dataType: "json",
			success: function(data){
				if(data.exito) bootbox.alert("<h4>"+data.exito+"</h4>", function(){ 
					$("#modalAgregarPaciente").modal("hide");
					location.reload();
				});
				if(data.error) bootbox.alert("<h4>"+data.error+"</h4>");
				$("#dvLoading").hide();
			},
			error: function(error){
				console.log(error);
				$("#dvLoading").hide();
			}
		});
	});

	$("#selectFiltroPacientes").on("change", function(){
		obtenerPacientes($(this).val());
	});

});

</script>

@stop

@section("section")

<fieldset>
	<legend>Pacientes</legend>
	@if(App\Models\Usuario::obtenerTipoUsuario(Auth::user()->rut) == 2)
	{{ Form::select('selectFiltroPacientes', ['0'=>'Todos los pacientes','1'=>'Mis pacientes'], 1, ['class' => 'form-control', 'id' => 'selectFiltroPacientes']) }}
	<br>
	@endif
	<table id="pacientes" class="table table-bordered table-striped">
		<thead>
			<tr>
				<th>Paciente</th>
				<th>Fecha de nacimiento</th>
				<th>Monitoreo</th>
				<th>Opciones</th>
			</tr>
		</thead>
		<tbody></tbody>
		@if(App\Models\Usuario::obtenerTipoUsuario(Auth::user()->rut) != 1)
		<tfoot>
			<tr>
				<td colspan="7">
					<a href="#modalAgregarPaciente" class="btn btn-primary" data-toggle="modal" title="Agregar nuevo paciente"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Paciente</a>
				</td>
			</tr>
		</tfoot>
		@endif
	</table>

</fieldset>


<div class="modal fade" id="modalAgregarPaciente" tabindex="-1" role="dialog" aria-labelledby="myModalLabelAgregarPaciente" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabelAgregarPaciente">Agregar paciente</h4>
			</div>
			{{ Form::open(array('url' => 'paciente/agregarPaciente', 'method' => 'post', 'class' => 'form-horizontal', 'role' => 'form', 'id' => 'formAgregarPaciente')) }}
			
			<div class="modal-body" style="min-height: 100px;">
				<div class="form-group error">
					<label for="rut" class="col-sm-2 control-label">Rut (*): </label>
					<div class="col-sm-10">
						<div class="input-group">
							{{Form::text('rut', null, array('id' => 'rut', 'class' => 'form-control', 'autofocus'))}}
							<span class="input-group-addon"> - </span>
							{{ Form::select('dv', $selectRut, null, array('class' => 'form-control', 'id' => 'dv')) }}
						</div>
					</div>
				</div>
				<div class="form-group error">
					<label for="nombre" class="col-sm-2 control-label">Nombres (*): </label>
					<div class="col-sm-10">
						<input id="nombre" name="nombre" class="form-control" type="text" onkeypress="return textoEspacioGuion(event);"/>
					</div>
				</div>
				<div class="form-group error">
					<label for="apellido_p" class="col-sm-2 control-label">Apellido paterno (*): </label>
					<div class="col-sm-10">
						<input id="apellido_p" name="apellido_p" class="form-control" type="text" onkeypress="return textoEspacioGuion(event);"/>
					</div>
				</div>
				<div class="form-group error">
					<label for="apellido_m" class="col-sm-2 control-label">Apellido materno: </label>
					<div class="col-sm-10">
						<input id="apellido_m" name="apellido_m" class="form-control" type="text" onkeypress="return textoEspacioGuion(event);"/>
					</div>
				</div>
				<div class="form-group error">
					<label for="genero" class="col-sm-2 control-label">Género: </label>
					<div class="col-sm-10">
						{{ Form::select('genero', $selectGenero, null, array('class' => 'form-control', 'id' => 'genero')) }}
					</div>
				</div>
				<div class="form-group error">
					<label for="fechaNacimiento" class="col-sm-2 control-label">Fecha de nacimiento: </label>
					<div class="col-sm-10">
						{{Form::text("fechaNacimiento", null, array("id" => "fechaNacimiento", "class" => "form-control"))}}
					</div>
				</div>
				<div class="form-group error">
					<label for="telefono" class="col-sm-2 control-label">Télefono: </label>
					<div class="col-sm-10">
						<input id="telefono" name="telefono" class="form-control" type="text" onkeypress="return soloNumeros(event);"/>
					</div>
				</div>
				<div class="form-group error">
					<label for="fecha" class="col-sm-2 control-label">Correo: </label>
					<div class="col-sm-10">
						<input id="correo" name="correo" class="form-control" type="text" />
					</div>
				</div>
				<div class="form-group error">
					<label for="password" class="col-sm-2 control-label">Contraseña (*): </label>
					<div class="col-sm-10">
						<input id="password" name="password" class="form-control" type="password" />
					</div>
				</div>
				<div class="form-group error">
					<label for="confirmPassword" class="col-sm-2 control-label">Confirmar contraseña (*): </label>
					<div class="col-sm-10">
						<input id="confirmPassword" name="confirmPassword" class="form-control" type="password" />
					</div>
				</div>
				<div class="form-group error">
					<label for="paciente-tipoAlerta" class="col-sm-2 control-label">Tipo Evento: </label>
					<div class="col-sm-10">
						{{ Form::select('paciente-tipoAlerta', $selectAlerta, null, array('class' => 'form-control', 'id' => 'paciente-tipoAlerta')) }}
					</div>
				</div>
				<div class="form-group error">
					<label for="comuna" class="col-sm-2 control-label">Comuna: </label>
					<div class="col-sm-10">
						{{ Form::select('comuna', $selectComunas, null, array('class' => 'form-control', 'id' => 'comuna')) }}	
					</div>
				</div>
				<div class="form-group error">
					<label for="calle" class="col-sm-2 control-label">Calle: </label>
					<div class="col-sm-10">
						<input id="calle" name="calle" class="form-control" type="text" />
					</div>
				</div>
				<div class="form-group error">
					<label for="dpto" class="col-sm-2 control-label">Departamento: </label>
					<div class="col-sm-10">
						<input id="dpto" name="dpto" class="form-control" type="text" />
					</div>
				</div>
				<div class="form-group error">
					<label for="numero" class="col-sm-2 control-label">Número: </label>
					<div class="col-sm-10">
						<input id="numero" name="numero" class="form-control" type="text" />
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary">Aceptar</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
			</div>
			{{ Form::close() }}
		</div>
	</div>
</div>

@stop
