<!--<fieldset>
	<legend class="negrita" style="font-size: 16px;">Timed Get Up and Go Test Inicial</legend>-->
<div style="text-align: left;">
		
	<div class="form">
		{{ Form::open(array('url' => 'paciente/editarEncuestasCharlson', 'method' => 'post', 'role' => 'form', 'id' => 'formCharlsonInicial')) }}
		<div>
			<input name="inicio" value="true" hidden/>
			<input name="tipo-encuesta" value="charlson" hidden/>
		</div>

		<h4>Indice de comorbilidad de Charlson Inicial</h4>

		<div class="row">
			<div class="col-sm-6">
				<div class="form-group ">
					<label for="CharlsonInicial-fecha-encuesta" class="control-label" style="width:100%;">Fecha</label>
					<input id="CharlsonInicial-fecha-encuesta" name="fecha-encuesta" class="form-control fecha" type="text"/>	
				</div>
			</div>                    
		</div>
		
		<fieldset>
			<!--<legend class="negrita" style="font-size: 14px;">Instrucciones:</legend>
			<p>Seleccione lo que proceda </p>-->
	                
			<table class="table table-bordered" id="tbl_1">
				<tbody>
					<tr>
				      	<td colspan="1">
				      		Total:
				      	</td>
				      	<td>
				      		<input type="number" min="0" name="indiceBarthel-total" class="form-control indiceCharlsonInicial-total"  readonly >
				      	</td>
				    </tr>
				</tbody>   
				<tbody>
                    <tr>
				      	<td>
                            <p>Infarto de miocardio: debe existir evidencia en la historia clínica que el paciente fue hospitalizado por ello, o bien evidencias de que existieron cambios e enzimas y/o en ecg</p>
				      	</td>
				      	<td>
				      		<label for="charlsonInicial-p-1" class="btn btn-default btn-radio-group">
				                <input id="charlsonInicial-p-1" type="checkbox" name="p-1" value="1" /> 1
                             </label>
                                            
				      	</td>
				      
				    </tr>
                    <tr>
				      	<td>
                         	<p>Insuficiencia cardiaca: debe existir historia de disnea de esfuerzos y/o signos de insuficiencia cardiaca en la exploración física que respondieron favorablemente al tratamiento con digital, diuréticos o vasodilatadores. los pacientes que estén tomando estos tratamientos pero no podamos constatar que hubo mejoría clínica de los síntomas y/o signos, no se incluirán como tales
                         </p>
				      	</td>
				      	<td>
                            <label for="charlsonInicial-p-2" class="btn btn-default btn-radio-group">
				                <input id="charlsonInicial-p-2" type="checkbox" name="p-2" value="1" /> 1
                             </label>
                                            
				      	</td>
				      	
				    </tr>
                    <tr>
				      	<td>
                         	<p>Enfermedad arterial periférica: incluye claudicación intermitente, interveidos de by-pass arterial periférico, isquemia arterial aguda y aquellos con aneurisma de la aorta (torácica o abdominal) de > 6 cm de diámetro</p>
				      	</td>
				      	<td>
				      		<label for="charlsonInicial-p-3" class="btn btn-default btn-radio-group">
				                <input id="charlsonInicial-p-3" type="checkbox" name="p-3" value="1" /> 1
                             </label>
				      	</td>
				      	
				    </tr>
                    <tr>
				      	<td>
                         	<p>Enfermedad cerebrovascular: paciente con AVC con mínimas secuelas o AVC transitorio </p>
				      	</td>
				      	<td>
				      		<label for="charlsonInicial-p-4" class="btn btn-default btn-radio-group">
				                <input id="charlsonInicial-p-4" type="checkbox" name="p-4" value="1" /> 1
                             </label>
				      	</td>
				      	
				    </tr>
                    <tr>
				      	<td>
                         	<p>Demencia: pacientes con evidencia en la historia clínica de deterioro cognitivo crónico</p>
				      	</td>
				      	<td>
				      		<label for="charlsonInicial-p-5" class="btn btn-default btn-radio-group">
				                <input id="charlsonInicial-p-5" type="checkbox" name="p-5" value="1" /> 1
                             </label>
				      	</td>
				      	
				    </tr>
                    <tr>
				      	<td>
                         	<p>Enfermedad respiratoria crónica: debe existir evidencia en la historia clínica, en la exploración física y en exploración complementaria de cualquier enfermedad respiratoria crónica, incluyendo EPOC y asma </p>
				      	</td>
				      	<td>
				      		<label for="charlsonInicial-p-6" class="btn btn-default btn-radio-group">
				                <input id="charlsonInicial-p-6" type="checkbox" name="p-6" value="1" /> 1
                             </label>
				      	</td>
				      	
				    </tr>
                    <tr>
				      	<td>
                         <p>Enfermedad del tejido conectivo: incluye lupus, polimiositis, enfermedad mixta, polimialgia reumática, arteritis de celulas gigantes y artritis reumatoide 
                         </p>
				      	</td>
				      	<td>
				      		<label for="charlsonInicial-p-7" class="btn btn-default btn-radio-group">
				                <input id="charlsonInicial-p-7" type="checkbox" name="p-7" value="1" /> 1
                             </label>
				      	</td>
				      	
				    </tr>
                    <tr>
				      	<td>
                         	<p>Úlcera gastroduodenal: incluye a aquellos que han recibido tratamiento por un ulcus y quellos que tuvieron sangrado por úlceras </p>
				      	</td>
				      	<td>
				      		<label for="charlsonInicial-p-8" class="btn btn-default btn-radio-group">
				                <input id="charlsonInicial-p-8" type="checkbox" name="p-8" value="1" /> 1
                             </label>
				      	</td>
				      	
				    </tr>
                    <tr>
				      	<td>
                         	<p>Hepatopatía crónica leve: sin evidencias de hipertensión portal, incluye pacientes con hepatitis crónica</p>
				      	</td>
				      	<td>
				      		<label for="charlsonInicial-p-9" class="btn btn-default btn-radio-group">
				                <input id="charlsonInicial-p-9" type="checkbox" name="p-9" value="1" /> 1
                             </label>
				      	</td>
				      	
				    </tr>
                    <tr>
				      	<td>
                         	<p>Diabetes: incluye los tratados con insulina o hipoglicemiantes, pero sin complicaciones tardías, no se incluirán los tratados únicamente con dieta </p>
				      	</td>
				      	<td>
				      		<label for="charlsonInicial-p-10" class="btn btn-default btn-radio-group">
				                <input id="charlsonInicial-p-10" type="checkbox" name="p-10" value="1" /> 1
                             </label>
				      	</td>
				      	
				    </tr>
                    <tr>
				      	<td>
                         	<p>Hemiplejia: evidencia de hemiplejia o paraplejia como consecuencia de un  AVC u otra condición </p>
				      	</td>
				      	<td>
				      		<label for="charlsonInicial-p-11" class="btn btn-default btn-radio-group">
				                <input id="charlsonInicial-p-11" type="checkbox" name="p-11" value="2" /> 2
                             </label>
				      	</td>
				      	
				    </tr>
                    <tr>
				      	<td>
                         	<p>Insuficiencia renal crónica moderada/severa: incluye pacientes en diálisis, o bien con creatininas > 3 mg/dl objetivadas de forma repetida y mantenida</p>
				      	</td>
				      	<td>
				      		<label for="charlsonInicial-p-12" class="btn btn-default btn-radio-group">
				                <input id="charlsonInicial-p-12" type="checkbox" name="p-12" value="2" /> 2
                             </label>
				      	</td>
				      	
				    </tr>
                    <tr>
				      	<td>
                         	<p>Diabetes con lesión en órganos diana: evidencia de retinopatía, neuropatía o nefropatía, se incluyen también antecedentes de cetoacidosis o descompensación hiperosmolar</p>
				      	</td>
				      	<td>
				      		<label for="charlsonInicial-p-13" class="btn btn-default btn-radio-group">
				                <input id="charlsonInicial-p-13" type="checkbox" name="p-13" value="2" /> 2
                             </label>
				      	</td>
				      	
				    </tr>
                    <tr>
				      	<td>
                         	<p>Tumor o neoplasia sólida: incluye pacientes con cáncer, pero si metástasis documentadas </p>
				      	</td>
				      	<td>
				      		<label for="charlsonInicial-p-14" class="btn btn-default btn-radio-group">
				                <input id="charlsonInicial-p-14" type="checkbox" name="p-14" value="2" /> 2
                             </label>
				      	</td>
				      	
				    </tr>
                    <tr>
				      	<td>
                         	<p>Leucemia: incluye leucemia mieloide crónica, leucemia crónica, policitemia vera, otras leucemias crónicas y todas las leucemias agudas</p>
				      	</td>
				      	<td>
				      		<label for="charlsonInicial-p-15" class="btn btn-default btn-radio-group">
				                <input id="charlsonInicial-p-15" type="checkbox" name="p-15" value="2" /> 2
                             </label>
				      	</td>
				      	
				    </tr>
                    <tr>
				      	<td>
                         	<p>Linfoma: incluye todos los linfomas, waldestrom y mieloma</p>
				      	</td>
				      	<td>
				      		<label for="charlsonInicial-p-16" class="btn btn-default btn-radio-group">
				                <input id="charlsonInicial-p-16" type="checkbox" name="p-16" value="2" /> 2
                             </label>
				      	</td>
				      	
				    </tr>
                    <tr>
				      	<td>
                         	<p>Hepatopatía crónica moderada/severa: con evidencia de hipertensión portal (ascitis, várices esofágicas o encefalopatía)</p>
				      	</td>
				      	<td>
				      		<label for="charlsonInicial-p-17" class="btn btn-default btn-radio-group">
				                <input id="charlsonInicial-p-17" type="checkbox" name="p-17" value="3" /> 3
                             </label>
				      	</td>
				      	
				    </tr>
                    <tr>
				      	<td>
                         	<p>Tumor o neoplasia sólida con metástasis</p>
				      	</td>
				      	<td>
				      		<label for="charlsonInicial-p-18" class="btn btn-default btn-radio-group">
				                <input id="charlsonInicial-p-18" type="checkbox" name="p-18" value="6" /> 6
                             </label>
				      	</td>
				      	
				    </tr>
                    <tr>
				      	<td>
                         	<p>Sida definido: no incluye portadores asintomáticos</p>
				      	</td>
				      	<td>
				      		<label for="charlsonInicial-p-19" class="btn btn-default btn-radio-group">
				                <input id="charlsonInicial-p-19" type="checkbox" name="p-19" value="6" /> 6
                             </label>
				      	</td>
				      	
				    </tr>
                                      
                    <tr>
                    	<td colspan="2">
                           <p>Fuente bibliográfica de la que se ha obtenido esta versión:</p>
                           <p>Charlson M, Pompei P, Ales KL, McKenzie CR. A new method of classyfing prognostic comorbidity in longitudinal studies: development and validation. J Chron Dis 1987; 40: 373-83.</p>
                           <p>Comentarios:</p>
                           <p>En general, se considera ausencia de comorbilidad: 0-1 puntos, comorbilidad baja: 2 puntos y alta > 3 puntos. Predicción de mortalidad en seguimientos cortos (< 3 años); índice de 0: (12% mortalidad/año); índice 1-2: (26%); índice 3-4: (52%); índice > 5: (85%). En seguimientos prolongados (> 5 años), la predicción de mortalidad deberá corregirse con el factor edad, tal como se explica en el artículo original (Charlson M, J Chron Dis 1987; 40: 373-83). Esta corrección se efectúa añadiendo un punto al índice por cada década existente a partir de los 50 años (p. ej., 50 años = 1 punto, 60 años = 2, 70 años = 3, 80 años = 4, 90 años = 5, etc.). Así, un paciente de 60 años (2 puntos) con una comorbilidad de 1, tendrá un índice de comorbilidad corregido de 3 puntos, o bien, un paciente de 80 años (4 puntos) con una comorbilidad de 2, tendrá un índice de comorbilidad corregido de 6 puntos. Tiene la limitación de que la mortalidad del sida en la actualidad no es la misma que cuando se publicó el índice.</p>                                              
                       </td>
                    </tr>
				</tbody>
				<tfoot>
					<tr>
				      	<td colspan="1">
				      		Total:
				      	</td>
				      	<td>
				      		<input type="number" min="0" name="indiceCharlson-total" class="form-control indiceCharlsonInicial-total"  readonly >
				      	</td>
				    </tr>
					<tr>
						<td colspan="3">
							<button type="button" class="btn btn-primary pull-center" onclick="sumarIndiceCharlson('charlsonInicial-p-','indiceCharlsonInicial-total','mensaje-resultado-indiceCharlsonInicial', 'formCharlsonInicial');" id="ecalcularindiceCharlsonInicial">Calcular</button> <span class="mensaje-resultado-indiceCharlsonInicial"></span>
						</td>
					</tr>
				</tfoot>
				    
			</table>
                                     

		<fieldset>
		<?php
			$tipoPaciente = App\Models\Usuario::obtenerNombreTipoUsuario(Auth::user()->rut);
			if ($tipoPaciente == "medico")
			{
		?>
		<button type="button" class="btn btn-primary pull-right" onclick="habilitarBoton('formCharlsonInicial', 'editarCharlsonInicial', 'guardarCharlsonInicial')" id="editarCharlsonInicial">Editar</button>
		
		<button type="button" class="btn btn-success pull-right" id="imprimirrCharlsonInicial" onclick="imprimirPDF('formCharlsonInicial')" >PDF</button>

		<button type="submit" class="btn btn-primary pull-right" id="guardarCharlsonInicial" style="display:none;">Guardar</button>
		<?php
			}
		?>
		{{ Form::close() }}
	</div>
</div>
<!--</fieldset>-->

<script type="text/javascript">



$(function(){
	
	sumarIndiceCharlson('charlsonInicial-p-','indiceCharlsonInicial-total','mensaje-resultado-indiceCharlsonInicial', 'formCharlsonInicial');

  
	$("#formCharlsonInicial").formValidation({
		excluded: ':disabled',
		framework: 'bootstrap',
		fields: {
			"fecha-encuesta": {
				validators:{
					/*notEmpty: {
						message: 'El nombre es obligatorio'
					},*/
                    date: {
                        format: 'DD-MM-YYYY',
                        //min: '01/01/2010',
                        //max: '12/30/2020',
                        message: 'La fecha no es válida'
                    }
				}
			}
                        
		}
	}).on('err.field.fv', function(e, data) {
		if (data.fv.getSubmitButton()) data.fv.disableSubmitButtons(false);
	}).on('success.field.fv', function(e, data) {
		if (data.fv.getSubmitButton()) data.fv.disableSubmitButtons(false);
	}).on("success.form.fv", function(evt){
		console.log("--- submit formCharlsonInicial ---");

		$("#formCharlsonInicial input[type='submit']").prop("disabled", false);
		evt.preventDefault(evt);
		$("#dvLoading").show();
		var $form = $(evt.target);
		
		var form = $(this).serializeArray();
		form.push({name:"rut", value:"{{ $rut }}"});

		$.ajax({
			url: $form.prop("action"),
			data: form,
			type: "post",
			dataType: "json",
			async: false,
			success: function(data){
				if(data.exito) bootbox.alert("<h4>"+data.exito+"</h4>", function(){
					//verDatosPaciente();
					deshabilitarBoton('formCharlsonInicial', 'editarCharlsonInicial', 'guardarCharlsonInicial');
				});
				if(data.error) bootbox.alert("<h4>"+data.error+"</h4>");
				$("#dvLoading").hide();
			},
			error: function(error){
				console.log(error);
				$("#dvLoading").hide();
			}
		});
		return false;
	});


});
</script>
