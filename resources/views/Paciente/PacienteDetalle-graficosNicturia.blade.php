<script>
Highcharts.setOptions({
		lang:{
			shortMonths:["Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic"],
			weekdays:["Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado"]
		}
});
$.ajax({
        url: "graficoCartillaMicciones",
        type: "post",
        data: {rut: "{{ $rut }}"},
        dataType: "json",
        success: function (data) {
        	console.log(data);
        	var color1="#f7a35c";
			var color2="#4cd7c8";
			var color3="#9398d8";
			var color4="#d89893";
			$("#grafico_nicturia").highcharts('StockChart', {
				chart: {
					zoomType: 'x'
				},
	
				rangeSelector: {
	
				   
					enabled:false
				},
				title: {
					text: 'Nicturia'
				},
				subtitle: {
					text: 'Cartilla de micciones'
				},
				xAxis: [{
					
					labels: {
						overflow: 'justify'
					}
				}],
				yAxis: [{ 
					labels: {
						format: '{value}',
						style: {
							color: color1
						}
					},
					title: {
						text: 'Veces que se levantó el paciente',
						style: {
							color: color1
						}
					}
				}],
				tooltip: {
					shared:true,
					formatter: function () {
						var s = Highcharts.dateFormat('%A %e de %b, %Y', this.x);
		
						$.each(this.points, function () {
							s += '<br/>Veces que se levantó: <b>' + this.y + '</b>';
						});
		
						return s;
					}

				},
				legend: {
					layout: 'vertical',
					backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
				},
				series: [{
					type:'column',
					name: 'Veces que se levantó',
					data: data.tiempo_dias,
				//	step:true,
					color: color1
				}]
			});


        	
        },
        error: function (error) {
            console.log(error);
        }
    });


</script>
<div>
	<div id="grafico_nicturia" style="width:80%;">
	</div>
</div>
