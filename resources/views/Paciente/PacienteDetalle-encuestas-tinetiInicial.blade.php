<!--<fieldset>
	<legend class="negrita" style="font-size: 16px;">Timed Get Up and Go Test Inicial</legend>-->
<div style="text-align: left;">
		
	<div class="form">
		{{ Form::open(array('url' => 'paciente/editarEncuestasTineti', 'method' => 'post', 'role' => 'form', 'id' => 'formTinetiInicial')) }}
		<div>
			<input name="inicio" value="true" hidden/>
			<input name="tipo-encuesta" value="tineti" hidden/>
		</div>

		<h4>Evaluación de la marcha y el equilibrio ( Tinetti 1ra parte: equilibrio)</h4>

		<div class="row">
			<div class="col-sm-6">
				<div class="form-group ">
					<label for="TinetiInicial-fecha-encuesta" class="control-label" style="width:100%;">Fecha</label>
					<input id="TinetiInicial-fecha-encuesta" name="fecha-encuesta" class="form-control fecha" type="text"/>	
				</div>
			</div>                    
		</div>
		
		<fieldset>
			<legend class="negrita" style="font-size: 14px;">Instrucciones:</legend>
			<p>Equilibrio: el paciente está situado en una silla dura sin apoyabrazos. Se realizan las siguientes maniobras: </p>
	                
			<table class="table table-bordered" id="tbl_1">
				<tbody>
					<tr>
				      	<td colspan="1">
				      		Total Equilibrio:
				      	</td>
				      	<td style="width:80px;">
				      		<input type="number" min="0" name="indiceTineli-total" class="form-control indiceTineti-total-equilibrio" readonly >
				      	</td>
				    </tr>
				</tbody>
				<tbody>
                    <tr>
                        <td colspan="2" class="negrita">1. Equilibrio sentado</td>
                    </tr>
				    <tr>
				      	<td>
                        	<p>Se inclina o se desliza en la silla</p>
				      	</td>
				      	<td>
                            <label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indicetinettiInicial-p-1" name="p-1" value="0" valor="0" /> 0
				            </label>
				      	</td>
				    </tr>
                    <tr>
				      	<td>
                        	<p>Se mantiene seguro</p>
				      	</td>
				      	<td>
                        	<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indicetinettiInicial-p-2" name="p-1" value="1" valor="1" /> 1
				            </label>
				      	</td>
				      	
				    </tr>  
				    <!-- 2 --> 
                    <tr>
                        <td colspan="2" class="negrita">2. Levantarse</td>
                    </tr>
				    <tr>
				      	<td>
                        	<p>Imposible sin ayuda</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indicetinettiInicial-p-3" name="p-2" value="0" valor="0" /> 0
				            </label>
				      	</td>
				      
				    </tr>
                    <tr>
				      	<td>
                        	<p>Capaz, pero necesita más de un intento</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indicetinettiInicial-p-4" name="p-2" value="1" valor="1" /> 1
				            </label>
				      	</td>
				      	
				    </tr>
                    <tr>
				      	<td>
                        	<p>Capaz, sin usar los brazos</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indicetinettiInicial-p-5" name="p-2" value="2" valor="2" /> 2
				            </label>
				      	</td>
				      	
				    </tr>
                                      
                    <!-- 3 -->                  
                                      
                    <tr>
                        <td colspan="2" class="negrita">3. Intentos para levantarse</td>
                    </tr>
				    <tr>
				      	<td>
                        <p>Incapaz sin ayuda</p>
				      	</td>
				      	<td>
				      		 <label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indicetinettiInicial-p-6" name="p-3" value="0" valor="0" /> 0
				            </label>
				      	</td>
				      
				    </tr>
                    <tr>
				      	<td>
                        <p>Capaz, pero necesita más de un intento</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indicetinettiInicial-p-7" name="p-3" value="1" valor="1" /> 1
				            </label>
				      	</td>
				      	
				    </tr>
                    <tr>
				      	<td>
                        <p>Capaz de levantarse con sólo un intento</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indicetinettiInicial-p-8" name="p-3" value="2" valor="2" /> 2
				            </label>
				      	</td>
				      	
				    </tr>
                                      
                    <!-- 4 -->
                                      
                    <tr>
                        <td colspan="2" class="negrita">4. Equilibrio en bipedestación inmediata (los primeros 5 segundos)</td>
                    </tr>
				    <tr>
				      	<td>
                        <p>Inestable (se tambalea, mueve los pies), marcado balanceo del tronco</p>
				      	</td>
				      	<td>
				      		 <label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indicetinettiInicial-p-9" name="p-4" value="0" valor="0" /> 0
				            </label>
				      	</td>
				      
				    </tr>
                    <tr>
				      	<td>
                        <p>Estable pero usa el andador, bastón o se agarra a otro objeto para mantenerse</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indicetinettiInicial-p-10" name="p-4" value="1" valor="1" /> 1
				            </label>
				      	</td>
				      	
				    </tr>
                    <tr>
				      	<td>
                        <p>Estable sin andador, bastón u otros soportes</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indicetinettiInicial-p-11" name="p-4" value="2" valor="2" /> 2
				            </label>
				      	</td>
				      	
				    </tr>
				     
                                            
                    <tr>
                        <td colspan="2" class="negrita">5. Equilibrio en bipedestación</td>
                    </tr>
				    <tr>
				      	<td>
                        	<p>Inestable</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indicetinettiInicial-p-12" name="p-5" value="0" valor="0" /> 0
				            </label>
				      	</td>
				      
				    </tr>
                    <tr>
				      	<td>
                        	<p>Estable, pero con apoyo amplio (talones separados más de 10 cm),o bien usa bastón u otro soporte</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indicetinettiInicial-p-13" name="p-5" value="1" valor="1" /> 1
				            </label>
				      	</td>
				      	
				    </tr>
                    <tr>
				      	<td>
                        	<p>Apoyo estrecho sin soporte</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indicetinettiInicial-p-14" name="p-5" value="2" valor="2" /> 2
				            </label>
				      	</td>
				      	
				    </tr>

                                      
                                  
                    <tr>
                        <td colspan="2" class="negrita">6. Empujar (bipedestación con el tronco erecto y los pies juntos). El examinador empuja suavemente en el esternón del paciente con la palma de la mano, 3 veces
                        </td>
                    </tr>
				    <tr>
				      	<td>
                        <p>Empieza a caerse</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indicetinettiInicial-p-15" name="p-6" value="0" valor="0" /> 0
				            </label>
				      	</td>
				      
				    </tr>
                    <tr>
				      	<td>
                        <p>Se tambalea, se agarra, pero se mantiene</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indicetinettiInicial-p-16" name="p-6" value="1" valor="1" /> 1
				            </label>
				      	</td>
				      	
				    </tr>
                    <tr>
				      	<td>
                        <p>Estable</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indicetinettiInicial-p-17" name="p-6" value="2" valor="2" /> 2
				            </label>
				      	</td>
				      	
				    </tr>
                                     
                    <tr>
                        <td colspan="2" class="negrita">7. Ojos cerrados (en la posición de 6)
                                                
                            </td>
                    </tr>
				    <tr>
				      	<td>
                        	<p>Inestable</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indicetinettiInicial-p-18" name="p-7" value="0" valor="0" /> 0
				            </label>
				      	</td>
				      
				    </tr>
                    <tr>
				      	<td>
                        	<p>Estable</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indicetinettiInicial-p-19" name="p-7" value="1" valor="1" /> 1
				            </label>
				      	</td>
				      	
				    </tr>
                                     
                                   
                    <tr>
                        <td colspan="2" class="negrita">8. Vuelta de 360 grados 
                                                
                            </td>
                    </tr>
				    <tr>
				      	<td>
                        <p>Pasos discontinuos</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indicetinettiInicial-p-20" name="p-8" value="0" valor="0" /> 0
				            </label>
				      	</td>
				      
				    </tr>
                    <tr>
				      	<td>
                        	<p>Continuos</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indicetinettiInicial-p-21" name="p-8" value="1" valor="1" /> 1
				            </label>
				      	</td>
				      	
				    </tr>
                                      
                    <tr>
				      	<td>
                        	<p>Inestable (se tambalea, se agarra)</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indicetinettiInicial-p22" name="p-8b" value="0" valor="0" /> 0
				            </label>
				      	</td>
				      	
				    </tr>
                                      
                     <tr>
				      	<td>
                        	<p>Estable</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indicetinettiInicial-p-23" name="p-8b" value="1" valor="1" /> 1
				            </label>
				      	</td>
				      	
				    </tr>
                                      
    
                                      
                    <tr>
                        <td colspan="2" class="negrita">9. Sentarse 
                        </td>
                    </tr>
				    <tr>
				      	<td>
                        	<p> Inseguro, calcula mal la distancia, cae en la silla</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indicetinettiInicial-p-24" name="p-9" value="0" valor="0" /> 0
				            </label>
				      	</td>
				      
				    </tr>
                    <tr>
				      	<td>
                        	<p>Usa los brazos o el movimiento es brusco</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indicetinettiInicial-p-25" name="p-9" value="1" valor="1" /> 1
				            </label>
				      	</td>
				      	
				    </tr>
                                      
                    <tr>
				      	<td>
                        	<p>Seguro, movimiento suave</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indicetinettiInicial-p-26" name="p-9" value="2" valor="2" /> 2
				            </label>
				      	</td>
				      	
				    </tr>
				    <tr>
				      	<td>
                            Puntuacion total equilibrio (maximo 16)
                                                  
                    	</td>
                    	<td>
                    	</td>
                    </tr>
                                      
				</tbody>
				<tfoot>
					<tr>
				      	<td colspan="1">
				      		Total Equilibrio:
				      	</td>
				      	<td>
				      		<input type="number" min="0" name="indiceTineli-total" class="form-control indiceTineti-total-equilibrio"  readonly >
				      	</td>
				    </tr>
					<tr>
						<td colspan="3">
							<button type="button" class="btn btn-primary pull-center" onclick="sumarIndiceTinetiEquilibrio('indicetinettiInicial-p-','indiceTineti-total-equilibrio','mensaje-resultado-indiceTineliInicial', 'formTinetiInicial');" id="ecalcularindiceTineliInicial">Calcular</button> <span class="mensaje-resultado-indiceTineliInicial"></span>
						</td>
					</tr>
				</tfoot>
				    
			</table>
                        
            <br>
            <legend class="negrita" style="font-size: 14px;">Evaluacion de la marcha y el equilibrio:(2da parte : Marcha)</legend>
			<p>El paciente permanece de pie con el examinador, camina por el pasillo o por la habitación (unos 8 metros) a «paso normal», luego regresa a «paso rápido pero seguro». </p>
            <table class="table table-bordered" id="tbl_2">
				
				<tbody>
					<tr>
				      	<td colspan="1">
				      		Total Marcha:
				      	</td>
				      	<td style="width:80px;">
				      		<input type="number" min="0" name="indiceTineli-total" class="form-control indiceTinetiInicial-total-marcha" readonly >
				      	</td>
				    </tr>
				</tbody>   
				<tbody>
                     <!-- 10 -->
                    <tr>
                        <td colspan="2" class="negrita">10. Iniciación de la marcha (inmediatamente después de decir que ande) </td>
                    </tr>
				    <tr>
				      	<td>
                        	<p>Algunas vacilaciones o múltiples intentos para empezar</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indicetinettiInicial-p-27" name="p-10" value="0" valor="0" /> 0
				            </label>
				      	</td>
				      
				    </tr>
                    <tr>
				      	<td>
                        <p>No vacila</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indicetinettiInicial-p-28" name="p-10" value="1" valor="1" /> 1
				            </label>
				      	</td>
				      	
				    </tr>
                                       
                                  
                    <tr>
                        <td colspan="2" class="negrita">11. Longitud y altura de paso</td>
                    </tr>
				    <tr>
				      	<td colspan="2">
                        <p>a) Movimiento del pie derecho</p>
				      	</td>
				    </tr>
                    <tr>
				      	<td>
                        	<p>No sobrepasa el pie izquierdo con el paso</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indicetinettiInicial-p-29" name="p-11aa" value="0" valor="0" /> 0
				            </label>
				      	</td>
				      	
				    </tr>
                    <tr>
				      	<td>
                        <p>Sobrepasa al pie izquierdo</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indicetinettiInicial-p-30" name="p-11aa" value="1" valor="1" /> 1
				            </label>
				      	</td>
				      	
				    </tr>
                    <tr>
				      	<td>
                        <p>El pie derecho no se separa completamente del suelo con el paso</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indicetinettiInicial-p-31" name="p-11ab" value="0" valor="0" /> 0
				            </label>
				      	</td>
				      	
				    </tr>
                                      
                     <tr>
				      	<td>
                        <p>El pie derecho se separa completamente del suelo con el paso</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indicetinettiInicial-p-32" name="p-11ab" value="1" valor="1" /> 1
				            </label>
				      	</td>
				      	
				    </tr>
                                      
                    <tr>
				      	<td colspan="2">
                        <p>b) Movimiento del pie izquierdo</p>
				      	</td>
				    </tr>
                    <tr>
				      	<td>
                        <p>No sobrepasa el pie derecho con el paso</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indicetinettiInicial-p-33" name="p-11ba" value="0" valor="0" /> 0
				            </label>
				      	</td>
				      	
				    </tr>
                    <tr>
				      	<td>
                        <p>Sobrepasa al pie derecho</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indicetinettiInicial-p-34" name="p-11ba" value="1" valor="1" /> 1
				            </label>
				      	</td>
				      	
				    </tr>
                    <tr>
				      	<td>
                        <p>El pie izquierdo no se separa completamente del suelo con el paso</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indicetinettiInicial-p-35" name="p-11bb" value="0" valor="0" /> 0
				            </label>
				      	</td>
				      	
				    </tr>
                                      
                     <tr>
                        <td>
                            <p>El pie izquierdo se separa completamente del suelo con el paso</p>
                        </td>
                        <td>
                        	<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indicetinettiInicial-p-36" name="p-11bb" value="1" valor="1" /> 1
				            </label>
                        </td>

                     </tr>

                    <tr>
                        <td colspan="2" class="negrita">12. Simetría del paso </td>
                    </tr>
				    <tr>
				      	<td>
                        	<p>La longitud de los pasos con los pies izquierdo y derecho no es igual</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indicetinettiInicial-p-37" name="p-12" value="0" valor="0" /> 0
				            </label>
				      	</td>
				      
				    </tr>
                    <tr>
				      	<td>
                        <p>La longitud parece igual</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indicetinettiInicial-p-38" name="p-12" value="1" valor="1"/> 1
				            </label>
				      	</td>
				      	
				    </tr>
                                      
                    <!-- 13 -->
                    <tr>
                        <td colspan="2" class="negrita">13. Fluidez del paso </td>
                    </tr>
				    <tr>
				      	<td>
                        <p>Paradas entre los pasos</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indicetinettiInicial-p-39" name="p-13" value="0" valor="0" /> 0
				            </label>
				      	</td>
				      
				    </tr>
                    <tr>
				      	<td>
                        <p>Los pasos parecen continuos </p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indicetinettiInicial-p-40" name="p-13" value="1" valor="1" /> 1
				            </label>
				      	</td>
				      	
				    </tr>
              
                    <tr>
                        <td colspan="2" class="negrita">14. Trayectoria (observar el trazado que realiza uno de los pies durante unos 3 metros) </td>
                    </tr>
				    <tr>
				      	<td>
                        	<p>Desviación grave de la trayectoria</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indicetinettiInicial-p-41" name="p-14" value="0" valor="0" /> 0
				            </label>
				      	</td>
				      
				    </tr>
                    <tr>
				      	<td>
                        <p>Leve/moderada desviación o usa ayudas para mantener la trayectoria </p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indicetinettiInicial-p-42" name="p-14" value="1" valor="1" /> 1
				            </label>
				      	</td>
				      	
				    </tr>
                    <tr>
				      	<td>
                        <p>Sin desviación o ayudas </p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indicetinettiInicial-p-43" name="p-14" value="2" valor="2" /> 2
				            </label>
				      	</td>
				      	
				    </tr>
                                      
                     
                    <tr>
                        <td colspan="2" class="negrita">15. Tronco </td>
                    </tr>
				    <tr>
				      	<td>
                        <p>Balanceo marcado o usa ayudas</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indicetinettiInicial-p-44" name="p-15" value="0" valor="0" /> 0
				            </label>
				      	</td>
				      
				    </tr>
                    <tr>
				      	<td>
                        <p>No balancea pero flexiona las rodillas o la espalda o separa los brazos al caminar </p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indicetinettiInicial-p-45" name="p-15" value="1" valor="1" /> 1
				            </label>
				      	</td>
				      	
				    </tr>
                    <tr>
				      	<td>
                        <p>No se balancea, no reflexiona, ni otras ayudas </p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indicetinettiInicial-p-46" name="p-15" value="2" valor="2" /> 2
				            </label>
				      	</td>
				      	
				    </tr>
                                      
           
                    <tr>
                        <td colspan="2" class="negrita">16. Postura al caminar </td>
                    </tr>
				    <tr>
				      	<td>
                        	<p>Talones separados</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indicetinettiInicial-p-47" name="p-16" value="0" valor="0" /> 0
				            </label>
				      	</td>
				      
				    </tr>
                    <tr>
				      	<td>
                        	<p> Talones casi juntos al caminar  </p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indicetinettiInicial-p-48" name="p-16" value="1" valor="1" /> 1
				            </label>
				      	</td>
				      	
				    </tr>
                    <tr>
				      	<td>              
                            Puntuacion marcha (maximo 12)
                      	</td>
                      	<td>
                      	</td>
                    </tr>
                     <tr>
				      	<td>              
                            Puntuacion total (equilibrio y marcha)(máximo 28)
                      	</td>
                      	<td>
                      	</td>
                    </tr>
				</tbody>
				<tfoot>
					<tr>
				      	<td colspan="1">
				      		<h5><strong>Total Marcha y Equilibrio:</strong></h5>
				      	</td>
				      	<td>
				      		<input type="number" min="0" name="indiceTineti-total" class="form-control indiceTinetiInicial-total"  readonly >
				      	</td>
				    </tr>
					<tr>
						<td colspan="3">
							<button type="button" class="btn btn-primary pull-center" onclick="sumarIndiceTineti('indicetinettiInicial-p-','indiceTinetiInicial-total','mensaje-resultado-indiceTinetiInicial', 'formTinetiInicial');" id="eCalcularindiceTinetiInicial">Calcular</button> <span class="mensaje-resultado-indiceTinetiInicial"></span>
						</td>
					</tr>
				</tfoot>
				<tfoot>
					<tr>
				      	<td colspan="1">
				      		Total Marcha:
				      	</td>
				      	<td>
				      		<input type="number" min="0" name="indiceTineti-total" class="form-control indiceTinetiInicial-total-marcha"  readonly >
				      	</td>
				    </tr>
					<tr>
						<td colspan="3">
							<button type="button" class="btn btn-primary pull-center" onclick="sumarIndiceTinetiMarcha('indicetinettiInicial-p-','indiceTinetiInicial-total-marcha','mensaje-resultado-indiceTinetiInicial', 'formTinetiInicial');" id="ecalcularindiceTinetiInicial">Calcular</button> <span class="mensaje-resultado-indiceTinetiInicial"></span>
						</td>
					</tr>
				</tfoot>
				    
			</table>
                      
              

		<fieldset>

		
		<p>Fuente bibliográfica de la que se ha obtenido esta versión:</p>
Rubenstein LZ. Instrumentos de evaluación. En: Abrams WB, Berkow R. El Manual Merck de Geriatría (Ed Esp). Barcelona: Ed Doyma;
1992. p. 1251-63 (en dicho libro se hace constar que esta versión es una modificación adaptada de Tinetti et al, en: J Am Geriatr Soc
1986; 34: 119). También es la misma versión recomendada por el Grupo de Trabajo de Caídas de la SEGG (Navarro C, Lázaro M, Cues-
ta F, Vilorria A, Roiz H. Métodos clínicos de evaluación de los trastornos del equilibrio y la marcha. En: Grupo de trabajo de caídas de
la Sociedad Española de Geriatría y Gerontología. 2.ª ed. Eds. Fundación Mapfre Medicina; 2001. p. 101-22).</p>
		<?php
			$tipoPaciente = App\Models\Usuario::obtenerNombreTipoUsuario(Auth::user()->rut);
			if ($tipoPaciente == "medico")
			{
		?>
		<button type="button" class="btn btn-primary pull-right" onclick="habilitarBoton('formTinetiInicial', 'editarTinetiInicial', 'guardarTinetiInicial')" id="editarTinetiInicial">Editar</button>
	
		<button type="button" class="btn btn-success pull-right" id="imprimirTinetiInicial" onclick="imprimirPDF('formTinetiInicial')" >PDF</button>

		<button type="submit" class="btn btn-primary pull-right" id="guardarTinetiInicial" style="display:none;">Guardar</button>
		<?php
			}
		?>
		{{ Form::close() }}
	</div>
</div>
<!--</fieldset>-->

<script type="text/javascript">



$(function(){
    
    sumarIndiceTinetiEquilibrio('indicetinettiInicial-p-','indiceTineti-total-equilibrio','mensaje-resultado-indiceTineliInicial', 'formTinetiInicial');
    sumarIndiceTinetiMarcha('indicetinettiInicial-p-','indiceTinetiInicial-total-marcha','mensaje-resultado-indiceTinetiInicial', 'formTinetiInicial');
    sumarIndiceTineti('indicetinettiInicial-p-','indiceTinetiInicial-total','mensaje-resultado-indiceTinetiInicial', 'formTinetiInicial');
  
	$("#formTinetiInicial").formValidation({
		excluded: ':disabled',
		framework: 'bootstrap',
		fields: {
			"fecha-encuesta": {
				validators:{
					/*notEmpty: {
						message: 'El nombre es obligatorio'
					},*/
                    date: {
                        format: 'DD-MM-YYYY',
                        //min: '01/01/2010',
                        //max: '12/30/2020',
                        message: 'La fecha no es válida'
                    }
				}
			}
                        
		}
	}).on('err.field.fv', function(e, data) {
		if (data.fv.getSubmitButton()) data.fv.disableSubmitButtons(false);
	}).on('success.field.fv', function(e, data) {
		if (data.fv.getSubmitButton()) data.fv.disableSubmitButtons(false);
	}).on("success.form.fv", function(evt){
		console.log("--- submit formTinetiInicial ---");

		$("#formTinetiInicial input[type='submit']").prop("disabled", false);
		evt.preventDefault(evt);
		$("#dvLoading").show();
		var $form = $(evt.target);
		
		var form = $(this).serializeArray();
		form.push({name:"rut", value:"{{ $rut }}"});

		$.ajax({
			url: $form.prop("action"),
			data: form,
			type: "post",
			dataType: "json",
			success: function(data){
				if(data.exito) bootbox.alert("<h4>"+data.exito+"</h4>", function(){
					verDatosPaciente();
					deshabilitarBoton('formTinetiInicial', 'editarTinetiInicial', 'guardarTinetiInicial');
				});
				if(data.error) bootbox.alert("<h4>"+data.error+"</h4>");
				$("#dvLoading").hide();
			},
			error: function(error){
				console.log(error);
				$("#dvLoading").hide();
			}
		});
		return false;
	});


});
</script>






