<!--<fieldset>
    <legend class="negrita" style="font-size: 16px;">Inicial</legend>-->
<div style="text-align: left;">
        
    <div class="form">
        {{ Form::open(array('url' => 'paciente/editarEncuestasIndiceBarthel', 'method' => 'post', 'role' => 'form', 'id' => 'formIndiceBarthelInicial')) }}
        <div>
            <input name="inicio" value="true" hidden/>
            <input name="tipo-encuesta" value="indiceBarthel" hidden/>
        </div>

        <h4>Movilidad</h4>

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group ">
                    <label for="indiceBarthelInicial-fecha-encuesta" class="control-label" style="width:100%;">Fecha</label>
                    <input id="indiceBarthelInicial-fecha-encuesta" name="fecha-encuesta" class="form-control fecha" type="text"/>  
                </div>
            </div>
        </div>

        <fieldset>

                    <hr>
                    <p><strong>Movilidad</strong></p>
                    <table data-role="table" class="ui-responsive table-stroke">
                        <tbody>
                            
                            <tr>
                                <td>
                                    <p>No tengo problemas para caminar</p>
                                </td>
                                <td width="5%">
                                    <label for="eq-p-1" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="eq-p-1" value="no aplica" />1
                                    </label>
                                </td>
                            </tr>

                            <tr class="text-center">
                                <td>
                                    <p>Tengo problemas para caminar</p>
                                </td>
                                <td>
                                    <label for="eq-p-2" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="eq-p-1" value="leve" />1
                                    </label>
                                </td>
                            </tr>

                            <tr class="text-center">
                                <td>
                                    <p>Tengo que estar en la cama</p>
                                </td>
                                <td>
                                    <label for="eq-p-3" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="eq-p-1" value="grave" />1
                                    </label>
                                </td>
                            </tr>
                        </tbody> 
                    </table>

                    <hr>
                    <p><strong>Cuidado-Personal</strong></p>
                    <table data-role="table" class="ui-responsive table-stroke">
                        <tbody>
                            
                            <tr class="text-center">
                                <td>
                                    <p>No tengo problemas con el cuidado personal</p>
                                </td>
                                <td width="5%">
                                    <label for="eq-p-4" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="eq-p-2" value="no aplica" />1
                                    </label>
                                </td>
                            </tr>

                            <tr class="text-center">
                                <td>
                                    <p>Tengo algunos problemas para lavarme o vestirme solo</p>
                                </td>
                                <td>
                                    <label for="eq-p-5" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="eq-p-2" value="leve" />1
                                    </label>
                                </td>
                            </tr>

                            <tr class="text-center">
                                <td>
                                    <p>Soy incapaz de lavarme o vestirme solo</p>
                                </td>
                                <td>
                                    <label for="eq-p-6" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="eq-p-2" value="grave" />1
                                    </label>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <hr>
                    <p><strong>Actividades de todos los dias</strong>(Ej: Trabajar, estudiar, hacer tareas domÃ©sticas, actividades familiares o realizadas durante el tiempo libre) </p>
                    <table data-role="table" class="ui-responsive table-stroke">
                        <tbody>
                            
                            <tr class="text-center">
                                <td>
                                    <p>No tengo problemas para realizar mis actividades de todos los dÃ­as</p>
                                </td>
                                <td width="5%">
                                    <label for="eq-p-7" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="eq-p-3" value="no aplica" />1
                                    </label>
                                </td>
                            </tr>

                            <tr class="text-center">
                                <td>
                                    <p>Tengo algunos problemas para realizar mis actividades de todos los dÃ­as</p>
                                </td>
                                <td>
                                    <label for="eq-p-8" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="eq-p-3" value="leve" />1
                                    </label>
                                </td>
                            </tr>

                            <tr class="text-center">
                                <td>
                                    <p>Soy incapaz de realizar mis actividades de todos los dÃ­as</p>
                                </td>
                                <td>
                                    <label for="eq-p-9" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="eq-p-3" value="grave" />1
                                    </label>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <hr>
                    <p><strong>Dolor/Malestar</strong></p>
                    <table data-role="table" class="ui-responsive table-stroke">
                        <tbody>
                            
                            <tr class="text-center">
                                <td>
                                    <p>No tengo dolor ni malestar</p>
                                </td>
                                <td width="5%">
                                    <label for="eq-p-10" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="eq-p-4" value="no aplica" />1
                                    </label>
                                </td>
                            </tr>

                            <tr class="text-center">
                                <td>
                                    <p>Tengo moderado dolor o malestar</p>
                                </td>
                                <td>
                                    <label for="eq-p-11" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="eq-p-4" value="leve" />1
                                    </label>
                                </td>
                            </tr>

                            <tr class="text-center">
                                <td>
                                    <p>Tengo mucho dolor o malestar</p>
                                </td>
                                <td>
                                    <label for="eq-p-12" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="eq-p-4" value="grave" />1
                                    </label>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <hr>
                    <p><strong>Ansiedad/DepresiÃ³n</strong></p>
                    <table data-role="table" class="ui-responsive table-stroke">
                        <tbody>
                            
                            <tr class="text-center">
                                <td>
                                    <p>No estoy ansioso/a ni deprimido/a</p>
                                </td>
                                <td width="5%">
                                    <label for="eq-p-13" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="eq-p-5" value="no aplica" />1
                                    </label>
                                </td>
                            </tr>

                            <tr class="text-center">
                                <td>
                                    <p>Estoy moderadamente ansioso/a o deprimido/a</p>
                                </td>
                                <td>
                                    <label for="eq-p-14" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="eq-p-5" value="leve" />1
                                    </label>
                                </td>
                            </tr>

                            <tr class="text-center">
                                <td>
                                    <p>Estoy muy ansioso/a o deprimido/a</p>
                                </td>
                                <td>
                                    <label for="eq-p-15" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="eq-p-5" value="grave" />1
                                    </label>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    
                    <h4>Como las respuestas son anÃ³nimas, la informaciÃ³n personal que le pedimos a continuaciÃ³n nos ayudarÃ¡ a valorar mejor las respuestas que nos ha dado.</h4>

                    <hr>
                    <p>Â¿Tiene usted experiencia en enfermedades graves? (conteste a las tres situaciones)</p>
                    <table data-role="table" class="ui-responsive table-stroke">
                        <tbody>
                            
                            <tr class="text-center">
                                <td>
                                    <p></p>
                                </td>
                                <td>
                                    SI
                                </td>
                                <td>
                                    NO
                                </td>
                            </tr>

                            <tr class="text-center">
                                <td>
                                    <p>En usted mismo</p>
                                </td>
                                <td>
                                    <label for="eq-p-16" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="eq-p-6" value="1" />1
                                    </label>
                                </td>
                                <td>
                                    <label for="eq-p-16" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="eq-p-6" value="1" />1
                                    </label>
                                </td>
                            </tr>

                            <tr class="text-center">
                                <td>
                                    <p>En su familia</p>
                                </td>
                                <td>
                                    <label for="eq-p-17" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="eq-p-7" value="1" />1
                                    </label>
                                </td>
                                <td>
                                    <label for="eq-p-17" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="eq-p-7" value="1" />1
                                    </label>
                                </td>
                            </tr>

                            <tr class="text-center">
                                <td>
                                    <p>En el cuidado de los niÃ±os</p>
                                </td>
                                <td>
                                    <label for="eq-p-18" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="eq-p-8" value="1" />1
                                    </label>
                                </td>
                                <td>
                                    <label for="eq-p-18" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="eq-p-8" value="1" />1
                                    </label>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    
                    <hr>
                    <p>Â¿CuÃ¡ntos aÃ±os tiene?</p>
                    <div class="form-group">
                        <input type="number" id="eq-p-16">
                    </div>

                    <hr>
                    <p>Es usted</p>
                    <table data-role="table" class="ui-responsive table-stroke">
                        <tbody>
                            
                            <tr class="text-center">

                                <td>
                                    VarÃ³n
                                </td>
                                <td>
                                    Mujer
                                </td>
                            </tr>

                            <tr class="text-center">
                                <td width="5%">
                                    <label for="eq-p-19" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="eq-p-9" value="M" />1
                                    </label>
                                </td>
                                <td width="5%">
                                    <label for="eq-p-19" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="eq-p-9" value="F" />1
                                    </label>
                                </td>
                            </tr>

                            <!--<tr class="text-center">
                                <td>
                                    <p>Mujer</p>
                                </td>
                                <td>
                                    <label for="eq-p-20" class="btn btn-default btn-radio-group">
                                        <input id="eq-p-20" type="radio" name="eq-p-20" value="1" />1
                                    </label>
                                </td>
                                <td>
                                    <label for="eq-p-20" class="btn btn-default btn-radio-group">
                                        <input id="eq-p-20" type="radio" name="eq-p-20" value="1" />1
                                    </label>
                                </td>
                            </tr> -->
                        </tbody>
                    </table>

                    <hr>
                    <p>Es usted</p>
                    <table data-role="table" class="ui-responsive table-stroke">
                        <tbody>
                            
                            <tr class="text-center">
                                <td>
                                    <p></p>
                                </td>
                                <td>
                                    
                                </td>
                            </tr>

                            <tr class="text-center">
                                <td>
                                    <p>Fumador</p>
                                </td>
                                <td width="5%">
                                    <label for="eq-p-21" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="eq-p-10" value="fumador" />1
                                    </label>
                                </td>
                            </tr>

                            <tr class="text-center">
                                <td>
                                    <p>Ex-fumador</p>
                                </td>
                                <td>
                                    <label for="eq-p-21" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="eq-p-10" value="ex-fumador" />1
                                    </label>
                                </td>
                            </tr>

                            <tr class="text-center">
                                <td>
                                    <p>Nunca he fumado</p>
                                </td>
                                <td>
                                    <label for="eq-p-21" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="eq-p-10" value="nunca" />1
                                    </label>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <hr>
                    <p>Â¿Trabaja o ha trabajado en servicios sociales?</p>
                    <table data-role="table" class="ui-responsive table-stroke">
                        <tbody>
                            
                            <tr class="text-center">
                                <td>
                                    SI
                                </td>
                                <td>
                                    NO
                                </td>
                            </tr>

                            <tr class="text-center">
                                <td width="5%">
                                    <label for="eq-p-22" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="eq-p-11" value="1" />1
                                    </label>
                                </td>
                                <td width="5%">
                                    <label for="eq-p-22" class="btn btn-default btn-radio-group">
                                        <input  type="radio" name="eq-p-11" value="2" />1
                                    </label>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <p>Si ha contestado que si, Â¿en calidad de quÃ©?</p>
                    <div class="form-group">
                        <textarea name="" cols="30" rows="10" id="eq-p-23"></textarea>
                    </div>

                    <hr>
                    <p>Â¿CuÃ¡l es su principal actividad diaria?</p>
                    <table data-role="table" class="ui-responsive table-stroke">
                        <tbody>
                            
                            <tr class="text-center">
                                <td>
                                    <p>Empleado o trabaja para si mismo</p>
                                </td>
                                <td>
                                    <label for="eq-p-24" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="eq-p-24" value="empleado" />1
                                    </label>
                                </td>
                                <td>
                                    
                                </td>
                            </tr>

                            <tr class="text-center">
                                <td>
                                    <p>Retirado o jubilado</p>
                                </td>
                                <td>
                                    <label for="eq-p-24" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="eq-p-24" value="retirado" />1
                                    </label>
                                </td>
                                <td>
                                    
                                </td>
                            </tr>

                            <tr class="text-center">
                                <td>
                                    <p>Tareas domÃ©sticas</p>
                                </td>
                                <td>
                                    <label for="eq-p-24" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="eq-p-24" value="casa" />1
                                    </label>
                                </td>
                                <td>
                                    
                                </td>
                            </tr>

                            <tr class="text-center">
                                <td>
                                    <p>Estudiante</p>
                                </td>
                                <td>
                                    <label for="eq-p-24" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="eq-p-24" value="estudiante" />1
                                    </label>
                                </td>
                                <td>
                                    
                                </td>
                            </tr>
                            
                            <tr class="text-center">
                                <td>
                                    <p>Buscando trabajo</p>
                                </td>
                                <td>
                                    <label for="eq-p-24" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="eq-p-24" value="buscando" />1
                                    </label>
                                </td>
                                <td>
                                    
                                </td>
                            </tr>
                            
                            <tr class="text-center">
                                <td>
                                    <p>Otros(Por favor especifique)</p>
                                </td>
                                <td>
                                    <label for="eq-p-24" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="eq-p-24" value="otro" />1
                                    </label>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="text" id="eq-p-25">
                                    </div>
                                </td>
                            </tr>

                        </tbody>
                    </table>
                    
                    <hr>
                    <p>Â¿Nivel de estudios completados?</p>
                    <table data-role="table" class="ui-responsive table-stroke">
                        <tbody>
                            
                            <tr class="text-center">
                                <td>
                                    <p>Leer y escribir</p>
                                </td>
                                <td width="5%">
                                    <label for="eq-p-26" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="eq-p-14" value="leer" />1
                                    </label>
                                </td>
                                
                            </tr>

                            <tr class="text-center">
                                <td>
                                    <p>Elementaria, intermedia</p>
                                </td>
                                <td>
                                    <label for="eq-p-26" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="eq-p-14" value="elementaria" />1
                                    </label>
                                </td>
                                
                            </tr>

                            <tr class="text-center">
                                <td>
                                    <p>Secundaria, vocacional</p>
                                </td>
                                <td>
                                    <label for="eq-p-26" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="eq-p-14" value="secundaria" />1
                                    </label>
                                </td>
                                
                            </tr>
                            
                            <tr class="text-center">
                                <td>
                                    <p>Universidad</p>
                                </td>
                                <td>
                                    <label for="eq-p-26" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="eq-p-14" value="universidad" />1
                                    </label>
                                </td>
                                
                            </tr>
                            
                        </tbody>
                    </table>

                    <p>Si conoce su cÃ³digo postal, por favor escrÃ­balo aquÃ­</p>
                    <div class="form-group">
                        <input type="text" id="eq-p-15">
                    </div>


            <p>Máxima puntuación: 100 puntos (90 si va en silla de ruedas)</p>

            <table class="table table-bordered">
                <thead>
                        <tr>
                        <th>Resultado</th>
                            <th>Grado de dependencia</th>
                        </tr>                     
                </thead>
                <tbody>
                      <tr>
                        <td>&lt;20</td>
                        <td>Total</td>
                      </tr>

                      <tr>
                        <td>20-35</td>
                        <td>Grave</td>
                      </tr>

                      <tr>
                        <td>40-55</td>
                        <td>Moderado</td>
                      </tr>

                      <tr>
                        <td>≥ 60</td>
                        <td>Leve</td>
                      </tr>

                      <tr>
                        <td>100</td>
                        <td>Independiente</td>
                      </tr>
                </tbody>
            </table>

        <fieldset>

        
        <?php
            $tipoPaciente = App\Models\Usuario::obtenerNombreTipoUsuario(Auth::user()->rut);
            if ($tipoPaciente == "medico")
            {
        ?>

        <button type="button" class="btn btn-primary pull-right" onclick="habilitarBoton('formIndiceBarthelInicial', 'editarindiceBarthelInicial', 'guardarindiceBarthelInicial')" id="editarindiceBarthelInicial">Editar</button>
        
        <button type="button" class="btn btn-success pull-right" id="imprimirBarthelInicial" onclick="imprimirPDF('formIndiceBarthelInicial')" >PDF</button>

        <button type="submit" class="btn btn-primary pull-right" id="guardarindiceBarthelInicial" style="display:none;">Guardar</button>
        <?php
            }
        ?>
        {{ Form::close() }}
    </div>
</div>
<!--</fieldset>-->

<script type="text/javascript">

$(function(){
    sumarIndiceBarthel('indiceBarthel-p-','indiceBarthelInicial-total','mensaje-resultado-indiceBarthelInicial', 'formIndiceBarthelInicial');

    $("#formIndiceBarthelInicial").formValidation({
        excluded: ':disabled',
        framework: 'bootstrap',
        fields: {
            "fecha-encuesta": {
                validators:{
                    /*notEmpty: {
                        message: 'El nombre es obligatorio'
                    },*/
                    date: {
                        format: 'DD-MM-YYYY',
                        //min: '01/01/2010',
                        //max: '12/30/2020',
                        message: 'La fecha no es válida'
                    }
                }
            }
        }
    }).on('err.field.fv', function(e, data) {
        if (data.fv.getSubmitButton()) data.fv.disableSubmitButtons(false);
    }).on('success.field.fv', function(e, data) {
        if (data.fv.getSubmitButton()) data.fv.disableSubmitButtons(false);
    }).on("success.form.fv", function(evt){
        console.log("--- submit editarEncuestasIndiceBarthel ---");

        $("#formIndiceBarthelInicial input[type='submit']").prop("disabled", false);
        evt.preventDefault(evt);
        $("#dvLoading").show();
        var $form = $(evt.target);
        
        var form = $(this).serializeArray();
        form.push({name:"rut", value:"{{ $rut }}"});

        $.ajax({
            url: $form.prop("action"),
            data: form,
            type: "post",
            dataType: "json",
            success: function(data){
                if(data.exito) bootbox.alert("<h4>"+data.exito+"</h4>", function(){
                    verDatosPaciente();
                    deshabilitarBoton('formIndiceBarthelInicial', 'editarindiceBarthelInicial', 'guardarindiceBarthelInicial');
                });
                if(data.error) bootbox.alert("<h4>"+data.error+"</h4>");
                $("#dvLoading").hide();
            },
            error: function(error){
                console.log(error);
                $("#dvLoading").hide();
            }
        });
        return false;
    });


});
</script>






