@extends("Template/Template")

@section("titulo")
Detección Puertas
@stop

@section("script")

<script>

$(function(){
	// subir archivos
    var inArchivos = $("#inArchivos");
    inArchivos.fileinput({
        language: "es",
        showUpload: true,
        uploadAsync: true,
        uploadUrl: "../paciente/subir2",
        uploadExtraData: function(){
            return {
                        seccionArchivo: "archivo",
                        rut_paciente:$('[name=rut_pacientes]').val()

                     };
        }
    });

});

</script>

@stop

@section("section")

<fieldset>
	<legend>Detección Puertas</legend>
    <label for="rut" class="col-sm-2 control-label">Rut del paciente: </label>
    <div class="col-sm-10">
        <input type="text" id="rut_pacientes" placeholder="sin dígito verificador" name="rut_pacientes">
    </div>
	<div style="text-align: left;">
		<!-- subir archivos -->
        <div role="tabpanel" class="tab-pane" id="subir">
            <div class="row">
                <div class="col-md-12 panel panel-body">
                    <input id="inArchivos" multiple type="file">
                </div>
            </div>
        </div>
		
	</div>
</fieldset>

@stop
