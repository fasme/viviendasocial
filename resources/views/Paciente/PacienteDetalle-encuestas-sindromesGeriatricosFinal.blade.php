<!--<fieldset>
	<legend class="negrita" style="font-size: 16px;"> Final</legend>-->
<div style="text-align: left;">
		
	<div class="form">
		{{ Form::open(array('url' => 'paciente/editarEncuestasSindromesGeriatricos', 'method' => 'post', 'role' => 'form', 'id' => 'formSindromesGeriatricosFinal')) }}
		<div>
			<input name="inicio" value="false" hidden/>
			<input name="tipo-encuesta" value="sindromesGeriatricos" hidden/>
		</div>

		<h4>Síndromes Geriátricos </h4>

		<div class="row">
			<div class="col-sm-6">
				<div class="form-group ">
					<label for="sindromesGeriatricosFinal-fecha-encuesta" class="control-label" style="width:100%;">Fecha</label>
					<input id="sindromesGeriatricosFinal-fecha-encuesta" name="sindromesGeriatricos-fecha-encuesta" class="form-control fecha" type="text"/>	
				</div>
			</div>
			<div class="col-sm-6">
				<div class="form-group ">
					<label for="sindromesGeriatricosFinal-caidas-ultimos-meses" class="control-label" style="width:100%;">Caidas Últimos 6 meses</label>
					<input id="sindromesGeriatricosFinal-caidas-ultimos-meses" name="sindromesGeriatricos-caidas-ultimos-meses" class="form-control" type="text"/>	
				</div>
			</div>
		</div>

		<fieldset>
			<!--<legend class="negrita" style="font-size: 14px;">Instrucciones:</legend>-->
			<table class="table table-bordered">
				<thead>
				    <tr>
				        <th> </th>
				        <th>Si/No</th>
				        <th>Comentarios</th>
				    </tr>
				</thead>
				<tbody>

					<tr>
				      	<td>
				      		Caídas y Trastorno Marcha
				      	</td>
				      	<td>
				        	<div class="btn-group" data-toggle="buttons">
				                <label class="btn btn-default btn-default-si btn-radio-group">
				                    <input type="radio" id="sindromesGeriatricosFinal-p-1-si" name="sindromesGeriatricos-p-1" value="true" /> Si
				                </label>
				                <label class="btn btn-default btn-default-si btn-radio-group">
				                    <input type="radio" id="sindromesGeriatricosFinal-p-1-no" name="sindromesGeriatricos-p-1" value="false" /> No
				                </label>
				                <label class="btn btn-default active btn-radio-group">
				                    <input type="radio" id="sindromesGeriatricosFinal-p-1-null" name="sindromesGeriatricos-p-1" value="" class="radio-ninguno"/> Ninguno
				                </label>
				            </div>
				      	</td>
				      	<td>
				      		<textarea class="form-control" id="sindromesGeriatricosFinal-c-1" name="sindromesGeriatricos-c-1">
				      		</textarea>
				      	</td>
				    </tr>

				    <tr>
				      	<td>
				      		Polifarmacia
				      	</td>
				      	<td>
				        	<div class="btn-group" data-toggle="buttons">
				                <label class="btn btn-default btn-default-si btn-radio-group">
				                    <input type="radio" id="sindromesGeriatricosFinal-p-2-si" name="sindromesGeriatricos-p-2" value="true" /> Si
				                </label>
				                <label class="btn btn-default btn-default-si btn-radio-group">
				                    <input type="radio" id="sindromesGeriatricosFinal-p-2-no" name="sindromesGeriatricos-p-2" value="false" /> No
				                </label>
				                <label class="btn btn-default active btn-radio-group">
				                    <input type="radio" id="sindromesGeriatricosFinal-p-2-null" name="sindromesGeriatricos-p-2" value="" class="radio-ninguno"/> Ninguno
				                </label>
				            </div>
				      	</td>
				      	<td>
				      		<textarea class="form-control" id="sindromesGeriatricosFinal-c-2" name="sindromesGeriatricos-c-2">
				      		</textarea>
				      	</td>
				    </tr>

				    <tr>
				      	<td>
				      		Incontinencia Urinaria
				      	</td>
				      	<td>
				        	<div class="btn-group" data-toggle="buttons">
				                <label class="btn btn-default btn-default-si btn-radio-group">
				                    <input type="radio" id="sindromesGeriatricosFinal-p-3-si" name="sindromesGeriatricos-p-3" value="true" /> Si
				                </label>
				                <label class="btn btn-default btn-default-si btn-radio-group">
				                    <input type="radio" id="sindromesGeriatricosFinal-p-3-no" name="sindromesGeriatricos-p-3" value="false" /> No
				                </label>
				                <label class="btn btn-default active btn-radio-group">
				                    <input type="radio" id="sindromesGeriatricosFinal-p-3-null" name="sindromesGeriatricos-p-3" value="" class="radio-ninguno"/> Ninguno
				                </label>
				            </div>
				      	</td>
				      	<td>
				      		<textarea class="form-control" id="sindromesGeriatricosFinal-c-3" name="sindromesGeriatricos-c-3">
				      		</textarea>
				      	</td>
				    </tr>

				    <tr>
				      	<td>
				      		Incontinencia Fecal
				      	</td>
				      	<td>
				        	<div class="btn-group" data-toggle="buttons">
				                <label class="btn btn-default btn-default-si btn-radio-group">
				                    <input type="radio" id="sindromesGeriatricosFinal-p-4-si" name="sindromesGeriatricos-p-4" value="true" /> Si
				                </label>
				                <label class="btn btn-default btn-default-si btn-radio-group">
				                    <input type="radio" id="sindromesGeriatricosFinal-p-4-no" name="sindromesGeriatricos-p-4" value="false" /> No
				                </label>
				                <label class="btn btn-default active btn-radio-group">
				                    <input type="radio" id="sindromesGeriatricosFinal-p-4-null" name="sindromesGeriatricos-p-4" value="" class="radio-ninguno"/> Ninguno
				                </label>
				            </div>
				      	</td>
				      	<td>
				      		<textarea class="form-control" id="sindromesGeriatricosFinal-c-4" name="sindromesGeriatricos-c-4">
				      		</textarea>
				      	</td>
				    </tr>

				    <tr>
				      	<td>
				      		Deterioro Cognitivo
				      	</td>
				      	<td>
				        	<div class="btn-group" data-toggle="buttons">
				                <label class="btn btn-default btn-default-si btn-radio-group">
				                    <input type="radio" id="sindromesGeriatricosFinal-p-5-si" name="sindromesGeriatricos-p-5" value="true" /> Si
				                </label>
				                <label class="btn btn-default btn-default-si btn-radio-group">
				                    <input type="radio" id="sindromesGeriatricosFinal-p-5-no" name="sindromesGeriatricos-p-5" value="false" /> No
				                </label>
				                <label class="btn btn-default active btn-radio-group">
				                    <input type="radio" id="sindromesGeriatricosFinal-p-5-null" name="sindromesGeriatricos-p-5" value="" class="radio-ninguno"/> Ninguno
				                </label>
				            </div>
				      	</td>
				      	<td>
				      		<textarea class="form-control" id="sindromesGeriatricosFinal-c-5" name="sindromesGeriatricos-c-5">
				      		</textarea>
				      	</td>
				    </tr>

				    <tr>
				      	<td>
				      		Delirium
				      	</td>
				      	<td>
				        	<div class="btn-group" data-toggle="buttons">
				                <label class="btn btn-default btn-default-si btn-radio-group">
				                    <input type="radio" id="sindromesGeriatricosFinal-p-6-si" name="sindromesGeriatricos-p-6" value="true" /> Si
				                </label>
				                <label class="btn btn-default btn-default-si btn-radio-group">
				                    <input type="radio" id="sindromesGeriatricosFinal-p-6-no" name="sindromesGeriatricos-p-6" value="false" /> No
				                </label>
				                <label class="btn btn-default active btn-radio-group">
				                    <input type="radio" id="sindromesGeriatricosFinal-p-6-null" name="sindromesGeriatricos-p-6" value="" class="radio-ninguno"/> Ninguno
				                </label>
				            </div>
				      	</td>
				      	<td>
				      		<textarea class="form-control" id="sindromesGeriatricosFinal-c-6" name="sindromesGeriatricos-c-6">
				      		</textarea>
				      	</td>
				    </tr>

				    <tr>
				      	<td>
				      		Trastorno del Ánimo
				      	</td>
				      	<td>
				      		<div class="btn-group" data-toggle="buttons">
				                <label class="btn btn-default btn-default-si btn-radio-group">
				                    <input type="radio" id="sindromesGeriatricosFinal-p-7-si" name="sindromesGeriatricos-p-7" value="true" /> Si
				                </label>
				                <label class="btn btn-default btn-default-si btn-radio-group">
				                    <input type="radio" id="sindromesGeriatricosFinal-p-7-no" name="sindromesGeriatricos-p-7" value="false" /> No
				                </label>
				                <label class="btn btn-default active btn-radio-group">
				                    <input type="radio" id="sindromesGeriatricosFinal-p-7-null" name="sindromesGeriatricos-p-7" value="" class="radio-ninguno"/> Ninguno
				                </label>
				            </div>
				      	</td>
				      	<td>
				      		<textarea class="form-control" id="sindromesGeriatricosFinal-c-7" name="sindromesGeriatricos-c-7">
				      		</textarea>
				      	</td>
				    </tr>

				    <tr>
				      	<td>
				      		Trastorno Sueño
				      	</td>
				      	<td>
				        	<div class="btn-group" data-toggle="buttons">
				                <label class="btn btn-default btn-default-si btn-radio-group">
				                    <input type="radio" id="sindromesGeriatricosFinal-p-8-si" name="sindromesGeriatricos-p-8" value="true" /> Si
				                </label>
				                <label class="btn btn-default btn-default-si btn-radio-group">
				                    <input type="radio" id="sindromesGeriatricosFinal-p-8-no" name="sindromesGeriatricos-p-8" value="false" /> No
				                </label>
				                <label class="btn btn-default active btn-radio-group">
				                    <input type="radio" id="sindromesGeriatricosFinal-p-8-null" name="sindromesGeriatricos-p-8" value="" class="radio-ninguno"/> Ninguno
				                </label>
				            </div>
				      	</td>
				      	<td>
				      		<textarea class="form-control" id="sindromesGeriatricosFinal-c-8" name="sindromesGeriatricos-c-8">
				      		</textarea>
				      	</td>
				    </tr>

				    <tr>
				      	<td>
				      		Malnutrición
				      	</td>
				      	<td>
				        	<div class="btn-group" data-toggle="buttons">
				                <label class="btn btn-default btn-default-si btn-radio-group">
				                    <input type="radio" id="sindromesGeriatricosFinal-p-9-si" name="sindromesGeriatricos-p-9" value="true" /> Si
				                </label>
				                <label class="btn btn-default btn-default-si btn-radio-group">
				                    <input type="radio" id="sindromesGeriatricosFinal-p-9-no" name="sindromesGeriatricos-p-9" value="false" /> No
				                </label>
				                <label class="btn btn-default active btn-radio-group">
				                    <input type="radio" id="sindromesGeriatricosFinal-p-9-null" name="sindromesGeriatricos-p-9" value="" class="radio-ninguno"/> Ninguno
				                </label>
				            </div>
				      	</td>
				      	<td>
				      		<textarea class="form-control" id="sindromesGeriatricosFinal-c-9" name="sindromesGeriatricos-c-9">
				      		</textarea>
				      	</td>
				    </tr>

				    <tr>
				      	<td>
				      		Deficit Sensorial
				      	</td>
				      	<td>
				        	<div class="btn-group" data-toggle="buttons">
				                <label class="btn btn-default btn-default-si btn-radio-group">
				                    <input type="radio" id="sindromesGeriatricosFinal-p-10-si" name="sindromesGeriatricos-p-10" value="true" /> Si
				                </label>
				                <label class="btn btn-default btn-default-si btn-radio-group">
				                    <input type="radio" id="sindromesGeriatricosFinal-p-10-no" name="sindromesGeriatricos-p-10" value="false" /> No
				                </label>
				                <label class="btn btn-default active btn-radio-group">
				                    <input type="radio" id="sindromesGeriatricosFinal-p-10-null" name="sindromesGeriatricos-p-10" value="" class="radio-ninguno"/> Ninguno
				                </label>
				            </div>
				      	</td>
				      	<td>
				      		<textarea class="form-control" id="sindromesGeriatricosFinal-c-10" name="sindromesGeriatricos-c-10">
				      		</textarea>
				      	</td>
				    </tr>

				    <tr>
				      	<td>
				      		Inmovilidad
				      	</td>
				      	<td>
				        	<div class="btn-group" data-toggle="buttons">
				                <label class="btn btn-default btn-default-si btn-radio-group">
				                    <input type="radio" id="sindromesGeriatricosFinal-p-11-si" name="sindromesGeriatricos-p-11" value="true" /> Si
				                </label>
				                <label class="btn btn-default btn-default-si btn-radio-group">
				                    <input type="radio" id="sindromesGeriatricosFinal-p-11-no" name="sindromesGeriatricos-p-11" value="false" /> No
				                </label>
				                <label class="btn btn-default active btn-radio-group">
				                    <input type="radio" id="sindromesGeriatricosFinal-p-11-null" name="sindromesGeriatricos-p-11" value="" class="radio-ninguno"/> Ninguno
				                </label>
				            </div>
				      	</td>
				      	<td>
				      		<textarea class="form-control" id="sindromesGeriatricosFinal-c-11" name="sindromesGeriatricos-c-11">
				      		</textarea>
				      	</td>
				    </tr>

				    <tr>
				      	<td>
				      		Úlceras por Presión
				      	</td>
				      	<td>
				        	<div class="btn-group" data-toggle="buttons">
				                <label class="btn btn-default btn-default-si btn-radio-group">
				                    <input type="radio" id="sindromesGeriatricosFinal-p-12-si" name="sindromesGeriatricos-p-12" value="true" /> Si
				                </label>
				                <label class="btn btn-default btn-default-si btn-radio-group">
				                    <input type="radio" id="sindromesGeriatricosFinal-p-12-no" name="sindromesGeriatricos-p-12" value="false" /> No
				                </label>
				                <label class="btn btn-default active btn-radio-group">
				                    <input type="radio" id="sindromesGeriatricosFinal-p-12-null" name="sindromesGeriatricos-p-12" value="" class="radio-ninguno"/> Ninguno
				                </label>
				            </div>
				      	</td>
				      	<td>
				      		<textarea class="form-control" id="sindromesGeriatricosFinal-c-12" name="sindromesGeriatricos-c-12">
				      		</textarea>
				      	</td>
				    </tr>

				</tbody>
			</table>

		<fieldset>

		
		<?php
			$tipoPaciente = App\Models\Usuario::obtenerNombreTipoUsuario(Auth::user()->rut);
			if ($tipoPaciente == "medico")
			{
		?>

		<button type="button" class="btn btn-primary pull-right" onclick="habilitarBoton('formSindromesGeriatricosFinal', 'editarsindromesGeriatricosFinal', 'guardarsindromesGeriatricosFinal')" id="editarsindromesGeriatricosFinal">Editar</button>
	
		<button type="button" class="btn btn-success pull-right" id="imprimirGeriatricosFinal" onclick="imprimirPDF('formSindromesGeriatricosFinal')" >PDF</button>

		<button type="submit" class="btn btn-primary pull-right" id="guardarsindromesGeriatricosFinal" style="display:none;">Guardar</button>
		<?php
			}
		?>
		{{ Form::close() }}
	</div>
</div>
<!--</fieldset>-->

<script type="text/javascript">

$(function(){

	$("#formSindromesGeriatricosFinal").formValidation({
		excluded: ':disabled',
		framework: 'bootstrap',
		fields: {
			"sindromesGeriatricos-fecha-encuesta": {
				validators:{
					/*notEmpty: {
						message: 'El nombre es obligatorio'
					},*/
                    date: {
                        format: 'DD-MM-YYYY',
                        //min: '01/01/2010',
                        //max: '12/30/2020',
                        message: 'La fecha no es válida'
                    }
				}
			}
		}
	}).on('err.field.fv', function(e, data) {
		if (data.fv.getSubmitButton()) data.fv.disableSubmitButtons(false);
	}).on('success.field.fv', function(e, data) {
		if (data.fv.getSubmitButton()) data.fv.disableSubmitButtons(false);
	}).on("success.form.fv", function(evt){
		console.log("--- submit editarEncuestasSindromesGeriatricos ---");

		$("#formSindromesGeriatricosFinal input[type='submit']").prop("disabled", false);

		$("#editarsindromesGeriatricosFinal").click(function(){
			$('#guardarsindromesGeriatricosFinal').prop("disabled", false);
			$('#guardarsindromesGeriatricosFinal').removeClass('disabled');
		});

		evt.preventDefault(evt);
		$("#dvLoading").show();
		var $form = $(evt.target);
		
		var form = $(this).serializeArray();
		form.push({name:"rut", value:"{{ $rut }}"});

		$.ajax({
			url: $form.prop("action"),
			data: form,
			type: "post",
			dataType: "json",
			success: function(data){
				if(data.exito) bootbox.alert("<h4>"+data.exito+"</h4>", function(){
					verDatosPaciente();
					deshabilitarBoton('formSindromesGeriatricosFinal', 'editarsindromesGeriatricosFinal', 'guardarsindromesGeriatricosFinal');
				});
				if(data.error) bootbox.alert("<h4>"+data.error+"</h4>");
				$("#dvLoading").hide();
			},
			error: function(error){
				console.log(error);
				$("#dvLoading").hide();
			}
		});
		return false;
	});


});
</script>






