<script>
$(function(){

		$("#fechai,#fechaf").datetimepicker({
			locale: "es",
			format: "DD-MM-YYYY",
			sideBySide: true,
			stepping:1
		});
		$("#generar-grafico").on("click",function(){
				crearGrafico($("#fechai").val(),$("#fechaf").val(),$("#tiempo").val());
				
		});
		$("#fechai").on("focusout",function(){
			var date = new Date(darVueltaFechaString($(this).val()));
			var newdate = new Date(date);
		
			newdate.setDate(newdate.getDate() + 30);
			
			var dd = newdate.getDate();
			var mm = newdate.getMonth() + 1;
			var y = newdate.getFullYear();
		
			var someFormattedDate = (dd<10?"0"+dd:dd) + '-' + (mm<10?"0"+mm:mm) + '-' + y;
			$("#fechaf").val(someFormattedDate);
		});
	/*	$("#filtro").change(function(){
				var filtro=$("#filtro").val();
			//	if()
		});*/
		$("#configuracion").click(function(){
			$.ajax({
				url: "guardarConfiguracionAccionesRepetidas",
				type: "post",
				data: {rut: "{{ $rut }}",fecha_inicial:$("#fechai").val(),margen:0,frecuencia:$("#tiempo").val()},
				dataType: "json",
				success: function (data) {
					if(data.exito)
						bootbox.alert("<h3>"+data.exito+"</h3>");
				},
				error: function (error) {
					console.log(error);
				}
			});
		});
		
		$("#generador").click(function(){
console.log("comenzando generacion de datos");
			$.ajax({
				url: "generarResumenDatosAccionesRepetidas",
				type: "post",
				data:{rut:"{{$rut}}"},
				dataType: "json",
				success: function (data) {
					console.log(data);
					bootbox.alert(data);
				},
				error: function (error) {
					console.log("error");
					console.log(error);
				}
			});
		});
		
		$.ajax({
			url: "cargarConfiguracionAccionesRepetidas",
			type: "post",
			data: {rut: "{{ $rut }}"},
			dataType: "json",
			success: function (data) {
				if(data.fecha_inicial)
				{
					$("#fechai").val(darVueltaFechaString(data.fecha_inicial));
					$("#fechai").data("DateTimePicker").date(darVueltaFechaString(data.fecha_inicial));
					$("#fechai").trigger("focusout");
					$("#rango").val(data.margen);
					$("#tiempo").val(data.frecuencia);
				}
				else if(data.fecha_estudio)
				{
					$("#fechai").val(darVueltaFechaString(data.fecha_estudio));
					$("#fechai").data("DateTimePicker").date(darVueltaFechaString(data.fecha_estudio));
					$("#fechai").trigger("focusout");
				}
				
			},
			error: function (error) {
				console.log(error);
			}
		});
});
function recargarGraficos(response,filtro)
{
	
	var color1="#f7a35c";
	var color2="#4cd7c8";
	var color3="#9398d8";
	
	var categorias=[];
	var datos1=[];
	var datosSegundos1=[];
	
	var datos2=[];
	var datosSegundos2=[];
	
	var datos3=[];
	var datosSegundos3=[];
	
	
	var frec1=[];
	var frec2=[];
	var frec3=[];
	
	var data=response.datos;
	for(var i=0;i<data.length;i++)
	{
		
		categorias.push(data[i].fecha);
		
		datos1.push(data[i].sensor1.cantidad);
		datosSegundos1.push(data[i].sensor1.segundos/60);
		datos2.push(data[i].sensor2.cantidad);
		datosSegundos2.push(data[i].sensor2.segundos/60);
		datos3.push(data[i].sensor3.cantidad);
		datosSegundos3.push(data[i].sensor3.segundos/60);
		
		frec1.push(data[i].sensor1.frecuencia);
		frec2.push(data[i].sensor2.frecuencia);
		frec3.push(data[i].sensor3.frecuencia);
	}
	
	$('#sensor1').highcharts({
		chart: {
			zoomType: 'xy'
		},
		title: {
			text: 'Tiempo promedio de aperturas, cantidad y frecuencia'
		},
		subtitle: {
			text: response.ubicaciones.s1
		},
		xAxis: [{
			categories: categorias,
			crosshair: true
		}],
		yAxis: [{ 
			title: {
				text: 'Cantidad',
				style: {
					color: color1
				}
			},
			labels: {
				format: '{value} veces',
				style: {
					color: color1
				}
			}
		}, { 
			title: {
				text: 'Frecuencia',
				style: {
					color: color3
				}
			},
			labels: {
				format: '{value} veces',
				style: {
					color: color3
				}
			}
		}],
		tooltip: {
			shared:true
				
			
		},
		legend: {
			layout: 'vertical',
			backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
		},
		series: [{
			name: 'Cantidad de veces que se abrió la puerta',
			type: 'column',
			yAxis: 0,
			data: datos1,
			color: color1

		
		},{
			name: 'Frecuencia de apertura de la puerta cada '+$("#tiempo").val()+' segundos',
			type: 'spline',
			yAxis: 0,
			data: frec1,
			color: color3
		}]
	});
	//sensor2
	$('#sensor2').highcharts({
		chart: {
			zoomType: 'xy'
		},
		title: {
			text: 'Tiempo promedio de aperturas, cantidad y frecuencia'
		},
		subtitle: {
			text: response.ubicaciones.s2
		},
		xAxis: [{
			categories: categorias,
			crosshair: true
		}],
		yAxis: [{ 
			title: {
				text: 'Cantidad',
				style: {
					color: color1
				}
			},
			labels: {
				format: '{value} veces',
				style: {
					color: color1
				}
			}
	
		}, { 
			title: {
				text: 'Frecuencia',
				style: {
					color: color3
				}
			},
			labels: {
				format: '{value} veces',
				style: {
					color: color3
				}
			}
		}],
		tooltip: {
			shared:true
		},
		legend: {
			layout: 'vertical',
			backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
		},
		series: [{
			name: 'Cantidad de veces que se abrió la puerta',
			type: 'column',
			yAxis: 0,
			data: datos2,
			color: color1,
			tooltip: {
				valueSuffix: ' veces'
			}

		
		},{
			name: 'Frecuencia de apertura de la puerta cada '+$("#tiempo").val()+' segundos',
			type: 'spline',
			yAxis: 0,
			data: frec2,
			color: color3,
			tooltip: {
				valueSuffix: ' veces'
			}
		}]
	});
	//sensor3
	$('#sensor3').highcharts({
		chart: {
			zoomType: 'xy'
		},
		title: {
			text: 'Tiempo promedio de aperturas, cantidad y frecuencia'
		},
		subtitle: {
			text: response.ubicaciones.s3
		},
		xAxis: [{
			categories: categorias,
			crosshair: true
		}],
		yAxis: [{ 
			title: {
				text: 'Cantidad',
				style: {
					color: color1
				}
			},
			labels: {
				format: '{value} veces',
				style: {
					color: color1
				}
			}
	
		}, { 
			title: {
				text: 'Frecuencia',
				style: {
					color: color3
				}
			},
			labels: {
				format: '{value} veces',
				style: {
					color: color3
				}
			}
		}],
		tooltip: {
			shared:true
						
		},
		legend: {
			layout: 'vertical',
			backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
		},
		series: [{
			name: 'Cantidad de veces que se abrió la puerta',
			type: 'column',
			yAxis: 0,
			data: datos3,
			color: color1,
			tooltip: {
				valueSuffix: ' veces'
			}

		
		},{
			name: 'Frecuencia de apertura de la puerta cada '+$("#tiempo").val()+' segundos',
			type: 'spline',
			yAxis: 0,
			data: frec3,
			color: color3,
			tooltip: {
				valueSuffix: ' veces'
			}
		}]
	});
	
	var categorias_cantidad=[
				"00:00",
				"01:00",
				"02:00",
				"03:00",
				"04:00",
				"05:00",
				"06:00",
				"07:00",
				"08:00",
				"09:00",
				"10:00",
				"11:00",
				"12:00",
				"13:00",
				"14:00",
				"15:00",
				"16:00",
				"17:00",
				"18:00",
				"19:00",
				"20:00",
				"21:00",
				"22:00",
				"23:00"
	]
	$('#cant_sensor1').highcharts({
		chart: {
			zoomType: 'xy'
		},
		title: {
			text: 'Cantidad de personas que abrieron la puerta'
		},
		subtitle: {
			text: response.ubicaciones.s1
		},
		xAxis: [{
			categories: categorias_cantidad,
			crosshair: true
		}],
		yAxis: [{ 
			title: {
				text: 'Cantidad',
				style: {
					color: color1
				}
			},
			labels: {
				format: '{value} personas',
				style: {
					color: color1
				}
			}
		}],
		tooltip: {
			shared:true
				
		},
		legend: {
			layout: 'vertical',
			backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
		},
		series: [{
			name: 'Cantidad de personas que se abrieron la puerta',
			type: 'spline',
			yAxis: 0,
			data: response.cantidad_aperturas1,
			color: color1

		}]
	});
	
	$('#cant_sensor2').highcharts({
		chart: {
			zoomType: 'xy'
		},
		title: {
			text: 'Cantidad de personas que abrieron la puerta'
		},
		subtitle: {
			text: response.ubicaciones.s2
		},
		xAxis: [{
			categories: categorias_cantidad,
			crosshair: true
		}],
		yAxis: [{ 
			title: {
				text: 'Cantidad',
				style: {
					color: color2
				}
			},
			labels: {
				format: '{value} personas',
				style: {
					color: color2
				}
			}
		}],
		tooltip: {
			shared:true
				
		},
		legend: {
			layout: 'vertical',
			backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
		},
		series: [{
			name: 'Cantidad de personas que se abrieron la puerta',
			type: 'spline',
			yAxis: 0,
			data: response.cantidad_aperturas2,
			color: color2

		}]
	});
	
	$('#cant_sensor3').highcharts({
		chart: {
			zoomType: 'xy'
		},
		title: {
			text: 'Cantidad de personas que abrieron la puerta'
		},
		subtitle: {
			text: response.ubicaciones.s3
		},
		xAxis: [{
			categories: categorias_cantidad,
			crosshair: true
		}],
		yAxis: [{ 
			title: {
				text: 'Cantidad',
				style: {
					color: color3
				}
			},
			labels: {
				format: '{value} personas',
				style: {
					color: color3
				}
			}
		}],
		tooltip: {
			shared:true
				
		},
		legend: {
			layout: 'vertical',
			backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
		},
		series: [{
			name: 'Cantidad de personas que se abrieron la puerta',
			type: 'spline',
			yAxis: 0,
			data: response.cantidad_aperturas3,
			color: color3

		}]
	});
	
	
	
	$("#dias").empty();
	$("#tab_graficos").empty();
	for(var i=0;i<categorias.length;i++)
	{
		var $grafico=$("<div class='tab-pane' id='dia"+(i+1)+"'></div>");
		var $s1=$("<div id='dia"+(i+1)+"_1' style='width:80%'></div>");
		var $s2=$("<div id='dia"+(i+1)+"_2' style='width:80%'></div>");
		var $s3=$("<div id='dia"+(i+1)+"_3' style='width:80%'></div>");
		var $h3=$("<h3 style='text-align:center;'>"+categorias[i]+"</h3>");
		$("#dias").append($grafico);
		var $li=$("<li><a href='#dia"+(i+1)+"' data-toggle='tab' >Día "+(i+1)+"</a></li>");
		$("#tab_graficos").append($li);
		
		$grafico.append($h3);
		$grafico.append($s1);
		$grafico.append($s2);
		$grafico.append($s3);
		
		
		$s1.highcharts('StockChart', {
			chart: {
				zoomType: 'x'
			},

			rangeSelector: {

			   buttons: [{
							type: 'second',
							count: 30,
							text: '30s'
						}, {
							type: 'minute',
							count: 1,
							text: '1m'
						}, {
							type: 'minute',
							count: 10,
							text: '10m'
						}, {
							type: 'minute',
							count: 30,
							text: '30m'
						}, {
							type: 'minute',
							count: 60,
							text: '1h'
						}, {
							type: 'all',
							text: 'Todo'
						}],
				selected: 5,
				inputEnabled:false
			},
			title: {
				text: 'Datos'
			},
			subtitle: {
				text: response.ubicaciones.s1
			},
			xAxis: [{
				labels: {
					overflow: 'justify'
				},
				ordinal:false
			}],
			yAxis: [{ // Primary yAxis
				labels: {
					format: '{value}',
					style: {
						color: color1
					}
				},
				title: {
					text: 'Abierto/Cerrado',
					style: {
						color: color1
					}
				}
			
			}],
			tooltip: {
				shared:true
		
			},
			legend: {
				layout: 'vertical',
				backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
			},
			series: [{
				type:'line',
				lineWidth:3,
				name: 'asd',
				data: data[i].sensor1.acumulado,
				step:true,
				color: color1
			}],
			plotOptions: {
				area: {
					pointInterval: 3600000
					
				}
			}
		});
		
		
		$s2.highcharts('StockChart', {
			chart: {
				zoomType: 'x'
			},

			rangeSelector: {

			   buttons: [{
							type: 'second',
							count: 30,
							text: '30s'
						}, {
							type: 'minute',
							count: 1,
							text: '1m'
						}, {
							type: 'minute',
							count: 10,
							text: '10m'
						}, {
							type: 'minute',
							count: 30,
							text: '30m'
						}, {
							type: 'minute',
							count: 60,
							text: '1h'
						}, {
							type: 'all',
							text: 'Todo'
						}],
				selected: 5,
				inputEnabled:false
			},
			title: {
				text: 'Datos'
			},
			subtitle: {
				text: response.ubicaciones.s2
			},
			xAxis: [{
				labels: {
					overflow: 'justify'
				},
				ordinal:false
			}],
			yAxis: [{ // Primary yAxis
				labels: {
					format: '{value}',
					style: {
						color: color2
					}
				},
				title: {
					text: 'Abierto/Cerrado',
					style: {
						color: color2
					}
				}
			
			}],
			tooltip: {
				shared:true
			},
			legend: {
				layout: 'vertical',
				backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
			},
			series: [{
				type:'line',
				lineWidth:2,
				name: 'asd',
				data: data[i].sensor2.acumulado,
				step:true,
				color: color2
			}],
			plotOptions: {
				area: {
					pointInterval: 3600000
					
				}
			}
		});
		
		
		$s3.highcharts('StockChart', {
			chart: {
				zoomType: 'x'
			},

			rangeSelector: {

			   buttons: [{
							type: 'second',
							count: 30,
							text: '30s'
						}, {
							type: 'minute',
							count: 1,
							text: '1m'
						}, {
							type: 'minute',
							count: 10,
							text: '10m'
						}, {
							type: 'minute',
							count: 30,
							text: '30m'
						}, {
							type: 'minute',
							count: 60,
							text: '1h'
						}, {
							type: 'all',
							text: 'Todo'
						}],
				selected: 5,
				inputEnabled:false
			},
			title: {
				text: 'Datos'
			},
			subtitle: {
				text: response.ubicaciones.s3
			},
			xAxis: [{
				labels: {
					overflow: 'justify'
				},
				ordinal:false
			}],
			yAxis: [{ // Primary yAxis
				labels: {
					format: '{value}',
					style: {
						color: color3
					}
				},
				title: {
					text: 'Abierto/Cerrado',
					style: {
						color: color3
					}
				}
			
			}],
			tooltip: {
				shared:true
			},
			legend: {
				layout: 'vertical',
				backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
			},
			series: [{
				type:'line',
				lineWidth:2,
				name: 'asd',
				data: data[i].sensor3.acumulado,
				step:true,
				color: color3
			}],
			plotOptions: {
				area: {
					pointInterval: 3600000
					
				}
			}
		});
		
	}
	
}
var crearGrafico = function (fechai,fechaf,tiempo) {
	if(fechai==""||fechaf==""||tiempo=="")
	{
		bootbox.alert("<h3>Debe ingresar todos los datos</h3>");
		return;
	}
    $.ajax({
        url: "generarEstadisticasAccionesRepetidas",
        type: "post",
        data: {rut: "{{ $rut }}", fecha_inicial:darVueltaFechaString(fechai), fecha_final:darVueltaFechaString(fechaf),tiempo:tiempo},
        dataType: "json",
        beforeSend:function(){
        	$("#dvLoading").show();
        },
        success: function (data) {
        	recargarGraficos(data,$("#filtro").val());
        	$("#tab_graficos a").first().tab("show");
			$("#dvLoading").hide();
			inicializarNavegador();
        	
        },
        error: function (error) {
        	console.log("error");
            console.log(error);
        }
    });
};
var actual=1;

function inicializarNavegador()
{
	
	var cantidad_a_mostrar=calcularCantidad();
	
	$("#tab_graficos").find("li").each(function(indice,elemento){
			if(indice>=cantidad_a_mostrar)
				$(elemento).hide();
	});
	$("#atras-btn,#adelante-btn").css("visibility","visible");
	
	$("#atras-btn").off("click").click(function()
		{
			$("#tab_graficos").find("li.active").prev().show();
			$("#tab_graficos").find("li.active").prev().find("a").tab("show");
			
			actual=$("#tab_graficos li.active").index()+1;

			if(actual<1)
				actual=1;

			if($("#tab_graficos li:visible").index()+1==actual)
			{
				if((actual>calcularCantidad())||$("#tab_graficos li:visible").length>calcularCantidad())
				{
					$("#tab_graficos li:visible").last().hide();
					$("#tab_graficos li:visible").first().prev().show();
				}
			}
			
		});
	$("#adelante-btn").off("click").click(function()
		{
			$("#tab_graficos").find("li.active").next().show();
			$("#tab_graficos").find("li.active").next().find("a").tab("show");
			
			actual=$("#tab_graficos li.active").index()+1;

			if(actual>$("#tab_graficos li").length)
				actual=$("#tab_graficos li").length;
			if((actual>calcularCantidad())&&$("#tab_graficos li:visible").length>calcularCantidad())
				$("#tab_graficos li:visible").first().hide();
		});
	
	
	
}
function calcularCantidad()
{
	var largoLista=parseInt($("#tab_graficos").css("width"));
	
	var $lis=$("#tab_graficos").find("li");
	var maximo=0;
	$lis.each(function(indice,elemento){
			maximo=Math.max(parseInt($(elemento).css("width")),maximo);
	});

	return Math.round(largoLista/maximo)-1;
}
</script>
<div>
	<div class="row">
		<div class="form-inline">
			<div class="form-group">
				<label for="fechai" class="col-sm-4 control-label">Desde: </label>
				<div class="col-sm-8">
					<input class="form-control" type="text" id="fechai" name="fechai">
				</div>
			</div>
			<div class="form-group">
				<label for="fechaf" class="col-sm-4 control-label">Hasta: </label>
				<div class="col-sm-8">
					<input class="form-control" type="text" id="fechaf" name="fechaf" disabled>
				</div>
			</div>
			<div class="form-group">
				<label for="tiempo" class="col-sm-4 control-label">Frecuencia de apertura (en segundos): </label>
				<div class="col-sm-8">
					<input class="form-control" type="text" id="tiempo" name="tiempo" value="3600">
				</div>
			</div>
		<!--	<div class="form-group">
				<label for="filtro" class="col-sm-4 control-label">Filtro: </label>
				<div class="col-sm-8">
					<select class="form-control" type="text" id="filtro" name="filtro" value="todos">
						<option value="todos">Todos</option>	
						<option value="nadie">Nadie</option>
						<option value="objetivo1">Objetivo 1</option>
						<option value="no_objetivo">No objetivo</option>
						<option value="objetivo2">Objetivo 2</option>
					</select>
				</div>
			</div>-->
			
		</div>
	</div>
	<div class="row">
		<div class="form-group">
			<div class="col-sm-2">
				<input class="btn btn-primary" type="button" id="generar-grafico" value="Generar gráficos">
			</div>
			<div class="col-sm-3">
				<input class="btn btn-primary" type="button" id="configuracion" value="Guardar esta configuración">
			</div>
		</div>
	</div>
	<div class="row">
		<div id="sensor1" style="width:100%">
		</div>
		<div id="sensor2" style="width:100%">
		</div>
		<div id="sensor3" style="width:100%">
		</div>
		<div id="cant_sensor1" style="width:100%">
		</div>
		<div id="cant_sensor2" style="width:100%">
		</div>
		<div id="cant_sensor3" style="width:100%">
		</div>
		<div class="row">
			<div class="form-inline">
				<div class="form-group col-sm-1" >
					<a id="atras-btn" class="btn btn-primary" style="visibility:hidden;display:block;">&lt;</a>
				</div>
				<div class="form-group col-sm-10" >
					<ul class="nav nav-tabs" id="tab_graficos">
					</ul>
				</div>
				<div class="form-group col-sm-1" >
					<a id="adelante-btn" class="btn btn-primary" style="visibility:hidden;display:block;">&gt;</a>
				</div>
			</div>
		</div>
		
		@if(Auth::user()->rut==14428752)
		<button disabled id="generador" class="btn btn-primary" type="button">Generador</button>
		@endif
		<div id="dias" class="tab-content" >	
		</div>
		
	</div>
</div>
