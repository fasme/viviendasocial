<fieldset>
	<legend style="font-size: 16px;">Contactos del paciente</legend>
	<br>
	<table id="contactos" class="table table-bordered table-striped">
		<thead>
			<tr>
				<th>Prioridad</th>
				<th>Nombre</th>
				<th>Parentesco</th>
				<th>Teléfono</th>
				<th>Opciones</th>
			</tr>
		</thead>
		<tbody></tbody>
		@if(App\Models\Usuario::obtenerTipoUsuario(Auth::user()->rut) == 3) 
		<tfoot>
			<tr>

				<td colspan="7">
					<button class="btn btn-primary" data-toggle="modal" title="Editar lista de contactos" onclick="agregarContactos('{{$rut}}','1')"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Agregar contacto</button>

					<!-- <button class="btn btn-primary" data-toggle="modal" title="Editar lista de contactos" onclick="agregarContactos('{{$rut}}','2')"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Lista de Médicos</button> -->
				</td>
			</tr>
		</tfoot>
		@endif 
	</table>
</fieldset>


<div class="modal fade" id="modalAgregarContactoMedico" tabindex="-1" role="dialog" aria-labelledby="myModalLabelAgregarContactoMedico" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabelAgregarContactoMedico">Agregar contactos</h4>
			</div>
			{{ Form::open(array('url' => 'contactos/agregarContacto', 'method' => 'post', 'class' => 'form-horizontal', 'role' => 'form', 'id' => 'formAgregarContactoMedico')) }}
			
			<div class="modal-body" style="min-height: 100px;">
				<div class="form-group error">
					<label for="rut" class="col-sm-3 control-label">Buscar por RUT o Nombre de usuario: </label>
					<div class="col-sm-9">
						<div class="input-group">
							{{Form::text('rut', null, array('id' => 'buscar-rut-medico', 'class' => 'form-control', 'autofocus'))}}
							<span class="input-group-btn">
								<button type="button" class="btn btn-primary" title="Agregar a lista de contactos" id="agregarALista_btnMedico" style="top: -2px;" onclick='agregarContactoALista("#agregarALista_btnMedico","#rut_hdnMedico", "#dv_hdnMedico", "#nombre_hdnMedico", "#buscar-rut-medico", "#listaContactosMedico", "médico");'><span class="glyphicon glyphicon-plus"></span> A lista</button>
							</span> 
							{{ Form::hidden('rut_hdn', 'null', array('id' => 'rut_hdnMedico')) }}
							{{ Form::hidden('dv_hdn', 'null', array('id' => 'dv_hdnMedico')) }}
							{{ Form::hidden('nombre_hdn', 'null', array('id' => 'nombre_hdnMedico')) }}
							{{ Form::hidden('rutUsuario_hdn', 'null', array('id' => 'rutUsuario_hdnMedico')) }}
						</div>
					</div>
				</div> 

			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary">Guardar lista de contactos</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
			</div>
			{{ Form::close() }}
		</div>
	</div>
</div>




<div class="modal fade" id="modalAgregarContactoFamiliar" tabindex="-1" role="dialog" aria-labelledby="myModalLabelAgregarContactoFamiliar" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabelAgregarContactoFamiliar">Agregar contactos</h4>
			</div >
				{{ Form::open(array('url' => 'contactos/agregarContacto', 'method' => 'post', 'class' => 'form-horizontal', 'role' => 'form', 'id' => 'formAgregarContactoFamiliar')) }}
			
			<div class="modal-body" style="min-height: 100px;" id = "modalBodyContacto">
				<div class="form-group error">
					<label for="rut" class="col-sm-3 control-label">Buscar por RUT o Nombre de usuario: </label>
					<div class="col-sm-9">
						<div class="input-group">
							{{Form::text('rut', null, array('id' => 'buscar-rut-familiar', 'class' => 'form-control', 'autofocus', 'placeholder' => 'Buscar', 'onkeydown' => 'probar()'))}}
							{{ Form::hidden('rutPaciente', 'null', array('id' => 'rutPaciente')) }}
							
						</div>
					</div>

					
					
				</div> 

				<div class="form-group error" style="margin-bottom: 0px;">
					<label for="paciente-rut" class="col-sm-3 ontrol-label">RUT</label>	
				</div>

				<div class="form-group error">
					<div class="col-sm-5">
						{{Form::text("agregarPacienteRut", null, array("id" => "agregarPacienteRut", "class" => "form-control", "readonly"))}}	
					</div>
				</div>
				
				<div class="form-group error" style="margin-bottom: 0px;">
					<label for="paciente-nombre" class="col-sm-4 ontrol-label">Nombre:</label>	
					<label for="paciente-primerApellido" class="col-sm-4 ontrol-label">Primer Apellido:</label>		
					<label for="paciente-segundoApellido" class="col-sm-4 ontrol-label">Segundo Apellido:</label>				
					
				</div>

				<div class="form-group error">
					
					<div class="col-sm-4">
						{{Form::text("nombres", null, array("id" => "agregarPacienteNombre", "class" => "form-control", "readonly"))}}	
					</div>
					<div class="col-sm-4">
						{{Form::text("apellidoP", null, array("id" => "agregarPacienteApellidoP", "class" => "form-control", "readonly"))}}	
					</div>	
					<div class="col-sm-4">
						{{Form::text("apellidoM", null, array("id" => "agregarPacienteApellidoM", "class" => "form-control", "readonly"))}}	
					</div>
				</div>

				<div class="form-group error" style="margin-bottom: 0px;">
					<label for="paciente-parentesco" class="col-sm-4 ontrol-label">Parentesco:</label>	
					<label for="paciente-correo" class="col-sm-4 ontrol-label">Correo:</label>					
					
				</div>

				<div class="form-group error">
					
					<div class="col-sm-4">
						{{Form::select("parentesco", $parentesco, null, array("id" => "agregarPacienteParentesco", "class" => "form-control"))}}	
					</div>
					<div class="col-sm-4">
						{{Form::text("correo", null, array("id" => "agregarPacienteCorreo", "class" => "form-control")) }}	
					</div>	
				</div>

				<div class="form-group error" >
					<button type="button" class="col-sm-4 control-label btn btn-primary" style="    margin-left: 15px;" onclick="ingresarNumero()">Ingresar número telefonico</button>	
				</div>

				<div id = "telefonos">
					
				</div>



			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary">Guardar lista de contactos</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
			</div>
			{{ Form::close() }}
		</div>
	</div>
</div>