<!--<fieldset>
	<legend class="negrita" style="font-size: 16px;">Timed Get Up and Go Test Inicial</legend>-->
<div style="text-align: left;">
		
	<div class="form">
		{{ Form::open(array('url' => 'paciente/mmse', 'method' => 'post', 'role' => 'form', 'id' => 'formMmseInicial')) }}
		<div>
			<input name="inicio" value="true" hidden/>
			<input name="tipo-encuesta" value="mmse" hidden/>
		</div>

		<h4>Mini mental status examination (FOLSTEIN)</h4>

		<div class="row">
			<div class="col-sm-6">
				<div class="form-group ">
					<label class="control-label" style="width:100%;">Fecha</label>
					<input id="EvaluacionMmseInicial-fecha-encuesta" name="fecha_test" class="form-control fecha2" type="text"/>	
				</div>
			</div>
		</div>
		
		<fieldset>
			
	                
			<table class="table table-bordered" id="tbl_1">
				<tbody>
					<tr>
				      	<td colspan="1">
				      		Total:
				      	</td>
				      	<td>
				      		<input type="number" min="0" name="EvaluacionMmse-total" class="form-control EvaluacionMmseInicial-total fecha2"  readonly >
				      	</td>
				    </tr>
				</tbody>
				<tbody>
					<tr>
					    <td colspan="2"><b>1.Orientación.</b></td>
					</tr>
					<tr>
					    <td colspan="2">Temporal:</td>
					</tr>
				    <tr>
				      	<td style="width:80%;">
					    <p>¿En qué día estamos? (fecha)</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="EvaluacionMmseInicial-p-1" name="p-1" value="true" valor="1"/> Bien
				                <input type="radio" id="EvaluacionMmseInicial-p-1" name="p-1" value="false" valor="0"/> Mal
				            </label>
				      	</td>
				      
				    </tr>
                    <tr>
				      	<td>
					    <p>¿En qué mes?</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				           		<input type="radio" id="EvaluacionMmseInicial-p-2" name="p-2" value="true" valor="1"/> Bien
				                <input type="radio" id="EvaluacionMmseInicial-p-2" name="p-2" value="false" valor="0"/> Mal

				            </label>
				      	</td>
				      	
				    </tr>
                    <tr>
				      	<td>
					    <p>¿En qué día de la semana?</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
						        <input type="radio" id="EvaluacionMmseInicial-p-3" name="p-3" value="true" valor="1"/> Bien
				                <input type="radio" id="EvaluacionMmseInicial-p-3" name="p-3" value="false" valor="0"/> Mal
				            </label>
				      	</td>
				      
				    </tr>
                    <tr>
				      	<td>
					    <p>¿En qué año estamos?</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="EvaluacionMmseInicial-p-4" name="p-4" value="true" valor="1"/> Bien
				                <input type="radio" id="EvaluacionMmseInicial-p-4" name="p-4" value="false" valor="0"/> Mal

				            </label>
				      	</td>
				      	
				    </tr>
				    <tr>
				      	<td>
					    <p>¿En qué estación?</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="EvaluacionMmseInicial-p-5" name="p-5" value="true" valor="1"/> Bien
				                <input type="radio" id="EvaluacionMmseInicial-p-5" name="p-5" value="false" valor="0"/> Mal

				            </label>
				      	</td>
				      	
				    </tr>



				      


				      <tr>
				      		<td colspan="2">Espacial:</td>
				      </tr>

				      <tr>
				      	<td>
				      		<p/>¿En qué hospital (o lugar) estamos?</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				      			<input type="radio" id="EvaluacionMmseInicial-p-6" name="p-6" value="true" valor="1"> Bien
				      			<input type="radio" id="EvaluacionMmseInicial-p-6" name="p-6" value="false" valor="0"> Mal
				      	</td>
				      </tr>

				      <tr>
				      	<td>
				      		<p/>¿En qué piso (o planta, sala, servicio)?</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				      			<input type="radio" id="EvaluacionMmseInicial-p-7" name="p-7" value="true" valor="1"> Bien
				      			<input type="radio" id="EvaluacionMmseInicial-p-7" name="p-7" value="false" valor="0"> Mal
				      	</td>
				      </tr>


				      <tr>
				      	<td>
				      		<p/>¿En qué pueblo (ciudad)?</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				      			<input type="radio" id="EvaluacionMmseInicial-p-8" name="p-8" value="true" valor="1"> Bien
				      			<input type="radio" id="EvaluacionMmseInicial-p-8" name="p-8" value="false" valor="0"> Mal
				      	</td>
				      </tr>

				      <tr>
				      	<td>
				      		<p/>¿En qué provincia estamos?</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				      			<input type="radio" id="EvaluacionMmseInicial-p-9" name="p-9" value="true" valor="1"> Bien
				      			<input type="radio" id="EvaluacionMmseInicial-p-9" name="p-9" value="false" valor="0"> Mal
				      	</td>
				      </tr>


				      <tr>
				      	<td>
				      		<p/>¿En qué país (o nación, autonomía)?</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				      			<input type="radio" id="EvaluacionMmseInicial-p-10" name="p-10" value="true" valor="1"> Bien
				      			<input type="radio" id="EvaluacionMmseInicial-p-10" name="p-10" value="false" valor="0"> Mal
				      	</td>
				      </tr>

				</tbody>
				    
			</table>
                        
            <table class="table table-bordered" id="tbl_2">
				<tbody>
					<tr>
					    <td colspan="2"><b>2.Repetición inmediata
					    </b></td>
					</tr>
					<tr>
						<td colspan="2">
						Nombre tres palabras Árbol-Mesa-Perro a razón de 1 por segundo. Luego se pide al paciente que las repita. Esta primera repetición otorga la puntuación. Otorgue 1 punto por cada palabra correcta, pero continúe diciéndolas hasta que el sujero repita las 3, hasta un máximo de 6 veces.
						</td>
					</tr>
					<tr>
				      	<td style="width:80%;">
					    <p>Árbol</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="EvaluacionMmseInicial-p-11" name="p-11" value="true" valor="1"/> Correcta
				                <input type="radio" id="EvaluacionMmseInicial-p-11" name="p-11" value="false" valor="0"/> No sabe
				            </label>
				      	</td>
				      
				    </tr>
                    <tr>
				      	<td>
					    <p>Mesa</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="EvaluacionMmseInicial-p-12" name="p-12" value="true" valor="1"/> Correcta
				                <input type="radio" id="EvaluacionMmseInicial-p-12" name="p-12" value="false" valor="0"/> No sabe
				            </label>
				      	</td>
				      	
				    </tr>
                    <tr>
				      	<td>
					    <p>Perro</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="EvaluacionMmseInicial-p-13" name="p-13" value="true" valor="1"/> Correcta
				                <input type="radio" id="EvaluacionMmseInicial-p-13" name="p-13" value="false" valor="0"/> No sabe
				            </label>
				      	</td>
				      
				    </tr>
                    <tr>
				      	<td>
					    <p>Numero de repeticiones</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="number" value="0" id="EvaluacionMmseInicial-repeticion1" name="repeticion1"/>
				            </label>
				      	</td>
				      	
				    </tr>
				      

				</tbody>
				    
			</table>

                        
            <table class="table table-bordered" id="tbl_3">
				<tbody>
					<tr>
					    <td colspan="2"><b>3.&nbspAtención y cálculo: </b></td>
					</tr>
					<tr>
						<td colspan="2">Deletrear MUNDO al revés</td>
					</tr>
					<tr>
				      	
				      	<td>

					      	<table class="table table-bordered " id="tbl_3">
						      	<tr align="center">
						      		<td>Respuesta </td>
						      		<td><label class="btn btn-default btn-radio-group">
				                <input type="radio" id="EvaluacionMmseInicial-p-14" name="p-14" value="true" valor="1"/> Correcta
				                <input type="radio" id="EvaluacionMmseInicial-p-14" name="p-14" value="false" valor="0"/> No sabe
				            </label></td>
						      		<td>
						      			<label class="btn btn-default btn-radio-group">
						                <input type="radio" id="EvaluacionMmseInicial-p-15" name="p-15" value="true" valor="1"/> Correcta
						                <input type="radio" id="EvaluacionMmseInicial-p-15" name="p-15" value="false" valor="0"/> No sabe
						           		 </label>
						      		</td>
						      		<td><label class="btn btn-default btn-radio-group">
						                <input type="radio" id="EvaluacionMmseInicial-p-16" name="p-16" value="true" valor="1"/> Correcta
						                <input type="radio" id="EvaluacionMmseInicial-p-16" name="p-16" value="false" valor="0"/> No sabe
						           		 </label></td>
						      		<td><label class="btn btn-default btn-radio-group">
						                <input type="radio" id="EvaluacionMmseInicial-p-17" name="p-17" value="true" valor="1"/> Correcta
						                <input type="radio" id="EvaluacionMmseInicial-p-17" name="p-17" value="false" valor="0"/> No sabe
						           		 </label></td>
						      		<td><label class="btn btn-default btn-radio-group">
						                <input type="radio" id="EvaluacionMmseInicial-p-18" name="p-18" value="true" valor="1"/> Correcta
						                <input type="radio" id="EvaluacionMmseInicial-p-18" name="p-18" value="false" valor="0"/> No sabe
						           		 </label></td>
						      	</tr>
						      	<tr align="center">
						      		<td>Respuesta correcta</td><td>O</td><td>D</td><td>N</td><td>U</td><td>M</td>
						      	</tr>
					      	</table>
		
				      	</td>
				    </tr>
				</tbody>
				    
			</table>
                        
                        
                        
            <!-- solo mujer -->
            <table class="table table-bordered" id="tbl_4">
				<tbody>
					<tr>
					    <td colspan="2"><b>4.&nbspMemoria.</b></td>
					</tr>
					<tr>
					    <td colspan="2">Pedir que repita las 3 palabras previas, dar 1 punto por cada respuesta correcta.</td>
					</tr>   
					
					<tr>
				      	<td style="width:80%;">
					    <p>Árbol</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="EvaluacionMmseInicial-p-19" name="p-19" value="true" valor="1"/> Correcta
				                <input type="radio" id="EvaluacionMmseInicial-p-19" name="p-19" value="false" valor="0"/> No sabe
				            </label>
				      	</td>
				      
				    </tr>
                    <tr>
				      	<td>
					    <p>Mesa</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="EvaluacionMmseInicial-p-20" name="p-20" value="true" valor="1"/> Correcta
				                <input type="radio" id="EvaluacionMmseInicial-p-20" name="p-20" value="false" valor="0"/> No sabe
				            </label>
				      	</td>
				      	
				    </tr>
                    <tr>
				      	<td>
					    <p>Perro</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="EvaluacionMmseInicial-p-21" name="p-21" value="true" valor="1"/> Correcta
				                <input type="radio" id="EvaluacionMmseInicial-p-21" name="p-21" value="false" valor="0"/> No sabe
				            </label>
				      	</td>
				      
				    </tr>
                  


				</tbody>
				    
			</table>
                        
            <table class="table table-bordered" id="tbl_5">
				<tbody>
					<tr>
					    <td colspan="2"><b>5.&nbspLenguaje.</b></td>
					</tr>
				

				</tbody>
				    
					<tr>
				      	<td style="width:80%;">
					    <p>DENOMINACIÓN. Mostrarle un lápiz o un boligrafo y preguntar ¿Qué es?</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="EvaluacionMmseInicial-p-22" name="p-22" value="true" valor="1"/> Correcto
				                <input type="radio" id="EvaluacionMmseInicial-p-22" name="p-22" value="false" valor="0"/> Incorrecto
				               
				            </label>
				      	</td>
				      
				    </tr>

				    <tr>
				      	<td style="width:80%;">
					    <p>Mostrarle un reloj de pulsera y preguntar ¿Qué es?</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="EvaluacionMmseInicial-p-23" name="p-23" value="true" valor="1"/> Correcto
				                <input type="radio" id="EvaluacionMmseInicial-p-23" name="p-23" value="false" valor="0"/> Incorrecto
				               
				            </label>
				      	</td>
				      
				    </tr>


				    <tr>
				      	<td style="width:80%;">
					    <p>REPETICIÓN. Pedirle que repita la frase: "ni sí, ni no, ni pero" (o "En un trigal había 5 perros")</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="EvaluacionMmseInicial-p-24" name="p-24" value="true" valor="1"/> Correcto
				                <input type="radio" id="EvaluacionMmseInicial-p-24" name="p-24" value="false" valor="0"/> Incorrecto
				               
				            </label>
				      	</td>
				      
				    </tr>

				    <tr>
				      	<td style="width:80%;">
					    <p>ORDENES. Pedirle que siga la orden: "coja un papel con la mano derecha"</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="EvaluacionMmseInicial-p-25" name="p-25" value="true" valor="1"/> Correcto
				                <input type="radio" id="EvaluacionMmseInicial-p-25" name="p-25" value="false" valor="0"/> Incorrecto
				               
				            </label>
				      	</td>
				      
				    </tr>

				    <tr>
				      	<td style="width:80%;">
					    <p>Dóblelo por la mitad</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="EvaluacionMmseInicial-p-26" name="p-26" value="true" valor="1"/> Correcto
				                <input type="radio" id="EvaluacionMmseInicial-p-26" name="p-26" value="false" valor="0"/> Incorrecto
				               
				            </label>
				      	</td>
				      
				    </tr>

				    <tr>
				      	<td style="width:80%;">
					    <p>Y póngalo en el suelo</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="EvaluacionMmseInicial-p-27" name="p-27" value="true" valor="1"/> Correcto
				                <input type="radio" id="EvaluacionMmseInicial-p-27" name="p-27" value="false" valor="0"/> Incorrecto
				               
				            </label>
				      	</td>
				      
				    </tr>

				    <tr>
				      	<td style="width:80%;">
					    <p>LECTURA. Escriba legiblemente en un papel "Cierre los ojos". Pídale que lo lea y haga lo que dice la frase</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="EvaluacionMmseInicial-p-28" name="p-28" value="true" valor="1"/> Correcto
				                <input type="radio" id="EvaluacionMmseInicial-p-28" name="p-28" value="false" valor="0"/> Incorrecto
				               
				            </label>
				      	</td>
				      
				    </tr>

				    <tr>
				      	<td style="width:80%;">
					    <p>ESCRITURA. Que escriba una frase (con sujeto y predicado)</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="EvaluacionMmseInicial-p-29" name="p-29" value="true" valor="1"/> Correcto
				                <input type="radio" id="EvaluacionMmseInicial-p-29" name="p-29" value="false" valor="0"/> Incorrecto
				               
				            </label>
				      	</td>
				      
				    </tr>

				   
                   
			</table>
                        
            <!-- fin tareas mujer -->
                        
            <table class="table table-bordered" id="tbl_6">
				<tbody>
					<tr>
					    <td colspan="2"><b>6.&nbsp COPIA. Dibuje 2 pentágonos intersectados y pida al sujeto que los copie tal cual. Para otorgar un punto deben estar presentes los 10 ángulos y la intersección</b></td>
					</tr>      
					<tr>
					    <td colspan="2">.<br>
					     {{ HTML::image('images/mmse.jpg', null, ['class' => 'img-responsive center-block']) }}<br>
					     </td>
					</tr>                                
                    <tr>
				      	<td>
					    <p>Evalue el dibujo realizado</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="EvaluacionMmseInicial-p-30" name="p-30" value="true" valor="1"/> Correcto
				                <input type="radio" id="EvaluacionMmseInicial-p-30" name="p-30" value="false" valor="0"/> Incorrecto
				         	</label>
				      	</td>
				      	
				    </tr>
                                    
				</tbody>
				  
				<tfoot>
					<tr>
				      	<td colspan="1">
				      		Total:
				      	</td>
				      	<td>
				      		<input style="width:115px;" id="EvaluacionMmseInicial-total" type="number" min="0" name="EvaluacionMmse-total" class="form-control EvaluacionMmseInicial-total"  readonly >
				      	</td>
				    </tr>
					<tr>
						<td colspan="2">
						<button type="button" class="btn btn-primary pull-center" onclick="sumarMmse('EvaluacionMmseInicial-p-','EvaluacionMmseInicial-total','mensaje-resultado-EvaluacionMmse', 'formMmseInicial');" id="ecalcularMmseInicial" >Calcular</button> <span class="mensaje-resultado-EvaluacionCognitivaInicial"></span>
						</td>
					</tr>
				</tfoot>
			</table>
                    
		<fieldset>
		<?php
			$tipoPaciente = App\Models\Usuario::obtenerNombreTipoUsuario(Auth::user()->rut);
			if ($tipoPaciente == "medico")
			{
		?>
		
		<button type="button" class="btn btn-primary pull-right" onclick="habilitarBoton('formMmseInicial', 'editarMmseInicial', 'guardarMmseInicial')" id="editarMmseInicial">Editar</button>

		<button type="button" class="btn btn-success pull-right" id="imprimirrCharlsonInicial" onclick="imprimirPDF('formMmseInicial')" >PDF</button>

		<button type="submit" class="btn btn-primary pull-right" id="guardarMmseInicial" style="display:none;">Guardar</button>
		<!--<button type="button" class="btn btn-success pull-right" id="imprimirLawtonBrodyInical" onclick="imprimirPDF('formEvaluacionCognitivaInicial')" >PDF</button>-->
		<?php
			}
		?>
		{{ Form::close() }}
	</div>
</div>
<!--</fieldset>-->

<script type="text/javascript">


$(function(){
	console.log("estoy en las funcion previa a sumar");
    sumarMmse('EvaluacionMmseInicial-p-','EvaluacionMmseInicial-total','mensaje-resultado-EvaluacionMmse', 'formMmseInicial');
    
    function obtenerGenero(genero){
    	console.log("El genero es :" + genero);        
    }

    $('#EvaluacionMmseInicial-fecha-encuesta').prop("disabled", true);

    
	$("#formMmseInicial").formValidation({
		excluded: ':disabled',
		framework: 'bootstrap',
		fields: {
			"fecha_test": {
				validators:{
					/*notEmpty: {
						message: 'El nombre es obligatorio'
					},*/
                    date: {
                        format: 'DD-MM-YYYY',
                        //min: '01/01/2010',
                        //max: '12/30/2020',
                        message: 'La fecha no es válida'
                    }
				}
			}

                        
		}
	}).on('err.field.fv', function(e, data) {
		if (data.fv.getSubmitButton()) data.fv.disableSubmitButtons(false);
	}).on('success.field.fv', function(e, data) {
		if (data.fv.getSubmitButton()) data.fv.disableSubmitButtons(false);
	}).on("success.form.fv", function(evt){
		console.log("--- submit guardarMmseInicial ---");

		

		$("#formEvaluacionCognitivaInicial input[type='submit']").prop("disabled", false);
		$('#EvaluacionCognitiva-fecha-encuesta').prop("disabled", false);
		evt.preventDefault(evt);
		$("#dvLoading").show();
		var $form = $(evt.target);
		
		var form = $(this).serializeArray();
		form.push({name:"rut", value:"{{ $rut }}"});

		$.ajax({
			url: $form.prop("action"),
			data: form,
			type: "post",
			dataType: "json",
			success: function(data){
				if(data.exito) bootbox.alert("<h4>"+data.exito+"</h4>", function(){
					verDatosPaciente();
					deshabilitarBoton('formMmseInicial', 'editarMmseInicial', 'guardarMmseInicial');
				});
				if(data.error) bootbox.alert("<h4>"+data.error+"</h4>");
				$("#dvLoading").hide();
			},
			error: function(error){
				console.log(error);
				$("#dvLoading").hide();
			}
		});
		return false;
		
	});


});
</script>






