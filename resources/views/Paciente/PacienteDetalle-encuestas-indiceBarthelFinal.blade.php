<!--<fieldset>
	<legend class="negrita" style="font-size: 16px;">Final</legend>-->
<div style="text-align: left;">
		
	<div class="form">
		{{ Form::open(array('url' => 'paciente/editarEncuestasIndiceBarthel', 'method' => 'post', 'role' => 'form', 'id' => 'formIndiceBarthelFinal')) }}
		<div>
			<input name="inicio" value="false" hidden/>
			<input name="tipo-encuesta" value="indiceBarthel" hidden/>
		</div>

		<h4>Índice de Barthel Final</h4>

		<div class="row">
			<div class="col-sm-6">
				<div class="form-group ">
					<label for="indiceBarthelFinal-fecha-encuesta" class="control-label" style="width:100%;">Fecha</label>
					<input id="indiceBarthelFinal-fecha-encuesta" name="fecha-encuesta" class="form-control fecha" type="text"/>	
				</div>
			</div>
		</div>

		<fieldset>
			<!--<legend class="negrita" style="font-size: 14px;">Instrucciones:</legend>-->
			<table class="table table-bordered">
				<thead>
				    <tr>
				        <th>Parámetro</th>
				        <th>Situación del paciente</th>
				        <th>Puntuación</th>
				    </tr>
				</thead>
				<tbody>
					<tr>
				      	<td colspan="2">
				      		Total:
				      	</td>
				      	<td>
				      		<input type="number" min="0" name="indiceBarthel-total" class="form-control indiceBarthelFinal-total"  readonly >
				      	</td>
				    </tr>
				</tbody>

				<tbody class="agrupar-trs">
					<tr>
				      	<td rowspan="3">
				      		Comer
				      	</td>
				      	<td>
				        	- Totalmente independiente
				      	</td>
				      	<td>				      		
				      		<label class="btn btn-default btn-radio-group" for="indiceBarthelFinal-p-1-10">
				                <input type="radio" id="indiceBarthelFinal-p-1-10" name="indiceBarthel-p-1" value="10" /> 10
				            </label>				      					      		
				      	</td>
				    </tr>
				    <tr>
				      	<td>
				        	- Necesita ayuda para cortar carne, el pan, etc.
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceBarthelFinal-p-1-5" name="indiceBarthel-p-1" value="5" /> 5
				            </label>
				      	</td>
				    </tr>
				    <tr>
				      	<td>
				        	- Dependiente
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceBarthelFinal-p-1-0" name="indiceBarthel-p-1" value="0" /> 0
				            </label>
				      	</td>
				    </tr>
				</tbody>

				<tbody class="agrupar-trs">
				    <tr>
				      	<td rowspan="2">
				      		Lavarse
				      	</td>
				      	<td>
				        	- Independiente: entra y sale solo del baño
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceBarthelFinal-p-2-10" name="indiceBarthel-p-2" value="5" /> 5
				            </label>
				      	</td>
				    </tr>
				    <tr>
				      	<td>
				        	- Dependiente
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceBarthelFinal-p-2-5" name="indiceBarthel-p-2" value="0" /> 0
				            </label>
				      	</td>
				    </tr>
				</tbody>

				<tbody class="agrupar-trs"> 
					<tr>
				      	<td rowspan="3">
				      		Vestirse
				      	</td>
				      	<td>
				        	- Independiente: capaz de ponerse y de quitarse la ropa, abotonarse, atarse los zapatos
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceBarthelFinal-p-3-10" name="indiceBarthel-p-3" value="10" /> 10
				            </label>
				      	</td>
				    </tr>
				    <tr>
				      	<td>
				        	- Necesita ayuda
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceBarthelFinal-p-3-5" name="indiceBarthel-p-3" value="5" /> 5
				            </label>
				      	</td>
				    </tr>
				    <tr>
				      	<td>
				        	- Dependiente
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceBarthelFinal-p-3-0" name="indiceBarthel-p-3" value="0" /> 0
				            </label>
				      	</td>
				    </tr>
				</tbody>

				<tbody class="agrupar-trs"> 
				    <tr>
				      	<td rowspan="2">
				      		Arreglarse
				      	</td>
				      	<td>
				        	- Independiente para lavarse la cara, las manos, peinarse, afeitarse, maquillarse, etc.
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceBarthelFinal-p-4-10" name="indiceBarthel-p-4" value="5" /> 5
				            </label>
				      	</td>
				    </tr>
				    <tr>
				      	<td>
				        	- Dependiente
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceBarthelFinal-p-4-5" name="indiceBarthel-p-4" value="0" /> 0
				            </label>
				      	</td>
				    </tr>
				</tbody>

				<tbody class="agrupar-trs"> 
				    <tr>
				      	<td rowspan="3">
				      		Deposiciones (valórese la semana previa)
				      	</td>
				      	<td>
				        	- Continencia normal
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceBarthelFinal-p-5-10" name="indiceBarthel-p-5" value="10" /> 10
				            </label>
				      	</td>
				    </tr>
				    <tr>
				      	<td>
				        	- Ocasionalmente algún episodio de incontinencia, o necesita ayuda para administrarse supositorios o lavativas
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceBarthelFinal-p-5-5" name="indiceBarthel-p-5" value="5" /> 5
				            </label>
				      	</td>
				    </tr>
				    <tr>
				      	<td>
				        	- Incontinencia
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceBarthelFinal-p-5-0" name="indiceBarthel-p-5" value="0" /> 0
				            </label>
				      	</td>
				    </tr>
				</tbody>


				<tbody class="agrupar-trs"> 
				    <tr>
				      	<td rowspan="3">
				      		Micción (valórese la semana previa)
				      	</td>
				      	<td>
				        	- Continencia normal, o es capaz de cuidarse de la sonda si tiene una puesta
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceBarthelFinal-p-6-10" name="indiceBarthel-p-6" value="10" /> 10
				            </label>
				      	</td>
				    </tr>
				    <tr>
				      	<td>
				        	- Un episodio diario como máximo de incontinencia, o necesita ayuda para cuidar de la sonda
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceBarthelFinal-p-6-5" name="indiceBarthel-p-6" value="5" /> 5
				            </label>
				      	</td>
				    </tr>
				    <tr>
				      	<td>
				        	- Incontinencia
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceBarthelFinal-p-6-0" name="indiceBarthel-p-6" value="0" /> 0
				            </label>
				      	</td>
				    </tr>
				</tbody>

				<tbody class="agrupar-trs"> 
				    <tr>
				      	<td rowspan="3">
				      		Usar el retrete
				      	</td>
				      	<td>
				        	- Independiente para ir al cuarto de aseo, quitarse y ponerse la ropa…
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceBarthelFinal-p-7-10" name="indiceBarthel-p-7" value="10" /> 10
				            </label>
				      	</td>
				    </tr>
				    <tr>
				      	<td>
				        	- Necesita ayuda para ir al retrete, pero se limpia solo
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceBarthelFinal-p-7-5" name="indiceBarthel-p-7" value="5" /> 5
				            </label>
				      	</td>
				    </tr>
				    <tr>
				      	<td>
				        	- Dependiente
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceBarthelFinal-p-7-0" name="indiceBarthel-p-7" value="0" /> 0
				            </label>
				      	</td>
				    </tr>
				</tbody>

				<tbody class="agrupar-trs"> 
				    <tr>
				      	<td rowspan="4">
				      		Trasladarse
				      	</td>
				      	<td>
				        	- Independiente para ir del sillón a la cama
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceBarthelFinal-p-8-15" name="indiceBarthel-p-8" value="15" /> 15
				            </label>
				      	</td>
				    </tr>
				    <tr>
				      	<td>
				        	- Mínima ayuda física o supervisión para hacerlo
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceBarthelFinal-p-8-10" name="indiceBarthel-p-8" value="10" /> 10
				            </label>
				      	</td>
				    </tr>
				    <tr>
				      	<td>
				        	- Necesita gran ayuda, pero es capaz de mantenerse sentado solo
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceBarthelFinal-p-8-5" name="indiceBarthel-p-8" value="5" /> 5
				            </label>
				      	</td>
				    </tr>
				    <tr>
				      	<td>
				        	- Dependiente
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceBarthelFinal-p-8-0" name="indiceBarthel-p-8" value="0" /> 0
				            </label>
				      	</td>
				    </tr>
				</tbody>

				<tbody class="agrupar-trs"> 
				    <tr>
				      	<td rowspan="4">
				      		Deambular
				      	</td>
				      	<td>
				        	- Independiente, camina solo 50 metros
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceBarthelFinal-p-9-15" name="indiceBarthel-p-9" value="15" /> 15
				            </label>
				      	</td>
				    </tr>
				    <tr>
				      	<td>
				        	- Necesita ayuda física o supervisión para caminar 50 metros
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceBarthelFinal-p-9-10" name="indiceBarthel-p-9" value="10" /> 10
				            </label>
				      	</td>
				    </tr>
				    <tr>
				      	<td>
				        	- Independiente en silla de ruedas sin ayuda
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceBarthelFinal-p-9-5" name="indiceBarthel-p-9" value="5" /> 5
				            </label>
				      	</td>
				    </tr>
				    <tr>
				      	<td>
				        	- Dependiente
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceBarthelFinal-p-9-0" name="indiceBarthel-p-9" value="0" /> 0
				            </label>
				      	</td>
				    </tr>
				</tbody>

				<tbody class="agrupar-trs"> 
				    <tr>
				      	<td rowspan="3">
				      		Escalones
				      	</td>
				      	<td>
				        	- Independiente para bajar y subir escaleras
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceBarthelFinal-p-10-10" name="indiceBarthel-p-10" value="10" /> 10
				            </label>
				      	</td>
				    </tr>
				    <tr>
				      	<td>
				        	- Necesita ayuda física o supervisión para hacerlo
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceBarthelFinal-p-10-5" name="indiceBarthel-p-10" value="5" /> 5
				            </label>
				      	</td>
				    </tr>
				    <tr>
				      	<td>
				        	- Dependiente
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				            <input type="radio" id="indiceBarthelFinal-p-10-0" name="indiceBarthel-p-10" value="0" /> 0
				            </label>
				      	</td>
				    </tr>
				</tbody>
				<tfoot>
					<tr>
				      	<td colspan="2">
				      		Total:
				      	</td>
				      	<td>
				      		<input type="number" min="0" name="indiceBarthel-total" class="form-control indiceBarthelFinal-total"  readonly >
				      	</td>
				    </tr>
					<tr>
						<td colspan="3">
							<button type="button" class="btn btn-primary pull-center" onclick="sumarIndiceBarthel('indiceBarthel-p-','indiceBarthelFinal-total','mensaje-resultado-indiceBarthelFinal', 'formIndiceBarthelFinal');" id="ecalcularindiceBarthelFinal">Calcular</button> <span class="mensaje-resultado-indiceBarthelFinal"></span>
						</td>
					</tr>
				</tfoot>
			</table>

			<p>Máxima puntuación: 100 puntos (90 si va en silla de ruedas)</p>

			<table class="table table-bordered">
				<thead>
				    	<tr>
				    	<th>Resultado</th>
				    		<th>Grado de dependencia</th>
				    	</tr>				      
				</thead>
				<tbody>
				      <tr>
				      	<td>&lt;20</td>
				      	<td>Total</td>
				      </tr>

				      <tr>
				      	<td>20-35</td>
				      	<td>Grave</td>
				      </tr>

				      <tr>
				      	<td>40-55</td>
				      	<td>Moderado</td>
				      </tr>

				      <tr>
				      	<td>≥ 60</td>
				      	<td>Leve</td>
				      </tr>

				      <tr>
				      	<td>100</td>
				      	<td>Independiente</td>
				      </tr>
				</tbody>
			</table>
			<tfoot>
					<tr>
				      	<td colspan="1">
				      		Total:
				      	</td>
				      	<td>
				      		<input type="number" min="0" name="indiceCharlson-total" class="form-control indiceCharlsonFinal-total"  readonly >
				      	</td>
				    </tr>
					<tr>
						<td colspan="3">
							<button type="button" class="btn btn-primary pull-center" onclick="sumarIndiceCharlson('charlsonFinal-p-','indiceCharlsonFinal-total','mensaje-resultado-indiceCharlsonFinal', 'formCharlsonFinal');" id="ecalcularindiceCharlsonFinal">Calcular</button> <span class="mensaje-resultado-indiceCharlsonInicial"></span>
						</td>
					</tr>
				</tfoot>

		<fieldset>

		
		<?php
			$tipoPaciente = App\Models\Usuario::obtenerNombreTipoUsuario(Auth::user()->rut);
			if ($tipoPaciente == "medico")
			{
		?>
                                                                             
		<button type="button" class="btn btn-primary pull-right" onclick="habilitarBoton('formIndiceBarthelFinal', 'editarindiceBarthelFinal', 'guardarindiceBarthelFinal')" id="editarindiceBarthelFinal">Editar</button>
		
		<button type="button" class="btn btn-success pull-right" id="imprimirBarthelFinal" onclick="imprimirPDF('formIndiceBarthelFinal')" >PDF</button>

		<button type="submit" class="btn btn-primary pull-right" id="guardarindiceBarthelFinal" style="display:none;">Guardar</button>
		<?php
			}
		?>
		{{ Form::close() }}
	</div>
</div>
<!--</fieldset>-->

<script type="text/javascript">
	
$(function(){
	sumarIndiceBarthel('indiceBarthel-p-','indiceBarthelFinal-total','mensaje-resultado-indiceBarthelFinal', 'formIndiceBarthelFinal');
	$("#formIndiceBarthelFinal").formValidation({
		excluded: ':disabled',
		framework: 'bootstrap',
		fields: {
			"fecha-encuesta": {
				validators:{
					/*notEmpty: {
						message: 'El nombre es obligatorio'
					},*/
                    date: {
                        format: 'DD-MM-YYYY',
                        //min: '01/01/2010',
                        //max: '12/30/2020',
                        message: 'La fecha no es válida'
                    }
				}
			}
		}
	}).on('err.field.fv', function(e, data) {
		if (data.fv.getSubmitButton()) data.fv.disableSubmitButtons(false);
	}).on('success.field.fv', function(e, data) {
		if (data.fv.getSubmitButton()) data.fv.disableSubmitButtons(false);
	}).on("success.form.fv", function(evt){
		console.log("--- submit editarEncuestasIndiceBarthel ---");

		$("#formIndiceBarthelFinal input[type='submit']").prop("disabled", false);
		evt.preventDefault(evt);
		$("#dvLoading").show();
		var $form = $(evt.target);
		
		var form = $(this).serializeArray();
		form.push({name:"rut", value:"{{ $rut }}"});

		$.ajax({
			url: $form.prop("action"),
			data: form,
			type: "post",
			dataType: "json",
			success: function(data){
				if(data.exito) bootbox.alert("<h4>"+data.exito+"</h4>", function(){
					verDatosPaciente();
					deshabilitarBoton('formIndiceBarthelFinal', 'editarindiceBarthelFinal', 'guardarindiceBarthelFinal');
				});
				if(data.error) bootbox.alert("<h4>"+data.error+"</h4>");
				$("#dvLoading").hide();
			},
			error: function(error){
				console.log(error);
				$("#dvLoading").hide();
			}
		});
		return false;
	});


});
</script>






