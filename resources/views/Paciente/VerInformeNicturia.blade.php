@extends("Template/Template")

@section("titulo")
Ver resultados informes de nicturia
@stop

@section("script")

<script>
$(function(){
	$("#tabla_informe_nicturia").DataTable({
		"ordering": false,
		"paging":false,
		"oLanguage": {
			"sUrl": "{{URL::to('/')}}/js/spanish.json"
		},
		"columnDefs": [
			{
				"className": "dt-center",
				"targets": "_all"
			}
		]
	});
	
    $.ajax({
            url: "cargarUsuarios",
            dataType: "json",
            data:{tipo:'evaluador_1'},
            type: "post",
            success: function(dat) {
            	
            	var $select=$("#usuarios");
            	$select.append("<option value=''></option>");
            	for(var i=0;i<dat.length;i++)
            	{
            		$select.append("<option value='"+dat[i].rut+"'>"+dat[i].nombre+"</option>");
            	}
            },
            error: function(error) {
                console.log(error);
            }
        });
    $("#usuarios").change(function(){
    		$.ajax({
            url: "cargarDatosInformeNicturia",
            dataType: "json",
            data:{usuario:$("#usuarios").val()},
            type: "post",
            success: function(dat) {
            	
            	var $tabla=$("#tabla_informe_nicturia").DataTable();
            	$tabla.clear();
            	var contador_si=0;
            	var contador_no=0;
            	var contador_ni=0;
            	
            	for(var i=0;i<dat.length;i++)
            	{
            		
            		var valor="<span class='glyphicon glyphicon-ok'></span>";
            		var valor2="";
            		var nombre=formato_numero(dat[i].rut,0,null,".")+"-"+Fn.dv(dat[i].rut)+" "+(dat[i].nombre_completo!=null?dat[i].nombre_completo:"");
            		var tiene=dat[i].tiene_nicturia;
            		
            		var fila=[];
            		
            		if(tiene===true)
            		{
            			contador_si++;
            			fila=[nombre,valor,valor2,valor2];
            		}
            		else if(tiene===false)
            		{
            			contador_no++;
            			fila=[nombre,valor2,valor,valor2];
            		}
            		else
            		{
            			contador_ni++;
            			fila=[nombre,valor2,valor2,valor];
            		}
            		$tabla.row.add(fila);
            	}
            	
            	var total=contador_si+contador_no+contador_ni;
            	var p1=(Math.round(contador_si*10000/total))/100;
            	var p2=(Math.round(contador_no*10000/total))/100;
            	var p3=(Math.round(contador_ni*10000/total))/100;
            	
            	
            	$("#total-si").text(contador_si);
            	$("#total-no").text(contador_no);
            	$("#total-ni").text(contador_ni);
            	$("#porcentaje-si").text(p1);
            	$("#porcentaje-no").text(p2);
            	$("#porcentaje-ni").text(p3);
            	
            	$tabla.draw();
            },
            error: function(error) {
                console.log(error);
            }
        });
    });
	
});
</script>

@stop

@section("section")

<div>
<?php
	 $tipoPaciente = Usuario::obtenerNombreTipoUsuario(Auth::user()->rut);
	if ($tipoPaciente != "evaluador_2")
	{
?>
	<div class="col-sm-3">
		<label>Seleccione evaluador</label>
		<select class="form-control " id="usuarios">
		</select>
	</div>
	<table id="tabla_informe_nicturia" class="table table-bordered">
		<thead>
			<tr>
				<th>Paciente</th>
				<th>Presenta nicturia</th>
				<th>No presenta nicturia</th>
				<th>Sin evaluación</th>
				
			</tr>
		</thead>
		<tfoot>
			<tr>
				<th>Total<br>Porcentaje</th>
				<th><span id="total-si">0</span><br><span id="porcentaje-si">0</span>%</th>
				<th><span id="total-no">0</span><br><span id="porcentaje-no">0</span>%</th>
				<th><span id="total-ni">0</span><br><span id="porcentaje-ni">0</span>%</th>
			</tr>
		</tfoot>
	</table>
	<?php
	}
	
	 
	if ($tipoPaciente == "evaluador_2")
	{
	?>
	<fieldset>
		<legend>Informes de nicturia</legend>
		<h4>Datos diarios</h4>
		{{ Form::open(array('url' => 'paciente/procesarDatosTabla1', 'method' => 'get', 'class' => 'form-horizontal', 'role' => 'form', 'id' => 'formReporte')) }}
		<button id="informe_tabla1" class="btn btn-primary" type="submit">Descargar informe tabla 1</button>
		
		{{ Form::close() }}
		
		<br>
		<h4>Resumen</h4>
		{{ Form::open(array('url' => 'paciente/procesarDatosTabla2', 'method' => 'get', 'class' => 'form-horizontal', 'role' => 'form', 'id' => 'formReporte')) }}
		<button id="informe_tabla2" class="btn btn-primary" type="submit">Descargar informe tabla 2</button>
		{{ Form::close() }}
	</fieldset>
	<?php
	}
	?>
</div>
@stop
