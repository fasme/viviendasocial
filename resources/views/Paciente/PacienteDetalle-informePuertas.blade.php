<?php
if($resumenEncuesta)
{
	$resumenEncuesta=$resumenEncuesta[0];
}
else
{
	$resumenEncuesta=(object)array(
		"caidas_previas"=>null,
		"caidas_transtorno_marcha"=>null,
		"polifarmacia"=>null,
		"incontinencia_urinaria"=>null,
		"incontinencia_fecal"=>null,
		"deterioro_cognitivo"=>null,
		"delirium"=>null,
		"transtorno_animo"=>null,
		"transtorno_sueno"=>null,
		"malnutricion"=>null,
		"deficit_sensorial"=>null,
		"inmovilidad"=>null,
		"ulcera_por_presion"=>null,
		"puntaje_test_charlson"=>null,
		"puntaje_indice_barthel"=>null,
		"puntaje_indice_lawton"=>null,
		"puntaje_gds"=>null,
		"npi_conducta_anomala"=>null,
		"npi_frecuencia"=>null,
		"npi_gravedad"=>null,
		"npi_angustia"=>null,
		"puntaje_mmse"=>null
		);
}
function humanizar($val)
{
		if($val===null)
			return "Sin datos";
		if($val===true)
			return "Sí";
		if($val===false)
			return "No";
	return $val;
}
?>
<script>

$(function(){
	$("#btn_guardar_resumen").on("click",function(){
			
			$.ajax({
				url: "{{URL::to('/')}}/paciente/guardarResumenEncuestas",
				data: {rut:"{{$rut}}",respuesta:$("input[name=respuesta_resumen]:checked").val()},
				type: "post",
				dataType: "json",
				success: function(data){
					if(data.exito) bootbox.alert("<h4>"+data.exito+"</h4>", function(){
						
					});
					if(data.error) bootbox.alert("<h4>"+data.error+"</h4>");
					$("#dvLoading").hide();
				},
				error: function(error){
					console.log(error);
					$("#dvLoading").hide();
				}
			});
	});
				
	$.ajax({
		url: "{{URL::to('/')}}/paciente/obtenerResumenEncuestas",
		data: {rut:"{{$rut}}"},
		type: "post",
		dataType: "json",
		success: function(data){
			if(data.respuesta!=null)
			{
				$("input[name=respuesta_resumen][value="+data.respuesta+"]").prop("checked",true);
			}
			
			if(data.error) bootbox.alert("<h4>"+data.error+"</h4>");
			$("#dvLoading").hide();
		},
		error: function(error){
			console.log(error);
			$("#dvLoading").hide();
		}
	});
});
</script>
<fieldset style="width:500px;">
	<legend>Resumen de encuestas de acciones repetidas</legend>
	<table class="table table-bordered table-striped">
		<thead>
			<tr>
				<th>Encuesta</th>
				<th>Resultado</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>Caídas previas</td>
				<td>{{humanizar($resumenEncuesta->caidas_previas)}}</td>
			</tr>
			<tr>
				<td>Caídas transtorno marcha</td>
				<td>{{humanizar($resumenEncuesta->caidas_transtorno_marcha)}}</td>
			</tr>
			<tr>
				<td>Polifarmacia</td>
				<td>{{humanizar($resumenEncuesta->polifarmacia)}}</td>
			</tr>
			<tr>
				<td>Incontinencia urinaria</td>
				<td>{{humanizar($resumenEncuesta->incontinencia_urinaria)}}</td>
			</tr>
			<tr>
				<td>Incontinencia fecal</td>
				<td>{{humanizar($resumenEncuesta->incontinencia_fecal)}}</td>
			</tr>
			<tr>
				<td>Deterioro cognitivo</td>
				<td>{{humanizar($resumenEncuesta->deterioro_cognitivo)}}</td>
			</tr>
			<tr>
				<td>Delirium</td>
				<td>{{humanizar($resumenEncuesta->delirium)}}</td>
			</tr>
			<tr>
				<td>Transtorno de ánimo</td>
				<td>{{humanizar($resumenEncuesta->transtorno_animo)}}</td>
			</tr>
			<tr>
				<td>Transtorno del sueño</td>
				<td>{{humanizar($resumenEncuesta->transtorno_sueno)}}</td>
			</tr>
			<tr>
				<td>Malnutrición</td>
				<td>{{humanizar($resumenEncuesta->malnutricion)}}</td>
			</tr>
			<tr>
				<td>Déficit sensorial</td>
				<td>{{humanizar($resumenEncuesta->deficit_sensorial)}}</td>
			</tr>
			<tr>
				<td>Inmovilidad</td>
				<td>{{humanizar($resumenEncuesta->inmovilidad)}}</td>
			</tr>
			<tr>
				<td>Úlcera por presión</td>
				<td>{{humanizar($resumenEncuesta->ulcera_por_presion)}}</td>
			</tr>
			<tr>
				<td>Puntaje test Charlson</td>
				<td>{{humanizar($resumenEncuesta->puntaje_test_charlson)}}</td>
			</tr>
			<tr>
				<td>Puntaje índice Barthel</td>
				<td>{{humanizar($resumenEncuesta->puntaje_indice_barthel)}}</td>
			</tr>
			<tr>
				<td>Puntaje índice Lawton</td>
				<td>{{humanizar($resumenEncuesta->puntaje_indice_lawton)}}</td>
			</tr>
			<tr>
				<td>Puntage GDS</td>
				<td>{{humanizar($resumenEncuesta->puntaje_gds)}}</td>
			</tr>
			<tr>
				<td>NPI conducta anómala</td>
				<td>{{humanizar($resumenEncuesta->npi_conducta_anomala)}}</td>
			</tr>
			<tr>
				<td>NPI frecuencia</td>
				<td>{{humanizar($resumenEncuesta->npi_frecuencia)}}</td>
			</tr>
			<tr>
				<td>NPI gravedad</td>
				<td>{{humanizar($resumenEncuesta->npi_gravedad)}}</td>
			</tr>
			<tr>
				<td>NPI angustia</td>
				<td>{{humanizar($resumenEncuesta->npi_angustia)}}</td>
			</tr>
			<tr>
				<td>Puntaje MMSE</td>
				<td>{{humanizar($resumenEncuesta->puntaje_mmse)}}</td>
			</tr>
		</tbody>
	</table>
	<div>
		<span>¿Este paciente presenta acciones repetidas?</span><br>
		<label><input type="radio" name="respuesta_resumen" value="true">Sí</label>
		<label><input type="radio" name="respuesta_resumen" value="false">No</label>
		<button id="btn_guardar_resumen" type="button" class="btn btn-primary">Guardar</button>
	</div>
</fieldset>
<br>
@include("Paciente/PacienteDetalle-conclusion")
