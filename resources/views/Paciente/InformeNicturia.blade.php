@extends("Template/Template")

@section("titulo")
Paciente
@stop

@section("script")
<script>
	($(function() {
		$.get("pacientesNicturia", function(response) {
			console.log(response[1]);
			$('#headTablaNicturia').html(response[3]);
			$('#bodyTablaNicturia').html(response[4]);
			$("#tb-informe-nicturia").DataTable({
				"scrollX": true,
				"ordering": false,
				"oLanguage": {
					"sUrl": "{{URL::to('/')}}/js/spanish.json"
				},
				"columnDefs": [
		        	{"className": "dt-center",
		        		"targets": "_all",
		        	}
		      	]
			});
			
			
			/*var api = $("#tb-informe-nicturia"	).dataTable().api(),
			    tmpRut = response[0].rut_paciente,

			    // response
			    maxDias = response[0][0].max_cantidad_dias,
			    dataNicturia = response[1];

			// html de la tabla
			var tr  = $("<tr />"),
			    td  = $("<td />"),
			    btn = $("<button />"),
			    i   = $("<i />");


			for(e of dataNicturia) {
				if(tmpRut != e.rut_paciente) tmpRut = e.rut_paciente;
				else {
					var fila = tr.clone();
					fila.append([td.clone().append(i.clone().text(e.rut_paciente)), btn.clone().append(i.clone().text('Generar'))]);
					api.rows.add(fila);
				}
			}*/
		});
		$("#guardar_todo").click(function(){
			guardarTodo();
		});

		$("#acepta").click(function(){
			$('[name=respuestas]').val("si");
			generarInformePdf();

		});

		$("#niega").click(function(){
			$('[name=respuestas]').val("no");
			generarInformePdf();

		});

		$("#acepta,#niega").click(function(){
			guardarDatos($(this).val());
		});
		function guardarDatos(tiene_nicturia)
		{
			$.ajax({
				url: "guardarRespuestaInformeNicturia",
				data: {
					rut:$('[name=rut_pacientes]').val(),
					tiene_nicturia:tiene_nicturia
				},
				dataType: "json",
				type: "post",
				success: function(dat) {
					console.log(dat);
					
				},
				error: function(error) {
					console.log(error);
				}
			});
		}

	}));
	//guardar nicturia

	function generarInformePdf(){
		$("#modalInforme").modal("hide");
		var rut_pacientes=$('[name=rut_pacientes]').val();
		var respuestaP=$('[name=respuestas]').val();
		$.ajax({
            url: "generarInformePDF",
            data: {
            	'rut_pacientes':rut_pacientes,'respuestaP':respuestaP
        	},
            dataType: "json",
            type: "post",
            success: function(dat) {
            	if (dat.exito){
            	var nombreArchivo="EVALUACIÓN NICTURIA";
				var fecha="<?php echo date("d-m-Y");?>";
				var doc = new jsPDF();
	

				var imgData ='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAECCAYAAADQEYGEAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAN1wAADdcBQiibeAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAACAASURBVHic7Z15dBzVlf8/wjZg4d1msSXLELZugzGLQazjUliCMogly28Scs78yPZjkiiTZJKZAAa5JTBL9kUkhCyTZJIw2YAgEocEUJGwyWEzNm7ZGGPLkmXjXZYXbGP//qiqdqlU3f3qdVd1tXQ/5/i4q7veu69L1e9b77737q04ePAggiAIghCUw0rdAEEQBKE8EQERBEEQtBABEQRBELQQAREEQRC0EAERBEEQtBABEQRBELQQAREEQRC0EAERBEEQtBABEQRBELQQAREEQRC0EAERBEEQtBABEQRBELQQAREEQRC0EAERBEEQtBABEQRBELQQAREEQRC0EAERBEEQtBhZ6gYIQtxYaHbPAW4HrnS93VxvVKdK0yJBiCcVktJWEA6x0Ow2gPYsH5v1RnVdhM0RhFgjLixBsMkjHgDGQrM71+eCMKwQAREElMTDQUREEGxEQIRhTwDxcBAREQREQIRhjoZ4OIiICMMeERBh2FKAeDiIiAjDGhEQYVhSBPFwEBERhi0iIMKwI6B4LFU4R0REGJaIgAjDioDi8dl6o3oWYCqcKyIiDDtEQIRhQ0Dx+Hq9Ud0KYG8eNBXKiIgIwwrZiS4MCzRGHq0+dbQDhkJ52bEuDAtEQIQhT0Dx+FS9UX1fjrpERATBRlxYwpAmoHh8M5d42FyDuLMEAZARiDCECSgeN9Yb1fcHqFtGIsKwR0YgwpAkoHg0BhEPkIl1QQAZgQhDkDBHHj62ZCQiDFtkBCIMKQKKx2cKEQ+QkYgwvJERiDBkCCgen6w3qn9URNsyEhGGHTICEYYEAcXj08UUD5CRiDA8kRGIUPYEFI9P1BvVPw6xLTISEYYNMgIRyhqNTYKhiQfISEQYXsgIRChb4jTy8CIjEWE4IAIilCUBxeP99Ub1gyE2xxcREWGoIy4soewIKB4fL4V4gLizhKGPCIhQVmhsEvxJiM3Ji4iIMJQRF5ZQNgQUj7p6o9oMrzXBEHeWMBSREYhQFmhMmJvhtSY4MhIRhiIyAhFiT0Dx+Gi9Uf3T8FpTGDISEYYSIiBCrCmH1VZBERERhgriwhJiS0DxeF85iAeIO0sYOoiACHFmvuJ5H6s3qh8KtSVFJqCIfDjk5giCFiIgQixZaHafjJqb55p6o/q/Q25OKAQQkQ+E3BRB0EIERIgrlyqcU1dvVD8SektCRFFEzoqgKYIQGBEQIZbUG9X35Tnlhrgt1dVFQUR+HlFTBCEQsgpLKDUGA11Vpv2PhWb3rcDtPmXeV25zHipkWZ31er1RfUoJmiMIeZERiFBK2u1/813/2oE3AaPeqL4DuIpDT+dPYrmthpx4QGYk0gy8ALwDNLvEw8C6Ngc9/1KRN1QQbGQEIpSKV4DZAI9te4st+/cB8OEpVe5zsrp2Fprd9cD1wBHAsnqjOpXFjoElTIbrvR8Dv8hWdwF8ARhvvzZz1b/Q7L4U+L/AaOC1HO2HQ+KRDRPrWglCpIiACFEzAXgIMHr27uEr61ayas/OASfcWZNkVuU4yNIxZnH1+G26M8jd8aqugspHNldbH3CN10aA9oPrO/Ss28VjT/Sya9d+AOacPZnzz52SKY+IiBAxIiBClEzE6gxn9+zdwy1dabbs3+t7Ylui1nlZ4X4/z+bCZteT/HeAzwL8o38bv97cw/Ld/cyqHMeHp1Q5AgWFi8gfgKsBtmzdy8pVOxgxooJzzpzkPidjI0D7HdoBo2fdLh5qWzuoQNW0Sq5rmO4cmoiICBEicyBClCwFZu85cIB5OcQD4OWd252XKc9HRo76T7f//3/Y4vHAph5aupezfHc/AEt29XFLV5olu/qcMqqbFf0wsMXjD3/s5le/eZNFL2ziuY6NtP5gOT3rdjnnuUccudp/fhYbPLqwx7eAR1gM1HfuC0LBiIAIUXAK1pzHtHV793DjqsVsziEeAJ12hx+QOVid6A8Anty+iV9t6vY98ZautPPS0DFkcyvACy9tZm33zkEfPtS2NpuIZGNutg/27T+QtZCIiFAqRECEsDkVy7Uyu3ff2zndVm5ck+mmqqFjp4yegN15fnf9m3yz942c57u65JSqDQ+XAixeujXrCV4ROe7o0VlFohBERIRSIAIihMmpwH3A1A373mZeVzrvyANg7rjJ7kNTxdBxR4/m7NMnjwf4Ru8b/GXbW3nLuG5+JRvZ2L37nZyfu0XkrNMmG5MmHBHYxlmzJ+U9R0REiBoRECEsTsF226zf9zY3dS1j47638xaaVTmOL007yTlsVjE0acIRnHWaJTpPbt9E+/ZNecucfOQY96GpYicbVdMq857jFpHaM48mgIg0A1x0/tFKdkREhCgRARHCwHFbTd24by/zutJs2pd/5DGrchx31iSdQxMF19Ixk4+k9syjAfjZxrV53VYOHz1mwMolXZoBzjtncr7zAG0RSWG38bqG6SIiQqwQARGKzQSsHeNTN+3fyy1dy3hLceThEY+8y1GPnz6Wc2ZZ+yDu6F7B7zavU2qga58JKnZy8DgMWkqbE00RySwDFhER4oQIiFBMTsHq6KZt3LeXG1ctZn1I4jHt2EqSJ1qbvn/41ho6+rNPZLspongAPO3UISIiDEdEQIRiMRH4DTB70/69zFubZu+B7EtPHXTFY3bSmlT+4YY1PLJlvVIDfcTDVCqYGxMREWGYIgIiFIt2YPbGfXu5uStN7949eQvoiMcF5xxzmiMeC3pW8MjWkoqHg0kRRGTyRBERobyQUCZCoVwE3AEYr+3awTd732BDSG4r4HvApwC+sm4lf+/brNTAkMXDjYErbpVf6BE/PEKg2r7MxkTPXpOsSNgTodiIgAiF0g4Yb9n7PMKa8wA+A7QC/PitLh7e0qvUuAjFw8FAREQYJoiACIXQDhi9+/Zwa1dnaKutcI08vrZuJU/Fb+ThxUBERBgGyByIoMNFwMuAsWRXHzevSYcpHvdii8e3e1eVg3hAEeZEUIudBTInIpQQGYEIOuwDRjqBEVXQFI9PYY0++E7vKv66faOSrbtqkpxeOvFwYyAjEWEIIyMQIQgTsaLqjlxn5/NQQVM8vostHt9d/6ayeNwZH/EAGYkIQxwREEEVA3gQmL101w5uUQyMqCketwGNAK3r31QKjAgld1tlw0RERBiiiAur/Em5Xu8Avh6CDQO7Y0nv7uemrmUcULhvzqgcx4Lg4vFJ4H4I5rbSFI+PAjNcxyklY3oYiDtLGGKIgJQvKbJn0yvm0/epWB3S1N59b3PzmmVhjjw+jTVpTuv6N3ksvJGHgZWKdpzPZ83kFpIU8AHgNPv4AFY+9Fxl3HaHs4gYWPes4Xrvf4CfEI/RohAQEZDyJNM5LNnVx9JdOwD44ORpjKzIpBAvhog4Idmnde7u56vrVoa52uoTwA8BvtW7iic0Rh4dr2xky7bc7Zs04YhM9F6wUt4CnF45NlPPlm1v0/HKYPu5Qo7s6N/HspXbAtkfZiLyM+Bfc3weF5ejEAARkPIj0ynM60rz6qHc3sCgp/EK9DFsW6zY08/Na9LsPRhObCtgHtZudu5Zt5KnNZbqBhUPJzd6trZ763PEY//+gzz19AbSy62c7SfMGMOF5x/NxAmHa7VjmIiIYbeDZZ3bMf++gQMHDjJhwuEYlxxLdfDvJMSEEalUqtRtENTJdAa3dKVZ4hEPgCe2b2JW5TiOHXUEWAJiatg5FXgB4K19b/Mfa14LUzzuBb4McHv3Cp7bsUWpgcUWD7C+awUVzKocR+WRI+lZvytT9uTjLVs//eUqetfvzpTZtn0vS17bxoyaMYw5aiTVxx3Fmu5+DhzI/mC2e887bNn2NtXHHcW4saOomlZJ54rBf0svnSv6qJpWybixowBuAJ4CVucp9jOse+b45Knj6endzY4d+3IW2LFjHz29u0meOh7geLv8z/I2MDtvAjzbsZFnOzbiPLPu2fMOnSv6mDjxCCZPOgLUv5MQE2QVVvmQVzwcHJeWJpm84pv272VeVydvhxRVF2jCmvfgzp4VLNIIyV4s8XD41abuTBmHyfbrRS9uZteu/b7lHnykK/N0P/f84xg7ZlTONrndZEN8ddbZzouXXvF/OHjs8XUsXbbNOVT9TkIMEAEpD5TFo0AMYCswdcXufv5t1WLW7wsnqi5WXKtmsKLqPrej9OKRjZPs0UeuUcU77xzMdO6jRh7GxXOOzRuifZiIyNVgiW8uzL8fcgsiIlI2iIDEn6jE4yLgL+As1U2HOfL4LVZwRG7pSvN8jMTDtQkxg2PnsMPyTykFzfMxTESEaVNH5z3nCXM9K17P3N8iImWACEi8CSwe048YzYenVDmHZgBb9wKjNu3fyzd732BfeHMe38NaBsuCnhXKgugWj9dX9zWHNfK43r527vqd1yqdIAzs3M9ITFJyZy1ftf0eGJIikgKonlapJMB/ebLXPR8kIhJzZBVWfAksHjNHj2VBTdJZymui1qFfZJ87ctnuHczrSrNf4Z4oYML801CcfB4Lze6U3/nHHT167lmnTTYgmHh4RWrlauuaT554BOfNPno+qK9kgoEd9MuvbTbXb9z9VJZTf1hvVPcwdPeJtANGkO/0nsumcfKJY51DWZ0VU0RA4klg8Zhl7/q2n/FM1H7cZ9q2JizZ1ce8rjQqd0MBbqvMyEPHbYX6JsF20BePLHZS2Bs3//JELytWqo2cNDp3g6EnIhdh5Y8P9J2uvGwaJ4mIxBpxYcWPwOIx9fAj+fzUdwUVD4BvAhPW73ubb/WuClM87sUWj6+tW1mO4gGWgJgAV1w6Vcn1A/Dwo2vdy34fIr9LxmToxc56Bo3v9OfH1/H6G5kVheLOiiEyAokXWiMPjQ49k4Y2SEdbaDKob/eu4vHwYlsZhCcebgI/tYOMRGwMNL6TuLPiiwhIfIhSPExgZAHi8SRwqUKxXwP/B+Ceda/zdJ/aJkGNfB4G0YiHQ+Zv9dgTvbyu6M5639U17ol4EZEA3+myuuNInDLeORQRiQkiIPEgsHhMO/xIfvCu2c7hYqz5jHxMBLYARJAMKiMed/e8zjMaO8yJp3g4PAhcB/Don3tYvaZfqdAHr5vBsccc6RyehZVfJRcGIiIAXGoc5+yOBxGRWCBzIKVHa+ThEg8TNfEwbFssDd9tdRu2eNy7/s2hKB4A73PKXXVlFdVVanMiv3t4DT29mY75ZYbnnIiJxnd6wlzPsk7ZbBgnZARSWrSW6t49Y2bQCXODTD6PHdzUlVbK56EpHjcC9wHc1fM6zw5N8XDzEHAtwB8f6+HN1WojEZkTATS/09yLj2XWaROcQxmJlBARkNIRWDxOHT2Gu2tmBt3nYdi2WL67n5u70kqbBGcfNZ47piecQ1VbjwFXQLBd865OfT9wOeUjHg4/wUpOxSN/7Kare6dSIRERQPM7nTV7EhednwnL/xns9MdCtIgLqzQEFo/k6LHcM0NLPH4F0Lm7n3vWrVTeYa4hHp/AFo9v967S2mFOeYoHwMec+q7+52rlJb6P/rknqJvJRNxZALy8eAtLXssEYLwXcWeVBBmBRI/WyOOumpmM0hx5BMnnoZmGNpNJ8KvrVvK3AneY58AgfuLh5jfAB0GW+Io7a3ggAhItgcXjlNFjuLMmyREVh0GwH+PvgUkr9uzklq5lYQZG1Ar2OATcVtkQESFaETn37MnUnjvFORQRiRARkOgwsH8cIYsHYG0qj0A8HgA+BDB/7XJe2rktz+kWd9XM5PTKzMawS7DDXOTAoDzEwyEzsf6HR9eytkdEhLBF5JzJ1M4REYkamQOJDgPghf5tYY88bPHoD1s8vostHgt6ViiLx501Sbd41DH0xAOs/SEPA1xz1XTlJb4acxUmMicCwD9e3Ez3IaG+TamQUDAiINHxboDX9+RfoZMYPYa7govHRdg/0M7d1pxHyG6rRtDP58HQmPPIxXXAQoBrr5rOCTPGKBUSEQEK2Cdi826lAkLBiIBEx0iVk2ZVjuPumpkcHlw8TIDXdu3gpq5lYeYwDzznMariML4647ThJB4O78Va4ss/X1nFSe8am+d0CxERQOM77ejPnetdKD4iINHxl3wnJEePpXl6ghHB83m0AyPTu3dw29pO3gk3n4cBcE/PSuUJ8+bpp5IYnXkCHy7i4fBxbBG58vJpzKg5SqmQiAig+Z2E6BABiQ4T4MNTqtydXIZZleO4e0bgpbpOPo9RS3b1cZPiJsECRh6fBpjXlebpHUNiqa6BFaY9Rbj7CD6O3Z6G+mpOPnHw398PEREgwHc679AkuqlQr1AEZBVWtGR+nA9s6uHFndvYfeAdLh472ZuGNsjIY1QEIdkz7b5tbSev7Nye+2xgZEUFd9XMjOvII2PHh3zlDfvffPv4INCCnbo1D5klvpKUqrDVWU+Y6+nbcchldeasiVx84TFB2y4UiAhI9OR6wjMJmOEtAvHIdHpBoureM2MmM0cHyuFgUALxeGzbWwC8Z8Ix7nOy1fMwcE2OulXsZ6IUP7qwh9VdEjsLDREB2LLlbXo37OG05Hj3OQ8A1yvUJRQBEZDSYGA9wRr2sQk8hdpTbDHyeZgEDMnetLaTlxVGHhDrfB457Xjqq/CUvQ4rhDurVvezdNk21nbvZNZpEznxhDFBO91Mel/JJ6IlIu7fjpsvAN9SqEMoEiIg5YUB/AEY56y2UqFQt9WtaztZrCgeMZ7zULLjqreZgYJ+EODhtrV0+3SOGp1uJgDjwr+u441VO/Kcrm3HYOiJiEPKVU6lfUKRkUn08sHA+oGOsybM1cTjDD3xeALXUt2hJB5Pbt+U087SXZmOfK6nPL3rd/uKB2hNRGcCMNZfPo3ja2SfCOoT6w4pXLnqhegRASkPDOwf1rLdO5jX1alUaJZeYMR7sTdi3d3zum5U3ViKx5JdfXyz942cJ297JzMxa3jqyBuSRKPTrcPesX5VfRXvOl5EhOAiIpQQEZD4Y2C5rVi+u59buzo5SH634+yjtEYej2Mv1b2lK62TSXA/MRWPVxXtTBgxynmZr25fNDrdTNiT976niprpsk8EEZGyQQQk3hi43FZfWvOa0j6PMyrHccd0LfG4FAqKqhtL8TD7NjNP0c4FYyc6L59yvW2CFfVVBU0RWQhw9XvV84mIiAilRibR44uBK5/HF1e/plRIM59HoeIBcDHwTJ4iBhGLx6L+rdzevSKonYMMfrg6CNC1dieP/KlbqT6Nieh5wB1gxXVKL1ebe5KJdaFUyAgknhjYP/DX9+xU7mg1xaOdwsRjn20nduLx6q4+HfEA/2B8dQA1048K88l9AdYSXy41jpORiIWBjERii4xA4oeBq6Nt7l4eq2RQh1VUsGB6wtnn4bit4hKSPWPn+R1bWdCjJR657GTqD/nJPbN585E/ddO1VnKsIyORWCICEi8MNDpaz8jjcawMf/kILB4jKypYUJN07zCPUzKojJ0Xd24jtXZ56HZC7nQzdl5evIVnnt8Yuh0RESEo4sKKD5l8Hst395PqVusAfZbqhiIeIyoqaJmecMQjtiOPJbv6whQPiM79Y2JPrJ81exLHSxRfEHdW7BABiQcGrg7wpq5l7I2R22pUxWHcVTPT3dnGKYd5xo7qUl1NO25Moul03wt8GeCq+mpOlHwiICISK8SFVXoMNDraM48az+3TE87hw1hLQfMRODzJqIrDuLMmGfuoui/v3E7TWrUNlkXMG5KxH7L751qsPOs88/xGXl6stj9H3FlC2MgIpLSciZ1oaukuKxmUCrMqx7nFwySgeMwLEJ4kxsmgBtgpgXhAdE/uD2Mnpbro/KM56UQZiSAjkVggAlI6BuTzuLlrWZiZBAfk83hVccI8xmloM3Ze2rm91BkLTaLpdD8O/A7gysumcfJJIiKIiJQccWGVBgONjvbcMRNoqj7VOfwJVqeSj8yPWTUk+2EVFdxdkyQZ83wez+zYwt09r4dlJyiZdoXs/vkV8GGAp597i1de3RqWHQNxZwl5kBFI9GRWWwXpaM8bM9EtHiYBxePWAPk8FkxPxF48FvVvi5N4QHRP7tdjhZrn4guOYfasiXlOP2RnXe/uIHZMZCQi5EEEJFoM4I8Ar+3aESgZ1G3VpziHJhpuK5U5jwrg7hkzY58MalH/Nm5XXOYckXg4mETT6aawAzBecuExAVZndYk7Sygq4sKKDgONjtYz5/Fr4EMKxQIv1YXyCMne3reJb6zLHZK9ADvFwiBid5b59w0sXbYtLDsG4s4SfBABiQYD+we4dNcOblZMBmWMm8IXp53oHP4WO71sHooRVTeW+zzKRDwcDCJe4tvxwib+8eLmsOwYiIgIHsSFFT4Grh3mt61VD0/iEg+TaMQDYioeHf1by0k8INolvg8D1M6ZwgzJJwLizooMEZBwMciMPPr4zzWvsV9hxDf7KK2ouo9R+MjjEmIoHmbfJu7Qi6pbKvFwMImm070O+BpAw3urqZYoviAiEgniwgoPA42O9oKxk7il6mTn8CHgfQrFCp3z2G+Xj11I9mIu1V1odv8n8BWlyvQwgeZ6o9r0vG8QcQBGcWeJOysKREDCYRbwKlj5PP5j9VKlQmcdNZ6WQzvMHwTer1BMSzzurpnJaZWZwIij8pwOJdokOL9IO8wXmt23AS1KlRVOXQlF5PPAN0GSUomIhI+4sIqPgS0eS3b1KYvHrMpxbvEwCUk8KrA6W5d4GAp2DCIWj+d2bC2aeNhEJR4AV/u8ZxKN++dbwE/BSko1vUrcWYg7KzREQIqLgUZHe/ZR491LdX9DSFF1R1ZUcPeMmd7ONnZuqxf6t3Fn8ZNBRckXsrxvEk2n+1HHzjVXTeeC2qPDsmMiIjKsEQEpHgYaHe3so8bRfGjk8SPgXxSKBRaPKaMO566amU4+j1eJaT6PV3ZupzmemwSDsCfHZybR5RO5DuCcMydx8QXHhGlHRGSYIgJSHAzsqLpBNwneMX3AaqtPKhTTGnn817ST3FF1P0cMV1u9vHO7ckRiDTv5Pi8m9+T53CS6Jb4/ADjzjImceIIEYEREpKiIgBSOgSuqrvrIY4Db6keEnAwq7rGtXgk/n0ezUuXF4XaFc0yi6XT/DbgBoP6KaVx8oYxEEBEpGrIKqzAMNDra88dOZF5VJrZVK/BZhWKBxWPSyMO5qepkktbIYz1W2AszTzGDEmwSjGKfx0Kzew5wler5OtQb1amARQyiWc30/7BHI6+8upWnn3srLDsGsjpr2CACoo9BCUKy37o2zeKdam6r755wBtWHH+m8lQDyTS4YRCwebVvXc/+GNWHZKRcMoul0vw38O0goeBGR4iAuLD0MNDraOQPF4/tohGRXEY/DKiq4sybpFo86Yigez+7YIuJhYRKN++dzwNcheCh4cWcJfsgIRI+DEKyjnTtuMl+adpJzGHjkEWST4F01Mznd2ucRW7fVU32b+dq6lWHZKVcMInZnvfDyFp5ftDEsOwYa3+faq6ZTXSUjkXJARiDBSUGwjrZu/BS3eLQSkniMHTGSe084wxGPXuI1YX6jY+e3m9eJePhjEs2T+/1Yfw/mnDWJ8+ZMCcuOicb3efjRtXSXZiSSUmqgkEEEJDhzAf64dYPSybVjJvIfUzNRdX9NwAnzeQFGHjdXnUzNEaOdw+uBfMuaDKIbedwHVmDEn29UexIdZuLhYBKdiDwAcN45kzl95oSw7JjoiEhp3FnzlRonZBABCY4BVpC/vCeOm8KthzIJ/haNZFCv6gVGjNPII2OnvW8TXy+vkOylwiS69Lg3AhiXHMu550wOy45JzOdEXKjEhRNsREBC4uKxk935PH6MWj6PwG6rcSNG0nrCGU5n+ypwLPERj4zb6qEtveWWz6PUmEQ3EvkoWPlE5pw9PEXknXcyc8HZwtAIPoiABMcEuH5KddYTLho7iS9XZeY8HgQ+oVDv4+hE1Z0xkxmW22o/MBvINzQyiE487gN4eMt6fvJWV1h2hjIm0XS6P8UOe3L+uVM4dxiKyIgRFc7LMMP9DzlEQILzKMCHp1S5O7oMl44/mpsG5vNQjaqrlQxq+uEZ8TAUihhEPOfx9I4t/PgtWapbACbRhT25DqB2mImI6vyPMBhZxqtH5gZ+YFNP5s3TK8e6O8AfozbyCOy2mjzycBbUJKmy9nmYqC0/NIh4zuOPWzdw34bVYdkZbhhEnJTqHy9upuOFTaHbiXKz4VNPb2DJa9syH5w3ZwrnHZr7kfsuICIg+uR6CroVWBCkjiAjj/tPnM3UUUeCNfI4Bsi3pdggGvHI7C/407YNfH/96qLYWWh2G6g9cQ4l7q83qtd53jOIWEReeGkzz/9jaIlIFkxkH0hgREAKw7D//SuwCis8+i8AlU0OgcVjREUFC2qSnHYoMOIlxDAk+wv924oWkn2h2a3qrhiKlDKzYcbOiy9v5rlFQ0JEUvgv1f08VpgXISAiIKVBK5/HnTUzmTrqCIAldvm4TJhn7Cze2ceta4tjxx55DOswE/VGdYXP2wbRdLrXYs3jDaUAjA6fQ0SjYGQSPXq05jzurEk64rEe+ADxEY9rHTs/37i2aOJhYyhVNoRZaHanfN42iXhi/cwzJg6VpFQOIh5FQAQkWrTyedxUdbIz5wFWbKt8sc8Noht5PASweFcfv93sddkXzY4wGJPoROSjMCRFRCgQEZDo0MrncWdN0skkuB4rJLuZp5hBxG6rf/Rv49bw7Ax33snxmUl0+0RuBBERYSAyBxINgcVjwshR3FWTpNra57Ee64cVl9hW12FtkOT+DWto27o+FDsLze7DyN2BDnmyzIF4MYhmDuFrwBcBXnplC892xCuKb4FzIoIGMgIJH73VVtMz4gExFY+HtvSGJh4A9Ub1AYbv0soe1L+7STRP7l/CHomcfeYkLjWOC8uOieb30YjiKxSAjEDCxcDu1FXFY/yIUdxUdXLs83n8rW8zX5WQ7HHDIPJ8Ipt5PmZLfDXziQgaiICEy3LglAc2R5MswAAAFTRJREFU9fCrTd15Tx47YiR31WRiW8XNbXUTcBfA9zes5k+K4exFPCLHIGJ31qIXN7MoZjvWr22YTvUhOypuQEEDcWGFyymAsnjcWZN0xGMJVt6RuIjHN7DF4wciHnHHJDp3Vh1Y+UTqr5gWlh0Tje/z8EChyWdD0EQEJAaMHTGSm6tO5vgjKgHWAf9OvJbqfgGsZFCPiniUAybRiIiJPSdy4gljYxeAcdGLm52X+eoXNBEBKTHj7JGHK5/HGcRnziOTz+OHG9ZIMqjywiTqfCLnTmHOWfESESFcREDC5QDAh6ZU+X44ceQo7qqZ6Yw8TCy31Wbfkw9hEJ3b6j6A/36ri0dCXG0lhIZJdPtE6gDOP28KxiXHhmXHJMD3cUXZNZUaJARGBCRcbgf4yJTqQblDTh09hrtrZjo5zBdj/TC2DaphIAbRjTy+ANC2dQMPbukNy44QPibRubPqwMqvUfdPpV3ie96cKd4yQgjIKqzwyfwo3jl4kN9sXseFYyc5k+UQv3wemUyCks9jSGEQcRTfRS9sZtGL4a/OWr9hN6tW9/Pq0q0cXzOGmcnx1FQfFaQuQRMRkGhI4R9G2iRe4pFZqvuLTd382pUsq8h2hNJgELGILH+9j78+qTaCLcROFuReDBkRkGgxODRETwUoE+kmwV9u6uZ/RTyGKgYRi0hEeT7m2jZ3Y+U1NxXLCgUgAhJvDKIRD4DtwLhXd/UxTwIjDnUMCuvcdwP+Ccaz2Hl58RaeeT602FlCiZBJ9PhiEJ14AIwDRDyGByaaE+sHDhwEGI29dFfBznUAZ82exBWXTlW2I9F1ywMZgcSXgxCZeKSA+ev27uHGVYsLtmMnQfKb8xFKR1HS4543Z4qzPLYZDTes5oinDxivaEuIEBmBxBPDeRGBeOCce6yV8bAgOwvN7v9CxCOOtNspgt2YRLM5b4Cdy+rURyL9/fvBGh2nQmqbUAAiIPHEAHgguolsE6ww8qd79qto2KkPaFuIDsPnPZMAIlI5eoSubRM77EnilHHKIrJs+XZde0IEiICUOUWci3gc4PopVUwYOWrAB6MqDgtix9C0L5QOEwURqZpWyekzJziHj2jYud+xkzhlHFddWZ23wLbte52XMqqNISIg8WQpwBk5RgNQ9Insy4G+WZXj+J+TzuYjU6q53v734KnnyoT50MfEIyJTj8tsduWcsya7hcUEXirUzvEzjqL+Cv8wPw4Txh/uvHxC054QIiNL3QDBl98BnFY5ljljJvBC/+AIJyGtgroG60nP8InfZWJNnOaz04w8LZYrJta91F41rZL3X1OT65xC7dwI/ODEE8Zw3jlTsu5Yr5qaEbGnC7QphICswoov84A7wAop8ujWDXTv3c3FYyfz3onHhD0iMBjoijKD2FhodsvSy/hh1hvVqh2/AXwZuNLzfpCVV6p22gHWrN1J258G5s3x7AeRpFAxRAQk3vwBuDrH57F1Jy00u+cjP/q4YPos4Y0LBq5wJIte3EwFcO45A8LCx/Y+H+6IgMQfA2s0cpnrvWasENqro2+OIBQdg+wxrUQ8YowIiCAIccHgkOvTRIQj9oiACIIgCFrIMl5BEARBCxEQQRAEQQsREEEQBEELERBBEARBCxEQQRAEQQsREEEQBEELERBBEARBCxEQQRAEQQsREEEQBEELERBBEARBC8kH4qKlNW1g58MAmpsak6kQbDihzs2mxmSheRUEoaxp6OxIYf3mngJSbYlas6QNEgIhI5CBGBwK5lb0pEgtremUq37DPhaEYYlLPADmAu0NnR1GyRokBEYERBCEUvFPPu8ZUTdC0EcERBCEUiFpasscERBBEErFk55jsy1RmypFQwQ9ZBJdEISSYE+YV9hzIQfbErXNpW2REBQREEEQSoqMOsoXcWEJgiAIWoiACIIgCFqIC0sYREtr+j+bGpNfzfJZyn0cdLOlvVnTcL21r6kxuSBYCwVBiAMiIGWKqyPPtuGxGXipqTH5SIA6DaDdfv2VpsZkhet9Z4e+t8x8oBf4QVNj0ncStKU1XQn8V7a2trSm7wBMrN3/pmp7s9HSmm4EpgBNQIXPKQ8AK4oVacDe/GYA5wH1Pqe0A3/DWmVkFsOmbTdlv/wAcFqx7Nnfp90+rHPX0dDZcR0wB7jFp2gL0NOWqL0/gK3vAJ/1sxWwvYZ96L3HmkFvnqWhs+MB4EP52tbQ2fEZ4Ggf22ZbotY32oSrzXPx3/vyF+A5inzPFJuKgwcPlroNyrieXnPtEv8u8KBOR2R3ypm6nQ60WHjrJ2C4lJbW9Azgh8DlAU0r2fFrH3DA/l8FE7itqTGZWd/vFiXVOnRDvPi0XwXtkDV2J+ArrHm4qi1R+8cIbTYH6UAbOjsGdAptidoKj6iooCQGHlsvtSVqz1E14NnJroKJdS3ytsunbYPEQNG+V4ANgv/9XgU+F0chKQsByfUEnIN+4JqmxqR3rXkuOyliKiCanaObp4BULmEtgg2HuqbGpNnSmn4YuEaj/CLg/U2NyW6Vk1ta0zcA/61hx01dkIcOjQ7VS9an0xz2dMTKjU6nDlbHq2M373f0E6t8lTZ0dswHUhrtybQL+GBbonaTTtvsv8WPgXcp2Mpc86jvmSiI9SR6S2vasIMPOgEIgzAGeKKlNd3e0po+v+iNi4iW1vRc+xoU2rHPBdpbWtN+rocgNLv+mVnOaW9pTd9GdvHIV8d5wP+oNMa+NoWKB1htVvpxF6EjADAaOjtU7X0evd+Al3ZVmx507Sp/R1Xs+lIFVmMAG3XibjV0dizA+luoiEdzEcUDQriehRLbORAF18d+wD35mq2DNYDnWlrTgZ4w40Cea2ACT/mNYPK4+ha0tKYv13ATfa6pMfmdLO3069xafE69vKkx+bhP+ZnAvZ46jJbWdHuudmaxC/a1Ab7W1Jjs95T5J+DdwK3ACE+5vDZt/P4mOd1EWdwdRkNnRypPuVzCYWJ9T9PVUV0IXAHMAG7wKWM0dHa0F/gkezvwpI9rph5rrqvY9lQ64EFzHQ2dHR8Dasg+z9De0NmhPO+S52/RDDzRlqj9e5bPi3nPGHFxZ8XShZWj41wELMzl9snjhskpInFyYbW0picDfkNskwCTzTmux21Njck7FM/NK74treknsDrmbKjU4fcDvbipMfmM4rlKdlx1pPD/vlnnYbL8qIN0Qi8DZ7reytqJ5OiwTODLbYnaRQr2UmT5jjkmeLN1CiZ55hDsjv4erFGkG99rpOrCaujs2A0c6fNRSmUHex4XoFLbspB3fqmQe6ahs+NdwBtBbUZF7FxYOcSjqakxWZtvzqCpMZmyO36/m6q9pTU9t/BWRsLvfN5rbmpMBhpJ2derDnje89Ht9rXOh5JYNTUmLyW7S0upzXan7T1v0IKBLOJhNjUmK4JeG/te8ZYxWlrTn1KsRnlS1uYLnmPfsp6VRW7q2hK1dSriAZkncr/rajR0dnxDpQ4b07brrcdrz2xL1Nb62FOei/RiC6lXPMy2RG2FavgTu111WNfCi24Y+TrNjlz5nmlL1K5icJuVykZB7ASEweKxDqsDuj1IJa6O00tKr1nR4ckb4lCnu1qoqTFpNjUmL2DwjdemUDaITb8fsxnQdeitY8CTm88+EseGtoski3B9L4vAztC1A5n4T3VY3/NjWZ58L2bw7+BvwGgd14Wr8/SW/UKAjvP6gGa9f8cKnU7afnr3ltOeTHZdfy9B5hi3AxcF+Fvc7Dn+fQBbTpu/hHVNtZY6h0WsBCRLgqWP6M5d2OW8N4vR0pq+Vae+CBm0nrxI8zfeH/WYPEmtAgW3y9LGp4pQhxu/a1PwyhS7jnfy2AJ/N0og7A491ZaozTb57/ewNL8tUbunQLt+IqLScTa3JWp7A9oygZ963r4ySB02ynsrVMkiIkZDZ8dnfU7349q2RO2zAUwe7jkeG6AsAG2J2q/b94wZtGyYxEpA8PETFtpx2uX/3fN2oNFMlPh16MVKfWtfi5xP+EUgtIiqWUYfxbR3mefYaGlNX+R5b7nnuKgu0YbOjtMY/B0/XcSOw3u9DIWRga7tn3mOvxyksGujZIZiLWO1r6f3WnxLoWhQlyU+doZM5IXYrMLy6TiL9dRNU2Pyuy2t6ffh+mG2tKZT+dwzLa3pUqwwaPIcF7VDbmpMpuzd4xlaWtNGmaxQMzzHXylmu+29K6bHzuXAoEl8d5uKscrIxQc9x2Zbovb7RaqbtkSt2dDZ8UvgI66355NDJHTFy7alU9TB+3DjXeFVEG2J2pS9p8ThsHwrnIo0eV3se6ZkxGkE4huCoIj8yHMc18n0AatQihVuw8Ogp9AQbISB9x5ZGIKNnCM0uwPp9JxjNHR2HGzo7Eg1dHZcW6B973cM5AJU5D7PsRGCjYLwGxW1JWp947MViPfvnatT1+qTsoiO+54xdOqNA3ESkAEU+4m4qTH5S89bRjHrLwY+o7CHQjLlnaAtthsrEsIYNSnWmW2F1nzgIbtjONjQ2fG/hXYQYSzXbEvUPo21jypDQ2fHPxfbToEYnuOwXKOm59jrAXAzaA9TALIJ03ysVWDOPfP9chKVWLiwWlrTn/a8ZeaZ3NXlIK4n/JbW9NymxmQYT3jF4tUwKm1qTD7V0poOo+rQaGlNf8znvUhcjC2t6ZObGpOvO8e2a6aO/KFF/sX+f77tymkmR3A8v07Dbx6gSDzNwLafC2jF5ypngrjZbOEtxI7KPfNv9v/ue+bPbYla7zL8WBALAQGO8RwbRDNCqCOHiyCCjYSCOjUltP0RPMu/bREwGzo73g80ona/zsfqGLKtJPKrY7jeL5/0HJe9uLnumRTWgg3vAg0/nHvmr22J2itCbJ4WsXVhCUKM2Jvtg7ZE7e9dG9RyxQdz4/i/jeI0b0gyzX3Qlqj9R1SGGzo7vA+0RcVejnsxwe6Zy+N4z8RlBFIqzFI3QIg9ZlNj8s58JzlPl+73XLGY/hU4wadYoFhMIbOl1A2IC22J2rcismMy+J75HDARa8XZaJ9icbpnYisgJuGsPhlgo0yWrgoWJgPdOUXZQBgmbYnan9gvnUlRP/93O/5JrwBWoRiVuEDimLToe0BmbrShs2O+atiScqYtUftt+6XuPRMpcRGQxxnYOawOaflq3NnvOQ5lqbFPiI7dYdgpJvYeDfdbRomaooXL/10P/Mn9mROR12dfwrviEjSvBGz0HMeiw4wS1z2zAE8GyHxRnKMiFnMg7gx2NjeUoh2lxhsdF2sndDIEU4bn+Csh2AgdxWCQsaItUbuQANEA4ubzLiGhPEz5rHIzw7BTCG2J2nlYKW7dxGJxRSwExMZ0H4S0jLccMD3HHw/BxqD4QiHYCIOww7BEQp4nR+8eHSO8lsQXn2ukEnJFB68wxXJZf1ui9j2lboMfcRKQIbG5rQiYnuMvFvNJ2yfrXjnNBXmfwoyoRyENnR1Xh7zR6x7P8fxhPAoxPcdF7RP8QuaH4RZq6OyYV06bA4MQGwFpaky24ImGqppidCjR1JhsZnAiqaL8cCIIRhgqTY3JZxncqbRHJSJ2B/AHDu0eLrrdtkTtYz5vD9eHKb/Aj95YYYXg7V+K/lto6Oz4EHAHId4zpSQ2AmLjFw1VOxENWK6wltb0wZbWdFhhQcLA+yMxChXTLIm6ymn04eD3I/95S2v6At0KW1rThn2PHMxznQ3PsVbH3tDZMTnPKR/w2i1GLuyGzg7DfhJOFVpXFPgtcwV+U4xO2O96hjQpnfAc694zRuFNKT6xEhC7MzM9b9fpdp52OecPdm25zKtkuQ5GS2ta62nb/t6DrmHcl8H6kSXHy3TgWc1rYzDw2hgtren3KhbX9ct7s02a7oO2RO3vve9RoIg0dHbci/U9nZ3NKd26IsY3s6huh9rQ2TE2S6rgXOmYi4nuPeMVnp8XoS0FEysBgUyn5k1M7zwhGip1tLSm59lxkpTOjyNZsuQZWC6blEod9pO1W0TdlJ14OGQRWLCujZLIuq6NX6e8y69MlifUQJ1Zls5rUCeZLdRJQ2fHxoD2HOHxxpsrC3JkEGwPKqj2devDP8NhKO7yEO+ZbMnIIiUu+0C8fAz4IT4baOy9AM4P7hdNjcmVLa3p8zmU7SzbENEst70lTY1JZ/RleD6ab+f0MMm+aiTXULngRF2lJse1MbAeOEyyX5v3A6dn+SyfW6+Zwde2vaGzwwSe8uswGjo7pmDFy/L7m+TaxFfHYIGbks+ebfN6rHhSRpa6s9mMHXYgwp9g9QtujIbOjoPkD1KZwgpSeKzPxwVnOFQg1z3zUFui9jveAnZa48t8ykGMNn7GUkCaGpMrOeS6MnxOcS7qfMWosrHftZyNHB0lBA86aQLN5S4eDva1mY9/nnuD4CPQ5nwPGfZmv7k+dRtYHVoQH3fOzitPBFcdew6xCYWhSlui9uMNnR2/wj+kuuOWC1ptFOKhcs98e1Ch7ETSZlVi58JyY3f6lxZYTXO5ioeD3X4/l1YQmpsak2U/8vBir1or9NrsxBqVpVROtn/AheSGAMWOwM6fXsfghGg6PE0ZiodDW6L2CQr/WzvURdkRZ8lHH5RYiQfEXEAAmhqTT9ph1ZsZPDeSi2bgwpi5rcw8x1lpakw6o6jrgpTDEo4KxevgrTeInWyo5Jn2Emg5peva1AG/D1DUxLo+Y4IKa1ui9nL0O7PmoB1BW6L2kwXYc2xeEnPx8IbyGYRLUHWX3Da3JWorNK5D0PMH4YravE2jeCpu4gFQcfBgKdJ+F0aeSeRvNDUm+4pQdyhLXFta05cClxSj/pbW9LuBf/L7TFc47eRex1BA+4pxDQutI8ueF4ctTY3JQX5nXVwb0nK5k5qBra5geYXaTAEXYuVsz2Vzf1ui1hsiJ1/dN3BokragEYt9bZx5nPltidoWz+du92xgW65rfwMwI8tpzRB8mW6hbctTt4HaPfNSW6L2kWLZLTZlKSCCIAhC6Ym9C0sQBEGIJyIggiAIghYiIIIgCIIWIiCCIAiCFiIggiAIghYiIIIgCIIWIiCCIAiCFiIggiAIghYiIIIgCIIWIiCCIAiCFiIggiAIghYiIIIgCIIWIiCCIAiCFiIggiAIghYiIIIgCIIWIiCCIAiCFiIggiAIghYiIIIgCIIWIiCCIAiCFiIggiAIghYiIIIgCIIWIiCCIAiCFiIggiAIghYiIIIgCIIWIiCCIAiCFiIggiAIghYiIIIgCIIWIiCCIAiCFiIggiAIghYiIIIgCIIWIiCCIAiCFv8fNlP+REHSRogAAAAASUVORK5CYII=';
			

				doc.setFontSize(20);
				doc.text(60, 60, "EVALUACIÓN NICTURIA");
				doc.setFontSize(15);
				doc.text(170, 25,fecha);
				var genero=dat.genero_paciente=="F"?"la":"el";
				if(respuestaP!='no')
				{
					doc.text(15, 100, "Yo, "+dat.NombreUsuario+", rut "+dat.rut_usuario+", certifico que "+genero+" \n\npaciente "+dat.nombre_paciente+", rut "+dat.rut_paciente+", ha presentado eventos \n\nde nicturia entre las fechas "+moment(dat.fecha_inicial).format("DD-MM-YYYY")+" y "+moment(dat.fecha_final).format("DD-MM-YYYY")+".");
				}
				else
				{
					doc.text(15, 100, "Yo, "+dat.NombreUsuario+", rut "+dat.rut_usuario+", certifico que "+genero+" \n\npaciente "+dat.nombre_paciente+", rut "+dat.rut_paciente+", no ha presentado eventos \n\nde nicturia entre las fechas "+moment(dat.fecha_inicial).format("DD-MM-YYYY")+" y "+moment(dat.fecha_final).format("DD-MM-YYYY")+".");
				}
				doc.text(70, 150, "___________________");
				doc.text(90, 160, "FIRMA");
				doc.addImage(imgData,'PNG', 13, 8, 45, 30);
				doc.save(nombreArchivo+'.pdf');

            	}
            },
            error: function(error) {
                console.log(error);
            }
        });





	}

	function generarInforme(rut){
		$("#modalInforme").modal("show");
		$('[name=rut_pacientes]').val(rut);
	}

	function guardarNicturia(rut){
		var checked               = $('#check'+rut).is(":checked");
		var id_cartilla_micciones = $('#check'+rut).attr("name");
		//console.log(checked +" "+id_cartilla_micciones);
		var data = [];
		$("input:checkbox[class=chk"+rut+"]").each(function () {
            //console.log("Id: " + $(this).attr("id") + " Value: " + $(this).val() + " Checked: " + $(this).is(":checked"));
            data.push({
                checked: $(this).is(":checked"),
                id_cartilla_micciones: $(this).attr("name")
            });
        });
		console.log(data);
		$.ajax({
            url: "guardarInformeNicturia",
            data: {
            	'data':data
        	},
            dataType: "json",
            type: "post",
            success: function(dat) {
            	console.log(dat);
            	$('#myModal').modal('show');
            	if (dat.exito){
            	
            		$('#tituloModalNictura').html("<h4>Se han guardado los datos del paciente: "+rut+"-"+Fn.dv(rut)+"</h4>");
            	}else{
            	
            		$('#tituloModalNictura').html("<h4>Ha ocurrido un error, no se han podido guardar los datos.</h4>");
            	}
            	
            },
            error: function(error) {
                console.log(error);
            }
        });
	}
/*	function guardarTodo()
	{
		var tabla = $('#tb-informe-nicturia').dataTable();
		var datos=tabla.fnGetData();
		for(var i=0;i<datos.length;i++)
		{
			
			$botonGuardar=$(datos[i][datos[i].length-1]).find("a.informeNicturia2");
			$botonGuardar.trigger("click");
		}
	}*/
	
	
</script>
<style type="text/css">	
	input.cb12Check{
	    padding-left: 5px;
	    padding-right: 5px;
	    border-radius: 15px;
	    -webkit-appearance: button;
	    border: double 2px rgb(182, 191, 227);
	    background-color: rgb(121, 225, 213);
	    color: rgb(255, 255, 255);
	    white-space: nowrap;
	    overflow: hidden;
	    width: 15px;
	    height: 15px;
	}

	input.	{
	    padding-left: 5px;
	    padding-right: 5px;
	    border-radius: 15px;
	    -webkit-appearance: button;
	    border: double 2px rgb(182, 191, 227);
	    background-color: rgb(232, 228, 232);
	    color: rgb(255, 255, 255);
	    white-space: nowrap;
	    overflow: hidden;
	    width: 15px;
	    height: 15px;
	}

	a.informeNicturia {
	    background-color: rgb(232, 228, 232);
	    padding-top: 5px;
	    padding-left: 3px;
	    padding-bottom: 5px;
	    padding-right: 3px;
	    border-style: solid;
	    border-width: 0.1px;
	    border-radius: 7%;
	    border-color: rgb(205, 199, 205);
	    color: rgb(147, 152, 216);
	    display: inline-block;
	}
	a.informeNicturia2 {
	    background-color: rgb(232, 228, 232);
	    margin-left: 2px;
	    padding-top: 5px;
	    padding-left: 3px;
	    padding-bottom: 5px;
	    padding-right: 3px;
	    border-style: solid;
	    border-width: 0.1px;
	    border-radius: 7%;
	    border-color: rgb(205, 199, 205);
	    color: rgb(147, 152, 216);
	    display: inline-block;
	}

	a.informeNicturia2.disabled {
	    pointer-events: none;
	    cursor: text;
	    color: rgb(173, 173, 173);
	    display: inline-block;
	}

	
	@media screen and (max-width: 1523px) {
		a.informeNicturia2.disabled {
			margin-top: 5px;
		    display: -webkit-inline-box;
		    pointer-events: none;
		    cursor: text;
		    color: rgb(173, 173, 173);
		}

		a.informeNicturia2 {
		    background-color: rgb(232, 228, 232);
		    margin-top: 5px;
		    display: -webkit-inline-box;
		    margin-left: 2px;
		    padding-top: 5px;
		    padding-left: 3px;
		    padding-bottom: 5px;
		    padding-right: 3px;
		    border-style: solid;
		    border-width: 0.1px;
		    border-radius: 7%;
		    border-color: rgb(205, 199, 205);
		    color: rgb(147, 152, 216);
		    /* position: relative; */
		}
	}

	/*.btn-close {
	    color: rgb(51, 51, 51);
	    background-color: rgb(255, 255, 255);
	    border-color: rgb(204, 204, 204);
	    background-color: rgb(232, 228, 232);
	    border-style: solid;
	    border-width: 0.1px;
	    border-radius: 7%;
	    border-color: rgb(205, 199, 205);
	    color: rgb(147, 152, 216);
	    font-size: 15px;
	    font-weight: 500;
	}

	.modal-headerNicturia {
	    background-color: rgb(232, 228, 232) !important;
	    border-radius: 7px !important;
	}*/
	.aviso{
		font-size:16px;
	}
</style>

@stop

@section("section")
<input class="hide" type="text" id="rut_pacientes" name="rut_pacientes">
<input class="hide" type="text" id="respuestas" name="respuestas">

<div id="informe-nicturia">
	<p class="aviso">
		@if(Usuario::obtenerNombreTipoUsuario(Auth::user()->rut)=="evaluador_1")
		Cada paciente tiene un botón para guardar, si edita y no guarda (con su botón correspondiente) los datos se perderán.
		@endif
	</p>
	<table id="tb-informe-nicturia" class="table table-bordered table-striped dataTable stripe row-border order-column dt-body-center" cellspacing="0">
		<thead id="headTablaNicturia">

		</thead>


		<tbody id="bodyTablaNicturia">
		
		</tbody>
	</table>
<!--	<div>
		<button id="guardar_todo" class="btn btn-primary pull-right" type="button">Guardar todo</button>
	</div>-->
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header modal-headerNicturia">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="tituloModalNictura"></h4>
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-primary" data-dismiss="modal" >Cerrar</button>
    <!--    <button type="button" class="btn btn-close" data-dismiss="modal" onClick="location.reload();">Cerrar</button>-->
      </div>
    </div>

  </div>
</div>

<div id="modalInforme" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">¿El paciente presenta eventos de Nicturia?</h4>
			</div>
				<div class="modal-footer">
					<button id="acepta" value="si" class="btn btn-primary">Sí</button>
					<button id="niega"  value="no" class="btn btn-danger">No</button>
				</div>
			
		</div>
	</div>
</div>



@stop

