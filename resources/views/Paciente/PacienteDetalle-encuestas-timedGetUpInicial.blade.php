<!--<fieldset>
	<legend class="negrita" style="font-size: 16px;">Timed Get Up and Go Test Inicial</legend>-->
<div style="text-align: left;">
		
	<div class="form">
		{{ Form::open(array('url' => 'paciente/editarEncuestasTimedGetUp', 'method' => 'post', 'role' => 'form', 'id' => 'formTimedGetUpInicial')) }}
		<div>
			<input name="inicio" value="true" hidden/>
			<input name="tipo-encuesta" value="timedGetUp" hidden/>
		</div>

		<h4>Medidas de movilidad en las personas que son capaces de caminar por su cuenta (dispositivo de asistencia permitida)</h4>

		<div class="row">
			<div class="col-sm-6">
				<div class="form-group ">
					<label for="timedGetUpInicial-fecha-encuesta" class="control-label" style="width:100%;">Fecha</label>
					<input id="timedGetUpInicial-fecha-encuesta" name="fecha-encuesta" class="form-control fechaEncuesta" type="text"/>	
				</div>
			</div>
		</div>
		
		<fieldset>
			<legend class="negrita" style="font-size: 14px;">Instrucciones:</legend>
			<p>La persona puede usar su calzado habitual y puede utilizar cualquier dispositivo de ayuda que normalmente usa.</p>
	
			<ol>
			  <li>El paciente debe sentarse en la silla con la espalda apoyada y los brazos descansando sobre los apoyabrazos.</li>
			  <li>Pídale a la persona que se levante de una silla estándar y camine una distancia de 3 metros.</li>
			  <li>Haga que la persona se dé media vuelta, camine de vuelta a la silla y se siente de nuevo.</li>
			</ol>
			<table class="table table-bordered">
				    <thead>
				      <tr>
				        <th>Repetición</th>
				        <th>Respuesta</th>
				      </tr>
				    </thead>
				    <tbody>
				      <tr>
				      	<td>
				      		1.- 
				      	</td>
				      	<td>
				      		<div class="form-group error">
				        		<label for="timedGetUpInicial-p-1" class="control-label label-act">Segundos</label>
				        	<input id="timedGetUpInicial-p-1" type="number" min="0" name="p-1" class="form-control" onchange="calcularPromTestTimedGetUp('timedGetUpInicial-p-1', 'timedGetUpInicial-p-2', 'timedGetUpInicial-p-3', 'timedGetUpInicial-tiempo-encuesta')" step="any"/>
				        	</div>
				      	</td>
				      </tr>

				      <tr>
				      	<td>
				      		2.- 
				      	</td>
				      	<td>
				      		<div class="form-group error">
				        	<label for="timedGetUpInicial-p-2" class="control-label label-act">Segundos</label>
				        	<input id="timedGetUpInicial-p-2" type="number" min="0" name="p-2" class="form-control" onchange="calcularPromTestTimedGetUp('timedGetUpInicial-p-1', 'timedGetUpInicial-p-2', 'timedGetUpInicial-p-3', 'timedGetUpInicial-tiempo-encuesta')" step="any"/>
				        	</div>
				      	</td>
				      </tr>

				      <tr>
				      	<td>
				      		3.- 
				      	</td>
				      	<td>
				      		<div class="form-group error">		      	
				        	<label for="timedGetUpInicial-p-3" class="control-label label-act">Segundos</label>
				        	<input id="timedGetUpInicial-p-3" type="number" min="0" name="p-3"  class="form-control" onchange="calcularPromTestTimedGetUp('timedGetUpInicial-p-1', 'timedGetUpInicial-p-2', 'timedGetUpInicial-p-3', 'timedGetUpInicial-tiempo-encuesta')" step="any"/>
				        	</div>
				      	</td>
				      </tr>

				    </tbody>
				    <tfoot>
				    	<tr>
				    	<td>Promedio total (segundos)</td>
				    	<td><input id="timedGetUpInicial-tiempo-encuesta" name="tiempo-encuesta" class="form-control" type="number" min="0"  value="0" step="any"/>
				    	</td>
				    	</tr>
				    </tfoot>
				</table>

				<p>El cronometraje comienza cuando la persona comienza a levantarse de la silla y termina cuando regresa a la silla y se sienta.
				</p>

				<p>La persona debe dar un intento de práctica y luego repite 3 intentos. Se promedian los tres ensayos reales se promedian.
				</p>

				<p>Resultados predictivos</p>

				<table class="table table-bordered">
				    <thead>
				    	<tr>
				    		<th colspan="2">Valoración en segundos</th>
				    	</tr>				      
				    </thead>
				    <tbody>
				      <tr>
				      	<td>&lt;10</td>
				      	<td>Movilidad independiente</td>
				      </tr>

				      <tr>
				      	<td>&lt;20</td>
				      	<td>Mayormente independiente</td>
				      </tr>

				      <tr>
				      	<td>20-29</td>
				      	<td>Movilidad variable</td>
				      </tr>

				      <tr>
				      	<td>>20</td>
				      	<td>Movilidad reducida</td>
				      </tr>

				    </tbody>
				</table>

		<fieldset>

		
		<p>Source: Podsiadlo, D., Richardson, S. The timed ‘Up and Go’ Test: a Test of Basic Functional Mobility for Frail Elderly Persons. Journal of American Geriatric Society. 1991; 39:142-148</p>
		<?php
			$tipoPaciente = App\Models\Usuario::obtenerNombreTipoUsuario(Auth::user()->rut);
			if ($tipoPaciente == "medico")
			{
		?>
		<button type="button" class="btn btn-primary pull-right" onclick="habilitarBoton('formTimedGetUpInicial', 'editarTimedGetUpInicial', 'guardarTimedGetUpInicial')" id="editarTimedGetUpInicial">Editar</button>
			
		<button type="button" class="btn btn-success pull-right" id="imprimirGetUpInicial" onclick="imprimirPDF('formTimedGetUpInicial')" >PDF</button>

		<button type="submit" class="btn btn-primary pull-right" id="guardarTimedGetUpInicial" style="display:none;">Guardar</button>
		<?php
			}
		?>
		{{ Form::close() }}
	</div>
</div>
<!--</fieldset>-->

<script type="text/javascript">

$(function(){
	$("#formTimedGetUpInicial").formValidation({
		excluded: ':disabled',
		framework: 'bootstrap',
		fields: {
			"fecha-encuesta": {
				validators:{
					/*notEmpty: {
						message: 'El nombre es obligatorio'
					},*/
                    date: {
                        format: 'DD-MM-YYYY',
                        //min: '01/01/2010',
                        //max: '12/30/2020',
                        message: 'La fecha no es válida'
                    }
				}
			},
			"tiempo-encuesta": {
				validators:{
					/*notEmpty: {
						message: 'El apellido materno es obligatorio'
					},*/
					numeric: {
						message: "Debe ingresar solo números"
					}
				}
			},
			"p-1": {
				validators:{
					/*notEmpty: {
						message: 'El apellido materno es obligatorio'
					},*/
					numeric: {
						message: "Debe ingresar solo números"
					},
					greaterThan: {
                        value: 18,
                        message: 'Debe ingresar un valor mayor o igual a cero'
                    }
				}
			},
			"p-2": {
				validators:{
					/*notEmpty: {
						message: 'El correo es obligatorio'
					},*/
					numeric: {
						message: "Debe ingresar solo números"
					},
					greaterThan: {
                        value: 18,
                        message: 'Debe ingresar un valor mayor o igual a cero'
                    }
				}
			},
			"p-3": {
				validators:{
					/*notEmpty: {
						message: 'El teléfono es obligatorio'
					},*/
					numeric: {
						message: "Debe ingresar solo números"
					},
					greaterThan: {
                        value: 18,
                        message: 'Debe ingresar un valor mayor o igual a cero'
                    }
				}
			}
		}
	}).on('err.field.fv', function(e, data) {
		if (data.fv.getSubmitButton()) data.fv.disableSubmitButtons(false);
	}).on('success.field.fv', function(e, data) {
		if (data.fv.getSubmitButton()) data.fv.disableSubmitButtons(false);
	}).on("success.form.fv", function(evt){
		console.log("--- submit formTimedGetUpInicial ---");

		$("#formTimedGetUpInicial input[type='submit']").prop("disabled", false);
		evt.preventDefault(evt);
		$("#dvLoading").show();
		var $form = $(evt.target);
		
		var form = $(this).serializeArray();
		form.push({name:"rut", value:"{{ $rut }}"});

		$.ajax({
			url: $form.prop("action"),
			data: form,
			type: "post",
			dataType: "json",
			success: function(data){
				if(data.exito) bootbox.alert("<h4>"+data.exito+"</h4>", function(){
					verDatosPaciente();
					deshabilitarBoton('formTimedGetUpInicial', 'editarTimedGetUpInicial', 'guardarTimedGetUpInicial');
				});
				if(data.error) bootbox.alert("<h4>"+data.error+"</h4>");
				$("#dvLoading").hide();
			},
			error: function(error){
				console.log(error);
				$("#dvLoading").hide();
			}
		});
		return false;
	});


});
</script>






