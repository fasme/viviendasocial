<script>

var tableAlertaPuerta = null;
var tableAlertaPuertaDetalle = null;

$(function(){

	tableAlertaPuerta = $('#table-puerta').DataTable({	
		"iDisplayLength": 10,
		"bJQueryUI": true,
		"info": true,
		"bAutoWidth" : false,
		"responsive": true,
		"searching": false,
        "ordering": false,
        "info": false,
        "order": [[0, "desc"]],
		"oLanguage": {
			"sUrl": "{{URL::to('/')}}/js/spanish.json"
		},
		"ajax": {
			"url": "obtenerAlertasPuertaNicturia",
			"data": {rut: "{{$rut}}", evento: eventos.PUERTA},
			"type": "post"
		},
		initComplete: function (settings, json) {
            $("#table-puerta_length").remove();
        },
	});	

	tableAlertaPuertaDetalle = $('#table-puerta-detalle').DataTable({	
		"iDisplayLength": 10,
		"bJQueryUI": true,
		"info": true,
		"bAutoWidth" : false,
		"responsive": true,
		"searching": false,
        "ordering": false,
        "info": false,
        "order": [[0, "desc"]],
		"oLanguage": {
			"sUrl": "{{URL::to('/')}}/js/spanish.json"
		},
		initComplete: function (settings, json) {
            $("#table-puerta-detalle_length").remove();
        }
	});	

	generarGrafico(12, "grafico-12-puertas", eventos.PUERTA);
	generarGrafico(30, "grafico-30-puertas", eventos.PUERTA);

});

</script>


<div class="row">
	<div class="col-md-5">
		<div id="grafico-12-puertas"></div>
	</div>
	<div class="col-md-5">
		<div id="grafico-30-puertas"></div>
	</div>
</div>
<br>

<table id="table-puerta" class="table table-bordered table-striped dataTable dtr-inline">
	<thead>
		<tr>
			<th>Fecha de alerta</th>
			<th>Cantidad</th>
			<th></th>
		</tr>
	</thead>
</table>

<table id="table-puerta-detalle" class="table table-bordered table-striped dataTable dtr-inline">
	<thead>
		<tr>
			<th>Fecha detalle alerta</th>
		</tr>
	</thead>
</table>