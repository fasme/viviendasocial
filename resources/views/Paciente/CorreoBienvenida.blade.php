<html>
<head>
	<title>Correo de Bienvenida al sistema eHomeseniors </title>
</head>
<body>

<br>
Bienvenido(a) al sistema eHomeseniors
<br>
{{$nombre}} le recordamos que puede ingresar a la aplicación en: http://www.ehomeseniors.cl/
<br>
Su usuario es su rut con dígito verificador y su contraseña es {{ $pass }}
<br>
<br>
No olvide descargar la aplicación móvil para recibir alertas de nicturia, caídas y acciones repetidas de sus pacientes o familiares.
<br>
<br>
Saludos cordiales,
<br>
Soporte eHomeseniors

</body>
</html>