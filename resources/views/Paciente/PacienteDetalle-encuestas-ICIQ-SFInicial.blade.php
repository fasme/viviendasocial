<!--<fieldset>
	<legend class="negrita" style="font-size: 16px;">Timed Get Up and Go Test Inicial</legend>-->
<div style="text-align: left;">
		
	<div class="form">
		{{ Form::open(array('url' => 'paciente/editarEncuestasICIQ', 'method' => 'post', 'role' => 'form', 'id' => 'formICIQ-SFInicial')) }}
		<div>
			<input name="inicio" value="true" hidden/>
			<input name="tipo-encuesta" value="ICIQ" hidden/>
		</div>

		<h4>Índice ICIQ-SF</h4>

		<div class="row">
			<div class="col-sm-6">
				<div class="form-group ">
					<label for="ICIQ-SFInicial-fecha-encuesta" class="control-label" style="width:100%;">Fecha</label>
					<input id="ICIQ-SFInicial-fecha-encuesta" name="fecha-encuesta" class="form-control fecha" type="text"/>	
				</div>
			</div>
            <div class="col-sm-6">
				<div class="form-group error tiene-genero">			
					<label for="paciente-genero" class="control-label text-danger"><h4>Debe seleccionar gérero de paciente <small class="text-danger">(Pestaña Información)</small></h4></label>
				</div>
			</div>
		</div>
		
		<fieldset>
			
	                
			<table class="table table-bordered" id="tbl_1">
				<tbody>
					<tr>
				      	<td colspan="1">
				      		Total:
				      	</td>
				      	<td>
				      		<input type="number" min="0" name="indiceICIQ-SF-total" class="form-control indiceICIQ-SFInicial-total"  readonly >
				      	</td>
				    </tr>
				</tbody>
				<tbody>
					<tr>
					    <td colspan="2"><b>¿Con qué frecuencia pierde orina?</b></td>
					</tr>
				    <tr>
				      	<td style="width:80%;">
					    <p>Nunca</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceICIQ-SFInicial-p-1" name="p-1" value="0" valor="0"/> 0
				            </label>
				      	</td>
				      
				    </tr>
                    <tr>
				      	<td>
					    <p>Una vez a la semana o menos</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceICIQ-SFInicial-p-2" name="p-1" value="1" valor="1" /> 1
				            </label>
				      	</td>
				      	
				    </tr>
                    <tr>
				      	<td>
					    <p>Dos o tres veces a la semana</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceICIQ-SFInicial-p-3" name="p-1" value="2" valor="2" /> 2
				            </label>
				      	</td>
				      
				    </tr>
                    <tr>
				      	<td>
					    <p>Una vez al día</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceICIQ-SFInicial-p-4" name="p-1" value="3" valor="3" /> 3
				            </label>
				      	</td>
				      	
				    </tr>
				    <tr>
				      	<td>
					    <p>Varias veces al día</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceICIQ-SFInicial-p-5" name="p-1" value="4" valor="4" /> 4
				            </label>
				      	</td>
				      	
				    </tr>
				    <tr>
				      	<td>
					    <p>Contínuamente</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceICIQ-SFInicial-p-6" name="p-1" value="5" valor="5" /> 5
				            </label>
				      	</td>
				      	
				    </tr>
				      

				</tbody>
				    
			</table>
                        
            <table class="table table-bordered" id="tbl_2">
				<tbody>
					<tr>
					    <td colspan="2"><b>Nos gustaría saber su impresión acerca de la cantidad de orina que usted cree que se le escapa<br>
					    Cantidad de orina que pierde habitualmente (Tanto si lleva protección como si no)</b>
					    </td>
					</tr>
					<tr>
				      	<td style="width:80%;">
					    <p>No se me escapa nada</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceICIQ-SFInicial-p-7" name="p-2" value="0" valor="0"/> 0
				            </label>
				      	</td>
				      
				    </tr>
                    <tr>
				      	<td>
					    <p>Muy poca cantidad</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceICIQ-SFInicial-p-8" name="p-2" value="2" valor="2" /> 2
				            </label>
				      	</td>
				      	
				    </tr>
                    <tr>
				      	<td>
					    <p>Una cantidad moderada</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceICIQ-SFInicial-p-9" name="p-2" value="4" valor="4" /> 4
				            </label>
				      	</td>
				      
				    </tr>
                    <tr>
				      	<td>
					    <p>Mucha Cantidad</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceICIQ-SFInicial-p-10" name="p-2" value="6" valor="6" /> 6
				            </label>
				      	</td>
				      	
				    </tr>
				      

				</tbody>
				    
			</table>

                        
            <table class="table table-bordered" id="tbl_3">
				<tbody>
					<tr>
					    <td colspan="11"><b>¿Estos escapes de orina que tiene cuánto afectan su vida diaria?</b></td>
					</tr>
					<tr>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceICIQ-SFInicial-p-11"  name="p-3" value="0" valor="0" /> 0
				            </label>
				            <p class="text-center">Nada</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceICIQ-SFInicial-p-12"  name="p-3" value="1" valor="1" /> 1
				            </label>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceICIQ-SFInicial-p-13"  name="p-3" value="2" valor="2" /> 2
				            </label>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceICIQ-SFInicial-p-14"  name="p-3" value="3" valor="3" /> 3
				            </label>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceICIQ-SFInicial-p-15"  name="p-3" value="4" valor="4" /> 4
				            </label>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceICIQ-SFInicial-p-16"  name="p-3" value="5" valor="5" /> 5
				            </label>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceICIQ-SFInicial-p-17"  name="p-3" value="6" valor="6" /> 6
				            </label>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceICIQ-SFInicial-p-18"  name="p-3" value="7" valor="7" /> 7
				            </label>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceICIQ-SFInicial-p-19"  name="p-3" value="8" valor="8" /> 8
				            </label>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceICIQ-SFInicial-p-20"  name="p-3" value="9" valor="9" /> 9
				            </label>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceICIQ-SFInicial-p-21"  name="p-3" value="10" valor="10" /> 10
				            </label>
				            <p class="text-center">Mucho</p>
				      	</td>
				      
				    </tr>

				</tbody>
				<tfoot>
					<tr>
				      	<td colspan="10">
				      		Total:
				      	</td>
				      	<td>
				      		<input type="number" min="0" name="indiceICIQ-SF-total" class="form-control indiceICIQ-SFInicial-total"  readonly >
				      	</td>
				    </tr>
					<tr>
						<td colspan="11">
							<button type="button" class="btn btn-primary pull-center" onclick="sumarIndiceICIQ('indiceICIQ-SFInicial-p-','indiceICIQ-SFInicial-total','mensaje-resultado-indiceICIQ-SFInicial', 'formICIQ-SFInicial');" id="ecalcularindiceICIQ-SFInicial">Calcular</button> <span class="mensaje-resultado-indiceICIQ-SFInicial"></span>
						</td>
					</tr>
				</tfoot>
				    
			</table>
                        
                        
                        
            <!-- solo mujer -->
            <table class="table table-bordered" id="tbl_4">
				<tbody>
					<tr>
					    <td colspan="2"><b>¿Cuándo pierde orina? (Señale todo lo que le pasa a usted)</b></td>
					</tr>
					<tr>
				      	<td style="width:80%;">
					    <p>Nunca pierde orina</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="checkbox" id="indiceICIQ-SFInicial-p-22" name="p-4"  />
				            </label>
				      	</td>
				      
				    </tr>
                    <tr>
				      	<td>
					    <p>Pierde orina antes de llegar al WC</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="checkbox" id="indiceICIQ-SFInicial-p-23" name="p-5" />
				            </label>
				      	</td>
				      	
				    </tr>
				    <tr>
				      	<td>
					    <p>Pierde orina cuando tose o estornuda</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="checkbox" id="indiceICIQ-SFInicial-p-24" name="p-6"  />
				            </label>
				      	</td>
				      	
				    </tr>
                    <tr>
				      	<td>
					    <p>Pierde orina cuando duerme</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="checkbox" id="indiceICIQ-SFInicial-p-25" name="p-7" />
				            </label>
				      	</td>
				      
				    </tr>
                    <tr>
				      	<td>
					    <p>Pierde orina cuando hace esfuerzos físicos o ejercicio</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group ">
				                <input type="checkbox" id="indiceICIQ-SFInicial-p-26" name="p-8" />
				            </label>
				      	</td>
				      	
				    </tr>
				    <tr>
				      	<td>
					    <p>Pierde orina al acabar de orinar y ya se ha vestido</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="checkbox" id="indiceICIQ-SFInicial-p-27" name="p-9" />
				            </label>
				      	</td>
				      	
				    </tr>
				    <tr>
				      	<td>
					    <p>Pierde orina sin un motivo evidente</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="checkbox" id="indiceICIQ-SFInicial-p-28" name="p-10" />
				            </label>
				      	</td>
				      	
				    </tr>
				    <tr>
				      	<td>
					    <p>Pierde orina de forma continua</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="checkbox" id="indiceICIQ-SFInicial-p-29" name="p-11" />
				            </label>
				      	</td>
				      	
				    </tr>

				</tbody>

			</table>
                        
		<fieldset>

		
		<p>Fuente bibliográfica de la que se ha obtenido esta versión: </p>
		<p>Comentarios:</p>
		<p></p>
		<?php
			$tipoPaciente = App\Models\Usuario::obtenerNombreTipoUsuario(Auth::user()->rut);
			if ($tipoPaciente == "medico")
			{
		?>
		<button type="button" class="btn btn-primary pull-right" onclick="habilitarBoton('formICIQ-SFInicial', 'editarICIQ-SFInicial', 'guardarICIQ-SFInicial')" id="editarICIQ-SFInicial">Editar</button>
		
		<button type="button" class="btn btn-success pull-right" id="imprimirICIQ-SFInicial" onclick="imprimirPDF('formICIQ-SFInicial')" >PDF</button>

		<button type="submit" class="btn btn-primary pull-right" id="guardarICIQ-SFInicial" style="display:none;">Guardar</button>
		<?php
			}
		?>
		{{ Form::close() }}
	</div>
</div>
<!--</fieldset>-->

<script type="text/javascript">



$(function(){
    
    sumarIndiceICIQ('indiceICIQ-SFInicial-p-','indiceICIQ-SFInicial-total','mensaje-resultado-indiceICIQ-SFInicial', 'formICIQ-SFInicial');
    
    function obtenerGenero(genero){
    	console.log("El genero es :" + genero);        
    }

   $('#ICIQ-SFInicial-fecha-encuesta').prop("disabled", true);

    
	$("#formICIQ-SFInicial").formValidation({
		excluded: ':disabled',
		framework: 'bootstrap',
		fields: {
			"fecha-encuesta": {
				validators:{
					/*notEmpty: {
						message: 'El nombre es obligatorio'
					},*/
                    date: {
                        format: 'DD-MM-YYYY',
                        //min: '01/01/2010',
                        //max: '12/30/2020',
                        message: 'La fecha no es válida'
                    }
				}
			}
                        
		}
	}).on('err.field.fv', function(e, data) {
		if (data.fv.getSubmitButton()) data.fv.disableSubmitButtons(false);
	}).on('success.field.fv', function(e, data) {
		if (data.fv.getSubmitButton()) data.fv.disableSubmitButtons(false);
	}).on("success.form.fv", function(evt){
		console.log("--- submit formICIQ-SFInicial ---");

		$("#formICIQ-SFInicial input[type='submit']").prop("disabled", false);
		$('#ICIQ-SFInicial-fecha-encuesta').prop("disabled", false);
		evt.preventDefault(evt);
		$("#dvLoading").show();
		var $form = $(evt.target);
		
		var form = $(this).serializeArray();
		form.push({name:"rut", value:"{{ $rut }}"});
		$.ajax({
			url: $form.prop("action"),
			data: form,
			type: "post",
			dataType: "json",
			success: function(data){
				if(data.exito) bootbox.alert("<h4>"+data.exito+"</h4>", function(){
					//verDatosPaciente();
					deshabilitarBoton('formICIQ-SFInicial', 'editarICIQ-SFInicial', 'guardarICIQ-SFInicial');
				});
				if(data.error) bootbox.alert("<h4>"+data.error+"</h4>");
				$("#dvLoading").hide();
			},
			error: function(error){
				console.log(error);
				$("#dvLoading").hide();
			}
		});
		return false;
	});


});
</script>






