<!--<fieldset>
	<legend class="negrita" style="font-size: 16px;">Timed Get Up and Go Test Inicial</legend>-->
<div style="text-align: left;">
		
	<div class="form">
		{{ Form::open(array('url' => 'paciente/EvaluacionCognitiva', 'method' => 'post', 'role' => 'form', 'id' => 'formEvaluacionCognitivaFinal')) }}
		<div>
			<input name="inicio" value="false" hidden/>
			<input name="tipo-encuesta" value="EvaluacionCognitivaFinal" hidden/>
		</div>

		<h4>Evaluación Cognitiva (MMSE abreviado)</h4>

		<div class="row">
			<div class="col-sm-6">
				<div class="form-group ">
					<label class="control-label" style="width:100%;">Fecha</label>
					<input id="EvaluacionCognitivaFin-fecha-encuesta" name="fecha_test" class="form-control fecha2" type="text"/>	
				</div>
			</div>
		</div>
		
		<fieldset>
			
	                
			<table class="table table-bordered" id="tbl_1">
				<tbody>
					<tr>
				      	<td colspan="1">
				      		Total:
				      	</td>
				      	<td>
				      		<input type="number" min="0" name="EvaluacionCognitivaFinal-total" class="form-control EvaluacionCognitivaFinal-total fecha2"  readonly >
				      	</td>
				    </tr>
				</tbody>
				<tbody>
					<tr>
					    <td colspan="2"><b>1.&nbspPor favor, dígame la fecha de hoy.</b></td>
					</tr>
					<tr>
					    <td colspan="2">Sondee el mes, el día del mes, el año y el día de la semana.</td>
					</tr>
				    <tr>
				      	<td style="width:80%;">
					    <p>Mes</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="EvaluacionCognitivaFinal-p-1-bien" name="p-1" value="bien" valor="1"/> Bien
				                <input type="radio" id="EvaluacionCognitivaFinal-p-1-mal" name="p-1" value="mal" valor="0"/> Mal
				                <input type="radio" id="EvaluacionCognitivaFinal-p-1-ns" name="p-1" value="ns" valor="0"/> N.S
				                <input type="radio" id="EvaluacionCognitivaFinal-p-1-nr" name="p-1" value="nr" valor="0"/> N.R
				            </label>
				      	</td>
				      
				    </tr>
                    <tr>
				      	<td>
					    <p>Día mes</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				           		<input type="radio" id="EvaluacionCognitivaFinal-p-2-bien" name="p-2" value="bien" valor="1"/> Bien
				                <input type="radio" id="EvaluacionCognitivaFinal-p-2-mal" name="p-2" value="mal" valor="0"/> Mal
				                <input type="radio" id="EvaluacionCognitivaFinal-p-2-ns" name="p-2" value="ns" valor="0"/> N.S
				                <input type="radio" id="EvaluacionCognitivaFinal-p-2-nr" name="p-2" value="nr" valor="0"/> N.R
				            </label>
				      	</td>
				      	
				    </tr>
                    <tr>
				      	<td>
					    <p>Año</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
						        <input type="radio" id="EvaluacionCognitivaFinal-p-3-bien" name="p-3" value="bien" valor="1"/> Bien
				                <input type="radio" id="EvaluacionCognitivaFinal-p-3-mal" name="p-3" value="mal" valor="0"/> Mal
				                <input type="radio" id="EvaluacionCognitivaFinal-p-3-ns" name="p-3" value="ns" valor="0"/> N.S
				                <input type="radio" id="EvaluacionCognitivaFinal-p-3-nr" name="p-3" value="nr" valor="0"/> N.R
				            </label>
				      	</td>
				      
				    </tr>
                    <tr>
				      	<td>
					    <p>Dia semana</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="EvaluacionCognitivaFinal-p-4-bien" name="p-4" value="bien" valor="1"/> Bien
				                <input type="radio" id="EvaluacionCognitivaFinal-p-4-mal" name="p-4" value="mal" valor="0"/> Mal
				                <input type="radio" id="EvaluacionCognitivaFinal-p-4-ns" name="p-4" value="ns" valor="0"/> N.S
				                <input type="radio" id="EvaluacionCognitivaFinal-p-4-nr" name="p-4" value="nr" valor="0"/> N.R
				            </label>
				      	</td>
				      	
				    </tr>
				      

				</tbody>
				    
			</table>
                        
            <table class="table table-bordered" id="tbl_2">
				<tbody>
					<tr>
					    <td colspan="2"><b>2.&nbspAhora le voy a nombrar tres objetos. Después que se los diga, le voy a pedir que repita en voz alta los
					    que recuerde, en cualquier orden. Recuerde los objetos porque se los voy a preguntar más adelante. ¿Tiene alguna pregunta que hacerme?
					    </b></td>
					</tr>
					<tr>
						<td colspan="2">
						Explique bien para que el entrevistado entienda la tarea. Lea los nombres de los objetos lentamente y a ritmo constante, aproximadamente una palabra
						cada dos segundos. Se anota un punto por cada objeto recordado en el primer intento.<br><br> Si para algún objeto, la respuesta no es correcta
						,repita todos los objetos hasta que el entrevistado se los aprenda (máximo cinco repeticiones). Registre el número de repeticiones que debió hacer.
						</td>
					</tr>
					<tr>
				      	<td style="width:80%;">
					    <p>Árbol</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="EvaluacionCognitivaFinal-p-5-bien" name="p-5" value="bien" valor="1"/> Correcta
				                <input type="radio" id="EvaluacionCognitivaFinal-p-5-mal" name="p-5" value="mal" valor="0"/> No sabe
				            </label>
				      	</td>
				      
				    </tr>
                    <tr>
				      	<td>
					    <p>Mesa</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="EvaluacionCognitivaFinal-p-6-bien" name="p-6" value="bien" valor="1"/> Correcta
				                <input type="radio" id="EvaluacionCognitivaFinal-p-6-mal" name="p-6" value="mal" valor="0"/> No sabe
				            </label>
				      	</td>
				      	
				    </tr>
                    <tr>
				      	<td>
					    <p>Avión</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="EvaluacionCognitivaFinal-p-7-bien" name="p-7" value="bien" valor="1"/> Correcta
				                <input type="radio" id="EvaluacionCognitivaFinal-p-7-mal" name="p-7" value="mal" valor="0"/> No sabe
				            </label>
				      	</td>
				      
				    </tr>
                    <tr>
				      	<td>
					    <p>Numero de repeticiones</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="text" value="0" id="repeticionesF" name="repeticiones"/>
				            </label>
				      	</td>
				      	
				    </tr>
				      

				</tbody>
				    
			</table>

                        
            <table class="table table-bordered" id="tbl_3">
				<tbody>
					<tr>
					    <td colspan="2"><b>3.&nbspAhora voy a decirle unos números y quiero que me los repita al revés.</b></td>
					</tr>
					<tr>
				      	<td style="width:55%;">
					    <p>Números : &nbsp 1 3 5 7 9 <br><br> Anote la respuesta (el número), en el espacio correspondiente.</p>
				      	</td>
				      	<td>

					      	<table class="table table-bordered" id="tbl_3">
						      	<tr align="center">
						      		<td>Respuesta entrevistado</td>
						      		<td><input type="text" id="primer_numeroF" name="primer_numero" style="width:25px;text-align:center;"/></td>
						      		<td><input type="text" id="segundo_numeroF" name="segundo_numero" style="width:25px;text-align:center;"/></td>
						      		<td><input type="text" id="tercer_numeroF" name="tercer_numero" style="width:25px;text-align:center;"/></td>
						      		<td><input type="text" id="cuarto_numeroF" name="cuarto_numero" style="width:25px;text-align:center;"/></td>
						      		<td><input type="text" id="quinto_numeroF" name="quinto_numero" style="width:25px;text-align:center;"/></td>
						      	</tr>
						      	<tr align="center">
						      		<td>Respuesta correcta</td><td>9</td><td>7</td><td>5</td><td>3</td><td>1</td>
						      	</tr>
					      	</table>
				      	</td>
				    </tr>
				</tbody>
				    
			</table>
                        
                        
                        
            <!-- solo mujer -->
            <table class="table table-bordered" id="tbl_4">
				<tbody>
					<tr>
					    <td colspan="2"><b>4.&nbspLe voy a dar un papel; tómelo con su mano derecha, dóblelo por la mitad con ambas manos
					    y colóqueselo sobre las piernas.</b></td>
					</tr>
					<tr>
					    <td colspan="2">Entréguele el papel y anote un punto por cada acción realizada correctamente.</td>
					</tr>   
					<tr>
				      	<td style="width:80%;">
					    <p>Toma papel con la mano derecha</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="EvaluacionCognitivaFinal-p-8-bien" name="p-8" value="true" valor="1"/> Correcto
				                <input type="radio" id="EvaluacionCognitivaFinal-p-8-mal" name="p-8" value="false" valor="0"/> Incorrecto
				         	</label>
				      	</td>
				      
				    </tr>
					<tr>
				      	<td style="width:80%;">
					    <p>Dobla por la mitad con ambas manos</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="EvaluacionCognitivaFinal-p-9-bien" name="p-9" value="true" valor="1"/> Correcto
				                <input type="radio" id="EvaluacionCognitivaFinal-p-9-mal" name="p-9" value="false" valor="0"/> Incorrecto
				         	</label>
				      	</td>
				      
				    </tr>
					<tr>
				      	<td style="width:80%;">
					    <p>Coloca sobre las piernas</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="EvaluacionCognitivaFinal-p-10-bien" name="p-10" value="true" valor="1"/> Correcto
				                <input type="radio" id="EvaluacionCognitivaFinal-p-10-mal" name="p-10" value="false" valor="0"/> Incorrecto
				         	</label>
				      	</td>
				      
				    </tr>
				</tbody>
				    
			</table>
                        
            <table class="table table-bordered" id="tbl_5">
				<tbody>
					<tr>
					    <td colspan="2"><b>5.&nbspHace un momento le leí una serie de tres palabras y usted repitío las que recordó. Por favor, dígame ahora cuáles recuerda.</b></td>
					</tr>
					<tr>
					    <td colspan="2"> Anote un punto por cada palabra que recuerde. No importa el orden.</td>
					</tr>    

				</tbody>
				    
					<tr>
				      	<td style="width:80%;">
					    <p>Árbol</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="EvaluacionCognitivaFinal-p-11-bien" name="p-11" value="bien" valor="1"/> Correcto
				                <input type="radio" id="EvaluacionCognitivaFinal-p-11-mal" name="p-11" value="mal" valor="0"/> Incorrecto
				                <input type="radio" id="EvaluacionCognitivaFinal-p-11-nr" name="p-11" value="nr" valor="0"/> No responde
				            </label>
				      	</td>
				      
				    </tr>
                    <tr>
				      	<td>
					    <p>Mesa</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="EvaluacionCognitivaFinal-p-12-bien" name="p-12" value="bien" valor="1"/> Correcto
				                <input type="radio" id="EvaluacionCognitivaFinal-p-12-mal" name="p-12" value="mal" valor="0"/> Incorrecto
				                <input type="radio" id="EvaluacionCognitivaFinal-p-12-nr" name="p-12" value="nr" valor="0"/> No responde
				            </label>
				      	</td>
				      	
				    </tr>
                    <tr>
				      	<td>
					    <p>Avión</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="EvaluacionCognitivaFinal-p-13-bien" name="p-13" value="bien" valor="1"/> Correcto
				                <input type="radio" id="EvaluacionCognitivaFinal-p-13-mal" name="p-13" value="mal" valor="0"/> Incorrecto
				                <input type="radio" id="EvaluacionCognitivaFinal-p-13-nr" name="p-13" value="nr" valor="0"/> No responde
				            </label>
				      	</td>
				      
				    </tr>
			</table>
                        
            <!-- fin tareas mujer -->
                        
            <table class="table table-bordered" id="tbl_6">
				<tbody>
					<tr>
					    <td colspan="2"><b>6.&nbsp Por favor copie este dibujo</b></td>
					</tr>      
					<tr>
					    <td colspan="2">Muestre al entrevistado el dibujo con los círculos que se cruzan. La acción está correcta se lo círculos
					     no se cruzan más de la mitad.<br>
					     {{ HTML::image('images/circulos.png', null, ['class' => 'img-responsive center-block']) }}<br>
					     </td>
					</tr>                                
                    <tr>
				      	<td>
					    <p>Evalue el dibujo realizado</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="EvaluacionCognitivaFinal-p-14-bien" name="p-14" value="bien" valor="1"/> Correcto
				                <input type="radio" id="EvaluacionCognitivaFinal-p-14-mal" name="p-14" value="mal" valor="0"/> Incorrecto
				                <input type="radio" id="EvaluacionCognitivaFinal-p-14-nr" name="p-14" value="nr" valor="0"/> No responde
				         	</label>
				      	</td>
				      	
				    </tr>
                                    
				</tbody>
				  
				<tfoot>
					<tr>
				      	<td colspan="1">
				      		Total:
				      	</td>
				      	<td>
				      		<input style="width:115px;" type="number" min="0" id="EvaluacionCognitivaFinal-total" name="EvaluacionCognitivaFinal-total" class="form-control EvaluacionCognitivaFinal-total"  readonly >
				      	</td>
				    </tr>
					<tr>
						<td colspan="2">
						<button type="button" class="btn btn-primary pull-center" onclick="sumarEvaluacionCognitivaFinal('EvaluacionCognitivaFinal-p-','EvaluacionCognitivaFinal-total','mensaje-resultado-EvaluacionCognitivaFinal', 'formEvaluacionCognitivaFinal');" id="ecalcularEvaluacionCognitivaFinal" >Calcular</button> <span class="mensaje-resultado-EvaluacionCognitivaFinal"></span>
						</td>
					</tr>
				</tfoot>
			</table>
                    
		<fieldset>

		
		<button type="button" class="btn btn-primary pull-right" onclick="habilitarBoton('formEvaluacionCognitivaFinal', 'editarEvaluacionCognitivaFinal', 'guardarEvaluacionCognitivaFinal')" id="editarEvaluacionCognitivaFinal">Editar</button>
		<button type="button" class="btn btn-success pull-right" id="imprimirEvaluacionCognitivaFinal" onclick="imprimirPDF('formEvaluacionCognitivaFinal')" >PDF</button>
		<button type="submit" class="btn btn-primary pull-right" id="guardarEvaluacionCognitivaFinal" style="display:none;">Guardar</button>
	<!--	<button type="button" class="btn btn-success pull-right" id="imprimirLawtonBrodyInicial" onclick="imprimirPDF('formEvaluacionCognitivaFinal')" >PDF</button>-->
		{{ Form::close() }}
	</div>
</div>
<!--</fieldset>-->

<script type="text/javascript">


$(function(){
    sumarEvaluacionCognitivaFinal('EvaluacionCognitivaFinal-p-','EvaluacionCognitivaFinal-total','mensaje-resultado-EvaluacionCognitivaFinal', 'formEvaluacionCognitivaFinal');
    
    function obtenerGenero(genero){
    	console.log("El genero es :" + genero);        
    }

    $('#EvaluacionCognitivaFin-fecha-encuesta').prop("disabled", true);

    
	$("#formEvaluacionCognitivaFinal").formValidation({
		excluded: ':disabled',
		framework: 'bootstrap',
		fields: {
			"fecha-encuesta": {
				validators:{
					/*notEmpty: {
						message: 'El nombre es obligatorio'
					},*/
                    date: {
                        format: 'DD-MM-YYYY',
                        //min: '01/01/2010',
                        //max: '12/30/2020',
                        message: 'La fecha no es válida'
                    }
				}
			}
                        
		}
	}).on('err.field.fv', function(e, data) {
		if (data.fv.getSubmitButton()) data.fv.disableSubmitButtons(false);
	}).on('success.field.fv', function(e, data) {
		if (data.fv.getSubmitButton()) data.fv.disableSubmitButtons(false);
	}).on("success.form.fv", function(evt){
		console.log("--- submit formEvaluacionCognitivaFinal ---");

		$("#formEvaluacionCognitivaFinal input[type='submit']").prop("disabled", false);
		$('#EvaluacionCognitivaFin-fecha-encuesta').prop("disabled", false);
		evt.preventDefault(evt);
		$("#dvLoading").show();
		var $form = $(evt.target);
		
		var form = $(this).serializeArray();
		form.push({name:"rut", value:"{{ $rut }}"});

		$.ajax({
			url: $form.prop("action"),
			data: form,
			type: "post",
			dataType: "json",
			success: function(data){
				if(data.exito) bootbox.alert("<h4>"+data.exito+"</h4>", function(){
					verDatosPaciente();
					deshabilitarBoton('formEvaluacionCognitivaFinal', 'editarEvaluacionCognitivaFinal', 'guardarEvaluacionCognitivaFinal');
				});
				if(data.error) bootbox.alert("<h4>"+data.error+"</h4>");
				$("#dvLoading").hide();
			},
			error: function(error){
				console.log(error);
				$("#dvLoading").hide();
			}
		});
		return false;
	});


});
</script>






