<html>
<head>
	<title>Correo de Alerta eHomeseniors </title>
</head>
<body>

<br>
Estimado(a),
<br>
<br>
Se ha detectado un posible evento: {{$msg}}
<br>
<br>
Paciente: {{$nombre}}
<br>
Fecha alerta: {{$fecha}}
<br>
<br>
<br>
Le recordamos que puede ingresar a la aplicación en: http://www.dominio.cl/
<br>
<br>
No olvide descargar la aplicación móvil para recibir alertas de nicturia, caídas y acciones repetidas de sus pacientes o familiares.
<br>
<br>
Saludos cordiales,
<br>
Soporte

</body>
</html>
