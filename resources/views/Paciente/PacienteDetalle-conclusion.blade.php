<script type="text/javascript">
$(function(){
	$('#guardar_conclusion').click(function() {
			var valor=$('input:radio[name=conclusion]:checked').val();
			var usuario=<?php echo Auth::user()->rut; ?>;
			var paciente=<?php echo $rut; ?>;

		$.ajax({
			url: '{{URL::to("/")}}/paciente/detallePaciente/conclusion',
			type:'POST',
			data: 'valor='+valor+'&paciente='+paciente+'&usuario='+usuario,
			success: function(data){
				if(data.exito) bootbox.alert("<h4>"+data.exito+"</h4>");
				if(data.error) bootbox.alert("<h4>"+data.error+"</h4>");
			}
		});
	});

});
</script>


<div style="text-align: left;">
		
	<div class="form">
		{{ Form::open(array('url' => 'paciente/detallePaciente/conclusion', 'method' => 'post', 'role' => 'form', 'id' => 'formConclusion')) }}
			<legend>Sensores</legend>

			<span>¿Este paciente presenta acciones repetidas?</span><br>

      		<label >
                <input type="radio"  name="conclusion" value="true" /> Si
                <input type="radio"  name="conclusion" value="false" /> No 
            </label>
            <button id="guardar_conclusion" type="button" class="btn btn-primary">Guardar</button>

		{{ Form::close() }}
	</div>
</div>







