<!--<fieldset>
	<legend class="negrita" style="font-size: 16px;">Timed Get Up and Go Test Inicial</legend>-->
<div style="text-align: left;">
		
	<div class="form">
		{{ Form::open(array('url' => 'paciente/conductaMotriz', 'method' => 'post', 'role' => 'form', 'id' => 'formConductaMotrizInicial')) }}
		<div>
			<input name="inicio" value="true" hidden/>
			<input name="tipo-encuesta" value="mmse" hidden/>
		</div>

		<h4>Conducta motriz anómala</h4>

		<div class="row">
			<div class="col-sm-6">
				<div class="form-group ">
					<label class="control-label" style="width:100%;">Fecha</label>
					<input id="EvaluacionConductaMotrizInicial-fecha-encuesta" name="fecha_test" class="form-control fecha2" type="text"/>	
				</div>
			</div>
		</div>
		
		<fieldset>
			
	                
			<table class="table table-bordered" id="tbl_1">
				<tbody>
					
				</tbody>
				<tbody>
					<tr>
					    <td><b>¿El paciente hace una y otra vez cosas tales como abrir los armarios o cajones o coge las cosas repetidas veces o enrolla un cordel o hilos?</b></td>
					    <td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" name="p-0" value="true" valor="1"/> Si
				                <input type="radio" name="p-0" value="false" valor="0"/> No
				            </label>
				      	</td>
					</tr>
					<tr>
					    <td colspan="2"></td>
					</tr>
				    <tr>
				      	<td style="width:80%;">
					    <p>1. ¿Va y viene por la casa sin un objeto aparente?</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" name="p-1" value="true" valor="1"/> Si
				                <input type="radio" name="p-1" value="false" valor="0"/> No
				            </label>
				      	</td>
				      
				    </tr>
                    <tr>
				      	<td>
					    <p>2. ¿Busca desordenadamente, abriendo y vaciando cajones o armarios</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				           		<input type="radio" name="p-2" value="true" valor="1"/> Si
				                <input type="radio" name="p-2" value="false" valor="0"/> No

				            </label>
				      	</td>
				      	
				    </tr>
                    <tr>
				      	<td>
					    <p>3. ¿Se pone y se quita la ropa reiteradamente?</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
						        <input type="radio" name="p-3" value="true" valor="1"/> Si
				                <input type="radio" name="p-3" value="false" valor="0"/> No
				            </label>
				      	</td>
				      
				    </tr>
                    <tr>
				      	<td>
					    <p>4. ¿Realiza actividades repetitivas o tiene "costumbres" que repite una y otra vez?</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" name="p-4" value="true" valor="1"/> Si
				                <input type="radio" name="p-4" value="false" valor="0"/> No

				            </label>
				      	</td>
				      	
				    </tr>
				    <tr>
				      	<td>
					    <p>5. ¿Se dedica a actividades repetitivas como manipular botones, recoger, enrollar hilo, etc.?</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" name="p-5" value="true" valor="1"/> Si
				                <input type="radio" name="p-5" value="false" valor="0"/> No

				            </label>
				      	</td>
				      	
				    </tr>
				    <tr>
				      	<td>
					    <p>6. ¿Se mueve demasiado, parece incapaz de estar sentado sin moverse o mueve los pies o tamborilea much9o con los dedos?</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" name="p-6" value="true" valor="1"/> Si
				                <input type="radio" name="p-6" value="false" valor="0"/> No

				            </label>
				      	</td>
				      	
				    </tr>
				    <tr>
				      	<td>
					    <p>7. ¿Realiza el paciente cualesquiera otras actividades de una manera repetitiva?</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" name="p-7" value="true" valor="1"/> Si
				                <input type="radio" name="p-7" value="false" valor="0"/> No

				            </label>
				      	</td>
				      	
				    </tr>



				      


				      <tr>
				      		<td colspan="2"></td>
				      </tr>

				      <tr>
				      	<td colspan="2">
				      		<p/>FRECUENCIA</p>
				      	</td>
				      	
				      </tr>
				      <tr>
				      	<td colspan="2"> 
				      			<div class="radio" style="margin-left:30px">
				      				<input type="radio" id="evaluacionConductaMotrizInicial-p-8-0" name="p-8" value="0" valor="0"> 0.- No presente
				      			</div>
				      	   		<div class="radio" style="margin-left:30px">
				      				<input type="radio" id="evaluacionConductaMotrizInicial-p-8-1" name="p-8" value="1" valor="1"> 1.- De vez en cuando: menos de una vez a la semana 
				      			</div>
				      			 <div class="radio" style="margin-left:30px">

				      				<input type="radio" id="evaluacionConductaMotrizInicial-p-8-2" name="p-8" value="2" valor="0">2.- A menudo: alrededor de una vez a la semana 
				      			</div>
				      			<div class="radio" style="margin-left:30px">

				      				<input type="radio" id="evaluacionConductaMotrizInicial-p-8-3" name="p-8" value="3" valor="1"> 3.- Con frecuencia: varias veces a la semana pero no cada día 
				      			</div>

				      			 <div class="radio" style="margin-left:30px">

				      				<input type="radio" id="evaluacionConductaMotrizInicial-p-8-4" name="p-8" value="4" valor="0"> 4.- Con mucha frecuencia: una o más veces al día <br>
				      			</div>
				      	</td>
				      </tr>

				<tr>
				      	<td colspan="2">
				      		<p/>GRAVEDAD</p>
				      	</td>
				      	
				      </tr>
				      <tr>
				      	<td colspan="2">
				      			<div class="radio" style="margin-left:30px">
				      				<input type="radio" id="evaluacionConductaMotrizInicial-p-9-0" name="p-9" value="0" valor="0"> 0.- No presente
				      			</div>
				      	   		<div class="radio" style="margin-left:30px">
				      				<input type="radio" id="evaluacionConductaMotrizInicial-p-9-1" name="p-9" value="1" valor="1"> 1.- Leve: la actividad motriz anómala es notable pero no interfiere muchoen la rutina diaria.
				      			</div>
				      			 <div class="radio" style="margin-left:30px">

				      				<input type="radio" id="evaluacionConductaMotrizInicial-p-9-2" name="p-9" value="2" valor="0">2.- Moderada: la actividad motriz anómala es muy evidente; puede ser superada con ayuda del cuidador.
				      			</div>
				      			<div class="radio" style="margin-left:30px">

				      				<input type="radio" id="evaluacionConductaMotrizInicial-p-9-3" name="p-9" value="3" valor="1"> 3.- Severa: la actividad motriz anómala es muy evidente, por lo general no responde a niguna intervención por parte del cuidador y es una fuente importante de aflicción. 
				      			</div>
				      	</td>
				      </tr>

				      <tr>
					      <td colspan="2">
					      TOTAL: <input type="text" id="TotalEvaluacionConducaMotrizInicial">	 <button type="button" class="btn btn-primary" onclick="calcularEvaluacionConductaMotrizInicial()"">Calcular</button>
					      </td>
				      </tr>


				      <tr>
				      	<td colspan="2">
				      		<p/>ANGUSTIA</p>
				      	</td>
				      	
				      </tr>
				      <tr>
				      	<td colspan="2">
				      			<div class="radio" style="margin-left:30px">

				      				<input type="radio" id="evaluacionConductaMotrizInicial-p-10-1" name="p-10" value="1" valor="0"> 1.- Nada en absoluto
				      			</div>
				      	   		<div class="radio" style="margin-left:30px">
				      				<input type="radio" id="evaluacionConductaMotrizInicial-p-10-2" name="p-10" value="2" valor="1"> 2.- Mínimamente  
				      			</div>
				      			 <div class="radio" style="margin-left:30px">

				      				<input type="radio" id="evaluacionConductaMotrizInicial-p-10-3" name="p-10" value="3" valor="0">3.- Levemente
				      			</div>
				      			<div class="radio" style="margin-left:30px">

				      				<input type="radio" id="evaluacionConductaMotrizInicial-p-10-4" name="p-10" value="4" valor="1"> 4.- Severamente
				      			</div>

				      			 <div class="radio" style="margin-left:30px">

				      				<input type="radio" id="evaluacionConductaMotrizInicial-p-10-5" name="p-10" value="5" valor="0"> 5.- Muy severamente o extremdamente
				      			</div>
				      	</td>
				      </tr>

				</tbody>
				    
			</table>
            

          
     
                    
		<fieldset>
		<?php
			$tipoPaciente = App\Models\Usuario::obtenerNombreTipoUsuario(Auth::user()->rut);
			if ($tipoPaciente == "medico")
			{
		?>
		
		<button type="button" class="btn btn-primary pull-right" onclick="habilitarBoton('formConductaMotrizInicial', 'editarConductaMotrizInicial', 'guardarConductoMotrizInicial')" id="editarConductaMotrizInicial">Editar</button>
		<button type="button" class="btn btn-success pull-right" id="imprimirConductoMotrizInicial" onclick="imprimirPDF('formConductaMotrizInicial')" >PDF</button>
		<button type="submit" class="btn btn-primary pull-right" id="guardarConductoMotrizInicial" style="display:none;">Guardar</button>

		<?php
			}
		?>
		{{ Form::close() }}
	</div>
</div>
<!--</fieldset>-->

<script type="text/javascript">




$(function(){
    
    function obtenerGenero(genero){
    	console.log("El genero es :" + genero);        
    }

//calcularEvaluacionConductaMotrizInicial();
    $('#EvaluacionConductaMotrizInicial-fecha-encuesta').prop("disabled", true);
calcularEvaluacionConductaMotrizInicial();
    
	$("#formConductaMotrizInicial").formValidation({
		excluded: ':disabled',
		framework: 'bootstrap',
		fields: {
			"fecha_test": {
				validators:{
					/*notEmpty: {
						message: 'La fecha es obligatoria'
					},*/
                    date: {
                        format: 'DD-MM-YYYY',
                        //min: '01/01/2010',
                        //max: '12/30/2020',
                        message: 'La fecha no es válida'
                    }
				}
			}
                        
		}
	}).on('err.field.fv', function(e, data) {
		if (data.fv.getSubmitButton()) data.fv.disableSubmitButtons(false);
	}).on('success.field.fv', function(e, data) {
		if (data.fv.getSubmitButton()) data.fv.disableSubmitButtons(false);
	}).on("success.form.fv", function(evt){
	

		$("#formConductaMotrizInicial input[type='submit']").prop("disabled", false);
		$('#EvaluacionConductaMotrizInicial-fecha-encuesta').prop("disabled", false);
		evt.preventDefault(evt);
		$("#dvLoading").show();
		var $form = $(evt.target);
		var form = $(this).serializeArray();
		form.push({name:"rut", value:"{{ $rut }}"});

		$.ajax({
			url: $form.prop("action"),
			data: form,
			type: "post",
			dataType: "json",
			success: function(data){
				if(data.exito) bootbox.alert("<h4>"+data.exito+"</h4>", function(){
					verDatosPaciente();
					deshabilitarBoton('formConductaMotrizInicial', 'editarConductaMotrizInicial', 'guardarConductoMotrizInicial');
				});
				if(data.error) bootbox.alert("<h4>"+data.error+"</h4>");
				$("#dvLoading").hide();
			},
			error: function(error){
				console.log(error);
				$("#dvLoading").hide();
			}
		});
		return false;
		
	});


});
</script>






