<!--<fieldset>
	<legend class="negrita" style="font-size: 16px;">Inicial</legend>-->
<script>
    var addFila4=function()
    {
        var $template = $('#templateRow4'),
        $clone    = $template
        .clone()
        .removeClass('hide')
        .removeAttr('id')
        .insertBefore($template);
        $clone.find("input").prop("disabled", false);
		$clone.find('[name="fecha_evaluacion[]"]').datetimepicker({
			locale: "es",
			format: "DD-MM-YYYY hh:mm:ss",
			maxDate: moment().add('days', 1).format("YYYY/MM/DD"),
			useCurrent: false
		});
        console.log($clone.find('[name="comentario[]"]'));
    }

/*
$("#formEvaluacion").submit(function(evt){
 			evt.preventDefault(evt);

 });
*/
</script>

<!--<fieldset>
	<legend class="negrita" style="font-size: 16px;">Inicial</legend>-->
<div style="text-align: left;">
		
	<div class="form">
		{{ Form::open(array('url' => 'paciente/IngresarEvaluacion', 'method' => 'post', 'role' => 'form', 'id' => 'formEvaluacion')) }}
		<div>
			<input name="inicio" value="true" hidden/>
			<input name="tipo-encuesta" value="sindromesGeriatricos" hidden/>
		</div>
		<div class="row">
			<div class="col-sm-2">
				<label>Número de mascotas</label>
			</div>
			<div class="col-sm-3">
				<input class="form-control" name="numero_mascota">
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-sm-2">
				<label>Frecuencia mascota hogar</label>
			</div>
			<div class="col-sm-3">
				<select class="form-control" name="frecuencia_mascota">
					<option value="Gran parte del día">Gran parte del día</option>
					<option value="Frecuentemente">Frecuentemente</option>
					<option value="Ocasionalmente">Ocasionalmente</option>
					<option value="Muy ocasionalmente">Muy ocasionalmente</option>
					<option value="Nunca">Nunca</option>
				</select>
			</div>
		</div>
		<br>
		
		<div class="row">
			<div class="col-sm-2">
				<label>Número personas</label>
			</div>
			<div class="col-sm-3">
				<input class="form-control" name="numero_personas">
			</div>
		</div>
		<br>
		<legend align="center">Evaluación histórica del paciente</legend>
		<fieldset>
<?php $flag=0;?>
@foreach ($evaluacion as $evaluacionP)

<table class="table table-bordered">
 @if($flag==0)
				<thead>
				    <tr>
				        <th width="25%">Fecha</th>
				        <th width="15%">Caída</th>
				        <th width="25%">Comentario</th>
				        <th width="15%" class="n1">Evaluación</th>
				        <th width="20%" class="n2">Número de alertas</th>
				    </tr>
				</thead>
<?php $flag=1;?>
 @endif
				<tbody>
				
				<tr>
				    <td width="25%">
				    <div class="col-sm-10">
				    	<input type="hidden" name="id_evaluacion_caidas[]" value="0">
				   		{{Form::text('fecha_evaluacion[]', $evaluacionP->fecha_evaluacion, array('id' => 'fecha_evaluacion2', 'class' => 'form-control'))}}
				   		<input type="hidden" name="_fecha_evaluacion[]" value="{{$evaluacionP->fecha_evaluacion}}">
				   		
				    </div>
				    </td>
				    <td width="15%">
			       	<select  class="form-control" name="caida[]"> 
			             <option value="Si">Sí</option>
			             <option value="No">No</option>   
			             <option value="Sin dato" selected>Sin dato</option>     
			             </select>
				   	</td>
				    <td width="25%">
						<div class="col-sm-10">
					
					{{Form::text('comentario[]', '', array('id' => 'comentario', 'class' => 'form-control'))}}
						</div>	
				    </td>
				    <td width="15%" class="n1">
			        <select  class="form-control" name="evaluacion[]"> 
			             <option value="Si">Sí</option>
			             <option value="No" selected>No</option>      
			            </select>
				    </td>
				    <td width="20%"  class="n2">
						<div class="col-sm-10">
						{{Form::text('alertas[]', $evaluacionP->cantidad, array('id' => 'alertas2', 'class' => 'form-control'))}}
						<input type="hidden" name="_alertas[]" value="{{$evaluacionP->cantidad}}">
						</div>
				    </td>
				</tr>
				</tbody>
		</table>
@endforeach
	<!--	<h4>Nueva información caidas</h4><br>
		<table class="table table-bordered">
				<thead>
				    <tr>
				        <th width="25%">Fecha</th>
				        <th width="15%">Caida</th>
				        <th width="25%">Comentario</th>
				        <th width="15%">Evaluación</th>
				        <th width="20%">Numero de alertas</th>
				    </tr>
				</thead>
				<tbody>
				<tr>
				    <td width="25%">
				    <div class="col-sm-10">
				   		<input  name="fecha_evaluacion[]" class="form-control fecha2" type="text"/>
				    </div>
				    </td>
				    <td width="15%">
			            <select  name="caida[]"> 
			             <option>Si</option>
			             <option selected>No</option>   
			             <option >Sin dato</option>     
			            </select>
				   	</td>
				    <td width="25%">
						<div class="col-sm-10">
							<textarea id="comentario" name="comentario[]" class="form-control"></textarea>
						</div>	
				    </td>
				    <td width="15%">
			            <select  name="evaluacion[]"> 
			             <option>Si</option>
			             <option selected>No</option>      
			            </select>
				    </td>
				    <td width="20%">
						<div class="col-sm-10">
							{{Form::text('alertas[]', $numeroAlerta, array('id' => 'alertas', 'class' => 'form-control'))}}
						</div>
				    </td>
				</tr>
				</tbody>
		</table>
	<div id="templateRow4" class="hide">
		<table class="table table-bordered">
				<tbody>
					<tr>
					    <td width="25%">
					    <div class="col-sm-10">
					   		<input  name="fecha_evaluacion[]" class="form-control fecha2" type="text"/>
					    </div>
					    </td>
					    <td width="15%">
				            <select  name="caida[]"> 
				             <option>Si</option>
				             <option selected>No</option>   
				             <option >Sin dato</option>     
				            </select>
					   	</td>
					    <td width="25%">
							<div class="col-sm-10">
								<textarea id="comentario" name="comentario[]" class="form-control"></textarea>
							</div>	
					    </td>
					    <td width="15%">
				            <select  name="evaluacion[]"> 
				             <option>Si</option>
				             <option selected>No</option>      
				            </select>
					    </td>
					    <td width="20%">
							<div class="col-sm-10">
								<input type="text" class="form-control" id="alertas" name="alertas[]"/>
							</div>
					    </td>
					</tr>
				</tbody>
		</table>
	</div>-->

	<!--	<tfoot>
		    <tr>
		        <td colspan="4" class="text-left">
		            <a class="btn btn-default disabled" id="agregar2" onclick="addFila4();"><span class="glyphicon glyphicon-plus"></span> Agregar</a>
		        </td>
		    </tr>
		</tfoot>-->

	<fieldset>

		
		

		<button type="button" class="btn btn-primary pull-right" onclick="habilitarBoton('formEvaluacion', 'editarEvaluacion', 'guardarEvaluacion')" id="editarEvaluacion">Editar</button>
		<button type="submit" class="btn btn-primary pull-right" id="guardarEvaluacion" style="display:none;">Guardar</button>
		<!--<button type="button" class="btn btn-success pull-right" id="imprimirGeriatricosInicial" onclick="imprimirPDF('formEvaluacion')" >PDF</button>-->
		{{ Form::close() }}
	</div>
</div>
<!--</fieldset>-->

<script type="text/javascript">

$(function(){

	$("#editarEvaluacion").click(function(){
			console.log("editar ");
			$('#guardarEvaluacion').prop("disabled", false);
			$('#agregar2').removeClass('disabled');
			$('#guardarEvaluacion').removeClass('disabled');
		});
	$("#formEvaluacion").formValidation({
		excluded: ':disabled',
		framework: 'bootstrap',
		fields: {
		}
	}).on('err.field.fv', function(e, data) {
		if (data.fv.getSubmitButton()) data.fv.disableSubmitButtons(false);
	}).on('success.field.fv', function(e, data) {
		if (data.fv.getSubmitButton()) data.fv.disableSubmitButtons(false);
	}).on('submit', function(event) {
   	event.preventDefault();
    }).on("success.form.fv", function(evt){
		console.log("--- submit editarEncuestasSindromesGeriatricos ---");

		$("#formEvaluacion input[type='submit']").prop("disabled", false);
		
		evt.preventDefault(evt);
		$("#dvLoading").show();
		var $form = $(evt.target);
		
		var form = $(this).serializeArray();
		form.push({name:"rut", value:"{{ $rut }}"});
		$.ajax({
			url: $form.prop("action"),
			data: form,
			type: "post",
			dataType: "json",
			success: function(data){
				if(data.exito) bootbox.alert("<h4>"+data.exito+"</h4>", function(){
					verDatosPaciente();
					deshabilitarBoton('formEvaluacion', 'editarEvaluacion', 'guardarEvaluacion');
					cargarDatos();
				});
				if(data.error) bootbox.alert("<h4>"+data.error+"</h4>");
				$("#dvLoading").hide();
			},
			error: function(error){
				console.log(error);
				$("#dvLoading").hide();
			}
		});
		return false;
	});
	function cargarDatos()
	{
		$.ajax({
			url: "obtenerEvaluacion",
			data: "rut={{$rut}}",
			type: "post",
			dataType: "json",
			async:false,
			success: function(data){
				for(var i=0;i<data.length;i++)
				{
					$("[name='fecha_evaluacion[]']").eq(i).val(moment(data[i].fecha_evaluacion).format("DD-MM-YYYY"));
					$("[name='caida[]']").eq(i).val(data[i].caida=="si"?"Si":data[i].caida=="no"?"No":data[i].caida=="sin dato"?"Sin dato":"");
					$("[name='comentario[]']").eq(i).val(data[i].comentario);
					$("[name='evaluacion[]']").eq(i).val(data[i].evaluacion?"Si":"No");
					$("[name='id_evaluacion_caidas[]']").eq(i).val(data[i].id_evaluacion_caidas);
				}
			},
			error: function(error){
				console.log(error);
			}
		});
		
		
		$.ajax({
			url: "obtenerDatosHogar",
			data: "rut={{$rut}}",
			type: "post",
			dataType: "json",
			async:false,
			success: function(data){
				$("[name=frecuencia_mascota]").val(data.frecuencia_mascota_hogar);
				$("[name=numero_mascota]").val(data.n_mascotas_vive);
				$("[name=numero_personas]").val(data.n_personas_vive);
			},
			error: function(error){
				console.log(error);
			}
		});
	}
	
	
	<?php
	$nombre=Usuario::obtenerNombreTipoUsuario(Auth::user()->rut);
	if($nombre=="medico")
	{
	?>
	if(!$(".n1").hasClass("hide"))
		$(".n1").addClass("hide");
	if(!$(".n2").hasClass("hide"))
		$(".n2").addClass("hide");
	$("#editarEvaluacion").click(function(){
			console.log("medico");
			$("[name='fecha_evaluacion[]']").prop("disabled",true);
			$("[name='evaluacion[]']").prop("disabled",true);
			$("[name='alertas[]']").prop("disabled",true);
			$("[name='alertas[]']").prop("disabled",true);
			
			$("[name=frecuencia_mascota]").prop("disabled",false);
			$("[name=numero_mascota]").prop("disabled",false);
			$("[name=numero_personas]").prop("disabled",false);
			
			
	});
	$("[name=frecuencia_mascota]").prop("disabled",true);
	$("[name=numero_mascota]").prop("disabled",true);
	$("[name=numero_personas]").prop("disabled",true);
	
	<?php
	}
	else if($nombre=="evaluador_1")
	{
	?>
	if(!$(".n2").hasClass("hide"))
		$(".n2").addClass("hide");
	$("#editarEvaluacion").click(function(){
			console.log("ev1");
			$("[name='fecha_evaluacion[]']").prop("disabled",true);
			$("[name='caida[]']").prop("disabled",true);
			$("[name='comentario[]']").prop("disabled",true);
			$("[name='alertas[]']").prop("disabled",true);
			$("[name=frecuencia_mascota]").prop("disabled",true);
			
			
	});
	
	$("[name=numero_mascota]").prop("disabled",true);
	$("[name=numero_personas]").prop("disabled",true);
	<?php
	}
	else if($nombre=="evaluador_2")
	{
	?>
	$("#editarEvaluacion").click(function(){
			console.log("ev2");
			$("[name='fecha_evaluacion[]']").prop("disabled",true);
			$("[name='caida[]']").prop("disabled",true);
			$("[name='comentario[]']").prop("disabled",true);
			$("[name='evaluacion[]']").prop("disabled",true);
			$("[name='alertas[]']").prop("disabled",true);
			
			
	});
	$("[name=frecuencia_mascota]").prop("disabled",true);
	$("[name=numero_mascota]").prop("disabled",true);
	$("[name=numero_personas]").prop("disabled",true);
	$("#editarEvaluacion").css("display","none");
	<?php
	}
	?>
	cargarDatos();
});
</script>



