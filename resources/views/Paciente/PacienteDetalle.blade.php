@extends("Template/Template")

@section("titulo")
Detalle de paciente
@stop

@section("script")

<script>

var tableContactos=null;
var dateTime = null;
var numero = 0;

//se usa
var quitarNumero = function (numero) {
	console.log('numero: ', numero);
	$( "."+numero ).remove();
}

//se usa
var ingresarNumero = function () {
	console.log("rut: ", {{$rut}})
	var datos;

	datos = "	<div class='form-group error a-"+numero+"' style='margin-bottom: 0px;' >		<label for='paciente-telefono' class='col-sm-4 ontrol-label'>Telefono:</label>			<label for='paciente-tipo' class='col-sm-4 ontrol-label'>Tipo:</label>											</div>	<div class='form-group error a-"+numero+"' >				<div class='col-sm-4'>			<input class='form-control' type='text' name='telefono[]'>		</div>		<div class='col-sm-4'>					<select  name='tipo[]' class='form-control'>				<option value='Celular' selected>Celular</option><option value='Casa'>Casa</option>			</select>			</div>			<div class='col-sm-2'>					<button type='button' class='btn btn-danger' onclick=\"quitarNumero(\'a-"+numero+"\')\"><span class='glyphicon glyphicon-trash'></span></button>					</div>		";
	
	numero+=1;
	$( "#modalBodyContacto" ).append("<div id = 'telefonos' style='overflow-y: scroll; height= 300px !important;'></div>");
	$( "#telefonos" ).append( datos);
}

var modificarContacto = function(rutContacto, rutPaciente){
	console.log("hola");
	$.ajax({
		url: "modificarContacto",
		data: {rutPaciente: rutPaciente, rutContacto: rutContacto},
		headers: {        
		  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		type: "post",
		dataType: "json",
		success: function(data){
			console.log(data);
		},
		error: function(error){
			console.log(error);
		}
	});
};

var eliminarContacto = function(rutContacto, rutPaciente){
	$.ajax({
		url: "eliminarContacto",
		data: {idAlerta: idAlerta},
		headers: {        
		  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		type: "post",
		dataType: "json",
		success: function(data){
			console.log(data);
		},
		error: function(error){
			console.log(error);
		}
	});
};

var verDetalleAlerta = function(idAlerta, idEvento){
	$.ajax({
		url: "obtenerDetalleAlerta",
		data: {idAlerta: idAlerta},
		type: "post",
		dataType: "json",
		success: function(data){
			if(idEvento == eventos.PUERTA){
				tableAlertaPuertaDetalle.clear().draw();
				tableAlertaPuertaDetalle.rows.add(data).draw();
			}
			if(idEvento == eventos.NICTURIA){
				tableAlertaNicturiaDetalle.clear().draw();
				tableAlertaNicturiaDetalle.rows.add(data).draw();
			}
		},
		error: function(error){
			console.log(error);
		}
	});
};

var generarGraficoCaidas = function () {
    $.ajax({
        url: "generarEstadisticasCaidas",
        type: "post",
        data: {rut: "{{ $rut }}"},
        dataType: "json",
        success: function (data) {
            $('#grafico-12-meses').highcharts({
                plotOptions: {
                    series: {
                        animation: false
                    }
                },
                chart: {
                    type: 'column',
                    animation: false,
                    width: 300
                },
                title: {
                    text: 'Total de alertas caidas últimos 12 meses'
                },
                xAxis: {
                    type: 'category',
                    labels: {
                        rotation: -45,
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Total'
                    }
                },
                legend: {
                    enabled: false
                },
                tooltip: {
                    pointFormat: 'Total de alertas: {point.y}'
                },
                series: [{
                        name: 'Total',
                        data: data
                    }]
            });
        },
        error: function (error) {
            console.log(error);
        }
    });
};

var generarGrafico = function (dias, id, evento) {
    $.ajax({
        url: "generarEstadisticas",
        type: "post",
        data: {rut: "{{ $rut }}", dias: dias, evento: evento},
        dataType: "json",
        success: function (data) {
        	console.log(data);
            $('#' + id).highcharts({
                plotOptions: {
                    series: {
                        animation: false
                    }
                },
                chart: {
                    type: 'column',
                    animation: false
                },
                title: {
                    text: 'Total de alertas últimos ' + dias + " días"
                },
                xAxis: {
                    type: 'category',
                    labels: {
                        rotation: -45,
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    },
                    categories: data[0],
		            title: {
		                text: null
		            }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Total'
                    }
                },
                legend: {
                    enabled: false
                },
                tooltip: {
                    pointFormat: 'Total de alertas: {point.y}'
                },
                series: [{
                        name: 'Total',
                        data: data[1]
                    }]
            });
        },
        error: function (error) {
            console.log(error);
        }
    });
};




var generarGraficoCaidasDias = function (dias, id, evento) {
    $.ajax({
        url: "generarEstadisticas",
        type: "post",
        data: {rut: "{{ $rut }}", dias: dias, evento: evento},
        dataType: "json",
        async: false,
        success: function (data) {
        	console.log(data);
            $('#' + id).highcharts({
                plotOptions: {
                    series: {
                        animation: false
                    }
                },
                chart: {
                    type: 'column',
                    animation: false,
                    width: 500,
                    reflow: true
                },
                title: {
                    text: 'Total de alertas últimos ' + dias + " días"
                },
                xAxis: {
                    type: 'category',
                    labels: {
                        rotation: -45,
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    },
                    categories: data[0],
		            title: {
		                text: null
		            }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Total'
                    }
                },
                legend: {                    
		            layout: 'vertical',
		            align: 'right',
		            verticalAlign: 'middle',
		            borderWidth: 0		     
                },
                tooltip: {
                	//shared: true,
                    pointFormat: 'Total de alertas: {point.y}'
                },
                series: [{
                        name: data[4][0]["ubicacion"],
                        data: data[1]
                    },{
                        name: data[4][1]["ubicacion"],
                        data: data[2]
                    },{
                        name: data[4][2]["ubicacion"],
                        data: data[3]
                    }]
            });

        },
        error: function (error) {
            console.log(error);
        }

    });

    
};

//se usa
var obtenerContactoPaciente = function(){
	$.ajax({
		url: "obtenerContactoPaciente",
		data: { rut: "{{$rut}}" },
		headers: {        
		  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		type: "post",
		dataType: "json",
		success: function(data){
			tableContactos.clear().draw();
			tableContactos.rows.add(data).draw();
		}
	});
}


	

var agregarContactos = function(rut, idTipoUsuario){
	var tipoUsuario = (idTipoUsuario == "1") ? "Familiar" : "Medico";
	$('#formAgregarContacto'+tipoUsuario).trigger("reset");
	cargarListaPacientes(rut, "listaContactos"+tipoUsuario, idTipoUsuario);
	$('#rutUsuario_hdn'+tipoUsuario).val(rut);
	$('#modalAgregarContacto'+tipoUsuario).modal("show");
}

var listaContactosFamiliar = new Bloodhound({
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('rut'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
    	url: '%QUERY/obtenerRutNombreFamiliar',
    	filter: function(response) {
    		return response;
        }
    },
    limit: 10
});
listaContactosFamiliar.initialize();

var listaContactosMedico = new Bloodhound({
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('rut'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
    	url: '%QUERY/obtenerRutNombreMedico',
    	filter: function(response) {
    		return response;
        }
    },
    limit: 10
});
listaContactosMedico.initialize();


//se usa
var cargarListaPacientes = function(rutPaciente, idSelect, idTipoUsuario){
	$.ajax({
		url: "obtenerPacientesDeContacto",
		data: { rutPaciente: rutPaciente , idTipoUsuario: idTipoUsuario},
		headers: {        
		  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		type: "post",
		async: false, 
		dataType: "json",
		success: function(data){
			console.log(data);
			$("#"+idSelect).empty();
			if (data.length>0){
				for (var i = 0; i < data.length; i++) {        
					$("#"+idSelect).append('<option value="'+data[i][2]+'" selected="selected"><strong>'+data[i][2]+"-"+data[i][3]+"</strong> - "+data[i][0]+'</option>');
				};
			}

		}, error : function(error){ console.log(error); }
	});
}




/**

INFORMACION

**/

var obtenerDatosPaciente = function(){
	$.ajax({
		url: "obtenerDatosPaciente",
		data: {rut: "{{$rut}}"},
		headers: {        
		  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		type: "post",
		dataType: "json",
		success: function(data){
			$('#paciente-nombre').val(data.nombre);
			$('#paciente-apellido').val(data.paterno);
			$('#paciente-apellido_m').val(data.materno);
			$('#paciente-rut').val(data.rut);
			$('#paciente-dv').val(data.dv);
			$('#paciente-telefono').val(data.telefono);
			$('#paciente-correo').val(data.correo);
			$('#paciente-genero').val(data.genero);
			//$('#paciente-fechanacimiento').val(data.fecha);	
			$('#paciente-comuna').val(data.id_comuna);
			$('#paciente-calle').val(data.calle);
			$('#paciente-dpto').val(data.departamento);	
			$('#paciente-numero').val(data.numero);	

			$('#ficha-nombre-paciente').html(data.nombre+" "+data.paterno+" "+data.materno);

			$('#paciente-fechanacimiento').data("DateTimePicker").date(data.fecha);

			$('#paciente-tipoAlerta').val(data.id_alerta);

			$('#ac-timeGetUp').hide();
			$('#ac-tinetti').hide();
			$('#ac-timeGetUpFinal').hide();
			$('#ac-tinettiFinal').hide();
			$('#ac-iciq').hide();
			$('#ac-iciqFinal').hide();

			$('#ac-cognitiva').hide();
			$('#ac-cognitivaFinal').hide();
			$('#ac-gds').hide();
			$('#ac-gdsFinal').hide();
			$('#ac-conductamotriz').hide();
			$('#ac-conductamotrizFinal').hide();
			//esconder pestañas que no son de la alerte del paciente


			// if(data.id_alerta==1){//CAIDAS
			// 	$('#menu_Caidas').show();
			// 	$('#menu_Evaluacion').show();
			// 	$('#menu_Puertas').hide();
			// 	$('#menu_informe_Puertas').hide();
			// 	$('#menu_Nicturia').hide();
			// 	$('#menu_Micciones').hide();
			// 	$('#menu_Encuestas').show();
			// 	$('#menu_DatosMicciones').hide();
				
			// 	$('#ac-barthel').show();
			// 	$('#ac-charlson').show();
			// 	$('#ac-geriatrico').show();
			// 	$('#ac-timeGetUp').show();
			// 	$('#ac-lawton').show();
			// 	$('#ac-tinetti').show();

			// 	$('#ac-barthelFinal').show();
			// 	$('#ac-charlsonFinal').show();
			// 	$('#ac-geriatricoFinal').show();
			// 	$('#ac-timeGetUpFinal').show();
			// 	$('#ac-lawtonFinal').show();
			// 	$('#ac-tinettiFinal').show();

			// 	$('#ac-iciq').hide();
			// 	$('#ac-iciqFinal').hide();

			// 	$('#ac-cognitiva').hide();
			// 	$('#ac-cognitivaFinal').hide();
			// 	$('#ac-gds').hide();
			// 	$('#ac-gdsFinal').hide();
			// 	$('#ac-mmse').hide()
			// 	$('#ac-mmseFinal').hide();
			// 	$('#ac-conductamotriz').hide();
			// 	$('#ac-conductamotrizFinal').hide();
			// 	$('#InformeConclu').hide();

			// }
			// if(data.id_alerta==2){//ACCIONES REPETITIVAS
			// 	$('#menu_Puertas').show();
			// 	<?php
			// 	$nombre=App\Models\Usuario::obtenerNombreTipoUsuario(Auth::user()->rut);
			// 	if($nombre=="evaluador_1")
			// 	{
			// 	?>
			// 	$('#menu_informe_Puertas').show();
			// 	<?php
			// 	}
			// 	else
			// 	{
			// 	?>
			// 	$('#menu_informe_Puertas').hide();
			// 	<?php
			// 	}
			// 	?>
			// 	$('#menu_Caidas').hide();
			// 	$('#menu_Evaluacion').hide();
			// 	$('#menu_Nicturia').hide();
			// 	$('#menu_Micciones').hide();
			// 	$('#menu_Encuestas').show();
			// 	$('#menu_DatosMicciones').hide();

			// 	$('#ac-barthel').show();
			// 	$('#ac-charlson').show();
			// 	$('#ac-geriatrico').show();
			/*	$('#ac-timeGetUp').hide();
			// 	$('#ac-lawton').show();
			 	$('#ac-tinetti').hide();
			// 	$('#ac-iciq').hide();

			// 	$('#ac-barthelFinal').show();
			// 	$('#ac-charlsonFinal').show();
			// 	$('#ac-geriatricoFinal').show();
				$('#ac-timeGetUpFinal').hide();
			// 	$('#ac-lawtonFinal').show();
			 	$('#ac-tinettiFinal').hide();*/
			// 	$('#ac-iciqFinal').hide();

			// 	$('#ac-cognitiva').hide();
			// 	$('#ac-cognitivaFinal').hide();
			// 	$('#ac-gds').show();
			// 	$('#ac-gdsFinal').show();
			// 	$('#ac-mmse').show()
			// 	$('#ac-mmseFinal').show();
			// 	$('#ac-conductamotriz').show();
			// 	$('#ac-conductamotrizFinal').show();
			// 	$('#InformeConclu').show();
			// }
			// if(data.id_alerta==3){//NICTURIA
			// 	$('#menu_Nicturia').show();
			// 	$('#menu_Micciones').show();
			// 	$('#menu_Puertas').hide();
			// 	$('#menu_informe_Puertas').hide();
			// 	$('#menu_Caidas').hide();
			// 	$('#menu_Evaluacion').hide();
			// 	$('#menu_Encuestas').show();
			// 	$('#ac-mmse').hide()
			// 	$('#ac-mmseFinal').hide();
				
			// 	<?php
			// 	$nombre= App\Models\Usuario::obtenerNombreTipoUsuario(Auth::user()->rut);
				
			// 	if($nombre=="evaluador_2")
			// 	{
			// 	?>
			// 	$('#menu_DatosMicciones').show();
			// 	<?php
			// 	}
			// 	else
			// 	{
			// 	?>
			// 	$('#menu_DatosMicciones').hide();
			// 	<?php
			// 	}
			// 	?>
			// 	$('#ac-barthel').show();
			// 	$('#ac-charlson').show();
			// 	$('#ac-geriatrico').show();
			// 	$('#ac-iciq').show();

			// 	$('#ac-barthelFinal').show();
			// 	$('#ac-charlsonFinal').show();
			// 	$('#ac-geriatricoFinal').show();
			// 	$('#ac-iciqFinal').show();

			// 	$('#ac-timeGetUp').hide();
			// 	$('#ac-lawton').hide();
			// 	$('#ac-tinetti').hide();

			// 	$('#ac-timeGetUpFinal').hide();
			// 	$('#ac-lawtonFinal').hide();
			// 	$('#ac-tinettiFinal').hide();

			// 	$('#ac-cognitiva').hide();
			// 	$('#ac-cognitivaFinal').hide();
			// 	$('#ac-gds').hide();
			// 	$('#ac-gdsFinal').hide();
			// 	$('#ac-conductamotriz').hide();
			// 	$('#ac-conductamotrizFinal').hide();
			// 	$('#InformeConclu').hide();


			// }
			// if(!data.id_alerta){//NO TIENE EVENTO ASOCIADO
			// 	$('#menu_Nicturia').hide();
			// 	$('#menu_Micciones').hide();
			// 	$('#menu_Puertas').hide();
			// 	$('#menu_informe_Puertas').hide();
			// 	$('#menu_Caidas').hide();
			// 	$('#menu_Evaluacion').hide();
			// 	$('#menu_DatosMicciones').hide();

			// 	$('#ac-barthel').hide();
			// 	$('#ac-charlson').hide();
			// 	$('#ac-geriatrico').hide();
			// 	$('#ac-timeGetUp').hide();
			// 	$('#ac-lawton').hide();
			// 	$('#ac-tinetti').hide();
			//  	$('#ac-iciq').hide();

			// // 	$('#ac-barthelFinal').hide();
			// // 	$('#ac-charlsonFinal').hide();
			// // 	$('#ac-geriatricoFinal').hide();
			// // 	$('#ac-timeGetUpFinal').hide();
			// // 	$('#ac-lawtonFinal').hide();
			// // 	$('#ac-tinettiFinal').hide();
			//  	$('#ac-iciqFinal').hide();

			//  	$('#ac-cognitiva').hide();
			//  	$('#ac-cognitivaFinal').hide();
			//  	$('#ac-gds').hide();
			//  	$('#ac-gdsFinal').hide();
			//  	$('#ac-conductamotriz').hide();
			//  	$('#ac-conductamotrizFinal').hide();
			// 	$('#InformeConclu').hide();

			// }
			// //Desplegar u ocultar mensaje de que se debe agregar el género del paciente
			// if(data.genero=='D'){
			// 	$('.tiene-genero').show();
			// 	$('.tiene-genero-final').show();
			// }else{
			// 	$('.tiene-genero').hide();
			// 	$('.tiene-genero-final').hide();
			// }
		}
	});
}

var verDatosPaciente = function(){
	obtenerDatosPaciente();
	obtenerEncuestas();
	$('#formEditarPaciente,#formConductaMotrizInicial,#formConductaMotrizFinal,#formEvaluacionCognitivaFinal,#formMicciones,#formEvaluacion,#formEvaluacionCognitivaInicial,#formAntPersonales, #formAntFamiliares, #formTimedGetUpInicial, #formTimedGetUpFinal, #formSindromesGeriatricosInicial, #formSindromesGeriatricosFinal, #formIndiceBarthelInicial, #formIndiceBarthelFinal, #formCharlsonInicial, #formCharlsonFinal, #formTinetiInicial, #formTinetiFinal, #formularioGDS, #formularioGDSFinal, #formWhoqol, #formWhoqolA3').find('input[type=text], textarea,  select, input[type=radio], input[type=number], input[type=date]').attr('disabled','disabled'); 
	
	$(".btn-radio-group").addClass('disabled').prop('disabled',true);
}



var obtenerEncuestas = function(){
	$.ajax({
		url: "obtenerEncuestas",
		data: {rut: "{{$rut}}"},
		headers: {        
		  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		type: "post",
		dataType: "json",
		async: false,
		success: function(data){
			console.log(" -- Datos encuestas -- ");
			console.log(data);

			if(data.testCharlson){
				//console.log("estoy en el if charlson");
				setCharlsonInicio(data.testCharlson);
			}

			if(data.indiceBarthel){
				//console.log("estoy en el if barthel");
				setIndiceBarthel(data.indiceBarthel);			
			}

			if(data.testLawtonBrody){
				//console.log("estoy en el if lawton");
				setLawtonBrody(data.testLawtonBrody);			
			}

			if(data.sindromesGeriatricos){	
				//console.log("estoy en el if geriatrico");			
				setSindromesGeriatricos(data.sindromesGeriatricos);			
			}

			if(data.mmseInicial){
				//console.log("estoy en el if mmse");
				setMmseInicial(data.mmseInicial);
			}

			if(data.testWhoqol){
				//console.log("estoy en el if Whoqol");
				setWhoqol(data.testWhoqol);
			}

			if(data.testWhoqolA3){
				//console.log("estoy en el if whoqolA3");
				setWhoqolA3(data.testWhoqolA3);
			}


			/*Aca no se usan*/

			// if(data.testGetUp){
			// 	setTestGetUpInicial(data.testGetUp);
			// }

			// if(data.testGetUpFinal){
			// 	setTestGetUpFinal(data.testGetUpFinal);
			// }

			

			// if(data.sindromesGeriatricosFinal){
			// 	setSindromesGeriatricosFinal(data.sindromesGeriatricosFinal);		
			// }


			// if(data.indiceBarthelFinal){
			// 	setIndiceBarthelFinal(data.indiceBarthelFinal);	
			// }

			

			// if(data.testLawtonBrodyFinal){
			// 	setLawtonBrodyFinal(data.testLawtonBrodyFinal);	
			// }

			// if(data.testTinetiEquilibrio){
			// 	setTinetiEquilibrioInicio(data.testTinetiEquilibrio);
			// }

			// if(data.testTinetiEquilibrioFinal){
			// 	setTinetiEquilibrioFinal(data.testTinetiEquilibrioFinal);	
			// }

			// if(data.testTinetiMarcha){
			// 	setTinetiMarchaInicio(data.testTinetiMarcha);		
			// }

			// if(data.testTinetiMarchaFinal){
			// 	setTinetiMarchaFinal(data.testTinetiMarchaFinal);	
			// }

			

			// if(data.testCharlsonFinal){
			// 	setCharlsonFinal(data.testCharlsonFinal);
			// }

			// if(data.testIciq){
			// 	setIciqInicio(data.testIciq);
			// }

			// if(data.testIciqFinal){
			// 	setIciqFinal(data.testIciqFinal);
			// }
			// if(data.evaluacionCognitivaIni){
			// 	setEvaluacionCognitivaIni(data.evaluacionCognitivaIni);
			// }

			// if(data.evaluacionCognitivaFinal){
			// 	setEvaluacionCognitivaFin(data.evaluacionCognitivaFinal);
			// }
			// if(data.gdsInicial){
			// 	setGDSInicial(data.gdsInicial);
			// }

			// if(data.gdsFinal){
			// 	setGDSFinal(data.gdsFinal);
			// }

			
			// if(data.mmseFinal){
			// 	setMmseFinal(data.mmseFinal);
			// }
			// if(data.ConductaMotrizInicial){
			// 	setConductaMotrizInicial(data.ConductaMotrizInicial);
			// }

			// if(data.ConductaMotrizFinal){
			// 	setConductaMotrizFinal(data.ConductaMotrizFinal);
			// }
			// if(data.Conclu_acciones_repetidas){
			// 	setInformeAccionesRepetidas(data.Conclu_acciones_repetidas);
			// }

		}
	});
}





/**

CONTACTO

**/

$(function(){
	//$("#menuPaciente").collapse();

	$('#modalAgregarContactoFamiliar').on('hidden.bs.modal', function () {
  		$( "#telefonos" ).remove();
	});


	$('#charlson').click(function () {
		$("#modalCharlson").modal("show");
		
	});

	$('#barthel').click(function () {
		$("#modalBarthel").modal("show");
	});

	$('#lawton').click(function () {
		$("#modalLawton").modal("show");
	});

	$('#sinGer').click(function () {
		$("#modalSinGer").modal("show");
	});

	$('#mmse').click(function () {
		$("#modalMMSE").modal("show");
	});

	$('#whoqol').click(function () {
		$("#modalWhoqol").modal("show");
	});

	$('#whoqolA3').click(function () {
		$("#modalWhoqolA3").modal("show");
	});

	$('#EQ5D').click(function () {
		$("#modalEQ5D-primero").modal("show");
	});

	document.getElementById('buscar-rut-familiar').addEventListener('keyup', function(){
		console.log($('#buscar-rut-familiar').val());

		$.ajax({
			url: "cargarDatosPaciente",
			data: { rut: $('#buscar-rut-familiar').val() },
			headers: {        
			  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			type: "post",
			dataType: "json",
			success: function(data){
				console.log(data);
				console.log("rut: ", {{$rut}})
				$( "#telefonos" ).remove();
				if(data == false){
					document.getElementById('agregarPacienteNombre').value = "";
					document.getElementById('agregarPacienteApellidoP').value = "";
					document.getElementById('agregarPacienteApellidoM').value = "";
					document.getElementById('agregarPacienteRut').value = "";
					document.getElementById('agregarPacienteParentesco').value = "";
					document.getElementById('agregarPacienteCorreo').value = "";

					$( "#telefonos" ).remove();
				}else{
					$('#rutPaciente').val({{$rut}});
					document.getElementById('agregarPacienteNombre').value = data.nombre;
					document.getElementById('agregarPacienteApellidoP').value = data.primerApellido;
					document.getElementById('agregarPacienteApellidoM').value = data.segundoApellido;
					document.getElementById('agregarPacienteRut').value = data.rut;
					document.getElementById('agregarPacienteParentesco').value = data.parentesco;
					document.getElementById('agregarPacienteCorreo').value = data.correo;

					$( "#modalBodyContacto" ).append("<div id = 'telefonos' style='overflow-y: scroll; height= 300px !important;'></div>");
					console.log(data.agenda);
					for(i=0; i< data.agenda.length; i++){

						datos = "<div class='form-group error i-"+i+"' style='margin-bottom: 0px;'><label for='paciente-telefono' class='col-sm-4 ontrol-label'>Teléfono:</label><label for='paciente-tipo' class='col-sm-4 ontrol-label'>Tipo:</label></div><div class='form-group error i-"+i+"'>	<div class='col-sm-4'><input class='form-control' type='text' name='telefono[]'  value='";

						datos += data.agenda[i].telefono;

						datos +="'></div><div class='col-sm-4'><select name='tipo[]' class='form-control'>"

						if (data.agenda[i].tipo_telefono == 'Celular') {
							datos += "		<option value='Celular' selected>Celular</option><option value='Casa'>Casa</option>"
						}else{
							datos += "		<option value='Celular'>Celular</option><option value='Casa' selected>Casa</option>"
						}
						datos += "</select></div> <div class='col-sm-2'>					<button type='button' class='btn btn-danger' onclick=\"quitarNumero(\'i-"+i+"\')\"><span class='glyphicon glyphicon-trash'></span></button>					</div>	"
						
						$( "#telefonos" ).append( datos);
								 
							
							 
						// document.getElementById('agregarPacienteTelefono').value = data.agenda[i].telefono;
						// document.getElementById('agregarPacienteTipo').value = data.agenda[i].tipo_telefono;
					}

				}
				
			},
			error: function(error){
		        console.log("error cargarDatosPaciente" );
		    }
		});


	});

	// Encuentro los radios del formulario y les  pongo color al fondo del label 
	$('#formIndiceBarthelInicial').find('input[type=radio]').change(function() {
		name = $(this).attr('name');
		$('#formIndiceBarthelInicial').find('input[type=radio][name='+name+']').each(function (index, element) {
    		$(this).parent().removeClass('btn-default-si-label');
		});
        
       $(this).parent().addClass('btn-default-si-label').siblings().removeClass('btn-default-si-label');

    });

    $('#formIndiceBarthelFinal').find('input[type=radio]').change(function() {
		name = $(this).attr('name');
		$('#formIndiceBarthelFinal').find('input[type=radio][name='+name+']').each(function (index, element) {
    		$(this).parent().removeClass('btn-default-si-label');
		});
        
       $(this).parent().addClass('btn-default-si-label').siblings().removeClass('btn-default-si-label');

    });

    $('#formLawtonBrodyInicial').find('input[type=radio]').change(function() {
		name = $(this).attr('name');
		$('#formLawtonBrodyInicial').find('input[type=radio][name='+name+']').each(function (index, element) {
    		$(this).parent().removeClass('btn-default-si-label');
		});
        
       $(this).parent().addClass('btn-default-si-label').siblings().removeClass('btn-default-si-label');

    });

    $('#formLawtonBrodyFinal').find('input[type=radio]').change(function() {
		name = $(this).attr('name');
		$('#formLawtonBrodyFinal').find('input[type=radio][name='+name+']').each(function (index, element) {
    		$(this).parent().removeClass('btn-default-si-label');
		});
        
       $(this).parent().addClass('btn-default-si-label').siblings().removeClass('btn-default-si-label');

    });

    $('#formTinetiInicial').find('input[type=radio]').change(function() {
		name = $(this).attr('name');
		$('#formTinetiInicial').find('input[type=radio][name='+name+']').each(function (index, element) {
    		$(this).parent().removeClass('btn-default-si-label');
		});
        
       $(this).parent().addClass('btn-default-si-label').siblings().removeClass('btn-default-si-label');

    });

    $('#formTinetiFinal').find('input[type=radio]').change(function() {
		name = $(this).attr('name');
		$('#formTinetiFinal').find('input[type=radio][name='+name+']').each(function (index, element) {
    		$(this).parent().removeClass('btn-default-si-label');
		});
        
       $(this).parent().addClass('btn-default-si-label').siblings().removeClass('btn-default-si-label');

    });

    $('#formCharlsonInicial input[type=checkbox], #formCharlsonInicial input[type=checkbox]').change(function() {
		id = $(this).attr('id');
		if( $('#'+id).is(':checked') ) $('#'+id).parent().addClass('btn-default-si-label');
    	else $('#'+id).parent().removeClass('btn-default-si-label');

    });

    $('#formCharlsonFinal input[type=checkbox], #formCharlsonFinal input[type=checkbox]').change(function() {
		id = $(this).attr('id');
		if( $('#'+id).is(':checked') ) $('#'+id).parent().addClass('btn-default-si-label');
    	else $('#'+id).parent().removeClass('btn-default-si-label');

    });

    $('#formICIQ-SFInicial input[type=checkbox], #formICIQ-SFInicial input[type=checkbox]').change(function() {
		id = $(this).attr('id');
		if( $('#'+id).is(':checked') ) $('#'+id).parent().addClass('btn-default-si-label');
    	else $('#'+id).parent().removeClass('btn-default-si-label');
    });
	
	$('#formICIQ-SFInicial').find('input[type=radio]').change(function() {
		name = $(this).attr('name');
		$('#formICIQ-SFInicial').find('input[type=radio][name='+name+']').each(function (index, element) {
    		$(this).parent().removeClass('btn-default-si-label');
		});
        
       $(this).parent().addClass('btn-default-si-label').siblings().removeClass('btn-default-si-label');

    });

    $('#formICIQ-SFFinal input[type=checkbox], #formICIQ-SFFinal input[type=checkbox]').change(function() {
		id = $(this).attr('id');
		if( $('#'+id).is(':checked') ) $('#'+id).parent().addClass('btn-default-si-label');
    	else $('#'+id).parent().removeClass('btn-default-si-label');

    });
	
	$('#formICIQ-SFFinal').find('input[type=radio]').change(function() {
		name = $(this).attr('name');
		$('#formICIQ-SFFinal').find('input[type=radio][name='+name+']').each(function (index, element) {
    		$(this).parent().removeClass('btn-default-si-label');
		});
        
       $(this).parent().addClass('btn-default-si-label').siblings().removeClass('btn-default-si-label');

    });

    $('#formEvaluacionCognitivaInicial').find('input[type=radio]').change(function() {
		name = $(this).attr('name');
		$('#formEvaluacionCognitivaInicial').find('input[type=radio][name='+name+']').each(function (index, element) {
    		$(this).parent().removeClass('btn-default-si-label');
		});
        
       $(this).parent().addClass('btn-default-si-label').siblings().removeClass('btn-default-si-label');

    });

    $('#formEvaluacionCognitivaFinal').find('input[type=radio]').change(function() {
		name = $(this).attr('name');
		$('#formEvaluacionCognitivaFinal').find('input[type=radio][name='+name+']').each(function (index, element) {
    		$(this).parent().removeClass('btn-default-si-label');
		});
        
       $(this).parent().addClass('btn-default-si-label').siblings().removeClass('btn-default-si-label');

    });
    $('.fecha_gds').datetimepicker({
		locale: "es",
		format: "DD-MM-YYYY",
		maxDate: moment().add('days', 1).format("YYYY/MM/DD"),
		useCurrent: false
	});
	$('.fecha').datetimepicker({
		locale: "es",
		format: "DD-MM-YYYY",
		maxDate: moment().add('days', 1).format("YYYY/MM/DD"),
		useCurrent: false
	});

	$('.fecha2').datetimepicker({
		locale: "es",
		format: "DD-MM-YYYY",
		maxDate: moment().add('days', 1).format("YYYY/MM/DD"),
		useCurrent: false
	});

	/*$('.fecha3').datetimepicker({
		locale: "es",
		format: "hh:mm:ss",
		maxDate: moment().add('days', 1).format("YYYY/MM/DD"),
		useCurrent: false
	});*/

	$('.fechaEncuesta').datetimepicker({
		locale: "es",
		format: "DD-MM-YYYY",
		maxDate: moment().add('days', 1).format("YYYY/MM/DD"),
		useCurrent: true,
	});

	$('#paciente-fechanacimiento').datetimepicker({
		locale: "es",
		format: "DD-MM-YYYY",
		maxDate: moment().add('days', 1).format("YYYY/MM/DD"),
		useCurrent: false
	});

	verDatosPaciente(); 

	tableContactos = $('#contactos').DataTable({	
		"iDisplayLength": 10,
		"bJQueryUI": true,
		"searching": true,
		"ordering": false,
		"info": true,
		"bAutoWidth" : false,
		"responsive": true,
		"oLanguage": {
			"sUrl": "{{URL::to('/')}}/js/spanish.json"
		}
	});

	obtenerContactoPaciente();

	// busca sugerencia para modal familiar
	$("#buscar-rut-familiar").typeahead(null,{
		source: listaContactosFamiliar.ttAdapter(),
		display: 'rut',
		templates: {
		    empty: [
		      '<div class="empty-message">',
		        'No hay resultados',
		      '</div>'
		    ].join('\n'),
		    suggestion: function(data) {
		    	console.log(data);

		    	$('#agregarALista_btnFamiliar').prop("disabled", true);
    			return '<p><strong>' + data.rut + '</strong> – ' + data.nombre + '</p>';
			}
	  	} 
	}).on('typeahead:selected', function(event, selection) {
		// console.log("evento: ",event);
		// console.log("selecion: ",selection);
		// console.log("rut: ", {{$rut}})
		$( "#telefonos" ).remove();

		document.getElementById('agregarPacienteNombre').value = selection.nombre;
		document.getElementById('agregarPacienteApellidoP').value = selection.primerApellido;
		document.getElementById('agregarPacienteApellidoM').value = selection.segundoApellido;
		document.getElementById('agregarPacienteRut').value = selection.rut;
		document.getElementById('agregarPacienteParentesco').value = selection.parentesco;
		document.getElementById('agregarPacienteCorreo').value = selection.correo;

		$( "#modalBodyContacto" ).append("<div id = 'telefonos' style='overflow-y: scroll; height= 300px !important;'  ></div>");
		console.log("agenda: ",selection.agenda);
		for(i=0; i< selection.agenda.length; i++){
			datos = "<div class='form-group error a-"+numero+"' style='margin-bottom: 0px;'><label for='paciente-telefono' class='col-sm-4 ontrol-label'>Teléfono:</label><label for='paciente-tipo' class='col-sm-4 ontrol-label'>Tipo:</label></div><div class='form-group error a-"+numero+"'>	<div class='col-sm-4'><input class='form-control' type='text' name='telefono[]'  value='";

			datos += selection.agenda[i].telefono;

			datos +="'></div><div class='col-sm-4'><select  name='tipo[]' class='form-control'>"

			if (selection.agenda[i].tipo_telefono == 'Celular') {
				datos += "		<option value='Celular' selected>Celular</option><option value='Casa'>Casa</option>"
			}else{
				datos += "		<option value='Celular'>Celular</option><option value='Casa' selected>Casa</option>"
			}
			datos += "</select></div><div class='col-sm-2'>					<button type='button' class='btn btn-danger' onclick=\"quitarNumero(\'a-"+numero+"\')\"><span class='glyphicon glyphicon-trash'></span></button>					</div>"
						
			$( "#telefonos" ).append( datos);
		}

		$('#rutPaciente').val({{$rut}});
		
	});

	// busca sugerencia para modal medico
	$("#buscar-rut-medico").typeahead(null, {
		source: listaContactosMedico.ttAdapter(),
		display: 'rut',
		templates: {
		    empty: [
		      '<div class="empty-message">',
		        'No hay resultados',
		      '</div>'
		    ].join('\n'),
		    suggestion: function(data) {
		    	$('#agregarALista_btnMedico').prop("disabled", true);
    			return '<p><strong>' + data.rut + '</strong> – ' + data.nombre + '</p>';
			}
	  	} 
	}).on('typeahead:selected', function(event, selection) {
		var rutdv = selection.rut;
		rutdv = rutdv.split("-");
		$('#rut_hdnMedico').val(rutdv[0]);
		$('#dv_hdnMedico').val(rutdv[1]);
		$('#nombre_hdnMedico').val(selection.nombre);
		$('#agregarALista_btnMedico').prop("disabled", false);
	});
	
	//se usa
	$("#formAgregarContactoFamiliar").submit(function(){ 
      	var form=$(this);

      	bootbox.confirm("<h4>¿Seguro que quiere modificar la lista de familiares?</h4>", function(result) {
	        if (result) {

	        	$('#modalAgregarContactoFamiliar').modal("hide");
	        	$(this).removeData('modalAgregarContactoFamiliar');
	          	$("#listaContactosFamiliar").each(function(){
	            	$('#listaContactosFamiliar option').prop('selected',true);
	          	});
				var datos = form.serializeArray(); 
				datos.push({name: "idTipoUsuario", value: "1"});
	          	
	          	$.ajax({
		            url: form.prop("action"),
		            data: datos,
		            type: form.prop("method"),
		            dataType: "json",
		            success: function(data){
		              	if(data.error) bootbox.alert("<h4>"+data.error+"</h4>");
		              	if(data.exito) bootbox.alert("<h4>"+data.exito+"</h4>", function(){
		                	location.reload();
		              	});
		            }, 
		            error: function(error){
		              console.log(error);
		            }
	          	});
	        }
      	});
      	return false;
    });

	$("#formAgregarContactoMedico").submit(function(){ 
      	var form=$(this);
      	bootbox.confirm("<h4>¿Seguro que quiere modificar la lista médicos?</h4>", function(result) {
	        if (result) {
	        	$('#modalAgregarContactoMedico').modal("hide");
	          	$("#listaContactosMedico").each(function(){
	            	$('#listaContactosMedico option').prop('selected',true);
	          	});
	          	var datos = form.serializeArray(); 
				datos.push({name: "idTipoUsuario", value: "2"});
	          	$.ajax({
		            url: form.prop("action"),
		            data: datos,
		            type: form.prop("method"),
		            dataType: "json",
		            success: function(data){
		              	if(data.error) bootbox.alert("<h4>"+data.error+"</h4>");
		              	if(data.exito) bootbox.alert("<h4>"+data.exito+"</h4>", function(){
		                	location.reload();
		              	});
		            }, 
		            error: function(error){
		              console.log(error);
		            }
	          	});
	        }
      	});
      	return false;
    });

	$("#btn-editar-dp").on("click", function(){
		$('#formEditarPaciente').find('input[type=text], textarea, select[name!="dv"], input[type=radio]').removeAttr('disabled'); 
		$('#btn-editar-dp').hide();
		$('#btn-guardar-dp').show();
	});

	$("#btn-editar-ap").on("click", function(){
		$('#formAntPersonales').find('input[type=text], textarea, select[name!="dv"], input[type=radio]').removeAttr('disabled'); 
		$('#btn-editar-ap').hide();
		$('#btn-guardar-ap').show();
	});


	$("#formEditarPaciente").formValidation({
		excluded: ':disabled',
		framework: 'bootstrap',
		fields: {
			nombre: {
				validators:{
					notEmpty: {
						message: 'El nombre es obligatorio'
					}
				}
			},
			apellido_p: {
				validators:{
					notEmpty: {
						message: 'El apellido paterno es obligatorio'
					}
				}
			}/*
			apellido_m: {
				validators:{
					notEmpty: {
						message: 'El apellido materno es obligatorio'
					}
				}
			}*/,
			telefono: {
				validators:{
					notEmpty: {
						message: 'El teléfono es obligatorio'
					},
					integer: {
						message: "Debe ingresar solo números"
					}
				}
			},
			numero: {
				validators:{
					integer: {
						message: "Debe ingresar solo números"
					}
				}
			}
		}
	}).on('err.field.fv', function(e, data) {
		if (data.fv.getSubmitButton()) data.fv.disableSubmitButtons(false);
	}).on('success.field.fv', function(e, data) {
		if (data.fv.getSubmitButton()) data.fv.disableSubmitButtons(false);
	}).on("success.form.fv", function(evt){
		$("#formEditarPaciente input[type='submit']").prop("disabled", false);
		evt.preventDefault(evt);
		$("#dvLoading").show();
		var $form = $(evt.target);
		$.ajax({
			url: $form.prop("action"),
			data: $form.serialize(),
			type: "post",
			dataType: "json",
			success: function(data){
				if(data.exito) bootbox.alert("<h4>"+data.exito+"</h4>", function(){
					verDatosPaciente();
					$('#btn-editar-dp').show();
					$('#btn-guardar-dp').hide();
				});
				if(data.error) bootbox.alert("<h4>"+data.error+"</h4>");
				$("#dvLoading").hide();
			},
			error: function(error){
				console.log(error);
				$("#dvLoading").hide();
			}
		});
	});

	$("#formAntPersonales").formValidation({
		excluded: ':disabled',
		framework: 'bootstrap',
		fields: {
			fumaN: {
				validators:{
					integer: {
						message: "Debe ingresar solo números"
					}
				}
			},
			alcoholN: {
				validators:{
					integer: {
						message: "Debe ingresar solo números"
					}
				}
			},
			talla: {
				validators:{
					integer: {
						message: "Debe ingresar solo números"
					}
				}
			},
			peso: {
				validators:{
					numeric: {
						message: "Debe ingresar solo números"
					}
				}
			},
			imc: {
				validators:{
					numeric: {
						message: "Debe ingresar solo números"
					}
				}
			},
			cintura: {
				validators:{
					numeric: {
						message: "Debe ingresar solo números"
					}
				}
			},
			estatura: {
				validators:{
					numeric: {
						message: "Debe ingresar solo números"
					}
				}
			},
			psistolica: {
				validators:{
					numeric: {
						message: "Debe ingresar solo números"
					}
				}
			},
			diastolica: {
				validators:{
					numeric: {
						message: "Debe ingresar solo números"
					}
				}
			}
		}
	}).on('err.field.fv', function(e, data) {
		if (data.fv.getSubmitButton()) data.fv.disableSubmitButtons(false);
	}).on('success.field.fv', function(e, data) {
		if (data.fv.getSubmitButton()) data.fv.disableSubmitButtons(false);
	}).on("success.form.fv", function(evt){
		$("#formEditarPaciente input[type='submit']").prop("disabled", false);
		evt.preventDefault(evt);
		$("#dvLoading").show();
		var $form = $(evt.target);
		$.ajax({
			url: $form.prop("action"),
			data: $form.serialize(),
			type: "post",
			dataType: "json",
			success: function(data){
				if(data.exito) bootbox.alert("<h4>"+data.exito+"</h4>", function(){
					$("#formAntPersonales").data('formValidation').resetForm();
					$('#formAntPersonales').find('input[type=text], textarea,  select, input[type=radio]').attr('disabled','disabled'); 
					$('#btn-editar-ap').show();
					$('#btn-guardar-ap').hide();
				});
				if(data.error) bootbox.alert("<h4>"+data.error+"</h4>");
				$("#dvLoading").hide();
			},
			error: function(error){
				console.log(error);
				$("#dvLoading").hide();
			}
		});
	});

});



</script>

@stop

@section("section")
<fieldset>
	<legend style="font-size: 15px; background-color: rgb(76, 215, 200); padding: 10px; border-radius: 5px; font-style: italic; color: rgb(85, 85, 85);" >Paciente: <span id="ficha-nombre-paciente" style="font-weight: bold;"></span></legend>
	<ul class="nav nav-tabs">
	  <li class="active"><a href="#tab-info" data-toggle="tab" >Información paciente</a></li>
	  <li><a href="#tab-contacto" data-toggle="tab">Información contactos</a></li>
	  @if(App\Models\Usuario::obtenerTipoEvento($rut) == 1)
	  <li id="menu_Caidas"><a href="#tab-caida" data-toggle="tab">Caídas</a></li>
	  @endif
	  @if(App\Models\Usuario::obtenerTipoEvento($rut) == 3)
	  <li id="menu_Nicturia"><a href="#tab-nicturia" data-toggle="tab">Nicturia</a></li>
	  @endif
	  @if(App\Models\Usuario::obtenerTipoEvento($rut) == 2)
	  
	  <li id="menu_Puertas"><a href="#tab-puerta" data-toggle="tab">Acciones repetidas</a></li>
	  @endif
	  <li id="menu_Encuestas"><a href="#tab-encuestas" data-toggle="tab">Encuestas</a></li>
	  @if(App\Models\Usuario::obtenerTipoEvento($rut) == 1)
	  <li id="menu_Evaluacion"><a href="#tab-evaluacion" data-toggle="tab">Evaluación</a></li>
	  @endif
	  @if(App\Models\Usuario::obtenerTipoEvento($rut) == 3)
	  <li id="menu_Micciones"><a href="#tab-micciones" data-toggle="tab">Cartilla de micciones</a></li>
	  <li id="menu_DatosMicciones"><a href="#tab-datos-micciones" data-toggle="tab">Datos</a></li>
	  @endif
	  @if(App\Models\Usuario::obtenerTipoEvento($rut) == 2)

	  <li id="menu_informe_Puertas"><a href="#tab-informe-puerta" data-toggle="tab">Resumen acciones repetidas</a></li>

	  @endif
	</ul>

	<div class="tab-content" style="background-color: white; padding: 10px; border-left: 1px solid rgb(221, 221, 221); border-right: 1px solid rgb(221, 221, 221); border-bottom: 1px solid rgb(221, 221, 221);">

		<div class="tab-pane" id="tab-contacto" style="width: auto; padding-top: 10px;">		
			@include('Paciente/PacienteDetalle-contactos')
		</div>	

		<div class="tab-pane active" id="tab-info" style="width: auto; padding-top: 10px;">
			@include('Paciente/PacienteDetalle-informacion', ["datosPersonales" => $datosPersonales, "viveCon" => $viveCon])
		</div>	
		

		

		<div class="tab-pane" id="tab-encuestas" style="width: auto; padding-top: 10px;">
			@include('Paciente/PacienteDetalle-encuestas', ["rut" => $rut,"NombretipoUsuario"=>$NombretipoUsuario])
			<div id="InformeConclu">
			
            </div>
		</div>	

		@if(App\Models\Usuario::obtenerTipoEvento($rut) == 1)
		<div class="tab-pane" id="tab-evaluacion" style="width: auto; padding-top: 10px;">
			@include('Paciente/PacienteDetalle-evaluacion', ["rut" => $rut,"evaluacion"=>$evaluacion,"numeroAlerta"=>$numeroAlerta])
		</div>	
		@endif
		
		
	</div>
</fieldset>
@stop
