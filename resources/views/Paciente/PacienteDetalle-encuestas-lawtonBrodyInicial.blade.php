<!--<fieldset>
	<legend class="negrita" style="font-size: 16px;">Timed Get Up and Go Test Inicial</legend>-->
<div style="text-align: left;">
		
	<div class="form">
		{{ Form::open(array('url' => 'paciente/editarEncuestasLawtonBrody', 'method' => 'post', 'role' => 'form', 'id' => 'formLawtonBrodyInicial')) }}
		<div>
			<input name="inicio" value="true" hidden/>
			<input name="tipo-encuesta" value="LawtonBrody" hidden/>
		</div>

		<h4>Índice LawtonBrody (Actividades instrumentales de la vida diaria)</h4>

		<div class="row">
			<div class="col-sm-6">
				<div class="form-group ">
					<label for="LawtonBrodyInicial-fecha-encuesta" class="control-label" style="width:100%;">Fecha</label>
					<input id="LawtonBrodyInicial-fecha-encuesta" name="fecha-encuesta" class="form-control fecha" type="text"/>	
				</div>
			</div>
            <div class="col-sm-6">
				<div class="form-group error tiene-genero">			
					<label for="paciente-genero" class="control-label text-danger"><h4>Debe seleccionar gérero de paciente <small class="text-danger">(Pestaña Información)</small></h4></label>
				</div>
			</div>
		</div>
		
		<fieldset>
			
	                
			<table class="table table-bordered" id="tbl_1">
				<tbody>
					<tr>
				      	<td colspan="1">
				      		Total:
				      	</td>
				      	<td>
				      		<input type="number" min="0" name="indiceLawtonBrody-total" class="form-control indiceLawtonBrodyInicial-total"  readonly >
				      	</td>
				    </tr>
				</tbody>
				<tbody>
					<tr>
					    <td colspan="2"><b>Teléfono</b></td>
					</tr>
				    <tr>
				      	<td style="width:80%;">
					    <p>Utilizar el telefono por iniciativa propia, buscar y marcar los numeros</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceLawtonBrodyInicial-p-1" name="p-1" value="1" valor="1"/> 1
				            </label>
				      	</td>
				      
				    </tr>
                    <tr>
				      	<td>
					    <p>Sabe marcar números desconocidos</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceLawtonBrodyInicial-p-2" name="p-1" value="2" valor="1" /> 1
				            </label>
				      	</td>
				      	
				    </tr>
                    <tr>
				      	<td>
					    <p>Contesta al teléfono, pero no sabe marcar</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceLawtonBrodyInicial-p-3" name="p-1" value="3" valor="1" /> 1
				            </label>
				      	</td>
				      
				    </tr>
                    <tr>
				      	<td>
					    <p>No utiliza el teléfono en absoluto</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceLawtonBrodyInicial-p-4" name="p-1" value="4" valor="0" /> 0
				            </label>
				      	</td>
				      	
				    </tr>
				      

				</tbody>
				    
			</table>
                        
            <table class="table table-bordered" id="tbl_2">
				<tbody>
					<tr>
					    <td colspan="2"><b>Compras</b></td>
					</tr>
					<tr>
				      	<td style="width:80%;">
					    <p>Realiza todas las compras necesarias de manera independiente</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceLawtonBrodyInicial-p-5" name="p-2" value="5" valor="1"/> 1
				            </label>
				      	</td>
				      
				    </tr>
                    <tr>
				      	<td>
					    <p>Sólo sabe hacer pequeñas compras</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceLawtonBrodyInicial-p-6" name="p-2" value="6" valor="0" /> 0
				            </label>
				      	</td>
				      	
				    </tr>
                    <tr>
				      	<td>
					    <p>Ha de ir acompañado para cualquier compra</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceLawtonBrodyInicial-p-7" name="p-2" value="7" valor="0" /> 0
				            </label>
				      	</td>
				      
				    </tr>
                    <tr>
				      	<td>
					    <p>Completamente incapaz de hacer la compra</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceLawtonBrodyInicial-p-8" name="p-2" value="8" valor="0" /> 0
				            </label>
				      	</td>
				      	
				    </tr>
				      

				</tbody>
				    
			</table>

                        
            <table class="table table-bordered" id="tbl_3">
				<tbody>
					<tr>
					    <td colspan="2"><b>Preparación de la comida</b></td>
					</tr>
					<tr>
				      	<td style="width:80%;">
					    <p>Organiza, Prepara y sirve cualquier comida por si solo/a</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceLawtonBrodyInicial-p-9"  name="p-3" value="9" valor="1" /> 1
				            </label>
				      	</td>
				      
				    </tr>
                    <tr>
				      	<td>
					    <p>Prepara la comida sólo si le proporcionan los ingredientes</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceLawtonBrodyInicial-p-10" name="p-3" value="10" valor="0" /> 0
				            </label>
				      	</td>
				      	
				    </tr>
                    <tr>
				      	<td>
					    <p>Prepara, calienta y sirve la comida, pero no sigue una dieta adecuada</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" name="p-3" id="indiceLawtonBrodyInicial-p-11" value="11" valor="0" /> 0
				            </label>
				      	</td>
				      
				    </tr>
                    <tr>
				      	<td>
					    <p>Necesita que le preparen y le sirvan la comida</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceLawtonBrodyInicial-p-12" name="p-3" value="12" valor="0" /> 0
				            </label>
				      	</td>
				      	
				    </tr>
				      

				</tbody>
				    
			</table>
                        
                        
                        
            <!-- solo mujer -->
            <table class="table table-bordered" id="tbl_4">
				<tbody>
					<tr>
					    <td colspan="2"><b>Tareas Domésticas</b></td>
					</tr>
					<tr>
				      	<td style="width:80%;">
					    <p>Realiza las tareas de la casa por si sola, sólo ayuda ocacional</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceLawtonBrodyInicial-p-13" name="p-4" value="13" valor="1" /> 1
				            </label>
				      	</td>
				      
				    </tr>
                    <tr>
				      	<td>
					    <p>Realiza tareas ligeras (fregar platos, camas...)</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceLawtonBrodyInicial-p-14" name="p-4" value="14" valor="1" /> 1
				            </label>
				      	</td>
				      	
				    </tr>
                    <tr>
				      	<td>
					    <p>
					    Realiza tareas ligeras, pero no mantiene un nivel de limpieza adecuado    
					    </p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceLawtonBrodyInicial-p-15" name="p-4" value="15" valor="1" /> 1
				            </label>
				      	</td>
				      
				    </tr>
                    <tr>
				      	<td>
					    <p>Necesita ayuda, pero realiza todas las tareas domésticas</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceLawtonBrodyInicial-p-16" name="p-4" value="16"  valor="1" /> 1
				            </label>
				      	</td>
				      	
				    </tr>
				    <tr>
				      	<td>
					    <p>No participa ni hace ninguna tarea</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceLawtonBrodyInicial-p-17" name="p-4" value="17" valor="0" /> 0
				            </label>
				      	</td>
				      	
				    </tr>

				</tbody>
				    
			</table>
                        
            <table class="table table-bordered" id="tbl_5">
				<tbody>
					<tr>
					    <td colspan="2"><b>Lavar la ropa</b></td>
					</tr>
					<tr>
				      	<td style="width:80%;">
					    <p>
					        Lava sola toda la ropa
					    </p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceLawtonBrodyInicial-p-18" name="p-5" value="18" valor="1" /> 1
				            </label>
				      	</td>
				      
				    </tr>
                    <tr>
				      	<td>
					    <p>Lava sólo prendas pequeñas (calcetines, medias, etc.)</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceLawtonBrodyInicial-p-19" name="p-5" value="19" valor="1" /> 1
				            </label>
				      	</td>
				      	
				    </tr>
                    <tr>
				      	<td>
					    <p>La ropa la tiene que lavar otra persona</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceLawtonBrodyInicial-p-20" name="p-5" value="20" valor="0" /> 0
				            </label>
				      	</td>
				      	
				    </tr>
                                      
				     

				</tbody>
				    
			</table>
                        
            <!-- fin tareas mujer -->
                        
            <table class="table table-bordered" id="tbl_6">
				<tbody>
					<tr>
					    <td colspan="2"><b>Transporte</b></td>
					</tr>
					<tr>
				      	<td style="width:80%;">
					    <p>
					        Viaja por si solo/a, utiliza transporte publico/conduce coche
					    </p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceLawtonBrodyInicial-p-21" name="p-6" value="21" valor="1" /> 1
				            </label>
				      	</td>
				      
				    </tr>
                                      
                    <tr>
				      	<td>
					    <p>Puede ir solo en taxi, no utiliza otro transporte publico</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceLawtonBrodyInicial-p-22" name="p-6" value="22" valor="1" /> 1
				            </label>
				      	</td>
				      	
				    </tr>
                                      
                                      
                                      
                    <tr>
				      	<td>
					    <p>Solo viaja en transporte público si va acompañado</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceLawtonBrodyInicial-p-23" name="p-6" value="23" valor="1" /> 1
				            </label>
				      	</td>
				      	
				    </tr>
                                      
                    <tr>
				      	<td>
					    <p>Viajes limitados en taxi o coche con ayuda de otros (adaptado)</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceLawtonBrodyInicial-p-24" name="p-6" value="24" valor="0" /> 0
				            </label>
				      	</td>
				      	
				    </tr>
                                      
				    <tr>
				      	<td>
					    <p>No viaja en absoluto</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceLawtonBrodyInicial-p-25" name="p-6" value="25" valor="0" /> 0
				            </label>
				      	</td>
				      	
				    </tr>

				</tbody>
				    
			</table>
                        
            <table class="table table-bordered" id="tbl_7">
				<tbody>
					<tr>
					    <td colspan="2"><b>Responsabilidad respecto a la medicación</b></td>
					</tr>
					<tr>
				      	<td style="width:80%;">
					    <p>
					        Es capaz de tomar la medicación a la hora y en la dosis correcta, solo/a
					    </p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceLawtonBrodyInicial-p-26" name="p-7" value="26" valor="1" /> 1
				            </label>
				      	</td>
				      
				    </tr>
                    <tr>
				      	<td>
					    <p>Toma la medicación sólo si se la preparan previamente</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceLawtonBrodyInicial-p-27" name="p-7" value="27" valor="0" /> 0
				            </label>
				      	</td>
				      	
				    </tr>
                    <tr>
				      	<td>
					    <p>No es capaz de tomar la medicación solo/a</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceLawtonBrodyInicial-p-28" name="p-7" value="28" valor="0" /> 0
				            </label>
				      	</td>
				      	
				    </tr>
                                      
				     

				</tbody>
				    
			</table>
                        
            <table class="table table-bordered" id="tbl_8">
				<tbody>
					<tr>
					    <td colspan="2"><b>Capacidad de utilizar el dinero</b></td>
					</tr>
					<tr>
				      	<td style="width:80%;">
					    <p>
					        Se responsabiliza de asuntos económicos solo/a
					    </p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceLawtonBrodyInicial-p-29" name="p-8" value="29" valor="1"/> 1
				            </label>
				      	</td>
				      
				    </tr>
                    <tr>
				      	<td>
					    <p>Se encarga de compras diarias, pero necesita ayuda para ir al banco</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceLawtonBrodyInicial-p-30" name="p-8" value="30" valor="1" /> 1
				            </label>
				      	</td>
				      	
				    </tr>
                    <tr>
				      	<td>
					    <p>Incapaz de utilizar el dinero</p>
				      	</td>
				      	<td>
				      		<label class="btn btn-default btn-radio-group">
				                <input type="radio" id="indiceLawtonBrodyInicial-p-31" name="p-8" value="31" valor="0" /> 0
				            </label>
				      	</td>
				      	
				    </tr>                                     
				     

				</tbody>
				<tfoot>
					<tr>
				      	<td colspan="1">
				      		Total:
				      	</td>
				      	<td>
				      		<input type="number" min="0" name="indiceLawtonBrody-total" class="form-control indiceLawtonBrodyInicial-total"  readonly >
				      	</td>
				    </tr>
					<tr>
						<td colspan="3">
							<button type="button" class="btn btn-primary pull-center" onclick="sumarIndiceLawtonBrody('indiceLawtonBrodyInicial-p-','indiceLawtonBrodyInicial-total','mensaje-resultado-indiceLawtonBrodyInicial', 'formLawtonBrodyInicial');" id="ecalcularindiceLawtonBrodyInicial">Calcular</button> <span class="mensaje-resultado-indiceLawtonBrodyInicial"></span>
						</td>
					</tr>
				</tfoot>
				    
			</table>
                                
                        

		<fieldset>

		
		<p>Fuente bibliográfica de la que se ha obtenido esta versión: Lawton MP, Brody EM. Assessment of older people: self-maintaining and instrumental activities of daily living. Gerontologist 1969; 9:179-86.</p>
		<p>Comentarios:</p>
		<p>Actividades instrumentales propias del medio extrahospitalario y necesarias para vivir solo. Su normalidad suele ser indicativa de integridad de las actividades básicas para el autocuidado y del estado mental (es útil en programas de screening de ancianos de riesgo en la comunidad). Hay tres actividades que en la cultura occidental son más propias de mujeres (comida, tareas del hogar, lavar ropa); por ello, los autores de la escala admiten que en los hombres estas actividades puedan suprimirse de la evaluación, de esta manera existirá una puntuación total para hombres y otra para mujeres (se considera anormal < 5 en hombre y < 8 en mujer). El deterioro de las actividades instrumentales, medido con el índice de Lawton, es predictivo de deterioro de las actividades básicas, durante un ingreso hospitalario (Sager et al. J Am Geriatr Soc 1996; 44: 251-7); por ello, algunos autores han sugerido que este índice puede ser un indicador de fragilidad (Nourhashémi F, et al. J Gerontol Med Sci 2001; 56A: M448-M53).</p>
		<?php
			$tipoPaciente = App\Models\Usuario::obtenerNombreTipoUsuario(Auth::user()->rut);
			if ($tipoPaciente == "medico")
			{
		?>
		<button type="button" class="btn btn-primary pull-right" onclick="habilitarBoton('formLawtonBrodyInicial', 'editarLawtonBrodyInicial', 'guardarLawtonBrodyInicial')" id="editarLawtonBrodyInicial">Editar</button>
		
		<button type="button" class="btn btn-success pull-right" id="imprimirLawtonBrodyInicial" onclick="imprimirPDF('formLawtonBrodyInicial')" >PDF</button>

		<button type="submit" class="btn btn-primary pull-right" id="guardarLawtonBrodyInicial" style="display:none;">Guardar</button>
		<?php
			}
		?>
		{{ Form::close() }}
	</div>
</div>
<!--</fieldset>-->

<script type="text/javascript">



$(function(){
    
    sumarIndiceLawtonBrody('indiceLawtonBrodyInicial-p-','indiceLawtonBrodyInicial-total','mensaje-resultado-indiceLawtonBrodyInicial', 'formLawtonBrodyInicial');
    
    function obtenerGenero(genero){
    	console.log("El genero es :" + genero);        
    }

    $('#LawtonBrodyInicial-fecha-encuesta').prop("disabled", true);

    
	$("#formLawtonBrodyInicial").formValidation({
		excluded: ':disabled',
		framework: 'bootstrap',
		fields: {
			"fecha-encuesta": {
				validators:{
					/*notEmpty: {
						message: 'El nombre es obligatorio'
					},*/
                    date: {
                        format: 'DD-MM-YYYY',
                        //min: '01/01/2010',
                        //max: '12/30/2020',
                        message: 'La fecha no es válida'
                    }
				}
			}
                        
		}
	}).on('err.field.fv', function(e, data) {
		if (data.fv.getSubmitButton()) data.fv.disableSubmitButtons(false);
	}).on('success.field.fv', function(e, data) {
		if (data.fv.getSubmitButton()) data.fv.disableSubmitButtons(false);
	}).on("success.form.fv", function(evt){
		console.log("--- submit formLawtonBrodyInicial ---");

		$("#formLawtonBrodyInicial input[type='submit']").prop("disabled", false);
		$('#LawtonBrodyInicial-fecha-encuesta').prop("disabled", false);
		evt.preventDefault(evt);
		$("#dvLoading").show();
		var $form = $(evt.target);
		
		var form = $(this).serializeArray();
		form.push({name:"rut", value:"{{ $rut }}"});

		$.ajax({
			url: $form.prop("action"),
			data: form,
			type: "post",
			dataType: "json",
			success: function(data){
				if(data.exito) bootbox.alert("<h4>"+data.exito+"</h4>", function(){
					verDatosPaciente();
					deshabilitarBoton('formLawtonBrodyInicial', 'editarLawtonBrodyInicial', 'guardarLawtonBrodyInicial');
				});
				if(data.error) bootbox.alert("<h4>"+data.error+"</h4>");
				$("#dvLoading").hide();
			},
			error: function(error){
				console.log(error);
				$("#dvLoading").hide();
			}
		});
		return false;
	});


});
</script>






