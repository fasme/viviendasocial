<fieldset>
	<legend style="font-size: 16px;">Datos personales</legend>
	<div style="text-align: left;">
	{{ Form::open(array('url' => 'paciente/agregarPaciente', 'method' => 'post', 'class' => '', 'role' => 'form', 'id' => 'formEditarPaciente')) }}
		<div class="row">
			<div class="col-sm-4">
				<div class="form-group error">
					<label for="paciente-nombre" class="control-label">Nombres</label>
					<input id="paciente-nombre" name="nombre" class="form-control" type="text" />	
				</div>
			</div>

			<div class="col-sm-4">
				<div class="form-group error">	

					<label for="paciente-apellido" class="control-label">Primer paterno</label>
					<input id="paciente-apellido" name="apellido_p" class="form-control" type="text" />			
				</div>
			</div>
			
			<div class="col-sm-4">
				<div class="form-group error">
					<label for="paciente-apellido" class="control-label">Segundo materno</label>	
					<input id="paciente-apellido_m" name="apellido_m" class="form-control" type="text" />			
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-4">
				<div class="form-group error">	
					<label for="paciente-rut" class="control-label">Rut </label>
					<div class="input-group">
						{{Form::text('rut', null, array('id' => 'paciente-rut', 'class' => 'form-control', 'autofocus', 'readonly' => 'readonly'))}}
						<span class="input-group-addon"> - </span>
						{{ Form::select('dv', $selectRut, null, array('class' => 'form-control', 'id' => 'paciente-dv', 'readonly' => 'readonly', 'disabled')) }}
					</div>			
				</div>
			</div>	
			

			<div class="col-sm-4">
				<div class="form-group error">		
					<label for="paciente-fechanacimiento" class="control-label">Fecha de nacimiento</label>			
					{{Form::text("fechaNacimiento", null, array("id" => "paciente-fechanacimiento", "class" => "form-control"))}}
				</div>
			</div>

			<div class="col-sm-4">
				<div class="form-group error">			
					<label for="paciente-genero" class="control-label">Género</label>	
					{{ Form::select('genero', $selectGenero, null, array('class' => 'form-control')) }}
				</div>
			</div>

			
		</div>

		<div class="row">	

			<div class="col-sm-6">
				<div class="form-group error">	
					<label for="paciente-correo" class="control-label">APS</label>	
					{{ Form::select('genero', $selectInstitucion, null, array('class' => 'form-control')) }}
				</div>
			</div>

			<div class="col-sm-6">
				<div class="form-group error">	
					<label for="paciente-correo" class="control-label">Correo</label>	
					<input id="paciente-correo" name="correo" class="form-control" type="text" />
				</div>
			</div>
			<!-- <div class="col-sm-3">
				<div class="form-group error">
					<label for="paciente-tipoAlerta" class=" control-label">Tipo Evento: </label>
					{{ Form::select('paciente-tipoAlerta', $selectAlerta, null, array('class' => 'form-control', 'id' => 'paciente-tipoAlerta')) }}
				</div>
			</div> -->
		</div>

		<div class="row">
			<div class="col-sm-2">
				<div class="form-group error">
					<label for="paciente-telefono" class="control-label">Télefono</label>	
					<input name="telefono" class="form-control" type="text" />	
				</div>
			</div>

			<div class="col-sm-2">
				<div class="form-group error">
					<label for="paciente-telefono" class="control-label">Télefono</label>	
					<input name="telefono" class="form-control" type="text" />	
				</div>
			</div>
			
		</div>

		
	
	

	<legend style="font-size: 16px;">Dirección</legend>

	
		<div class="row">
			<div class="col-sm-6">
				<div class="form-group error">	
					<label for="paciente-comuna" class="control-label">Comuna</label>	
					{{ Form::select('comuna', $selectComunas, null, array('class' => 'form-control', 'id' => 'paciente-comuna')) }}	
				</div>
			</div>

			<div class="col-sm-6">
				<div class="form-group error">	
					<label for="paciente-calle" class="control-label">Calle</label>		
					<input id="paciente-calle" name="calle" class="form-control" type="text" />	
				</div>
			</div>

			
			
		</div>	

		<div class="row">
			

			<div class="col-sm-4">
				<div class="form-group error">	
					<label for="paciente-numero" class="control-label">Número</label>		
					<input id="paciente-numero" name="numero" class="form-control" type="text" />
				</div>
			</div>

			<div class="col-sm-6">
				<div class="form-group error">	
					<label for="paciente-observacion" class="control-label">Observación</label>		
					<input id="paciente-observacion" name="observacion" class="form-control" type="text" />
				</div>
			</div>

			<div class="col-sm-2">
				
			</div>
		</div>

		<div id="map"></div>

		<div class="row">
			<div class="col-sm-12">
				<div class="form-group error">	
					<label for="paciente-adicional" class="control-label">Información adicional</label>		
					<input id="paciente-adicional" name="adicional" class="form-control" type="text" />	
				</div>
			</div>
		</div>


		<div class="row">
			<div class="col-sm-4">
				<div class="form-group error">	
					<label for="paciente-monitoreoInicio" class="control-label">Rango monitoreo</label>		
					{{Form::text("monitoreoInicio", null, array("id" => "paciente-monitoreoInicio", "class" => "form-control"))}}

				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group error">		
					<label for="paciente-monitoreoFin" class="control-label"></label>	
					{{Form::text("monitoreoFin", null, array("id" => "paciente-monitoreoFin", "class" => "form-control"))}}
					
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-4">
				<div class="form-group error">	
					<label for="paciente-nocheInicio" class="control-label">Rango noche</label>		
					{{Form::text("nocheInicio", null, array("id" => "paciente-nocheInicio", "class" => "form-control"))}}
				</div>
			</div>

			<div class="col-sm-4">
				<div class="form-group error">		
					<label for="paciente-nocheFin" class="control-label"></label>	
					{{Form::text("nocheFin", null, array("id" => "paciente-nocheFin", "class" => "form-control"))}}
					
				</div>
			</div>
		</div>
		<!--<div class="row">	
			<div class="col-sm-4">
				<div class="form-group error">	
					<label for="paciente-estado-civil" class="control-label">Estado civil</label>
					<input id="paciente-estado-civil" name="estado-civil" class="form-control" type="text" />
				</div>
			</div>

			<div class="col-sm-2">
				<div class="form-group error">	
					<label for="paciente-grupo-sangre" class="control-label">Grupo sanguíneo</label>		
					<input id="paciente-grupo-sangre" name="grupo-sangre" class="form-control" type="text" />
				</div>
			</div>
		</div>-->
		@if(App\Models\Usuario::obtenerTipoUsuario(Auth::user()->rut) == 3 )
		<button id="btn-editar-dp" type="button" class="btn btn-primary pull-right">Editar</button>
		@endif
		<button id="btn-guardar-dp" type="submit" class="btn btn-primary pull-right" style="display:none;">Guardar</button>	
	</div>
	{{ Form::close() }}
</fieldset>





