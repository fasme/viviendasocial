<script>

var tableAlertaCaidas = null;

$(function(){

	tableAlertaCaidas = $('#table-alerta-caida').DataTable({	
		"iDisplayLength": 10,
		"bJQueryUI": true,
		"info": true,
		"bAutoWidth" : false,
		"responsive": true,
		"searching": false,
        "ordering": false,
        "info": false,
        "order": [[0, "desc"]],
		"oLanguage": {
			"sUrl": "{{URL::to('/')}}/js/spanish.json"
		},
		"ajax": {
			"url": "obtenerAlertasCaidas",
			"data": {rut: "{{$rut}}"},
			"type": "post"
		},
		initComplete: function (settings, json) {
            $("#table-alerta-caida_length").remove();
        },
	});	

	generarGraficoCaidasDias(12, "grafico-12-caida", eventos.CAIDA);
	//generarGrafico(12, "grafico-12-caida", eventos.CAIDA);
	generarGraficoCaidas();
	console.log(" - cargando caidas - ");

});

</script>

<div class="row">
	<div class="col-md-6">
		<div id="grafico-12-caida" ></div>
	</div>
	<div class="col-md-6">
		<div id="grafico-12-meses" ></div>
	</div>
</div>
<br>
<table id="table-alerta-caida" class="table table-bordered table-striped dataTable dtr-inline">
	<thead>
		<tr>
			<th>Fecha de alerta</th>
		</tr>
	</thead>
</table>