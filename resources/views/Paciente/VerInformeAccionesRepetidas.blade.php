@extends("Template/Template")

@section("titulo")
Ver resultados informes de acciones repetidas
@stop

@section("script")


@stop

@section("section")
<script>
$(function(){
		$("#informe_ar").click(function(){
				$("#informe_ar").prop("disabled",true);
				$("#info").text("\nSe están obteniendo los datos del informe, esta operación puede tardar unos minutos.");
				$.ajax({
					url: "procesarDatosAccionesRepetidas",
					type: "post",
					dataType: "json",
					success: function (data) {
						
						console.log(data);
						$("#info").text("\nSe está generando el informe.");
						generarInforme(data);
						$("#info").text("");
						$("#informe_ar").prop("disabled",false);
						
					},
					error: function (error) {
						console.log(error);
						$("#informe_ar").prop("disabled",false);
					}
				});
				
		});
		$("#informe_sin_ar").click(function(){
				$("#informe_sin_ar").prop("disabled",true);
				$("#info").text("\nSe están obteniendo los datos del informe, esta operación puede tardar unos minutos.");
				$.ajax({
					url: "procesarDatosAccionesRepetidas",
					type: "post",
					dataType: "json",
					success: function (data) {
						
						console.log(data);
						$("#info").text("\nSe está generando el informe.");
						generarInforme(data,false);
						$("#info").text("");
						$("#informe_sin_ar").prop("disabled",false);
						
					},
					error: function (error) {
						console.log(error);
						$("#informe_sin_ar").prop("disabled",false);
					}
				});
				
		});
});
</script>
<div>
	<fieldset>
		<legend>Informe de acciones repetidas</legend>
<?php
	$tipoPaciente = Usuario::obtenerNombreTipoUsuario(Auth::user()->rut);
	if ($tipoPaciente == "evaluador_2")
	{
	?>
	
		<h4>Datos diarios</h4>
		{{ Form::open(array('url' => 'paciente/procesarInformeAccionesRepetidas', 'method' => 'get', 'class' => 'form-horizontal', 'role' => 'form', 'id' => 'formReporte')) }}
		<button id="informe_tabla_acciones_repetidas" class="btn btn-primary" type="submit">Descargar informe acciones repetidas</button>
		
		
		{{ Form::close() }}
		
	
	<?php
	}
	if ($tipoPaciente == "evaluador_1")
	{
	?>
		<h4>Informe encuestas</h4>
		<button id="informe_sin_ar" class="btn btn-primary" type="button">Descargar</button>
		<br><br>
		<h4>Informe encuestas + sensores</h4>
		<button id="informe_ar" class="btn btn-primary" type="button">Descargar</button>
		<br><br>
		<p id="info" style="font-size:16px;"></p>
		<canvas id="graf" style="width:500px;height:250px;display:none;" ></canvas>
		<div id="graf_cant1" style="display:none;"></div>
		<div id="graf_cant2" style="display:none;"></div>
		<div id="graf_cant3" style="display:none;"></div>
		<div id="graf_s1_7" style="display:none;"></div>
		<div id="graf_s1_14" style="display:none;"></div>
		<div id="graf_s1_21" style="display:none;"></div>
		<div id="graf_s1_28" style="display:none;"></div>
		<div id="graf_s1_30" style="display:none;"></div>
		<div id="graf_s2_7" style="display:none;"></div>
		<div id="graf_s2_14" style="display:none;"></div>
		<div id="graf_s2_21" style="display:none;"></div>
		<div id="graf_s2_28" style="display:none;"></div>
		<div id="graf_s2_30" style="display:none;"></div>
		<div id="graf_s3_7" style="display:none;"></div>
		<div id="graf_s3_14" style="display:none;"></div>
		<div id="graf_s3_21" style="display:none;"></div>
		<div id="graf_s3_28" style="display:none;"></div>
		<div id="graf_s3_30" style="display:none;"></div>
		<svg id="img_graf"  style="display:none;"></svg>
	<?php
	}
	?>
	</fieldset>
</div>
@stop
