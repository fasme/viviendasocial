<!--<fieldset>
	<legend class="negrita" style="font-size: 16px;">Inicial</legend>-->
<script>
    var addFila3=function()
    {
        var $template = $('#templateRow3'),
        $clone    = $template
        .clone()
        .removeClass('hide')
        .removeAttr('id')
        .insertBefore($template);
        $clone.find("input").prop("disabled", false);
        $clone.find('[name="horario[]"]').mask("99:99",{placeholder:"00:00"});
/*		$clone.find('[name="horario[]"]').datetimepicker({
			locale: "es",
			format: "hh:mm:ss",
			maxDate: moment().add('days', 1).format("YYYY/MM/DD"),
			useCurrent: true
		});*/

        console.log($clone.find('[name="horario[]"]'));
    }
    var agregarHorario=function(componente,deshabilitado,id,hora,volumen,perdida)
    {
    	console.log(perdida);
    	var hora=hora||"";
    	var volumen=volumen||"";
    	var id=id||0;
    	
    	var $template = $(componente).parent().parent().children(".horarios.hide");
        $clone    = $template
        .clone()
        .removeClass('hide')
        .removeAttr('id')
        .insertBefore($template);
        $clone.find("input").prop("disabled", false);
        $clone.find('[name="horario[]"]').mask("99:99",{placeholder:"00:00"});
        $clone.find('[name="iddetalle[]"]').val(id);
        $clone.find('[name="horario[]"]').val(hora?moment(hora,"HH:mm:ss").format("HH:mm"):"");
        $clone.find('[name="volumen[]"]').val(volumen);
        if(perdida===""){$clone.find('[name="perdidaOrina[]"]').val("sn");}
        else {$clone.find('[name="perdidaOrina[]"]').val(perdida=="true"?"si":"no");}
        $contador=$(componente).parent().parent().find("[name='contador[]']");
        $contador.val(parseInt($contador.val())+1);
        $clone.find("#agregar").prop("disabled",deshabilitado);
        $clone.find("[name='horario[]']").prop("disabled",deshabilitado);
		$clone.find("[name='volumen[]']").prop("disabled",deshabilitado);
    }
    var agregarFecha=function(deshabilitado,id,fecha,hora)
    {
    	var fecha=fecha||"";
    	var hora=hora||"";
    	var id=id||0;
    	var deshabilitado=deshabilitado||false;
    	var $template = $('.cartillas').last(),
    	$clone    = $template
        .clone()
        .removeClass('hide')
        .removeAttr('id')
        .insertBefore($template);
        $clone.find(".fecha3").mask("99:99",{placeholder:"00:00"});
        $clone.find(".fecha").datetimepicker({
			locale: "es",
			format: "DD-MM-YYYY",
			maxDate: moment().add('days', 1).format("YYYY/MM/DD"),
			useCurrent: false
		});
		$clone.find("#fecha_micciones").val(fecha?moment(fecha).format("DD-MM-YYYY"):"");
		$clone.find("#hora_acostarse").val(hora?moment(hora,"HH:mm:ss").format("HH:mm"):"");
		$clone.find('[name="idcartilla[]"]').val(id);
        $clone.find(".horarios").not(".hide").remove();

        
        return $clone;
    }
    
</script>



<div style="text-align: left;">
		
	<div class="form">
		{{ Form::open(array('url' => 'paciente/IngresarMicciones', 'method' => 'post', 'role' => 'form', 'id' => 'formMicciones')) }}

		<legend align="center">Cartilla de micciones nocturnas histórica.</legend> 

<h4>Cartilla de micción</h4><br>
	<div class="cartillas hide" style="border-style:solid;border-width:1px;padding:20px;border-color:lightgrey;margin-bottom:10px;">
		<input type="hidden" name="contador[]" value="1">
		<input type="hidden" name="idcartilla[]" value="0">
		<div class="row" >
			<div class="col-sm-6">
				<div class="form-group ">
					<label  class="control-label" >Fecha</label>
					<input id="fecha_micciones" name="fecha_micciones[]" class="form-control fecha" type="text" />	
				</div>
			</div>
			<div class="col-sm-6">
				<div class="form-group ">
					<label  class="control-label" >Hora de acostarse</label>
					<input id="hora_acostarse" name="hora_acostarse[]" class="form-control fecha3" type="text"/>	
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4">
				<label class="control-label">Horario</label>
			</div>
			<div class="col-sm-4">
				<label class="control-label">Volumen aproximado</label>
			</div>
			<div class="col-sm-4">
				<label class="control-label">Pérdida de orina</label>
			</div>
		</div>
		<div id="horarios" class="horarios hide">
			<input type="hidden" name="iddetalle[]" value="0">
			<div class="col-sm-4 fila">
				<input name="horario[]" id="horario" type="text" class="form-control fecha3" placeholder="hh:mm" disabled="disabled"/>
			</div>
			<div class="col-sm-4 fila">
				<input name="volumen[]" type="text" id="volumen" class="form-control" disabled="disabled">
			</div>
			<div class="col-sm-4 fila">
				{{ Form::select('perdidaOrina[]', array('si' => 'Si', 'no' => 'No', 'sn' => 'Sin información'), null, array('id' => 'perdidaOrina', 'class' => 'form-control')) }}
			</div>
		</div>
		<div class="row">
			<button type="button" class="btn btn-default btn_agregar" id="agregar" onclick="agregarHorario(this);" disabled="disabled" style="margin-left:30px;"><span class="glyphicon glyphicon-plus"></span> Agregar Horario</button>
		</div>
	</div>
	<div >
		<button type="button" class="btn btn-default btn_agregar" id="masFecha" onclick="agregarFecha();" disabled="disabled"><span class="glyphicon glyphicon-plus"></span> Agregar Fecha</button>
	</div>
<br>
		@if(Usuario::obtenerNombreTipoUsuario(Auth::user()->rut)!="evaluador_1"&&Usuario::obtenerNombreTipoUsuario(Auth::user()->rut)!="evaluador_2")
		<button type="button" class="btn btn-primary pull-right" onclick="habilitarBoton('formMicciones', 'editarMicciones', 'guardarMicciones','agregar')" id="editarMicciones">Editar</button>
		@endif
		<button type="submit" class="btn btn-primary pull-right" id="guardarMicciones" style="display:none;">Guardar</button>
		
		{{ Form::close() }}
	</div>
</div>

<div id="modalAceptaringreso" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">Confirma agregar nueva fecha (recuerde guardar la anterior)</h4>
			</div>
				<div class="modal-footer">
					<button id="acepto" class="btn btn-primary">Aceptar</button>
					<button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
				</div>
			
		</div>
	</div>
</div>


<!--</fieldset>-->

<script type="text/javascript">

$(function(){
$(".fecha3").mask("99:99",{placeholder:"00:00"});

var $div=null;
var idmicciones_inicial=0;
var idmicciones=0;
@foreach($detalleCartilla as $cartilla)
idmicciones="{{$cartilla->id_cartilla_micciones}}";
if(idmicciones!=idmicciones_inicial)
{
	$div=agregarFecha(true,"{{$cartilla->id_cartilla_micciones}}","{{$cartilla->fecha_cartilla}}","{{$cartilla->hora_acostarse}}");
	idmicciones_inicial=idmicciones;
}
agregarHorario($div.find("#agregar"),true,"{{$cartilla->id_detalle_cartilla_micciones}}","{{$cartilla->hora_detalle}}","{{$cartilla->volumen}}","{{$cartilla->perdida_orina}}");
@endforeach

agregarFecha(true);
$("input[name='hora_acostarse2[]']").mask("99:99",{placeholder:"00:00"});

	$("#editarMicciones").click(function(){
			$('#guardarMicciones').prop("disabled", false);
			$('#agregar').removeClass('disabled');
			$('#masFecha').removeClass('disabled');
			$('#guardarMicciones').removeClass('disabled');
		});

/*	$("#masFecha").click(function(){
		$("#modalAceptaringreso").modal("show");
		});*/

	$("#acepto").click(function(){
		$("#modalAceptaringreso").modal("hide");
		$('#fecha_micciones').val('');
		$('#hora_acostarse').val('');
		$('input[name="horario[]"]').val('');
	});

	$("#formMicciones").formValidation({
		excluded: ':disabled',
		framework: 'bootstrap',
		fields: {
			"fecha_micciones[]": {
				validators:{
					/*notEmpty: {
						message: 'El nombre es obligatorio'
					},*/
                    date: {
                        format: 'DD-MM-YYYY',
                        //min: '01/01/2010',
                        //max: '12/30/2020',
                        message: 'La fecha no es válida'
                    }
				}
			},
			"hora_acostarse[]":{
				validators:{
					regexp:{
						regexp: /[0-9]{2}:[0-9]{2}/
					}
				}
			}
		}
	}).on('err.field.fv', function(e, data) {
		if (data.fv.getSubmitButton()) data.fv.disableSubmitButtons(false);
	}).on('success.field.fv', function(e, data) {
		if (data.fv.getSubmitButton()) data.fv.disableSubmitButtons(false);
	}).on("success.form.fv", function(evt){
		console.log("--- submit editarEncuestasSindromesGeriatricos ---");

		$("#formMicciones input[type='submit']").prop("disabled", false);
		
	

		evt.preventDefault(evt);
		$("#dvLoading").show();
		var $form = $(evt.target);
		
		var form = $(this).serializeArray();
		form.push({name:"rut", value:"{{ $rut }}"});
		$.ajax({
			url: $form.prop("action"),
			data: form,
			type: "post",
			dataType: "json",
			success: function(data){
				if(data.exito) bootbox.alert("<h4>"+data.exito+"</h4>", function(){
					verDatosPaciente();
					deshabilitarBoton('formMicciones', 'editarMicciones', 'guardarMicciones');
				});
				if(data.error) bootbox.alert("<h4>"+data.error+"</h4>");
				$("#dvLoading").hide();
			},
			error: function(error){
				console.log(error);
				$("#dvLoading").hide();
			}
		});
		return false;
	});


});
</script>






