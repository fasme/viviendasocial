@extends("Template/Template")

@section("titulo")
Anexos
@stop

@section("script")

<script>

$(function(){
	// subir archivos
    var inArchivos = $("#inArchivos");
    inArchivos.fileinput({
        language: "es",
        showUpload: true,
        uploadAsync: true,
        uploadUrl: "../paciente/subir",
        uploadExtraData: {
            seccionArchivo: "archivo"
        }
    });

});

</script>

@stop

@section("section")

<fieldset>
	<legend>Anexos</legend>
	<div style="text-align: left;">
		<!-- subir archivos -->
        <div role="tabpanel" class="tab-pane" id="subir">
            <div class="row">
                <div class="col-md-12 panel panel-body">
                    <input id="inArchivos" multiple type="file">
                </div>
            </div>
        </div>
		
	</div>
</fieldset>

@stop
