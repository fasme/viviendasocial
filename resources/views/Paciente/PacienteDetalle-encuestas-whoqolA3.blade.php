<!--<fieldset>
	<legend class="negrita" style="font-size: 16px;">Timed Get Up and Go Test Inicial</legend>-->
<div style="text-align: left;">
		
	<div class="form">
		{{ Form::open(array('url' => 'paciente/editarEncuestasWhoqol', 'method' => 'post', 'role' => 'form', 'id' => 'formWhoqolA3')) }}
		<div>
			<input name="inicio" value="true" hidden/>
			<input name="tipo-encuesta" value="whoqol" hidden/>
		</div>

		<h4>Escala de calidad de vida WHOQOL-BREF</h4>

		<div class="row">
			<div class="col-sm-6">
				<div class="form-group ">
					<label for="Whoqol-fecha-encuesta" class="control-label" style="width:100%;">Fecha</label>
					<input id="WhoqolA3-fecha-encuesta" name="fecha-encuesta" class="form-control fecha" type="text"/>	
				</div>
			</div>                    
		</div>
		
		<fieldset>
			<legend class="negrita" style="font-size: 14px;">Instrucciones:</legend>
			<p><strong>Instrucciones: </strong>Este cuestionario sirve para conocer su opinión acerca de su calidad de vida, su salud y otras áreas de su vida. Por favor <strong>conteste todas las preguntas</strong>. Si no está seguro/a de qué respuesta dar a una pregunta, escoja la que le parezca más apropiada. A veces, ésta puede ser la primera respuesta que le viene a la cabeza.</p>

			 <p>Tenga presente su modo de vivir, expectativas, placeres y preocupaciones. Le pedimos que piense en su vida <strong>durante las dos últimas semanas</strong> .Por ejemplo, pensando en las dos últimas semanas, se puede preguntar:</p>

			 <p>¿Obtiene de otras personas el apoyo que necesita?</p>


			 <table data-role="table" class="ui-responsive table-stroke">
                <tbody>
                    <tr class="text-center">
                       	<td class="col-md-2">
                       		<p>Nada</p>
                       	</td>
                        <td class="col-md-2">
                            <p>Un poco</p>
                        </td>
                        <td class="col-md-2">
        		            <p>Moderado</p>
                        </td>
                        <td class="col-md-2">
                            <p>Bastante</p>
                        </td>
                        <td class="col-md-2">
                            <p>Totalmente</p>
                        </td>
                    </tr>
                    <tr class="text-center">
                        <td >
                            <label for="Whoqol-l-6" class="btn btn-default btn-radio-group">
                         	    <input type="radio" name="whoqolA3-1" value="1">
                      	    </label>
                        </td >
                        <td >
                            <label for="Whoqol-l-6" class="btn btn-default btn-radio-group">
                                <input type="radio" name="whoqolA3-1" value="2">
                            </label>
                        </td>
                        <td >
                             <label for="Whoqol-l-6" class="btn btn-default btn-radio-group">
                                <input type="radio" name="whoqolA3-1" value="3">
                            </label>
                        </td>
                        <td >
                            <label for="Whoqol-l-6" class="btn btn-default btn-radio-group">
                     	        <input type="radio" name="whoqolA3-1" value="4">
                            </label>
                        </td>
                        <td >
                            <label for="Whoqol-l-6" class="btn btn-default btn-radio-group">
                        	    <input type="radio" name="whoqolA3-1" value="5">
                           </label>
                        </td>

                    </tr>
                </tbody>
            </table>
	                
			<hr>
            <p>Por favor, lea la pregunta, valore sus sentimientos y haga un cáculo en el número de la escala que represente mejor su opción de respuesta.</p>

                    <hr>
                    <p>¿Cómo calificaría su calidad de vida?</p>
                    <table data-role="table" class="ui-responsive table-stroke">
                        <tbody>
                            <tr class="text-center">
                                <td class="col-md-2">
                                    <p>Muy mala</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Regular</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Normal</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Bastanta buena</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Muy buena</p>
                                </td>
                            </tr>

                            <tr class="text-center">
                                <td>
                                    <label for="whoqol-2" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-2" value="1">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-2" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-2" value="2">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-2" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-2" value="3">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-2" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-2" value="4">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-2" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-2" value="5">
                                    </label>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <hr>
                    <p>¿Cómo de satisfecho/a está con su salud?</p>
                    <table data-role="table" class="ui-responsive table-stroke">
                        <tbody>
                            <tr class="text-center">
                                <td class="col-md-2">
                                    <p>Muy mala</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Regular</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Normal</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Bastanta buena</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Muy buena</p>
                                </td>
                            </tr>

                            <tr class="text-center">
                                <td>
                                    <label for="whoqol-3" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-3" value="1">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-3" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-3" value="2">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-3" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-3" value="3">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-3" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-3" value="4">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-3" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-3" value="5">
                                    </label>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <hr>
                    <h4>Las siguientes preguntas hacen referencia al grado en que ha experimentado ciertos hechos en las dos últimas semanas.</h4>
                    
                    <p>¿Hasta qué punto piensa que el dolor (físico) le impide hacer lo que necesita?</p>
                    <table data-role="table" class="ui-responsive table-stroke">
                        <tbody>
                            <tr class="text-center">
                                <td class="col-md-2">
                                    <p>Nada</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Un poco</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Lo normal</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Bastante</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Extremadamente</p>
                                </td>
                            </tr>

                            <tr class="text-center">
                                <td>
                                    <label for="whoqol-4" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-4" value="1">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-4" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-4" value="2">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-4" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-4" value="3">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-4" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-4" value="4">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-4" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-4" value="5">
                                    </label>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <hr>
                    <p>¿En qué grado necesita de un tratamiento médico para funcionar en su vida diaria?</p>
                    <table data-role="table" class="ui-responsive table-stroke">
                        <tbody>
                            <tr class="text-center">
                                <td class="col-md-2">
                                    <p>Nada</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Un poco</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Lo normal</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Bastante</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Extremadamente</p>
                                </td>
                            </tr>

                            <tr class="text-center">
                                <td>
                                    <label for="whoqol-5" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-5" value="1">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-5" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-5" value="2">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-5" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-5" value="3">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-5" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-5" value="4">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-5" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-5" value="5">
                                    </label>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <hr>
                    <p>¿Cuánto disfruta de la vida?</p>
                    <table data-role="table" class="ui-responsive table-stroke">
                        <tbody>
                            <tr class="text-center">
                                <td class="col-md-2">
                                    <p>Nada</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Un poco</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Lo normal</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Bastante</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Extremadamente</p>
                                </td>
                            </tr>

                            <tr class="text-center">
                                <td>
                                    <label for="whoqol-6" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-6" value="1">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-6" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-6" value="2">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-6" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-6" value="3">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-6" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-6" value="4">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-6" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-6" value="5">
                                    </label>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <hr>
                    <p>¿Hasta qué punto siente que su vida tiene sentido?</p>
                    <table data-role="table" class="ui-responsive table-stroke">
                        <tbody>
                            <tr class="text-center">
                                <td class="col-md-2">
                                    <p>Nada</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Un poco</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Lo normal</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Bastante</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Extremadamente</p>
                                </td>
                            </tr>

                            <tr class="text-center">
                                <td>
                                    <label for="whoqol-7" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-7" value="1">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-7" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-7" value="2">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-7" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-7" value="3">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-7" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-7" value="4">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-7" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-7" value="5">
                                    </label>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <hr>
                    <p>¿Cuál es su capacidad de concentración?</p>
                    <table data-role="table" class="ui-responsive table-stroke">
                        <tbody>
                            <tr class="text-center">
                                <td class="col-md-2">
                                    <p>Nada</p>
                                </td>
                                <td class="col-md-2"> 
                                    <p>Un poco</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Lo normal</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Bastante</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Extremadamente</p>
                                </td>
                            </tr>

                            <tr class="text-center">
                                <td>
                                    <label for="whoqol-8" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-8" value="1">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-8" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-8" value="2">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-8" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-8" value="3">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-8" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-8" value="4">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-8" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-8" value="5">
                                    </label>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <hr>
                    <p>¿Cuánta seguridad tiene en su vida diaria?</p>
                    <table data-role="table" class="ui-responsive table-stroke">
                        <tbody>
                            <tr class="text-center">
                                <td class="col-md-2">
                                    <p>Nada</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Un poco</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Lo normal</p>
                                </td>
                                <td class="col-md-2"
                                    <p>Bastante</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Extremadamente</p>
                                </td>
                            </tr>

                            <tr class="text-center">
                                <td>
                                    <label for="whoqol-9" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-9" value="1">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-9" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-9" value="2">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-9" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-9" value="3">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-9" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-9" value="4">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-9" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-9" value="5">
                                    </label>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <hr>
                    <p>¿Cómo de saludable es el ambiente físico a su alrededor?</p>
                    <table data-role="table" class="ui-responsive table-stroke">
                        <tbody>
                            <tr class="text-center">
                                <td class="col-md-2">
                                    <p>Nada</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Un poco</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Lo normal</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Bastante</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Extremadamente</p>
                                </td>
                            </tr>

                            <tr class="text-center">
                                <td>
                                    <label for="whoqol-10" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-10" value="1">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-10" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-10" value="2">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-10" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-10" value="3">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-10" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-10" value="4">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-10" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-10" value="5">
                                    </label>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <hr>
                    <p>¿Tiene energÃía suficiente para la vida diaria?</p>
                    <table data-role="table" class="ui-responsive table-stroke">
                        <tbody>
                            <tr class="text-center">
                                <td class="col-md-2">
                                    <p>Nada</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Un poco</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Lo normal</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Bastante</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Extremadamente</p>
                                </td>
                            </tr>

                            <tr class="text-center">
                                <td>
                                    <label for="whoqol-11" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-11" value="1">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-11" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-11" value="2">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-11" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-11" value="3">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-11" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-11" value="4">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-11" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-11" value="5">
                                    </label>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <hr>
                    <p>¿Es capaz de aceptar su apariencia física?</p>
                    <table data-role="table" class="ui-responsive table-stroke">
                        <tbody>
                            <tr class="text-center">
                                <td class="col-md-2">
                                    <p>Nada</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Un poco</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Lo normal</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Bastante</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Extremadamente</p>
                                </td>
                            </tr>

                            <tr class="text-center">
                                <td>
                                    <label for="whoqol-12" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-12" value="1">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-12" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-12" value="2">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-12" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-12" value="3">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-12" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-12" value="4">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-12" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-12" value="5">
                                    </label>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <hr>
                    <p>¿Tiene suficiente dinero para cubrir sus nececidades?</p>
                    <table data-role="table" class="ui-responsive table-stroke">
                        <tbody>
                            <tr class="text-center">
                                <td class="col-md-2">
                                    <p>Nada</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Un poco</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Lo normal</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Bastante</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Extremadamente</p>
                                </td>
                            </tr>

                            <tr class="text-center">
                                <td>
                                    <label for="whoqol-13" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-13" value="1">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-13" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-13" value="2">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-13" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-13" value="3">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-13" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-13" value="4">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-13" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-13" value="5">
                                    </label>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <hr>
                    <p>¿Dispone de la información que necesita para su vida diaria?</p>
                    <table data-role="table" class="ui-responsive table-stroke">
                        <tbody>
                            <tr class="text-center">
                                <td class="col-md-2">
                                    <p>Nada</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Un poco</p>
                                </td >
                                <td class="col-md-2">
                                    <p>Lo normal</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Bastante</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Extremadamente</p>
                                </td>
                            </tr>

                            <tr class="text-center">
                                <td>
                                    <label for="whoqol-14" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-14" value="1">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-14" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-14" value="2">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-14" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-14" value="3">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-14" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-14" value="4">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-14" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-14" value="5">
                                    </label>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <hr>
                    <p>¿Hasta qué punto tiene la oportunidad de realizar actividades de ocio?</p>
                    <table data-role="table" class="ui-responsive table-stroke">
                        <tbody>
                            <tr class="text-center">
                                <td class="col-md-2">
                                    <p>Nada</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Un poco</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Lo normal</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Bastante</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Extremadamente</p>
                                </td>
                            </tr>

                            <tr class="text-center">
                                <td>
                                    <label for="whoqol-15" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-15" value="1">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-15" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-15" value="2">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-15" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-15" value="3">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-15" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-15" value="4">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-15" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-15" value="5">
                                    </label>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <hr>
                    <p>¿Es capaz de desplazarse de un lugar a otro?</p>
                    <table data-role="table" class="ui-responsive table-stroke">
                        <tbody>
                            <tr class="text-center">
                                <td class="col-md-2">
                                    <p>Nada</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Un poco</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Lo normal</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Bastante</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Extremadamente</p>
                                </td>
                            </tr>

                            <tr class="text-center">
                                <td>
                                    <label for="whoqol-16" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-16" value="1">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-16" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-16" value="2">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-16" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-16" value="3">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-16" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-16" value="4">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-16" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-16" value="5">
                                    </label>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <hr>
                    <p>¿Cómo de satisfecho/a está con su sueño?</p>
                    <table data-role="table" class="ui-responsive table-stroke">
                        <tbody>
                            <tr class="text-center">
                                <td class="col-md-2">
                                    <p>Nada</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Un poco</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Lo normal</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Bastante</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Extremadamente</p>
                                </td>
                            </tr>

                            <tr class="text-center">
                                <td>
                                    <label for="whoqol-17" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-17" value="1">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-17" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-17" value="2">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-17" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-17" value="3">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-17" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-17" value="4">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-17" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-17" value="5">
                                    </label>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <hr>
                    <p>¿Cómo de satisfecho/a está con su habilidad para realizar sus actividades de la vida diaria?</p>
                    <table data-role="table" class="ui-responsive table-stroke">
                        <tbody>
                            <tr class="text-center">
                                <td class="col-md-2">
                                    <p>Nada</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Un poco</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Lo normal</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Bastante</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Extremadamente</p>
                                </td>
                            </tr>

                            <tr class="text-center">
                                <td>
                                    <label for="whoqol-18" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-18" value="1">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-18" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-18" value="2">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-18" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-18" value="3">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-18" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-18" value="4">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-18" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-18" value="5">
                                    </label>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <hr>
                    <p>¿Cómo de satisfecho/a está con su capacidad de trabajo?</p>
                    <table data-role="table" class="ui-responsive table-stroke">
                        <tbody>
                            <tr class="text-center">
                                <td class="col-md-2">
                                    <p>Nada</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Un poco</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Lo normal</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Bastante</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Extremadamente</p>
                                </td>
                            </tr>

                            <tr class="text-center">
                                <td>
                                    <label for="whoqol-19" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-19" value="1">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-19" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-19" value="2">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-19" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-19" value="3">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-19" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-19" value="4">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-19" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-19" value="5">
                                    </label>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <hr>
                    <p>¿Cómo de satisfecho/a está de sí mismo?</p>
                    <table data-role="table" class="ui-responsive table-stroke">
                        <tbody>
                            <tr class="text-center">
                                <td class="col-md-2">
                                    <p>Nada</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Un poco</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Lo normal</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Bastante</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Extremadamente</p>
                                </td>
                            </tr>

                            <tr class="text-center">
                                <td>
                                    <label for="whoqol-20" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-20" value="1">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-20" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-20" value="2">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-20" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-20" value="3">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-20" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-20" value="4">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-20" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-20" value="5">
                                    </label>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <hr>
                    <p>¿Cómo de satisfecho/a está con sus relaciones personales?</p>
                    <table data-role="table" class="ui-responsive table-stroke">
                        <tbody>
                            <tr class="text-center">
                                <td class="col-md-2">
                                    <p>Nada</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Un poco</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Lo normal</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Bastante</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Extremadamente</p>
                                </td>
                            </tr>

                            <tr class="text-center">
                                <td>
                                    <label for="whoqol-21" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-21" value="1">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-21" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-21" value="2">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-21" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-21" value="3">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-21" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-21" value="4">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-21" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-21" value="5">
                                    </label>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <hr>
                    <p>¿Cómo de satisfecho/a está con su vida sexual?</p>
                    <table data-role="table" class="ui-responsive table-stroke">
                        <tbody>
                            <tr class="text-center">
                                <td class="col-md-2">
                                    <p>Nada</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Un poco</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Lo normal</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Bastante</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Extremadamente</p>
                                </td>
                            </tr>

                            <tr class="text-center">
                                <td>
                                    <label for="whoqol-22" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-22" value="1">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-22" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-22" value="2">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-22" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-22" value="3">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-22" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-22" value="4">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-22" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-22" value="5">
                                    </label>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <hr>
                    <p>¿Cómo de satisfecho/a está con el apoyo que obtiene de sus amigos/as?</p>
                    <table data-role="table" class="ui-responsive table-stroke">
                        <tbody>
                            <tr class="text-center">
                                <td class="col-md-2">
                                    <p>Nada</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Un poco</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Lo normal</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Bastante</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Extremadamente</p>
                                </td>
                            </tr>

                            <tr class="text-center">
                                <td>
                                    <label for="whoqol-23" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-23" value="1">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-23" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-23" value="2">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-23" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-23" value="3">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-23" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-23" value="4">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-23" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-23" value="5">
                                    </label>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <hr>
                    <p>¿Cómo de satisfecho/a está de las condiciones del luga donde vive?</p>
                    <table data-role="table" class="ui-responsive table-stroke">
                        <tbody>
                            <tr class="text-center">
                                <td class="col-md-2">
                                    <p>Nada</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Un poco</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Lo normal</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Bastante</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Extremadamente</p>
                                </td>
                            </tr>

                            <tr class="text-center">
                                <td>
                                    <label for="whoqol-24" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-24" value="1">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-24" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-24" value="2">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-24" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-24" value="3">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-24" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-24" value="4">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-24" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-24" value="5">
                                    </label>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <hr>
                    <p>¿Cómo de satisfecho/a está con el acceso que tiene a los servicios sanitarios?</p>
                    <table data-role="table" class="ui-responsive table-stroke">
                        <tbody>
                            <tr class="text-center">
                                <td class="col-md-2">
                                    <p>Nada</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Un poco</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Lo normal</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Bastante</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Extremadamente</p>
                                </td>
                            </tr>

                            <tr class="text-center">
                                <td>
                                    <label for="whoqol-25" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-25" value="1">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-25" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-25" value="2">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-25" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-25" value="3">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-25" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-25" value="4">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-25" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-25" value="5">
                                    </label>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <hr>
                    <p>¿Cómo de satisfecho/a está con los servicios de transporte de su zona?</p>
                    <table data-role="table" class="ui-responsive table-stroke">
                        <tbody>
                            <tr class="text-center">
                                <td class="col-md-2">
                                    <p>Nada</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Un poco</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Lo normal</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Bastante</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Extremadamente</p>
                                </td>
                            </tr>

                            <tr class="text-center">
                                <td>
                                    <label for="whoqol-26" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-26" value="1">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-26" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-26" value="2">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-26" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-26" value="3">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-26" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-26" value="4">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-26" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-26" value="5">
                                    </label>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <hr>
                    <p>¿Con qué frecuencia tiene sentimientos negativos, tales como tristeza, desesperanza, ansiedad, o depresión?</p>
                    <table data-role="table" class="ui-responsive table-stroke">
                        <tbody>
                            <tr class="text-center">
                                <td class="col-md-2">
                                    <p>Nada</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Un poco</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Lo normal</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Bastante</p>
                                </td>
                                <td class="col-md-2">
                                    <p>Extremadamente</p>
                                </td>
                            </tr>

                            <tr class="text-center">
                                <td>
                                    <label for="whoqol-27" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-27" value="1">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-27" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-27" value="2">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-27" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-27" value="3">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-27" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-27" value="4">
                                    </label>
                                </td>
                                <td >
                                    <label for="whoqol-27" class="btn btn-default btn-radio-group">
                                        <input type="radio" name="whoqolA3-27" value="5">
                                    </label>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <hr>
                    <div class="form-group">
                        <p>¿Le ha ayudado alguien a rellenar el cuestionario?</p>
                        <textarea class="form-control" name="ayuda" id="ayudaA3" rows="6"></textarea>
                    </div>
                    
                    <div class="form-group">
                        <p>¿Cuánto tiempo ha tardado en contestarlo?</p>
                        <textarea class="form-control" name="tiempo" id="tiempoA3" rows="6"></textarea>
                    </div>
                    
                    <div class="form-group">
                        <p>¿Le gustarí­a hacer algún comentario sobre el cuestionario?</p>
                        <textarea class="form-control" name="comentario" id="comentarioA3" crows="6"></textarea>
                    </div>


                </fieldset>

		<?php
			$tipoPaciente = App\Models\Usuario::obtenerNombreTipoUsuario(Auth::user()->rut);
			if ($tipoPaciente == "medico")
			{
		?>
		<button type="button" class="btn btn-primary pull-right" onclick="habilitarBoton('formWhoqolA3', 'editarWhoqolA3', 'guardarWhoqolA3')" id="editarWhoqolA3">Editar</button>
	
		<button type="button" class="btn btn-success pull-right" id="imprimirWhoqolA3" onclick="imprimirPDF('formWhoqolA3')" >PDF</button>

		<button type="submit" class="btn btn-primary pull-right" id="guardarWhoqolA3" style="display:none;">Guardar</button>
		<?php
			}
		?>
		{{ Form::close() }}
	</div>
</div>
<!--</fieldset>-->

<script type="text/javascript">



$(function(){
    
    
    sumarIndiceWhoqolA3('whoqolA3-',' ',' ', 'formWhoqolA3');
  
	$("#formWhoqolA3").formValidation({
		excluded: ':disabled',
		framework: 'bootstrap',
		fields: {
			"fecha-encuesta": {
				validators:{
					/*notEmpty: {
						message: 'El nombre es obligatorio'
					},*/
                    date: {
                        format: 'DD-MM-YYYY',
                        //min: '01/01/2010',
                        //max: '12/30/2020',
                        message: 'La fecha no es válida'
                    }
				}
			}
                        
		}
	}).on('err.field.fv', function(e, data) {
		if (data.fv.getSubmitButton()) data.fv.disableSubmitButtons(false);
	}).on('success.field.fv', function(e, data) {
		if (data.fv.getSubmitButton()) data.fv.disableSubmitButtons(false);
	}).on("success.form.fv", function(evt){
		console.log("--- submit formWhoqolA3 ---");

		$("#formWhoqolA3 input[type='submit']").prop("disabled", false);
		evt.preventDefault(evt);
		$("#dvLoading").show();
		var $form = $(evt.target);
		
		var form = $(this).serializeArray();
		form.push({name:"rut", value:"{{ $rut }}"});

		$.ajax({
			url: $form.prop("action"),
			data: form,
			type: "post",
			dataType: "json",
			success: function(data){
				if(data.exito) bootbox.alert("<h4>"+data.exito+"</h4>", function(){
					verDatosPaciente();
					//deshabilitarBoton('formTinetiInicial', 'editarTinetiInicial', 'guardarTinetiInicial');
				});
				if(data.error) bootbox.alert("<h4>"+data.error+"</h4>");
				$("#dvLoading").hide();
			},
			error: function(error){
				console.log(error);
				$("#dvLoading").hide();
			}
		});
		return false;
	});


});
</script>






