<script>

var tableAlertaNicturia = null;
var tableAlertaNicturiaDetalle = null;

$(function(){

	tableAlertaNicturia = $('#table-nicturia').DataTable({	
		"iDisplayLength": 10,
		"bJQueryUI": true,
		"info": true,
		"bAutoWidth" : false,
		"responsive": true,
		"searching": false,
        "ordering": false,
        "info": false,
        "order": [[0, "desc"]],
		"oLanguage": {
			"sUrl": "{{URL::to('/')}}/js/spanish.json"
		},
		"ajax": {
			"url": "obtenerAlertasPuertaNicturia",
			"data": {rut: "{{$rut}}", evento: eventos.NICTURIA},
			"type": "post"
		},
		initComplete: function (settings, json) {
            $("#table-nicturia_length").remove();
        },
	});	

	tableAlertaNicturiaDetalle = $('#table-nicturia-detalle').DataTable({	
		"iDisplayLength": 10,
		"bJQueryUI": true,
		"info": true,
		"bAutoWidth" : false,
		"responsive": true,
		"searching": false,
        "ordering": false,
        "info": false,
        "order": [[0, "desc"]],
		"oLanguage": {
			"sUrl": "{{URL::to('/')}}/js/spanish.json"
		},
		initComplete: function (settings, json) {
            $("#table-nicturia-detalle_length").remove();
        }
	});	

	generarGrafico(12, "grafico-12-nicturia", eventos.NICTURIA);
	generarGrafico(30, "grafico-30-nicturia", eventos.NICTURIA);

});

</script>

<div class="row">
	<div class="col-md-5">
		<div id="grafico-12-nicturia"></div>
	</div>
	<div class="col-md-5">
		<div id="grafico-30-nicturia"></div>
	</div>
</div>
<br>

<table id="table-nicturia" class="table table-bordered table-striped dataTable dtr-inline">
	<thead>
		<tr>
			<th>Fecha de alerta</th>
			<th>Cantidad</th>
			<th></th>
		</tr>
	</thead>
</table>

<table id="table-nicturia-detalle" class="table table-bordered table-striped dataTable dtr-inline">
	<thead>
		<tr>
			<th>Fecha detalle alerta</th>
		</tr>
	</thead>
</table>