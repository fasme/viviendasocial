<?php
use App\Models\Usuario;

header('Access-Control-Allow-Origin: *');
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function(){
  return View::make('Sesion/Login');
});

Route::get('index', array('before' => 'auth', function(){
  $rut = Auth::user()->rut;
  return View::make('Index/Index', ["rut" => $rut]);
}));

Route::get('documentos', array('before' => 'auth', function(){
  $tipoUsuario=Usuario::obtenerTipoUsuario(Auth::user()->rut);
  switch($tipoUsuario)
  {
    case 1:
    return Response::download("manual/Familiar.pdf");
    case 2:
    return Response::download("manual/Medico.pdf");
    case 3:
    return Response::download("manual/Administrador.pdf");
    case 5:
    return Response::download("manual/Evaluador 1.pdf");
    case 6:
    return Response::download("manual/Evaluador 2.pdf");
    default:
    return Response::download("manual/Paciente.pdf");
  }
  
}));
Route::get('docs/{num}', function($num){
  
  switch($num)
  {
    case 1:
    return Response::download("manual/Familiar.pdf");
    case 2:
    return Response::download("manual/Medico.pdf");
    case 3:
    return Response::download("manual/Administrador.pdf");
    case 5:
    return Response::download("manual/Evaluador 1.pdf");
    case 6:
    return Response::download("manual/Evaluador 2.pdf");
    default:
    return Response::download("manual/Paciente.pdf");
  }
  
});
Route::group(array('prefix' => 'index'), function() {
  Route::post('ultimosEventosPacientes', array( 'as'=> 'ultimosEventosPacientes', 'uses' => 'MobileController@ultimosEventosPacientes'));
  Route::post('ultimosEventosPacientesDePaciente', array( 'as'=> 'ultimosEventosPacientesDePaciente', 'uses' => 'MobileController@ultimosEventosPacientesDePaciente'));
}); 



Route::group(array('prefix' => 'sesion'), function() {
  Route::post('doLogin', array( 'as'=> 'doLogin', 'uses' => 'SesionController@doLogin'));
  Route::get('cerrarSesion', array( 'as'=> 'cerrarSesion', 'uses' => 'SesionController@cerrarSesion'));
  Route::post('enviarCorreoContacto', array( 'as'=> 'enviarCorreoContacto', 'uses' => 'SesionController@enviarCorreoContacto'));
  Route::post('cargarTodasNoticias', array( 'as'=> 'cargarTodasNoticias', 'uses' => 'SesionController@cargarTodasNoticias'));
  Route::post('cargarNoticia', array( 'as'=> 'cargarNoticia', 'uses' => 'SesionController@cargarNoticia'));
  Route::post('cargarNoticiaLabitec', array( 'as'=> 'cargarNoticiaLabitec', 'uses' => 'SesionController@cargarNoticiaLabitec'));
}); 

Route::group(array('prefix' => 'perfil', 'before' => 'auth'), function() {
  Route::post('editarPerfil', array('as'=> 'editarPerfil', 'uses' => 'PerfilController@editarPerfil'));
});


Route::group(array('prefix' => 'paciente'), function() {
  Route::post('detallePaciente/conclusion', array('as'=> 'conclusion', 'uses' => 'PacienteController@conclusion'));
  Route::get('pacientes', array('as'=> 'pacientes', 'uses' => function(){
    return View::make("Paciente/Paciente", [
      "selectRut" => Funciones::selectRut(),
      "selectComunas" => App\Models\Comuna::obtenerSelectComunas(),
      "selectGenero" => Funciones::selectGenero(),
      "tipoUsuario" => App\Models\Usuario::obtenerTipoUsuario(Auth::user()->rut),
      "selectAlerta" => Funciones::selectAlerta()
      ]);
  }));
  Route::get('informeNicturia', ['as' => 'informeNicturia', 'uses' => function() {
    return View::make("Paciente/InformeNicturia");
  }]);
  Route::get('verInformeNicturia', array('as'=> 'verInformeNicturia', 'uses' => function(){
    return View::make("Paciente/VerInformeNicturia");
  }));
  Route::get('verInformeAccionesRepetidas', array('as'=> 'verInformeAccionesRepetidas', 'uses' => function(){
    return View::make("Paciente/VerInformeAccionesRepetidas");
  }));
  Route::post('guardarInformeNicturia', ['as' => 'guardarInformeNicturia', 'uses' => 'PacienteController@guardarInformeNicturia']);
  Route::post('guardarResumenEncuestas', ['as' => 'guardarResumenEncuestas', 'uses' => 'PacienteController@guardarResumenEncuestas']);
  Route::post('obtenerResumenEncuestas', ['as' => 'obtenerResumenEncuestas', 'uses' => 'PacienteController@obtenerResumenEncuestas']);
  Route::post('guardarRespuestaInformeNicturia', ['as' => 'guardarRespuestaInformeNicturia', 'uses' => 'PacienteController@guardarRespuestaInformeNicturia']);
  Route::post('cargarDatosInformeNicturia', ['as' => 'cargarDatosInformeNicturia', 'uses' => 'PacienteController@cargarDatosInformeNicturia']);//de la respuesta ↑
  Route::post('cargarUsuarios', ['as' => 'cargarUsuarios', 'uses' => 'PacienteController@cargarUsuarios']);
  Route::post('generarInformePDF', ['as' => 'generarInformePDF', 'uses' => 'PacienteController@generarInformePDF']);
  Route::get('procesarDatosTabla1', array('as'=>'procesarDatosTabla1','uses' => 'PacienteController@procesarDatosTabla1'));
  Route::get('procesarDatosTabla2', array('as'=>'procesarDatosTabla2','uses' => 'PacienteController@procesarDatosTabla2'));
  Route::get('procesarInformeAccionesRepetidas', array('as'=>'procesarInformeAccionesRepetidas','uses' => 'PacienteController@procesarInformeAccionesRepetidas'));
  Route::post('procesarDatosAccionesRepetidas', array('as'=>'procesarDatosAccionesRepetidas','uses' => 'PacienteController@procesarDatosAccionesRepetidas'));
  Route::get('pacientesNicturia', ['as' => 'pacientesNicturia', 'uses' => 'PacienteController@obtenerPacientesNicturia']);
  
  Route::get('anexos', array('as'=> 'anexos', 'uses' => function(){
    return View::make("Paciente/Anexos", [
      "selectRut" => Funciones::selectRut(),
      "selectComunas" => Comuna::obtenerSelectComunas(),
      "selectGenero" => Funciones::selectGenero(),
      "tipoUsuario" => Usuario::obtenerTipoUsuario(Auth::user()->rut),
      "selectAlerta" => Funciones::selectAlerta()
      ]);
  }));

  Route::get('puertas', array('as'=> 'puertas', 'uses' => function(){
    return View::make("Paciente/DeteccionPuerta", [
      "selectRut" => Funciones::selectRut(),
      "selectComunas" => Comuna::obtenerSelectComunas(),
      "selectGenero" => Funciones::selectGenero(),
      "tipoUsuario" => Usuario::obtenerTipoUsuario(Auth::user()->rut),
      "selectAlerta" => Funciones::selectAlerta()
      ]);
  }));

  Route::post('subir', array('as'=> 'subir', 'uses' => 'ArchivoController@recibir'));
  Route::post('subir2', array('as'=> 'subir2', 'uses' => 'ArchivoController@recibir2'));

  Route::post('obtenerPacientes', array('as'=> 'obtenerPacientes', 'uses' => 'PacienteController@obtenerPacientes'));
  Route::post('agregarPaciente', array('as'=> 'agregarPaciente', 'uses' => 'PacienteController@agregarPaciente'));
  Route::post('existePaciente', array('as'=> 'existePaciente', 'uses' => 'PacienteController@existePaciente'));
  
  

  //Se usa
  Route::get('detallePaciente/{rut}', array('as'=> 'detallePaciente', 'uses' => function($rut){
  //    var_dump(Evaluacion::obtenerEvaluacionSemanal($rut));
    return View::make("Paciente/PacienteDetalle", [
      "rut" => $rut, 
      "selectEvento" => App\Models\Evento::obtenerEventos(),
      "selectRut" => Funciones::selectRut(),
      "selectComunas" => App\Models\Comuna::obtenerSelectComunas(),
      "selectGenero" => Funciones::selectGenero(),
      "selectInstitucion" => App\Models\Institucion::obtenerInstituciones(),
      "datosPersonales" =>  App\Models\FichaPacienteVista::obtenerDatosPersonales($rut),
      "viveCon" =>  App\Models\Consultas::obtenerEnum("vive_con"),
      "selectAlerta" => Funciones::selectAlerta(),
      "parentesco" => App\Models\RelacionFamiliar::obtenerRelacion(),
      //"evaluacion" =>  App\Models\Evaluacion::obtenerEvaluacionSemanal($rut),
      //"numeroAlerta" =>  App\Models\AlertaCaida::AlertaPaciente($rut),
      //"detalleCartilla" =>  App\Models\DetalleCartillaMicciones::obtenerDetallePaciente($rut),
      //"configuracionNicturia"=>  App\Models\ConfiguracionNicturia::obtenerConfiguraciones(),
      "NombretipoUsuario" =>  App\Models\Usuario::obtenerNombreTipoUsuario(Auth::user()->rut),
      //"resumenEncuesta"=>  App\Http\Controllers\PacienteController::obtenerResumenEncuesta($rut),
      
    ]);
    
  }));

  //se usa
  Route::post('detallePaciente/modificarContacto', array('as'=> 'modificarContacto', 'uses' => 'PacienteController@modificarContacto'));
  Route::post('detallePaciente/eliminarContacto', array('as'=> 'eliminarContacto', 'uses' => 'PacienteController@eliminarContacto'));
  
  Route::post('detallePaciente/buscarResultadosMicciones', array('as'=> 'buscarResultadosMicciones', 'uses' => 'PacienteController@obtenerDatos'));
  Route::post('detallePaciente/guardarConfiguracionMicciones', array('as'=> 'guardarConfiguracionMicciones', 'uses' => 'ConfiguracionCartillaMiccionesController@guardarConfiguracionMicciones'));
  Route::post('detallePaciente/cargarConfiguracionMicciones', array('as'=> 'cargarConfiguracionMicciones', 'uses' => 'ConfiguracionCartillaMiccionesController@cargarConfiguracionMicciones'));
  Route::post('detallePaciente/obtenerContactoPaciente', array('as'=> 'obtenerContactoPaciente', 'uses' => 'ContactoController@obtenerContactoPaciente'));
  Route::post('detallePaciente/obtenerDatosPaciente', array('as'=> 'obtenerDatosPaciente', 'uses' => 'FichaPacienteVistaController@obtenerDatosPaciente'));
  Route::post('eliminarUsuario', array('as'=> 'eliminarUsuario', 'uses' => 'UsuarioController@eliminarUsuario'));
  
  //se usa
  Route::get('detallePaciente/{query}/obtenerRutNombreFamiliar', array('as'=> 'obtenerRutNombreFamiliar', 'uses' => 'EncargadoController@obtenerRutNombreFamiliar'));
  //se usa
  Route::post('detallePaciente/cargarDatosPaciente', array('as'=> 'cargarDatosPaciente', 'uses' => 'EncargadoController@cargarDatosPaciente'));

  Route::get('detallePaciente/{query}/obtenerRutNombreMedico', array('as'=> 'obtenerRutNombreMedico', 'uses' => 'EncargadoController@obtenerRutNombreMedico'));
  Route::post('detallePaciente/obtenerPacientesDeContacto', array('as'=> 'obtenerPacientesDeContacto', 'uses' => 'UsuarioController@obtenerPacientesDeContacto'));
  Route::post('detallePaciente/obtenerAlertasCaidas', array('as'=> 'obtenerAlertasCaidas', 'uses' => 'PacienteController@obtenerAlertasCaidas'));
  Route::post('detallePaciente/obtenerAlertasPuertaNicturia', array('as'=> 'obtenerAlertasPuertaNicturia', 'uses' => 'PacienteController@obtenerAlertasPuertaNicturia'));
  Route::post('detallePaciente/obtenerDetalleAlerta', array('as'=> 'obtenerDetalleAlerta', 'uses' => 'PacienteController@obtenerDetalleAlerta'));
  Route::post('detallePaciente/generarEstadisticasCaidas', array('as'=> 'generarEstadisticasCaidas', 'uses' => 'MobileController@generarEstadisticasCaidas'));
  Route::post('detallePaciente/generarEstadisticas', array('as'=> 'generarEstadisticas', 'uses' => 'MobileController@generarEstadisticas'));
  Route::post('detallePaciente/generarEstadisticasAccionesRepetidas', array('as'=> 'generarEstadisticasAccionesRepetidas', 'uses' => 'ResumenAccionesRepetidasController@generarGrafico'));
  Route::post('detallePaciente/generarResumenDatosAccionesRepetidas', array('as'=> 'generarResumenDatosAccionesRepetidas', 'uses' => 'DeteccionPuertasController@analizarDatos'));
  Route::post('detallePaciente/guardarConfiguracionAccionesRepetidas', array('as'=> 'guardarConfiguracionAccionesRepetidas', 'uses' => 'ConfiguracionEventosRepetidosController@guardar'));
  Route::post('detallePaciente/cargarConfiguracionAccionesRepetidas', array('as'=> 'cargarConfiguracionAccionesRepetidas', 'uses' => 'ConfiguracionEventosRepetidosController@cargar'));
  Route::post('detallePaciente/graficoCartillaMicciones', array('as'=> 'graficoCartillaMicciones', 'uses' => 'MobileController@graficoCartillaMicciones'));
  Route::post('editarDatosPersonales', array('as'=> 'editarDatosPersonales', 'uses' => 'PacienteController@editarDatosPersonales'));
  Route::post('editarEncuestasTimedGetUp', array('as'=> 'editarEncuestasTimedGetUp', 'uses' => 'PacienteController@editarEncuestasTimedGetUp'));
  Route::post('detallePaciente/obtenerEncuestas', array('as'=> 'obtenerEncuestas', 'uses' => 'PacienteController@obtenerEncuestas'));

  Route::post('editarEncuestasSindromesGeriatricos', array('as'=> 'editarEncuestasSindromesGeriatricos', 'uses' => 'PacienteController@editarEncuestasSindromesGeriatricos'));
  Route::post('editarEncuestasIndiceBarthel', array('as'=> 'editarEncuestasIndiceBarthel', 'uses' => 'PacienteController@editarEncuestasIndiceBarthel'));
  Route::post('editarEncuestasLawtonBrody', array('as'=>'editarEncuestaLawrenBrody','uses' => 'PacienteController@editarEncuestasLawtonBrody'));
  Route::post('editarEncuestasTineti', array('as'=>'editarEncuestaTineti','uses' => 'PacienteController@editarEncuestasTineti'));
  
  //se usa
  Route::post('editarEncuestasCharlson', array('as'=>'editarEncuestaCharlson','uses' => 'PacienteController@editarEncuestasCharlson'));
  //se usa
  Route::post('editarEncuestasWhoqol', array('as'=>'editarEncuestaWhoqol','uses' => 'PacienteController@editarEncuestasWhoqol'));

  Route::post('editarEncuestasICIQ', array('as'=>'editarEncuestaICIQ','uses' => 'PacienteController@editarEncuestasICIQ'));
  Route::post('conductaMotriz', array('as'=>'conductaMotriz','uses' => 'PacienteController@conductaMotriz'));
  Route::post('mmse', array('as'=>'mmse','uses' => 'PacienteController@mmse'));

  Route::post('EvaluacionCognitiva', array('as'=>'EvaluacionCognitiva','uses' => 'PacienteController@EvaluacionCognitiva'));
  Route::post('IngresarEvaluacion', array('as'=>'IngresarEvaluacion','uses' => 'PacienteController@IngresarEvaluacion'));
  Route::post('IngresarMicciones', array('as'=>'IngresarMicciones','uses' => 'PacienteController@IngresarMicciones'));
  Route::post('formularioGDS', array('as'=>'formularioGDS','uses' => 'PacienteController@formularioGDS'));
  Route::post('detallePaciente/obtenerEvaluacion', array('as'=>'obtenerEvaluacion','uses' => 'PacienteController@obtenerEvaluacion'));
  Route::post('detallePaciente/obtenerDatosHogar', array('as'=>'obtenerDatosHogar','uses' => 'PacienteController@obtenerDatosHogar'));
});

Route::group(array('prefix' => 'contactos', 'before' => 'auth'), function() {
  Route::get('contacto/{rut}', array('as'=> 'contacto', 'uses' => function($rut){
    return View::make("Contacto/Contactos", ["rut" => $rut, 
      "selectEvento" => Evento::obtenerEventos(),
      "selectRut" => Funciones::selectRut()
      ]);
  }));
  Route::post('contacto/obtenerContactoPaciente', array('as'=> 'obtenerContactoPaciente', 'uses' => 'ContactoController@obtenerContactoPaciente'));
  Route::post('agregarContacto', array('as'=> 'agregarContacto', 'uses' => 'ContactoController@agregarContacto'));
});

Route::group(array('prefix' => 'usuario', 'before' => 'auth'), function() {
  Route::get('usuarios', array('as'=> 'usuarios', 'uses' => function(){
    return View::make("Usuario/Usuario", [
      "selectRut" => Funciones::selectRut(),
      "selectComunas" => Comuna::obtenerSelectComunas(),
      "selectTipoUsuario" => TipoUsuario::obtenerSelect()
      ]);
  }));
  Route::post('obtenerUsuarios', array('as'=> 'obtenerUsuarios', 'uses' => 'UsuarioController@obtenerUsuarios'));
  Route::post('agregarUsuario', array('as'=> 'agregarUsuario', 'uses' => 'UsuarioController@agregarUsuario'));
  Route::post('obtenerDatosUsuario', array('as'=> 'obtenerDatosUsuario', 'uses' => 'UsuarioController@obtenerDatosUsuario'));
  Route::post('eliminarUsuario', array('as'=> 'eliminarUsuario', 'uses' => 'UsuarioController@eliminarUsuario'));
  Route::get('{query}/obtenerNombreRutPacientes', array('as'=> 'obtenerNombreRutPacientes', 'uses' => 'PacienteController@obtenerNombreRutPacientes'));
  Route::post('obtenerPacientesDeContacto', array('as'=> 'obtenerPacientesDeContacto', 'uses' => 'UsuarioController@obtenerPacientesDeContacto'));
  Route::post('asociarPacienteAUsuario', array('as'=> 'asociarPacienteAUsuario', 'uses' => 'ContactoController@asociarPacienteAUsuario'));
  Route::post('existeEncargado', array('as'=> 'existeEncargado', 'uses' => 'UsuarioController@existeEncargado'));
  Route::post('existeUsuario', array('as'=> 'existeUsuario', 'uses' => 'UsuarioController@existeUsuario'));
});



/*Luis Urrea*/


// Route::get('/', function () {
//     return view('welcome');
// });

Route::post('registrarAlertaCaida', array('as'=> 'registrarAlertaCaida', 'uses' => 'AlertaController@registrarAlertaCaida'));

Route::get('registrarAlertaGases/{rut_paciente}/{id_sensor}/{alerta_monoxido}/{gas1}/{gas2}/{gas3}/{humedad}/{temperatura}', array('as'=> 'registrarAlertaGases', 'uses' => 'AlertaController@registrarAlertaGases'));

Route::post('registrarAlertaBotonPanico', array('as'=> 'registrarAlertaBotonPanico', 'uses' => 'AlertaController@registrarAlertaBotonPanico'));

Route::post('enviarAlerta', array('as'=> 'enviarAlerta', 'uses' => 'AlertaController@enviarAlerta'));


Route::group(array('prefix' => 'mobile'), function() {
  Route::post('login', array('as'=> 'login', 'uses' => 'MobileController@login'));
  Route::post('editarPerfil', array('as'=> 'editarPerfil', 'uses' => 'MobileController@editarPerfil'));
	//Route::post('obtenerRutPaciente', array('as'=> 'obtenerRutPaciente', 'uses' => 'MobileController@obtenerRutPaciente'));
  Route::post('getUsuariosEncuesta', array('as'=> 'getUsuariosEncuesta', 'uses' => 'MobileController@getUsuariosEncuesta'));
  Route::post('existeUsuario', array('as'=> 'existeUsuario', 'uses' => 'MobileController@existeUsuario'));
  Route::post('buscarUsuario', array('as'=> 'buscarUsuario', 'uses' => 'MobileController@buscarUsuario'));
  Route::post('obtenerComunas', array('as'=> 'obtenerComunas', 'uses' => 'MobileController@obtenerComunas'));
  Route::post('getPacientesUsuario', array('as'=> 'getPacientesUsuario', 'uses' => 'MobileController@getPacientesUsuario'));
  Route::post('ultimosEventosPacientes', array('as'=> 'ultimosEventosPacientes', 'uses' => 'MobileController@ultimosEventosPacientes'));
  Route::post('buscarFichaMedica', array('as'=> 'buscarFichaMedica', 'uses' => 'MobileController@buscarFichaMedica'));
  Route::post('buscarAlertasCaidas', array('as'=> 'buscarAlertasCaidas', 'uses' => 'MobileController@buscarAlertasCaidas'));
  Route::post('buscarAlertasMonoxido', array('as'=> 'buscarAlertasMonoxido', 'uses' => 'MobileController@buscarAlertasMonoxido'));
  Route::post('buscarAlertasPanico', array('as'=> 'buscarAlertasPanico', 'uses' => 'MobileController@buscarAlertasPanico'));
  Route::post('revisarAlerta', array('as'=> 'revisarAlerta', 'uses' => 'MobileController@revisarAlerta'));
  Route::post('guardarGCM', array('as'=> 'guardarGCM', 'uses' => 'MobileController@guardarGCM'));
  Route::post('guardarMensaje', array('as'=> 'guardarMensaje', 'uses' => 'MobileController@guardarMensaje'));
  Route::post('editarEncuestasSindromesGeriatricosMovil', array('as'=> 'editarEncuestasSindromesGeriatricosMovil', 'uses' => 'MobileController@editarEncuestasSindromesGeriatricosMovil'));
  Route::post('editarEncuestasLawtonBrody', array('as'=> 'editarEncuestasLawtonBrody', 'uses' => 'MobileController@editarEncuestasLawtonBrody'));
  Route::post('editarEncuestasCharlson', array('as'=> 'editarEncuestasCharlson', 'uses' => 'MobileController@editarEncuestasCharlson'));
  Route::post('editarEncuestasIndiceBarthel', array('as'=> 'editarEncuestasIndiceBarthel', 'uses' => 'MobileController@editarEncuestasIndiceBarthel'));
  Route::post('editarEncuestasMmse', array('as'=> 'editarEncuestasMmse', 'uses' => 'MobileController@editarEncuestasMmse'));
  Route::post('editarEncuestasWhoqol', array('as'=> 'editarEncuestasWhoqol', 'uses' => 'MobileController@editarEncuestasWhoqol'));
  Route::post('editarEncuestasEq', array('as'=> 'editarEncuestasEq', 'uses' => 'MobileController@editarEncuestasEq'));
  
  

});


Route::group(array('prefix' => 'evento', 'before' => 'auth'), function() {
  Route::get('eventos', array('as'=> 'eventos', 'uses' => function(){
    return View::make("Evento/Evento");
  }));
  Route::post('obtenerEventos', array('as'=> 'obtenerEventos', 'uses' => 'ConfiguracionEventoController@obtenerEventos'));
  Route::post('editarEvento', array('as'=> 'editarEvento', 'uses' => 'ConfiguracionEventoController@editarEvento'));
  
});

Route::group(array('prefix' => 'enlace'), function() {
  Route::post('registrarAlertaPuertaNicturia', array('as'=> 'registrarAlertaPuertaNicturia', 'uses' => 'AlertaController@registrarAlertaPuertaNicturia'));
  Route::get('registrarAlertaPuertaNicturia', array('as'=> 'registrarAlertaPuertaNicturia', 'uses' => 'AlertaController@registrarAlertaPuertaNicturia'));
  Route::post('actualizarIpPublica', array('as'=> 'actualizarIpPublica', 'uses' => 'AlertaController@actualizarIpPublica'));
});


Route::group(array('prefix' => 'estadisticas', 'before' => 'auth'), function() {
  Route::get('reportes', array('as'=> 'reportes', 'uses' => function(){
    $instituciones = Institucion::obtenerInstituciones();
    $instituciones[0] = "Todos";

    $comunas = Comuna::obtenerSelectComunas();
    $comunas[0] = "Todas";

    return View::make("Estadisticas/Reportes", ["instituciones" => $instituciones, "comunas" => $comunas]);
  }));
  Route::post('obtenerRut', array('as'=> 'obtenerRut', 'uses' => 'EstadisticasController@obtenerRut'));
  Route::post('descargarReporte', array('as'=> 'descargarReporte', 'uses' => 'EstadisticasController@descargarReporte'));
});
